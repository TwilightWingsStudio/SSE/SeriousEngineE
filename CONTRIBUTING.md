This file describes how to contribute to **SeriousEngineE**.

Reporting an Issue
--------------------------------------------------------------------------------
When you want to report bug or request new feature you should **open an issue**.
 - Main rule! Always open **one issue per one bug report (or feature request)**!
 - Before creating issue **search first** in the issues database! If you don't find any relevant match then don't be afraid to open a new issue. If it is actually duplicate then we will just mark it as duplicate and close.
 - Bug reporting:
   - Give us important data about your platform:
     - Operating System
     - Architecture (x86, amd64, etc.)
     - GPU model (and driver version you use)
   - Specify **exact steps to reproduce**. Try to keep steps minimal as possible.
   - You can provide us **simple example** (i.e. saved game, map or any other input data) that causes issue.
 - Mark your issues with corresponding labels.

Merge/Pull Requests
--------------------------------------------------------------------------------
If you want to add new functionality, please make sure that:
 - This functionality is desired.
 - You communicated with other developers on how to implement it best. (maybe create issue before making request)
 - Even if your request does not get merged, your request will be useful for future work by another developer.

Similar rules can be applied when contributing bug fixes. And it is always better discuss the implementation in the bug report first if you are not completely sure about how to fix it best.

**Keeping git history clean.**  
Try to keep your requests under one specific topic. Same with issues. Better make N requests where each request fixes a specific issue than making one big request out of N commits that fixes N amount of issues.

**Follow our Coding Standards & Styling Guidelines.**  
Check STYLE.md file. Everything that you need is described there.

**Commit messages.**  
Describe your changes in imperative mood. Don't use past tenses.

Bad:
 - ``Fixed X in Y``;
 - ``Changed N``;

Good:
 - ``Fix X in Y``;
 - ``Change M in N``;