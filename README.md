**[SEE] - Serious Engine E**
================================================================================
This is the unofficial fork of the Serious Engine 1.

Our goal - improve the Engine in all possible ways.
But... At least we want to provide more clean, documented and polished source
code than an original one.

This repository contains __only pure basic__ components of the Engine.
Why? Because other components related to Gameplay and Tools were split into separate repositories.
Why? More freedom in parallel development, separate versioning, more clean repositories. Enough?

Components:
--------------------------------------------------------------------------------
 * Common
   * `Ecc` - The Entity Class Compiler, a custom build tool used to compile *.es (entity source) files.
   * `Core` - The most important components. Math, resource system, data types etc.
   * `Engine` - The Engine itself.
   * `Shaders` - Library with hardcoded shaders. These shaders can be used on SKA meshes.
   * `Depend` - Small tool that used to build a list of dependency files based on a list of root files.
   * `DecodeReport` - Small tool that used to decode *.rpt (crash report) files.
 * 3rd_party:
   * `libvorbis`, `libogg` - Third party libraries used for playing OGG-encoded ingame music (see http://www.vorbis.com/ for more information)

Every project folder have project files for Visual Studio 2013.

Optional features
--------------------------------------------------------------------------------
There are several optional features disabled by default:
 - DirectX 8.1 renderer
 - MP3 playback
 - 3D Exploration
 - IFeel

##### DirectX 8.1 renderer:
If you need DirectX support you'll have to download DirectX8 SDK (headers & libraries) ( https://www.microsoft.com/en-us/download/details.aspx?id=6812 ) and then enable the SE1_D3D switch for all projects in the solution (Project properties -> Configuration properties -> C/C++ -> Preprocessor -> Preprocessor definitions -> Add "SE1_D3D" for both Debug and Release builds). You will also need to make sure the DirectX8 headers and libraries are located in the following folders (make the folder structure if it's not existing yet):
* `/Tools.Win32/Libraries/DX8SDK/Include/..`
* `/Tools.Win32/Libraries/DX8SDK/Lib/..`

##### MP3 playback:
If you need this feature, you will have to copy `amp11lib.dll` to the `/Bin/` directory (and `/Bin/Debug/` for MP3 support in debug mode). The `amp11lib.dll` is distributed with older versions of Serious Sam: The First Encounter.

##### 3D Exploration:
Disabled in the open source version of Serious Engine 1 due to copyright issues.  
In case if you need to create new models you will have to either use editing tools from any of the original games, or write your own code for 3D object import/export.

##### IFeel
IFeel support is disabled in the open source version of Serious Engine 1 due to copyright issues.  
In case if you need IFeel support you will have to:
 - Enable ``SE1_IFEEL`` preprocessor macros for the Engine project.
 - Copy IFC22.dll and ImmWrapper.dll from the original game into the `/Bin/` folder.


License
--------------------------------------------------------------------------------

Serious Engine is licensed under the GNU GPL v2 (see LICENSE file).

Some of the code included with the engine sources is not licensed under the GNU GPL v2:

* zlib (located in `Sources/Engine/zlib`) by Jean-loup Gailly and Mark Adler
* libogg/libvorbis (located in `Sources/libogg` and `Sources/libvorbis`) by Xiph.Org Foundation