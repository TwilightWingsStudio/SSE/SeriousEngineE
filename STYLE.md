Hello, this document describes how your code should look in context of this project.
Following standards and guidelines is the only way to have readable code.

# **Coding Standards**
Following our coding standards is absolutely essential to have your pull/merge request approved.  
While we may not close your pull/merge request if it doesn't follow these coding standards, we most likely will delay merging it until compliant.

#### **Coding standards - General**
 - Tabs Size: **2**
 - Use Spaces: **Yes** (only spaces!)
 - Max Line Length: **120**
 - GPLv2 license header should be included in every file with the source code.
 - C/C++ header files should not have multiple classes declared at once.
    - Exceptions: Data storage-like classes with only fields and without any methods.

#### **Coding standards - Expressions**
--------------------------------------------------------------------------------
 - Every operand should be split with spaces from operators that stay in-between.
    - Wrong: ``c=a*b``
    - Right: ``c = a * b``
 - In these cases operation characters should stay with operand.
     - ``*`` (dereference) operator.
        - Wrong: ``* pSomePointer``
        - Right: ``*pSomePointer``
     - ``&`` (address of) operator.
        - Wrong: ``& x;``
        - Right: ``&x;``
     - ``!`` (NOT) operator.
        - Wrong: ``! bVar``
        - Right: ``!bVar``

#### **Coding standards - Conditions** (if, else, else if)
 - There should always be space in-between the keyword and a round bracket.
    - Wrong: ``if(``
    - Right:  `` if (``
 - There should always be space in-between the round bracket and a curly bracket.
    - Wrong: ``if (true){``
    - Right: ``if (true) {``
 - When using ``else`` or ``else if`` then curly brackets should be on same line.
```cpp
// Wrong.
}
else
{

// Wrong.
} else
{

// Wrong.
}
else {

// Correct way how it should look.
} else {
```

#### **Coding standards - Loops** (for, while, do-while)
--------------------------------------------------------------------------------
 - There should always be space in-between the keyword and a round bracket.
    - Wrong: ``while(``
    - Right:  `` while (``
 - Loop body block open bracket should be on its own line.
    - Exceptions:
      - ``do {``.
      - If there is one line of code inside the loop.
    - Wrong: ``while (true) {``
```cpp
// Right.
while (true)
{
```
 - When using ``do-while`` there should always be space in-between body closing curly bracket and a ``while`` keyword.
    - Wrong: ``}while (true);``
    - Right:  ``} while (true);``

#### **Coding standards - Methods and Functions**
--------------------------------------------------------------------------------
 - NEVER make one-line function definitions.
```cpp
// Wrong.
void SomeFunc() { /* some code */ }
```
 - Function body brackets should always be on separate lines.
```cpp
// Correct way how it should look.
void SomeFunc()
{
  /* some code */
}
```
 - Function definitions should always be split with empty line.
```cpp
// Wrong.

void DoSomething1()
{
  /* some code */
}
void DoSomething2()
{
  /* some code */
}
```
```cpp
// Correct way:

void DoSomething1()
{
  /* some code */
}

void DoSomething2()
{
  /* some code */
}
```

#### **Coding standards - Declarations** (enum, struct, class)
 - Body open bracket should be on own line.
    - Wrong: ``class CSomething {``
```cpp
// Correct way how it should look.
struct SSomething
{
  /* declarations */
};

class CSomething
{
  /* fields & methods declarations */
};

enum ESomething
{
  /* variants */
};
```


#### **Coding standards - Access Modifiers**
--------------------------------------------------------------------------------
 - ``public``, ``protected``, ``private`` within class body should be tabbed.
 - Declarations relative to ``public``, ``protected``, ``private`` should be tabbed.
```cpp
// Correct way how it should look.
class CSomething
{
  private:
    int iSomething;

  protected:
    float X;

  public:
    float GetX();
};
```
#### **Coding standards - switch-case**
--------------------------------------------------------------------------------
 - There should always be space in-between the keyword and a round bracket.
    - Wrong: ``switch(``
    - Right:  `` switch (``

 - **switch** body block open bracket should be on own line.
    - Wrong: ``switch (i) {``
```cpp
// Right.
switch (i)
{
```
 - ``case`` or ``default`` within **switch** always should be tabbed.
```cpp
// Wrong!
switch (i)
{
case 1:
```
 - ``case`` or ``default`` and bracket on one line always should be split with space character.
    - Wrong: ``case 1:{``
    - Right: ``case 1: {``
 - Statements relative to ``case`` or ``default`` should be tabbed.
```cpp
// Correct way how it should look.
switch (i)
{
  case 1: {
    SomeFunc(1);
  } break;

  case 2: {
    SomeFunc(2);
  } break;

  case 3:
  default: {
    SomeFunc(3);
  } break;
}
```

#### **Coding standards - Comments**
--------------------------------------------------------------------------------
 - There should always be space in-between slashes and a comment text.
    - Wrong: ``//This code does something.``
    - Right: ``// This code does something.``
 - NEVER use multi-line comments in cases where the comment is just a single line of text.
    - Wrong: ``/* This code does something. */``
    - Right: ``// This code does something.``
 - Use Doxygen-style comments notation for comments related to classes, functions, structures and members declarations and definitions.
   - Single-line comments: ``//!``
     - It is possible to use it as multi-line comments alternative. Just stack multiple single-line comments.
   - Multi-line comments: ``/** */``
```cpp
//! This function does something.
void DoSomething();

//! Test class.
class CTest
{
  public:
    //! Constructor.
    CTest();

    //! Test function.
    void Test();

  private:
    float _fSize; //! Object size.
};
```

# **Styling Guidelines**
--------------------------------------------------------------------------------
These styling guidelines represent the way we prefer to have all the things styled. Ignoring these guidelines will not affect my decision on approving your pull/merge request.

#### **Style - Operator Without Brackets**
--------------------------------------------------------------------------------
 - Just avoid using it!
```cpp
// Just ugly.
if (condition) return;

// Thats ugly too.
if (condition)
  return;

// This looks fine! :)
if (condition) {
  return;
}
```

#### **Style - Comments Near Function Declarations**
--------------------------------------------------------------------------------
If you have comment line for function declaration it is better put empty line between.

Bad:
```cpp
//! <comment for function1>
void function1();
//! <comment for function2>
void function2()
```

Good:
```cpp
//! <comment for function1>
void function1();

//! <comment for function2>
void function2()
```

#### **Style - Empty Lines**
--------------------------------------------------------------------------------
 - Avoid putting more than one empty line at any case.