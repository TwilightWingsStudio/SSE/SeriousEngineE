/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>
#include <Core/Core.h>

#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/Terrain_p.h>
#include <Engine/Resources/ResourceManager.h>

extern CTerrain *_ptrTerrain;

#define TR_CURRENT_VERSION 9

static void ReadTerrainVersion9_t(CTerrain *ptrTerrain, CTStream *istrFile)
{
  ASSERT(ptrTerrain != NULL);
  CTerrainPrivate *d_ptr = ptrTerrain->d_ptr;
  
  ASSERT(_CrtCheckMemory());
  PIX pixHeightMapWidth;
  PIX pixHeightMapHeight;
  SLONG slShadowMapSizeAspect;
  SLONG slShadingMapSizeAspect;
  INDEX cttl;

  // Read height map width and height
  (*istrFile) >> pixHeightMapWidth;
  (*istrFile) >> pixHeightMapHeight;
  (*istrFile) >> d_ptr->tr_vStretch;
  (*istrFile) >> d_ptr->tr_fDistFactor;
  (*istrFile) >> d_ptr->tr_vTerrainSize;

  // Reallocate memory for terrain with size
  ptrTerrain->AllocateHeightMap(pixHeightMapWidth, pixHeightMapHeight);

  istrFile->ExpectID_t("TRSM"); // 'Terrain shadowmap'

  (*istrFile) >> slShadowMapSizeAspect;
  (*istrFile) >> slShadingMapSizeAspect;

  ptrTerrain->SetShadowMapsSize(slShadowMapSizeAspect, slShadingMapSizeAspect);
  
  const PIX pixShadowMapWidth = CTerrain::GetShadowMapDim(pixHeightMapWidth, slShadowMapSizeAspect);
  const PIX pixShadowMapHeight = CTerrain::GetShadowMapDim(pixHeightMapHeight, slShadowMapSizeAspect);
  const PIX pixShadingMapWidth = ptrTerrain->GetShadingMapWidth();
  const PIX pixShadingMapHeight = ptrTerrain->GetShadingMapHeight();

  const SLONG slHeightMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UWORD);
  const SLONG slShadowMapSize = pixShadowMapWidth * pixShadowMapHeight * sizeof(ULONG);
  const SLONG slShadingMapSize = pixShadingMapWidth * pixShadingMapHeight * sizeof(UWORD);
  const SLONG slEdgeMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UBYTE);

  // Read shadow map
  ASSERT(ptrTerrain->GetShadowMapTex()->td_pulFrames != NULL);
  istrFile->Read_t(&ptrTerrain->GetShadowMapTex()->td_pulFrames[0], slShadowMapSize);

  // Read shading map
  ASSERT(ptrTerrain->GetShadingMap() != NULL);
  istrFile->Read_t(&ptrTerrain->GetShadingMap()[0], slShadingMapSize);

  // Create shadow map mipmaps
  INDEX ctMipMaps = GetNoOfMipmaps(pixShadowMapWidth, pixShadowMapHeight);
  MakeMipmaps(ctMipMaps, ptrTerrain->GetShadowMapTex()->td_pulFrames, pixShadowMapWidth, pixShadowMapHeight);

  // Upload shadow map
  ptrTerrain->GetShadowMapTex()->SetAsCurrent(0, TRUE);

  istrFile->ExpectID_t("TSEN"); // 'Terrain shadowmap end'

  // If there is edge map saved - Read terrain edge map
  if (istrFile->PeekID_t() == CChunkID("TREM")) { // 'Terrain edge map'
    istrFile->ExpectID_t("TREM"); // 'Terrain edge map'
    istrFile->Read_t(&ptrTerrain->GetEdgeMap()[0], slEdgeMapSize);
    istrFile->ExpectID_t("TEEN"); // 'Terrain edge map end'
  }

  // Read height map
  (*istrFile).ExpectID_t("TRHM");  // 'Terrain heightmap'
  (*istrFile).Read_t(&ptrTerrain->GetHeightMap()[0], slHeightMapSize);
  (*istrFile).ExpectID_t("THEN");  // 'Terrain heightmap end'

  // Read Terrain layers
  (*istrFile).ExpectID_t("TRLR");  // 'Terrain layers'

  // Read terrain layers
  (*istrFile) >> cttl;

  // Create layers
  ptrTerrain->d_ptr->tr_atlLayers.Push(cttl);
  // For each terrain layer
  for (INDEX itl = 0; itl < cttl; itl++)
  {
    ptrTerrain->ReadLayerOld_t(itl, istrFile);
  }

  (*istrFile).ExpectID_t("TLEN");  // 'Terrain layers end'

  // End of reading
  (*istrFile).ExpectID_t("TREN");  // 'terrain end'
}

static void ReadTerrainVersion12_t(CTerrain *ptrTerrain, CTStream *istrFile)
{
  ASSERT(ptrTerrain != NULL);
  CTerrainPrivate *d_ptr = ptrTerrain->d_ptr;

  ASSERT(_CrtCheckMemory());
  PIX pixHeightMapWidth;
  PIX pixHeightMapHeight;
  SLONG slShadowMapSizeAspect;
  SLONG slShadingMapSizeAspect;
  INDEX cttl;

  // Terrain Global Data
  istrFile->ExpectID_t("TRGD");
  (*istrFile) >> pixHeightMapWidth;
  (*istrFile) >> pixHeightMapHeight;
  (*istrFile) >> slShadowMapSizeAspect;
  (*istrFile) >> slShadingMapSizeAspect;
  (*istrFile) >> cttl;
  (*istrFile) >> d_ptr->tr_fDistFactor;
  (*istrFile) >> d_ptr->tr_vStretch;
  (*istrFile) >> d_ptr->tr_vTerrainSize;
  
  const PIX pixEdgeMapWidthTemp = pixHeightMapWidth - 1;
  const PIX pixEdgeMapHeightTemp = pixHeightMapHeight - 1;
  const PIX pixShadowMapWidth = CTerrain::GetShadowMapDim(pixHeightMapWidth, slShadowMapSizeAspect);
  const PIX pixShadowMapHeight = CTerrain::GetShadowMapDim(pixHeightMapHeight, slShadowMapSizeAspect);
  
  const SLONG slHeightMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UWORD);
  const SLONG slEdgeMapSizeTemp = pixEdgeMapWidthTemp * pixEdgeMapHeightTemp * sizeof(UBYTE); // Version 12
  const SLONG slEdgeMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UBYTE); // Version 9
  const SLONG slShadowMapSize = pixShadowMapWidth * pixShadowMapHeight * sizeof(ULONG);

  // Reallocate memory for terrain with size
  ptrTerrain->AllocateHeightMap(pixHeightMapWidth, pixHeightMapHeight);
  ptrTerrain->SetShadowMapsSize(slShadowMapSizeAspect, slShadingMapSizeAspect);
  
  UWORD *puwHeightMap = ptrTerrain->GetHeightMap();
  UBYTE *pubEdgeMapTemp = (UBYTE *)AllocMemory(slEdgeMapSize); // Version 12
  UBYTE *pubEdgeMap   = ptrTerrain->GetEdgeMap(); // Version 9
  ULONG *pulShadowMap = ptrTerrain->GetShadowMapTex()->td_pulFrames;
  
  ASSERT(pulShadowMap != NULL);
  
  // Terrain Heighmap
  istrFile->ExpectID_t("TRHM");
  istrFile->Read_t(&puwHeightMap[0], slHeightMapSize);
  
  // Terrain Edge Map
  istrFile->ExpectID_t("TREM");
  istrFile->Read_t(&pubEdgeMapTemp[0], slEdgeMapSizeTemp);
  
  // Terrain Shadow Map
  istrFile->ExpectID_t("TRSM");
  istrFile->Read_t(pulShadowMap, slShadowMapSize);
  
  // Create layers
  ptrTerrain->d_ptr->tr_atlLayers.Push(cttl);
  
  // Convert edge map from new format to old one.
  {
    memset(pubEdgeMap, 0, slEdgeMapSize);

    // For each line. Each line in 12 version is 1 pixel shorter.
    for (INDEX iLine = 0; iLine < pixEdgeMapHeightTemp; iLine++)
    {
      UBYTE *pubLineSrc = &pubEdgeMapTemp[iLine * pixEdgeMapWidthTemp]; // ptr + iLine * (pixHeightMapWidth - 1)
      UBYTE *pubLineDst =  &pubEdgeMap[iLine * pixHeightMapWidth]; // ptr + iLine * pixHeightMapWidth
      memcpy(pubLineDst, pubLineSrc, pixEdgeMapWidthTemp);
    }
    
    FreeMemory(pubEdgeMapTemp);
  }

  // For each terrain layer
  for (INDEX itl = 0; itl < cttl; itl++)
  {
    ptrTerrain->ReadLayer_t(itl, istrFile);
  }
  
  // Terrain data end
  istrFile->ExpectID_t("TRDE");
  
  // Create shadow map mipmaps
  INDEX ctMipMaps = GetNoOfMipmaps(pixShadowMapWidth, pixShadowMapHeight);
  MakeMipmaps(ctMipMaps, ptrTerrain->GetShadowMapTex()->td_pulFrames, pixShadowMapWidth, pixShadowMapHeight);

  // Upload shadow map
  ptrTerrain->GetShadowMapTex()->SetAsCurrent(0, TRUE);
}

static void WriteTerrainVersion9_t(CTerrain *ptrTerrain, CTStream *ostrFile)
{
  ASSERT(ptrTerrain != NULL);
  CTerrainPrivate *d_ptr = ptrTerrain->d_ptr;
  
  // Get pixel dimensions.
  const PIX pixHeightMapWidth = ptrTerrain->GetHeightMapWidth();
  const PIX pixHeightMapHeight = ptrTerrain->GetHeightMapHeight();
  const PIX pixShadowMapWidth = CTerrain::GetShadowMapDim(pixHeightMapWidth, d_ptr->tr_iShadowMapSizeAspect);
  const PIX pixShadowMapHeight = CTerrain::GetShadowMapDim(pixHeightMapHeight, d_ptr->tr_iShadowMapSizeAspect);
  
  const INDEX cttl = d_ptr->tr_atlLayers.Count();

  // Find size for big memory pieces.
  const SLONG slHeightMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UWORD);
  const SLONG slEdgeMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UBYTE);
  const SLONG slShadowMapSize = pixShadowMapWidth * pixShadowMapHeight * sizeof(ULONG);
  const SLONG slShadingMapSize = ptrTerrain->GetShadingMapWidth() * ptrTerrain->GetShadingMapHeight() * sizeof(UWORD);

  const UWORD *puwHeightMap = ptrTerrain->GetHeightMap();
  const ULONG *pulShadowMap = ptrTerrain->GetShadowMapTex()->td_pulFrames;
  const UBYTE *pubEdgeMap   = ptrTerrain->GetEdgeMap(); // Version 9

  // Write the terrain version
  ostrFile->WriteID_t("TERR");  // 'Terrain'
  (*ostrFile) << ULONG(TR_CURRENT_VERSION);
  
  // Write terrain global data.
  (*ostrFile) << pixHeightMapWidth;
  (*ostrFile) << pixHeightMapHeight;
  (*ostrFile) << ptrTerrain->GetStretch();
  (*ostrFile) << ptrTerrain->GetDistFactor();
  (*ostrFile) << d_ptr->tr_vTerrainSize;

  // Write terrain shadow map
  ostrFile->WriteID_t("TRSM");    // 'Terrain shadowmap'

  (*ostrFile) << d_ptr->tr_iShadowMapSizeAspect;
  (*ostrFile) << d_ptr->tr_iShadingMapSizeAspect;

  // Write shadow map
  ASSERT(pulShadowMap != NULL);
  ostrFile->Write_t(pulShadowMap, slShadowMapSize);
  
  // Write shading map
  ASSERT(ptrTerrain->GetShadingMap() != NULL);
  ostrFile->Write_t(&ptrTerrain->GetShadingMap()[0], slShadingMapSize);

  ostrFile->WriteID_t("TSEN");    // 'Terrain shadowmap end'

  // If edge map exists - Write edge map
  if (pubEdgeMap != NULL) {
    ostrFile->WriteID_t("TREM");    // 'Terrain edge map'
    ostrFile->Write_t(pubEdgeMap, slEdgeMapSize);
    ostrFile->WriteID_t("TEEN");    // 'Terrain edge map end'
  }

  // Write height map
  ostrFile->WriteID_t("TRHM");  // 'Terrain heightmap'
  ostrFile->Write_t(puwHeightMap, slHeightMapSize);
  ostrFile->WriteID_t("THEN");  // 'Terrain heightmap end'

  // Write terrain layers
  ostrFile->WriteID_t("TRLR");  // 'Terrain layers'
  (*ostrFile) << cttl;

  for (INDEX itl = 0; itl < cttl; itl++)
  {
    ptrTerrain->WriteLayerOld_t(itl, ostrFile);
  }

  ostrFile->WriteID_t("TLEN");  // 'Terrain layers end'

  // End marker!
  ostrFile->WriteID_t("TREN");  // 'terrain end'
}

static void WriteTerrainVersion12_t(CTerrain *ptrTerrain, CTStream *ostrFile)
{
  ASSERT(ptrTerrain != NULL);
  CTerrainPrivate *d_ptr = ptrTerrain->d_ptr;
  
  // Get some numbers...
  const INDEX cttl = d_ptr->tr_atlLayers.Count();
  const PIX pixHeightMapWidth = ptrTerrain->GetHeightMapWidth();
  const PIX pixHeightMapHeight = ptrTerrain->GetHeightMapHeight();
  const PIX pixShadowMapWidth = CTerrain::GetShadowMapDim(pixHeightMapWidth, d_ptr->tr_iShadowMapSizeAspect);
  const PIX pixShadowMapHeight = CTerrain::GetShadowMapDim(pixHeightMapHeight, d_ptr->tr_iShadowMapSizeAspect);
  
  // Find size for big memory pieces.
  const SLONG slHeightMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UWORD);
  const SLONG slEdgeMapSize = pixHeightMapWidth * pixHeightMapHeight * sizeof(UBYTE); // Version 9
  const SLONG slShadowMapSize = pixShadowMapWidth * pixShadowMapHeight * sizeof(ULONG);
  
  const UWORD *puwHeightMap = ptrTerrain->GetHeightMap();
  const ULONG *pulShadowMap = ptrTerrain->GetShadowMapTex()->td_pulFrames;
  
  ostrFile->WriteID_t("TRVR");
  (*ostrFile) << INDEX(12);
  
  // Terrain Global Data
  ostrFile->WriteID_t("TRGD");
  (*ostrFile) << pixHeightMapWidth;
  (*ostrFile) << pixHeightMapHeight;
  (*ostrFile) << d_ptr->tr_iShadowMapSizeAspect;
  (*ostrFile) << d_ptr->tr_iShadingMapSizeAspect;
  (*ostrFile) << cttl;
  (*ostrFile) << ptrTerrain->GetDistFactor();
  (*ostrFile) << ptrTerrain->GetStretch();
  (*ostrFile) << d_ptr->tr_vTerrainSize;
  
  // Terrain Height Map
  ostrFile->WriteID_t("TRHM");
  ostrFile->Write_t(puwHeightMap, slHeightMapSize);
  
  // Terrain Edge Map
  ostrFile->WriteID_t("TREM");
  {
    const PIX pixEdgeMapWidthTemp = pixHeightMapWidth - 1;
    const PIX pixEdgeMapHeightTemp = pixHeightMapHeight - 1;
    const SLONG slEdgeMapSizeTemp = pixEdgeMapWidthTemp * pixEdgeMapHeightTemp * sizeof(UBYTE); // Version 12

    const UBYTE *pubEdgeMap   = ptrTerrain->GetEdgeMap(); // Version 9
    UBYTE *pubEdgeMapTemp = (UBYTE *)AllocMemory(slEdgeMapSizeTemp);
    memset(pubEdgeMapTemp, 0, slEdgeMapSizeTemp);
    
    for (INDEX iLine = 0; iLine < pixEdgeMapHeightTemp; iLine++)
    {
      const UBYTE *pubLineSrc =  &pubEdgeMap[iLine * pixHeightMapWidth]; // ptr + iLine * pixHeightMapWidth
      UBYTE *pubLineDst = &pubEdgeMapTemp[iLine * pixEdgeMapWidthTemp]; // ptr + iLine * (pixHeightMapWidth - 1)
      memcpy(pubLineDst, pubLineSrc, pixEdgeMapWidthTemp);
    }
    
    ostrFile->Write_t(pubEdgeMapTemp, slEdgeMapSizeTemp);
    FreeMemory(pubEdgeMapTemp);
  }

  // Terrain Shadowmap
  ostrFile->WriteID_t("TRSM");
  ostrFile->Write_t(pulShadowMap, slShadowMapSize);
  
  for (INDEX itl = 0; itl < cttl; itl++)
  {
    ptrTerrain->WriteLayer_t(itl, ostrFile);
  }
  
  // Terrain data end
  ostrFile->WriteID_t("TRDE");
}

// Read from stream.
void CTerrain::Read_t(CTStream *istrFile)
{
  // Set current terrain
  _ptrTerrain = this;
  
  CChunkID cidBegin = istrFile->PeekID_t();
  
  if (cidBegin == CChunkID("TERR")) {
    istrFile->ExpectID_t("TERR");  // 'Terrain'

    // Read the version number
    INDEX iSavedVersion;
    (*istrFile) >> iSavedVersion;

    ReadTerrainVersion9_t(this, istrFile);

  } else {
    istrFile->ExpectID_t("TRVR");  // 'Terrain Version'

    // Read the version number
    INDEX iSavedVersion;
    (*istrFile) >> iSavedVersion;

    if (iSavedVersion == 12) {
      ReadTerrainVersion12_t(this, istrFile);
    } else {
      ThrowF_t(TRANS("The terrain version on disk is %d.\n"
        "Current supported version is %d."), iSavedVersion, TR_CURRENT_VERSION);
    }
  }

  // Terrain will be rebuild in entity.cpp
  _ptrTerrain = NULL;
}

// Write to stream.
void CTerrain::Write_t(CTStream *ostrFile)
{
  WriteTerrainVersion9_t(this, ostrFile);
}
