/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ARRAY_HOLDER_H
#define SE_INCL_ARRAY_HOLDER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/Graphics/Vertex.h>
#include <Core/Templates/StaticStackArray.cpp>
#include <Core/Templates/DynamicContainer.h>
#include <Core/Templates/DynamicContainer.cpp>
#include <Engine/Graphics/Texture.h>

/*
 * TODO: Add comment here
 */
struct TileLayer
{
  TStaticStackArray<INDEX>       tl_auiIndices;   // Array of indices for one layer
  TStaticStackArray<GFXColor>    tl_acColors;     // Array of colors for one layer
  TStaticStackArray<GFXTexCoord> tl_atcTexCoords; // Array of texcoords for one layer
  TStaticStackArray<GFXVertex4>   tl_avVertices;   // Array of vertices  for one layer (used only if tile layer)
};

/*
 * TODO: Add comment here
 */
struct TileArrays
{
  void operator=(const TileArrays &taOther)
  {
    this->ta_avVertices   = taOther.ta_avVertices;
    this->ta_auvTexCoords = taOther.ta_auvTexCoords;
    this->ta_auvShadowMap = taOther.ta_auvShadowMap;
    this->ta_auvDetailMap = taOther.ta_auvDetailMap;
    this->ta_auiIndices   = taOther.ta_auiIndices;
    this->ta_atlLayers    = taOther.ta_atlLayers;
    this->ta_ptdTopMap    = taOther.ta_ptdTopMap;
  }
  TStaticStackArray<GFXVertex4>  ta_avVertices;   // Array of vertices for one tile
  TStaticStackArray<GFXTexCoord> ta_auvTexCoords; // Array of texcoords for one tile (not used in highest lod)
  TStaticStackArray<GFXTexCoord> ta_auvShadowMap; // Array of texcoords for shadow map
  TStaticStackArray<GFXTexCoord> ta_auvDetailMap; // Array of texcoords for detail map
  TStaticStackArray<INDEX>       ta_auiIndices;   // Array of indices for one tile
  TStaticStackArray<TileLayer>   ta_atlLayers;    // Array if layers per tile (used only in highest lod)
  CTextureData                  *ta_ptdTopMap;    // Pointer to tile top map
};

/*
 * TODO: Add comment here
 */
class ENGINE_API CArrayHolder
{
  public:
    CArrayHolder();
    ~CArrayHolder();
    void operator=(const CArrayHolder &ahOther);

    // Returns index of new tile arrays
    INDEX GetNewArrays();
    
    // Mark tile arrays as unused
    void FreeArrays(INT iOldArraysIndex);
    
    // Just do popall on all arrays
    void EmptyArrays(INDEX iArrayIndex);
    
    // Release array holder
    void Clear(void);
    
    // Count used memory
    SLONG GetUsedMemory(void);

  public:
    CTerrain *ah_ptrTerrain; // Terrain that owns this array holder
    TStaticStackArray<TileArrays> ah_ataTileArrays; // array of tile arrays
    TStaticStackArray<INDEX>      ah_aiFreeArrays;  // array of indices of free arrays
    INDEX ah_iLod; // this array holder works in this lod
};

#endif