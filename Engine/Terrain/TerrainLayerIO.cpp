/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/IO/Stream.h>
#include <Core/Base/ErrorReporting.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/TerrainLayer.h>

static void ReadLayerVersion12_t(CTerrain *ptr, INDEX iTerrainLayer, CTStream *istrFile)
{
  CTerrainLayer *pLayer = &ptr->GetLayer(iTerrainLayer);
  CTerrainLayer *d_ptr = pLayer;

  const PIX pixLayerMaskWidth = ptr->GetHeightMapWidth(); // Version 9
  const PIX pixLayerMaskHeight = ptr->GetHeightMapHeight(); // Version 9
  const PIX pixLayerMaskWidthTemp   = ptr->GetHeightMapWidth() - 1; // Version 12
  const PIX pixLayerMaskHeightTemp  = ptr->GetHeightMapHeight() - 1; // Version 12
  const SLONG slLayerMaskSize   = pixLayerMaskWidth * pixLayerMaskHeight * sizeof(UBYTE); // Version 9
  const SLONG slLayerMaskSizeTemp   = pixLayerMaskWidthTemp * pixLayerMaskHeightTemp * sizeof(UBYTE); // Version 12
  
  // Set layer size
  pLayer->SetLayerSize(pixLayerMaskWidth, pixLayerMaskHeight);

  INDEX iLayerType;
  
  istrFile->ExpectID_t("TRLG");  // 'Terrain layer global (data)'
  (*istrFile) >> d_ptr->tl_strName;
  (*istrFile) >> d_ptr->tl_bVisible;
  (*istrFile) >> iLayerType;
  (*istrFile) >> d_ptr->tl_colMultiply;
  (*istrFile) >> d_ptr->tl_ulFlags;
  
  d_ptr->tl_ltType = (LayerType)iLayerType;
  
  (*istrFile) >> d_ptr->tl_fOffsetX;
  (*istrFile) >> d_ptr->tl_fOffsetY;
  (*istrFile) >> d_ptr->tl_fRotateX;
  (*istrFile) >> d_ptr->tl_fRotateY;
  (*istrFile) >> d_ptr->tl_fStretchX;
  (*istrFile) >> d_ptr->tl_fStretchY;
  
  (*istrFile) >> d_ptr->tl_bAutoRegenerated;
  (*istrFile) >> d_ptr->tl_fCoverage;
  (*istrFile) >> d_ptr->tl_fCoverageNoise;
  (*istrFile) >> d_ptr->tl_fCoverageRandom;

  (*istrFile) >> d_ptr->tl_bApplyMinAltitude;
  (*istrFile) >> d_ptr->tl_bApplyMaxAltitude;
  (*istrFile) >> d_ptr->tl_fMinAltitude;
  (*istrFile) >> d_ptr->tl_fMaxAltitude;
  (*istrFile) >> d_ptr->tl_fMinAltitudeFade;
  (*istrFile) >> d_ptr->tl_fMaxAltitudeFade;
  (*istrFile) >> d_ptr->tl_fMinAltitudeNoise;
  (*istrFile) >> d_ptr->tl_fMaxAltitudeNoise;
  (*istrFile) >> d_ptr->tl_fMinAltitudeRandom;
  (*istrFile) >> d_ptr->tl_fMaxAltitudeRandom;
  (*istrFile) >> d_ptr->tl_bApplyMinSlope;
  (*istrFile) >> d_ptr->tl_bApplyMaxSlope;
  (*istrFile) >> d_ptr->tl_fMinSlope;
  (*istrFile) >> d_ptr->tl_fMaxSlope;
  (*istrFile) >> d_ptr->tl_fMinSlopeFade;
  (*istrFile) >> d_ptr->tl_fMaxSlopeFade;
  (*istrFile) >> d_ptr->tl_fMinSlopeNoise;
  (*istrFile) >> d_ptr->tl_fMaxSlopeNoise;
  (*istrFile) >> d_ptr->tl_fMinSlopeRandom;
  (*istrFile) >> d_ptr->tl_fMaxSlopeRandom;
  
	istrFile->ExpectID_t("TRLM"); // 'Terrain Layer Mask'
  {
    UBYTE *pubLayerMask = &pLayer->tl_aubColors[0]; // Version 9
    UBYTE *pubLayerMaskTemp = (UBYTE *)AllocMemory(slLayerMaskSizeTemp); // Version 12
    
    istrFile->Read_t(&pubLayerMaskTemp[0], slLayerMaskSizeTemp);
    
    memset(pubLayerMask, 0, slLayerMaskSize);

    // For each line. Each line in 12 version is 1 pixel shorter.
    for (INDEX iLine = 0; iLine < pixLayerMaskHeightTemp; iLine++)
    {
      UBYTE *pubLineSrc = &pubLayerMaskTemp[iLine * pixLayerMaskWidthTemp];
      UBYTE *pubLineDst =  &pubLayerMask[iLine * pixLayerMaskWidth];
      memcpy(pubLineDst, pubLineSrc, pixLayerMaskWidthTemp);
    }

    FreeMemory(pubLayerMaskTemp);
  }
  
}

static void ReadLayerVersion9_t(CTerrain *ptr, INDEX iTerrainLayer, CTStream *istrFile)
{
  CTerrainLayer *pLayer = &ptr->GetLayer(iTerrainLayer);
  CTerrainLayer *d_ptr = pLayer;
  
  CTFileName fnmTexture;
  INDEX iMaskWidth;
  INDEX iMaskHeight;

  // Read terrain layer texture
  istrFile->ExpectID_t("TLTX");  // 'Terrain layer texture'
  (*istrFile) >> fnmTexture;
  
  // Add texture to layer
  pLayer->SetLayerTexture_t(fnmTexture);  
  
  // Read terrain layer mask
  istrFile->ExpectID_t("TLMA"); // 'Terrain layer mask'
  (*istrFile) >> iMaskWidth;
  (*istrFile) >> iMaskHeight;

  // Set layer size
  pLayer->SetLayerSize(iMaskWidth, iMaskHeight);

  const SLONG slMaskSize = d_ptr->tl_iMaskWidth * d_ptr->tl_iMaskHeight * sizeof(UBYTE);
  istrFile->Read_t(&pLayer->tl_aubColors[0], slMaskSize);
  
  // Read terrain layer params
  istrFile->ExpectID_t("TLPR"); // 'Terrain layer params'

  (*istrFile) >> d_ptr->tl_strName;
  (*istrFile) >> d_ptr->tl_bVisible;

  (*istrFile) >> d_ptr->tl_fRotateX;
  (*istrFile) >> d_ptr->tl_fRotateY;
  (*istrFile) >> d_ptr->tl_fStretchX;
  (*istrFile) >> d_ptr->tl_fStretchY;
  (*istrFile) >> d_ptr->tl_fOffsetX;
  (*istrFile) >> d_ptr->tl_fOffsetY;

  (*istrFile) >> d_ptr->tl_bAutoRegenerated;
  (*istrFile) >> d_ptr->tl_fCoverage;
  (*istrFile) >> d_ptr->tl_fCoverageNoise;
  (*istrFile) >> d_ptr->tl_fCoverageRandom;

  (*istrFile) >> d_ptr->tl_bApplyMinAltitude;
  (*istrFile) >> d_ptr->tl_fMinAltitude;
  (*istrFile) >> d_ptr->tl_fMinAltitudeFade;
  (*istrFile) >> d_ptr->tl_fMinAltitudeNoise;
  (*istrFile) >> d_ptr->tl_fMinAltitudeRandom;

  (*istrFile) >> d_ptr->tl_bApplyMaxAltitude;
  (*istrFile) >> d_ptr->tl_fMaxAltitude;
  (*istrFile) >> d_ptr->tl_fMaxAltitudeFade;
  (*istrFile) >> d_ptr->tl_fMaxAltitudeNoise;
  (*istrFile) >> d_ptr->tl_fMaxAltitudeRandom;

  (*istrFile) >> d_ptr->tl_bApplyMinSlope;
  (*istrFile) >> d_ptr->tl_fMinSlope;
  (*istrFile) >> d_ptr->tl_fMinSlopeFade;
  (*istrFile) >> d_ptr->tl_fMinSlopeNoise;
  (*istrFile) >> d_ptr->tl_fMinSlopeRandom;

  (*istrFile) >> d_ptr->tl_bApplyMaxSlope;
  (*istrFile) >> d_ptr->tl_fMaxSlope;
  (*istrFile) >> d_ptr->tl_fMaxSlopeFade;
  (*istrFile) >> d_ptr->tl_fMaxSlopeNoise;
  (*istrFile) >> d_ptr->tl_fMaxSlopeRandom;

  INDEX iType;
  (*istrFile) >> d_ptr->tl_colMultiply;
  (*istrFile) >> d_ptr->tl_fSmoothness;
  (*istrFile) >> iType;
  d_ptr->tl_ltType = (LayerType)iType;

  // Tile layer properties
  (*istrFile) >> d_ptr->tl_ctTilesInRow;
  (*istrFile) >> d_ptr->tl_ctTilesInCol;
  (*istrFile) >> d_ptr->tl_iSelectedTile;
  (*istrFile) >> d_ptr->tl_pixTileWidth;
  (*istrFile) >> d_ptr->tl_pixTileHeight;
  (*istrFile) >> d_ptr->tl_fTileU;
  (*istrFile) >> d_ptr->tl_fTileV;
}

static void WriteLayerVersion12_t(CTerrain *ptr, INDEX iTerrainLayer, CTStream *ostrFile)
{
  ASSERT(ptr != NULL);
  CTerrainLayer *pLayer = &ptr->GetLayer(iTerrainLayer);
  CTerrainLayer *d_ptr = pLayer;

  // Terrain Layer Globals
  ostrFile->WriteID_t("TRLG");
  (*ostrFile) << d_ptr->tl_strName;
  (*ostrFile) << d_ptr->tl_bVisible;
  (*ostrFile) << INDEX(d_ptr->tl_ltType );
  (*ostrFile) << d_ptr->tl_colMultiply;
  (*ostrFile) << d_ptr->tl_ulFlags;

  (*ostrFile) << d_ptr->tl_fOffsetX;
  (*ostrFile) << d_ptr->tl_fOffsetY;
  (*ostrFile) << d_ptr->tl_fRotateX;
  (*ostrFile) << d_ptr->tl_fRotateY;
  (*ostrFile) << d_ptr->tl_fStretchX;
  (*ostrFile) << d_ptr->tl_fStretchY;
  
  (*ostrFile) << d_ptr->tl_bAutoRegenerated;
  (*ostrFile) << d_ptr->tl_fCoverage;
  (*ostrFile) << d_ptr->tl_fCoverageNoise;
  (*ostrFile) << d_ptr->tl_fCoverageRandom;

  (*ostrFile) << d_ptr->tl_bApplyMinAltitude;
  (*ostrFile) << d_ptr->tl_bApplyMaxAltitude;
  (*ostrFile) << d_ptr->tl_fMinAltitude;
  (*ostrFile) << d_ptr->tl_fMaxAltitude;
  (*ostrFile) << d_ptr->tl_fMinAltitudeFade;
  (*ostrFile) << d_ptr->tl_fMaxAltitudeFade;
  (*ostrFile) << d_ptr->tl_fMinAltitudeNoise;
  (*ostrFile) << d_ptr->tl_fMaxAltitudeNoise;
  (*ostrFile) << d_ptr->tl_fMinAltitudeRandom;
  (*ostrFile) << d_ptr->tl_fMaxAltitudeRandom;
  (*ostrFile) << d_ptr->tl_bApplyMinSlope;
  (*ostrFile) << d_ptr->tl_bApplyMaxSlope;
  (*ostrFile) << d_ptr->tl_fMinSlope;
  (*ostrFile) << d_ptr->tl_fMaxSlope;
  (*ostrFile) << d_ptr->tl_fMinSlopeFade;
  (*ostrFile) << d_ptr->tl_fMaxSlopeFade;
  (*ostrFile) << d_ptr->tl_fMinSlopeNoise;
  (*ostrFile) << d_ptr->tl_fMaxSlopeNoise;
  (*ostrFile) << d_ptr->tl_fMinSlopeRandom;
  (*ostrFile) << d_ptr->tl_fMaxSlopeRandom;
  
  // Terrain Layer Mask
  ostrFile->WriteID_t("TRLM");
  {
    const PIX pixMaskWidthTemp = d_ptr->tl_iMaskWidth - 1;
    const PIX pixMaskHeightTemp = d_ptr->tl_iMaskHeight - 1;
    const SLONG slMaskSize = d_ptr->tl_iMaskWidth * d_ptr->tl_iMaskHeight * sizeof(UBYTE);
    const SLONG slMaskSizeTemp = pixMaskWidthTemp * pixMaskHeightTemp * sizeof(UBYTE);
    const UBYTE *pubMask = d_ptr->tl_aubColors;

    // Allocate buffer.
    UBYTE *pubMaskTemp = (UBYTE *)AllocMemory(slMaskSizeTemp);
    memset(pubMaskTemp, 0, slMaskSizeTemp);

    // For each line. Each line in 12 version is 1 pixel shorter.
    for (INDEX iLine = 0; iLine < pixMaskHeightTemp; iLine++)
    {
      const UBYTE *pubLineSrc = &pubMask[iLine * d_ptr->tl_iMaskWidth];
      UBYTE *pubLineDst =  &pubMaskTemp[iLine * pixMaskWidthTemp];
      memcpy(pubLineDst, pubLineSrc, pixMaskWidthTemp);
    }

    ostrFile->Write_t(pubMaskTemp, slMaskSizeTemp);
    FreeMemory(pubMaskTemp);
  }
}

static void WriteLayerVersion9_t(CTerrain *ptr, INDEX iTerrainLayer, CTStream *ostrFile)
{
  ASSERT(ptr != NULL);
  CTerrainLayer *pLayer = &ptr->GetLayer(iTerrainLayer);
  CTerrainLayer *d_ptr = pLayer;

  const SLONG slMaskSize = d_ptr->tl_iMaskWidth * d_ptr->tl_iMaskHeight * sizeof(UBYTE);
  const UBYTE *pubLayerMask = d_ptr->tl_aubColors;
  
  // Terrain Layer Texture
  (*ostrFile).WriteID_t("TLTX");
  const CTFileName &fn = d_ptr->tl_ptdTexture->GetName();
  (*ostrFile) << fn;

  // Terrain Layer Mask
  ostrFile->WriteID_t("TLMA");
  (*ostrFile) << d_ptr->tl_iMaskWidth;
  (*ostrFile) << d_ptr->tl_iMaskHeight;
  ostrFile->Write_t(pubLayerMask, slMaskSize);

  // Write terrain layer params
  ostrFile->WriteID_t("TLPR"); // 'Terrain layer params'
  (*ostrFile) << d_ptr->tl_strName;
  (*ostrFile) << d_ptr->tl_bVisible;
  
  (*ostrFile) << d_ptr->tl_fRotateX;
  (*ostrFile) << d_ptr->tl_fRotateY;
  (*ostrFile) << d_ptr->tl_fStretchX;
  (*ostrFile) << d_ptr->tl_fStretchY;
  (*ostrFile) << d_ptr->tl_fOffsetX;
  (*ostrFile) << d_ptr->tl_fOffsetY;

  (*ostrFile) << d_ptr->tl_bAutoRegenerated;
  (*ostrFile) << d_ptr->tl_fCoverage;
  (*ostrFile) << d_ptr->tl_fCoverageNoise;
  (*ostrFile) << d_ptr->tl_fCoverageRandom;

  (*ostrFile) << d_ptr->tl_bApplyMinAltitude;
  (*ostrFile) << d_ptr->tl_fMinAltitude;
  (*ostrFile) << d_ptr->tl_fMinAltitudeFade;
  (*ostrFile) << d_ptr->tl_fMinAltitudeNoise;
  (*ostrFile) << d_ptr->tl_fMinAltitudeRandom;

  (*ostrFile) << d_ptr->tl_bApplyMaxAltitude;
  (*ostrFile) << d_ptr->tl_fMaxAltitude;
  (*ostrFile) << d_ptr->tl_fMaxAltitudeFade;
  (*ostrFile) << d_ptr->tl_fMaxAltitudeNoise;
  (*ostrFile) << d_ptr->tl_fMaxAltitudeRandom;

  (*ostrFile) << d_ptr->tl_bApplyMinSlope;
  (*ostrFile) << d_ptr->tl_fMinSlope;
  (*ostrFile) << d_ptr->tl_fMinSlopeFade;
  (*ostrFile) << d_ptr->tl_fMinSlopeNoise;
  (*ostrFile) << d_ptr->tl_fMinSlopeRandom;

  (*ostrFile) << d_ptr->tl_bApplyMaxSlope;
  (*ostrFile) << d_ptr->tl_fMaxSlope;
  (*ostrFile) << d_ptr->tl_fMaxSlopeFade;
  (*ostrFile) << d_ptr->tl_fMaxSlopeNoise;
  (*ostrFile) << d_ptr->tl_fMaxSlopeRandom;

  (*ostrFile) << d_ptr->tl_colMultiply;
  (*ostrFile) << d_ptr->tl_fSmoothness;
  (*ostrFile) << (INDEX)d_ptr->tl_ltType;

  // Tile layer properties
  (*ostrFile) << d_ptr->tl_ctTilesInRow;
  (*ostrFile) << d_ptr->tl_ctTilesInCol;
  (*ostrFile) << d_ptr->tl_iSelectedTile;
  (*ostrFile) << d_ptr->tl_pixTileWidth;
  (*ostrFile) << d_ptr->tl_pixTileHeight;
  (*ostrFile) << d_ptr->tl_fTileU;
  (*ostrFile) << d_ptr->tl_fTileV;
}

void CTerrain::ReadLayerOld_t(INDEX iTerrainLayer, CTStream *istrFile)
{
  ReadLayerVersion9_t(this, iTerrainLayer, istrFile);
}

void CTerrain::ReadLayer_t(INDEX iTerrainLayer, CTStream *istrFile)
{
  CTerrainLayer &tl = GetLayers()[iTerrainLayer];
  
  // Terrain layer texture
  CTFileName fnmTexture;
  istrFile->ExpectID_t("TRLT");
  (*istrFile) >> fnmTexture;

  // Add texture to layer
  tl.SetLayerTexture_t(fnmTexture);
  
  // Terrain layer version
  INDEX iLayerVersion;
  istrFile->ExpectID_t("TRLV");
  (*istrFile) >> iLayerVersion;
  
  if (iLayerVersion == 12) {
    ReadLayerVersion12_t(this, iTerrainLayer, istrFile);
  } else {
    ThrowF_t(TRANS("The terrain layer version on disk is %d.\n"
      "Current supported version is %d."), iLayerVersion, 12);
  }
}

void CTerrain::WriteLayerOld_t(INDEX iTerrainLayer, CTStream *ostrFile)
{
  WriteLayerVersion9_t(this, iTerrainLayer, ostrFile);
}

void CTerrain::WriteLayer_t(INDEX iTerrainLayer, CTStream *ostrFile)
{
  CTerrainLayer &tl = GetLayers()[iTerrainLayer];
  
  // Terrain Layer Texture
  ostrFile->WriteID_t("TRLT");
  const CTFileName &fn = tl.tl_ptdTexture->GetName();
  (*ostrFile) << fn;

  // Terrain layer version
  INDEX iLayerVersion = 12;
  ostrFile->WriteID_t("TRLV");
  (*ostrFile) << iLayerVersion;

  WriteLayerVersion12_t(this, iTerrainLayer, ostrFile);
}
