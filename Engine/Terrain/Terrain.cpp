/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/IO/Stream.h>
#include <Core/Base/ListIterator.inl>
#include <Core/Math/Projection.h>
#include <Core/Math/FixInt.h>
#include <Core/Core.h>
#include <Engine/Graphics/Drawport.h>
#include <Engine/Graphics/ImageInfo.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/TerrainRender.h>
#include <Engine/Terrain/TerrainEditing.h>
#include <Engine/Terrain/TerrainMisc.h>
#include <Engine/Resources/ResourceManager.h>
#include <Engine/Entities/Entity.h>
#include <Engine/Entities/ShadingInfo.h>
#include <Engine/Graphics/Font.h>
#include <Core/Base/Console.h>
#include <Engine/Rendering/Render.h>

#include <Engine/Terrain/Terrain_p.h>

CTerrainPrivate::CTerrainPrivate()
{
  tr_vStretch = FLOAT3D(1,0.05f,1);
  tr_fDistFactor = 32;
  tr_iMaxTileLod = 0;
  
  tr_pixHeightMapWidth       = 256;
  tr_pixHeightMapHeight      = 256;
  tr_pixTopMapWidth          = 256;
  tr_pixTopMapHeight         = 256;
  tr_pixFirstMipTopMapWidth  = 256;
  tr_pixFirstMipTopMapHeight = 256;
  tr_ptdDetailMap  = NULL;
  tr_puwHeightMap  = NULL;
  tr_pubEdgeMap    = NULL;
  tr_puwShadingMap = NULL;
  
  // TEMP
  try {
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_TEXTURE, (CTString)"Textures\\Detail\\Crumples04.tex");
    tr_ptdDetailMap = static_cast<CTextureData *>(pser);
  } catch(char *) {}
};

CTerrainPrivate::~CTerrainPrivate()
{
};

extern CTerrain *_ptrTerrain;

static INDEX ctGeneratedTopMaps = 0; // TEMP
static INDEX ctGlobalTopMaps    = 0; // TEMP
INDEX  _ctShadowMapUpdates = 0;      // TEMP
// TEMP

INDEX _ctNodesVis = 0;
INDEX _ctTris = 0;
INDEX _ctDelayedNodes = 0;
static void ShowTerrainInfo(CAnyProjection3D &apr, CDrawPort *pdp, CTerrain *ptrTerrain); // TEMP

// Terrain initialization
CTerrain::CTerrain() : d_ptr(new CTerrainPrivate())
{
  tr_iSelectedLayer = 0;
  tr_ctDriverChanges = -1;

  // Set size of shadow and shading maps
  SetShadowMapsSize(0,0);

  // Set terrain size
  SetTerrainSize(FLOAT3D(256, 80, 256));
  
  // Set num of quads in one tile
  SetQuadsPerTileRow(32);

  SetFlags(TR_REGENERATE);
}

// Render visible terrain tiles
void CTerrain::Render(CAnyProjection3D &apr, CDrawPort *pdp)
{
  // Prepare gfx stuff
  PrepareScene(apr, pdp, this);

  if (tr_ctDriverChanges != _pGfx->gl_ctDriverChanges) {
    tr_ctDriverChanges = _pGfx->gl_ctDriverChanges;
    // RefreshTerrain();
  }

  // If terrain is not frozen
  extern INDEX ter_bNoRegeneration;
  if (GetFlags() & TR_REGENERATE && !ter_bNoRegeneration) {
    // Regenerate tiles
    ReGenerate();
  }
  // If shadow map must be regenerated
  if (GetFlags() & TR_UPDATE_SHADOWMAP) {
    UpdateShadowMap();
  }


  // If top map regen is allowed
  if (GetFlags() & TR_ALLOW_TOP_MAP_REGEN)
  {
    // If top map regen is requested
    if (GetFlags() & TR_REGENERATE_TOP_MAP) {
      // Update terrain top map
      UpdateTopMap(-1);
      
      // Remove request for top map regen
      RemoveFlag(TR_REGENERATE_TOP_MAP);
    }
    
    // Remove flag that allows terrain to regenerate top map
    RemoveFlag(TR_ALLOW_TOP_MAP_REGEN);
  }

  // Show
  RenderTerrain();

  // If flag show brush selection has been set
  if (GetFlags() & TR_SHOW_SELECTION) {
    ShowSelectionInternal(this);
    
    // Remove show selection terrain flag
    RemoveFlag(TR_SHOW_SELECTION);
  }

  // If flag for quadtree rebuilding is set
  if (GetFlags() & TR_REBUILD_QUADTREE) {
    // Resize quadtree
    UpdateQuadTree();
    
    // Remove flag for quadtree rebuilding
    RemoveFlag(TR_REBUILD_QUADTREE);
  }

  RemoveFlag(TR_HAS_FOG);
  RemoveFlag(TR_HAS_HAZE);

  extern INDEX ter_bShowWireframe;
  // If wireframe mode forced
  if (ter_bShowWireframe) {
    COLOR colWire = 0xFFFFFFFF;
    RenderTerrainWire(colWire);
  }

  extern INDEX ter_bShowQuadTree;
  // If showing of quad tree is required
  if (ter_bShowQuadTree) {
    DrawQuadTree();
  }

  extern INDEX ter_bShowInfo;
  if (ter_bShowInfo) {
    ShowTerrainInfo(apr, pdp, this);
  }

  _ptrTerrain = NULL;
}

// TODO: Add comment here
void CTerrain::RenderWireFrame(CAnyProjection3D &apr, CDrawPort *pdp, COLOR &colEdges)
{
  // Prepare gfx stuff
  PrepareScene(apr, pdp, this);
  
  // Regenerate tiles 
  if (d_ptr->tr_ctTiles >= 0)
  {
    CTerrainTile &tt = d_ptr->tr_attTiles[0];
    if (tt.tt_iLod == -1) {
      ReGenerate();
    }
  }
  
  // Show
  RenderTerrainWire(colEdges);
}

// Create empty terrain with given size
void CTerrain::CreateEmptyTerrain_t(PIX pixWidth,PIX pixHeight)
{
  _ptrTerrain = this;
  // Clear old terrain data if exists
  Clear();

  ASSERT(d_ptr->tr_puwHeightMap == NULL);
  ASSERT(d_ptr->tr_pubEdgeMap == NULL);
  
  AllocateHeightMap(pixWidth,pixHeight);

  AddDefaultLayer_t();
  
  // Rebuild terrain
  ReBuildTerrain();
  _ptrTerrain = NULL;
}

// Import height map from targa file
void CTerrain::ImportHeightMap_t(CTFileName fnHeightMap, BOOL bUse16b/*=TRUE*/)
{
  _ptrTerrain = this;
  BOOL bResizeTerrain = FALSE;

  // Load targa file 
  CImageInfo iiHeightMap;
  iiHeightMap.LoadAnyGfxFormat_t(fnHeightMap);

  // If new width and height are same 
  if (d_ptr->tr_pixHeightMapWidth == iiHeightMap.ii_Width && d_ptr->tr_pixHeightMapHeight == iiHeightMap.ii_Height) {
    // Clear terrain data without removing layers
    bResizeTerrain = FALSE;
  } else {
    // Clear all terrain data
    bResizeTerrain = TRUE;
  }
  bResizeTerrain = TRUE;

  FLOAT fLogWidht  = Log2(iiHeightMap.ii_Width  - 1);
  FLOAT fLogHeight = Log2(iiHeightMap.ii_Height - 1);
  if (fLogWidht != INDEX(fLogWidht) || fLogHeight != INDEX(fLogHeight)) {
    ThrowF_t("Invalid terrain width or height");
  }
  if (iiHeightMap.ii_Width != iiHeightMap.ii_Height) {
    ThrowF_t("Only terrains with same width and height are supported in this version");
  }

  // Reallocate memory for terrain with size
  ReAllocateHeightMap(iiHeightMap.ii_Width, iiHeightMap.ii_Height);

  INDEX iHeightMapSize = iiHeightMap.ii_Width * iiHeightMap.ii_Height;

  UBYTE *puwSrc = &iiHeightMap.ii_Picture[0];
  UWORD *puwDst = &d_ptr->tr_puwHeightMap[0];
  INDEX iBpp = iiHeightMap.ii_BitsPerPixel/8;

  // For each word in loaded image
  for (INDEX iw = 0; iw < iHeightMapSize; iw++)
  {
    // Use 16 bits for importing
    if (bUse16b) {
      *puwDst = *(UWORD*)puwSrc;
      
    // Use 8 bits for importing
    } else {
      *puwDst = *(UBYTE*)puwSrc << 8;
    }
    puwDst++;
    puwSrc += iBpp;
  }

  // Rebuild terrain
  ReBuildTerrain();

  _ptrTerrain = NULL;
}  

// Export height map to targa file
void CTerrain::ExportHeightMap_t(CTFileName fnHeightMap, BOOL bUse16b/*=TRUE*/)
{
  ASSERT(d_ptr->tr_puwHeightMap != NULL);
  INDEX iSize = d_ptr->tr_pixHeightMapWidth * d_ptr->tr_pixHeightMapHeight;

  CImageInfo iiHeightMap;
  iiHeightMap.ii_Width  = d_ptr->tr_pixHeightMapWidth;
  iiHeightMap.ii_Height = d_ptr->tr_pixHeightMapHeight;
  iiHeightMap.ii_BitsPerPixel = 32;
  iiHeightMap.ii_Picture = (UBYTE*)AllocMemory(iSize * iiHeightMap.ii_BitsPerPixel / 8);

  GFXColor *pacolImage = (GFXColor*)&iiHeightMap.ii_Picture[0];
  UWORD    *puwHeight  = d_ptr->tr_puwHeightMap;
  for (INDEX ipix = 0; ipix < iSize; ipix++)
  {
    *pacolImage = 0x00000000;
    if (bUse16b) {
      UWORD *puwData = (UWORD*)&pacolImage[0];
      *puwData = *puwHeight;
    } else {
      UBYTE *pubData = (UBYTE*)&pacolImage[0];
      UWORD *puwHData = puwHeight;
      *pubData = (UBYTE)(*puwHData >> 8);
    }
    pacolImage++;
    puwHeight++;
  }
  iiHeightMap.SaveTGA_t(fnHeightMap);
  iiHeightMap.Clear();
}

// Rebuild all terrain
void CTerrain::ReBuildTerrain(BOOL bDelayTileRegen/*=FALSE*/)
{
  _ptrTerrain = this;

  ClearTopMaps();
  ClearTiles();
  ClearArrays();
  ClearQuadTree();

  // Make sure terrain is same size (in metars)
  SetTerrainSize(d_ptr->tr_vTerrainSize);
  
  // Build terrain data
  BuildTerrainData();
  
  // Build terrain quadtree
  BuildQuadTree();
  
  // Generate global top map
  GenerateTerrainTopMap();
  
  // Clear current regen list
  ClearRegenList();
  
  // Add all tiles to reqen queue
  AddAllTilesToRegenQueue();
  
  // if not delaying tile regen
  if (!bDelayTileRegen) {
    // Regenerate tiles now
    ReGenerate();
    
    // Update shadow map
    UpdateShadowMap();
  }
}

// Refresh terrain
void CTerrain::RefreshTerrain(void)
{
  ReBuildTerrain();
}

// Set terrain size
void CTerrain::SetTerrainSize(FLOAT3D vSize)
{
  d_ptr->tr_vStretch(1) = vSize(1) / (d_ptr->tr_pixHeightMapWidth - 1);
  d_ptr->tr_vStretch(2) = vSize(2) / 65535.0f;
  d_ptr->tr_vStretch(3) = vSize(3) / (d_ptr->tr_pixHeightMapHeight - 1);
  
  // Remember new size
  d_ptr->tr_vTerrainSize = vSize;
}

// TODO: Add comment here
template <class Type>
static void CropMap(INDEX iNewWidth, INDEX iNewHeight, INDEX iOldWidth, INDEX iOldHeight, Type *pNewData, Type *pOldData)
{
  INDEX iWidth  = Min(iOldWidth, iNewWidth);
  INDEX iHeight = Min(iOldHeight, iNewHeight);
  INDEX iNewStepX = ClampDn(iNewWidth - iOldWidth, 0L);
  INDEX iOldStepX = ClampDn(iOldWidth - iNewWidth, 0L);

  INDEX iNew = 0;
  INDEX iOld = 0;
  for (INDEX iy = 0; iy < iHeight; iy++)
  {
    for (INDEX ix = 0; ix < iWidth; ix++)
    {
      pNewData[iNew] = pOldData[iOld];
      iNew++;
      iOld++;
    }
    iNew += iNewStepX;
    iOld += iOldStepX;
  }
}

// TODO: Add comment here
template <class Type>
static void StretchMap(INDEX iNewWidth, INDEX iNewHeight, INDEX iOldWidth, INDEX iOldHeight, Type *pNewData, Type *pOldData)
{
  int a = 0;
  CropMap(iNewWidth,iNewHeight,iOldWidth,iOldHeight,pNewData,pOldData);
}

// TODO: Add comment here
template <class Type>
static void ShrinkMap(INDEX iNewWidth, INDEX iNewHeight, INDEX iOldWidth, INDEX iOldHeight, Type *pNewData, Type *pOldData)
{
  FLOAT fWidth  = iNewWidth;
  FLOAT fHeight = iNewHeight;
  FLOAT fDiffX = (FLOAT)iNewWidth  / iOldWidth;
  FLOAT fDiffY = (FLOAT)iNewHeight / iOldHeight;

  ULONG *pulNewData = (ULONG*)AllocMemory(iNewWidth * iNewHeight * sizeof(ULONG));
  memset(pulNewData, 0, iNewWidth * iNewHeight * sizeof(ULONG));

  INDEX iOldPix = 0;
  for (FLOAT fy = 0; fy < iNewHeight; fy += fDiffY)
  {
    for (FLOAT fx = 0; fx < iNewWidth; fx += fDiffX)
    {
      INDEX iNewPix = floor(fx) + floor(fy) * iNewWidth;
      pulNewData[iNewPix] += pOldData[iOldPix];
      iOldPix++;
    }
  }

  ULONG ulDiv = ceil(1.0f / fDiffX) * ceil(1.0f / fDiffY);
  for (INDEX ii = 0;ii < iNewWidth * iNewHeight; ii++)
  {
    pNewData[ii] = pulNewData[ii] / ulDiv;
  }
  FreeMemory(pulNewData);
  ASSERT(_CrtCheckMemory());
}

// TODO: Add comment here
template <class Type>
static void ResizeMap(INDEX iNewWidth, INDEX iNewHeight, INDEX iOldWidth, INDEX iOldHeight, Type *pNewData, Type *pOldData)
{
  CropMap(iNewWidth, iNewHeight, iOldWidth, iOldHeight, pNewData, pOldData);

  // if (iNewWidth >= iOldWidth && iNewHeight >= iOldHeight) {
  //   StretchMap(iNewWidth, iNewHeight, iOldWidth, iOldHeight, pNewData, pOldData);
  // } else if (iNewWidth <= iOldWidth && iNewHeight <= iOldHeight) {
  //   ShrinkMap(iNewWidth, iNewHeight, iOldWidth, iOldHeight, pNewData, pOldData);
  // } else {
  //   INDEX iTempWidth  = Max(iNewWidth,  iOldWidth);
  //   INDEX iTempHeight = Max(iNewHeight, iOldHeight);
  //   INDEX iTempSize   = iTempWidth * iTempHeight * sizeof(Type);
  //   Type *pTempData   = (Type*)AllocMemory(iTempSize);
  //   memset(pTempData,0,iTempSize);
  //   StretchMap(iTempWidth, iTempHeight, iOldWidth, iOldHeight, pTempData, pOldData);
  //   ShrinkMap(iNewWidth, iNewHeight, iTempWidth, iTempHeight, pNewData, pTempData);
  //   FreeMemory(pTempData);
  // }

}

// TODO: Add comment here
void CTerrain::AllocateHeightMap(PIX pixWidth, PIX pixHeight)
{
  ASSERT(d_ptr->tr_puwHeightMap == NULL);
  ASSERT(d_ptr->tr_pubEdgeMap == NULL);

  FLOAT fLogWidht  = Log2(pixWidth  - 1);
  FLOAT fLogHeight = Log2(pixHeight - 1);
  if (fLogWidht != INDEX(fLogWidht) || fLogHeight != INDEX(fLogHeight)) {
    ASSERTALWAYS("Invalid terrain width or height");
    return;
  }
  if (pixWidth != pixHeight) {
    ASSERTALWAYS("Only terrains with same width and height are supported in this version");
    return;
  }

  INDEX iSize = pixWidth * pixHeight * sizeof(UBYTE);

  // Allocate memory for maps
  d_ptr->tr_puwHeightMap  = (UWORD*)AllocMemory(iSize * 2);
  d_ptr->tr_pubEdgeMap    = (UBYTE*)AllocMemory(iSize);
  memset(d_ptr->tr_puwHeightMap, 0, iSize * 2);
  memset(d_ptr->tr_pubEdgeMap, 255, iSize);

  d_ptr->tr_pixHeightMapWidth  = pixWidth;
  d_ptr->tr_pixHeightMapHeight = pixHeight;

  // Update shadow map size cos it depends on size of height map
  SetShadowMapsSize(d_ptr->tr_iShadowMapSizeAspect,d_ptr->tr_iShadingMapSizeAspect);
}

void CTerrain::ReAllocateHeightMap(PIX pixWidth, PIX pixHeight)
{
  ASSERT(d_ptr->tr_puwHeightMap != NULL);
  ASSERT(d_ptr->tr_pubEdgeMap != NULL);

  FLOAT fLogWidht  = Log2(pixWidth  - 1);
  FLOAT fLogHeight = Log2(pixHeight -1);
  if (fLogWidht != INDEX(fLogWidht) || fLogHeight != INDEX(fLogHeight)) {
    ASSERTALWAYS("Invalid terrain width or height");
    return;
  }
  if (pixWidth != pixHeight) {
    ASSERTALWAYS("Only terrains with same width and height are supported in this version");
    return;
  }

  INDEX iSize = pixWidth * pixHeight * sizeof(UBYTE);

  // Allocate memory for maps
  UWORD *auwHeightMap = (UWORD*)AllocMemory(iSize * 2);
  UBYTE *aubEdgeMap   = (UBYTE*)AllocMemory(iSize);

  // Resize height map
  memset(auwHeightMap, 0, iSize * 2);
  ResizeMap(pixWidth, pixHeight, d_ptr->tr_pixHeightMapWidth, d_ptr->tr_pixHeightMapHeight, auwHeightMap, d_ptr->tr_puwHeightMap);

  // Resize edge map
  memset(aubEdgeMap, 255, iSize);
  ResizeMap(pixWidth,pixHeight,d_ptr->tr_pixHeightMapWidth,d_ptr->tr_pixHeightMapHeight,aubEdgeMap,d_ptr->tr_pubEdgeMap);

  // For each layer
  INDEX cttl = d_ptr->tr_atlLayers.Count();
  for (INDEX itl=0; itl < cttl; itl++)
  {
    CTerrainLayer &tl = d_ptr->tr_atlLayers[itl];
    
    // Allocate memory for layer mask
    UBYTE *aubLayerMask = (UBYTE*)AllocMemory(iSize);
    memset(aubLayerMask, 0, iSize);
    ASSERT(tl.tl_iMaskWidth  == d_ptr->tr_pixHeightMapWidth);
    ASSERT(tl.tl_iMaskHeight == d_ptr->tr_pixHeightMapHeight);
    
    //Resize layer
    ResizeMap(pixWidth, pixHeight, tl.tl_iMaskWidth, tl.tl_iMaskHeight, aubLayerMask, tl.tl_aubColors);
    
    // Free old mask
    FreeMemory(tl.tl_aubColors);
    
    // Apply changes
    tl.tl_aubColors = aubLayerMask;
    tl.tl_iMaskWidth  = pixWidth;
    tl.tl_iMaskHeight = pixHeight;
    
    // If this is first layer 
    if (itl == 0) {
      // Fill it
      tl.ResetLayerMask(255);
    }
  }

  // Free old maps
  FreeMemory(d_ptr->tr_puwHeightMap);
  FreeMemory(d_ptr->tr_pubEdgeMap);

  // Apply changes
  d_ptr->tr_puwHeightMap = auwHeightMap;
  d_ptr->tr_pubEdgeMap   = aubEdgeMap;
  d_ptr->tr_pixHeightMapWidth  = pixWidth;
  d_ptr->tr_pixHeightMapHeight = pixHeight;

  ASSERT(_CrtCheckMemory());
  
  // Update shadow map size cos it depends on size of height map
  SetShadowMapsSize(d_ptr->tr_iShadowMapSizeAspect, d_ptr->tr_iShadingMapSizeAspect);

  // // Clear current maps if they exists
  // ClearHeightMap();
  // ClearEdgeMap();
  // ClearLayers();

  
  // ASSERT(d_ptr->tr_puwHeightMap == NULL);
  // ASSERT(d_ptr->tr_pubEdgeMap == NULL);

  // INDEX iSize = sizeof(UBYTE) * pixWidth * pixHeight;
  // // Allocate memory for heightmap
  // d_ptr->tr_puwHeightMap = (UWORD*)AllocMemory(iSize * 2);
  // // Allocate memory for edge map
  // d_ptr->tr_pubEdgeMap = (UBYTE*)AllocMemory(iSize);

  // // Reset height map to 0
  // memset(d_ptr->tr_puwHeightMap, 0, iSize * 2);
  // // Reset edge map to 255
  // memset(d_ptr->tr_pubEdgeMap, 255, iSize);

  // d_ptr->tr_pixHeightMapWidth  = pixWidth;
  // d_ptr->tr_pixHeightMapHeight = pixHeight;
}

// Set shadow map size aspect (relative to height map size) and shading map aspect (relative to shadow map size)
void CTerrain::SetShadowMapsSize(INDEX iShadowMapAspect, INDEX iShadingMapAspect)
{
  // TEMP
  #pragma message(">> Clamp dn SetShadowMapsSize")

  if (iShadingMapAspect < 0) {
    iShadingMapAspect = 0;
  }
  ASSERT(iShadingMapAspect >= 0);


  d_ptr->tr_iShadowMapSizeAspect  = iShadowMapAspect;
  d_ptr->tr_iShadingMapSizeAspect = iShadingMapAspect;

  if (GetShadowMapWidth() < 32 || GetShadingMapHeight() < 32) {
    d_ptr->tr_iShadowMapSizeAspect = -(FastLog2(d_ptr->tr_pixHeightMapWidth - 1) - 5);
  }

  if (GetShadingMapWidth() < 32 || GetShadingMapHeight() < 32) {
    d_ptr->tr_iShadingMapSizeAspect = 0;
  }

  PIX pixShadowMapWidth   = GetShadowMapWidth();
  PIX pixShadowMapHeight  = GetShadowMapHeight();

  PIX pixShadingMapWidth  = GetShadingMapWidth();
  PIX pixShadingMapHeight = GetShadingMapHeight();


  // Clear current shadow map
  ClearShadowMap();
 
  ULONG ulShadowMapFlags = 0;
  // If current app is world editor app
  if (_bWorldEditorApp) {
    // Force texture to be static
    ulShadowMapFlags = TEX_STATIC;
  }

  // Create new shadow map texture
  ASSERT(d_ptr->tr_tdShadowMap.td_pulFrames == NULL);
  CreateTexture(d_ptr->tr_tdShadowMap, pixShadowMapWidth, pixShadowMapHeight, ulShadowMapFlags);
  
  // Reset shadow map texture
  memset(&d_ptr->tr_tdShadowMap.td_pulFrames[0], 0, sizeof(COLOR) * pixShadowMapWidth * pixShadowMapHeight);

  // Create new shading map
  ASSERT(d_ptr->tr_puwShadingMap == NULL);
  d_ptr->tr_puwShadingMap = (UWORD*)AllocMemory(pixShadingMapWidth * pixShadingMapHeight * sizeof(UWORD));
  
  // Reset shading map
  memset(&d_ptr->tr_puwShadingMap[0], 0, pixShadingMapWidth * pixShadingMapHeight * sizeof(UWORD));
}

// Set size of terrain top map texture
void CTerrain::SetGlobalTopMapSize(PIX pixTopMapSize)
{
  FLOAT fLogSize = Log2(pixTopMapSize);
  if (fLogSize != INDEX(fLogSize)) {
    ASSERTALWAYS("Invalid top map size");
    return;
  }
  d_ptr->tr_pixTopMapWidth  = pixTopMapSize;
  d_ptr->tr_pixTopMapHeight = pixTopMapSize;
}

// Set size of top map texture for tiles in lower lods
void CTerrain::SetTileTopMapSize(PIX pixLodTopMapSize)
{
  FLOAT fLogSize = Log2(pixLodTopMapSize);
  if (fLogSize != INDEX(fLogSize)) {
    ASSERTALWAYS("Invalid top map size");
    return;
  }
  d_ptr->tr_pixFirstMipTopMapWidth  = pixLodTopMapSize;
  d_ptr->tr_pixFirstMipTopMapHeight = pixLodTopMapSize;
}

// Set lod distance factor
void CTerrain::SetLodDistanceFactor(FLOAT fLodDistance)
{
  d_ptr->tr_fDistFactor = ClampDn(fLodDistance, 0.1f);
}

// Get shading map width
inline PIX CTerrain::GetShadingMapWidth(void)
{
  ASSERT(d_ptr->tr_iShadingMapSizeAspect >= 0);
  return GetShadowMapWidth() >> d_ptr->tr_iShadingMapSizeAspect;
}

// Get shading map height
inline PIX CTerrain::GetShadingMapHeight(void)
{
  ASSERT(d_ptr->tr_iShadingMapSizeAspect >= 0);
  return GetShadowMapHeight() >> d_ptr->tr_iShadingMapSizeAspect;
}

// Get reference to layer
CTerrainLayer &CTerrain::GetLayer(INDEX iLayer)
{
  INDEX cttl = d_ptr->tr_atlLayers.Count();

  ASSERT(iLayer < cttl);
  ASSERT(iLayer >= 0);
  return d_ptr->tr_atlLayers[iLayer];
}

// Add new layer
CTerrainLayer &CTerrain::AddLayer_t(CTFileName fnTexture, LayerType ltType/*=LT_NORMAL*/, BOOL bUpdateTerrain/*=TRUE*/)
{
  CTerrainLayer &tl = d_ptr->tr_atlLayers.Push();

  // Set layer properties
  tl.tl_ltType = ltType;
  tl.SetLayerSize(d_ptr->tr_pixHeightMapWidth, d_ptr->tr_pixHeightMapHeight);
  tl.SetLayerTexture_t(fnTexture);

  // If update terrain flag has been set
  if (bUpdateTerrain) {
    // Refresh whole terrain
    RefreshTerrain();
  }
  return tl;
}

// Remove one layer
void CTerrain::RemoveLayer(INDEX iLayer, BOOL bUpdateTerrain/*=TRUE*/)
{
  TStaticStackArray<class CTerrainLayer> atlLayers;
  INDEX cttl = d_ptr->tr_atlLayers.Count();

  if (iLayer < 0 || iLayer >= cttl) {
    ASSERTALWAYS("Invalid layer index");
    return;
  }

  if (iLayer == 0 && cttl == 1) {
    ASSERTALWAYS("Can't remove last layer");
    return;
  }

  // For each exisiting layer
  for (INDEX itl = 0; itl < cttl; itl++)
  {
    // If this layer index is not same as index of layer that need to be removed
    if (itl != iLayer) {
      // Add new layer
      CTerrainLayer &tl = atlLayers.Push();
      
      // Copy this layer into new array
      tl = d_ptr->tr_atlLayers[itl];
    }
  }

  ASSERT(atlLayers.Count() == cttl - 1);
  
  // Clear old layers
  d_ptr->tr_atlLayers.Clear();
  
  // Copy new layers insted of old one
  d_ptr->tr_atlLayers = atlLayers;

  // If update terrain flag has been set
  if (bUpdateTerrain) {
    // Refresh whole terrain
    RefreshTerrain();
  }
}

// Move layer to new position
INDEX CTerrain::SetLayerIndex(INDEX iLayer, INDEX iNewIndex, BOOL bUpdateTerrain) // = TRUE
{
  TStaticStackArray<class CTerrainLayer> atlLayers;
  INDEX cttl = d_ptr->tr_atlLayers.Count();

  if (iLayer < 0 || iLayer >= cttl) {
    ASSERTALWAYS("Invalid layer index");
    return iLayer;
  }

  if (iLayer == 0 && cttl == 1) {
    ASSERTALWAYS("Can't move only layer");
    return iLayer;
  }

  if (iLayer == iNewIndex) {
    ASSERTALWAYS("Old layer index is same as new one");
    return iLayer;
  }

  TStaticStackArray<class CTerrainLayer> &atlFrom = d_ptr->tr_atlLayers;
  TStaticStackArray<class CTerrainLayer> &atlTo = atlLayers;

  atlTo.Push(cttl);

  INDEX iOld = iLayer;
  INDEX iNew = iNewIndex;

  for (INDEX iFrom = 0; iFrom < cttl; iFrom++)
  {
    INDEX iTo = -1;
    if (iNew == iOld) {
      iTo = iFrom;
    } 
    // FIXME: Formatting was looked like "else if" was missing.
    if ((iFrom < iOld && iFrom < iNew) || (iFrom > iOld && iFrom > iNew)) {
      iTo = iFrom;
    } else if (iFrom == iOld) {
      iTo = iNew;
    } else {
      if (iNew > iOld) {
        iTo = iFrom - 1;
      } else {
        iTo = iFrom + 1;
      }
    }
    atlTo[iTo] = atlFrom[iFrom];
  }

  ASSERT(atlLayers.Count() == cttl);
  
  // Clear old layers
  d_ptr->tr_atlLayers.Clear();
  
  // Copy new layers insted of old one
  d_ptr->tr_atlLayers = atlLayers;

  // If update terrain flag has been set
  if (bUpdateTerrain) {
    // Refresh whole terrain
    RefreshTerrain();
  }
  return iNewIndex;
}

// Add tile to reqen queue
void CTerrain::AddTileToRegenQueue(INDEX iTileIndex)
{
  INDEX &iRegenIndex = d_ptr->tr_auiRegenList.Push();
  CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];

  iRegenIndex = iTileIndex;
  tt.AddFlag(TT_REGENERATE);
}

// Add all tiles to regen queue
void CTerrain::AddAllTilesToRegenQueue()
{
  // for each terrain tile
  for (INDEX itt = 0; itt < d_ptr->tr_ctTiles; itt++)
  {
    // Add tile to reqen queue
    CTerrainTile &tt = d_ptr->tr_attTiles[itt];
    AddTileToRegenQueue(itt);
  }
}

// Clear current regen list
void CTerrain::ClearRegenList(void)
{
  d_ptr->tr_auiRegenList.PopAll();
}

void CTerrain::UpdateShadowMap(FLOATaabbox3D *pbboxUpdate/*=NULL*/, BOOL bAbsoluteSpace/*=FALSE*/)
{
  // If this is world editor app
  if (!_bWorldEditorApp) {
    // Shadow map can only be updated from world editor
    return;
  }
  
  // If shadow update is allowed
  if (_wrpWorldRenderPrefs.GetShadowsType() == CWorldRenderPrefs::SHT_FULL) {
    // Update terrain shadow map
    UpdateTerrainShadowMap(this, pbboxUpdate, bAbsoluteSpace);
    
    // Don't update shadow map next frame
    RemoveFlag(TR_UPDATE_SHADOWMAP);
  } else {
    // Dont update shadow map but remeber that it's not up to date
    AddFlag(TR_UPDATE_SHADOWMAP);
  }
}

// Temp:   // <-???

// TODO: Add comment here
__forceinline void CopyPixel(COLOR *pubSrc,COLOR *pubDst,FLOAT fMaskStrength)
{
  GFXColor *pcolSrc = (GFXColor*)pubSrc;
  GFXColor *pcolDst = (GFXColor*)pubDst;
  pcolSrc->r = Lerp(pcolSrc->r, pcolDst->r, fMaskStrength);
  pcolSrc->g = Lerp(pcolSrc->g, pcolDst->g, fMaskStrength);
  pcolSrc->b = Lerp(pcolSrc->b, pcolDst->b, fMaskStrength);
  pcolSrc->a = 255;
}

// TODO: Add comment here
static INDEX _ctSavedTopMaps = 0;
static void SaveAsTga(CTextureData *ptdTex)
{
  INDEX iSize = ptdTex->td_mexWidth * ptdTex->td_mexHeight * 4;
  CImageInfo iiHeightMap;
  iiHeightMap.ii_Width  = ptdTex->td_mexWidth;
  iiHeightMap.ii_Height = ptdTex->td_mexHeight;
  iiHeightMap.ii_BitsPerPixel = 32;
  iiHeightMap.ii_Picture = (UBYTE*)AllocMemory(iSize);

  memcpy(&iiHeightMap.ii_Picture[0], &ptdTex->td_pulFrames[0], iSize);

  
  CTString strTopMap = CTString(0, "Temp\\Topmap%d.tga", ++_ctSavedTopMaps);
  iiHeightMap.SaveTGA_t(strTopMap);
  iiHeightMap.Clear();

  // GFXColor *pacolImage = (GFXColor*)&iiHeightMap.ii_Picture[0];
  // UWORD    *puwHeight  = d_ptr->tr_puwHeightMap;
  // for (INDEX ipix=0;ipix<iSize;ipix++) {
    // *pacolImage = 0x00000000;
    // if (bUse16b) {
      // UWORD *puwData = (UWORD*)&pacolImage[0];
      // *puwData = *puwHeight;
    // } else {
      // UBYTE *pubData = (UBYTE*)&pacolImage[0];
      // UWORD *puwHData = puwHeight;
      // *pubData = (UBYTE)(*puwHData>>8);
    // }
    // pacolImage++;
    // puwHeight++;
  // }

}

// TODO: Add comment here
static void AddTileLayerToTopMap(CTerrain *ptrTerrain, INDEX iTileIndex, INDEX iLayer)
{
  CTerrainLayer &tl = ptrTerrain->d_ptr->tr_atlLayers[iLayer];
  CTextureData *ptdSrc = tl.tl_ptdTexture;
  CTextureData *ptdDst;

  INDEX ctQuadsPerTile = ptrTerrain->GetQuadsPerTileRow();
  INDEX iOffsetX = 0;
  INDEX iOffsetZ = 0;

  if (iTileIndex == (-1)) {
    ptdDst = ptrTerrain->GetTopMapTex();
    ctQuadsPerTile = ptrTerrain->GetTilesPerRow() * ptrTerrain->GetQuadsPerTileRow();
  } else {
    CTerrainTile  &tt = ptrTerrain->d_ptr->tr_attTiles[iTileIndex];
    ptdDst = tt.GetTopMap();
    iOffsetX = tt.tt_iOffsetX * ctQuadsPerTile;
    iOffsetZ = tt.tt_iOffsetZ * ctQuadsPerTile;
  }

  ULONG *pulFirstInTopMap = ptdDst->td_pulFrames;
  UBYTE *pubFirstInLayerMask = tl.tl_aubColors;

  // Calculate width and height of quad that will be draw in top map
  PIX pixDstQuadWidth = ptdDst->GetPixWidth() / ctQuadsPerTile;
  PIX pixSrcQuadWidth = tl.tl_pixTileWidth;

  // If dst quad is smaller then one pixel
  if (pixDstQuadWidth==0) {
    return; 
  }

  ASSERT(tl.tl_ctTilesInRow==tl.tl_ctTilesInCol);

  INDEX iSrcMipmap = FastLog2((ptdSrc->GetPixWidth() / tl.tl_ctTilesInRow) / pixDstQuadWidth);
  INDEX iSrcMipMapOffset = GetMipmapOffset(iSrcMipmap,ptdSrc->GetPixWidth(),ptdSrc->GetPixHeight());
  INDEX iSrcMipWidth = ptdSrc->GetPixWidth() >> iSrcMipmap;
  INDEX iSrcMipQuadWidth = pixSrcQuadWidth >> iSrcMipmap;
  INDEX iSrcMipQuadHeight = iSrcMipQuadWidth;

  ASSERT(pixDstQuadWidth==iSrcMipQuadWidth);
   
  ULONG *pulSrcMip = &ptdSrc->td_pulFrames[iSrcMipMapOffset];

  INDEX iMaskIndex = iOffsetX + iOffsetZ*ptrTerrain->GetHeightMapWidth();
  INDEX iMaskStepX = ptrTerrain->GetHeightMapWidth() - ctQuadsPerTile;

  // For each quad in tile
  for (INDEX iQuadY = 0; iQuadY < ctQuadsPerTile; iQuadY++)
  {
    for (INDEX iQuadX = 0; iQuadX < ctQuadsPerTile; iQuadX++)
    {
      UBYTE ubMask = pubFirstInLayerMask[iMaskIndex];
      BOOL  bFlipX   = (ubMask & TL_FLIPX)   >> TL_FLIPX_SHIFT;
      BOOL  bFlipY   = (ubMask & TL_FLIPY)   >> TL_FLIPY_SHIFT;
      BOOL  bSwapXY  = (ubMask & TL_SWAPXY)  >> TL_SWAPXY_SHIFT;
      BOOL  bVisible = (ubMask & TL_VISIBLE) >> TL_VISIBLE_SHIFT;
      INDEX iTile = ubMask&TL_TILE_INDEX;
      INDEX iTileX = iTile % tl.tl_ctTilesInRow;
      INDEX iTileY = iTile / tl.tl_ctTilesInRow;

      // If not visible
      if (!bVisible) {
        iMaskIndex++;
        continue; // skip it
      }

      ASSERT(iTileX < tl.tl_ctTilesInRow);
      ASSERT(iTileY < tl.tl_ctTilesInCol);

      INDEX iFirstDstQuadPixel = (iQuadX * pixDstQuadWidth) + iQuadY * pixDstQuadWidth * ptdDst->GetPixWidth();
      ULONG *pulDstPixel = &pulFirstInTopMap[iFirstDstQuadPixel];
      PIX pixSrc = iTileX * iSrcMipQuadWidth + iTileY * iSrcMipQuadWidth * iSrcMipWidth;
      PIX pixDst = 0;
      PIX pixDstModulo = ptdDst->GetPixWidth() - pixDstQuadWidth;
      PIX pixSrcStepY = iSrcMipWidth;
      PIX pixSrcStepX = 1;

      if (bFlipY) {
        pixSrcStepY = -pixSrcStepY; 
        pixSrc += iSrcMipQuadHeight * iSrcMipWidth;
      }
      
      if (bFlipX) {
        pixSrcStepX = -pixSrcStepX;
        pixSrc += iSrcMipQuadWidth;
      }

      if (bSwapXY) {
        Swap(pixSrcStepX, pixSrcStepY);
      }

      pixSrcStepY -= pixDstQuadWidth*pixSrcStepX;

      // For each pixel in this quad
      for (PIX pixY = 0; pixY < pixDstQuadWidth; pixY++)
      {
        for (PIX pixX = 0; pixX < pixDstQuadWidth; pixX++)
        {
          if ((pulSrcMip[pixSrc] & 0xFF000000) > 0x80000000) {
            pulDstPixel[pixDst] = pulSrcMip[pixSrc];
          }
          pixSrc += pixSrcStepX;
          pixDst++;
        }
        pixDst += pixDstModulo;
        pixSrc += pixSrcStepY;
      }
      iMaskIndex++;
    }
    iMaskIndex += iMaskStepX;
  }
}

// TODO: Add comment here
void CTerrain::UpdateTopMap(INDEX iTileIndex, Rect *prcDest/*=NULL*/)
{
  //ReGenerateTopMap(this, iTileIndex);
  
  if (iTileIndex == (-1)) {
    ctGlobalTopMaps++;
  } else {
    ctGeneratedTopMaps++;
  }

  FIX16_16 fiMaskDiv = 1;
  INDEX iFirstInMask = 0;
  INDEX iMaskWidth = d_ptr->tr_pixHeightMapWidth;
  INDEX iTiling = 1;
  INDEX iSrcMipWidth = 1;
  

  // Destionation texture (must have set allocated memory)
  CTextureData *ptdDest;

  // If global top map
  if (iTileIndex == (-1)) {
    ptdDest = &d_ptr->tr_tdTopMap;
    
  // else tile top map
  } else {
    CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
    ptdDest = tt.GetTopMap();
    fiMaskDiv = d_ptr->tr_ctTilesX;
    iFirstInMask = iMaskWidth * tt.tt_iOffsetZ * (d_ptr->tr_ctVerticesInTileRow - 1) +
                   (tt.tt_iOffsetX * (d_ptr->tr_ctVerticesInTileRow - 1));
  }

  ASSERT(ptdDest->td_pulFrames == NULL);
  PrepareSharedTopMapMemory(ptdDest, iTileIndex);

  ASSERT(ptdDest != NULL);
  ASSERT(ptdDest->td_pulFrames != NULL);
 
  // ASSERT(ptdDest->GetPixWidth()>0 && ptdDest->GetPixHeight()>0 && ptdDest->GetPixWidth()==ptdDest->GetPixHeight());
  // iTiling = ClampDn(iTiling,(INDEX)1);
  // INDEX iSrcMipWidth = ClampDn(ptdDest->GetWidth()/iTiling,(INDEX)1);


  INDEX ctLayers = d_ptr->tr_atlLayers.Count();
  // For each layer
  for (INDEX itl = 0; itl < ctLayers; itl++)
  {
    CTerrainLayer &tl = d_ptr->tr_atlLayers[itl];
    // If layer isn't visible
    if (!tl.tl_bVisible) {
      continue; // skip it
    }
    
    if (tl.tl_ltType == LT_TILE) {
      AddTileLayerToTopMap(this,iTileIndex,itl);
      continue;
    }

    if (iTileIndex == (-1)) {
      // ptdDest = &d_ptr->tr_tdTopMap;
      iTiling = (INDEX)(d_ptr->tr_ctTilesX * d_ptr->tr_ctQuadsInTileRow * tl.tl_fStretchX);

    // else tile top map
    } else {
      CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
      // ptdDest = tt.GetTopMap();
      fiMaskDiv = d_ptr->tr_ctTilesX;
      iFirstInMask = iMaskWidth * tt.tt_iOffsetZ * (d_ptr->tr_ctVerticesInTileRow - 1) +
                     (tt.tt_iOffsetX * (d_ptr->tr_ctVerticesInTileRow - 1));
      iTiling = (INDEX)(d_ptr->tr_ctQuadsInTileRow * tl.tl_fStretchX);
    }

    ASSERT(ptdDest->GetPixWidth() > 0 && ptdDest->GetPixHeight() > 0 && ptdDest->GetPixWidth() == ptdDest->GetPixHeight());
    ASSERT(iTiling >= 1);
    iTiling = ClampDn(iTiling, (INDEX)1);

    // get source texture
    CTextureData *ptdSrc = tl.tl_ptdTexture;
    INDEX iSrcMipWidth  = ClampDn(ptdSrc->GetPixWidth()  / iTiling, 1L);
    INDEX iSrcMipHeight = ClampDn(ptdSrc->GetPixHeight() / iTiling, 1L);

    // Get mipmap of source texture
    INDEX immW = FastLog2(ptdSrc->GetPixWidth()  / iSrcMipWidth);
    INDEX immH = FastLog2(ptdSrc->GetPixHeight() / iSrcMipHeight);
    
    // Get address of first byte in source mipmap
    INDEX imm = Max(immW, immH);
    INDEX iMipAdr = GetMipmapOffset(imm, ptdSrc->GetPixWidth(), ptdSrc->GetPixHeight());
 
    // Mask thing
    // Get first byte in layer mask
    UBYTE *ubFirstInMask = &tl.tl_aubColors[iFirstInMask];
    
    // Get first byte in edge map
    UBYTE *ubFirstInEdgeMap = &d_ptr->tr_pubEdgeMap[iFirstInMask];
    FIX16_16 fiHMaskStep = FIX16_16(iMaskWidth - 1) / FIX16_16(ptdDest->GetWidth() - 1) / fiMaskDiv;
    FIX16_16 fiVMaskStep = FIX16_16(iMaskWidth - 1) / FIX16_16(ptdDest->GetWidth() - 1) / fiMaskDiv;

    SLONG xHMaskStep = fiHMaskStep.slHolder;
    SLONG xVMaskStep = fiVMaskStep.slHolder;
    SLONG xMaskVPos = 0;
    
    // Get first byte in destination texture
    ULONG *pulTexDst = (ULONG*)&ptdDest->td_pulFrames[0];
    
    // Get first byte in source texture
    ULONG *pulFirstInMipSrc = (ULONG*)&ptdSrc->td_pulFrames[iMipAdr];
  
    // For each row
    for (UINT ir = 0; ir < ptdDest->GetPixHeight(); ir++)
    {
      // Get first byte for src mip texture in this row
      ULONG *pulSrcRow = &pulFirstInMipSrc[(ir&(iSrcMipWidth - 1)) * iSrcMipWidth];//%
      INDEX iMaskVPos = (INDEX)(xMaskVPos >> 16) * (iMaskWidth);
      UBYTE *pubMaskRow = &ubFirstInMask[iMaskVPos];
      UBYTE *pubEdgeMaskRow = &ubFirstInEdgeMap[iMaskVPos];
      SLONG xMaskHPos = 0;
      
      // For each column
      for (UINT ic = 0; ic < ptdDest->GetPixWidth(); ic++)
      {
        ULONG *ulSrc = &pulSrcRow[ic & (iSrcMipWidth - 1)];
        INDEX iMask = (INDEX)(xMaskHPos >> 16);

        SLONG x1 = (SLONG)(pubMaskRow[iMask + 0]) << 0; //NormByteToFixInt(pubMaskRow[iMask]);
        SLONG x2 = (SLONG)(pubMaskRow[iMask + 1]) << 0; //NormByteToFixInt(pubMaskRow[iMask+1]);
        SLONG x3 = (SLONG)(pubMaskRow[iMask+iMaskWidth + 0]) << 0;//NormByteToFixInt(pubMaskRow[iMask+iMaskWidth+0]);
        SLONG x4 = (SLONG)(pubMaskRow[iMask+iMaskWidth + 1]) << 0;//NormByteToFixInt(pubMaskRow[iMask+iMaskWidth+1]);
        SLONG xFactH = xMaskHPos - (xMaskHPos & 0xFFFF0000);
        SLONG xFactV = xMaskVPos - (xMaskVPos & 0xFFFF0000);
        
        SLONG xStrengthX1 = (x1 << 7) + (SLONG)(((x2 - x1) * xFactH) >> 9); //Lerp(fi1,fi2,fiFactH);
        SLONG xStrengthX2 = (x3 << 7) + (SLONG)(((x4 - x3) * xFactH) >> 9); //Lerp(fi3,fi4,fiFactH);
        SLONG xStrength   = (xStrengthX1 << 1) + (SLONG)((((xStrengthX2 >> 0) - (xStrengthX1 >> 0)) * xFactV) >> 15);   //Lerp(fiStrengthX1,fiStrengthX2,fiFactV);
        
        GFXColor *pcolSrc = (GFXColor*)pulTexDst;
        GFXColor *pcolDst = (GFXColor*)ulSrc;
        pcolSrc->r = (BYTE)((ULONG)pcolSrc->r + ((((ULONG)pcolDst->r - (ULONG)pcolSrc->r) * xStrength) >> 16));
        pcolSrc->g = (BYTE)((ULONG)pcolSrc->g + ((((ULONG)pcolDst->g - (ULONG)pcolSrc->g) * xStrength) >> 16));
        pcolSrc->b = (BYTE)((ULONG)pcolSrc->b + ((((ULONG)pcolDst->b - (ULONG)pcolSrc->b) * xStrength) >> 16));
        pcolSrc->a = pubEdgeMaskRow[iMask];
        
        pulTexDst++;
        xMaskHPos += xHMaskStep;
      }
      xMaskVPos += xVMaskStep;
    }
  }
  
  // Make mipmaps
  INDEX ctMipMaps = GetNoOfMipmaps(ptdDest->GetPixWidth(), ptdDest->GetPixHeight());
  MakeMipmaps(ctMipMaps, ptdDest->td_pulFrames, ptdDest->GetPixWidth(), ptdDest->GetPixHeight());

  #pragma message(">> Fix DitherMipmaps")
  INDEX iDithering = 4;
  DitherMipmaps(iDithering, ptdDest->td_pulFrames, ptdDest->td_pulFrames, ptdDest->GetPixWidth(), ptdDest->GetPixHeight());
  
  // Force topmap upload
  ptdDest->SetAsCurrent(0,TRUE);

  // Free shared memory
  FreeSharedTopMapMemory(ptdDest, iTileIndex);
}

// TODO: Add comment here
void CTerrain::GetAllTerrainBBox(FLOATaabbox3D &bbox)
{
  // Get last quad tree level
  INDEX ctqtl = d_ptr->tr_aqtlQuadTreeLevels.Count();
  QuadTreeLevel &qtl = d_ptr->tr_aqtlQuadTreeLevels[ctqtl - 1];

  ASSERT(qtl.qtl_ctNodes == 1);
  
  // Get quad tree node for last level
  QuadTreeNode  &qtn = d_ptr->tr_aqtnQuadTreeNodes[qtl.qtl_iFirstNode];
  bbox = qtn.qtn_aabbox;
}

// Get shading color from tex coords in shading map
COLOR CTerrain::GetShadeColor(CShadingInfo *psi)
{
  ASSERT(psi != NULL);
  ASSERT(d_ptr->tr_puwShadingMap != NULL);

  PIX pixShadowU = Clamp(psi->si_pixShadowU, 0L, GetShadingMapWidth()  - 2L);
  PIX pixShadowV = Clamp(psi->si_pixShadowV, 0L, GetShadingMapHeight() - 2L);
  FLOAT fUDRatio = psi->si_fUDRatio;
  FLOAT fLRRatio = psi->si_fLRRatio;

  PIX pixWidth  = GetShadingMapWidth();
  PIX pixShadow = pixShadowU + pixShadowV * pixWidth;
  UWORD auwShade[4];
  SLONG aslr[4], aslg[4], aslb[4];

  auwShade[0] = d_ptr->tr_puwShadingMap[pixShadow];
  auwShade[1] = d_ptr->tr_puwShadingMap[pixShadow + 1];
  auwShade[2] = d_ptr->tr_puwShadingMap[pixShadow + pixWidth];
  auwShade[3] = d_ptr->tr_puwShadingMap[pixShadow + pixWidth + 1];

  for (INDEX ish = 0; ish < 4; ish++)
  {
    aslr[ish] = (auwShade[ish] & 0x7C00) >> 10;
    aslg[ish] = (auwShade[ish] & 0x03E0) >>  5;
    aslb[ish] = (auwShade[ish] & 0x001F) >>  0;

    aslr[ish] = (aslr[ish] << 3) | (aslr[ish] >> 2);
    aslg[ish] = (aslg[ish] << 3) | (aslg[ish] >> 2);
    aslb[ish] = (aslb[ish] << 3) | (aslb[ish] >> 2);
  }

  SLONG slRed   = Lerp(Lerp(aslr[0], aslr[1], fLRRatio), Lerp(aslr[2], aslr[3], fLRRatio), fUDRatio);
  SLONG slGreen = Lerp(Lerp(aslg[0], aslg[1], fLRRatio), Lerp(aslg[2], aslg[3], fLRRatio), fUDRatio);
  SLONG slBlue  = Lerp(Lerp(aslb[0], aslb[1], fLRRatio), Lerp(aslb[2], aslb[3], fLRRatio), fUDRatio);

  ULONG ulPixel = ((slRed   << 24) & 0xFF000000) |
                  ((slGreen << 16) & 0x00FF0000) |
                  ((slBlue  <<  8) & 0x0000FF00) | 0xFF;

  return ulPixel;
}

// Get plane from given point
FLOATplane3D CTerrain::GetPlaneFromPoint(FLOAT3D &vAbsPoint)
{
  ASSERT(tr_penEntity != NULL);

  const VEC3F &vEntityPos = tr_penEntity->en_plPlacement.pl_PositionVector;
  const VEC3F &vStretch = d_ptr->tr_vStretch;
  const UWORD *puwHeightMap = GetHeightMap();

  FLOAT3D vRelPoint = (vAbsPoint - vEntityPos) * !tr_penEntity->en_mRotation;
  vRelPoint(1) /= vStretch(1);
  vRelPoint(3) /= vStretch(3);
  PIX pixX = floor(vRelPoint(1));
  PIX pixZ = floor(vRelPoint(3));
  PIX pixWidth = d_ptr->tr_pixHeightMapWidth;
  FLOAT fXRatio = vRelPoint(1) - pixX;
  FLOAT fZRatio = vRelPoint(3) - pixZ;

  INDEX iPix = pixX + pixZ * pixWidth;
  BOOL bFacing = (iPix) & 1;

  FLOAT3D vx0 = FLOAT3D((pixX + 0) * vStretch(1), puwHeightMap[iPix]                * vStretch(2), (pixZ + 0) * vStretch(3));
  FLOAT3D vx1 = FLOAT3D((pixX + 1) * vStretch(1), puwHeightMap[iPix + 1]            * vStretch(2), (pixZ + 0) * vStretch(3));
  FLOAT3D vx2 = FLOAT3D((pixX + 0) * vStretch(1), puwHeightMap[iPix + pixWidth]     * vStretch(2), (pixZ + 1) * vStretch(3));
  FLOAT3D vx3 = FLOAT3D((pixX + 1) * vStretch(1), puwHeightMap[iPix + pixWidth + 1] * vStretch(2), (pixZ + 1) * vStretch(3));

  vx0 = vx0 * tr_penEntity->en_mRotation + vEntityPos;
  vx1 = vx1 * tr_penEntity->en_mRotation + vEntityPos;
  vx2 = vx2 * tr_penEntity->en_mRotation + vEntityPos;
  vx3 = vx3 * tr_penEntity->en_mRotation + vEntityPos;

  if (bFacing)
  {
    if (fXRatio >= fZRatio) {
      return FLOATplane3D(vx0, vx2, vx1);
    } else {
      return FLOATplane3D(vx1, vx2, vx3);
    }
  } else {
    if (fXRatio >= fZRatio) {
      return FLOATplane3D(vx2, vx3, vx0);
    } else {
      return FLOATplane3D(vx0, vx3, vx1);
    }
  }
}

// Sets number of quads in row of one tile
void CTerrain::SetQuadsPerTileRow(INDEX ctQuadsPerTileRow)
{
  d_ptr->tr_ctQuadsInTileRow = Clamp(ctQuadsPerTileRow, (INDEX)4, (INDEX)(d_ptr->tr_pixHeightMapWidth - 1));
  if (d_ptr->tr_ctQuadsInTileRow != ctQuadsPerTileRow) {
    CWarningF("Warning: Quads per tile has been changed from requested %d to %d\n", ctQuadsPerTileRow, d_ptr->tr_ctQuadsInTileRow);
  }
  // TODO: Assert that it is 2^n
  d_ptr->tr_ctVerticesInTileRow = d_ptr->tr_ctQuadsInTileRow + 1;
}

// Set Terrain stretch
void CTerrain::SetTerrainStretch(FLOAT3D vStretch)
{
  d_ptr->tr_vStretch = vStretch;
}

// Build terrain data
void CTerrain::BuildTerrainData()
{
  // Allocate space for terrain tiles
  d_ptr->tr_ctTilesX = (d_ptr->tr_pixHeightMapWidth  - 1) / d_ptr->tr_ctQuadsInTileRow;
  d_ptr->tr_ctTilesY = (d_ptr->tr_pixHeightMapHeight -1)  / d_ptr->tr_ctQuadsInTileRow;
  d_ptr->tr_ctTiles  = d_ptr->tr_ctTilesX * d_ptr->tr_ctTilesY;
  d_ptr->tr_attTiles.New(d_ptr->tr_ctTiles);

  // Calculate max posible lod
  INDEX ctVtxInLod = d_ptr->tr_ctQuadsInTileRow;
  d_ptr->tr_iMaxTileLod = 0;
  while (ctVtxInLod > 2)
  {
    d_ptr->tr_iMaxTileLod++;
    ctVtxInLod = ctVtxInLod >> 1;
  }

  // Allocate memory for terrain tile arrays
  d_ptr->tr_aArrayHolders.New(d_ptr->tr_iMaxTileLod + 1);
  INDEX ctah  = d_ptr->tr_aArrayHolders.Count();
  
  // For each array handler
  for (INDEX iah = 0; iah < ctah; iah++)
  {
    CArrayHolder &ah = d_ptr->tr_aArrayHolders[iah];
    // Set its lod index
    ah.ah_iLod = iah;
    ah.ah_ptrTerrain = this;
  }

  // For each tile row
  for (INDEX iy = 0; iy < d_ptr->tr_ctTilesY; iy++)
  {
    // For each tile col
    for (INDEX ix = 0; ix < d_ptr->tr_ctTilesX; ix++)
    {
      // Initialize terrain tile
      UINT iTileIndex = ix + iy * d_ptr->tr_ctTilesX;
      CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
      tt.tt_iIndex = iTileIndex;
      tt.tt_iOffsetX = ix;
      tt.tt_iOffsetZ = iy;
      tt.tt_ctVtxX   = d_ptr->tr_ctVerticesInTileRow;
      tt.tt_ctVtxY   = d_ptr->tr_ctVerticesInTileRow;
      tt.tt_ctLodVtxX = tt.tt_ctVtxX;
      tt.tt_ctLodVtxY = tt.tt_ctVtxY;
      tt.tt_iLod = 0;
      tt.tt_fLodLerpFactor = 0;
      
      // Reset tile neighbours
      tt.tt_aiNeighbours[NB_TOP]    = -1;
      tt.tt_aiNeighbours[NB_LEFT]   = -1;
      tt.tt_aiNeighbours[NB_BOTTOM] = -1;
      tt.tt_aiNeighbours[NB_RIGHT]  = -1;
      
      // Set tile neighbours
      if (iy > 0) { 
       tt.tt_aiNeighbours[NB_TOP] = iTileIndex - d_ptr->tr_ctTilesX;
      }
      
      if (ix > 0) { 
       tt.tt_aiNeighbours[NB_LEFT] = iTileIndex - 1;
      }
      
      if (iy < d_ptr->tr_ctTilesX - 1) { 
       tt.tt_aiNeighbours[NB_BOTTOM] = iTileIndex + d_ptr->tr_ctTilesX;
      }
      
      if (ix < d_ptr->tr_ctTilesY - 1) { 
       tt.tt_aiNeighbours[NB_RIGHT]  = iTileIndex + 1;
      }
    }
  }
}

// FIXME: Is it needed?
/*
#if 1
void CTerrain::GenerateTopMap(INDEX iTileIndex)
{
  FIX16_16 fiMaskDiv = 1;
  INDEX iFirstInMask = 0;
  INDEX iMaskWidth = d_ptr->tr_pixHeightMapWidth;
  INDEX iTiling = (INDEX)(d_ptr->tr_ctTilesX * d_ptr->tr_ctQuadsInTileRow * tr_fTexStretch);

  // Destionation texture (must have set allocated memory)
  CTextureData *ptdDest;
  
  // If global top map
  if (iTileIndex == (-1)) {
    ptdDest = &d_ptr->tr_tdTopMap;
    
  // else tile top map
  } else {
    CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
    ptdDest = tt.GetTopMap();
    fiMaskDiv = d_ptr->tr_ctTilesX;
    iFirstInMask = iMaskWidth * tt.tt_iOffsetZ * (d_ptr->tr_ctVerticesInTileRow - 1) + (tt.tt_iOffsetX * (d_ptr->tr_ctVerticesInTileRow - 1));
    iTiling = (INDEX)(d_ptr->tr_ctQuadsInTileRow * tr_fTexStretch);
  }
 
  ASSERT(ptdDest->GetPixWidth() > 0 && ptdDest->GetPixHeight() > 0 && ptdDest->GetPixWidth() == ptdDest->GetPixHeight());
  INDEX iSrcMipWidth = ClampDn(ptdDest->GetWidth() / iTiling, (INDEX)1);


  INDEX ctLayers = d_ptr->tr_atlLayers.Count();
  
  // For each layer
  for (INDEX itl = 0; itl < ctLayers; itl++)
  {
    CTerrainLayer &tl = d_ptr->tr_atlLayers[itl];
    
    // Get source texture
    CTextureData *ptdSrc = tl.tl_ptdTexture;
    
    // Get mipmap of source texture
    INDEX imm = FastLog2(ptdSrc->GetPixWidth() / iSrcMipWidth);
    
    // Get address of first byte in source mipmap
    INDEX iMipAdr = GetMipmapOffset(imm, ptdSrc->GetPixWidth(), ptdSrc->GetPixHeight());
 
    // Mask thing
    // Get first byte in layer mask
    UBYTE *ubFirstInMask = &tl.tl_aubColors[iFirstInMask];
    FIX16_16 fiHMaskStep = FIX16_16(iMaskWidth - 1) / FIX16_16(ptdDest->GetWidth() - 1) / fiMaskDiv;
    FIX16_16 fiVMaskStep = FIX16_16(iMaskWidth - 1) / FIX16_16(ptdDest->GetWidth() - 1) / fiMaskDiv;

    SLONG xHMaskStep = fiHMaskStep.slHolder;
    SLONG xVMaskStep = fiVMaskStep.slHolder;
    SLONG xMaskVPos=0;
    
    // Get first byte in destination texture
    ULONG *pulTexDst = (ULONG*)&ptdDest->td_pulFrames[0];
    
    // Get first byte in source texture
    ULONG *pulFirstInMipSrc = (ULONG*)&ptdSrc->td_pulFrames[iMipAdr];
  
    // For each row
    for (UINT ir = 0; ir < ptdDest->GetHeight(); ir++)
    {
      // Get first byte for src mip texture in this row
      ULONG *pulSrcRow = &pulFirstInMipSrc[(ir & (iSrcMipWidth - 1)) * iSrcMipWidth];//%
      INDEX iMaskVPos = (INDEX)(xMaskVPos >> 16) * (iMaskWidth);
      UBYTE *pubMaskRow = &ubFirstInMask[iMaskVPos];
      SLONG xMaskHPos = 0;
      
      // For each column
      for (UINT ic = 0; ic < ptdDest->GetWidth(); ic++)
      {
        ULONG *ulSrc = &pulSrcRow[ic&(iSrcMipWidth - 1)];
        INDEX iMask = (INDEX)(xMaskHPos >> 16);

        SLONG x1 = (SLONG)(pubMaskRow[iMask + 0]) << 0; //NormByteToFixInt(pubMaskRow[iMask]);
        SLONG x2 = (SLONG)(pubMaskRow[iMask + 1]) << 0; //NormByteToFixInt(pubMaskRow[iMask+1]);
        SLONG x3 = (SLONG)(pubMaskRow[iMask + iMaskWidth + 0]) << 0;//NormByteToFixInt(pubMaskRow[iMask+iMaskWidth+0]);
        SLONG x4 = (SLONG)(pubMaskRow[iMask + iMaskWidth + 1]) << 0;//NormByteToFixInt(pubMaskRow[iMask+iMaskWidth+1]);
        SLONG xFactH = xMaskHPos - (xMaskHPos & 0xFFFF0000);
        SLONG xFactV = xMaskVPos - (xMaskVPos & 0xFFFF0000);
        
        SLONG xStrengthX1 = (x1 << 7) + (SLONG)(((x2 - x1) * xFactH) >> 9); //Lerp(fi1,fi2,fiFactH);
        SLONG xStrengthX2 = (x3 << 7) + (SLONG)(((x4 - x3) * xFactH) >> 9); //Lerp(fi3,fi4,fiFactH);
        SLONG xStrength   = (xStrengthX1 << 1) + (SLONG)((((xStrengthX2 >> 0) - (xStrengthX1 >> 0)) * xFactV) >> 15);   //Lerp(fiStrengthX1,fiStrengthX2,fiFactV);
        
        GFXColor *pcolSrc = (GFXColor*)pulTexDst;
        GFXColor *pcolDst = (GFXColor*)ulSrc;
        pcolSrc->r = (BYTE)((ULONG)pcolSrc->r + ((((ULONG)pcolDst->r - (ULONG)pcolSrc->r) * xStrength) >> 16));
        pcolSrc->g = (BYTE)((ULONG)pcolSrc->g + ((((ULONG)pcolDst->g - (ULONG)pcolSrc->g) * xStrength) >> 16));
        pcolSrc->b = (BYTE)((ULONG)pcolSrc->b + ((((ULONG)pcolDst->b - (ULONG)pcolSrc->b) * xStrength) >> 16));
        pcolSrc->a = 255;
        
        pulTexDst++;
        xMaskHPos += xHMaskStep;
      }
      xMaskVPos += xVMaskStep;
    }
  }
  
  // Make mipmaps
  MakeMipmaps(32, ptdDest->td_pulFrames, ptdDest->GetWidth(), ptdDest->GetHeight());
  
  // Force topmap upload
  ptdDest->SetAsCurrent(0,TRUE);
}
#else
void CTerrain::GenerateTopMap(INDEX iTileIndex)
{
  INDEX iMaskDiv = 1;
  INDEX iFirstInMask = 0;
  INDEX iMaskWidth = d_ptr->tr_pixHeightMapWidth;
  INDEX iTiling = (INDEX)(d_ptr->tr_ctTilesX * d_ptr->tr_ctQuadsInTileRow * tr_fTexStretch);

  // Destionation texture (must have set required width and height)
  CTextureData *ptdDest;
  // If global top map
  if (iTileIndex == (-1)) {
    ptdDest = &d_ptr->tr_tdTopMap;
  // else tile top map
  } else {
    CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
    ptdDest = &tt.GetTopMap();
    iMaskDiv = d_ptr->tr_ctTilesX;
    iFirstInMask = iMaskWidth * tt.tt_iOffsetZ * (d_ptr->tr_ctVerticesInTileRow - 1) + (tt.tt_iOffsetX * (d_ptr->tr_ctVerticesInTileRow - 1));
    iTiling = (INDEX)(d_ptr->tr_ctQuadsInTileRow * tr_fTexStretch);
  }
 
  ASSERT(ptdDest->GetPixWidth() > 0 && ptdDest->GetPixHeight()>0 && ptdDest->GetPixWidth() == ptdDest->GetPixHeight());

  INDEX iSrcMipWidth = ptdDest->GetWidth() / iTiling;

  INDEX ctLayers = d_ptr->tr_atlLayers.Count();
  // For each layer
  for (INDEX ilr = 0; ilr < ctLayers; ilr++)
  {
    CTerrainLayer &tl = d_ptr->tr_atlLayers[ilr];
    
    // Get source texture
    CTextureData *ptdSrc = tl.tl_ptdTexture;
    
    // Get mipmap of source texture
    INDEX imm = FastLog2(ptdSrc->GetPixWidth() / iSrcMipWidth);
    
    // Get address of first byte in source mipmap
    INDEX iMipAdr = GetMipmapOffset(imm, ptdSrc->GetPixWidth(), ptdSrc->GetPixHeight());
 
    // Mask thing
    // Get first byte in layer mask
    UBYTE *ubFirstInMask = &tl.tl_aubColors[iFirstInMask];
    FLOAT fHMaskStep = FLOAT(iMaskWidth - 1) / (ptdDest->GetWidth() - 1) / iMaskDiv;
    FLOAT fVMaskStep = FLOAT(iMaskWidth - 1) / (ptdDest->GetWidth() - 1) / iMaskDiv;
    FLOAT fMaskVPos = 0;
    
    // Get first byte in destination texture
    ULONG *pulTexDst = (ULONG*)&ptdDest->td_pulFrames[0];
    
    // Get first byte in source texture
    ULONG *pulFirstInMipSrc = &ptdSrc->td_pulFrames[iMipAdr];
  
    // For each row
    for (UINT ir = 0; ir < ptdDest->GetWidth(); ir++)
    {
      // Get first byte for src mip texture in this row
      ULONG *pulSrcRow = &pulFirstInMipSrc[(ir & (iSrcMipWidth - 1)) * iSrcMipWidth];//%
      INDEX iMaskVPos = (INDEX)fMaskVPos * (iMaskWidth);
      UBYTE *pubMaskRow = &ubFirstInMask[iMaskVPos];
      FLOAT fMaskHPos = 0;
      
      // For each column
      for (UINT ic = 0; ic < ptdDest->GetWidth(); ic++)
      {
        ULONG *ulSrc = &pulSrcRow[ic & (iSrcMipWidth - 1)];
        INDEX iMask = (INDEX)fMaskHPos;
        FLOAT f1 = NormByteToFloat(pubMaskRow[iMask]);
        FLOAT f2 = NormByteToFloat(pubMaskRow[iMask + 1]);
        FLOAT f3 = NormByteToFloat(pubMaskRow[iMask + iMaskWidth + 0]);
        FLOAT f4 = NormByteToFloat(pubMaskRow[iMask + iMaskWidth + 1]);
        FLOAT fStrengthX1 = Lerp(f1, f2, fMaskHPos - (INDEX)fMaskHPos);
        FLOAT fStrengthX2 = Lerp(f3, f4, fMaskHPos - (INDEX)fMaskHPos);
        FLOAT fStrength   = Lerp(fStrengthX1, fStrengthX2, fMaskVPos - (INDEX)fMaskVPos);

        CopyPixel(pulTexDst, ulSrc, fStrength);
        pulTexDst++;
        fMaskHPos += fHMaskStep;
      }
      fMaskVPos+=fVMaskStep;
    }
  }
  
  // Make mipmaps
  MakeMipmaps(32, ptdDest->td_pulFrames, ptdDest->GetWidth(), ptdDest->GetHeight());
  
  // Force topmap upload
  ptdDest->SetAsCurrent(0,TRUE);
}
#endif
*/

// TODO: Add comment here
void CTerrain::GenerateTerrainTopMap()
{
  CreateTopMap(d_ptr->tr_tdTopMap, d_ptr->tr_pixTopMapWidth, d_ptr->tr_pixTopMapHeight);
  UpdateTopMap(-1);
}

// Add default layer
void CTerrain::AddDefaultLayer_t(void)
{
  // Add one layer using default texture, but do not refresh terrain 
  CTerrainLayer &tl = AddLayer_t((CTString)"Textures\\Editor\\Default.TEX", LT_NORMAL, FALSE);
  
  // Fill this layer
  tl.ResetLayerMask(255);
}

// Build quadtree for terrain
void CTerrain::BuildQuadTree(void)
{
  INDEX ctQuadNodeRows = d_ptr->tr_ctTilesX;
  INDEX ctQuadNodeCols = d_ptr->tr_ctTilesY;
  INDEX ctQuadNodes = 0;

  // Create quad tree levels
  while (TRUE)
  {
    QuadTreeLevel &qtl = d_ptr->tr_aqtlQuadTreeLevels.Push();
    
    // Remember first node
    qtl.qtl_iFirstNode = ctQuadNodes;
    
    // Add nodes in this level to total node count
    ctQuadNodes += ClampDn(ctQuadNodeRows, (INDEX)1) * ClampDn(ctQuadNodeCols, (INDEX)1);

    // Count nodes in this level
    qtl.qtl_ctNodes    = ctQuadNodes - qtl.qtl_iFirstNode;
    qtl.qtl_ctNodesCol = ctQuadNodeCols;
    qtl.qtl_ctNodesRow = ctQuadNodeRows;
    
    // If only one node is in this level
    if (qtl.qtl_ctNodes == 1) {
      // this is last level so exit loop
      break;
    }
    if (ctQuadNodeCols % 2 == 1 && ctQuadNodeCols != 1) {
      ctQuadNodeCols = (ctQuadNodeCols + 1) >> 1;
    } else {
      ctQuadNodeCols = ctQuadNodeCols >> 1;
    }

    if (ctQuadNodeRows % 2 == 1 && ctQuadNodeRows != 1) {
      ctQuadNodeRows = (ctQuadNodeRows + 1) >> 1;
    } else {
      ctQuadNodeRows = ctQuadNodeRows >> 1;
    }
  }
  
  QuadTreeLevel &qtlFirst = d_ptr->tr_aqtlQuadTreeLevels[0];
  
  // Add quadtree nodes for first level
  d_ptr->tr_aqtnQuadTreeNodes.Push(qtlFirst.qtl_ctNodes);
  
  // For each quad tree node in first level
  for (INDEX iqn = 0; iqn < qtlFirst.qtl_ctNodes; iqn++)
  {
    // Generate vertices for tile in first lod with no vertex lerping
    CTerrainTile &tt = d_ptr->tr_attTiles[iqn];
    QuadTreeNode &qtn = d_ptr->tr_aqtnQuadTreeNodes[iqn];
    tt.tt_iLod = -1;
    tt.tt_iRequestedLod  = 0;
    tt.tt_fLodLerpFactor = 0;
    tt.AddFlag(TT_REGENERATE | TT_NO_TOPMAP_REGEN);
    ReGenerateTile(iqn);
    tt.RemoveFlag(TT_REGENERATE | TT_NO_TOPMAP_REGEN);

    // Set quad tree bbox as first vertex in tile
    GFXVertex4 &vtxFirst = tt.GetVertices()[0];
    qtn.qtn_aabbox = FLOATaabbox3D(FLOAT3D(vtxFirst.x,vtxFirst.y,vtxFirst.z));
    
    // For each vertex after first
    INDEX ctVtx = tt.GetVertices().Count();
    for (INDEX ivx = 1; ivx < ctVtx; ivx++)
    {
      // Add vertex in quad tree node bbox
      GFXVertex4 &vtx = tt.GetVertices()[ivx];
      qtn.qtn_aabbox |= FLOATaabbox3D(FLOAT3D(vtx.x, vtx.y, vtx.z));
    }
    
    // Release arrays of tile
    tt.ReleaseTileArrays();
    tt.tt_iLod = -1;
    tt.tt_iRequestedLod = 0;
    qtn.qtn_iTileIndex = iqn;
    
    // Nodes in first level does not have children
    qtn.qtn_iChild[0] = -1;
    qtn.qtn_iChild[1] = -1;
    qtn.qtn_iChild[2] = -1;
    qtn.qtn_iChild[3] = -1;
  }

  // Create all other levels of quad tree
  INDEX ctQuadLevels = d_ptr->tr_aqtlQuadTreeLevels.Count();
  
  // For each quadtree level after first
  for (INDEX iql = 1; iql < ctQuadLevels; iql++)
  {
    QuadTreeLevel &qtl = d_ptr->tr_aqtlQuadTreeLevels[iql];
    QuadTreeLevel &qtlPrev = d_ptr->tr_aqtlQuadTreeLevels[iql - 1];
    
    // For each quadtree node row
    for (INDEX ir = 0; ir < qtlPrev.qtl_ctNodesRow; ir += 2)
    {
      // For each quadtree node col
      for (INDEX ic = 0; ic < qtlPrev.qtl_ctNodesCol; ic += 2)
      {
        // Set quadtree node children 
        INDEX iqt = qtlPrev.qtl_iFirstNode + ic+ir*qtlPrev.qtl_ctNodesCol;
        QuadTreeNode &qtn = d_ptr->tr_aqtnQuadTreeNodes.Push();
        QuadTreeNode *pqtnFirst = &d_ptr->tr_aqtnQuadTreeNodes[iqt];
        
        // Set first child node
        qtn.qtn_aabbox = pqtnFirst->qtn_aabbox;
        qtn.qtn_iChild[0] = iqt;
        qtn.qtn_iChild[1] = -1;
        qtn.qtn_iChild[2] = -1;
        qtn.qtn_iChild[3] = -1;
        qtn.qtn_iTileIndex = -1; 
        
        // If second child node exists
        if (ic + 1 < qtlPrev.qtl_ctNodesCol) {
          // Set second child
          qtn.qtn_iChild[1] = iqt + 1;
          qtn.qtn_aabbox |= pqtnFirst[1].qtn_aabbox;
        }
        
        // If fourth child exist
        if (ir + 1 < qtlPrev.qtl_ctNodesRow) {
          // Set third child
          qtn.qtn_iChild[2] = iqt + qtlPrev.qtl_ctNodesCol;
          qtn.qtn_aabbox |= pqtnFirst[qtlPrev.qtl_ctNodesCol].qtn_aabbox;
          
          // Set fourth child
          if (ic + 1 < qtlPrev.qtl_ctNodesCol) {
            qtn.qtn_iChild[3] = iqt + qtlPrev.qtl_ctNodesCol + 1;
            qtn.qtn_aabbox |= pqtnFirst[qtlPrev.qtl_ctNodesCol + 1].qtn_aabbox;
          }
        }
      }
    }
  }
}


// Update higher quadtree levels from first one
void CTerrain::UpdateQuadTree()
{
  // for all quad tree levels after first one
  INDEX ctqtl = d_ptr->tr_aqtlQuadTreeLevels.Count();
  for (INDEX iqtl = 1; iqtl < ctqtl; iqtl++)
  {
    QuadTreeLevel &qtl = d_ptr->tr_aqtlQuadTreeLevels[iqtl];

    // For each quad tree node in this level
    INDEX ctqtn = qtl.qtl_iFirstNode + qtl.qtl_ctNodes;
    for (INDEX iqtn = qtl.qtl_iFirstNode; iqtn < ctqtn; iqtn++)
    {
      QuadTreeNode &qtn = d_ptr->tr_aqtnQuadTreeNodes[iqtn];
      
      // Trash qtn box
      qtn.qtn_aabbox.maxvect = FLOAT3D(-1, -1, -1);
      qtn.qtn_aabbox.minvect = FLOAT3D( 1,  1,  1);

      // For each child of qtn
      for (INDEX ichild = 0; ichild < 4; ichild++)
      {
        INDEX iqtnChild = qtn.qtn_iChild[ichild];
        // If child exists
        if (iqtnChild != (-1))
        {
          QuadTreeNode &qtnChild = d_ptr->tr_aqtnQuadTreeNodes[iqtnChild];
          // If qtn box is empty 
          if (qtn.qtn_aabbox.IsEmpty()) {
            // set qtn box as box of this child 
            qtn.qtn_aabbox = qtnChild.qtn_aabbox;
          // if it had some data
          } else {
            // just add child box to qtn box
            qtn.qtn_aabbox |= qtnChild.qtn_aabbox;
          }
        }
      }
    }
  }
}


// Generation
void CTerrain::ReGenerate(void)
{
  // For each tile in terrain
  for (INDEX it = 0; it < d_ptr->tr_ctTiles; it++)
  {
    CTerrainTile &tt = d_ptr->tr_attTiles[it];
    
    // Calculate new lod
    tt.tt_iRequestedLod = tt.CalculateLOD();
  }

  // For each tile that is waiting in regen queue
  INDEX ctrt = d_ptr->tr_auiRegenList.Count();
  INDEX irt = 0;
  for (; irt < ctrt; irt++)
  {
    // Mark tile as ready for regeneration
    INDEX iTileIndex = d_ptr->tr_auiRegenList[irt];
    CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
    tt.AddFlag(TT_REGENERATE);
  }

  // For each tile that is waiting in regen queue
  for (irt = 0; irt < ctrt; irt++)
  {
    INDEX iTileIndex = d_ptr->tr_auiRegenList[irt];
    CTerrainTile &tt = d_ptr->tr_attTiles[iTileIndex];
    
    // If tile needs to be regenerated
    if (tt.GetFlags() & TT_REGENERATE) {
      // Regenerate it now
      ReGenerateTile(tt.tt_iIndex);
      
      // Remove flag for regeneration
      tt.RemoveFlag(TT_REGENERATE);
    }
  }

  // Clear regenration list
  ClearRegenList();
}

// TODO: Add comment here
extern TStaticStackArray<GFXVertex4> _avLerpedVerices;
static void ShowTerrainInfo(CAnyProjection3D &apr, CDrawPort *pdp, CTerrain *ptrTerrain)
{
  pdp->SetFont(_pfdConsoleFont);
  pdp->SetTextAspect(1.0f);
  pdp->SetOrtho();
  CTString strInfo;
  INDEX ctTopMaps = ptrTerrain->d_ptr->tr_atdTopMaps.Count() + 1;
  strInfo.PrintF("Tris = %d\nNodes = %d\nDelayed nodes = %d\nTop maps = %d\nTexgens = %d, %d\nShadowmap updates = %d\n",
                 _ctTris, _ctNodesVis, _ctDelayedNodes, ctTopMaps, ctGeneratedTopMaps, ctGlobalTopMaps, _ctShadowMapUpdates);

  TStaticStackArray<INDEX> iaLodInfo;
  iaLodInfo.Push(ptrTerrain->GetMaxTileLod() + 1);
  memset(&iaLodInfo[0], 0, sizeof(INDEX) * iaLodInfo.sa_Count);
  
  // Build lod info
  for (INDEX it = 0; it < ptrTerrain->GetTileCount(); it++)
  {
    CTerrainTile &tt = ptrTerrain->d_ptr->tr_attTiles[it];
    INDEX &ili = iaLodInfo[tt.tt_iLod];
    ili++;
  }
  
  // Show how many tiles are in witch lod
  CTString strTemp = "LodInfo:\n";
  for (INDEX itti = 0; itti < ptrTerrain->GetMaxTileLod() + 1; itti++)
  {
    CTString str;
    CArrayHolder &ah = ptrTerrain->d_ptr->tr_aArrayHolders[itti];
    str.PrintF("L%d = mem = %d KB, nodes = %d\n", itti, ah.GetUsedMemory() / 1024, iaLodInfo[itti]);
    strTemp += str;
  }
  strTemp += "\n";
  strInfo +=strTemp;

  // Show memory usage
  SLONG slUsedMemory = 0;
  
  // Height map usage
  SLONG slHeightMap = ptrTerrain->GetHeightMapWidth() * ptrTerrain->GetHeightMapHeight() * sizeof(UWORD);
  
  // Edge map usage
  SLONG slEdgeMap = ptrTerrain->GetHeightMapWidth() * ptrTerrain->GetHeightMapHeight() * sizeof(UBYTE);
  
  // Shadow map usage
  SLONG slShadowMap = ptrTerrain->GetShadowMapTex()->GetUsedMemory();
  
  // Quad tree usage
  SLONG slQTNodes  = sizeof(QuadTreeNode) * ptrTerrain->d_ptr->tr_aqtnQuadTreeNodes.Count();
  SLONG slQTLevels = sizeof(QuadTreeLevel) * ptrTerrain->d_ptr->tr_aqtlQuadTreeLevels.Count();
  
  // Tiles usage 
  SLONG slTiles = 0;
  INDEX cttt = ptrTerrain->GetTileCount();
  for (INDEX itt = 0; itt < cttt; itt++)
  {
    CTerrainTile &tt = ptrTerrain->d_ptr->tr_attTiles[itt];
    slTiles += tt.GetUsedMemory();
  }
  
  // Arrays holders usage
  SLONG slArrayHoldes = 0;
  INDEX ctah = ptrTerrain->d_ptr->tr_aArrayHolders.Count();
  for (INDEX iah = 0; iah < ctah; iah++)
  {
    CArrayHolder &ah = ptrTerrain->d_ptr->tr_aArrayHolders[iah];
    slArrayHoldes += ah.GetUsedMemory();
  }
  SLONG slLayers = 0;
  
  // Terrain layers usage
  INDEX cttl = ptrTerrain->d_ptr->tr_atlLayers.Count();
  for (INDEX itl = 0; itl < cttl; itl++)
  {
    CTerrainLayer &tl = ptrTerrain->d_ptr->tr_atlLayers[itl];
    slLayers += tl.GetUsedMemory();
  }
  SLONG slTopMaps = 0;
  
  // Top maps usage
  INDEX cttm = ptrTerrain->d_ptr->tr_atdTopMaps.Count();
  for (INDEX itm = 0; itm < cttm; itm++)
  {
    CTextureData *ptdTopMap = &ptrTerrain->d_ptr->tr_atdTopMaps[itm];
    slTopMaps += ptdTopMap->GetUsedMemory();
  }
  SLONG slGlobalTopMap = ptrTerrain->GetTopMapTex()->GetUsedMemory();
  SLONG slTileBatchingSize = GetUsedMemoryForTileBatching();
  SLONG slVertexSmoothing  = _avLerpedVerices.sa_Count * sizeof(GFXVertex4);
  extern SLONG  _slSharedTopMapSize; // Shared top map size
  
  // Global top map usage
  SLONG slTotal = slHeightMap + slEdgeMap + slShadowMap + slQTNodes + slQTLevels + slTiles + slArrayHoldes + slLayers +
                  slTopMaps + slGlobalTopMap + slTileBatchingSize + slVertexSmoothing;
  CTString strMemoryUsed;
  strMemoryUsed.PrintF("Heightmap = %d KB\nEdgemap   = %d KB\nShadowMap = %d KB\nQuadTree  = %d KB\nTiles     = %d KB\nArrays    = %d KB\nLayers    = %d KB\nTopMaps   = %d KB\nGlobal TM = %d KB\nShared TM = %d KB\nVtx lerp  = %d KB\nBatching  = %d KB\nTotal     = %d KB\n",
                       slHeightMap / 1024, slEdgeMap / 1024, slShadowMap / 1024, (slQTNodes + slQTLevels) / 1024,
                       slTiles / 1024, slArrayHoldes / 1024, slLayers / 1024,slTopMaps / 1024, slGlobalTopMap / 1024,
                       _slSharedTopMapSize / 1024, slVertexSmoothing / 1024, slTileBatchingSize / 1024, slTotal / 1024);

  strInfo += strMemoryUsed;

  extern FLOAT3D _vDirection;
  strInfo += CTString(0,"Shadow map size = %d,%d [%d]\nShading map size= %d,%d [%d]\n",
                      ptrTerrain->GetShadowMapWidth(), ptrTerrain->GetShadowMapHeight(),
                      ptrTerrain->d_ptr->tr_iShadowMapSizeAspect, ptrTerrain->GetShadingMapWidth(),
                      ptrTerrain->GetShadingMapHeight(), ptrTerrain->d_ptr->tr_iShadingMapSizeAspect);
  pdp->PutText(strInfo, 0, 40);
}

// Copy terrain data from other terrain
void CTerrain::Copy(CTerrain &trOther)
{
  ASSERT(FALSE);
}

CTerrain::~CTerrain()
{
  Clear();

  delete d_ptr;
}

// Discard all cached shading info for models
void CTerrain::DiscardShadingInfos(void)
{
  FORDELETELIST(CShadingInfo, si_lnInPolygon, tr_lhShadingInfos, itsi)
  {
    itsi->si_penEntity->en_ulFlags &= ~ENF_VALIDSHADINGINFO;
    itsi->si_lnInPolygon.Remove();
    itsi->si_pbpoPolygon = NULL;
  }
}

// Clear height map
void CTerrain::ClearHeightMap(void)
{
  // If height map space was allocated
  if (d_ptr->tr_puwHeightMap != NULL) {
    // release it
    FreeMemory(d_ptr->tr_puwHeightMap);
    d_ptr->tr_puwHeightMap = NULL;
  }
}

// Clear shadow map
void CTerrain::ClearShadowMap(void)
{
  // Clear current terrain shadow map
  d_ptr->tr_tdShadowMap.Clear();

  // Also clear shading map
  if (d_ptr->tr_puwShadingMap != NULL) {
    FreeMemory(d_ptr->tr_puwShadingMap);
    d_ptr->tr_puwShadingMap = NULL;
  }
}

// TODO: Add comment here
void CTerrain::ClearEdgeMap(void)
{
  // If space for edge map was allocated
  if (d_ptr->tr_pubEdgeMap != NULL) {
    // Release it
    FreeMemory(d_ptr->tr_pubEdgeMap);
    d_ptr->tr_pubEdgeMap = NULL;
  }
}

// Clear all topmaps
void CTerrain::ClearTopMaps(void)
{
  // For each topmap in terrain
  INDEX cttm = d_ptr->tr_atdTopMaps.Count();
  for (INDEX itm = 0; itm < cttm; itm++)
  {
    CTextureData *ptdTopMap = &d_ptr->tr_atdTopMaps[0];
    
    // Remove memory pointer cos it is shared memory
    ptdTopMap->td_pulFrames = NULL;
    
    // Clear tile topmap
    ptdTopMap->Clear();
    
    // Remove topmap from container
    d_ptr->tr_atdTopMaps.Remove(ptdTopMap);
    delete ptdTopMap;
    ptdTopMap = NULL;
  }
  d_ptr->tr_atdTopMaps.Clear();
  ASSERT(_CrtCheckMemory());

  // Remove memory pointer from global top map cos it is shared memory
  d_ptr->tr_tdTopMap.td_pulFrames = NULL;
  
  // Clear global topmap
  d_ptr->tr_tdTopMap.Clear();
}

// Clear tiles
void CTerrain::ClearTiles(void)
{
  // for each tile
  for (INDEX itt = 0; itt < d_ptr->tr_ctTiles; itt++)
  {
    CTerrainTile &tt = d_ptr->tr_attTiles[itt];
    
    // Clear tile
    tt.Clear();
  }
  d_ptr->tr_attTiles.Clear();
  d_ptr->tr_ctTiles  = 0;
  d_ptr->tr_ctTilesX = 0;
  d_ptr->tr_ctTilesY = 0;
}

// Clear arrays
void CTerrain::ClearArrays(void)
{
  // Clear Array holders
  d_ptr->tr_aArrayHolders.Clear();
}

// Clear quadtree
void CTerrain::ClearQuadTree(void)
{
  // Destroy quad tree nodes
  d_ptr->tr_aqtnQuadTreeNodes.Clear();
  
  // Clear quad tree levels
  d_ptr->tr_aqtlQuadTreeLevels.Clear();
}

// Clear layers
void CTerrain::ClearLayers(void)
{
  // Clear layers
  d_ptr->tr_atlLayers.Clear();
}

// Clean terrain data (does not remove layers)
void CTerrain::Clean(BOOL bCleanLayers) // = TRUE
{
  ASSERT(FALSE);
  ClearHeightMap();
  ClearShadowMap();
  ClearEdgeMap();
  ClearTopMaps();
  ClearTiles();
  ClearArrays();
  ClearQuadTree();

  // If layers clear is required
  if (bCleanLayers) {
    ClearLayers();
  }
}

// Clear terrain data
void CTerrain::Clear(void)
{
  DiscardShadingInfos();
  ClearHeightMap();
  ClearShadowMap();
  ClearEdgeMap();
  ClearTopMaps();
  ClearTiles();
  ClearArrays();
  ClearQuadTree();
  ClearLayers();

  if (d_ptr->tr_ptdDetailMap != NULL) {
    _pResourceMgr->Release(d_ptr->tr_ptdDetailMap);
    d_ptr->tr_ptdDetailMap = NULL;
  }
}

UWORD *CTerrain::GetHeightMap()
{
  return d_ptr->tr_puwHeightMap;
}

UWORD *CTerrain::GetShadingMap()
{
  return d_ptr->tr_puwShadingMap;
}

UBYTE *CTerrain::GetEdgeMap()
{
  return d_ptr->tr_pubEdgeMap;
}

CTextureData *CTerrain::GetTopMapTex()
{
  return &d_ptr->tr_tdTopMap;
}

CTextureData *CTerrain::GetShadowMapTex()
{
  return &d_ptr->tr_tdShadowMap;
}

CTextureData *CTerrain::GetDetailMapTex()
{
  return d_ptr->tr_ptdDetailMap;
}

PIX CTerrain::GetHeightMapWidth() const
{
  return d_ptr->tr_pixHeightMapWidth;
}

PIX CTerrain::GetHeightMapHeight() const
{
  return d_ptr->tr_pixHeightMapHeight;
}

PIX CTerrain::GetTopMapWidth() const
{
  return d_ptr->tr_pixTopMapWidth;
}

PIX CTerrain::GetTopMapHeight() const
{
  return d_ptr->tr_pixTopMapHeight;
}

SLONG CTerrain::GetShadowMapAspect() const
{
  return d_ptr->tr_iShadowMapSizeAspect;
}

SLONG CTerrain::GetShadingMapAspect() const
{
  return d_ptr->tr_iShadingMapSizeAspect;
}

VEC3F CTerrain::GetStretch() const
{
  return d_ptr->tr_vStretch;
}

FLOAT CTerrain::GetDistFactor() const
{
  return d_ptr->tr_fDistFactor;
}

VEC3F CTerrain::GetMetricSize() const
{
  return d_ptr->tr_vTerrainSize;
}

PIX CTerrain::GetFirstMipTopMapWidth() const
{
  return d_ptr->tr_pixFirstMipTopMapWidth;
}

PIX CTerrain::GetFirstMipTopMapHeight() const
{
  return d_ptr->tr_pixFirstMipTopMapHeight;
}

INDEX CTerrain::GetTileCount() const
{
  return d_ptr->tr_ctTiles;
}

INDEX CTerrain::GetTilesPerRow() const
{
  return d_ptr->tr_ctTilesX;
}

INDEX CTerrain::GetTilesPerCol() const
{
  return d_ptr->tr_ctTilesY;
}

INDEX CTerrain::GetMaxTileLod() const
{
  return d_ptr->tr_iMaxTileLod;
}

INDEX CTerrain::GetQuadsPerTileRow(void) const
{
  return d_ptr->tr_ctQuadsInTileRow;
}

INDEX CTerrain::GetVerticesPerTileRow(void) const
{
  return d_ptr->tr_ctVerticesInTileRow;
}

TStaticStackArray<class CTerrainLayer> &CTerrain::GetLayers()
{
  return d_ptr->tr_atlLayers;
}