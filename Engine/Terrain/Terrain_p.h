/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TERRAINPRIVATE_H
#define SE_INCL_TERRAINPRIVATE_H

class CTerrainPrivate
{
  public:
    CTerrainPrivate();
    ~CTerrainPrivate();
    
  public:
    UWORD  *tr_puwHeightMap;        // Terrain height map
    UWORD  *tr_puwShadingMap;       // Terrain shading map
    UBYTE  *tr_pubEdgeMap;          // Terrain edge map
    CTextureData tr_tdTopMap;       // Terrain top map
    CTextureData tr_tdShadowMap;    // Terrain shadow map
    CTextureData *tr_ptdDetailMap;  // Terrain detail map
    
    PIX     tr_pixHeightMapWidth;   // Terrain height map widht
    PIX     tr_pixHeightMapHeight;  // Terrain height map height
    PIX     tr_pixTopMapWidth;         // Width  of terrain top map
    PIX     tr_pixTopMapHeight;        // Height of terrain top map
    SLONG   tr_iShadowMapSizeAspect;   // Size of shadow map (relative to height map size)
    SLONG   tr_iShadingMapSizeAspect;  // Size of shading map (relative to shadow map)
    FLOAT   tr_fDistFactor; // Distance for lod switching
    FLOAT3D tr_vStretch;    // Terrain stretch
    FLOAT3D tr_vTerrainSize;        // Terrain size in metars
    
    PIX     tr_pixFirstMipTopMapWidth; // Width  of tile first mip top map
    PIX     tr_pixFirstMipTopMapHeight;// Height of tile first mip top map
    
    INDEX   tr_ctTiles;     // Terrain tiles count
    INDEX   tr_ctTilesX;    // Terrain tiles count in row
    INDEX   tr_ctTilesY;    // Terrain tiles count in col
    INDEX   tr_iMaxTileLod; // Maximum lod in witch one tile can be
    INDEX   tr_ctQuadsInTileRow;    // Count of quads in one row in tile
    INDEX   tr_ctVerticesInTileRow; // Count of vertices in one row in tile
    
    TStaticStackArray<QuadTreeNode>        tr_aqtnQuadTreeNodes;  // Array of quadtree nodes
    TStaticStackArray<QuadTreeLevel>       tr_aqtlQuadTreeLevels; // Array of quadtree levels
    TStaticArray<class CTerrainTile>       tr_attTiles;           // Array of terrain tiles for terrain
    TStaticStackArray<class CTerrainLayer> tr_atlLayers;          // Array of terrain layers
    TStaticArray<class CArrayHolder>       tr_aArrayHolders;      // Array of memory holders for each lod
    TDynamicContainer<class CTextureData>  tr_atdTopMaps;         // Array of top maps for each tile array (used by ArrayHolder)
    TStaticStackArray<INDEX>               tr_auiRegenList;       // List of tiles that need to be regenerated
};

#endif