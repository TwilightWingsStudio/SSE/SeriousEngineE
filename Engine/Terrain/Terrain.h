/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TERRAIN_H
#define SE_INCL_TERRAIN_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Core/IO/FileName.h>
#include <Core/Math/AABBox.h>
#include <Engine/Brushes/BrushBase.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Terrain/TerrainLayer.h>
#include <Engine/Terrain/TerrainTile.h>
#include <Engine/Terrain/ArrayHolder.h>
#include <Core/Templates/StaticArray.cpp>

#define TR_REGENERATE              (1UL<<0) // terrain needs to be regenerated
#define TR_REGENERATE_TOP_MAP      (1UL<<1) // regenerate terrain top map
#define TR_ALLOW_TOP_MAP_REGEN     (1UL<<2) // allow top map regen (top map is not regenerate if this flag is not set)
#define TR_REBUILD_QUADTREE        (1UL<<3) // rebuild higher levels of quad tree nodes (last levels are rebuild by tile flags)
#define TR_UPDATE_SHADOWMAP        (1UL<<4) // update terrain shadow map       (NOT USED!)
#define TR_ALLOW_SHADOW_MAP_UPDATE (1UL<<5) // allow terrain shadow map update (NOT USED!)
#define TR_SHOW_SELECTION          (1UL<<6) // show selection
#define TR_HAS_FOG                 (1UL<<7) // terrain has fog
#define TR_HAS_HAZE                (1UL<<8) // terrain has haze

/*
 * TODO: Add comment here
 */
struct QuadTreeNode
{
  FLOATaabbox3D qtn_aabbox;    // Bounding box for this quadtree node
  INDEX         qtn_iTileIndex;// Index of tile that this node represents
  INDEX         qtn_iChild[4]; // Indices of children for this quadtree node
};

/*
 * TODO: Add comment here
 */
struct QuadTreeLevel
{
  INDEX qtl_iFirstNode; // Index of first quadtree node in this level
  INDEX qtl_ctNodes;    // Count of nodes in this level
  INDEX qtl_ctNodesCol; // Count of nodes in col
  INDEX qtl_ctNodesRow; // Count of nodes in row
};

/*
 * TODO: Add comment here
 */
struct Point
{
  Point() {}
  ~Point() {}
  
  Point(const Point &ptOther)
  {
    *this = ptOther;
  }
  
  INDEX pt_iX;
  INDEX pt_iY;
};

/*
 * TODO: Add comment here
 */
struct Rect
{
  Rect() {}
  ~Rect() {}
  
  Rect(const Rect &rcOther)
  {
    *this = rcOther;
  }
  
  Rect(INDEX iLeft, INDEX iTop, INDEX iRight, INDEX iBottom)
  {
    rc_iLeft   = iLeft;
    rc_iTop    = iTop;
    rc_iRight  = iRight;
    rc_iBottom = iBottom;
  }
  INDEX Width() {return rc_iRight-rc_iLeft;}
  INDEX Height() {return rc_iBottom-rc_iTop;}
  INDEX rc_iLeft;
  INDEX rc_iRight;
  INDEX rc_iTop;
  INDEX rc_iBottom;
};

/*
 * TODO: Add comment here
 */
class ENGINE_API CTerrain : public CBrushBase
{
  public:
    CTerrain();
    ~CTerrain();

    // Render terrain tiles
    void Render(CAnyProjection3D &apr, CDrawPort *pdp);
    
    // Render terrain tiles in wireframe
    void RenderWireFrame(CAnyProjection3D &apr, CDrawPort *pdp, COLOR &colEdges);

    
    // Create empty terrain with given size
    void CreateEmptyTerrain_t(PIX pixWidth,PIX pixHeight);
    
    // Import height map from targa file
    void ImportHeightMap_t(CTFileName fnHeightMap, BOOL bUse16b = TRUE);
    
    // Export height map to targa file
    void ExportHeightMap_t(CTFileName fnHeightMap, BOOL bUse16b = TRUE);
    
    // Rebuild all terrain
    void ReBuildTerrain(BOOL bDelayTileRegen = FALSE);
    
    // Refresh terrain
    void RefreshTerrain(void);

    // Set height map size
    void AllocateHeightMap(PIX pixWidth, PIX pixHeight);
    
    // Change height map size
    void ReAllocateHeightMap(PIX pixWidth, PIX pixHeight);
    
    // Set terrain size
    void SetTerrainSize(FLOAT3D vSize);
    
    // Set shadow map size aspect (relative to height map size) and shading map aspect (relative to shadow map size)
    void SetShadowMapsSize(INDEX iShadowMapAspect, INDEX iShadingMapAspect);

    // Set size of top map texture
    void SetGlobalTopMapSize(PIX pixTopMapSize);
    
    // Set size of top map texture for tiles in lower lods
    void SetTileTopMapSize(PIX pixLodTopMapSize);
    
    // Set lod distance factor
    void SetLodDistanceFactor(FLOAT fLodDistance);

    //! Get shadow map dimension.
    static inline PIX GetShadowMapDim(PIX pixHeightMapDim, SLONG slSizeAspect)
    {
      if (slSizeAspect < 0) {
        return (pixHeightMapDim - 1) >> (-slSizeAspect);
      } else {
        return (pixHeightMapDim - 1) << ( slSizeAspect);
      }
    }

    //! Get shadow map width.
    inline PIX GetShadowMapWidth(void)
    {
      return GetShadowMapDim(GetHeightMapWidth(), GetShadowMapAspect());
    }

    //! Get shadow map height.
    inline PIX GetShadowMapHeight(void)
    {
      return GetShadowMapDim(GetHeightMapHeight(), GetShadowMapAspect());
    }

    // Get shading map size
    PIX GetShadingMapWidth(void);
    PIX GetShadingMapHeight(void);

    
    // Get reference to layer
    CTerrainLayer &GetLayer(INDEX iLayer);
    
    // Add new layer
    CTerrainLayer &AddLayer_t(CTFileName fnTexture, LayerType ltType = LT_NORMAL, BOOL bUpdateTerrain = TRUE);
    
    // Remove one layer
    void RemoveLayer(INDEX iLayer, BOOL bUpdateTerrain = TRUE);
    
    // Move layer to new position
    INDEX SetLayerIndex(INDEX iLayer,INDEX iNewIndex, BOOL bUpdateTerrain = TRUE);

    
    // Add tile to reqen queue
    void AddTileToRegenQueue(INDEX iTileIndex);
    
    // Add all tiles to regen queue
    void AddAllTilesToRegenQueue(void);
    
    // Clear current regen list
    void ClearRegenList(void);
    

    // Update shadow map
    void UpdateShadowMap(FLOATaabbox3D *pbboxUpdate = NULL, BOOL bAbsoluteSpace = FALSE);
    
    // Update top map
    void UpdateTopMap(INDEX iTileIndex, Rect *prcDest = NULL);

    
    // Terrain flags handling
    inline ULONG &GetFlags(void)
    {
      return tr_ulTerrainFlags;
    }
    
    inline void SetFlags(ULONG ulFlags)
    {
      tr_ulTerrainFlags  = ulFlags;
    }
    
    inline void AddFlag(ULONG ulFlag)
    {
      tr_ulTerrainFlags |= ulFlag;
    }
    
    inline void RemoveFlag(ULONG ulFlag)
    {
      tr_ulTerrainFlags &= ~ulFlag;
    }

    
    // Get first quad tree node bounding box (all tiles box)
    void GetAllTerrainBBox(FLOATaabbox3D &bbox);

    INDEX GetBrushType(void)
    {
      return CBrushBase::BT_TERRAIN;
    } // this is terrain not brush
    
    // Get shading color from tex coords in shading map
    COLOR GetShadeColor(CShadingInfo *psi);
    
    // Get plane from given point
    FLOATplane3D GetPlaneFromPoint(FLOAT3D &vAbsPoint);


    // Sets number of quads in row of one tile
    void SetQuadsPerTileRow(INDEX ctQuadsPerTileRow);

    // Read from stream.
    void Read_t(CTStream *istrFile); // throw char *
    
    // Write to stream.
    void Write_t(CTStream *ostrFile);  // throw char *
    
    void ReadLayerOld_t(INDEX iTerrainLayer, CTStream *istrFile);
    void ReadLayer_t(INDEX iTerrainLayer, CTStream *istrFile);
    
    void WriteLayerOld_t(INDEX iTerrainLayer, CTStream *ostrFile);
    void WriteLayer_t(INDEX iTerrainLayer, CTStream *ostrFile);

    // Copy terrain data from other terrain
    void Copy(CTerrain &trOther);
    
    // Clean terrain data (does not remove layers)
    void Clean(BOOL bCleanLayers = TRUE);
    
    // Clean terrain data
    void Clear(void);


    // Renders visible terrain tiles in wireframe 
    void RenderWire(void);
    
    // Render vertices of all terrain tiles
    void RenderPoints(void);
    
    // Generate terrain tiles 
    void ReGenerate(void);
    
    // Build terrain data
    void BuildTerrainData(void);
    
    // Build quadtree for terrain
    void BuildQuadTree(void);
    
    // Update quadtree for terrain
    void UpdateQuadTree(void);
    
    // Generate terrain top map
    void GenerateTerrainTopMap(void);
    
    // Draws one quad node and its children
    void DrawQuadNode(INDEX iqn);
    
    // Set Terrain stretch
    void SetTerrainStretch(FLOAT3D vStretch);
    
    // Add default layer
    void AddDefaultLayer_t(void);

    // Discard all cached shading info for models
    void DiscardShadingInfos(void);

    // Clear height map
    void ClearHeightMap(void);
    
    // Clear shadow map
    void ClearShadowMap(void);
    
    // Clear edge map
    void ClearEdgeMap(void);
    
    // Clear all topmaps
    void ClearTopMaps(void);
    
    // Clear tiles
    void ClearTiles(void);
    
    // Clear arrays
    void ClearArrays(void);
    
    // Clear quadtree
    void ClearQuadTree(void);
    
    // Clear layers
    void ClearLayers(void);
    
  public:
    inline CEntity *GetEntity()
    {
      return tr_penEntity;
    }

  public:
    UWORD *GetHeightMap();
    UWORD *GetShadingMap();
    UBYTE *GetEdgeMap();
    CTextureData *GetTopMapTex();
    CTextureData *GetShadowMapTex();
    CTextureData *GetDetailMapTex();
    
    PIX GetHeightMapWidth() const;
    PIX GetHeightMapHeight() const;
    PIX GetTopMapWidth() const;
    PIX GetTopMapHeight() const;
    SLONG GetShadowMapAspect() const;
    SLONG GetShadingMapAspect() const;
    VEC3F GetStretch() const;
    FLOAT GetDistFactor() const;
    VEC3F GetMetricSize() const;
    
    PIX GetFirstMipTopMapWidth() const;
    PIX GetFirstMipTopMapHeight() const;
    
    INDEX GetTileCount() const;
    INDEX GetTilesPerRow() const;
    INDEX GetTilesPerCol() const;
    INDEX GetMaxTileLod() const;
    INDEX GetQuadsPerTileRow() const;
    INDEX GetVerticesPerTileRow(void) const;
    
    TStaticStackArray<class CTerrainLayer> &GetLayers();

  public:
    CListNode tr_lnInActiveTerrains; // for linking in list of active terrains in renderer
    CListHead tr_lhShadingInfos;     // for linking shading infos of entities
    CEntity *tr_penEntity;           // pointer to entity that holds this terrain
    INDEX   tr_iSelectedLayer;      // Selected layer in we
    ULONG   tr_ulTerrainFlags;      // Terrain flags

  private:
    INDEX   tr_ctDriverChanges;
    
  public:
    class CTerrainPrivate *d_ptr;
};

#endif