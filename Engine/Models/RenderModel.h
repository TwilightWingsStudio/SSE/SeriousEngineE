/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_RENDERMODEL_H
#define SE_INCL_RENDERMODEL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Math/Placement.h>
#include <Core/Math/Projection.h>

/*
 *  This instance of render prefs object represents global preferences
 *  used for rendering of all models and their shadows
 */
ENGINE_API extern class CModelRenderPrefs _mrpModelRenderPrefs;


/*
 *  This object is used to define how models and their shadows will be rendered
 */
class ENGINE_API CModelRenderPrefs
{
  private:
    BOOL rp_BBoxFrameVisible;                       // determines visibility of frame BBox
    BOOL rp_BBoxAllVisible;                         // determines visibility of all frames BBox
    COLOR rp_InkColor;                              // ink color (wire frame)
    COLOR rp_PaperColor;                            // paper color
    ULONG rp_RenderType;                            // model's rendering type
    INDEX rp_ShadowQuality;                         // model shadow's quality (substraction to mip model index)
    
  public:
    // Constructor sets defult values
    CModelRenderPrefs();
    
    
    // Set model rendering type
    void SetRenderType(ULONG rtNew);
    
    // Set model rendering texture type
    void SetTextureType(ULONG rtNew);
    
    // Set model shading texture type
    void SetShadingType(ULONG rtNew);
    
    // Set shadow quality (best 0, worse -1, ...)
    void SetShadowQuality(INDEX iNew);
    
    
    // Decrease shadow quality
    void DesreaseShadowQuality(void);
    
    // Increase shadow quality
    void IncreaseShadowQuality(void);
    
    // Set wire frame on/off
    void SetWire(BOOL bWireOn);
    
    // Set hiden lines on/off
    void SetHiddenLines(BOOL bHiddenLinesOn);
    
    // Bounding box frames visible?
    BOOL BBoxFrameVisible();
    
    // Bounding box all frames visible?
    BOOL BBoxAllVisible();
    
    // Returns TRUE if wire frame is on
    BOOL WireOn(void);
    
    // Returns TRUE if hiden lines are visible
    BOOL HiddenLines(void);
    
    // Set ink color
    void SetInkColor(COLOR clrNew);
    
    // Get ink color
    COLOR GetInkColor();
    
    // Set paper color
    void SetPaperColor(COLOR clrNew);
    
    // Get paper color
    COLOR GetPaperColor();
    
    // Show bounding box frame
    void BBoxFrameShow(BOOL bShow);
    
    // Show bounding box all frames
    void BBoxAllShow(BOOL bShow);
    
    // Get model rendering type
    ULONG GetRenderType(void);
    
    // Retrieves current shadow quality level
    INDEX GetShadowQuality(void);
    
    // Read functions
    void Read_t(CTStream *istrFile);   // throw char *
    
    // Write functions
    void Write_t(CTStream *ostrFile);  // throw char *
};


// Texture used for simple model shadows
extern ENGINE_API CTextureObject _toSimpleModelShadow;

// Begin/end model rendering to screen
extern ENGINE_API void BeginModelRenderingView(CAnyProjection3D &prProjection, CDrawPort *pdp);
extern ENGINE_API void EndModelRenderingView(BOOL bRestoreOrtho = TRUE);

// Begin model rendering to shadow mask
extern ENGINE_API void BeginModelRenderingMask(CAnyProjection3D &prProjection, UBYTE *pubMask,
                                               SLONG slMaskWidth, SLONG slMaskHeight);
                                               
// End model rendering to shadow mask
extern ENGINE_API void EndModelRenderingMask(void);

#define RMF_ATTACHMENT          (1UL<<0)    // set for attachment render models
#define RMF_FOG                 (1UL<<1)    // render in fog
#define RMF_HAZE                (1UL<<2)    // render in haze
#define RMF_SPECTATOR           (1UL<<3)    // model will not be rendered but shadows might
#define RMF_INVERTED            (1UL<<4)    // stretch is inverted
#define RMF_BBOXSET             (1UL<<5)    // bounding box has been calculated
#define RMF_INSIDE              (1UL<<6)    // doesn't need clipping to frustum
#define RMF_INMIRROR            (1UL<<7)    // doesn't need clipping to mirror/warp plane
#define RMF_WEAPON              (1UL<<8)    // TEMP: weapon model is rendering so don't use ATI's Truform!

class ENGINE_API CRenderModel
{
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Implementation:
  public:
    CModelData *rm_pmdModelData;        // model's data
    struct ModelMipInfo *rm_pmmiMip;    // current mip
    ULONG rm_rtRenderType;              // current rendering preferences
    
    // Lerp information
    INDEX rm_iFrame0, rm_iFrame1;       
    FLOAT rm_fRatio;
    INDEX rm_iMipLevel;
    INDEX rm_iTesselationLevel;
    
    union
    {
      struct ModelFrameVertex8  *rm_pFrame8_0;    // ptr to last frame
      struct ModelFrameVertex16 *rm_pFrame16_0;
    };
    
    union
    {
      struct ModelFrameVertex8  *rm_pFrame8_1;    // ptr to next frame
      struct ModelFrameVertex16 *rm_pFrame16_1;
    };
    
    FLOAT3D rm_vLightObj;                // light vector as seen from object space
    
    // Placement of the object
    FLOAT3D rm_vObjectPosition;
    FLOATmatrix3D rm_mObjectRotation;
    
    // Object to view placement
    FLOAT3D rm_vObjectToView;
    FLOATmatrix3D rm_mObjectToView;
    
    // Decompression and stretch factors
    FLOAT3D rm_vStretch, rm_vOffset;
    
    // Bounding box min/max coords in object space
    FLOAT3D rm_vObjectMinBB, rm_vObjectMaxBB;
    
    // Flags and blend color global for this rendering
    ULONG rm_ulFlags;
    COLOR rm_colBlend;

    // Set modelview matrix if not already set
    void SetModelView(void);
    
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Interface:
  public:
    // TODO Add comment here
    CRenderModel(void);
    
    // TODO Add comment here
    ~CRenderModel(void);
    
    // Set placement of the object
    void SetObjectPlacement(const CPlacement3D &pl);
    void SetObjectPlacement(const FLOAT3D &v, const FLOATmatrix3D &m);

    FLOAT rm_fDistanceFactor;           // mip factor not including scaling or biasing
    FLOAT rm_fMipFactor;                // real mip factor 
    FLOAT3D rm_vLightDirection;         // direction shading light
    COLOR rm_colLight;                  // color of the shading light
    COLOR rm_colAmbient;                // color of the ambient
};


#endif  /* include-once check. */

