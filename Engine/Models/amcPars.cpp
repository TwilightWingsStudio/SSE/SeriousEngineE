/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>

#include <Engine/Resources/ReplaceFile.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Meshes/ModelConfiguration.h>
#include <Engine/Meshes/ParsingSmbs.h>
#include <Engine/Resources/ResourceManager.h>

struct SConfigParserContext
{
  CTString strFile;
  INDEX ctLines;
  BOOL bPreview;
  BOOL bRoot;

  SConfigParserContext(const CTString &str)
  {
    strFile = str;
    ctLines = 0;
    bPreview = FALSE;
    bRoot = TRUE;
  }
};

static CTString GetNonEmptyLine_t(CTStream &strm, SConfigParserContext &ctx)
{
  FOREVER {
   if (strm.AtEOF()) {
     ThrowF_t("Unexpected end of file");
   }

   CTString str;
   ctx.ctLines++;
   strm.GetLine_t(str);
   str.TrimSpacesLeft();
   if (str.RemovePrefix("//")) {  // skip comments
     continue;
   }

   if (str != "") {
     str.TrimSpacesRight();
     return str;
   }
  }
}

static void FixupFileName_t(CTString &strFnm)
{
  CTString strTag("_FNM");
  strTag.Data()[0] = 'T';

  strFnm.TrimSpacesLeft();
  if (!strFnm.RemovePrefix(strTag)) {  // must not directly have ids in code
    ThrowF_t("Expected %s before filename", strTag);
  }

  strFnm.TrimSpacesLeft();
}

// skip one block in config
static void SkipBlock_t(CTStream &strm, SConfigParserContext &ctx)
{
  CTString strLine;

  // expect to begin with an open bracket
  strLine = GetNonEmptyLine_t(strm, ctx);
  if (strLine != "{") {
    ThrowF_t("Expected '{'");
  }

  // start at level one
  INDEX ctLevel = 1;

  // repeat
  do {
    strLine = GetNonEmptyLine_t(strm, ctx);

    // count brackets
    if (strLine == "{") {
      ctLevel++;
    } else if (strLine == "}") {
      ctLevel--;
    }
  // until we close down all brackets
  } while (ctLevel > 0);
}

static void ParseModelConfig_t(CModelObject *pmo, CTStream &strm, SConfigParserContext &ctx)
{
  const BOOL bIsFirstLine = ctx.ctLines == 0;

  CTString strLine;

  // expect to begin with an open bracket
  strLine = GetNonEmptyLine_t(strm, ctx);

  // Player model AMC may have 'Name: %s' at first line.
  if (bIsFirstLine && strLine.HasPrefix("Name: ")) {
    strLine = GetNonEmptyLine_t(strm, ctx); // Go to the next line.
  }
  
  if (strLine != "{") {
    ThrowF_t("Expected '{'");
  }

  // repeat
  FOREVER {
    // read one line
    strLine = GetNonEmptyLine_t(strm, ctx);
    
    // if closed bracket
    if (strLine == "}") {
      // finish parsing
      return;
    }

    // if a preview-only block
    if (strLine.RemovePrefix("PreviewOnly")) {
      // if this is a preview then keep parsing it
      if (ctx.bPreview) {
        ParseModelConfig_t(pmo, strm, ctx);
      // if this is not a preview then skip that block
      } else {
        SkipBlock_t(strm, ctx);
      }
    // if include block
    } else if (strLine.RemovePrefix("Include:")) {
      // open the new file
      FixupFileName_t(strLine);
      CTFileStream strmIncluded;
      strmIncluded.Open_t(strLine);

      // include it
      SConfigParserContext ctx2(strLine);
      ctx2.bPreview = ctx.bPreview;
      ctx2.bRoot = FALSE;
      ParseModelConfig_t(pmo, strmIncluded, ctx2);
      strmIncluded.Close();

    // if setting the model
    } else if (strLine.RemovePrefix("Model:")) {
      // set the model
      FixupFileName_t(strLine);
      pmo->SetData_t(strLine);
      ctx.bRoot = FALSE;

    // if setting an anim for the model
    } else if (strLine.RemovePrefix("Animation:")) {
      // get animation number
      INDEX iAnim = -1;
      strLine.ScanF("%d", &iAnim);
      if (iAnim < 0) {
        ThrowF_t("Invalid animation number");
      }

      // check it
      if (iAnim >= pmo->GetAnimsCt()) {
        ThrowF_t("Animation %d does not exist in that model", iAnim);
      };

      // set it
      pmo->PlayAnim(iAnim, AOF_LOOPING);

    // if texture
    } else if (strLine.RemovePrefix("Texture:")) {
      // set texture
      FixupFileName_t(strLine);
      pmo->mo_toTexture.SetData_t(strLine);

    // if specular
    } else if (strLine.RemovePrefix("Specular:")) {
      // set texture
      FixupFileName_t(strLine);
      pmo->mo_toSpecular.SetData_t(strLine);

    // if reflection
    } else if (strLine.RemovePrefix("Reflection:")) {
      // set texture
      FixupFileName_t(strLine);
      pmo->mo_toReflection.SetData_t(strLine);

    // if specular
    } else if (strLine.RemovePrefix("Bump:")) {
      // set texture
      FixupFileName_t(strLine);
      pmo->mo_toBump.SetData_t(strLine);

    // if attachment
    } else if (strLine.RemovePrefix("Attachment:")) {
      // get attachment number
      INDEX iAtt = -1;
      strLine.ScanF("%d", &iAtt);
      if (iAtt < 0) {
        ThrowF_t("Invalid attachment number");
      }

      if (ctx.bRoot) {
        ParseModelConfig_t(pmo, strm, ctx);
        continue;
      }

      // create attachment
      CModelData *pmd = (CModelData*)pmo->GetData();
      if (iAtt >= pmd->md_aampAttachedPosition.Count()) {
        ThrowF_t("Attachment %d does not exist in that model", iAtt);
      };

      CAttachmentModelObject *pamo = pmo->AddAttachmentModel(iAtt);
      
      // recursively parse it
      ParseModelConfig_t(&pamo->amo_moModelObject, strm, ctx);
    } else {
      ThrowF_t("Expected texture or attachment");
    }
  }
}

extern BOOL SetModelFromConfig_t(CModelObject *pmo, CTStream &strm, CTString &strName, BOOL bPreview)
{
  SConfigParserContext ctx(strm.GetDescription());
  ctx.bPreview = bPreview;

  // try to
  try {
    // parse the file recursively starting at root model object and add everything
    ParseModelConfig_t(pmo, strm, ctx);
    return TRUE;

  // if anything failed then report error
  } catch (char *strError) {
    ThrowF_t("File '%s' (%d)\n  %s\n", ctx.strFile.ConstData(), ctx.ctLines, strError);
    return FALSE;
  }
}