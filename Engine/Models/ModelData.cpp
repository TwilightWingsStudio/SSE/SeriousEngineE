/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Models/ModelObject.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Models/Normals.h>
#include <Core/IO/Stream.h>
#include <Core/Base/CTString.inl>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

#include <Engine/Resources/ResourceManager.h>

extern BOOL _bHasAlpha;

// Default constructor sets invalid data
CModelData::CModelData()
{
  INDEX i;
  md_bPreparedForRendering = FALSE;

  md_VerticesCt = 0;                              // number of vertices in model
  md_FramesCt = 0;                                // number of all frames used by this model
  md_MipCt = 0;                                   // number of mip-models

  md_bIsEdited = FALSE; // not edited by default

  // Invalidate mip-model info data
  for (i = 0; i < MAX_MODELMIPS; i++)
  {
    md_MipInfos[i].mmpi_PolygonsCt = 0;
  }

  md_Flags = 0;                                   // model flags (flat, reflection mapping)
  md_ShadowQuality = 0;
  md_Stretch = FLOAT3D(1,1,1);                    // stretch vector (static one, dynamic one is in model object)
  md_bCollideAsCube = FALSE;                      // collide as sphere
  md_colDiffuse = C_WHITE|CT_OPAQUE;
  md_colReflections = C_WHITE|CT_OPAQUE;
  md_colSpecular = C_WHITE|CT_OPAQUE;
  md_colBump = C_WHITE|CT_OPAQUE;
}

// Clear all model data arrays
void CModelData::Clear(void)
{
  INDEX i;
  md_bPreparedForRendering = FALSE;

  CAnimData::Clear();

  md_FrameVertices16.Clear();
  md_FrameVertices8.Clear();
  md_FrameInfos.Clear();
  md_MainMipVertices.Clear();
  md_TransformedVertices.Clear();
  md_VertexMipMask.Clear();
  md_aampAttachedPosition.Clear();
  md_acbCollisionBox.Clear();

  for (i = 0; i < md_MipCt; i++)
  {
    md_MipInfos[i].Clear();
  }
  
  for (i = 0; i < MAX_COLOR_NAMES; i++)
  {
    md_ColorNames[i].Clear();
  }
  
  for (i = 0; i < MAX_TEXTUREPATCHES; i++)
  {
    md_mpPatches[i].mp_toTexture.SetData_t(CTString(""));
  }

  md_VerticesCt = 0;
  md_FramesCt = 0;
  md_MipCt = 0;
}

// Get amount of memory used by this object
SLONG CModelData::GetUsedMemory(void)
{
  SLONG slUsed = sizeof(*this) + CAnimData::GetUsedMemory() - sizeof(CAnimData);

  slUsed += md_FrameVertices16.Count() * sizeof(struct ModelFrameVertex16);
  slUsed += md_FrameVertices8.Count() * sizeof(struct ModelFrameVertex8);
  slUsed += md_FrameInfos.Count() * sizeof(struct ModelFrameInfo);
  slUsed += md_MainMipVertices.Count() * sizeof(FLOAT3D);
  slUsed += md_TransformedVertices.Count() * sizeof(struct TransformedVertexData);
  slUsed += md_VertexMipMask.Count() * sizeof(ULONG);
  slUsed += md_acbCollisionBox.Count() * sizeof(CModelCollisionBox);
  slUsed += md_aampAttachedPosition.Count() * sizeof(CAttachedModelPosition);

  for (INDEX i = 0; i < md_MipCt; i++)
  {
    slUsed += md_MipInfos[i].mmpi_aPolygonsPerPatch.Count() * sizeof(struct PolygonsPerPatch);
    slUsed += md_MipInfos[i].mmpi_Polygons.Count() * sizeof(struct ModelPolygon);
    slUsed += md_MipInfos[i].mmpi_TextureVertices.Count() * sizeof(struct ModelTextureVertex);
    slUsed += md_MipInfos[i].mmpi_MappingSurfaces.Count() * sizeof(struct MappingSurface);
  }

  return slUsed;
}

// Check if this kind of objects is auto-freed
BOOL CModelData::IsAutoFreed(void)
{
  return FALSE;
}

// TODO: Add comment here
void CModelData::ClearAnimations(void)
{
  CAnimData::Clear();

  md_FrameVertices16.Clear();
  md_FrameVertices8.Clear();
  md_FrameInfos.Clear();
  md_FramesCt = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Riches texture dimensions
void CModelData::GetTextureDimensions(MEX &mexWidth, MEX &mexHeight)
{
  mexWidth = md_Width;
  mexHeight = md_Height;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Calculates bounding box of all frames
void CModelData::GetAllFramesBBox(FLOATaabbox3D &MaxBB)
{
  for (INDEX i = 0; i< md_FramesCt; i++)
  {
    MaxBB |= md_FrameInfos[i].mfi_Box;
  }
}

// TODO: Add comment here
FLOAT3D CModelData::GetCollisionBoxMin(ULONG ulCollisionBox)
{
  md_acbCollisionBox.Lock();
  ulCollisionBox = ClampUp(ulCollisionBox, md_acbCollisionBox.Count() - 1);
  FLOAT3D vMin = md_acbCollisionBox[ulCollisionBox].mcb_vCollisionBoxMin;
  md_acbCollisionBox.Unlock();
  return vMin;
};

// TODO: Add comment here
FLOAT3D CModelData::GetCollisionBoxMax(ULONG ulCollisionBox = 0)
{
  md_acbCollisionBox.Lock();
  ulCollisionBox = ClampUp(ulCollisionBox, md_acbCollisionBox.Count() - 1);
  FLOAT3D vMax = md_acbCollisionBox[ulCollisionBox].mcb_vCollisionBoxMax;
  md_acbCollisionBox.Unlock();
  return vMax;
};

// Returns HEIGHT_EQ_WIDTH, LENGHT_EQ_WIDTH or LENGHT_EQ_HEIGHT
INDEX CModelData::GetCollisionBoxDimensionEquality(ULONG ulCollisionBox = 0)
{
  md_acbCollisionBox.Lock();
  ulCollisionBox = ClampUp(ulCollisionBox, md_acbCollisionBox.Count() - 1);
  INDEX iDimEq = md_acbCollisionBox[ulCollisionBox].mcb_iCollisionBoxDimensionEquality;
  md_acbCollisionBox.Unlock();
  return iDimEq;
};

// TODO: Add comment here
ULONG CModelData::GetFlags(void)
{
  return md_Flags;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Add comment here
void CModelData::SpreadMipSwitchFactors(INDEX iFirst, float fStartingFactor)
{
  // Set switch steep to cover all mips until max default range reached
  FLOAT fSteep;
  
  // If we allready skipped max range or we don't have any more mips
  if ((fStartingFactor > MAX_SWITCH_FACTOR) || ((md_MipCt-iFirst) <= 0)) {
    // Define next switch factor offset
    fSteep = 1.2f;
    
  // Else divide factor from starting factor to max switch factor with number of mip models left  
  } else {
    fSteep = (MAX_SWITCH_FACTOR - fStartingFactor) / (md_MipCt - iFirst);
  }

  // Spread mip switch factors for rougher mip models
  for (INDEX i = iFirst; i < md_MipCt; i++)
  {
    md_MipSwitchFactors[i] = fStartingFactor + (i - iFirst + 1) * fSteep;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Default destructor asserts invalid data and clears valid ones
CModelData::~CModelData()
{
  Clear();
}

// TODO: Add comment here
void CModelData::LinkDataForSurfaces(BOOL bFirstMip)
{
  INDEX iMipStart = 1;
  if (bFirstMip) {
    iMipStart = 0;
  }
  
  // For each mip model
  for (INDEX iMip = iMipStart; iMip < md_MipCt; iMip ++)
  {
    ModelMipInfo *pMMI = &md_MipInfos[iMip];

    // --------------------- Set index of transformed vertex to texture vertex
    {
      for (INDEX iPolygon = 0; iPolygon < pMMI->mmpi_Polygons.Count(); iPolygon++)
      {
        for (INDEX iVertex = 0; iVertex < pMMI->mmpi_Polygons[iPolygon].mp_PolygonVertices.Count(); iVertex++)
        {
          ModelPolygonVertex *pmpvPolygonVertex = &pMMI->mmpi_Polygons[iPolygon].mp_PolygonVertices[iVertex];
          INDEX iTransformed = md_TransformedVertices.Index(pmpvPolygonVertex->mpv_ptvTransformedVertex);
          pmpvPolygonVertex->mpv_ptvTextureVertex->mtv_iTransformedVertex = iTransformed;
        }
      }
    }

    // --------------------- Linking polygons for surface
    // Array telling how many polygons are in each surface
    TStaticArray<INDEX> actPolygonsInSurface;
    INDEX ctSurfaces = pMMI->mmpi_MappingSurfaces.Count();
    actPolygonsInSurface.New(ctSurfaces);
    
    // For each surface
    {
      for (INDEX iSurface = 0; iSurface < ctSurfaces; iSurface++)
      {
        // Reset count of polygons
        actPolygonsInSurface[iSurface] = 0;
      }
    }
    
    // For each polygon
    {
      for (INDEX iPolygon = 0; iPolygon < pMMI->mmpi_Polygons.Count(); iPolygon++)
      {
        // Increment count of polygons in its surface
        actPolygonsInSurface[pMMI->mmpi_Polygons[iPolygon].mp_Surface]++;
      }
    }
    
    // For each surface
    {
      for (INDEX iSurface = 0; iSurface < ctSurfaces; iSurface++)
      {
        // Allocate array for polygon indices
        pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons.Clear();
        pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons.New(actPolygonsInSurface[iSurface]);
        if (actPolygonsInSurface[iSurface] != 0) {
          // Last place in array will contain counter of added polygons
          pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons[actPolygonsInSurface[iSurface]-1] = 0;
        }
      }
    }
    
    // For each polygon
    for (INDEX iPolygon = 0; iPolygon < pMMI->mmpi_Polygons.Count(); iPolygon++)
    {
      // Get surface, polygons in surface and last remembered index of polygon in surface
      INDEX iSurface = pMMI->mmpi_Polygons[iPolygon].mp_Surface;
      INDEX ctPolygonsInSurface = actPolygonsInSurface[iSurface];
      if (ctPolygonsInSurface != 0) {
        INDEX iLastSet = pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons[ctPolygonsInSurface - 1];
        
        // Remember last set polygon index
        pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons[ctPolygonsInSurface - 1] = iLastSet + 1;
        
        // Remember index of polygon
        pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons[iLastSet] = iPolygon;
      }
    }

    // --------------------- Linking texture vertices for surface
    // For each surface
    {
      for (INDEX iSurface = 0; iSurface < ctSurfaces; iSurface++)
      {
        TDynamicContainer<ModelTextureVertex> cmtvInSurface;
        
        // For each polygon in surface
        FOREACHINSTATICARRAY(pMMI->mmpi_MappingSurfaces[iSurface].ms_aiPolygons, INDEX, itimpo)
        {
          ModelPolygon &mpPolygon = pMMI->mmpi_Polygons[itimpo.Current()];
          
          // For each vertex in polygon
          for (INDEX iVertex = 0; iVertex < mpPolygon.mp_PolygonVertices.Count(); iVertex++)
          {
            // Get texture vertex
            ModelTextureVertex *ptv =
              mpPolygon.mp_PolygonVertices[iVertex].mpv_ptvTextureVertex;
            
            // If it is not added yet
            if (!cmtvInSurface.IsMember(ptv)) {
              // Add it
              cmtvInSurface.Add(ptv);
            }
          }
        }
        
        // Add needed number of texture vertices
        pMMI->mmpi_MappingSurfaces[iSurface].ms_aiTextureVertices.Clear();
        pMMI->mmpi_MappingSurfaces[iSurface].ms_aiTextureVertices.New(cmtvInSurface.Count());
        INDEX cttv = 0;
        
        // For each texture vertex in container
        FOREACHINDYNAMICCONTAINER(cmtvInSurface, ModelTextureVertex, itmtv)
        {
          INDEX idxtv = pMMI->mmpi_TextureVertices.Index(itmtv);
          pMMI->mmpi_MappingSurfaces[iSurface].ms_aiTextureVertices[cttv] = idxtv;
          cttv++;
        }
      }
    }
  }
}

// Routine converts mpv_ptvTransformedVertex and mpv_ptvTextureVertex from ptrs to Indices
void CModelData::PtrsToIndices()
{
  INDEX i, j;

  for (i = 0; i < md_MipCt; i++)
  {
    FOREACHINSTATICARRAY(md_MipInfos[i].mmpi_Polygons, ModelPolygon, it1)
    {
      FOREACHINSTATICARRAY(it1.Current().mp_PolygonVertices, ModelPolygonVertex, it2)
      {
        for (j = 0; j < md_TransformedVertices.Count(); j++)
        {
          if (it2.Current().mpv_ptvTransformedVertex == &md_TransformedVertices[j]) {
            break;
          }
        }
        it2.Current().mpv_ptvTransformedVertex = (struct TransformedVertexData*)j;

        for (j = 0; j < md_MipInfos[i].mmpi_TextureVertices.Count(); j++)
        {
          if (it2.Current().mpv_ptvTextureVertex == &md_MipInfos[i].mmpi_TextureVertices[j]) {
            break;
          }
        }
        it2.Current().mpv_ptvTextureVertex = (ModelTextureVertex*)j;
      }
    }
  }
}

// Routine converts mpv_ptvTransformedVertex and mpv_ptvTextureVertex from Indices to ptrs
void CModelData::IndicesToPtrs()
{
  INDEX i, j;

  for (i = 0; i < md_MipCt; i++)
  {
    FOREACHINSTATICARRAY(md_MipInfos[i].mmpi_Polygons, ModelPolygon, it1)
    {
      FOREACHINSTATICARRAY(it1.Current().mp_PolygonVertices, ModelPolygonVertex, it2)
      {
        struct ModelPolygonVertex * pMPV = &it2.Current();
        
        j = (INDEX) it2.Current().mpv_ptvTransformedVertex;
        it2.Current().mpv_ptvTransformedVertex = &md_TransformedVertices[j];
        
        j = (INDEX) it2.Current().mpv_ptvTextureVertex;
        it2.Current().mpv_ptvTextureVertex = &md_MipInfos[i].mmpi_TextureVertices[j];
      }
    }
  }
}

// Reference counting (override from CAnimData)
void CModelData::RemReference_internal(void)
{
  // If this model is part of edit model object
  if (md_bIsEdited) {
    Unreference(); // just unreference it
    
  // if this model is part of model stock
  } else {
    _pResourceMgr->Release(this);  // release it
  }
}

// Get the description of this object.
CTString CModelData::GetDescription(void)
{
  CTString str;

  ModelMipInfo &mmi0 = md_MipInfos[0];
  str.PrintF("%d mips, %d anims, %d frames, %d vtx, %d svx, %d tri",
             md_MipCt, ad_NumberOfAnims, md_FramesCt,
             mmi0.mmpi_ctMipVx, mmi0.mmpi_ctSrfVx, mmi0.mmpi_ctTriangles);

  return str;
}

UBYTE CModelData::GetType() const
{
  return TYPE_MODEL;
}