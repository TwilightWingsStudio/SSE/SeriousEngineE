/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MODELOBJECT_H
#define SE_INCL_MODELOBJECT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Engine/Meshes/AbstractModelInstance.h>
#include <Engine/Models/Model.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Placement.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Models/Model_internal.h>

class CAttachmentModelObject;
class CRenderModel;
class CModelInfo;

/*
 * TODO: Add comment here
 */
class ENGINE_API CModelObject : public CAbstractModelInstance, public CChangeable, public IAnimated
{
  private:
    ULONG mo_PatchMask;                              // used to turn on/off texture patches (i.e. blood patches)
    INDEX mo_iManualMipLevel;
    BOOL  mo_AutoMipModeling;

    ////////////////////////////////////////
    // API version
      
    // TODO: Add comment here
    void RenderModel_View(CRenderModel &rm);
      
    // TODO: Add comment here
    void RenderPatches_View(CRenderModel &rm);
      
    // TODO: Add comment here
    void AddSimpleShadow_View(CRenderModel &rm, const FLOAT fIntensity, const FLOATplane3D &plShadowPlane);
      
    // TODO: Add comment here
    void RenderShadow_View(CRenderModel &rm, const CPlacement3D &plLight,
                            const FLOAT fFallOff, const FLOAT fHotSpot, const FLOAT fIntensity,
                            const FLOATplane3D &plShadowPlane);

                            
    // Software version for drawing model mask
    void RenderModel_Mask(CRenderModel &rm);

  public:
    CModelData *mo_pmdModelData;
    CTextureObject mo_toTexture;                      // texture used for model rendering
    CTextureObject mo_toReflection;                   // texture used for reflection
    CTextureObject mo_toSpecular;                     // texture used for specularity
    CTextureObject mo_toBump;                         // texture used for bump
    FLOAT3D mo_Stretch;                               // dynamic stretching vector, (usually 1,1,1)
    ULONG mo_ColorMask;                               // mask telling what parts (colors) are visible
    INDEX mo_iLastRenderMipLevel;                     // last rendered mip model index remembered
    COLOR mo_colBlendColor;                           // dynamic blend color (alpha is applied)
    CListHead mo_lhAttachments;                       // list of currently active attachment models
    CAnimObject mo_aoAnimated;

    // Default constructor
    CModelObject(void);
    
    // Destructor
    ~CModelObject(void);   
    
    //! Copy data from another instance of same class.
    virtual void Copy(CAbstractModelInstance &mi);
    
    
    // Retrieves model's texture width
    MEX GetWidth(); 
    
    // Retrieves model's texture height
    MEX GetHeight();
    
    // Retrieves full model info
    void GetModelInfo(CModelInfo &miInfo);
    
    // Is model visible for given mip factor
    BOOL IsModelVisible(float fMipFactor);
    
    // Retrieves current mip model index
    INDEX GetMipModel(float fMipFactor);
    
    // Test if model has shadow at given mip level
    BOOL HasShadow(INDEX iModelMip);
    
    // Retrieves bounding box of given frame
    FLOATaabbox3D GetFrameBBox(INDEX iFrameNo);
    
    // TRUE if auto mip modeling is on
    BOOL IsAutoMipModeling();
    
    // Function starts auto mip modeling
    void AutoMipModelingOn();
    
    // Function stops auto mip modeling
    void AutoMipModelingOff();
    
    // Retrieves current mip level
    INDEX GetManualMipLevel(void);
    
    // Sets given mip-level as current (auto mip modeling off)
    void SetManualMipLevel(INDEX iNewMipLevel);
    
    
    // Sets previous mip-level (more precize)
    void PrevManualMipLevel();
    
    // Sets next mip-level (more rough)
    void NextManualMipLevel();
    
    // Sets given mip-level's new switch factor
    void SetMipSwitchFactor(INDEX iMipLevel, float fMipFactor);
    
    // Retrieves current frame's bounding box
    void GetCurrentFrameBBox(FLOATaabbox3D &MaxBB);
    
    // Retrieves bounding box of all frames
    void GetAllFramesBBox(FLOATaabbox3D &MaxBB);
    
    // TODO: Add comment here
    FLOAT3D GetCollisionBoxMin(INDEX iCollisionBox);
    
    // TODO: Add comment here
    FLOAT3D GetCollisionBoxMax(INDEX iCollisionBox);
    
    // Test it the model has alpha blending
    BOOL HasAlpha(void);
    
    // Returns HEIGHT_EQ_WIDTH, LENGTH_EQ_WIDTH or LENGTH_EQ_HEIGHT
    INDEX GetCollisionBoxDimensionEquality(INDEX iCollisionBox);
    
    // Retrieves number of surfaces used in given mip model
    INDEX SurfacesCt(INDEX iMipModel);
    
    // Retrieves number of polygons in given surface in given mip model
    INDEX PolygonsInSurfaceCt(INDEX iMipModel, INDEX iSurface);
    
    // Retrieves color of given surface
    COLOR GetSurfaceColor(INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Changes color of given surface
    void SetSurfaceColor(INDEX iCurrentMip, INDEX iSurface, COLOR colNewColorAndAlpha);

    // TODO: Add comment here
    void GetSurfaceRenderFlags(INDEX iCurrentMip, INDEX iCurrentSurface,
                               enum SurfaceShadingType &sstShading, enum SurfaceTranslucencyType &sttTranslucency,
                               ULONG &ulRenderingFlags);

    // TODO: Add comment here                           
    void SetSurfaceRenderFlags(INDEX iCurrentMip, INDEX iCurrentSurface,
                               enum SurfaceShadingType sstShading, enum SurfaceTranslucencyType sttTranslucency,
                               ULONG ulRenderingFlags);

    // Returns index and position of closest patch
    INDEX GetClosestPatch(MEX2D mexWanted, MEX2D &mexFound);
    
    // Retrieves name of color with given index
    CTString GetColorName(INDEX iColor);
    
    // Sets new color name
    void SetColorName(INDEX iColor, CTString &strNewName);

    
    // This function returns current value of patches mask
    ULONG GetPatchesMask();
    
    // Use this function to set new patches combination
    void  SetPatchesMask(ULONG new_patches_mask);


    // TODO: Add comment here
    void UnpackVertex(CRenderModel &rm, const INDEX iVertex, FLOAT3D &vVertex);
    
    // TODO: Add comment here
    BOOL CreateAttachment(CRenderModel &rmMain, CAttachmentModelObject &amo);
    
    // TODO: Add comment here
    void ResetAttachmentModelPosition(INDEX iAttachedPosition);
    
    // TODO: Add comment here
    CAttachmentModelObject *GetAttachmentModelList(INDEX iAttachedPosition, ...);
    
    // TODO: Add comment here
    CAttachmentModelObject *GetAttachmentModel(INDEX iAttachedPosition);
    
    // TODO: Add comment here
    CAttachmentModelObject *AddAttachmentModel(INDEX iAttachedPosition);
    
    // TODO: Add comment here
    void RemoveAttachmentModel(INDEX iAttachedPosition);
    
    // TODO: Add comment here
    void RemoveAllAttachmentModels(void);
    
    //! Stretch model instance.
    void StretchModel(const FLOAT3D &vStretch);
    
    // Stretches model relative to current size
    void StretchModelRelative(const FLOAT3D &vStretch);
    
    //! Stretch model instance without attachments.
    void StretchSingleModel(const FLOAT3D &vStretch);

    // Model rendering
    void SetupModelRendering(CRenderModel &rm);
    
    // TODO: Add comment here
    void RenderModel(CRenderModel &rm);
    
    // TODO: Add comment here
    void RenderPatches(CRenderModel &rm);
    
    // TODO: Add comment here
    void AddSimpleShadow(CRenderModel &rm, const FLOAT fIntensity,  const FLOATplane3D &plShadowPlane);
    
    // TODO: Add comment here
    void RenderShadow(CRenderModel &rm, const CPlacement3D &plLight,
                      const FLOAT fFallOff, const FLOAT fHotSpot, const FLOAT fIntensity,
                      const FLOATplane3D &plShadowPlane);
    
    // Get model vertices in absoulte space
    void GetModelVertices(TStaticStackArray<FLOAT3D> &avVertices, FLOATmatrix3D &mRotation,
                          FLOAT3D &vPosition, FLOAT fNormalOffset, FLOAT fMipFactor);
    
    // TODO: Add comment here
    void GetAttachmentMatrices(CAttachmentModelObject *pamo, FLOATmatrix3D &mRotation, FLOAT3D &vPosition);
    
    // TODO: Add comment here
    void GetAttachmentTransformations(INDEX iAttachment, FLOATmatrix3D &mRotation, FLOAT3D &vPosition,
                                      BOOL bDummyAttachment);
    
    // TODO: Add comment here
    void ProjectFrameVertices(CProjection3D *pProjection, INDEX iMipModel);
    
    // Colorizes polygons touching given box
    void ColorizeRegion(CDrawPort *pDP, CProjection3D *projection, PIXaabbox2D box, INDEX iChoosedColor,
                        BOOL bOnColorMode);
                        
    // Colorizes hitted polygon
    void ColorizePolygon(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1, INDEX iChoosedColor,
                         BOOL bOnColorMode);
    
    // TODO: Add comment here                 
    void ApplySurfaceToPolygon(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1,
                               INDEX iSurface, COLOR colSurfaceColor);
    
    // TODO: Add comment here
    void ApplySurfaceToPolygonsInRegion(CDrawPort *pDP, CProjection3D *projection,
                                        PIXaabbox2D box, INDEX iSurface, COLOR colSurfaceColor);
    
    // TODO: Add comment here
    void UnpackVertex(INDEX iFrame, INDEX iVertex, FLOAT3D &vVertex);
    
    // TODO: Add comment here
    CPlacement3D GetAttachmentPlacement(CAttachmentModelObject &amo);
    
    // TODO: Add comment here
    struct ModelPolygon *PolygonHit(CPlacement3D plRay, CPlacement3D plObject,
                                    INDEX iCurrentMip, FLOAT &fHitDistance); // returns ptr to hitted polygon (NULL if none)
    
    // TODO: Add comment here
    struct ModelPolygon *PolygonHitModelData(CModelData *pMD, CPlacement3D plRay, CPlacement3D plObject,
                                             INDEX iCurrentMip, FLOAT &fHitDistance);
                                             
    // Picks color from hitted polygon
    void PickPolyColor(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1,
                       INDEX &iPickedColorNo, BOOL bOnColorMode);
                       
    // Oicks surface from hitted polygon
    INDEX PickPolySurface(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1);
    
    // Obtains index of closest vertex
    INDEX PickVertexIndex(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1, FLOAT3D &vClosestVertex);
    
    
    // TODO: Add comment here
    void ShowPatch(INDEX iMaskBit);
    
    // TODO: Add comment here
    void HidePatch(INDEX iMaskBit);
    
    // TODO: Add comment here
    void Read_t(CTStream *istrFile);   // throw char *
    
    // TODO: Add comment here
    void Write_t(CTStream *ostrFile);  // throw char *

    
    // Set texture data for main texture in surface of this model
    void SetTextureData(CTextureData *ptdNewMainTexture);
    
    // TODO: Add comment here
    CTFileName GetName(void);
    
    // TODO: Add comment here
    void AutoSetTextures(void);
    
    // TODO: Add comment here
    void AutoSetAttachments(void);
    
    // Obtain model and set it for this object
    void SetData_t(const CTFileName &fnmModel); // throw char *
    
    // TODO: Add comment here
    void SetData(CModelData *pmd);
    
    // TODO: Add comment here
    CModelData *GetData(void);

    
    // Synchronize with another model (copy animations/attachments positions etc from there)
    void Synchronize(CModelObject &moOther);

    // Get amount of memory used by this object
    SLONG GetUsedMemory(void);

    //! Returns instance type.
    virtual ModelInstanceType GetInstanceType() const
    {
      return MIT_VERTEX;
    }

    //! Get pointer to animation object.
    virtual CAnimObject *GetAnimObject()
    {
      return &mo_aoAnimated;
    }
  public:
    virtual IAnimated *GetAnimated();
    virtual const IAnimated *GetAnimatedConst() const;

  // IAnimated
  public:
    //! Loop anims forward.
    virtual void NextAnim(void);

    //! Loop anims backward.
    virtual void PrevAnim(void);

    //! Get animation's info.
    virtual void GetAnimInfo(INDEX iAnimNo, CAnimInfo &aiInfo) const;

    //! Get animation's length.
    virtual FLOAT GetCurrentAnimLength(void) const;
    
    //! Get animation's length.
    virtual FLOAT GetAnimLength(INDEX iAnim) const;

    //! Get number of animations in current anim data.
    virtual INDEX GetAnimsCt() const;

    //! If animation has finished.
    virtual BOOL IsAnimFinished(void) const;

    //! Get time when current anim was started.
    virtual TIME GetStartTime(void) const;

    //! Get passed time from start of animation.
    virtual TIME GetPassedTime(void) const;

    //! Start new animation -- obsolete.
    virtual void StartAnim(INDEX iNew);

    //! Start playing an animation.
    virtual void PlayAnim(INDEX iNew, ULONG ulFlags);

    //! Seamlessly continue playing another animation from same point.
    //! NOTE: Never called.
    virtual void SwitchToAnim(INDEX iNew);

    //! Set new animation but doesn't starts it.
    virtual void SetAnim(INDEX iNew);

    //! Reset anim (restart).
    virtual void ResetAnim();

    //! Pauses current animation.
    virtual void PauseAnim();

    //! Continues paused animation.
    virtual void ContinueAnim();

    //! Offsets the animation phase.
    //! NOTE: Never called.
    virtual void OffsetPhase(TIME tm);

    //! Retrieves paused flag.
    virtual BOOL IsPaused(void);

    //! Gets the number of current animation.
    virtual INDEX GetAnim(void) const;

    //! Gets the number of last animation.
    virtual INDEX GetLastAnim(void) const;

    //! Loop frames forward.
    virtual void NextFrame(void);

    //! Loop frames backward.
    virtual void PrevFrame(void);

    //! Select first frame.
    virtual void FirstFrame(void);

    //! Select last frame.
    virtual void LastFrame(void);

    //! Gets the number of current frame.
    virtual INDEX GetFrame(void) const;

    //! Gets number of frames in current anim.
    //! NOTE: Never called.
    virtual INDEX GetFramesInCurrentAnim(void) const;

    //! Get information for linear interpolation beetween frames.
    virtual void GetFrame(INDEX &iFrame0, INDEX &iFrame1, FLOAT &fRatio) const;
};

/*
 * TODO: Add comment here
 */
class ENGINE_API CAttachmentModelObject
{
  public:
    CListNode amo_lnInMain;
    INDEX amo_iAttachedPosition;      // indentifier of positions saved in model data
    CPlacement3D amo_plRelative;      // relative placement used for rendering
    CModelObject amo_moModelObject;   // model and texture
    CRenderModel *amo_prm;            // render model structure used in rendering
};


#endif  /* include-once check. */