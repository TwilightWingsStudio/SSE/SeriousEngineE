/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/Base/CTString.inl>

#include <Engine/Models/ModelObject.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/RenderModel.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Models/Normals.h>
#include <Core/Graphics/Color.h>
#include <Engine/Graphics/DrawPort.h>
#include <Core/Math/Clipping.inl>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

#include <Engine/Resources/ResourceManager.h>

template TStaticArray<MappingSurface>;
template TStaticArray<ModelPolygon>;
template TStaticArray<ModelPolygonVertex>;
template TStaticArray<ModelTextureVertex>;
template TStaticArray<PolygonsPerPatch>;
template TDynamicArray<CAttachedModelPosition>;

// Model LOD biasing control
extern FLOAT mdl_fLODMul;
extern FLOAT mdl_fLODAdd;
extern INDEX mdl_iLODDisappear; // 0=never, 1=ignore bias, 2=with bias
extern INDEX mdl_bFineQuality;  // 0=force to 8-bit, 1=optimal

CModelData::CModelData(const CModelData &c) { ASSERT(FALSE); };
CModelData &CModelData::operator=(const CModelData &c){ ASSERT(FALSE); return *this; }; 

// If any surface in model that we are currently reading has any transparency
BOOL _bHasAlpha;

// Colors used to represent on and off bits
COLOR PaletteColorValues[] =
{
  C_RED, C_GREEN, C_BLUE, C_CYAN,
  C_MAGENTA, C_YELLOW, C_ORANGE, C_BROWN,
  C_PINK, C_dGRAY, C_GRAY, C_lGRAY,
  C_dRED, C_lRED, C_dGREEN, C_lGREEN,
  C_dBLUE, C_lBLUE, C_dCYAN, C_lCYAN,
  C_dMAGENTA, C_lMAGENTA, C_dYELLOW, C_lYELLOW,
  C_dORANGE, C_lORANGE, C_dBROWN, C_lBROWN,
  C_dPINK, C_lPINK, C_WHITE, C_BLACK,
};

// Instanciated global rendering preferences object containing info about rendering of models
CModelRenderPrefs _mrpModelRenderPrefs;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Default constructor sets default rendering preferences
CModelRenderPrefs::CModelRenderPrefs()
{
  rp_BBoxFrameVisible = FALSE;
  rp_BBoxAllVisible = FALSE;
  rp_InkColor = C_BLACK;
  rp_PaperColor = C_lGRAY;
  rp_RenderType = RT_TEXTURE | RT_SHADING_PHONG;
  rp_ShadowQuality = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Routines managing (get/set) rendering preferences

// TODO: Add comment here
void CModelRenderPrefs::SetRenderType(ULONG rtNew)
{
  rp_RenderType = rtNew;
}

// TODO: Add comment here
void CModelRenderPrefs::SetTextureType(ULONG rtNew)
{
  rp_RenderType = (rp_RenderType & (~RT_TEXTURE_MASK)) | rtNew;
}

// TODO: Add comment here
void CModelRenderPrefs::SetShadingType(ULONG rtNew)
{
  rp_RenderType = (rp_RenderType & (~RT_SHADING_MASK)) | rtNew;
}

// TODO: Add comment here
void CModelRenderPrefs::SetWire(BOOL bWireOn)
{
  if (bWireOn) {
    rp_RenderType = rp_RenderType | RT_WIRE_ON;
  } else {
    rp_RenderType = rp_RenderType & (~RT_WIRE_ON);
  }
}

// TODO: Add comment here
void CModelRenderPrefs::SetHiddenLines(BOOL bHiddenLinesOn)
{
  if (bHiddenLinesOn) {
    rp_RenderType = rp_RenderType | RT_HIDDEN_LINES;
  } else {
    rp_RenderType = rp_RenderType & (~RT_HIDDEN_LINES);
  }
}

// TODO: Add comment here
ULONG CModelRenderPrefs::GetRenderType()
{
  return(rp_RenderType);
}

// TODO: Add comment here
void CModelRenderPrefs::SetShadowQuality(INDEX iNewQuality)
{
  ASSERT(iNewQuality >= 0);
  rp_ShadowQuality = iNewQuality;
}

// TODO: Add comment here
void CModelRenderPrefs::DesreaseShadowQuality(void)
{
  rp_ShadowQuality += 1;
}

// TODO: Add comment here
void CModelRenderPrefs::IncreaseShadowQuality(void)
{
  if (rp_ShadowQuality > 0) {
    rp_ShadowQuality -= 1;
  }
}

// TODO: Add comment here
INDEX CModelRenderPrefs::GetShadowQuality()
{
  return(rp_ShadowQuality);
}

// TODO: Add comment here
BOOL CModelRenderPrefs::BBoxFrameVisible()
{
  return(rp_BBoxFrameVisible);
}

// TODO: Add comment here
void CModelRenderPrefs::BBoxFrameShow(BOOL bShow)
{
  rp_BBoxFrameVisible = bShow;
}

// TODO: Add comment here
BOOL CModelRenderPrefs::BBoxAllVisible()
{
  return(rp_BBoxAllVisible);
}

// TODO: Add comment here
void CModelRenderPrefs::BBoxAllShow(BOOL bShow)
{
  rp_BBoxAllVisible = bShow;
}

// TODO: Add comment here
BOOL CModelRenderPrefs::WireOn()
{
  return((rp_RenderType & RT_WIRE_ON) != 0);
}

// TODO: Add comment here
BOOL CModelRenderPrefs::HiddenLines()
{
  return((rp_RenderType & RT_HIDDEN_LINES) != 0);
}

// TODO: Add comment here
void CModelRenderPrefs::SetInkColor(COLOR clrNew)
{
  rp_InkColor = clrNew;
}

// TODO: Add comment here
COLOR CModelRenderPrefs::GetInkColor()
{
  return rp_InkColor;
}

// TODO: Add comment here
void CModelRenderPrefs::SetPaperColor(COLOR clrNew)
{
  rp_PaperColor = clrNew;
}

// TODO: Add comment here
COLOR CModelRenderPrefs::GetPaperColor()
{
  return rp_PaperColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Read and write functions

// TODO: Add comment here
void CModelRenderPrefs::Read_t(CTStream *istrFile) 
{
}

// TODO: Add comment here
void CModelRenderPrefs::Write_t(CTStream *ostrFile)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Add comment here
CModelPatch::CModelPatch(void)
{
  mp_strName = "";
  mp_mexPosition = MEX2D(1024, 1024);
  mp_fStretch = 1.0f;
}

// TODO: Add comment here
void CModelPatch::Read_t(CTStream *strFile)
{
  *strFile >> mp_strName;
  CTFileName fnPatchTexture;
  *strFile >> fnPatchTexture;
  try {
    mp_toTexture.SetData_t(fnPatchTexture);
  } catch(char *strError) {
    (void) strError;
  }
  *strFile >> mp_mexPosition;
  *strFile >> mp_fStretch;
}

void CModelPatch::Write_t(CTStream *strFile)
{
  *strFile << mp_strName;
  *strFile << mp_toTexture.GetName();
  *strFile << mp_mexPosition;
  *strFile << mp_fStretch;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Add comment here
ModelTextureVertex::ModelTextureVertex(void)
{
  mtv_iTransformedVertex = 0;
  mtv_Done = FALSE;
}

// Wtite
void ModelPolygonVertex::Write_t(CTStream *pFile) 
{
  (*pFile) << (INDEX) mpv_ptvTransformedVertex;
  (*pFile) << (INDEX) mpv_ptvTextureVertex;
}

// Read
void ModelPolygonVertex::Read_t(CTStream *pFile)
{
  INDEX itmp;

  (*pFile) >> itmp;
  mpv_ptvTransformedVertex = (struct TransformedVertexData*)itmp;
  
  (*pFile) >> itmp;
  mpv_ptvTextureVertex = (ModelTextureVertex*)itmp;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Write
void ModelPolygon::Write_t(CTStream *pFile)
{
  pFile->WriteID_t(CChunkID("MDP2"));
  INDEX ctVertices = mp_PolygonVertices.Count();
  (*pFile) << ctVertices;
  
  {
    FOREACHINSTATICARRAY(mp_PolygonVertices, ModelPolygonVertex, it)
    { 
      it.Current().Write_t(pFile);
    }
  }
  
  (*pFile) << mp_RenderFlags;
  (*pFile) << mp_ColorAndAlpha;
  (*pFile) << mp_Surface;
};

// Read
void ModelPolygon::Read_t(CTStream *pFile) 
{
  INDEX ctVertices;
  ULONG ulDummy;
  if (pFile->PeekID_t() == CChunkID("MDPL"))
  {
    pFile->ExpectID_t(CChunkID("MDPL"));
    pFile->ReadFullChunk_t(CChunkID("IMPV"), &ctVertices, sizeof(INDEX));
    mp_PolygonVertices.New(ctVertices);

    {
      FOREACHINSTATICARRAY(mp_PolygonVertices, ModelPolygonVertex, it)
      {
        it.Current().Read_t(pFile);
      }
    }
    
    (*pFile) >> mp_RenderFlags;
    (*pFile) >> mp_ColorAndAlpha;
    (*pFile) >> mp_Surface;
    (*pFile) >> ulDummy;  // ex on color
    (*pFile) >> ulDummy;  // ex off color
  } else {
    pFile->ExpectID_t(CChunkID("MDP2"));
    (*pFile) >> ctVertices;
    mp_PolygonVertices.New(ctVertices);

    {
      FOREACHINSTATICARRAY(mp_PolygonVertices, ModelPolygonVertex, it)
      {
        it.Current().Read_t(pFile);
      }
     }
     
    (*pFile) >> mp_RenderFlags;
    (*pFile) >> mp_ColorAndAlpha;
    (*pFile) >> mp_Surface;
  }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FIXME:
// TEMPORARY - REMOVE THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// POLYGON RENDER CONSTANTS
// new_render_types = rendertype (0-7)<<0 + phongstrength(0-7)<<3 + alphatype(0-3)<<6
// poly render types
#define PR_PHONGSHADING     (0x00<<0)     // textures phong shading
#define PR_LAMBERTSHADING   (0x01<<0)     // textures lambert shading (no phong/gouraud)
#define PR_ALPHAGOURAUD     (0x02<<0)     // alpha gouraud shading
#define PR_FRONTPROJECTION  (0x03<<0)     // front projection shading (no rotation, 2D zoom)
#define PR_SHADOWBLENDING   (0x04<<0)     // shadow blending (black alpha gouraud)
#define PR_COLORFILLING     (0x05<<0)     // flat filling (single color poly, no texture)

#define PR_MASK             (0x07<<0)     // mask for render types

// Phong strengths (shading types)
#define PS_MATTE  (0x00<<3)     // same as the gouraud
#define PS_SHINY  (0x01<<3)     // mild shininig
#define PS_METAL  (0x02<<3)     // shine as hell

#define PS_MASK   (0x07<<3)     // mask for phong strengths

// Texture's alpha channel flag
#define AC_SKIPALPHACHANNEL (0x00<<6) // opaque rendering regardless of alpha channel presence
#define AC_USEALPHACHANNEL  (0x01<<6) // texture's alpha ch. will be taken in consideration
#define AC_ZEROTRANSPARENCY (0x02<<6) // texture's zero values are transparent (no alpha ch.)

#define AC_MASK             (0x03<<6) // mask for texture's alpha flag

// Polygon's control flags
#define PCF_DOUBLESIDED (0x01<<9)     // double sided polygon
#define PCF_NOSHADING   (0x02<<9)     // this polygon will not be shaded anyhow (?)
#define PCF_CLIPPOLYGON (0x04<<9)     // polygon is clipped, instead rejected
#define PCF_REFLECTIONS (0x08<<9)     // use reflection mapping

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Add comment here
BOOL MappingSurface::operator==(const MappingSurface &msOther) const {
  return msOther.ms_Name == ms_Name;
};

// Convert old polygon flags from CTGfx into new rendering parameters
void MappingSurface::SetRenderingParameters(ULONG ulOldFlags)
{
  // Find rendering type
  if (ulOldFlags & PCF_NOSHADING) {
    ms_sstShadingType = SST_FULLBRIGHT;
  } else {
    switch (ulOldFlags & PS_MASK)
    {
      case PS_MATTE: {
        ms_sstShadingType = SST_MATTE;
      } break;
      
      case PS_SHINY: {
        ms_sstShadingType = SST_MATTE;
      } break;
      
      case PS_METAL: {
        ms_sstShadingType = SST_MATTE;
      } break;
      
      default: {
        ms_sstShadingType = SST_MATTE;
      }
    }
  }
  
  // Find translucency type
  if ((ulOldFlags & PR_MASK) == PR_ALPHAGOURAUD) {
    ms_sttTranslucencyType = STT_ALPHAGOURAUD;
  } else if ((ulOldFlags & AC_MASK) == AC_ZEROTRANSPARENCY) {
    ms_sttTranslucencyType = STT_TRANSLUCENT;
  } else {
    ms_sttTranslucencyType = STT_OPAQUE;
  }
  
  // Find flags
  ms_ulRenderingFlags = 0;
  if (ulOldFlags & PCF_DOUBLESIDED) {
    ms_ulRenderingFlags |= SRF_DOUBLESIDED;
  }
  
  if (ulOldFlags & PCF_REFLECTIONS) {
    ms_ulRenderingFlags |= SRF_REFLECTIONS;
  }
}

// Write
void MappingSurface::Write_t(CTStream *pFile) 
{
  (*pFile) << ms_Name;
  pFile->Write_t(&ms_vSurface2DOffset, sizeof(FLOAT3D));
  pFile->Write_t(&ms_HPB, sizeof(FLOAT3D));
  pFile->Write_t(&ms_Zoom, sizeof(float));

  pFile->Write_t(&ms_sstShadingType, sizeof(SurfaceShadingType));
  pFile->Write_t(&ms_sttTranslucencyType, sizeof(SurfaceTranslucencyType));
  (*pFile) << ms_ulRenderingFlags;

  INDEX ctPolygons = ms_aiPolygons.Count();
  (*pFile) << ctPolygons;

  if (ctPolygons != 0) {
    pFile->Write_t(&ms_aiPolygons[0], sizeof(INDEX) * ctPolygons);
  }

  INDEX ctTextureVertices = ms_aiTextureVertices.Count();
  (*pFile) << ctTextureVertices;

  if (ctTextureVertices != 0) {
    pFile->Write_t(&ms_aiTextureVertices[0], sizeof(INDEX) * ctTextureVertices);
  }

  (*pFile) << ms_colColor;

  (*pFile) << ms_colDiffuse;
  (*pFile) << ms_colReflections;
  (*pFile) << ms_colSpecular;
  (*pFile) << ms_colBump;
  
  (*pFile) << ms_ulOnColor;
  (*pFile) << ms_ulOffColor;
}

// Write settings
void MappingSurface::WriteSettings_t(CTStream *pFile) 
{
  (*pFile) << ms_Name;
  pFile->Write_t(&ms_sstShadingType, sizeof(SurfaceShadingType));
  pFile->Write_t(&ms_sttTranslucencyType, sizeof(SurfaceTranslucencyType));
  (*pFile) << ms_ulRenderingFlags;
  (*pFile) << ms_colDiffuse;
  (*pFile) << ms_colReflections;
  (*pFile) << ms_colSpecular;
  (*pFile) << ms_colBump;
  (*pFile) << ms_ulOnColor;
  (*pFile) << ms_ulOffColor;
}

// Read settings
void MappingSurface::ReadSettings_t(CTStream *pFile) 
{
  (*pFile) >> ms_Name;
  pFile->Read_t(&ms_sstShadingType, sizeof(SurfaceShadingType));
  pFile->Read_t(&ms_sttTranslucencyType, sizeof(SurfaceTranslucencyType));
  (*pFile) >> ms_ulRenderingFlags;
  (*pFile) >> ms_colDiffuse;
  (*pFile) >> ms_colReflections;
  (*pFile) >> ms_colSpecular;
  (*pFile) >> ms_colBump;
  (*pFile) >> ms_ulOnColor;
  (*pFile) >> ms_ulOffColor;
}

// Read
void MappingSurface::Read_t(CTStream *pFile, BOOL bReadPolygonsPerSurface, BOOL bReadSurfaceColors)
{
  (*pFile) >> ms_Name;
  pFile->Read_t(&ms_vSurface2DOffset, sizeof(FLOAT3D));
  pFile->Read_t(&ms_HPB, sizeof(FLOAT3D));
  pFile->Read_t(&ms_Zoom, sizeof(float));

  if (bReadPolygonsPerSurface)
  {
    pFile->Read_t(&ms_sstShadingType, sizeof(SurfaceShadingType));
    // WARNING !!! All shading types bigger than matte will be remaped into flat shading
    // this was done when SHINY and METAL were removed
    if (ms_sstShadingType > SST_MATTE) {
      ms_sstShadingType = SST_FLAT;
    }
    pFile->Read_t(&ms_sttTranslucencyType, sizeof(SurfaceTranslucencyType));
    (*pFile) >> ms_ulRenderingFlags;
    if ((ms_ulRenderingFlags & SRF_NEW_TEXTURE_FORMAT) == 0) {
      ms_ulRenderingFlags |= SRF_DIFFUSE | SRF_NEW_TEXTURE_FORMAT;
    }

    if (ms_sttTranslucencyType == STT_TRANSLUCENT || ms_sttTranslucencyType == STT_ALPHAGOURAUD || 
        ms_sttTranslucencyType == STT_ADD || ms_sttTranslucencyType == STT_MULTIPLY)
    {
      _bHasAlpha = TRUE;
    }

    ms_aiPolygons.Clear();
    INDEX ctPolygons;
    (*pFile) >> ctPolygons;
    ms_aiPolygons.New(ctPolygons);
    if (ctPolygons != 0) {
      pFile->Read_t(&ms_aiPolygons[0], sizeof(INDEX) * ctPolygons);
    }

    ms_aiTextureVertices.Clear();
    INDEX ctTextureVertices;
    (*pFile) >> ctTextureVertices;
    ms_aiTextureVertices.New(ctTextureVertices);
    if (ctTextureVertices != 0) {
      pFile->Read_t(&ms_aiTextureVertices[0], sizeof(INDEX) * ctTextureVertices);
    }

    (*pFile) >> ms_colColor;
  }
  
  if (bReadSurfaceColors)
  {
    (*pFile) >> ms_colDiffuse;
    (*pFile) >> ms_colReflections;
    (*pFile) >> ms_colSpecular;
    (*pFile) >> ms_colBump;
    (*pFile) >> ms_ulOnColor;
    (*pFile) >> ms_ulOffColor;
  }
}



// Default constructor
ModelMipInfo::ModelMipInfo(void)
{
  mmpi_ulFlags = MM_PATCHES_VISIBLE | MM_ATTACHED_MODELS_VISIBLE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This is write function of one mip-model. It saves all mip's arrays eather by saving
// them really or calling their write functions.
void ModelMipInfo::Write_t(CTStream *pFile) 
{
  INDEX iMembersCt;

  // Save count, call write for array of model polygons
  pFile->WriteFullChunk_t(CChunkID("IPOL"), &mmpi_PolygonsCt, sizeof(INDEX));
  {
    FOREACHINSTATICARRAY(mmpi_Polygons, ModelPolygon, it)
    {
      it.Current().Write_t(pFile);
    }
  }

  // Save count, array of texture vertices
  iMembersCt = mmpi_TextureVertices.Count();
  (*pFile) << iMembersCt;
  pFile->WriteFullChunk_t(CChunkID("TXV2"), &mmpi_TextureVertices[0], iMembersCt * sizeof(struct ModelTextureVertex));

  // Save count, call write for array of mapping surfaces
  iMembersCt = mmpi_MappingSurfaces.Count();
  (*pFile) << iMembersCt;
  {
    FOREACHINSTATICARRAY(mmpi_MappingSurfaces, MappingSurface, it)
    {
      it.Current().Write_t(pFile);
    }
  }

  // Write mip model flags
  (*pFile) << mmpi_ulFlags;
  
  // Write info of polygons occupied by patch
  INDEX ctPatches = mmpi_aPolygonsPerPatch.Count();
  (*pFile) << ctPatches;
  
  // For each patch
  for (INDEX iPatch = 0; iPatch < ctPatches; iPatch++)
  {
    // Write no of occupied polygons
    INDEX ctOccupied = mmpi_aPolygonsPerPatch[iPatch].ppp_iPolygons.Count();
    (*pFile) << ctOccupied;

    if (ctOccupied != 0) {
      pFile->WriteFullChunk_t(CChunkID("OCPL"),
                              &mmpi_aPolygonsPerPatch[iPatch].ppp_iPolygons[0],
                              ctOccupied * sizeof(INDEX));
    }
  }
}

// This is read function of one mip-model
void ModelMipInfo::Read_t(CTStream *pFile, BOOL bReadPolygonalPatches, BOOL bReadPolygonsPerSurface,
                          BOOL bReadSurfaceColors)
{
  INDEX iMembersCt;

  // Load count, allocate array and call Read for array of model polygons
  pFile->ReadFullChunk_t(CChunkID("IPOL"), &mmpi_PolygonsCt, sizeof(INDEX));
  mmpi_Polygons.New(mmpi_PolygonsCt);
  {
    FOREACHINSTATICARRAY(mmpi_Polygons, ModelPolygon, it)
    {
      it.Current().Read_t(pFile);
    }
  }

  // Load count, allocate and load array of texture vertices
  (*pFile) >> iMembersCt;
  mmpi_TextureVertices.New(iMembersCt);
  if (bReadPolygonsPerSurface)
  {
    // Chunk ID will tell us if we should read new format that contains bump normals
    CChunkID idChunk = pFile->GetID_t();
    
    // Jump over chunk size
    ULONG ulDummySize;
    (*pFile) >> ulDummySize;
    
    // If bump normals are saved (new format)
    if (idChunk == CChunkID("TXV2")) {
      pFile->ReadRawChunk_t(&mmpi_TextureVertices[0], iMembersCt * sizeof(struct ModelTextureVertex));
    } else {
      // Bump normals are not saved
      for (INDEX iVertex = 0; iVertex < iMembersCt; iVertex++)
      {
        pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_UVW, sizeof(FLOAT3D));
        pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_UV, sizeof(MEX2D));
        pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_Done, sizeof(BOOL));
        pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_iTransformedVertex, sizeof(INDEX));
        mmpi_TextureVertices[iVertex].mtv_vU = FLOAT3D(0,0,0);
        mmpi_TextureVertices[iVertex].mtv_vV = FLOAT3D(0,0,0);
      }
    }
  } else {
    pFile->ExpectID_t(CChunkID("TXVT"));
    
    // Jump over chunk size
    ULONG ulDummySize;
    (*pFile) >> ulDummySize;
    
    // Read models in old format
    for (INDEX iVertex = 0; iVertex < iMembersCt; iVertex++)
    {
      pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_UVW, sizeof(FLOAT3D));
      pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_UV, sizeof(MEX2D));
      pFile->Read_t(&mmpi_TextureVertices[iVertex].mtv_Done, sizeof(BOOL));
      mmpi_TextureVertices[iVertex].mtv_iTransformedVertex = 0;
      mmpi_TextureVertices[iVertex].mtv_vU = FLOAT3D(0, 0, 0);
      mmpi_TextureVertices[iVertex].mtv_vV = FLOAT3D(0, 0, 0);
    }
  }

  // Load count, allcate array and call Read for array of mapping surfaces
  (*pFile) >> iMembersCt;
  mmpi_MappingSurfaces.New(iMembersCt);
  INDEX iIndexOfSurface = 0;
  {
    FOREACHINSTATICARRAY(mmpi_MappingSurfaces, MappingSurface, it)
    {
      it.Current().Read_t(pFile, bReadPolygonsPerSurface, bReadSurfaceColors);
      
      // Obtain color per surface from polygons (old model format)
      if (!bReadPolygonsPerSurface)
      {
        // We will copy color from first polygon found with this surface into color of surface
        it->ms_colColor = C_WHITE;
        
        // For all polygons in this mip level
        for (INDEX iPolygon = 0; iPolygon < mmpi_PolygonsCt; iPolygon++)
        {
          if (mmpi_Polygons[iPolygon].mp_Surface == iIndexOfSurface) {
            it->ms_colColor = mmpi_Polygons[iPolygon].mp_ColorAndAlpha;
            break;
          }
        }
        iIndexOfSurface++;
      }
    }
  }

  if (bReadPolygonalPatches)
  {
    // Read mip model flags
    (*pFile) >> mmpi_ulFlags;
    
    // Read no of patches
    INDEX ctPatches;
    (*pFile) >> ctPatches;
    if (ctPatches != 0)
    {
      mmpi_aPolygonsPerPatch.New(ctPatches);
      // Read info for polygonal patches
      for (INDEX iPatch = 0; iPatch < ctPatches; iPatch++)
      {
        // Read no of occupied polygons
        INDEX ctOccupied;
        (*pFile) >> ctOccupied;
        if (ctOccupied != 0) {
          mmpi_aPolygonsPerPatch[iPatch].ppp_iPolygons.New(ctOccupied);
          pFile->ReadFullChunk_t(CChunkID("OCPL"), 
                                 &mmpi_aPolygonsPerPatch[iPatch].ppp_iPolygons[0],
                                 ctOccupied * sizeof(INDEX));
        }
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// TODO: Add comment here
CModelCollisionBox::CModelCollisionBox(void)
{
  mcb_vCollisionBoxMin = FLOAT3D(-0.5f, 0.0f,-0.5f);
  mcb_vCollisionBoxMax = FLOAT3D(0.5f, 2.0f, 0.5f);
  mcb_iCollisionBoxDimensionEquality = LENGTH_EQ_WIDTH;
  mcb_strName = "PART_NAME";
}

// TODO: Add comment here
void CModelCollisionBox::Read_t(CTStream *istrFile)
{
  // Read collision box min
  istrFile->Read_t(&mcb_vCollisionBoxMin, sizeof(FLOAT3D));
  
  // Read collision box size
  istrFile->Read_t(&mcb_vCollisionBoxMax, sizeof(FLOAT3D));
  
  // Get "collision box dimensions equality" value
  if ((mcb_vCollisionBoxMax(2) - mcb_vCollisionBoxMin(2)) == (mcb_vCollisionBoxMax(1) - mcb_vCollisionBoxMin(1)))
  {
    mcb_iCollisionBoxDimensionEquality = HEIGHT_EQ_WIDTH;
  } else if ((mcb_vCollisionBoxMax(3) - mcb_vCollisionBoxMin(3)) == 
             (mcb_vCollisionBoxMax(1) - mcb_vCollisionBoxMin(1)))
  {
    mcb_iCollisionBoxDimensionEquality = LENGTH_EQ_WIDTH;
  } else if ((mcb_vCollisionBoxMax(3) - mcb_vCollisionBoxMin(3)) ==
             (mcb_vCollisionBoxMax(2) - mcb_vCollisionBoxMin(2)))
  {
    mcb_iCollisionBoxDimensionEquality = LENGTH_EQ_HEIGHT;
  } else {
    /*
    // Force them to be legal (Lenght = Width)
    mcb_vCollisionBoxMax(3) = mcb_vCollisionBoxMin(3) +
                             (mcb_vCollisionBoxMax(1)-mcb_vCollisionBoxMin(1));
                             */
    mcb_iCollisionBoxDimensionEquality = LENGTH_EQ_WIDTH;
  }
}

// TODO: Add comment here
void CModelCollisionBox::ReadName_t(CTStream *istrFile)
{
  // read collision box name
  (*istrFile)>>mcb_strName;
}

// TODO: Add comment here
void CModelCollisionBox::Write_t(CTStream *ostrFile)
{
  // Write collision box min
  ostrFile->Write_t(&mcb_vCollisionBoxMin, sizeof(FLOAT3D));
  
  // Write collision box size
  ostrFile->Write_t(&mcb_vCollisionBoxMax, sizeof(FLOAT3D));
  
  // Write collision box name
  (*ostrFile)<<mcb_strName;
}

// TODO: Add comment here
CAttachedModelPosition::CAttachedModelPosition(void)
{
  amp_iCenterVertex = 0;
  amp_iFrontVertex = 1;
  amp_iUpVertex = 2;
  amp_plRelativePlacement = CPlacement3D(FLOAT3D(0, 0, 0), ANGLE3D(0, 0, 0));
}

// TODO: Add comment here
void CAttachedModelPosition::Read_t(CTStream *strFile)
{
  *strFile >> amp_iCenterVertex;
  *strFile >> amp_iFrontVertex;
  *strFile >> amp_iUpVertex;
  *strFile >> amp_plRelativePlacement;
}

// TODO: Add comment here
void CAttachedModelPosition::Write_t(CTStream *strFile)
{
  *strFile << amp_iCenterVertex;
  *strFile << amp_iFrontVertex;
  *strFile << amp_iUpVertex;
  *strFile << amp_plRelativePlacement;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Add comment here
void ModelMipInfo::Clear()
{
  mmpi_PolygonsCt = 0;                                      // reset number of polygons
  mmpi_Polygons.Clear();                                    // clear static arrays ...
  mmpi_TextureVertices.Clear();
  mmpi_MappingSurfaces.Clear();
  mmpi_aPolygonsPerPatch.Clear();
}

// TODO: Add comment here
ModelPolygon::ModelPolygon()
{
  mp_RenderFlags = 0;
}

// TODO: Add comment here
ModelPolygon::~ModelPolygon()
{
  mp_PolygonVertices.Clear();
}

// TODO: Add comment here
ModelMipInfo::~ModelMipInfo()
{
  Clear();
}

// TODO: Add comment here
MappingSurface::~MappingSurface()
{
}

// TODO: Add comment here
MappingSurface::MappingSurface()
{
  ms_ulRenderingFlags &= ~SRF_SELECTED;
  ms_colDiffuse = C_WHITE|CT_OPAQUE;
  ms_colReflections = C_WHITE|CT_OPAQUE;
  ms_colSpecular = C_WHITE|CT_OPAQUE;
  ms_colBump = C_WHITE|CT_OPAQUE;
  ms_ulOnColor = SC_ALLWAYS_ON;
  ms_ulOffColor = SC_ALLWAYS_OFF;
}
