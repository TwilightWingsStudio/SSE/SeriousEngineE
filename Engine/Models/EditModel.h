/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_EDITMODEL_H
#define SE_INCL_EDITMODEL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Core/Base/CTString.h>
#include <Core/IO/FileName.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Object3D.h>
#include <Core/Templates/StaticArray.h>
#include <Engine/Models/RenderModel.h>

#define MAX_MODELERTEXTURES		32
#define MAPPING_VERSION_WITHOUT_POLYGONS_PER_SURFACE "0001"
#define MAPPING_VERSION_WITHOUT_SOUNDS_AND_ATTACHMENTS "0002"
#define MAPPING_VERSION_WITHOUT_COLLISION "0003"
#define MAPPING_VERSION_WITHOUT_PATCHES "0004"
#define MAPPING_WITHOUT_SURFACE_COLORS "0005"
#define MAPPING_VERSION "0006"

/*
 * TODO: Add comment here
 */
class ENGINE_API CProgressRoutines
{
  public:
    CProgressRoutines();
    void (*SetProgressMessage)(char *strMessage);      // sets message for modeler's "new progress dialog"
    void (*SetProgressRange)(INDEX iProgresSteps);     // sets range of modeler's "new progress dialog"
    void (*SetProgressState)(INDEX iCurrentStep);      // sets current modeler's "new progress dialog" state
};


ENGINE_API extern CProgressRoutines ProgresRoutines;

/*
 * TODO: Add comment here
 */
class ENGINE_API CTextureDataInfo
{
  public:
    CListNode tdi_ListNode;
    CTextureData *tdi_TextureData;
    CTFileName tdi_FileName;
};


#define UNDO_VERTEX   0
#define UNDO_SURFACE  1

/*
 * TODO: Add comment here
 */
class ENGINE_API CMappingUndo
{
  public:
    CListNode mu_ListNode;
    INDEX mu_Action;
    ModelTextureVertex *mu_ClosestVertex;
    FLOAT3D mu_UndoCoordinate;
    INDEX mu_iCurrentMip;
    INDEX mu_iCurrentSurface;
    FLOAT mu_Zoom;
    FLOAT3D mu_Center;
    FLOAT3D mu_HPB;
};


/*
 * TODO: Add comment here
 */
class ENGINE_API CAttachedModel
{
  public:
    BOOL am_bVisible;
    CModelObject am_moAttachedModel; // used as smart pointer (holds file name of attachment), never rendered
    CTString am_strName;
    INDEX am_iAnimation;

    CAttachedModel(void);
    ~CAttachedModel(void);
    void SetModel_t(CTFileName fnModel);
    void Read_t(CTStream *strFile); // throw char *
    void Write_t(CTStream *strFile); // throw char *
    void Clear(void); // clear the object.
};


/*
 * TODO: Add comment here
 */
class ENGINE_API CAttachedSound {
  public:
    BOOL as_bLooping;
    BOOL as_bPlaying;
    FLOAT as_fDelay;
    CTFileName as_fnAttachedSound;

    CAttachedSound(void);
    void Read_t(CTStream *strFile); // throw char *
    void Write_t(CTStream *strFile); // throw char *
    void Clear(void)
    { 
      as_fnAttachedSound = CTString("");
    };
};


/*
 * TODO: Add comment here
 */
class ENGINE_API CThumbnailSettings
{
  public:
    BOOL ts_bSet;
    CPlacement3D ts_plLightPlacement;
    CPlacement3D ts_plModelPlacement;
    FLOAT ts_fTargetDistance;
    FLOAT3D ts_vTarget;
    ANGLE3D ts_angViewerOrientation;
    FLOAT ts_LightDistance;
    COLOR ts_LightColor;
    COLOR ts_colAmbientColor;
    COLORREF ts_PaperColor;
    COLORREF ts_InkColor;
    BOOL ts_IsWinBcgTexture;
    CTFileName ts_WinBcgTextureName;
    CModelRenderPrefs ts_RenderPrefs;

    CThumbnailSettings(void);
    void Read_t(CTStream *strFile); // throw char *
    void Write_t(CTStream *strFile); // throw char *
};

/*
 * TODO: Add comment here
 */
class ENGINE_API CEditModel : public CResource
{
  private: 
    // Creates new model, surface, vertice and polygon arrays
    void NewModel(CObject3D *pO3D);
    
    // Adds one mip model
    void AddMipModel(CObject3D *pO3D);
    
    // Loads and converts model's animation data from script file
    void LoadModelAnimationData_t(CTStream *pFile, const FLOATmatrix3D &mStretch); // throw char *
    
    INDEX edm_iActiveCollisionBox;  // collision box that is currently edited
    
  public:
    // Default contructor
    CEditModel();			

    // Default destructor
    ~CEditModel();
    
    CModelData edm_md;                                    // edited model data	
    
    TDynamicArray<CAttachedModel> edm_aamAttachedModels;  // array of attached models
    TStaticArray<CAttachedSound> edm_aasAttachedSounds;   // array of attached sounds
    
    CThumbnailSettings edm_tsThumbnailSettings;           // remembered parameters for taking thumbnail
    
    CListHead edm_WorkingSkins;                           // list of file names and texture data objects
    CListHead edm_UndoList;                               // list containing structures used for undo operation
    CListHead edm_RedoList;                               // list containing structures used for redo operation
    
    INDEX edm_Action;                                     // type of last mapping change action (used by undo/redo)
    
    CTFileName edm_fnSpecularTexture;                     // names of textures saved in ini file
    CTFileName edm_fnReflectionTexture;
    CTFileName edm_fnBumpTexture;
    
    // Create empty attaching sounds
    void CreateEmptyAttachingSounds(void);
    
    // Creates default script file
    void CreateScriptFile_t(CTFileName &fnFile);  // throw char *
    
    // Creates mip-model and mapping default constructios after it loads data from script
    void LoadFromScript_t(CTFileName &fnFileName); // throw char *
    
    // Recalculate mapping for surface after some polygon has been added to surface
    void RecalculateSurfaceMapping(INDEX iMipModel, INDEX iSurface);
    
    // Functions for extracting texture vertex links for given mip model
    void CalculateSurfaceCenterOffset(INDEX iCurrentMip, INDEX iSurface);
    void RemapTextureVerticesForSurface(INDEX iMip, INDEX iSurface, BOOL bJustCount);
    void RemapTextureVertices(INDEX iCurrentMip);
    
    // Calculates mapping from three special 3D objects
    void CalculateUnwrappedMapping(CObject3D &o3dClosed, CObject3D &o3dOpened, CObject3D &o3dUnwrapped);
    
    // Calculates mapping for mip models (except for main mip)
    void CalculateMappingForMips(void);
    
    // Updates animations
    void UpdateAnimations_t(CTFileName &fnScriptName);  // throw char *
    
    // Updates mip models configuration, looses their mapping !
    void UpdateMipModels_t(CTFileName &fnScriptName); // throw char *
    void CreateMipModels_t(CObject3D &objRestFrame, CObject3D &objMipSourceFrame, INDEX iVertexRemoveRate,
                           INDEX iSurfacePreservingFactor);
                           
    // Sets default mapping for given mip-model and surface (or all surfaces -1)
    void DefaultMapping(INDEX iCurrentMip, INDEX iSurface = -1);
    
    // Calculate mapping coordinates
    void CalculateMapping(INDEX iCurrentMip, INDEX iSurfaceNo);
    void CalculateMappingAll(INDEX iCurrentMip);
    
    // Draws given surface in wire frame
    void DrawWireSurface(CDrawPort *pDP, INDEX iCurrentMip, INDEX iCurrentSurface, FLOAT fMagnifyFactor,
                         PIX offx, PIX offy, COLOR clrVisible, COLOR clrInvisible); 

    // Fills given surface with color
    void DrawFilledSurface(CDrawPort *pDP, INDEX iCurrentMip, INDEX iCurrentSurface, FLOAT fMagnifyFactor,
                           PIX offx, PIX offy, COLOR clrVisible, COLOR clrInvisible);

    // Prints surface numbers
    void PrintSurfaceNumbers(CDrawPort *pDP, CFontData *pFont, INDEX iCurrentMip, FLOAT fMagnifyFactor,
                             PIX offx, PIX offy, COLOR clrInk);
    
    // TODO: Add comment here
    void ExportSurfaceNumbersAndNames(CTFileName fnFile);
    
    // Calculates given surface's center point in view's window (in pixels)
    void GetScreenSurfaceCenter(CDrawPort *pDP, INDEX iCurrentMip, INDEX iCurrentSurface, FLOAT fMagnifyFactor,
                                PIX offx, PIX offy, PIX &XCenter, PIX &YCenter); 

     // Used to calculate mapping coordinate's bbox
    FLOATaabbox3D CalculateSurfaceBBox(INDEX iCurrentMip, INDEX iSurfaceNo,
                                       FLOAT3D f3Position, FLOAT3D f3Angle, FLOAT fZoom);
    
    // Used to calculate mapping coordinates
    void CalculateMapping();
    
    // If it can, sets surface position with given offseted values
    BOOL ChangeSurfacePositionRelative(INDEX iCurrentMip, INDEX iCurrentSurface, FLOAT fDZoom,
                                       FLOAT dX, FLOAT dY, PIX dTH, PIX dTV);

    // If it can, sets surface position to given values
    BOOL ChangeSurfacePositionAbsolute(INDEX iCurrentMip, INDEX iCurrentSurface,
                                       FLOAT newZoom, FLOAT3D newCenter, FLOAT3D newAngle); 
    
    // Remember undo position                                       
    void RememberSurfaceUndo(INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Finds closest point and returns it
    ModelTextureVertex *FindClosestMappingVertex(INDEX iCurrentMip, INDEX iCurrentSurface, PIX ptx, PIX pty,
                                                 FLOAT fMagnifyFactor, PIX offx, PIX offy);

    // Try new vertex position
    void MoveVertex(ModelTextureVertex *pmtvClosestVertex, PIX dx, PIX dy, INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Remember coordinate change into undo buffer
    void RememberVertexUndo(ModelTextureVertex *pmtvClosestVertex);
    
    // Add one texture to list of working textures
    CTextureDataInfo *AddTexture_t(const CTFileName &fnFileName, const MEX mexWidth, 
                                   const MEX mexHeight); // throw char *
    
    // Retrieves given surface's position
    FLOAT3D GetSurfacePos(INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Retrieves given surface's angles
    FLOAT3D GetSurfaceAngles(INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Retrieves given surface's zoom factor
    FLOAT GetSurfaceZoom(INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Retrieves given surface's name
    const char *GetSurfaceName(INDEX iCurrentMip, INDEX iCurrentSurface);
    
    // Undoes last operation 
    void Undo(void);
    
    // Redoes last operation 
    void Redo(void);
    
    // Removes one of working textures in modeler app
    void RemoveTexture(char *pFileName);
    
    // TODO: Add comment here
    void MovePatchRelative(INDEX iMaskBit, MEX2D mexOffset);
    
    // TODO: Add comment here
    void SetPatchStretch(INDEX iMaskBit, FLOAT fNewStretch);
    
    // Adds one patch
    BOOL EditAddPatch(CTFileName fnPatchName, MEX2D mexPos, INDEX &iMaskBit);
    
    // Removes given patch
    void EditRemovePatch(INDEX iMaskBit);
    
    // TODO: Add comment here
    void EditRemoveAllPatches(void);
    
    // TODO: Add comment here
    INDEX CountPatches(void);
    
    // TODO: Add comment here
    ULONG GetExistingPatchesMask(void);
    
    
    // Finds first empty space ready to receive new patch
    BOOL GetFirstEmptyPatchIndex(INDEX &iMaskBit);
    
    // Finds first valid patch index
    BOOL GetFirstValidPatchIndex(INDEX &iMaskBit);
    
    // Sets previous valid patch index
    void GetPreviousValidPatchIndex(INDEX &iMaskBit);
    
    // Sets next valid patch index
    void GetNextValidPatchIndex(INDEX &iMaskBit);
    
    // TODO: Add comment here
    void CalculatePatchesPerPolygon(void);
    
    // Returns number of mip models
    INDEX GetMipCt()
    { 
      return edm_md.md_MipCt;
    };
    
    // Returns allowed width for model's texture
    MEX GetWidth()
    {
      return edm_md.md_Width;
    };
    
    // Returns allowed height for model's texture
    MEX GetHeight()
    {
      return edm_md.md_Height;
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Collision box handling functions
    
    // TODO: Add comment here
    FLOAT3D &GetCollisionBoxMin(void);
    
    // TODO: Add comment here
    FLOAT3D &GetCollisionBoxMax(void);
    
    // TODO: Add comment here
    void AddCollisionBox(void);
    
    // TODO: Add comment here
    void DeleteCurrentCollisionBox(void);
    
    // TODO: Add comment here
    INDEX GetActiveCollisionBoxIndex(void)
    {
      return edm_iActiveCollisionBox;
    };
    
    // TODO: Add comment here
    void ActivatePreviousCollisionBox(void);
    
    // TODO: Add comment here
    void ActivateNextCollisionBox(void);
    
    // TODO: Add comment here
    void SetCollisionBox(FLOAT3D vMin, FLOAT3D vMax);
    
    // TODO: Add comment here
    CTString GetCollisionBoxName(INDEX iCollisionBox);
    
    // TODO: Add comment here
    CTString GetCollisionBoxName(void);
    
    // TODO: Add comment here
    void SetCollisionBoxName(CTString strNewName);
    
    // TODO: Add comment here
    void CorrectCollisionBoxSize(void);
    
    // Returns HEIGHT_EQ_WIDTH, LENGTH_EQ_WIDTH or LENGTH_EQ_HEIGHT
    INDEX GetCollisionBoxDimensionEquality();
    
    // Set new collision box equality value
    void SetCollisionBoxDimensionEquality(INDEX iNewDimEqType);
    
    // Overloaded load function
    void Load_t(CTFileName fnFileName); // throw char *
    
    // Overloaded save function
    void Save_t(CTFileName fnFileName); // throw char *
    
    // Exports .h file (#define ......)
    void SaveIncludeFile_t(CTFileName fnFileName, CTString strDefinePrefix); // throw char *

    
    // Load modeler data
    void Read_t(CTStream *istrFile); // throw char *
    
    // Save modeler data (i.e. vindow positions, view prefs, texture file names...)
    void Write_t(CTStream *ostrFile); // throw char *

    
    // Load and save mapping data for whole model (iMip = -1) or just for one mip model
    void LoadMapping_t(CTFileName fnFileName, INDEX iMip = -1);
    void SaveMapping_t(CTFileName fnFileName, INDEX iMip = -1);
    
    // Read and write settings for given mip
    void ReadMipSettings_t(CTStream *istrFile, INDEX iMip);  // throw char *
    void WriteMipSettings_t(CTStream *ostrFile, INDEX iMip);
};


#endif  /* include-once check. */