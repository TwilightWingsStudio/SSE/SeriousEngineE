/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Models/ModelObject.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/RenderModel.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Models/Normals.h>
#include <Core/IO/Stream.h>
#include <Core/Base/CTString.inl>
#include <Core/Math/Clipping.inl>
#include <Core/Graphics/Color.h>
#include <Engine/Graphics/DrawPort.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

#include <Engine/Resources/ResourceManager.h>

// Model LOD biasing control
extern FLOAT mdl_fLODMul;
extern FLOAT mdl_fLODAdd;
extern INDEX mdl_iLODDisappear; // 0=never, 1=ignore bias, 2=with bias

// Function returns number of first setted bit in ULONG
INDEX GetBit(ULONG ulSource)
{
  for (INDEX i=0; i<32; i++)
  {
    if ((ulSource & (1<<i)) != 0) return i;
  }
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Object constructor
CModelObject::CModelObject()
{
  mo_pmdModelData = NULL;
  mo_colBlendColor = 0xFFFFFFFF;
  mo_ColorMask = 0x7FFFFFFF;
  mo_PatchMask = 0;
  mo_iManualMipLevel = 0;
  mo_AutoMipModeling = TRUE;
  mo_Stretch = FLOAT3D(1, 1, 1);
}

// Destructor
CModelObject::~CModelObject()
{
  mo_pmdModelData->RemReference();

  for (INDEX iPatch = 0; iPatch < MAX_TEXTUREPATCHES; iPatch++)
  {
    HidePatch(iPatch);
  }

  RemoveAllAttachmentModels();
}

// Copy from another object of same class
void CModelObject::Copy(CAbstractModelInstance &mi)
{
  if (mi.GetInstanceType() != GetInstanceType()) {
    ASSERTALWAYS("CModelObject::Copy called for different classes!");
    return;
  }

  CModelObject *pmo = (CModelObject *)(&mi);
  CModelObject &moOther = *pmo;

  // CAnimObject::Copy
  {
    SetData(moOther.GetData()); // copy model
    mo_aoAnimated.Copy(moOther.mo_aoAnimated); // CAnimObject::Copy(moOther);
  }
  
  mo_PatchMask = moOther.mo_PatchMask;
  mo_iManualMipLevel = moOther.mo_iManualMipLevel;
  mo_AutoMipModeling = moOther.mo_AutoMipModeling;
  mo_Stretch = moOther.mo_Stretch;
  mo_ColorMask = moOther.mo_ColorMask;
  mo_iLastRenderMipLevel = moOther.mo_iLastRenderMipLevel;
  mo_colBlendColor = moOther.mo_colBlendColor;

  mo_toTexture.Copy(moOther.mo_toTexture);
  mo_toReflection.Copy(moOther.mo_toReflection);
  mo_toSpecular.Copy(moOther.mo_toSpecular);
  mo_toBump.Copy(moOther.mo_toBump);

  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, moOther.mo_lhAttachments, itamo)
  {
    CAttachmentModelObject &amoOther = *itamo;
    CAttachmentModelObject &amo = *AddAttachmentModel(amoOther.amo_iAttachedPosition);
    amo.amo_plRelative = amoOther.amo_plRelative;
    amo.amo_moModelObject.Copy(amoOther.amo_moModelObject);
  }
}

// Synchronize with another model (copy animations/attachments positions etc from there)
void CModelObject::Synchronize(CModelObject &moOther)
{
  // Synchronize animation
  mo_aoAnimated.Synchronize(moOther.mo_aoAnimated); // CAnimObject::Synchronize(moOther);

  // Synchronize misc parameters
  mo_PatchMask = moOther.mo_PatchMask;
  mo_Stretch = moOther.mo_Stretch;
  mo_ColorMask = moOther.mo_ColorMask;
  mo_colBlendColor = moOther.mo_colBlendColor;

  CModelData *pmd = GetData();
  CModelData *pmdOther = moOther.GetData();
  if (pmd == NULL || pmdOther == NULL) {
    return;
  }

  // For each attachment in another object
  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, moOther.mo_lhAttachments, itamo)
  {
    CAttachmentModelObject *pamoOther = itamo;
    INDEX iap = pamoOther->amo_iAttachedPosition;
    
    // Get one here with same index
    CAttachmentModelObject *pamo = GetAttachmentModel(iap);
    
    // If found
    if (pamo != NULL)
    {
      // Sync the model itself
      pamo->amo_moModelObject.Synchronize(pamoOther->amo_moModelObject);

      // Get original placements of both attachments
      pmd->md_aampAttachedPosition.Lock();
      pmdOther->md_aampAttachedPosition.Lock();
      CPlacement3D plOrg = pmd->md_aampAttachedPosition[iap].amp_plRelativePlacement;
      CPlacement3D plOtherOrg = pmdOther->md_aampAttachedPosition[iap].amp_plRelativePlacement;
      pmd->md_aampAttachedPosition.Unlock();
      pmdOther->md_aampAttachedPosition.Unlock();
      FLOAT3D &v2 = pamo->amo_plRelative.pl_PositionVector;
      FLOAT3D &v1 = pamoOther->amo_plRelative.pl_PositionVector;
      FLOAT3D &v2O = plOrg.pl_PositionVector;
      FLOAT3D &v1O = plOtherOrg.pl_PositionVector;
      ANGLE3D &a2 = pamo->amo_plRelative.pl_OrientationAngle;
      ANGLE3D &a1 = pamoOther->amo_plRelative.pl_OrientationAngle;
      ANGLE3D &a2O = plOrg.pl_OrientationAngle;
      ANGLE3D &a1O = plOtherOrg.pl_OrientationAngle;
      v2 = v2O + v1 - v1O;
      a2 = a2O + a1 - a1O;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Write
void CModelObject::Write_t(CTStream *pFile) 
{
  mo_aoAnimated.Write_t(pFile); // CAnimObject::Write_t(pFile);

  pFile->WriteID_t(CChunkID("MODT"));
  *pFile << mo_colBlendColor;
  pFile->Write_t(&mo_PatchMask, sizeof(ULONG));
  pFile->Write_t(&mo_Stretch, sizeof(FLOAT3D));
  pFile->Write_t(&mo_ColorMask, sizeof(ULONG));
}

// Read
void CModelObject::Read_t(CTStream *pFile)
{
  mo_aoAnimated.Read_t(pFile); // CAnimObject::Read_t(pFile);

  if (pFile->PeekID_t() == CChunkID("MODT")) {
    pFile->ExpectID_t(CChunkID("MODT"));
    *pFile >> mo_colBlendColor;
  // model object is saved without dynamic blend color
  } else {
    mo_colBlendColor = 0xFFFFFFFF;
  }

  pFile->Read_t(&mo_PatchMask, sizeof(ULONG));
  pFile->Read_t(&mo_Stretch, sizeof(FLOAT3D));
  pFile->Read_t(&mo_ColorMask, sizeof(ULONG));

  for (INDEX i = 0; i < MAX_TEXTUREPATCHES; i++)
  {
    if ((mo_PatchMask & ((1UL) << i)) != 0) {
      ShowPatch(i);
    }
  }
}

// Retrieves model's texture width
MEX CModelObject::GetWidth()
{
  return GetData()->md_Width;
};

// Retrieves model's texture height
MEX CModelObject::GetHeight()
{
  return GetData()->md_Height;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Routine retrives full model data
void CModelObject::GetModelInfo(CModelInfo &miInfo)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  
  // Copy model data values
  miInfo.mi_VerticesCt = pMD->md_VerticesCt;
  miInfo.mi_FramesCt = pMD->md_FramesCt;
  miInfo.mi_MipCt = pMD->md_MipCt;
  
  for (INDEX i = 0; i < pMD->md_MipCt; i++)
  {
    miInfo.mi_MipInfos[i].mi_PolygonsCt = pMD->md_MipInfos[i].mmpi_PolygonsCt;

    // Calculate triangeles
    miInfo.mi_MipInfos[i].mi_TrianglesCt = 0;
    for (INDEX iPolygon = 0; iPolygon < pMD->md_MipInfos[i].mmpi_PolygonsCt; iPolygon++)
    {
      miInfo.mi_MipInfos[i].mi_TrianglesCt += 
        pMD->md_MipInfos[i].mmpi_Polygons[iPolygon].mp_PolygonVertices.Count() - 2;
    }

    ULONG ulMipMask = (1L) << i;      // working mip model's mask
    INDEX iVertexCt = 0;
    
    // Count vertices that exists in this mip model
    for (INDEX j = 0; j < pMD->md_VerticesCt; j++)
    {
      if (pMD->md_VertexMipMask[j] & ulMipMask) {
        iVertexCt++;
      }
    }
    miInfo.mi_MipInfos[i].mi_VerticesCt = iVertexCt;
  }

  miInfo.mi_Width = pMD->md_Width;
  miInfo.mi_Height = pMD->md_Height;
  miInfo.mi_Flags = pMD->md_Flags;
  miInfo.mi_ShadowQuality = pMD->md_ShadowQuality;
  miInfo.mi_Stretch = pMD->md_Stretch;
}

// Is model visible for given mip factor
BOOL CModelObject::IsModelVisible(FLOAT fMipFactor)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  ASSERT(pMD->md_MipCt > 0);
  
  // Visible if no mip models or disappearence not allowed
  if (pMD->md_MipCt == 0 || mdl_iLODDisappear == 0) {
    return TRUE;
  }
  
  // Adjust mip factor in case of dynamic stretch factor
  if (mo_Stretch != FLOAT3D(1, 1, 1)) {
    fMipFactor -= Log2(Max(mo_Stretch(1), Max(mo_Stretch(2), mo_Stretch(3))));
  }
  
  // Eventually adjusted mip factor with LOD control variables
  if (mdl_iLODDisappear == 2) {
    fMipFactor = fMipFactor * mdl_fLODMul + mdl_fLODAdd;
  }
  
  // Return true if mip factor is smaller than last in model's mip switch factors array
  return(fMipFactor < pMD->md_MipSwitchFactors[pMD->md_MipCt - 1]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Retrieves activ mip model's index
INDEX CModelObject::GetMipModel(FLOAT fMipFactor)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  if (!mo_AutoMipModeling) {
    return mo_iManualMipLevel;
  }
  
  // Calculate current mip model
  INDEX i = 0;
  for (; i < pMD->md_MipCt; i++)
  {
    if (fMipFactor < pMD->md_MipSwitchFactors[i]) {
      return i;
    }
  }
  return i - 1;
}

// Retrieves bounding box of given frame
FLOATaabbox3D CModelObject::GetFrameBBox(INDEX iFrameNo)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  return pMD->md_FrameInfos[iFrameNo].mfi_Box;
}

// Routine returns mo_AutoMipModeling flag
BOOL CModelObject::IsAutoMipModeling()
{
  return mo_AutoMipModeling;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Sets mo_AutoMipModeling flag to on
void CModelObject::AutoMipModelingOn()
{
  mo_AutoMipModeling = TRUE;
  MarkChanged();
}

// Sets mo_AutoMipModeling flag to off
void CModelObject::AutoMipModelingOff()
{
  mo_AutoMipModeling = FALSE;
  MarkChanged();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Routine retrieves current mip level
INDEX CModelObject::GetManualMipLevel()
{
  return mo_iManualMipLevel;
}

// Routine sets current mip level
void CModelObject::SetManualMipLevel(INDEX iNewMipLevel)
{
  mo_iManualMipLevel = iNewMipLevel;
  MarkChanged();
}

// Routine sets given mip-level's switch factor
void CModelObject::SetMipSwitchFactor(INDEX iMipLevel, float fMipFactor)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(iMipLevel < pMD->md_MipCt);
  pMD->md_MipSwitchFactors[iMipLevel] = fMipFactor;
  MarkChanged();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Select one rougher mip model level
void CModelObject::NextManualMipLevel()
{
  CModelData *pMD = (CModelData*)GetData();
  if (mo_iManualMipLevel < pMD->md_MipCt - 1) {
    mo_iManualMipLevel += 1;
    MarkChanged();
  }
}

// Select one more precize mip model level
void CModelObject::PrevManualMipLevel()
{
  if (mo_iManualMipLevel > 0) {
    mo_iManualMipLevel -= 1;
    MarkChanged();
  }
}

// Returns current value of patches mask
ULONG CModelObject::GetPatchesMask()
{
  return mo_PatchMask;
};

// Set new patches combination
void CModelObject::SetPatchesMask(ULONG new_patches_mask)
{
  mo_PatchMask = new_patches_mask;
}

// Sets new name to a color with given index
void CModelObject::SetColorName(INDEX iColor, CTString &strNewName)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(iColor < MAX_COLOR_NAMES);
  pMD->md_ColorNames[iColor] = strNewName;
}

// Retrieves name of color with given index
CTString CModelObject::GetColorName(INDEX iColor)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(iColor < MAX_COLOR_NAMES);
  return pMD->md_ColorNames[iColor];
}

// Retrieves color of given surface
COLOR CModelObject::GetSurfaceColor(INDEX iCurrentMip, INDEX iCurrentSurface)
{
  struct ModelPolygon *pPoly;
  CModelData *pMD = (CModelData*)GetData();
  if ((iCurrentMip >= pMD->md_MipCt) ||
      (iCurrentSurface >= pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces.Count()))
  {
    return -1;
  }
  
  for (INDEX i = 0; i < pMD->md_MipInfos[iCurrentMip].mmpi_PolygonsCt; i++)
  {
    pPoly = &pMD->md_MipInfos[iCurrentMip].mmpi_Polygons[i];
    if (pPoly->mp_Surface == iCurrentSurface) {
      return pPoly->mp_ColorAndAlpha;
    }
  }
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Changes color of given surface
void CModelObject::SetSurfaceColor(INDEX iCurrentMip, INDEX iCurrentSurface, COLOR colNewColorAndAlpha)
{
  struct ModelPolygon *pPoly;
  CModelData *pMD = (CModelData*)GetData();
  if ((iCurrentMip >= pMD->md_MipCt) ||
      (iCurrentSurface >= pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces.Count()))
  {
    return;
  }
  
  pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces[iCurrentSurface].ms_colColor = colNewColorAndAlpha;
  for (INDEX i = 0; i < pMD->md_MipInfos[iCurrentMip].mmpi_PolygonsCt; i++)
  {
    pPoly = &pMD->md_MipInfos[iCurrentMip].mmpi_Polygons[i];
    if (pPoly->mp_Surface == iCurrentSurface) {
      pPoly->mp_ColorAndAlpha = colNewColorAndAlpha;
    }
  }
}

// Retrieves rendering flags of given surface
void CModelObject::GetSurfaceRenderFlags(INDEX iCurrentMip, INDEX iCurrentSurface,
                                         enum SurfaceShadingType &sstShading,
                                         enum SurfaceTranslucencyType &sttTranslucency,
                                         ULONG &ulRenderingFlags)
{
  CModelData *pMD = (CModelData*)GetData();
  if ((iCurrentMip >= pMD->md_MipCt) ||
      (iCurrentSurface >= pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces.Count()))
  {
    return;
  }
  
  MappingSurface *pms = &pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces[iCurrentSurface];
  sstShading = pms->ms_sstShadingType;
  sttTranslucency = pms->ms_sttTranslucencyType;
  ulRenderingFlags = pms->ms_ulRenderingFlags;
}

// Changes rendering of given surface
void CModelObject::SetSurfaceRenderFlags(INDEX iCurrentMip, INDEX iCurrentSurface,
                                         enum SurfaceShadingType sstShading,
                                         enum SurfaceTranslucencyType sttTranslucency,
                                         ULONG ulRenderingFlags)
{
  CModelData *pMD = (CModelData*)GetData();
  if ((iCurrentMip >= pMD->md_MipCt) ||
      (iCurrentSurface >= pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces.Count()))
  {
    return;
  }
  
  // Convert surface rendering parameters from old polygon flags  -- temporary !!!!
  MappingSurface *pms = &pMD->md_MipInfos[iCurrentMip].mmpi_MappingSurfaces[iCurrentSurface];
  pms->ms_sstShadingType = sstShading;
  pms->ms_sttTranslucencyType = sttTranslucency;
  pms->ms_ulRenderingFlags = ulRenderingFlags;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Add comment here
void CModelObject::ProjectFrameVertices(CProjection3D *pProjection, INDEX iMipModel)
{
  FLOAT3D f3dVertex;

  CModelData *pMD = (CModelData*)GetData();
  pProjection->ObjectHandleL() = pMD->md_vCompressedCenter;
  pProjection->ObjectStretchL() = pMD->md_Stretch;
  
  // Apply dynamic stretch
  pProjection->ObjectStretchL()(1) *= mo_Stretch(1);
  pProjection->ObjectStretchL()(2) *= mo_Stretch(2);
  pProjection->ObjectStretchL()(3) *= mo_Stretch(3);
  pProjection->ObjectFaceForwardL() = pMD->md_Flags & (MF_FACE_FORWARD | MF_HALF_FACE_FORWARD);
  pProjection->ObjectHalfFaceForwardL() = pMD->md_Flags & MF_HALF_FACE_FORWARD;
  pProjection->Prepare();

  INDEX iCurrentFrame = GetFrame();
  ULONG ulVtxMask = (1L) << iMipModel;

  if (pMD->md_Flags & MF_COMPRESSED_16BIT)
  {
    ModelFrameVertex16 *pFrame = &pMD->md_FrameVertices16[iCurrentFrame * pMD->md_VerticesCt];
    for (INDEX i = 0; i < pMD->md_VerticesCt; i++)
    {
      if (pMD->md_VertexMipMask[i] & ulVtxMask) {
        f3dVertex(1) = (float)pFrame[i].mfv_SWPoint(1);
        f3dVertex(2) = (float)pFrame[i].mfv_SWPoint(2);
        f3dVertex(3) = (float)pFrame[i].mfv_SWPoint(3);
        pProjection->ProjectCoordinate(f3dVertex, pMD->md_TransformedVertices[i].tvd_TransformedPoint);
      }
    }
  } else {
    ModelFrameVertex8 *pFrame = &pMD->md_FrameVertices8[iCurrentFrame * pMD->md_VerticesCt];
    for (INDEX i = 0; i < pMD->md_VerticesCt; i++)
    {
      if (pMD->md_VertexMipMask[i] & ulVtxMask) {
        f3dVertex(1) = (float) pFrame[i].mfv_SBPoint(1);
        f3dVertex(2) = (float) pFrame[i].mfv_SBPoint(2);
        f3dVertex(3) = (float) pFrame[i].mfv_SBPoint(3);
        pProjection->ProjectCoordinate(f3dVertex, pMD->md_TransformedVertices[i].tvd_TransformedPoint);
      }
    }
  }
}


// Colorizes surfaces touching given box
void CModelObject::ColorizeRegion(CDrawPort *pDP, CProjection3D *pProjection, PIXaabbox2D box, INDEX iChoosedColor,
                                  BOOL bOnColorMode)
{
  struct ModelPolygon *pPoly;
  CModelData *pMD = (CModelData*)GetData();
  struct TransformedVertexData *pTransformedVertice;
  PIX pixDPHeight = pDP->GetHeight();
  
  // Project vertices for given mip model
  ProjectFrameVertices(pProjection, mo_iLastRenderMipLevel);
  for (INDEX j = 0; j < pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_PolygonsCt; j++)
  {
    pPoly = &pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_Polygons[j];

    for (INDEX i = 0; i < pPoly->mp_PolygonVertices.Count(); i++)
    {
      pTransformedVertice = pPoly->mp_PolygonVertices[i].mpv_ptvTransformedVertex;
      PIXaabbox2D ptBox = PIXaabbox2D(PIX2D((SWORD) pTransformedVertice->tvd_TransformedPoint(1),
                                       pixDPHeight - (SWORD) pTransformedVertice->tvd_TransformedPoint(2)));
      if (!((box & ptBox).IsEmpty()))
      {
        MappingSurface &ms = pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_MappingSurfaces[pPoly->mp_Surface];
        if (bOnColorMode) {
          //pPoly->mp_OnColor = 1UL << iChoosedColor;
          ms.ms_ulOnColor = 1UL << iChoosedColor;
        } else {
          //pPoly->mp_OffColor = 1UL << iChoosedColor;
          ms.ms_ulOffColor = 1UL << iChoosedColor;
        }
        break;
      }
    }
  }
}

// Colorizes polygons touching given box
void CModelObject::ApplySurfaceToPolygonsInRegion(CDrawPort *pDP, CProjection3D *pProjection, PIXaabbox2D box,
                                                  INDEX iSurface, COLOR colSurfaceColor)
{
  // Project vertices for given mip model
  ProjectFrameVertices(pProjection, mo_iLastRenderMipLevel);

  struct ModelPolygon *pPoly;
  struct TransformedVertexData *pTransformedVertice;
  CModelData *pMD = (CModelData*)GetData();
  PIX pixDPHeight = pDP->GetHeight();

  for (INDEX j = 0; j < pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_PolygonsCt; j++)
  {
    pPoly = &pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_Polygons[j];
    for (INDEX i = 0; i < pPoly->mp_PolygonVertices.Count(); i++)
    {
      pTransformedVertice = pPoly->mp_PolygonVertices[i].mpv_ptvTransformedVertex;
      PIXaabbox2D ptBox = PIXaabbox2D(PIX2D((SWORD) pTransformedVertice->tvd_TransformedPoint(1),
                                             pixDPHeight - (SWORD) pTransformedVertice->tvd_TransformedPoint(2)));
      if (!((box & ptBox).IsEmpty())) {
        pPoly->mp_Surface = iSurface;
        pPoly->mp_ColorAndAlpha = colSurfaceColor;
        break;
      }
    }
  }
}

// Unpack a vertex
void CModelObject::UnpackVertex(INDEX iFrame, INDEX iVertex, FLOAT3D &vVertex)
{
  CModelData *pmd = (CModelData*)GetData();
  
  // Get decompression/stretch factors
  FLOAT3D &vDataStretch = pmd->md_Stretch;
  FLOAT3D &vObjectStretch = mo_Stretch;
  FLOAT3D vStretch;
  vStretch(1) = vDataStretch(1) * vObjectStretch(1);
  vStretch(2) = vDataStretch(2) * vObjectStretch(2);
  vStretch(3) = vDataStretch(3) * vObjectStretch(3);
  FLOAT3D vOffset = pmd->md_vCompressedCenter;

  if (pmd->md_Flags & MF_COMPRESSED_16BIT) {
    struct ModelFrameVertex16 *pFrame16 = &pmd->md_FrameVertices16[iFrame * pmd->md_VerticesCt];
    vVertex(1) = (pFrame16[iVertex].mfv_SWPoint(1) - vOffset(1)) * vStretch(1);
    vVertex(2) = (pFrame16[iVertex].mfv_SWPoint(2) - vOffset(2)) * vStretch(2);
    vVertex(3) = (pFrame16[iVertex].mfv_SWPoint(3) - vOffset(3)) * vStretch(3);
  } else {
    struct ModelFrameVertex8 *pFrame8 = &pmd->md_FrameVertices8[iFrame * pmd->md_VerticesCt];
    vVertex(1) = (pFrame8[iVertex].mfv_SBPoint(1) - vOffset(1)) * vStretch(1);
    vVertex(2) = (pFrame8[iVertex].mfv_SBPoint(2) - vOffset(2)) * vStretch(2);
    vVertex(3) = (pFrame8[iVertex].mfv_SBPoint(3) - vOffset(3)) * vStretch(3);
  }
}

// TODO: Add comment here
CPlacement3D CModelObject::GetAttachmentPlacement(CAttachmentModelObject &amo)
{
  // Project reference points to view space
  FLOAT3D vCenter, vFront, vUp;
  CModelData *pmd = (CModelData*)GetData();
  pmd->md_aampAttachedPosition.Lock();
  INDEX iPosition = amo.amo_iAttachedPosition;
  INDEX iCenter = pmd->md_aampAttachedPosition[iPosition].amp_iCenterVertex;
  INDEX iFront = pmd->md_aampAttachedPosition[iPosition].amp_iFrontVertex;
  INDEX iUp = pmd->md_aampAttachedPosition[iPosition].amp_iUpVertex;
  INDEX iFrame = GetFrame();

  UnpackVertex(iFrame, iCenter, vCenter);
  UnpackVertex(iFrame, iFront, vFront);
  UnpackVertex(iFrame, iUp, vUp);

  // Make axis vectors in absolute space
  FLOAT3D &vO = vCenter;
  FLOAT3D vY = vUp-vCenter;
  FLOAT3D vZ = vCenter-vFront;
  FLOAT3D vX = vY * vZ;
  vY = vZ * vX;
  
  // Make a rotation matrix from those vectors
  vX.Normalize();
  vY.Normalize();
  vZ.Normalize();
  FLOATmatrix3D mOrientation;
  mOrientation(1, 1) = vX(1); mOrientation(1, 2) = vY(1); mOrientation(1, 3) = vZ(1);
  mOrientation(2, 1) = vX(2); mOrientation(2, 2) = vY(2); mOrientation(2, 3) = vZ(2);
  mOrientation(3, 1) = vX(3); mOrientation(3, 2) = vY(3); mOrientation(3, 3) = vZ(3);

  // Make reference placement in absolute space
  CPlacement3D plPoints;
  plPoints.pl_PositionVector = vO;
  DecomposeRotationMatrixNoSnap(plPoints.pl_OrientationAngle, mOrientation);
  CPlacement3D pl = amo.amo_plRelative;
  pl.RelativeToAbsoluteSmooth(plPoints);
  pmd->md_aampAttachedPosition.Unlock();
  return pl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Find hitted polygon
struct ModelPolygon *CModelObject::PolygonHit(CPlacement3D plRay, CPlacement3D plObject, INDEX iCurrentMip,
                                              FLOAT &fHitDistance)
{
  struct ModelPolygon *pResultPoly = NULL;

  fHitDistance = 100000.0f;

  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    FLOAT fHit;
    CPlacement3D plAttachment = GetAttachmentPlacement(*itamo);
    plAttachment.RelativeToAbsolute(plObject);
    struct ModelPolygon *pmp = itamo->amo_moModelObject.PolygonHit(plRay, plAttachment, iCurrentMip, fHit);
    if (fHit < fHitDistance) {
      fHitDistance = fHit;
      pResultPoly = pmp;
    }
  }
  
  FLOAT fHit;
  struct ModelPolygon *pmp = PolygonHitModelData((CModelData*)GetData(), plRay, plObject, iCurrentMip, fHit);
  if (fHit < fHitDistance) {
    fHitDistance = fHit;
    pResultPoly = pmp;
  }

  return pResultPoly;
}

// TODO: Add comment here
struct ModelPolygon *CModelObject::PolygonHitModelData(CModelData *pMD, CPlacement3D plRay, CPlacement3D plObject,
                                                       INDEX iCurrentMip, FLOAT &fHitDistance)
{
  FLOAT fClosest = -100000.0f;
  struct ModelPolygon *pPoly, *pResultPoly = NULL;
  CIntersector Intersector;

  CSimpleProjection3D spProjection;
  spProjection.ViewerPlacementL() = plRay;
  spProjection.ObjectPlacementL() = plObject;
  
  // Project vertices for given mip model
  ProjectFrameVertices(&spProjection, iCurrentMip);

  for (INDEX j = 0; j < pMD->md_MipInfos[iCurrentMip].mmpi_PolygonsCt; j++)
  {
    Intersector.Clear();
    pPoly = &pMD->md_MipInfos[iCurrentMip].mmpi_Polygons[j];
    for (INDEX i = 0; i < pPoly->mp_PolygonVertices.Count(); i++)
    {
      // Get next vertex index (first is i)
      INDEX next = (i + 1) % pPoly->mp_PolygonVertices.Count();
      
      // Add edge to intersection object
      Intersector.AddEdge(pPoly->mp_PolygonVertices[i].mpv_ptvTransformedVertex->tvd_TransformedPoint(1),
                          pPoly->mp_PolygonVertices[i].mpv_ptvTransformedVertex->tvd_TransformedPoint(2),
                          pPoly->mp_PolygonVertices[next].mpv_ptvTransformedVertex->tvd_TransformedPoint(1),
                          pPoly->mp_PolygonVertices[next].mpv_ptvTransformedVertex->tvd_TransformedPoint(2));
    }
    
    if (Intersector.IsIntersecting())
    {
      FLOAT3D f3dTr0 = pPoly->mp_PolygonVertices[0].mpv_ptvTransformedVertex->tvd_TransformedPoint;
      FLOAT3D f3dTr1 = pPoly->mp_PolygonVertices[1].mpv_ptvTransformedVertex->tvd_TransformedPoint;
      FLOAT3D f3dTr2 = pPoly->mp_PolygonVertices[2].mpv_ptvTransformedVertex->tvd_TransformedPoint;
      FLOATplane3D fplPlane = FLOATplane3D(f3dTr0, f3dTr1, f3dTr2);

      FLOAT3D f3dHitted3DPoint = FLOAT3D(0, 0, 0);
      fplPlane.GetCoordinate(3, f3dHitted3DPoint);
      if (f3dHitted3DPoint(3) <= 0.0f &&  f3dHitted3DPoint(3) > fClosest) {
        fClosest = f3dHitted3DPoint(3);
        pResultPoly = pPoly;
      }
    }
  }
  
  // Return closest hit polygon and the distance where it was hit
  fHitDistance = -fClosest;
  return pResultPoly;
}

// Colorizes hitted polygon
void CModelObject::ColorizePolygon(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1, INDEX iChoosedColor,
                                   BOOL bOnColorMode)
{
  CPlacement3D plRay;
  CPlacement3D plObjectPlacement;

  projection->Prepare();
  projection->RayThroughPoint(FLOAT3D((FLOAT)x1, (FLOAT)(pDP->GetHeight() - y1), 0.0f), plRay);
  plObjectPlacement = projection->ObjectPlacementR();

  FLOAT fHitDistance;
  struct ModelPolygon *pPoly = PolygonHit(plRay, plObjectPlacement, mo_iLastRenderMipLevel, fHitDistance);
  if (pPoly != NULL)
  {
    CModelData *pMD = (CModelData*)GetData();
    MappingSurface &ms = pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_MappingSurfaces[pPoly->mp_Surface];
    if (bOnColorMode) {
      //pPoly->mp_OnColor = 1UL << iChoosedColor;
      ms.ms_ulOnColor = 1UL << iChoosedColor;
    } else {
      //pPoly->mp_OffColor = 1UL << iChoosedColor;
      ms.ms_ulOffColor = 1UL << iChoosedColor;
    }
  }
}

// TODO: Add comment here
void CModelObject::ApplySurfaceToPolygon(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1, INDEX iSurface,
                                         COLOR colSurfaceColor)
{
  CPlacement3D plRay;
  CPlacement3D plObjectPlacement;

  projection->Prepare();
  projection->RayThroughPoint(FLOAT3D((FLOAT)x1, (FLOAT)(pDP->GetHeight() - y1), 0.0f), plRay);
  plObjectPlacement = projection->ObjectPlacementR();

  FLOAT fHitDistance;
  struct ModelPolygon *pPoly = PolygonHit(plRay, plObjectPlacement, mo_iLastRenderMipLevel, fHitDistance);
  if (pPoly != NULL) {
    pPoly->mp_ColorAndAlpha = colSurfaceColor;
    pPoly->mp_Surface = iSurface;
  }
}

// Picks color from hitted polygon
void CModelObject::PickPolyColor(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1, INDEX &iPickedColorNo,
                                 BOOL bOnColorMode)
{
  CPlacement3D plRay;
  CPlacement3D plObjectPlacement;

  projection->Prepare();
  projection->RayThroughPoint(FLOAT3D((FLOAT)x1, (FLOAT)(pDP->GetHeight() - y1), 0.0f), plRay);
  plObjectPlacement = projection->ObjectPlacementR();

  FLOAT fHitDistance;
  struct ModelPolygon *pPoly = PolygonHit(plRay, plObjectPlacement, mo_iLastRenderMipLevel, fHitDistance);
  if (pPoly != NULL)
  {
    CModelData *pMD = (CModelData*)GetData();
    MappingSurface &ms = pMD->md_MipInfos[mo_iLastRenderMipLevel].mmpi_MappingSurfaces[pPoly->mp_Surface];
    
    if (bOnColorMode) {
      iPickedColorNo = GetBit(ms.ms_ulOnColor);
    } else {
      iPickedColorNo = GetBit(ms.ms_ulOffColor);
    }
  }
}


// TODO: Add comment here
INDEX CModelObject::PickPolySurface(CDrawPort *pDP, CProjection3D *projection, PIX x1, PIX y1)
{
  CPlacement3D plRay;
  CPlacement3D plObjectPlacement;

  projection->Prepare();
  projection->RayThroughPoint(FLOAT3D((FLOAT)x1, (FLOAT)(pDP->GetHeight() - y1), 0.0f), plRay);
  plObjectPlacement = projection->ObjectPlacementR();

  FLOAT fHitDistance;
  struct ModelPolygon *pPoly = PolygonHit(plRay, plObjectPlacement, mo_iLastRenderMipLevel, fHitDistance);
  if (pPoly != NULL) {
    return pPoly->mp_Surface;
  }

  return -1;
}

// Obtains index of closest vertex
INDEX CModelObject::PickVertexIndex(CDrawPort *pDP, CProjection3D *pProjection, PIX x1, PIX y1, FLOAT3D &vClosestVertex)
{
  CModelData *pMD = (CModelData*)GetData();
  
  // Project vertices for given mip model
  ProjectFrameVertices(pProjection, mo_iLastRenderMipLevel);

  FLOAT fClosest = 64.0f;
  FLOAT iClosest = -1;
  INDEX iCurrentFrame = GetFrame();
  FLOAT3D vTargetPoint = FLOAT3D(x1, pDP->GetHeight() - y1, 0.0f);
  ULONG ulVtxMask = (1L) << mo_iLastRenderMipLevel;
  
  // Find closest vertice
  for (INDEX iVertex = 0; iVertex < pMD->md_VerticesCt; iVertex++)
  {
    if (pMD->md_VertexMipMask[iVertex] & ulVtxMask)
    {
      FLOAT3D vProjected = pMD->md_TransformedVertices[iVertex].tvd_TransformedPoint;
      vProjected(3) = 0.0f;
      FLOAT3D vUncompressedVertex;
      if (pMD->md_Flags & MF_COMPRESSED_16BIT) {
        ModelFrameVertex16 *pFrame = &pMD->md_FrameVertices16[iCurrentFrame * pMD->md_VerticesCt];
        vUncompressedVertex(1) = (float)pFrame[iVertex].mfv_SWPoint(1);
        vUncompressedVertex(2) = (float)pFrame[iVertex].mfv_SWPoint(2);
        vUncompressedVertex(3) = (float)pFrame[iVertex].mfv_SWPoint(3);
      } else {
        ModelFrameVertex8 *pFrame = &pMD->md_FrameVertices8[iCurrentFrame * pMD->md_VerticesCt];
        vUncompressedVertex(1) = (float)pFrame[iVertex].mfv_SBPoint(1);
        vUncompressedVertex(2) = (float)pFrame[iVertex].mfv_SBPoint(2);
        vUncompressedVertex(3) = (float)pFrame[iVertex].mfv_SBPoint(3);
      }

      FLOAT fDistance = Abs((vProjected-vTargetPoint).Length());
      if (fDistance < fClosest) {
        fClosest = fDistance;
        iClosest = iVertex;
        vClosestVertex(1) = vUncompressedVertex(1) * pMD->md_Stretch(1);
        vClosestVertex(2) = vUncompressedVertex(2) * pMD->md_Stretch(2);
        vClosestVertex(3) = vUncompressedVertex(3) * pMD->md_Stretch(3);
      }
    }
  }
  return iClosest;
}

// Retrieves current frame's bounding box
void CModelObject::GetCurrentFrameBBox(FLOATaabbox3D &MaxBB)
{
  // Obtain model data ptr
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  
  // Get current frame
  INDEX iCurrentFrame = GetFrame();
  ASSERT(iCurrentFrame < pMD->md_FramesCt);
  
  // Set current frame's bounding box
  MaxBB = pMD->md_FrameInfos[iCurrentFrame].mfi_Box;
}

// Retrieves bounding box of all frames
void CModelObject::GetAllFramesBBox(FLOATaabbox3D &MaxBB)
{
  // Obtain model data ptr
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  
  // Get all frames bounding box
  pMD->GetAllFramesBBox(MaxBB);
}

// TODO: Add comment here
FLOAT3D CModelObject::GetCollisionBoxMin(INDEX iCollisionBox)
{
  return GetData()->GetCollisionBoxMin(iCollisionBox);
}

// TODO: Add comment here
FLOAT3D CModelObject::GetCollisionBoxMax(INDEX iCollisionBox)
{
  return GetData()->GetCollisionBoxMax(iCollisionBox);
}

// Returns HEIGHT_EQ_WIDTH, LENGHT_EQ_WIDTH or LENGHT_EQ_HEIGHT
INDEX CModelObject::GetCollisionBoxDimensionEquality(INDEX iCollisionBox)
{
  return GetData()->GetCollisionBoxDimensionEquality(iCollisionBox);
}

// Test it the model has alpha blending
BOOL CModelObject::HasAlpha(void)
{
  return GetData()->md_bHasAlpha || (mo_colBlendColor & 0xFF) != 0xFF;
}

// Retrieves number of surfaces used in given mip model
INDEX CModelObject::SurfacesCt(INDEX iMipModel)
{
  ASSERT(GetData() != NULL);
  return GetData()->md_MipInfos[iMipModel].mmpi_MappingSurfaces.Count();
};

// Retrieves number of polygons in given surface in given mip model
INDEX CModelObject::PolygonsInSurfaceCt(INDEX iMipModel, INDEX iSurface)
{
  ASSERT(GetData() != NULL);
  return GetData()->md_MipInfos[iMipModel].mmpi_MappingSurfaces[iSurface].ms_aiPolygons.Count();
};

// Adds and shows given patch
void CModelObject::ShowPatch(INDEX iMaskBit)
{
  CModelData *pMD = (CModelData*)GetData();
  ASSERT(pMD != NULL);
  if (pMD == NULL) {
    return;
  }
  
  if ((mo_PatchMask & ((1UL) << iMaskBit)) != 0) {
    return;
  }

  mo_PatchMask |= (1UL) << iMaskBit;
}

// Hides given patch
void CModelObject::HidePatch(INDEX iMaskBit)
{
  CModelData *pMD = (CModelData*)GetData();
  if (pMD == NULL) {
    return;
  }

  if ((mo_PatchMask & ((1UL) << iMaskBit)) == 0) {
    return;
  }

  mo_PatchMask &= ~((1UL) << iMaskBit);
}

// Retrieves index of mip model that casts shadow
BOOL CModelObject::HasShadow(INDEX iModelMip)
{
  CModelData *pMD = (CModelData*)GetData();
  SLONG slShadowQuality = _mrpModelRenderPrefs.GetShadowQuality();
  ASSERT(slShadowQuality >= 0);
  SLONG res = iModelMip + slShadowQuality + pMD->md_ShadowQuality;
  
  // FIXME: Replace with an expression
  if (res >= pMD->md_MipCt) {
    return FALSE;
  }
  return TRUE;
}

// Set texture data for main texture in surface of this model.
void CModelObject::SetTextureData(CTextureData *ptdNewMainTexture)
{
  mo_toTexture.SetData(ptdNewMainTexture);
}

// TODO: Add comment here
CTFileName CModelObject::GetName(void)
{
  CModelData *pmd = (CModelData*)GetData();

  if (pmd == NULL) {
    return CTString("");
  }

  return pmd->GetName();
}

// Obtain model and set it for this object
void CModelObject::SetData_t(const CTFileName &fnmModel)
{
  // If the filename is empty
  if (fnmModel == "") {
    // Release current texture
    SetData(NULL);

  // If the filename is not empty
  } else {
    // Obtain it (adds one reference)
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_MODEL, fnmModel);

    // Set it as data (adds one more reference, and remove old reference)
    SetData(static_cast<CModelData *>(pser));
    
    // Release it (removes one reference)
    _pResourceMgr->Release(pser);
    // Total reference count +1+1-1 = +1 for new data -1 for old data
  }
}

// TODO: Add comment here
void CModelObject::SetData(CModelData *pmd)
{
  RemoveAllAttachmentModels();

  //CAnimObject::SetData
  {
    // mark new data as referenced once more
    pmd->AddReference();
    // mark old data as referenced once less
    mo_pmdModelData->RemReference();
    // remember new data
    mo_pmdModelData = pmd;

    if (pmd != NULL) StartAnim(0);

    mo_aoAnimated.SetData(pmd); //CAnimObject::SetData(pmd);

    // mark that something has changed
    MarkChanged();
  }
}

// TODO: Add comment here
CModelData *CModelObject::GetData(void)
{
  return mo_pmdModelData;//(CModelData*)CAnimObject::GetData();
}

// TODO: Add comment here
void CModelObject::AutoSetTextures(void)
{
  CTFileName fnModel = GetName();
  CTFileName fnDiffuse;
  INDEX ctDiffuseTextures;
  CTFileName fnReflection;
  CTFileName fnSpecular;
  CTFileName fnBump;
  
  // Extract from model's ini file informations about attachment model's textures
  try
  {
    CTFileName fnIni = fnModel.NoExt() + ".ini";
    CTFileStream strmIni;
    strmIni.Open_t(fnIni);
    SLONG slFileSize = strmIni.GetStreamSize();
    
    // NEVER!NEVER! read after EOF
    while (strmIni.GetPos_t() < (slFileSize - 4))
    {
      CChunkID id = strmIni.PeekID_t();
      if (id == CChunkID("WTEX")) {
        CChunkID idDummy = strmIni.GetID_t();
        strmIni >> ctDiffuseTextures;
        strmIni >> fnDiffuse;
      } else if (id == CChunkID("FXTR")) {
        CChunkID idDummy = strmIni.GetID_t();
        strmIni >> fnReflection;
      } else if (id == CChunkID("FXTS")) {
        CChunkID idDummy = strmIni.GetID_t();
        strmIni >> fnSpecular;
      } else if (id == CChunkID("FXTB")) {
        CChunkID idDummy = strmIni.GetID_t();
        strmIni >> fnBump;
      } else {
        strmIni.Seek_t(1, CTStream::SD_CUR);
      }
    }
  } catch(char *strError){
    (void) strError;
  }

  try {
    if (fnDiffuse != "") {
      mo_toTexture.SetData_t(fnDiffuse);
    }
    
    if (fnReflection != "") {
      mo_toReflection.SetData_t(fnReflection);
    }
    
    if (fnSpecular != "") {
      mo_toSpecular.SetData_t(fnSpecular);
    }
    
    if (fnBump != "") {
      mo_toBump.SetData_t(fnBump);
    }
  } catch(char *strError){
    (void) strError;
  }
}

// TODO: Add comment here
void CModelObject::AutoSetAttachments(void)
{
  CTFileName fnModel = GetName();
  RemoveAllAttachmentModels();

  // Extract from model's ini file informations about attachment model's textures
  try
  {
    CTFileName fnIni = fnModel.NoExt() + ".ini";
    CTFileStream strmIni;
    strmIni.Open_t(fnIni);
    SLONG slFileSize = strmIni.GetStreamSize();
    
    // NEVER!NEVER! read after EOF
    while (strmIni.GetPos_t() < (slFileSize - 4))
    {
      CChunkID id = strmIni.PeekID_t();
      if (id == CChunkID("ATTM"))
      {
        CChunkID idDummy = strmIni.GetID_t();
        
        // Try to load attached models
        INDEX ctAttachedModels;
        strmIni >> ctAttachedModels;
        
        // Read all attached models
        for (INDEX iAtt = 0; iAtt < ctAttachedModels; iAtt++)
        {
          BOOL bVisible;
          CTString strName;
          CTFileName fnModel, fnDummy;
          strmIni >> bVisible;
          strmIni >> strName;
          
          // This data is used no more
          strmIni >> fnModel;

          INDEX iAnimation = 0;
          
          // New attached model format has saved index of animation
          if (strmIni.PeekID_t() == CChunkID("AMAN")) {
            strmIni.ExpectID_t(CChunkID("AMAN"));
            strmIni >> iAnimation;
          } else {
            strmIni >> fnDummy; // ex model's texture
          }

          if (bVisible) {
            CAttachmentModelObject *pamo = AddAttachmentModel(iAtt);
            pamo->amo_moModelObject.SetData_t(fnModel);
            pamo->amo_moModelObject.AutoSetTextures();
            pamo->amo_moModelObject.StartAnim(iAnimation);
          }
        }
      } else {
        strmIni.Seek_t(1,CTStream::SD_CUR);
      }
    }
  } catch(char *strError) {
    (void) strError;
    RemoveAllAttachmentModels();
  }

  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    itamo->amo_moModelObject.AutoSetAttachments();
  }
}

// TODO: Add comment here
CAttachmentModelObject *CModelObject::AddAttachmentModel(INDEX iAttachedPosition)
{
  CModelData *pMD = (CModelData*)GetData();

  if (pMD->md_aampAttachedPosition.Count() == 0) {
    return NULL;
  }
  ASSERT(iAttachedPosition >= 0);
  ASSERT(iAttachedPosition < pMD->md_aampAttachedPosition.Count());

  // [SEE]
  // If we already have attachment.
  if (GetAttachmentModel(iAttachedPosition)) {
    ASSERTALWAYS("Model already has attachment at iAttachedPosition!");
    return NULL;
  }

  iAttachedPosition = Clamp(iAttachedPosition, INDEX(0), INDEX(pMD->md_aampAttachedPosition.Count() - 1));
  CAttachmentModelObject *pamoNew = new CAttachmentModelObject;
  mo_lhAttachments.AddTail(pamoNew->amo_lnInMain);
  pamoNew->amo_iAttachedPosition = iAttachedPosition;
  pMD->md_aampAttachedPosition.Lock();
  pamoNew->amo_plRelative = pMD->md_aampAttachedPosition[iAttachedPosition].amp_plRelativePlacement;
  pMD->md_aampAttachedPosition.Unlock();

  return pamoNew;
}

// TODO: Add comment here
CAttachmentModelObject *CModelObject::GetAttachmentModel(INDEX ipos)
{
  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    CAttachmentModelObject &amo = *itamo;
    if (amo.amo_iAttachedPosition == ipos) {
      return &amo;
    }
  }

  return NULL;
}

// TODO: Add comment here
CAttachmentModelObject *CModelObject::GetAttachmentModelList(INDEX ipos, ...)
{
  va_list marker;
  va_start(marker, ipos);

  CAttachmentModelObject *pamo = NULL;
  CModelObject *pmo = this;

  // While not end of list
  while (ipos >= 0)
  {
    // Get attachment
    pamo = pmo->GetAttachmentModel(ipos);
    
    // If not found
    if (pamo == NULL) {
      // Return failure
      va_end(marker);
      return NULL;
    }
    
    // Get next attachment in list
    pmo = &pamo->amo_moModelObject;
    ipos = va_arg(marker, INDEX);
  }
  va_end(marker);

  // Return current attachment
  ASSERT(pamo != NULL);
  return pamo;
}

// TODO: Add comment here
void CModelObject::ResetAttachmentModelPosition(INDEX iAttachedPosition)
{
  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    if (itamo->amo_iAttachedPosition == iAttachedPosition) {
      CModelData *pMD = (CModelData*)GetData();
      pMD->md_aampAttachedPosition.Lock();
      itamo->amo_plRelative = pMD->md_aampAttachedPosition[iAttachedPosition].amp_plRelativePlacement;
      pMD->md_aampAttachedPosition.Unlock();
      return;
    }
  }
}

// TODO: Add comment here
void CModelObject::RemoveAttachmentModel(INDEX iAttachedPosition)
{
  FORDELETELIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    if (itamo->amo_iAttachedPosition == iAttachedPosition) {
      itamo->amo_lnInMain.Remove();
      delete &*itamo;
      return;
    }
  }
}

// TODO: Add comment here
void CModelObject::RemoveAllAttachmentModels(void)
{
  FORDELETELIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    itamo->amo_lnInMain.Remove();
    delete &*itamo;
  }
}

// TODO: Add comment here
void CModelObject::StretchModel(const FLOAT3D &vStretch)
{
  mo_Stretch = vStretch;
  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    itamo->amo_moModelObject.StretchModel(vStretch);
  }
}

// TODO: Add comment here
void CModelObject::StretchModelRelative(const FLOAT3D &vStretch)
{
  mo_Stretch(1) *= vStretch(1);
  mo_Stretch(2) *= vStretch(2);
  mo_Stretch(3) *= vStretch(3);
  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itamo)
  {
    itamo->amo_moModelObject.StretchModelRelative(vStretch);
  }
}

// TODO: Add comment here
void CModelObject::StretchSingleModel(const FLOAT3D &vStretch)
{
  mo_Stretch = vStretch;
}

// Get amount of memory used by this object
SLONG CModelObject::GetUsedMemory(void)
{
  // Initial size
  SLONG slUsedMemory = sizeof(CModelObject);
  
  // Add attachment(s) size
  FOREACHINLIST(CAttachmentModelObject, amo_lnInMain, mo_lhAttachments, itat)
  {
    slUsedMemory += sizeof(CAttachmentModelObject) - sizeof(CModelObject);
    itat->amo_moModelObject.GetUsedMemory();
  }
  
  // Done
  return slUsedMemory;
}


IAnimated *CModelObject::GetAnimated()
{
  return &mo_aoAnimated;
}

const IAnimated *CModelObject::GetAnimatedConst() const
{
  return &mo_aoAnimated;
}

/////////////////////////////////////////////////
// IAnimated
/////////////////////////////////////////////////

void CModelObject::NextAnim(void)
{
  GetAnimated()->NextAnim();
}

void CModelObject::PrevAnim(void)
{
  GetAnimated()->PrevAnim();
}

void CModelObject::GetAnimInfo(INDEX iAnimNo, CAnimInfo &aiInfo) const
{
  GetAnimatedConst()->GetAnimInfo(iAnimNo, aiInfo);
}

FLOAT CModelObject::GetCurrentAnimLength(void) const
{
  return GetAnimatedConst()->GetCurrentAnimLength();
}

FLOAT CModelObject::GetAnimLength(INDEX iAnim) const
{
  return GetAnimatedConst()->GetAnimLength(iAnim);
}

INDEX CModelObject::GetAnimsCt() const
{
  return GetAnimatedConst()->GetAnimsCt();
}

BOOL CModelObject::IsAnimFinished(void) const
{
  return GetAnimatedConst()->IsAnimFinished();
}

TIME CModelObject::GetStartTime(void) const
{
  return GetAnimatedConst()->GetStartTime();
}

TIME CModelObject::GetPassedTime(void) const
{
  return GetAnimatedConst()->GetPassedTime();
}

void CModelObject::StartAnim(INDEX iNew)
{
  GetAnimated()->StartAnim(iNew);
}

void CModelObject::PlayAnim(INDEX iNew, ULONG ulFlags)
{
  GetAnimated()->PlayAnim(iNew, ulFlags);
}

void CModelObject::SwitchToAnim(INDEX iNew)
{
  GetAnimated()->SwitchToAnim(iNew);
}

void CModelObject::SetAnim(INDEX iNew)
{
  GetAnimated()->SetAnim(iNew);
}

void CModelObject::ResetAnim()
{
  GetAnimated()->ResetAnim();
}

void CModelObject::PauseAnim()
{
  GetAnimated()->PauseAnim();
}

void CModelObject::ContinueAnim()
{
  GetAnimated()->ContinueAnim();
}

void CModelObject::OffsetPhase(TIME tm)
{
  GetAnimated()->OffsetPhase(tm);
}

BOOL CModelObject::IsPaused(void)
{
  return GetAnimated()->IsPaused();
}

INDEX CModelObject::GetAnim(void) const
{
  return GetAnimatedConst()->GetAnim();
}

INDEX CModelObject::GetLastAnim(void) const
{
  return GetAnimatedConst()->GetAnim();
}

void CModelObject::NextFrame(void)
{
  GetAnimated()->NextFrame();
}

void CModelObject::PrevFrame(void)
{
  GetAnimated()->PrevFrame();
}

void CModelObject::FirstFrame(void)
{
  GetAnimated()->FirstFrame();
}

void CModelObject::LastFrame(void)
{
  GetAnimated()->LastFrame();
}

INDEX CModelObject::GetFrame(void) const
{
  return GetAnimatedConst()->GetFrame();
}

INDEX CModelObject::GetFramesInCurrentAnim(void) const
{
  return GetAnimatedConst()->GetFramesInCurrentAnim();
}

void CModelObject::GetFrame(INDEX &iFrame0, INDEX &iFrame1, FLOAT &fRatio) const
{
  GetAnimatedConst()->GetFrame(iFrame0, iFrame1, fRatio);
}