/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Models/MipMaker.h>
#include <Core/Math/Vector.h>
#include <Core/IO/FileName.h>
#include <Core/Base/ErrorReporting.h>

#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

// if vertex removing should occure only inside surfaces
static BOOL _bPreserveSurfaces;


// TODO: Add comment here
CMipModel::~CMipModel()
{
  mm_amsSurfaces.Clear();
  mm_ampPolygons.Clear();
  mm_amvVertices.Clear();
}

// TODO: Add comment here
CMipVertex::CMipVertex()
{
}

// TODO: Add comment here
CMipVertex::~CMipVertex()
{
}

// TODO: Add comment here
void CMipVertex::Clear()
{
}

// TODO: Add comment here
CMipPolygon::CMipPolygon()
{
  mp_pmpvFirstPolygonVertex = NULL;
}

// TODO: Add comment here
CMipPolygon::~CMipPolygon()
{
  Clear();
}

// TODO: Add comment here
void CMipPolygon::Clear()
{
  if (mp_pmpvFirstPolygonVertex != NULL) {
    // Delete all vertices in this polygon
    CMipPolygonVertex *pmpvCurrentInPolygon = mp_pmpvFirstPolygonVertex;
    do {
      CMipPolygonVertex *pvNextInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      delete pmpvCurrentInPolygon;
      pmpvCurrentInPolygon = pvNextInPolygon;
    } while (pmpvCurrentInPolygon != mp_pmpvFirstPolygonVertex);
  }

  mp_pmpvFirstPolygonVertex = NULL;
}

// TODO: Add comment here
void CMipModel::ToObject3D(CObject3D &objDestination)
{
  // Add one sector
  CObjectSector *pOS = objDestination.ob_aoscSectors.New(1);
  
  // Add vertices to sector
  pOS->osc_aovxVertices.New(mm_amvVertices.Count());
  pOS->osc_aovxVertices.Lock();
  INDEX iVertice = 0;
  
  FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itVertice)
  {
    FLOAT3D vRestFrame = itVertice->mv_vRestFrameCoordinate;
    pOS->osc_aovxVertices[iVertice] = FLOATtoDOUBLE(vRestFrame);
    iVertice++;
  }

  // Add mip surfaces as materials to object 3d
  pOS->osc_aomtMaterials.New(mm_amsSurfaces.Count());
  pOS->osc_aomtMaterials.Lock();
  INDEX iMaterial = 0;
  FOREACHINDYNAMICARRAY(mm_amsSurfaces, CMipSurface, itSurface)
  {
    pOS->osc_aomtMaterials[iMaterial].omt_astrNames[0] = itSurface->ms_strName;
    pOS->osc_aomtMaterials[iMaterial].omt_Color = itSurface->ms_colColor;
    iMaterial ++;
  }

  // Add polygons to object 3d
  FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
  {
    // Prepare array of polygon vertex indices
    INDEX aivVertices[32];
    CMipPolygonVertex *pmpvPolygonVertex = itPolygon->mp_pmpvFirstPolygonVertex;
    INDEX ctPolygonVertices = 0;
    
    do {
      ASSERT(ctPolygonVertices < 32);
      if (ctPolygonVertices >= 32) {
        break;
      }
      
      // Add global index of vertex to list of vertex indices of polygon
      mm_amvVertices.Lock();
      aivVertices[ctPolygonVertices] = mm_amvVertices.Index(pmpvPolygonVertex->mpv_pmvVertex);
      mm_amvVertices.Unlock();
      pmpvPolygonVertex = pmpvPolygonVertex->mpv_pmpvNextInPolygon;
      ctPolygonVertices++;
    } while (pmpvPolygonVertex != itPolygon->mp_pmpvFirstPolygonVertex);
    
    // Add current polygon
    pOS->CreatePolygon(ctPolygonVertices, aivVertices, pOS->osc_aomtMaterials[itPolygon->mp_iSurface], 0, FALSE);
  }

  pOS->osc_aomtMaterials.Unlock();
  pOS->osc_aovxVertices.Unlock();
}

// TODO: Add comment here
void CMipModel::FromObject3D_t(CObject3D &objRestFrame, CObject3D &objMipSourceFrame)
{
  INDEX ctInvalidVertices = 0;
  CTString strInvalidVertices;
  char achrErrorVertice[256];
  objMipSourceFrame.ob_aoscSectors.Lock();
  objRestFrame.ob_aoscSectors.Lock();
  
  // Lock object sectors dynamic array
  objMipSourceFrame.ob_aoscSectors[0].LockAll();
  objRestFrame.ob_aoscSectors[0].LockAll();

  CObjectSector *pOS = &objMipSourceFrame.ob_aoscSectors[0];
  
  // Add mip surface
  mm_amsSurfaces.New(pOS->osc_aomtMaterials.Count());
  
  // Copy material data from object 3d to mip surfaces
  INDEX iMaterial = 0;
  FOREACHINDYNAMICARRAY(mm_amsSurfaces, CMipSurface, itSurface)
  {
    itSurface->ms_strName = pOS->osc_aomtMaterials[iMaterial].omt_astrNames[0];
    itSurface->ms_colColor = pOS->osc_aomtMaterials[iMaterial].omt_Color;
    iMaterial ++;
  }

  // Add mip vertices
  mm_amvVertices.New(pOS->osc_aovxVertices.Count());
  
  // Copy vertice coordinates from object3d to mip vertices
  INDEX iVertice = 0;
  {FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itVertice)
  {
    (FLOAT3D &)(*itVertice) = DOUBLEtoFLOAT(pOS->osc_aovxVertices[iVertice]);
    itVertice->mv_vRestFrameCoordinate = DOUBLEtoFLOAT(objRestFrame.ob_aoscSectors[0].osc_aovxVertices[iVertice]);
      
    // Calculate bounding box of all vertices
    mm_boxBoundingBox |= *itVertice;
    iVertice++;
  }}

  // Add mip polygons
  mm_ampPolygons.New(pOS->osc_aopoPolygons.Count());
  
  // Copy polygons object 3d to mip polygons
  INDEX iPolygon = 0;
  FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
  {
    CObjectPolygon &opoPolygon = pOS->osc_aopoPolygons[iPolygon];
    CMipPolygon &mpPolygon = itPolygon.Current();
    INDEX ctPolygonVertices = opoPolygon.opo_PolygonEdges.Count();
    
    // Allocate polygon vertices
    CMipPolygonVertex *ppvPolygonVertices[32];
    INDEX iPolygonVertice = 0;
    for (; iPolygonVertice < ctPolygonVertices; iPolygonVertice++)
    {
      // Allocate one polygon vertex
      ppvPolygonVertices[iPolygonVertice] = new(CMipPolygonVertex);
    }

    opoPolygon.opo_PolygonEdges.Lock();
    // For each polygon vertex in the polygon
    for (iPolygonVertice = 0; iPolygonVertice < ctPolygonVertices; iPolygonVertice++)
    {
      CMipPolygonVertex *ppvPolygonVertex = ppvPolygonVertices[iPolygonVertice];
      // Get the object vertex as first vertex of the edge
      CObjectVertex *povxStart, *povxEnd;
      opoPolygon.opo_PolygonEdges[iPolygonVertice].GetVertices(povxStart, povxEnd);
      INDEX iVertexInSector = pOS->osc_aovxVertices.Index(povxStart);
      
      // Set references to mip polygon and mip vertex
      ppvPolygonVertex->mpv_pmpPolygon = &mpPolygon;
      mm_amvVertices.Lock();
      ppvPolygonVertex->mpv_pmvVertex = &mm_amvVertices[iVertexInSector];
      mm_amvVertices.Unlock();
      
      // Link to previous and next vertices in the mip polygon
      INDEX iNext = (iPolygonVertice + 1) % ctPolygonVertices;
      ppvPolygonVertex->mpv_pmpvNextInPolygon = ppvPolygonVertices[iNext];
    }
    opoPolygon.opo_PolygonEdges.Unlock();

    // Set first polygon vertex ptr and surface index to polygon
    itPolygon->mp_pmpvFirstPolygonVertex = ppvPolygonVertices[0];
    itPolygon->mp_iSurface = pOS->osc_aomtMaterials.Index(opoPolygon.opo_Material);
    iPolygon++;
  }

	objRestFrame.ob_aoscSectors[0].UnlockAll();
  
  // Unlock all dynamic arrays in sector
	objMipSourceFrame.ob_aoscSectors[0].UnlockAll();
  objRestFrame.ob_aoscSectors.Unlock();
  objMipSourceFrame.ob_aoscSectors.Unlock();

  if (ctInvalidVertices != 0)
  {
    sprintf(achrErrorVertice, "%d invalid vertices found\n-------------------------\n\n", ctInvalidVertices);
    strInvalidVertices = CTString(achrErrorVertice) + strInvalidVertices;
    strInvalidVertices.Save_t(CTFileName(CTString("Temp\\ErrorVertices.txt")));
    ThrowF_t("%d invalid vertices found.\nUnable to create mip models.\nList of vertices "
             "that must be fixed can be found in file: \"Temp\\ErrorVertices.txt\".",
             ctInvalidVertices);
  }
}

// TODO: Add comment here
void CMipModel::CheckObjectValidity(void)
{
  // For all polygons
  FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
  {
    CMipPolygon &mpMipPolygon = *itPolygon;
    CMipPolygonVertex *pvFirstInPolygon = mpMipPolygon.mp_pmpvFirstPolygonVertex;
    CMipPolygonVertex *pvCurrent = pvFirstInPolygon;

    do {
      ASSERT(pvCurrent->mpv_pmpPolygon == &mpMipPolygon);
      pvCurrent = pvCurrent->mpv_pmpvNextInPolygon;
    } while (pvCurrent != pvFirstInPolygon);
  }
}

// TODO: Add comment here
FLOAT CMipModel::GetGoodness(CMipVertex *pmvSource, CMipVertex *pmvTarget)
{
  if ((_bPreserveSurfaces) && (pmvSource->mv_iSurface == -2)) {
    return -10000.0f;
  }

  FLOAT fDistST = (*pmvSource - *pmvTarget).Length();
  FLOAT fDistBBoxCenterT = (mm_boxBoundingBox.Center() - *pmvTarget).Length();
  return fDistBBoxCenterT / 100.0f + 1.0f / fDistST;
}

// TODO: Add comment here
INDEX CMipModel::FindSurfacesForVertices(void)
{
  {
    FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itVertice)
    {
      itVertice->mv_iSurface = -1;
    }
  }

  // For all polygons
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
    {
      // for all vertices in this polygon
      CMipPolygonVertex *pmpvCurrentInPolygon = itPolygon->mp_pmpvFirstPolygonVertex;
      do {
        CMipVertex *pmvVertex = pmpvCurrentInPolygon->mpv_pmvVertex;
        if (pmvVertex->mv_iSurface == -1) {
          pmvVertex->mv_iSurface = itPolygon->mp_iSurface;
        } else if (pmvVertex->mv_iSurface == -2) {
          // do nothing
        } else if (pmvVertex->mv_iSurface == itPolygon->mp_iSurface) {
          // do nothing
        } else {
          pmvVertex->mv_iSurface = -2;
        }
        pmpvCurrentInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      } while (pmpvCurrentInPolygon != itPolygon->mp_pmpvFirstPolygonVertex);
    }
  }

  // Count vertices that are sourounded with only one surface
  INDEX ctVerticesWithOneSurface = 0;
  // For all vertices
  {
    FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itVertice)
    {
      if (itVertice->mv_iSurface >= 0) {
        ctVerticesWithOneSurface++;
      }
    }
  }
  return ctVerticesWithOneSurface;
}

// TODO: Add comment here
void CMipModel::JoinVertexPair(CMipVertex *pmvBestSource, CMipVertex *pmvBestTarget)
{
  // For all polygons
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
    {
      // For all vertices in this polygon
      CMipPolygonVertex *pmpvCurrentInPolygon = itPolygon->mp_pmpvFirstPolygonVertex;
      do {
        if (pmpvCurrentInPolygon->mpv_pmvVertex == pmvBestSource) {
          pmpvCurrentInPolygon->mpv_pmvVertex = pmvBestTarget;
        }
        pmpvCurrentInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      } while (pmpvCurrentInPolygon != itPolygon->mp_pmpvFirstPolygonVertex);
    }
  }
  
  // Delete best source vertex
  mm_amvVertices.Delete(pmvBestSource);

  // For all polygons
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
    {
      // For all vertices in this polygon
      CMipPolygonVertex *pmpvCurrentInPolygon = itPolygon->mp_pmpvFirstPolygonVertex;
      do
      {
        CMipPolygonVertex *pmpvSuccesor = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
        
        // If current vertex and its sucessor are same mip vertex
        if (pmpvCurrentInPolygon->mpv_pmvVertex == pmpvSuccesor->mpv_pmvVertex)
        {
          // Enable looping even if vertex that is first in polygon is deleted
          if (pmpvSuccesor == itPolygon->mp_pmpvFirstPolygonVertex) {
            itPolygon->mp_pmpvFirstPolygonVertex = pmpvSuccesor->mpv_pmpvNextInPolygon;
          }
          // Relink current vertex over sucessor
          pmpvCurrentInPolygon->mpv_pmpvNextInPolygon = pmpvSuccesor->mpv_pmpvNextInPolygon;
          
          // Delete sucessor vertex
          delete pmpvSuccesor;
        }
        pmpvCurrentInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      }
      while (pmpvCurrentInPolygon != itPolygon->mp_pmpvFirstPolygonVertex);
    }
  }

  TDynamicContainer<CMipPolygon> cPolygonsToDelete;
  // For all polygons
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
    {
      CMipPolygonVertex *pmpvFirst = itPolygon->mp_pmpvFirstPolygonVertex;
      
      // If this is polygon with one or two vertices
      if ((pmpvFirst->mpv_pmpvNextInPolygon == pmpvFirst) ||
          (pmpvFirst->mpv_pmpvNextInPolygon->mpv_pmpvNextInPolygon == pmpvFirst))
      {
        // Add it to container for deleting
        cPolygonsToDelete.Add(&itPolygon.Current());
      }
    }
  }
  
  // Delete polygons
  {
    FOREACHINDYNAMICCONTAINER(cPolygonsToDelete, CMipPolygon, itPolygon)
    {
      mm_ampPolygons.Delete(&itPolygon.Current());
    }
  }
}

// TODO: Add comment here
void CMipModel::FindBestVertexPair(CMipVertex *&pmvBestSource, CMipVertex *&pmvBestTarget)
{
  pmvBestSource = NULL;
  pmvBestTarget = NULL;
  FLOAT fBestGoodnes = -999999.9f;
  
  // For all polygons
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itPolygon)
    {
      // For all vertices in this polygon
      CMipPolygonVertex *pmpvCurrentInPolygon = itPolygon->mp_pmpvFirstPolygonVertex;
      do {
        CMipVertex *pmvSource = pmpvCurrentInPolygon->mpv_pmvVertex;
        CMipVertex *pmvDestination = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon->mpv_pmvVertex;
        FLOAT fCurrentGoodnes = GetGoodness(pmvSource, pmvDestination);
        if (fCurrentGoodnes > fBestGoodnes) {
          fBestGoodnes = fCurrentGoodnes;
          pmvBestSource = pmvSource;
          pmvBestTarget = pmvDestination;
        }
        
        // Now for inverted order
        pmvSource = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon->mpv_pmvVertex;
        pmvDestination = pmpvCurrentInPolygon->mpv_pmvVertex;
        fCurrentGoodnes = GetGoodness(pmvSource, pmvDestination);
        if (fCurrentGoodnes > fBestGoodnes) {
          fBestGoodnes = fCurrentGoodnes;
          pmvBestSource = pmvSource;
          pmvBestTarget = pmvDestination;
        }
        pmpvCurrentInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      }
      while (pmpvCurrentInPolygon != itPolygon->mp_pmpvFirstPolygonVertex);
    }
  }
  ASSERT((pmvBestSource != NULL) && (pmvBestTarget != NULL));
  ASSERT(pmvBestSource != pmvBestTarget);
}

// TODO: Add comment here
void CMipModel::RemoveUnusedVertices(void)
{
  // If there are no vertices
  if (mm_amvVertices.Count() == 0) {
    return;
  }

  // Clear all vertex tags
  {FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itmvtx) {
    itmvtx->mv_bUsed = FALSE;
  }}

  // mark all vertices that are used by some polygon
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itpo)
    {
      CMipPolygonVertex *pmpvCurrentInPolygon = itpo->mp_pmpvFirstPolygonVertex;
      do {
        pmpvCurrentInPolygon->mpv_pmvVertex->mv_bUsed = TRUE;
        pmpvCurrentInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      } while (pmpvCurrentInPolygon != itpo->mp_pmpvFirstPolygonVertex);
    }
  }

  // Find number of used vertices
  INDEX ctUsedVertices = 0;
  {
    FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itmvtx)
    {
      if (itmvtx->mv_bUsed) {
        ctUsedVertices++;
      }
    }
  }

  // Create a new array with as much vertices as we have counted in last pass
  TDynamicArray<CMipVertex> amvxNew;
  CMipVertex *pmvxUsed = amvxNew.New(ctUsedVertices);

  // For each vertex
  {
    FOREACHINDYNAMICARRAY(mm_amvVertices, CMipVertex, itmvtx)
    {
      // If it is used
      if (itmvtx->mv_bUsed) {
        // Copy it to new array
        *pmvxUsed = itmvtx.Current();
        
        // Set its remap pointer into new array
        itmvtx->mv_pmvxRemap = pmvxUsed;
        pmvxUsed++;
        
      // If it is not used
      } else {
        // Clear its remap pointer (for debugging)
        #ifndef NDEBUG
        itmvtx->mv_pmvxRemap = NULL;
        #endif
      }
    }
  }

  // For each polygon
  {
    FOREACHINDYNAMICARRAY(mm_ampPolygons, CMipPolygon, itpo)
    {
      // For each polygon vertex in polygon
      CMipPolygonVertex *pmpvCurrentInPolygon = itpo->mp_pmpvFirstPolygonVertex;
      do {
        // Remap pointer to vertex
        pmpvCurrentInPolygon->mpv_pmvVertex = pmpvCurrentInPolygon->mpv_pmvVertex->mv_pmvxRemap;
        pmpvCurrentInPolygon = pmpvCurrentInPolygon->mpv_pmpvNextInPolygon;
      } while (pmpvCurrentInPolygon != itpo->mp_pmpvFirstPolygonVertex);
    }
  }

  // Use new array of vertices instead of the old one
  mm_amvVertices.Clear();
  mm_amvVertices.MoveArray(amvxNew);
}

// TODO: Add comment here
BOOL CMipModel::CreateMipModel_t(INDEX ctVerticesToRemove, INDEX iSurfacePreservingFactor)
{
  if (ctVerticesToRemove>mm_amvVertices.Count()) {
    return FALSE;
  }

  for (INDEX ctRemoved = 0; ctRemoved < ctVerticesToRemove; ctRemoved++)
  {
    INDEX ctVerticesWithOneSurface = FindSurfacesForVertices();

    // Setup flag for preserving surfaces
    _bPreserveSurfaces = TRUE;
    if ((ctVerticesWithOneSurface == 0) ||
        ((((FLOAT)ctVerticesWithOneSurface) / mm_amvVertices.Count()) * 100 <= (100 - iSurfacePreservingFactor)))
    {
      _bPreserveSurfaces = FALSE;
    }

    CMipVertex *pmvBestSource, *pmvBestTarget;
    FindBestVertexPair(pmvBestSource, pmvBestTarget);
    JoinVertexPair(pmvBestSource, pmvBestTarget);
    RemoveUnusedVertices();
    if (mm_amvVertices.Count() == 0) {
      return FALSE;
    }
  }

  return TRUE;
}
