/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MODELDATA_H
#define SE_INCL_MODELDATA_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Core/Base/CTString.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Placement.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Models/Model.h>
#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/StaticArray.h>


#define MMI_OPAQUE      (1L<<25)  // entire mip model writes to z-buffer
#define MMI_TRANSLUCENT (1L<<26)  // entire mip model doesn't write to z-buffer
// ! WARNING: to var that holds these flags is copied from surface flags which has reserved bits 20 and below !


/*
 * TODO: Add comment here
 */
class ENGINE_API CModelPatch
{
  public:
    CTString mp_strName;
    CTextureObject mp_toTexture;
    MEX2D mp_mexPosition;
    FLOAT mp_fStretch;
    
    // TODO: Add comment here
    CModelPatch(void);
    
    // TODO: Add comment here
    void Read_t(CTStream *strFile);  // throw char *
    
    // TODO: Add comment here
    void Write_t(CTStream *strFile); // throw char *
};

/*
 * TODO: Add comment here
 */
struct ENGINE_API ModelMipInfo
{
  // Constructor
  ModelMipInfo();
  
  // Destructor
  ~ModelMipInfo();
  
  INDEX mmpi_PolygonsCt;                                          // how many polygons in this mip-model
  ULONG mmpi_ulFlags;                                             // flags for this mip model
  
  TStaticArray<struct PolygonsPerPatch>   mmpi_aPolygonsPerPatch; // for each patch info telling which polygons are occupied by that patch
  TStaticArray<struct ModelPolygon>       mmpi_Polygons;          // array of polygons
  TStaticArray<struct ModelTextureVertex> mmpi_TextureVertices;   // with U,V and data for calculating U,V (for modeler) [???]
  TStaticArray<struct MappingSurface>     mmpi_MappingSurfaces;   // with HPB, zoom, name of all surfaces

  // Rendering info
  INDEX mmpi_ctMipVx;                         // TODO: Add comment here
  INDEX mmpi_ctSrfVx;                         // total number of surface vertices in this mip
  TStaticArray<UWORD>   mmpi_auwMipToMdl;     // model vertices used in this mip
  TStaticArray<UWORD>   mmpi_auwSrfToMip;     // surface vertices to mip vertices lookup
  TStaticArray<FLOAT2D> mmpi_avmexTexCoord;   // texture coordinates for each surface vertex
  TStaticArray<FLOAT3D> mmpi_avBumpU;         // bump directions for each surface vertex
  TStaticArray<FLOAT3D> mmpi_avBumpV;         // bump directions for each surface vertex
  ULONG mmpi_ulLayerFlags;                    // all texture layers needed in this mip
  INDEX mmpi_ctTriangles;                     // total triangles in this mip
  TStaticStackArray<INDEX> mmpi_aiElements;

  // Clears this mip model's arays and their sub-arrays, dealocates memory
  void Clear();
      
  // TODO: Add comment here
  void Read_t(CTStream *istrFile, BOOL bReadPolygonalPatches, BOOL bReadPolygonsPerSurface,
              BOOL bReadSurfaceColors); // throw char *

  // TODO: Add comment here            
  void Write_t(CTStream *ostrFile);  // throw char *
};

class ENGINE_API CModelData : public CAnimData
{
  public:
    INDEX md_VerticesCt;                            // number of vertices in model
    INDEX md_FramesCt;                              // number of all frames used by this model
    
    TStaticArray<struct ModelFrameVertex8> md_FrameVertices8;   // all frames and all their vertices [FramesCt*VerticesCt] (8-bit)
    TStaticArray<struct ModelFrameVertex16> md_FrameVertices16; // same but 16-bit
    TStaticArray<struct ModelFrameInfo> md_FrameInfos;          // bounding box info [FramesCt]
    TStaticArray<FLOAT3D> md_MainMipVertices;                   // global array with coordinates of all vertices of main mip-model, used for creating mip-models [VerticesCt]
    
    TStaticArray<struct TransformedVertexData> md_TransformedVertices;  // buffer with i,j,k, rotated and stretched gouraud normal [VerticesCt]
    TStaticArray<ULONG> md_VertexMipMask;                               // array of vertice masks telling in which mip model some vertex exists [VerticesCt]
    
    INDEX md_MipCt;                                 // number of mip-models
    float md_MipSwitchFactors[MAX_MODELMIPS];       // array of mip factors determing where mip-models switch
    struct ModelMipInfo md_MipInfos[MAX_MODELMIPS]; // contains how many polygons and which contains which vertices (different for each mip-model)
    MEX md_Width, md_Height;                        // size of texture, used for checking validiti of surfaces
    CModelPatch md_mpPatches[MAX_TEXTUREPATCHES];   // patches themselfs
    CTString md_ColorNames[MAX_COLOR_NAMES];        // strings containing edited color names
    ULONG md_Flags;                                 // model flags (face froward, reflesction mapping)
    SLONG md_ShadowQuality;                         // determines maximum shadow quality
    FLOAT3D md_Stretch;                             // stretch vector (static one, dynamic one is in model object)
    FLOAT3D md_vCenter;                             // center of object
    FLOAT3D md_vCompressedCenter;                   // compressed (0-255) center of object (handle)
    BOOL md_bHasAlpha;                              // set if any surface has alpha blending
    
    TDynamicArray<CModelCollisionBox> md_acbCollisionBox;   // array of collision boxes for this model
    
    BOOL md_bCollideAsCube;                         // if model colide as stretched cube
    
    TDynamicArray<CAttachedModelPosition> md_aampAttachedPosition;
    
    BOOL md_bIsEdited;                              // set if model is part of CEditModel object
    COLOR md_colDiffuse;
    COLOR md_colReflections;
    COLOR md_colSpecular;
    COLOR md_colBump;

    BOOL md_bPreparedForRendering;                  // set if model is prepared for rendering
    
  public:

    // TODO: Add comment here
    CModelData();
    
    // TODO: Add comment here
    ~CModelData();
    
    // TODO: Add comment here
    DECLARE_NOCOPYING(CModelData);

    // Reference counting (override from CAnimData)
    void RemReference_internal(void);
    
    // TODO: Add comment here
    void PtrsToIndices();
        
    // TODO: Add comment here
    void IndicesToPtrs();
        
    // Spreads mip switch factors
    void SpreadMipSwitchFactors(INDEX iFirst, float fStartingFactor);
    
    // Riches texture dimensions
    void GetTextureDimensions(MEX &mexWidth, MEX &mexHeight);
    
    // Calculates all frame's bounding box
    void GetAllFramesBBox(FLOATaabbox3D &MaxBB);    
    
    
    // TODO: Add comment here
    FLOAT3D GetCollisionBoxMin(ULONG ulCollisionBox);
    FLOAT3D GetCollisionBoxMax(ULONG ulCollisionBox);
    
    
    // Returns HEIGHT_EQ_WIDTH, LENGTH_EQ_WIDTH or LENGTH_EQ_HEIGHT
    INDEX GetCollisionBoxDimensionEquality(ULONG ulCollisionBox);
    
    // TODO: Add comment here
    ULONG GetFlags(void);
    
    // TODO: Add comment here
    void Clear(void);
    
    // Check if this kind of objects is auto-freed
    virtual BOOL IsAutoFreed(void);

    // Get amount of memory used by this object
    SLONG GetUsedMemory(void);
    
    // TODO: Add comment here
    void ClearAnimations(void);
    
    // Calculate polygons and vertices for surface
    void LinkDataForSurfaces(BOOL bFirstMip);
    
    
    // TODO: Add comment here
    void Read_t(CTStream *istrFile);   // throw char *
    
    // TODO: Add comment here
    void Write_t(CTStream *ostrFile);  // throw char *
    
    
    // Get the description of this object.
    CTString GetDescription(void);

    //! TODO: Add comment.
    virtual UBYTE GetType() const;
};


#endif  /* include-once check. */