/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include "initguid.h"

#include <Engine/Sound/SoundLibrary.h>
#include <Core/Base/Translation.h>

#include <Core/Base/Shell.h>
#include <Core/Base/Memory.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/ListIterator.inl>
#include <Core/Base/Console.h>
#include <Core/Base/Console_internal.h>
#include <Core/Base/Statistics_Internal.h>
#include <Engine/Input/IFeel.h>

#include <Engine/Sound/SoundProfile.h>
#include <Engine/Sound/SoundListener.h>
#include <Engine/Sound/SoundData.h>
#include <Engine/Sound/SoundObject.h>
#include <Engine/Sound/SoundDecoder.h>
#include <Engine/Network/Network.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/StaticStackArray.cpp>

template TStaticArray<CSoundListener>;

#pragma comment(lib, "winmm.lib")

// Pointer to global sound library object
CSoundLibrary *_pSound = NULL;

// Console variables
extern FLOAT snd_tmMixAhead  = 0.2f; // mix-ahead in seconds
extern FLOAT snd_fMasterVolume = 1.0f; // [SEE] Sound - Master Volume
extern FLOAT snd_fSoundVolume = 1.0f; // master volume for sound playing [0..1]
extern FLOAT snd_fMusicVolume = 1.0f; // master volume for music playing [0..1]

// NOTES: 
// - these 3d sound parameters have been set carefully, take extreme if changing !
// - ears distance of 20cm causes phase shift of up to 0.6ms which is very noticable
//   and is more than enough, too large values cause too much distorsions in other effects
// - pan strength needs not to be very strong, since lrfilter has panning-like influence also
// - if down filter is too large, it makes too much influence even on small elevation changes
//   and messes the situation completely

extern FLOAT snd_fDelaySoundSpeed   = 1E10;   // sound speed used for delay [m/s]
extern FLOAT snd_fDopplerSoundSpeed = 330.0f; // sound speed used for doppler [m/s]
extern FLOAT snd_fEarsDistance = 0.2f;   // distance between listener's ears
extern FLOAT snd_fPanStrength  = 0.1f;   // panning modifier (0=none, 1= full)
extern FLOAT snd_fLRFilter = 3.0f;   // filter for left-right
extern FLOAT snd_fBFilter  = 5.0f;   // filter for back
extern FLOAT snd_fUFilter  = 1.0f;   // filter for up
extern FLOAT snd_fDFilter  = 3.0f;   // filter for down

ENGINE_API extern INDEX snd_iFormat = 3;
extern INDEX snd_bMono = FALSE;
static INDEX snd_iDevice = -1;
static INDEX snd_iInterface = 2;   // 0=WaveOut, 1=DirectSound, 2=EAX
static INDEX snd_iMaxOpenRetries = 3;
static INDEX snd_iMaxExtraChannels = 32;
static FLOAT snd_tmOpenFailDelay = 0.5f;
static FLOAT snd_fEAXPanning = 0.0f;

static FLOAT snd_fNormalizer = 0.9f;
static FLOAT _fLastNormalizeValue = 1;

#include <Core\Core.h>
//extern HWND  _hwndMain; // global handle for application window
static HWND  _hwndCurrent = NULL;
static HINSTANCE _hInstDS = NULL;
static INDEX _iWriteOffset  = 0;
static INDEX _iWriteOffset2 = 0;
static BOOL  _bMuted  = FALSE;
static INDEX _iLastEnvType = 1234;
static FLOAT _fLastEnvSize = 1234;
static FLOAT _fLastPanning = 1234;


// TEMP! - for writing mixer buffer to file
static FILE *_filMixerBuffer;
static BOOL _bOpened = FALSE;


#define WAVEOUTBLOCKSIZE 1024
#define MINPAN (1.0f)
#define MAXPAN (9.0f)



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sound Library functions

// Construct uninitialized sound library.
CSoundLibrary::CSoundLibrary(void)
{
  sl_csSound.cs_iIndex = 3000;

  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Clear sound format
  memset(&sl_SwfeFormat, 0, sizeof(WAVEFORMATEX));
  sl_EsfFormat = SF_NONE;

  // Reset buffer ptrs
  sl_pslMixerBuffer   = NULL;
  sl_pswDecodeBuffer  = NULL;
  sl_pubBuffersMemory = NULL;
  
  // Clear wave out data
  sl_hwoWaveOut = NULL;

  // Clear direct sound data
  _hInstDS = NULL;
  sl_pDS   = NULL;
  sl_pKSProperty = NULL;
  sl_pDSPrimary    = NULL;
  sl_pDSSecondary  = NULL;
  sl_pDSSecondary2 = NULL;
  sl_pDSListener    = NULL;
  sl_pDSSourceLeft  = NULL;
  sl_pDSSourceRight = NULL;
  sl_bUsingDirectSound = FALSE;
  sl_bUsingEAX = FALSE;
}

// Destruct (and clean up).
CSoundLibrary::~CSoundLibrary(void)
{
  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Clear sound enviroment
  Clear();

  // Clear any installed sound decoders
  CSoundDecoder::EndPlugins();
}

// Post sound console variables' functions
static FLOAT _tmLastMixAhead = 1234;
static INDEX _iLastFormat = 1234;
static INDEX _iLastDevice = 1234;
static INDEX _iLastAPI = 1234;

static void SndPostFunc(void *pArgs)
{
  // Clamp variables
  snd_tmMixAhead = Clamp(snd_tmMixAhead, 0.1f, 0.9f);
  snd_iFormat    = Clamp(snd_iFormat, (INDEX)CSoundLibrary::SF_NONE, (INDEX)CSoundLibrary::SF_44100_16);
  snd_iDevice    = Clamp(snd_iDevice, -1L, 15L);
  snd_iInterface = Clamp(snd_iInterface, 0L, 2L);
  
  // If any variable has been changed
  if (_tmLastMixAhead != snd_tmMixAhead || _iLastFormat != snd_iFormat ||
      _iLastDevice != snd_iDevice || _iLastAPI != snd_iInterface)
  {
    // Reinit sound format
    _pSound->SetFormat((enum CSoundLibrary::SoundFormat)snd_iFormat, TRUE);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// some internal functions

// DirectSound shutdown procedure
static void ShutDown_dsound(CSoundLibrary &sl)
{
  // Free direct sound buffer(s)
  sl.sl_bUsingDirectSound = FALSE;
  sl.sl_bUsingEAX = FALSE;

  if (sl.sl_pDSSourceRight != NULL) {
    sl.sl_pDSSourceRight->Release();
    sl.sl_pDSSourceRight = NULL;
  }
  if (sl.sl_pDSSourceLeft != NULL) {
    sl.sl_pDSSourceLeft->Release();
    sl.sl_pDSSourceLeft = NULL;
  }
  if (sl.sl_pDSListener != NULL) {
    sl.sl_pDSListener->Release();
    sl.sl_pDSListener = NULL;
  }

  if (sl.sl_pDSSecondary2 != NULL) {
    sl.sl_pDSSecondary2->Stop();
    sl.sl_pDSSecondary2->Release();
    sl.sl_pDSSecondary2 = NULL;
  }
  if (sl.sl_pDSSecondary != NULL) {
    sl.sl_pDSSecondary->Stop();
    sl.sl_pDSSecondary->Release();
    sl.sl_pDSSecondary = NULL;
  }
  if (sl.sl_pDSPrimary != NULL) {
    sl.sl_pDSPrimary->Stop();
    sl.sl_pDSPrimary->Release();
    sl.sl_pDSPrimary = NULL;
  }

  if (sl.sl_pKSProperty != NULL) {
    sl.sl_pKSProperty->Release();
    sl.sl_pKSProperty = NULL;
  }

  // Free direct sound object
  if (sl.sl_pDS != NULL) {
    // Reset cooperative level
    if (_hwndCurrent != NULL) sl.sl_pDS->SetCooperativeLevel(_hwndCurrent, DSSCL_NORMAL);
    sl.sl_pDS->Release();
    sl.sl_pDS = NULL;
  }
  
  // Free direct sound library
  if (_hInstDS != NULL) {
    FreeLibrary(_hInstDS);
    _hInstDS = NULL;
  }
  
  // Free memory
  if (sl.sl_pslMixerBuffer != NULL) {
    FreeMemory(sl.sl_pslMixerBuffer);
    sl.sl_pslMixerBuffer = NULL;
  }
  
  if (sl.sl_pswDecodeBuffer != NULL) {
    FreeMemory(sl.sl_pswDecodeBuffer);
    sl.sl_pswDecodeBuffer = NULL;
  }
}

// Set wave format from library format
static void SetWaveFormat(CSoundLibrary::SoundFormat EsfFormat, WAVEFORMATEX &wfeFormat)
{
  // Change Library Wave Format
  memset(&wfeFormat, 0, sizeof(WAVEFORMATEX));
  wfeFormat.wFormatTag = WAVE_FORMAT_PCM;
  wfeFormat.nChannels = 2;
  wfeFormat.wBitsPerSample = 16;
  switch (EsfFormat)
  {
    case CSoundLibrary::SF_11025_16: wfeFormat.nSamplesPerSec = 11025; break;
    case CSoundLibrary::SF_22050_16: wfeFormat.nSamplesPerSec = 22050; break;
    case CSoundLibrary::SF_44100_16: wfeFormat.nSamplesPerSec = 44100; break;
    case CSoundLibrary::SF_NONE: ASSERTALWAYS("Can't set to NONE format"); break;
    default:                     ASSERTALWAYS("Unknown Sound format");     break;
  }
  wfeFormat.nBlockAlign     = (wfeFormat.wBitsPerSample / 8) * wfeFormat.nChannels;
  wfeFormat.nAvgBytesPerSec = wfeFormat.nSamplesPerSec * wfeFormat.nBlockAlign;
}


// Set library format from wave format
static void SetLibraryFormat(CSoundLibrary &sl)
{
  // If library format is none return
  if (sl.sl_EsfFormat == CSoundLibrary::SF_NONE) {
    return;
  }
  
  // else check wave format to determine library format
  ULONG ulFormat = sl.sl_SwfeFormat.nSamplesPerSec;
  
  // Find format
  switch (ulFormat)
  {
    case 11025: sl.sl_EsfFormat = CSoundLibrary::SF_11025_16; break;
    case 22050: sl.sl_EsfFormat = CSoundLibrary::SF_22050_16; break;
    case 44100: sl.sl_EsfFormat = CSoundLibrary::SF_44100_16; break;
    
    // Unknown format
    default:
      ASSERTALWAYS("Unknown sound format");
      FatalError(TRANS("Unknown sound format"));
      sl.sl_EsfFormat = CSoundLibrary::SF_ILLEGAL;
  }
}

// TODO: Add comment here
static BOOL DSFail(CSoundLibrary &sl, char *strError) 
{
  CErrorF(strError);
  ShutDown_dsound(sl);
  snd_iInterface = 1; // if EAX failed -> try DirectSound
  return FALSE;
}

// Some helper functions for DirectSound
static BOOL DSInitSecondary(CSoundLibrary &sl, LPDIRECTSOUNDBUFFER &pBuffer, SLONG slSize)
{
  // Eventuallt adjust for EAX
  DWORD dwFlag3D = NONE;
  if (snd_iInterface == 2) {
    dwFlag3D = DSBCAPS_CTRL3D;
    sl.sl_SwfeFormat.nChannels = 1;  // mono output
    sl.sl_SwfeFormat.nBlockAlign /= 2;
    sl.sl_SwfeFormat.nAvgBytesPerSec /= 2;
    slSize /= 2;
  }
  DSBUFFERDESC dsBuffer;
  memset(&dsBuffer, 0, sizeof(dsBuffer));
  dsBuffer.dwSize  = sizeof(DSBUFFERDESC);
  dsBuffer.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | dwFlag3D;
  dsBuffer.dwBufferBytes = slSize; 
  dsBuffer.lpwfxFormat = &sl.sl_SwfeFormat;
  HRESULT hResult = sl.sl_pDS->CreateSoundBuffer(&dsBuffer, &pBuffer, NULL);
  
  if (snd_iInterface == 2) {
    // Revert back to original wave format (stereo)
    sl.sl_SwfeFormat.nChannels = 2;
    sl.sl_SwfeFormat.nBlockAlign *= 2;
    sl.sl_SwfeFormat.nAvgBytesPerSec *= 2;
  }
  if (hResult != DS_OK) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot create secondary buffer.\n"));
  }
  return TRUE;
}

// TODO: Add comment here
static BOOL DSLockBuffer(CSoundLibrary &sl, LPDIRECTSOUNDBUFFER pBuffer, SLONG slSize, LPVOID &lpData, DWORD &dwSize)
{
  INDEX ctRetries = 1000;  // too much?
  if (sl.sl_bUsingEAX) {
    slSize /= 2; // buffer is mono in case of EAX
  }
  
  FOREVER
  {
    HRESULT hResult = pBuffer->Lock(0, slSize, &lpData, &dwSize, NULL, NULL, 0);
    if (hResult == DS_OK && slSize == dwSize) {
      return TRUE;
    }
    
    if (hResult != DSERR_BUFFERLOST) {
      return DSFail(sl, TRANS("  ! DirectSound error: Cannot lock sound buffer.\n"));
    }
    
    if (ctRetries-- == 0) {
      return DSFail(sl, TRANS("  ! DirectSound error: Couldn't restore sound buffer.\n"));
    }
    pBuffer->Restore();
  }
}
 
// TODO: Add comment here
static void DSPlayBuffers(CSoundLibrary &sl)
{
  DWORD dw;
  BOOL bInitiatePlay = FALSE;
  ASSERT(sl.sl_pDSSecondary != NULL && sl.sl_pDSPrimary != NULL);
  if (sl.sl_bUsingEAX && sl.sl_pDSSecondary2->GetStatus(&dw) == DS_OK && !(dw & DSBSTATUS_PLAYING)) {
    bInitiatePlay = TRUE;
  }
  
  if (sl.sl_pDSSecondary->GetStatus(&dw) == DS_OK && !(dw & DSBSTATUS_PLAYING)) {
    bInitiatePlay = TRUE;
  }
  
  if (sl.sl_pDSPrimary->GetStatus(&dw) == DS_OK && !(dw & DSBSTATUS_PLAYING)) {
    bInitiatePlay = TRUE;
  }

  // Done if all buffers are already playing
  if (!bInitiatePlay) {
    return;
  }
  
  // Stop buffers (in case some buffers are playing
  sl.sl_pDSPrimary->Stop();
  sl.sl_pDSSecondary->Stop();
  if (sl.sl_bUsingEAX) {
    sl.sl_pDSSecondary2->Stop();
  }
  
  // Check sound buffer lock and clear sound buffer(s) 
  LPVOID lpData;
  DWORD	 dwSize;
  if (!DSLockBuffer(sl, sl.sl_pDSSecondary, sl.sl_slMixerBufferSize, lpData, dwSize)) {
    return;
  }
  
  memset(lpData, 0, dwSize);
  sl.sl_pDSSecondary->Unlock(lpData, dwSize, NULL, 0);
  if (sl.sl_bUsingEAX)
  { 
    if (!DSLockBuffer(sl, sl.sl_pDSSecondary2, sl.sl_slMixerBufferSize, lpData, dwSize)) {
      return;
    }
    memset(lpData, 0, dwSize); 
    sl.sl_pDSSecondary2->Unlock(lpData, dwSize, NULL, 0);
    
    // Start playing EAX additional buffer
    sl.sl_pDSSecondary2->Play(0, 0, DSBPLAY_LOOPING);
  }
  
  // Start playing standard DirectSound buffers
  sl.sl_pDSPrimary->Play(0, 0, DSBPLAY_LOOPING);
  sl.sl_pDSSecondary->Play(0, 0, DSBPLAY_LOOPING);
  _iWriteOffset  = 0;
  _iWriteOffset2 = 0;

  // Adjust starting offsets for EAX
  if (sl.sl_bUsingEAX)
  {
    DWORD dwCursor1, dwCursor2;
    SLONG slMinDelta = MAX_SLONG;
    for (INDEX i = 0; i < 10; i++)  // shoud be enough to screw interrupts
    { 
      sl.sl_pDSSecondary->GetCurrentPosition(&dwCursor1, NULL);
      sl.sl_pDSSecondary2->GetCurrentPosition(&dwCursor2, NULL);
      
      SLONG slDelta1 = dwCursor2 - dwCursor1;
      sl.sl_pDSSecondary2->GetCurrentPosition(&dwCursor2, NULL);
      sl.sl_pDSSecondary->GetCurrentPosition(&dwCursor1, NULL);
      
      SLONG slDelta2 = dwCursor2 - dwCursor1;
      SLONG slDelta  = (slDelta1 + slDelta2) / 2;
      if (slDelta < slMinDelta) {
        slMinDelta = slDelta;
      }
      //CDebugF("D1=%5d,  D2=%5d,  AD=%5d,  MD=%5d\n", slDelta1, slDelta2, slDelta, slMinDelta);
    }
    
    if (slMinDelta < 0) {
      _iWriteOffset  = -slMinDelta * 2; // 2 because of offset is stereo
    }
    if (slMinDelta > 0) {
      _iWriteOffset2 = +slMinDelta * 2; 
    }
    _iWriteOffset  += _iWriteOffset  & 3;  // round to 4 bytes
    _iWriteOffset2 += _iWriteOffset2 & 3;

    // Assure that first writing offsets are inside buffers
    if (_iWriteOffset  >= sl.sl_slMixerBufferSize) {
      _iWriteOffset  -= sl.sl_slMixerBufferSize;
    }
    if (_iWriteOffset2 >= sl.sl_slMixerBufferSize) {
      _iWriteOffset2 -= sl.sl_slMixerBufferSize;
    }
    ASSERT(_iWriteOffset  >= 0 && _iWriteOffset  < sl.sl_slMixerBufferSize);
    ASSERT(_iWriteOffset2 >= 0 && _iWriteOffset2 < sl.sl_slMixerBufferSize);
  }
}

// Init and set DirectSound format (internal)
static BOOL StartUp_dsound(CSoundLibrary &sl, BOOL bReport=TRUE)
{
  // Startup
  sl.sl_bUsingDirectSound = FALSE;
  ASSERT(_hInstDS == NULL && sl.sl_pDS == NULL);
  ASSERT(sl.sl_pDSSecondary == NULL && sl.sl_pDSPrimary == NULL);
  
  // Update window handle (just in case)
  HRESULT (WINAPI *pDirectSoundCreate)(GUID FAR *lpGUID, LPDIRECTSOUND FAR *lplpDS, IUnknown FAR *pUnkOuter);
  
  if (bReport) {
    CInfoF(TRANS("Direct Sound initialization ...\n"));
  }
  ASSERT(_hInstDS == NULL);
  _hInstDS = LoadLibraryA("dsound.dll");
  if (_hInstDS == NULL) {
    CErrorF(TRANS("  ! DirectSound error: Cannot load 'DSOUND.DLL'.\n"));
    return FALSE;
  }
  
  // Get main procedure address
  pDirectSoundCreate = (HRESULT(WINAPI *)(GUID FAR *, LPDIRECTSOUND FAR *, IUnknown FAR *))GetProcAddress(_hInstDS, "DirectSoundCreate");
  if (pDirectSoundCreate == NULL) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot get procedure address.\n"));
  }

  // Init dsound
  HRESULT	hResult;
  hResult = pDirectSoundCreate(NULL, &sl.sl_pDS, NULL);
  if (hResult != DS_OK) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot create object.\n"));
  }
  
  // Get capabilities
  DSCAPS dsCaps;
  dsCaps.dwSize = sizeof(dsCaps);
  hResult = sl.sl_pDS->GetCaps(&dsCaps);
  if (hResult != DS_OK) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot determine capabilites.\n"));
  }
  
  // Fail if in emulation mode
  if (dsCaps.dwFlags & DSCAPS_EMULDRIVER) {
    CErrorF(TRANS("  ! DirectSound error: No driver installed.\n"));
    ShutDown_dsound(sl);
    return FALSE;
  }

  // Set cooperative level to priority
  _hwndCurrent = _hwndMain;
  hResult = sl.sl_pDS->SetCooperativeLevel(_hwndCurrent, DSSCL_PRIORITY);
  if (hResult != DS_OK) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot set cooperative level.\n"));
  }
  
  // Prepare 3D flag if EAX
  DWORD dwFlag3D = NONE;
  if (snd_iInterface == 2) {
    dwFlag3D = DSBCAPS_CTRL3D;
  }
  
  // Create primary sound buffer (must have one)
  DSBUFFERDESC dsBuffer;
  memset(&dsBuffer, 0, sizeof(dsBuffer));
  dsBuffer.dwSize  = sizeof(dsBuffer);
  dsBuffer.dwFlags = DSBCAPS_PRIMARYBUFFER | dwFlag3D;
  dsBuffer.dwBufferBytes = 0;
  dsBuffer.lpwfxFormat = NULL;
  hResult = sl.sl_pDS->CreateSoundBuffer(&dsBuffer, &sl.sl_pDSPrimary, NULL);
  if (hResult != DS_OK) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot create primary sound buffer.\n"));
  }
  
  // Set primary buffer format
  WAVEFORMATEX wfx = sl.sl_SwfeFormat;
  hResult = sl.sl_pDSPrimary->SetFormat(&wfx);
  if (hResult != DS_OK) {
    return DSFail(sl, TRANS("  ! DirectSound error: Cannot set primary sound buffer format.\n"));
  }
  
  // Startup secondary sound buffer(s)
  SLONG slBufferSize = (SLONG)(ceil(snd_tmMixAhead * sl.sl_SwfeFormat.nSamplesPerSec) *
                                    sl.sl_SwfeFormat.wBitsPerSample / 8 * sl.sl_SwfeFormat.nChannels);
  if (!DSInitSecondary(sl, sl.sl_pDSSecondary, slBufferSize)) {
    return FALSE;
  }
  
  // Set some additionals for EAX
  if (snd_iInterface == 2)
  {
    // 2nd secondary buffer
    if (!DSInitSecondary(sl, sl.sl_pDSSecondary2, slBufferSize)) {
      return FALSE;
    }
    
    // Set 3D for all buffers
    HRESULT hr1,hr2,hr3,hr4;
    hr1 = sl.sl_pDSPrimary->QueryInterface(IID_IDirectSound3DListener, (LPVOID*)&sl.sl_pDSListener);
    hr2 = sl.sl_pDSSecondary->QueryInterface(IID_IDirectSound3DBuffer, (LPVOID*)&sl.sl_pDSSourceLeft);
    hr3 = sl.sl_pDSSecondary2->QueryInterface(IID_IDirectSound3DBuffer, (LPVOID*)&sl.sl_pDSSourceRight);
    if (hr1 != DS_OK || hr2 != DS_OK || hr3 != DS_OK) {
      return DSFail(sl, TRANS("  ! DirectSound3D error: Cannot set 3D sound buffer.\n"));
    }
    
    hr1 = sl.sl_pDSListener->SetPosition(0, 0 , 0, DS3D_DEFERRED);
    hr2 = sl.sl_pDSListener->SetOrientation(0, 0, 1, 0, 1, 0, DS3D_DEFERRED);
    hr3 = sl.sl_pDSListener->SetRolloffFactor(1, DS3D_DEFERRED);
    if (hr1 != DS_OK || hr2 != DS_OK || hr3 != DS_OK) {
      return DSFail(sl, TRANS("  ! DirectSound3D error: Cannot set 3D parameters for listener.\n"));
    }
    hr1 = sl.sl_pDSSourceLeft->SetMinDistance(MINPAN, DS3D_DEFERRED);
    hr2 = sl.sl_pDSSourceLeft->SetMaxDistance(MAXPAN, DS3D_DEFERRED);
    hr3 = sl.sl_pDSSourceRight->SetMinDistance(MINPAN, DS3D_DEFERRED);
    hr4 = sl.sl_pDSSourceRight->SetMaxDistance(MAXPAN, DS3D_DEFERRED);
    if (hr1 != DS_OK || hr2 != DS_OK || hr3 != DS_OK || hr4 != DS_OK) {
      return DSFail(sl, TRANS("  ! DirectSound3D error: Cannot set 3D parameters for sound source.\n"));
    }
    
    // Apply
    hResult = sl.sl_pDSListener->CommitDeferredSettings();
    if (hResult != DS_OK) {
      return DSFail(sl, TRANS("  ! DirectSound3D error: Cannot apply 3D parameters.\n"));
    }
    
    // Reset EAX parameters
    _fLastPanning = 1234; 
    _iLastEnvType = 1234;
    _fLastEnvSize = 1234;

    // Query property interface to EAX
    hResult = sl.sl_pDSSourceLeft->QueryInterface(IID_IKsPropertySet, (LPVOID*)&sl.sl_pKSProperty);
    if (hResult != DS_OK) {
      return DSFail(sl, TRANS("  ! EAX error: Cannot set property interface.\n"));
    }
    
    // Query support
    ULONG ulSupport = 0;
    hResult = sl.sl_pKSProperty->QuerySupport(DSPROPSETID_EAX_ListenerProperties, DSPROPERTY_EAXLISTENER_ENVIRONMENT, &ulSupport);
    if (hResult != DS_OK || !(ulSupport & KSPROPERTY_SUPPORT_SET)) {
      return DSFail(sl, TRANS("  ! EAX error: Cannot query property support.\n"));
    }
    
    hResult = sl.sl_pKSProperty->QuerySupport(DSPROPSETID_EAX_ListenerProperties, DSPROPERTY_EAXLISTENER_ENVIRONMENTSIZE, &ulSupport);
    if (hResult != DS_OK || !(ulSupport & KSPROPERTY_SUPPORT_SET)) {
      return DSFail(sl, TRANS("  ! EAX error: Cannot query property support.\n"));
    }
    
    // Made it - EAX's on!
    sl.sl_bUsingEAX = TRUE;
  }

  // Mark that dsound is operative and set mixer buffer size (decoder buffer always works at 44khz)
  _iWriteOffset  = 0;
  _iWriteOffset2 = 0;
  sl.sl_bUsingDirectSound  = TRUE;
  sl.sl_slMixerBufferSize  = slBufferSize;
  sl.sl_slDecodeBufferSize = sl.sl_slMixerBufferSize * 
                             ((44100 + sl.sl_SwfeFormat.nSamplesPerSec - 1) / sl.sl_SwfeFormat.nSamplesPerSec);
  
  // Allocate mixing and decoding buffers
  sl.sl_pslMixerBuffer  = (SLONG*)AllocMemory(sl.sl_slMixerBufferSize * 2); // (*2 because of 32-bit buffer)
  sl.sl_pswDecodeBuffer = (SWORD*)AllocMemory(sl.sl_slDecodeBufferSize + 4); // (+4 because of linear interpolation of last samples)

  // Report success
  if (bReport)
  {
    CTString strDevice = TRANS("default device");
    if (snd_iDevice >= 0) {
      strDevice.PrintF(TRANS("device %d"), snd_iDevice); 
    }
    
    CInfoF(TRANS("  %dHz, %dbit, %s, mix-ahead: %gs\n"), sl.sl_SwfeFormat.nSamplesPerSec, 
                                                          sl.sl_SwfeFormat.wBitsPerSample,
                                                          strDevice,
                                                          snd_tmMixAhead); 
    CInfoF(TRANS("  mixer buffer size:  %d KB\n"), sl.sl_slMixerBufferSize  / 1024);
    CInfoF(TRANS("  decode buffer size: %d KB\n"), sl.sl_slDecodeBufferSize / 1024);
    
    // EAX?
    CTString strEAX = TRANS("Disabled");
    if (sl.sl_bUsingEAX) {
      strEAX = TRANS("Enabled");
    }
    CInfoF(TRANS("  EAX: %s\n"), strEAX);
  } 
  // Done
  return TRUE;
}

// Set WaveOut format (internal)
static INDEX _ctChannelsOpened = 0;
static BOOL StartUp_waveout(CSoundLibrary &sl, BOOL bReport=TRUE)
{
  // Not using DirectSound (obviously)
  sl.sl_bUsingDirectSound = FALSE;
  sl.sl_bUsingEAX = FALSE;
  if (bReport) {
    CInfoF(TRANS("WaveOut initialization ...\n"));
  }
  
  // Set maximum total number of retries for device opening
  INDEX ctMaxRetries = snd_iMaxOpenRetries;
  _ctChannelsOpened = 0;
  MMRESULT res;
  // Repeat
  FOREVER
  {
    // Try to open wave device
    HWAVEOUT hwo;
    res = waveOutOpen(&hwo, (snd_iDevice < 0)?WAVE_MAPPER:snd_iDevice, &sl.sl_SwfeFormat, NULL, NULL, NONE);
    // If opened
    if (res == MMSYSERR_NOERROR)
    {
      _ctChannelsOpened++;
      
      // If first one
      if (_ctChannelsOpened == 1) {
        sl.sl_hwoWaveOut = hwo;         // remember as used waveout
        
      // if extra channel
      } else {
        sl.sl_ahwoExtra.Push() = hwo;   // remember under extra
      }
      
      // If no extra channels should be taken
      if (_ctChannelsOpened >= snd_iMaxExtraChannels + 1) {
        break;  // no more tries
      } 
      
    // If cannot open
    } else {
      // Decrement retry counter
      ctMaxRetries--;
      
      // If no more retries
      if (ctMaxRetries < 0) {
        break;        // quit trying
      
      // If more retries left
      } else {
        // Wait a bit (probably sound-scheme is playing)
        Sleep(int(snd_tmOpenFailDelay * 1000));
      }
    }
  }

  // If couldn't set format
  if (_ctChannelsOpened == 0 && res != MMSYSERR_NOERROR)
  {
    // Report error
    CTString strError;
    switch (res)
    {
      case MMSYSERR_ALLOCATED:    strError = TRANS("Device already in use.");     break;
      case MMSYSERR_BADDEVICEID:  strError = TRANS("Bad device number.");         break;
      case MMSYSERR_NODRIVER:     strError = TRANS("No driver installed.");       break;
      case MMSYSERR_NOMEM:        strError = TRANS("Memory allocation problem."); break;
      case WAVERR_BADFORMAT:      strError = TRANS("Unsupported data format.");   break;
      case WAVERR_SYNC:           strError = TRANS("Wrong flag?");                break;
      default: strError.PrintF("%d", res);
    };
    CErrorF(TRANS("  ! WaveOut error: %s\n"), strError);
    return FALSE;
  }

  // Get waveout capabilities
  WAVEOUTCAPS woc;
  memset(&woc, 0, sizeof(woc));
  res = waveOutGetDevCaps((int)sl.sl_hwoWaveOut, &woc, sizeof(woc));
  
  // Report success
  if (bReport)
  {
    CTString strDevice = TRANS("default device");
    if (snd_iDevice >= 0) {
      strDevice.PrintF(TRANS("device %d"), snd_iDevice); 
    }
    CInfoF(TRANS("  opened device: %s\n"), woc.szPname);
    CInfoF(TRANS("  %dHz, %dbit, %s\n"), 
             sl.sl_SwfeFormat.nSamplesPerSec, sl.sl_SwfeFormat.wBitsPerSample, strDevice);
  }

  // Determine whole mixer buffer size from mixahead console variable
  sl.sl_slMixerBufferSize = (SLONG)(ceil(snd_tmMixAhead * sl.sl_SwfeFormat.nSamplesPerSec) *
                                         sl.sl_SwfeFormat.wBitsPerSample / 8 * sl.sl_SwfeFormat.nChannels);
                                         
  // Align size to be next multiply of WAVEOUTBLOCKSIZE
  sl.sl_slMixerBufferSize += WAVEOUTBLOCKSIZE - (sl.sl_slMixerBufferSize % WAVEOUTBLOCKSIZE);
  
  // Determine number of WaveOut buffers
  const INDEX ctWOBuffers = sl.sl_slMixerBufferSize / WAVEOUTBLOCKSIZE;
  
  // Decoder buffer always works at 44khz
  sl.sl_slDecodeBufferSize = sl.sl_slMixerBufferSize *
                             ((44100 + sl.sl_SwfeFormat.nSamplesPerSec - 1) / sl.sl_SwfeFormat.nSamplesPerSec);
  if (bReport) {
    CInfoF(TRANS("  parameters: %d Hz, %d bit, stereo, mix-ahead: %gs\n"),
            sl.sl_SwfeFormat.nSamplesPerSec, sl.sl_SwfeFormat.wBitsPerSample, snd_tmMixAhead);
    CInfoF(TRANS("  output buffers: %d x %d bytes\n"), ctWOBuffers, WAVEOUTBLOCKSIZE),
    CInfoF(TRANS("  mpx decode: %d bytes\n"), sl.sl_slDecodeBufferSize),
    CInfoF(TRANS("  extra sound channels taken: %d\n"), _ctChannelsOpened-1);
  }

  // Initialise waveout sound buffers
  sl.sl_pubBuffersMemory = (UBYTE*)AllocMemory(sl.sl_slMixerBufferSize);
  memset(sl.sl_pubBuffersMemory, 0, sl.sl_slMixerBufferSize);
  sl.sl_awhWOBuffers.New(ctWOBuffers); 
  for (INDEX iBuffer = 0; iBuffer < sl.sl_awhWOBuffers.Count(); iBuffer++)
  {
    WAVEHDR &wh = sl.sl_awhWOBuffers[iBuffer];
    wh.lpData = (char*)(sl.sl_pubBuffersMemory + iBuffer * WAVEOUTBLOCKSIZE);
    wh.dwBufferLength = WAVEOUTBLOCKSIZE;
    wh.dwFlags = 0;
  }
  
  // Initialize mixing and decoding buffer
  sl.sl_pslMixerBuffer  = (SLONG*)AllocMemory(sl.sl_slMixerBufferSize * 2); // (*2 because of 32-bit buffer)
  sl.sl_pswDecodeBuffer = (SWORD*)AllocMemory(sl.sl_slDecodeBufferSize + 4); // (+4 because of linear interpolation of last samples)

  // Done
  return TRUE;
}

// Set sound format
static void SetFormat_internal(CSoundLibrary &sl, CSoundLibrary::SoundFormat EsfNew, BOOL bReport)
{
  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl.sl_csSound, TRUE);

  // Remember library format
  sl.sl_EsfFormat = EsfNew;
  
  // Release library
  sl.ClearLibrary();

  // If none skip initialization
  _fLastNormalizeValue = 1;
  if (bReport) {
    CInfoF(TRANS("Setting sound format ...\n"));
  }
  
  if (sl.sl_EsfFormat == CSoundLibrary::SF_NONE)
  {
    if (bReport) {
      CInfoF(TRANS("  (no sound)\n"));
    }
    return;
  }

  // Set wave format from library format
  SetWaveFormat(EsfNew, sl.sl_SwfeFormat);
  snd_iDevice    = Clamp(snd_iDevice, -1L, (INDEX)(sl.sl_ctWaveDevices - 1));
  snd_tmMixAhead = Clamp(snd_tmMixAhead, 0.1f, 0.9f);
  snd_iInterface = Clamp(snd_iInterface, 0L, 2L);

  BOOL bSoundOK = FALSE;
  if (snd_iInterface == 2) {
    // If wanted, 1st try to set EAX
    bSoundOK = StartUp_dsound(sl, bReport);  
  }
  if (!bSoundOK && snd_iInterface == 1) {
    // If wanted, 2nd try to set DirectSound
    bSoundOK = StartUp_dsound(sl, bReport);  
  }
  
  // If DirectSound failed or not wanted
  if (!bSoundOK) { 
    // Try waveout
    bSoundOK = StartUp_waveout(sl, bReport); 
    snd_iInterface = 0; // mark that DirectSound didn't make it
  }

  // If didn't make it by now
  if (bReport) {
    CInfoF("\n");
  }
  if (!bSoundOK) {
    // Revert to none in case sound init was unsuccessful
    sl.sl_EsfFormat = CSoundLibrary::SF_NONE;
    return;
  } 
  
  // Set library format from wave format
  SetLibraryFormat(sl);

  // Add timer handler
  _pTimer->AddHandler(&sl.sl_thTimerHandler);
}

// Initialization
void CSoundLibrary::Init(void)
{
  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  _pShell->DeclareSymbol("void SndPostFunc(INDEX);", &SndPostFunc);

  _pShell->DeclareSymbol("           user INDEX snd_bMono;", &snd_bMono);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fEarsDistance;",      &snd_fEarsDistance);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fDelaySoundSpeed;",   &snd_fDelaySoundSpeed);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fDopplerSoundSpeed;", &snd_fDopplerSoundSpeed);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fPanStrength;", &snd_fPanStrength);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fLRFilter;",    &snd_fLRFilter);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fBFilter;",     &snd_fBFilter);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fUFilter;",     &snd_fUFilter);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fDFilter;",     &snd_fDFilter);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fMasterVolume;", &snd_fMasterVolume); // [SEE] Sound - Master Volume
  _pShell->DeclareSymbol("persistent user FLOAT snd_fSoundVolume;", &snd_fSoundVolume);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fMusicVolume;", &snd_fMusicVolume);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fNormalizer;",  &snd_fNormalizer);
  _pShell->DeclareSymbol("persistent user FLOAT snd_tmMixAhead post:SndPostFunc;", &snd_tmMixAhead);
  _pShell->DeclareSymbol("persistent user INDEX snd_iInterface post:SndPostFunc;", &snd_iInterface);
  _pShell->DeclareSymbol("persistent user INDEX snd_iDevice post:SndPostFunc;", &snd_iDevice);
  _pShell->DeclareSymbol("persistent user INDEX snd_iFormat post:SndPostFunc;", &snd_iFormat);
  _pShell->DeclareSymbol("persistent user INDEX snd_iMaxExtraChannels;", &snd_iMaxExtraChannels);
  _pShell->DeclareSymbol("persistent user INDEX snd_iMaxOpenRetries;",   &snd_iMaxOpenRetries);
  _pShell->DeclareSymbol("persistent user FLOAT snd_tmOpenFailDelay;",   &snd_tmOpenFailDelay);
  _pShell->DeclareSymbol("persistent user FLOAT snd_fEAXPanning;", &snd_fEAXPanning);
  
  // Print header
  CInfoF(TRANS("Initializing sound...\n"));

  // Initialize sound library and set no-sound format
  SetFormat(SF_NONE);

  // Initialize any installed sound decoders
  CSoundDecoder::InitPlugins();

  // Get number of devices
  INDEX ctDevices = waveOutGetNumDevs();
  CInfoF(TRANS("  Detected devices: %d\n"), ctDevices);
  sl_ctWaveDevices = ctDevices;
  
  // For each device
  for (INDEX iDevice = 0; iDevice < ctDevices; iDevice++)
  {
    // Get description
    WAVEOUTCAPS woc;
    memset(&woc, 0, sizeof(woc));
    MMRESULT res = waveOutGetDevCaps(iDevice, &woc, sizeof(woc));
    CInfoF(TRANS("    device %d: %s\n"), iDevice, woc.szPname);
    CInfoF(TRANS("      ver: %d, id: %d.%d\n"), woc.vDriverVersion, woc.wMid, woc.wPid);
    CInfoF(TRANS("      form: 0x%08x, ch: %d, support: 0x%08x\n"), woc.dwFormats, woc.wChannels, woc.dwSupport);
  }
  
  // Done
  CInfoF("\n");
}

// Clear Sound Library
void CSoundLibrary::Clear(void) {
  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Clear all sounds and datas buffers
  {
    FOREACHINLIST(CSoundData, sd_Node, sl_ClhAwareList, itCsdStop)
    {
      FOREACHINLIST(CSoundObject, so_Node, (itCsdStop->sd_ClhLinkList), itCsoStop)
      {
        itCsoStop->Stop();
      }
      itCsdStop->ClearBuffer();
    }
  }

  // Clear wave out data
  ClearLibrary();
  _fLastNormalizeValue = 1;
}

// Clear Library WaveOut
void CSoundLibrary::ClearLibrary(void)
{
  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);

  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Remove timer handler if added
  if (sl_thTimerHandler.th_Node.IsLinked())
  {
    _pTimer->RemHandler(&sl_thTimerHandler);
  }

  // Shut down direct sound buffers (if needed)
  ShutDown_dsound(*this);

  // Shut down wave out player buffers (if needed)
  if (sl_hwoWaveOut != NULL)
  {
    // Reset wave out play buffers (stop playing)
    MMRESULT res;
    res = waveOutReset(sl_hwoWaveOut);
    ASSERT(res == MMSYSERR_NOERROR);
    // Clear buffers
    for (INDEX iBuffer = 0; iBuffer < sl_awhWOBuffers.Count(); iBuffer++)
    {
      res = waveOutUnprepareHeader(sl_hwoWaveOut, &sl_awhWOBuffers[iBuffer], sizeof(sl_awhWOBuffers[iBuffer]));
      ASSERT(res == MMSYSERR_NOERROR);
    }
    sl_awhWOBuffers.Clear();
    
    // Close waveout device
    res = waveOutClose(sl_hwoWaveOut);
    ASSERT(res == MMSYSERR_NOERROR);
    sl_hwoWaveOut = NULL;
  }

  // For each extra taken channel
  for (INDEX iChannel = 0; iChannel < sl_ahwoExtra.Count(); iChannel++)
  {
    // Close its device
    MMRESULT res = waveOutClose(sl_ahwoExtra[iChannel]);
    ASSERT(res == MMSYSERR_NOERROR);
  }
  
  // Free extra channel handles
  sl_ahwoExtra.PopAll();

  // Free memory
  if (sl_pslMixerBuffer != NULL)
  {
    FreeMemory(sl_pslMixerBuffer);
    sl_pslMixerBuffer = NULL;
  }
  if (sl_pswDecodeBuffer != NULL)
  {
    FreeMemory(sl_pswDecodeBuffer);
    sl_pswDecodeBuffer = NULL;
  }
  if (sl_pubBuffersMemory != NULL)
  {
    FreeMemory(sl_pubBuffersMemory);
    sl_pubBuffersMemory = NULL;
  }
}

// Set listener enviroment properties (EAX)
BOOL CSoundLibrary::SetEnvironment(INDEX iEnvNo, FLOAT fEnvSize/*=0*/)
{
  if (!sl_bUsingEAX) {
    return FALSE;
  }
  
  // Trim values
  if (iEnvNo < 0   || iEnvNo > 25) {
    iEnvNo = 1;
  }
  if (fEnvSize < 1 || fEnvSize > 99) {
    fEnvSize = 8;
  }
  
  HRESULT hResult;
  hResult = sl_pKSProperty->Set(DSPROPSETID_EAX_ListenerProperties, DSPROPERTY_EAXLISTENER_ENVIRONMENT, NULL, 0, &iEnvNo, sizeof(DWORD));
  if (hResult != DS_OK) {
    return DSFail(*this, TRANS("  ! EAX error: Cannot set environment.\n"));
  }
  hResult = sl_pKSProperty->Set(DSPROPSETID_EAX_ListenerProperties, DSPROPERTY_EAXLISTENER_ENVIRONMENTSIZE, NULL, 0, &fEnvSize, sizeof(FLOAT));
  if (hResult != DS_OK) {
    return DSFail(*this, TRANS("  ! EAX error: Cannot set environment size.\n"));
  }
  return TRUE;
}

// Mute all sounds (erase playing buffer(s) and supress mixer)
void CSoundLibrary::Mute(void)
{
  // Stop all IFeel effects
  #ifdef SE1_IFEEL
  IFeel_StopEffect(NULL);
  #endif
  
  // Erase direct sound buffer (waveout will shut-up by itself), but skip if there's no more sound library
  if (this == NULL || !sl_bUsingDirectSound) {
    return;
  }
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Supress future mixing and erase sound buffer
  _bMuted = TRUE;
  static LPVOID lpData;
  static DWORD  dwSize;

  // Flush one secondary buffer
  if (!DSLockBuffer(*this, sl_pDSSecondary, sl_slMixerBufferSize, lpData, dwSize)) {
    return;
  }
  memset(lpData, 0, dwSize);
  sl_pDSSecondary->Unlock(lpData, dwSize, NULL, 0);
  
  // If EAX is in use
  if (sl_bUsingEAX)
  {
    // Flush right buffer, too
    if (!DSLockBuffer(*this, sl_pDSSecondary2, sl_slMixerBufferSize, lpData, dwSize)) {
      return;
    }
    memset(lpData, 0, dwSize);
    sl_pDSSecondary2->Unlock(lpData, dwSize, NULL, 0);
  } 
}

// Set sound format
CSoundLibrary::SoundFormat CSoundLibrary::SetFormat(CSoundLibrary::SoundFormat EsfNew, BOOL bReport/*=FALSE*/)
{
  // Access to the list of handlers must be locked
  CSyncLock slHooks(&_pTimer->tm_csHooks, TRUE);
  
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Pause playing all sounds
  {
    FOREACHINLIST(CSoundData, sd_Node, sl_ClhAwareList, itCsdStop)
    {
      itCsdStop->PausePlayingObjects();
    }
  }

  // Change format and keep console variable states
  SetFormat_internal(*this, EsfNew, bReport);
  _tmLastMixAhead = snd_tmMixAhead;
  _iLastFormat = snd_iFormat;
  _iLastDevice = snd_iDevice;
  _iLastAPI = snd_iInterface;

  // Continue playing all sounds
  CListHead lhToReload;
  lhToReload.MoveList(sl_ClhAwareList);
  {
    FORDELETELIST(CSoundData, sd_Node, lhToReload, itCsdContinue)
    {
      CSoundData &sd = *itCsdContinue;
      if (!(sd.sd_ulFlags & SDF_ENCODED)) {
        sd.Reload();
      } else {
        sd.sd_Node.Remove();
        sl_ClhAwareList.AddTail(sd.sd_Node);
      }
      sd.ResumePlayingObjects();
    }
  }

  // Done
  return sl_EsfFormat;
}

// Update all 3d effects and copy internal data.
void CSoundLibrary::UpdateSounds(void)
{
  // See if we have valid handle for direct sound and eventually reinit sound
  if (sl_bUsingDirectSound && _hwndCurrent!=_hwndMain) {
    _hwndCurrent = _hwndMain;
    SetFormat(sl_EsfFormat);
  }
  _bMuted = FALSE; // enable mixer
  _sfStats.StartTimer(CStatForm::STI_SOUNDUPDATE);
  _pfSoundProfile->StartTimer(CSoundProfile::PTI_UPDATESOUNDS);

  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Make sure that the buffers are playing
  if (sl_bUsingDirectSound) {
    DSPlayBuffers(*this);
  }

  // Determine number of listeners and get listener
  INDEX ctListeners = 0;
  CSoundListener *sli;
  {
    FOREACHINLIST(CSoundListener, sli_lnInActiveListeners, _pSound->sl_lhActiveListeners, itsli)
    {
      sli = itsli;
      ctListeners++;
    }
  }
  
  // If there's only one listener environment properties have been changed (in split-screen EAX is not supported)
  if (ctListeners == 1 && (_iLastEnvType != sli->sli_iEnvironmentType || _fLastEnvSize != sli->sli_fEnvironmentSize)) {
    // Keep new properties and eventually update environment (EAX)
    _iLastEnvType = sli->sli_iEnvironmentType;
    _fLastEnvSize = sli->sli_fEnvironmentSize;
    SetEnvironment(_iLastEnvType, _fLastEnvSize);
  }
  
  // If there are no listeners - reset environment properties
  if (ctListeners < 1 && (_iLastEnvType != 1 || _fLastEnvSize != 1.4f)) {
    // Keep new properties and update environment
    _iLastEnvType = 1;
    _fLastEnvSize = 1.4f;
    SetEnvironment(_iLastEnvType, _fLastEnvSize);
  }

  // Adjust panning if needed
  snd_fEAXPanning = Clamp(snd_fEAXPanning, -1.0f, +1.0f);
  if (sl_bUsingEAX && _fLastPanning != snd_fEAXPanning)
  {
    // Determine new panning
    _fLastPanning = snd_fEAXPanning;
    FLOAT fPanLeft  = -1.0f;
    FLOAT fPanRight = +1.0f;
    if (snd_fEAXPanning < 0) { fPanRight = MINPAN + Abs(snd_fEAXPanning) * MAXPAN; } // pan left
    if (snd_fEAXPanning > 0) { fPanLeft  = MINPAN + Abs(snd_fEAXPanning) * MAXPAN; } // pan right
    
    // Set and apply
    HRESULT hr1,hr2,hr3;
    hr1 = sl_pDSSourceLeft->SetPosition(fPanLeft, 0, 0, DS3D_DEFERRED);
    hr2 = sl_pDSSourceRight->SetPosition(fPanRight, 0, 0, DS3D_DEFERRED);
    hr3 = sl_pDSListener->CommitDeferredSettings();
    if (hr1 != DS_OK || hr2 != DS_OK || hr3 != DS_OK) {
      DSFail(*this, TRANS("  ! DirectSound3D error: Cannot set 3D position.\n"));
    }
  }

  // For each sound
  {
    FOREACHINLIST(CSoundData, sd_Node, sl_ClhAwareList, itCsdSoundData)
    {
      FORDELETELIST(CSoundObject, so_Node, itCsdSoundData->sd_ClhLinkList, itCsoSoundObject)
      {
        _sfStats.IncrementCounter(CStatForm::SCI_SOUNDSACTIVE);
        itCsoSoundObject->Update3DEffects();
      }
    }
  }

  // For each sound
  {
    FOREACHINLIST(CSoundData, sd_Node, sl_ClhAwareList, itCsdSoundData)
    {
      FORDELETELIST(CSoundObject, so_Node, itCsdSoundData->sd_ClhLinkList, itCsoSoundObject)
      {
        CSoundObject &so = *itCsoSoundObject;
        
        // If sound is playing
        if (so.so_slFlags & SOF_PLAY)
        {
          // Copy parameters
          so.so_sp = so.so_spNew;
          
          // Prepare sound if not prepared already
          if (!(so.so_slFlags & SOF_PREPARE)) {
            so.PrepareSound();
            so.so_slFlags |= SOF_PREPARE;
          }
          
        // If it is not playing
        } else {
          // Remove it from list
          so.so_Node.Remove();
        }
      }
    }
  }

  // Remove all listeners
  {
    FORDELETELIST(CSoundListener, sli_lnInActiveListeners, sl_lhActiveListeners, itsli)
    {
      itsli->sli_lnInActiveListeners.Remove();
    }
  }

  _pfSoundProfile->StopTimer(CSoundProfile::PTI_UPDATESOUNDS);
  _sfStats.StopTimer(CStatForm::STI_SOUNDUPDATE);
}

// This is called every TickQuantum seconds.
void CSoundTimerHandler::HandleTimer(void)
{
  /* memory leak checking routines
  ASSERT(_CrtCheckMemory());
  ASSERT(_CrtIsMemoryBlock((void*)_pSound->sl_pswDecodeBuffer, 
                           (ULONG)_pSound->sl_slDecodeBufferSize, NULL, NULL, NULL));
  ASSERT(_CrtIsValidPointer((void*)_pSound->sl_pswDecodeBuffer, (ULONG)_pSound->sl_slDecodeBufferSize, TRUE)); */
  // mix all needed sounds
  _pSound->MixSounds();
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MIXER helper functions

// Copying of mixer buffer to sound buffer(s)
static LPVOID _lpData, _lpData2;
static DWORD  _dwSize, _dwSize2;
static void CopyMixerBuffer_dsound(CSoundLibrary &sl, SLONG slMixedSize)
{
  LPVOID lpData;
  DWORD dwSize;
  SLONG slPart1Size, slPart2Size;

  // If EAX is in use
  if (sl.sl_bUsingEAX)
  {
    // lock left buffer and copy first part of 1st mono block
    if (!DSLockBuffer(sl, sl.sl_pDSSecondary, sl.sl_slMixerBufferSize, lpData, dwSize)) {
      return;
    }
    slPart1Size = Min(sl.sl_slMixerBufferSize-_iWriteOffset, slMixedSize);
    CopyMixerBuffer_mono(0, ((UBYTE*)lpData) + _iWriteOffset / 2, slPart1Size);
    
    // Copy second part of 1st mono block
    slPart2Size = slMixedSize - slPart1Size;
    CopyMixerBuffer_mono(slPart1Size, lpData, slPart2Size);
    _iWriteOffset += slMixedSize;
    if (_iWriteOffset >= sl.sl_slMixerBufferSize) {
      _iWriteOffset -= sl.sl_slMixerBufferSize;
    }
    ASSERT(_iWriteOffset >= 0 && _iWriteOffset < sl.sl_slMixerBufferSize);
    sl.sl_pDSSecondary->Unlock(lpData, dwSize, NULL, 0); 

    // Lock right buffer and copy first part of 2nd mono block
    if (!DSLockBuffer(sl, sl.sl_pDSSecondary2, sl.sl_slMixerBufferSize, lpData, dwSize)) {
      return;
    }
    slPart1Size = Min(sl.sl_slMixerBufferSize - _iWriteOffset2, slMixedSize);
    CopyMixerBuffer_mono(2, ((UBYTE*)lpData) + _iWriteOffset2 / 2, slPart1Size);
    
    // Copy second part of 2nd mono block
    slPart2Size = slMixedSize - slPart1Size;
    CopyMixerBuffer_mono(slPart1Size + 2, lpData, slPart2Size);
    _iWriteOffset2 += slMixedSize;
    if (_iWriteOffset2 >= sl.sl_slMixerBufferSize) {
      _iWriteOffset2 -= sl.sl_slMixerBufferSize;
    }
    ASSERT(_iWriteOffset2 >= 0 && _iWriteOffset2 < sl.sl_slMixerBufferSize);
    sl.sl_pDSSecondary2->Unlock(lpData, dwSize, NULL, 0);
    
  // If only standard DSound (no EAX)    
  } else {
    // Lock stereo buffer and copy first part of block
    if (!DSLockBuffer(sl, sl.sl_pDSSecondary, sl.sl_slMixerBufferSize, lpData, dwSize)) {
      return;
    }
    slPart1Size = Min(sl.sl_slMixerBufferSize - _iWriteOffset, slMixedSize);
    CopyMixerBuffer_stereo(0, ((UBYTE*)lpData) + _iWriteOffset, slPart1Size);
    
    // Copy second part of block
    slPart2Size = slMixedSize - slPart1Size;
    CopyMixerBuffer_stereo(slPart1Size, lpData, slPart2Size);
    _iWriteOffset += slMixedSize;
    if (_iWriteOffset >= sl.sl_slMixerBufferSize) {
      _iWriteOffset -= sl.sl_slMixerBufferSize;
    }
    ASSERT(_iWriteOffset >= 0 && _iWriteOffset < sl.sl_slMixerBufferSize);
    sl.sl_pDSSecondary->Unlock(lpData, dwSize, NULL, 0);
  }
}

// TODO: Add comment here
static void CopyMixerBuffer_waveout(CSoundLibrary &sl)
{
  MMRESULT res;
  SLONG slOffset = 0;
  for (INDEX iBuffer = 0; iBuffer < sl.sl_awhWOBuffers.Count(); iBuffer++)
  {
    // Skip prepared buffer
    WAVEHDR &wh = sl.sl_awhWOBuffers[iBuffer];
    if (wh.dwFlags & WHDR_PREPARED) {
      continue;
    }
    
    // Copy part of a mixer buffer to wave buffer
    CopyMixerBuffer_stereo(slOffset, wh.lpData, WAVEOUTBLOCKSIZE);
    slOffset += WAVEOUTBLOCKSIZE;
    
    // Write wave buffer (ready for playing)
    res = waveOutPrepareHeader(sl.sl_hwoWaveOut, &wh, sizeof(wh));
    ASSERT(res == MMSYSERR_NOERROR);
    res = waveOutWrite(sl.sl_hwoWaveOut, &wh, sizeof(wh));
    ASSERT(res == MMSYSERR_NOERROR);
  }
}

// Finds room in sound buffer to copy in next crop of samples
static SLONG PrepareSoundBuffer_dsound(CSoundLibrary &sl)
{
  // Determine writable block size (difference between write and play pointers)
  HRESULT hr1, hr2;
  DWORD dwCurrentCursor, dwCurrentCursor2;
  SLONG slDataToMix;
  ASSERT(sl.sl_pDSSecondary != NULL && sl.sl_pDSPrimary != NULL);

  // If EAX is in use
  if (sl.sl_bUsingEAX)
  {
    hr1 = sl.sl_pDSSecondary->GetCurrentPosition(&dwCurrentCursor,  NULL);
    hr2 = sl.sl_pDSSecondary2->GetCurrentPosition(&dwCurrentCursor2, NULL);
    if (hr1 != DS_OK || hr2 != DS_OK) {
      return DSFail(sl, TRANS("  ! DirectSound error: Cannot obtain sound buffer write position.\n"));
    }
    
    dwCurrentCursor  *= 2; // stereo mixer
    dwCurrentCursor2 *= 2; // stereo mixer
    
    // Store pointers and wrapped block sizes
    SLONG slDataToMix1 = dwCurrentCursor - _iWriteOffset;
    if (slDataToMix1 < 0) {
      slDataToMix1 += sl.sl_slMixerBufferSize;
    }
    ASSERT(slDataToMix1 >= 0 && slDataToMix1 <= sl.sl_slMixerBufferSize);
    slDataToMix1 = Min(slDataToMix1, sl.sl_slMixerBufferSize); 
    
    SLONG slDataToMix2 = dwCurrentCursor2 - _iWriteOffset2;
    if (slDataToMix2 < 0) {
      slDataToMix2 += sl.sl_slMixerBufferSize;
    }
    ASSERT(slDataToMix2 >= 0 && slDataToMix2 <= sl.sl_slMixerBufferSize);
    slDataToMix = Min(slDataToMix1, slDataToMix2);
  
  // If only standard DSound (no EAX)
  } else {
    hr1 = sl.sl_pDSSecondary->GetCurrentPosition(&dwCurrentCursor, NULL);
    if (hr1 != DS_OK) {
      return DSFail(sl, TRANS("  ! DirectSound error: Cannot obtain sound buffer write position.\n"));
    }
    
    // Store pointer and wrapped block size
    slDataToMix = dwCurrentCursor - _iWriteOffset;
    if (slDataToMix < 0) {
      slDataToMix += sl.sl_slMixerBufferSize;
    }
    ASSERT(slDataToMix >= 0 && slDataToMix <= sl.sl_slMixerBufferSize);
    slDataToMix = Min(slDataToMix, sl.sl_slMixerBufferSize); 
  }

  // Done
  //CDebugF("LP/LW: %5d / %5d,   RP/RW: %5d / %5d,    MIX: %5d\n", dwCurrentCursor, _iWriteOffset, dwCurrentCursor2, _iWriteOffset2, slDataToMix); // grgr
  return slDataToMix;
}

// TODO: Add comment here
static SLONG PrepareSoundBuffer_waveout(CSoundLibrary &sl)
{
  // Scan waveout buffers to find all that are ready to receive sound data (i.e. not playing)
  SLONG slDataToMix = 0;
  for (INDEX iBuffer = 0; iBuffer < sl.sl_awhWOBuffers.Count(); iBuffer++)
  {
    // If done playing
    WAVEHDR &wh = sl.sl_awhWOBuffers[iBuffer];
    if (wh.dwFlags&WHDR_DONE) {
      // Unprepare buffer
      MMRESULT res = waveOutUnprepareHeader(sl.sl_hwoWaveOut, &wh, sizeof(wh));
      ASSERT(res == MMSYSERR_NOERROR);
    }
    
    // If unprepared
    if (!(wh.dwFlags & WHDR_PREPARED)) {
      // Increase mix-in data size
      slDataToMix += WAVEOUTBLOCKSIZE;
    }
  }
  
  // Done
  ASSERT(slDataToMix <= sl.sl_slMixerBufferSize);
  return slDataToMix;
}

  
// Update Mixer
void CSoundLibrary::MixSounds(void)
{
  // Synchronize access to sounds
  CSyncLock slSounds(&sl_csSound, TRUE);

  // Do nothing if no sound
  if (sl_EsfFormat == SF_NONE || _bMuted) {
    return;
  }
  
  _sfStats.StartTimer(CStatForm::STI_SOUNDMIXING);
  _pfSoundProfile->IncrementAveragingCounter();
  _pfSoundProfile->StartTimer(CSoundProfile::PTI_MIXSOUNDS);

  // Seek available buffer(s) for next crop of samples
  SLONG slDataToMix;
  if (sl_bUsingDirectSound) { // using direct sound
    slDataToMix = PrepareSoundBuffer_dsound(*this);
  } else { // using wave out 
    slDataToMix = PrepareSoundBuffer_waveout(*this);
  }

  // Skip mixing if all sound buffers are still busy playing
  ASSERT(slDataToMix >= 0); // FIXME: tin 08122018 Comparison operators are different. It looks like a bug.
  if (slDataToMix <= 0) {
    _pfSoundProfile->StopTimer(CSoundProfile::PTI_MIXSOUNDS);
    _sfStats.StopTimer(CStatForm::STI_SOUNDMIXING);
    return;
  }

  // Prepare mixer buffer
  _pfSoundProfile->IncrementCounter(CSoundProfile::PCI_MIXINGS, 1);
  ResetMixer(sl_pslMixerBuffer, slDataToMix);

  BOOL bGamePaused = _pNetwork->IsPaused() || _pNetwork->IsServer() && _pNetwork->GetLocalPause();

  // For each sound
  FOREACHINLIST(CSoundData, sd_Node, sl_ClhAwareList, itCsdSoundData)
  {
    FORDELETELIST(CSoundObject, so_Node, itCsdSoundData->sd_ClhLinkList, itCsoSoundObject)
    {
      CSoundObject &so = *itCsoSoundObject;
      // If the sound is in-game sound, and the game paused
      if (!(so.so_slFlags & SOF_NONGAME) && bGamePaused) {
        continue; // Don't mix it it
      }
      
      // If sound is prepared and playing
      if (so.so_slFlags & SOF_PLAY && so.so_slFlags & SOF_PREPARE && !(so.so_slFlags & SOF_PAUSED)) {
        MixSound(&so);  // Mix it
      }
    }
  }

  // Eventually normalize mixed sounds
  snd_fNormalizer = Clamp(snd_fNormalizer, 0.0f, 1.0f);
  NormalizeMixerBuffer(snd_fNormalizer, slDataToMix, _fLastNormalizeValue);

  // Write mixer buffer to file
  // if (!_bOpened) _filMixerBuffer = fopen("d:\\MixerBufferDump.raw", "wb");
  // fwrite((void*)sl_pslMixerBuffer, 1, slDataToMix, _filMixerBuffer);
  // _bOpened = TRUE;

  // Copy mixer buffer to buffers buffer(s)
  if (sl_bUsingDirectSound) { // using direct sound
    CopyMixerBuffer_dsound(*this, slDataToMix);
  } else { // using wave out 
    CopyMixerBuffer_waveout(*this);
  }

  // All done
  _pfSoundProfile->StopTimer(CSoundProfile::PTI_MIXSOUNDS);
  _sfStats.StopTimer(CStatForm::STI_SOUNDMIXING);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sound mode awareness functions

// Add sound in sound aware list
void CSoundLibrary::AddSoundAware(CSoundData &CsdAdd) {
  sl_ClhAwareList.AddTail(CsdAdd.sd_Node);  // add sound to list tail
};

// Remove a display mode aware object.
void CSoundLibrary::RemoveSoundAware(CSoundData &CsdRemove) {
  CsdRemove.sd_Node.Remove();  // remove it from list
};

// Listen from this listener this frame
void CSoundLibrary::Listen(CSoundListener &sl)
{
  // Just add it to list
  if (sl.sli_lnInActiveListeners.IsLinked()) {
    sl.sli_lnInActiveListeners.Remove();
  }
  sl_lhActiveListeners.AddTail(sl.sli_lnInActiveListeners);
}