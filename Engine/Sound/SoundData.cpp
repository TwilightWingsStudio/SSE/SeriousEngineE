/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Sound/SoundData.h>

#include <Core/Base/Memory.h>
#include <Core/IO/Stream.h>
#include <Core/Base/ListIterator.inl>
#include <Engine/Sound/Wave.h>
#include <Engine/Sound/SoundDecoder.h>
#include <Engine/Sound/SoundLibrary.h>
#include <Engine/Sound/SoundObject.h>

#include <Engine/Resources/ResourceManager.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sound data awareness functions

// Pause all playing objects
void CSoundData::PausePlayingObjects(void)
{
  // For all objects linked to data pause playing
  FOREACHINLIST(CSoundObject, so_Node, sd_ClhLinkList, itCsoPause)
  {
    itCsoPause->Pause();  // pause playing
  }
}

// Resume all playing objects
void CSoundData::ResumePlayingObjects(void)
{
  // For all objects resume play
  FOREACHINLIST(CSoundObject, so_Node, sd_ClhLinkList, itCsoResume)
  {
    itCsoResume->Resume();  // call play method again
  }
}

// Add object in sound aware list
void CSoundData::AddObjectLink(CSoundObject &CsoAdd)
{
  sd_ClhLinkList.AddTail(CsoAdd.so_Node); // add object to list tail
}

// Remove a object from aware list
void CSoundData::RemoveObjectLink(CSoundObject &CsoRemove)
{
  CsoRemove.so_Node.Remove();  // remove it from list
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class global methods

// Constructor
CSoundData::CSoundData()
{
  sd_pswBuffer = NULL;
}

// Destructor
CSoundData::~CSoundData()
{
  Clear();
}

// Free Buffer (and all linked Objects)
void CSoundData::ClearBuffer(void)
{
  // If buffer exist
  if (sd_pswBuffer != NULL) {
    FreeMemory(sd_pswBuffer); // release it
    sd_pswBuffer = NULL;
  }
}

// Get Sound Length in seconds
double CSoundData::GetSecondsLength(void)
{
  // If not encoded
  if (!(sd_ulFlags & SDF_ENCODED) ) {
    // Len is read from wave
    return sd_dSecondsLength;
    
  // If encoded
  } else {
    // TODO: Implement this!!!!
    ASSERT(FALSE);
    return 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Input methods (load sound in SoundData class)

// Read sound in memory
void CSoundData::Read_t(CTStream *inFile) 
{
  // Synchronize access to sounds
  CSyncLock slSounds(&_pSound->sl_csSound, TRUE);

  ASSERT(sd_pswBuffer == NULL);
  sd_ulFlags = NONE;

  // Get filename
  CTFileName fnm = inFile->GetDescription();
  
  // If this is encoded file
  if (fnm.FileExt() == ".ogg" || fnm.FileExt() == ".mp3")
  {
    CSoundDecoder *pmpd = new CSoundDecoder(fnm);
    if (pmpd->IsOpen()) {
      pmpd->GetFormat(sd_wfeFormat);
    }
    delete pmpd;
    
    // Mark that this is streaming encoded file
    sd_ulFlags = SDF_ENCODED | SDF_STREAMING;

  // If this is wave file
  } else {
    // Load wave info
    PCMWaveInput CpwiLoad;
    sd_wfeFormat = CpwiLoad.LoadInfo_t(inFile);
    
    // Store sample length in seconds and average byte rate
    sd_dSecondsLength = CpwiLoad.GetSecondsLength();

    // If sound library is in lower format convert sound to library format
    if ((_pSound->sl_SwfeFormat).nSamplesPerSec < sd_wfeFormat.nSamplesPerSec) {
      sd_wfeFormat.nSamplesPerSec = (_pSound->sl_SwfeFormat).nSamplesPerSec;
    }
    
    // Same goes for bits/sample (must be 16)
    sd_wfeFormat.wBitsPerSample = 16;

    // If library is active create buffer and load sound data
    if (_pSound->IsActive())
    {
      // Create Buffer
      sd_slBufferSampleSize = CpwiLoad.GetDataLength(sd_wfeFormat);
      SLONG slBufferSize = CpwiLoad.DetermineBufferSize(sd_wfeFormat);
      sd_pswBuffer = (SWORD*)AllocMemory(slBufferSize + 8);
      
      // Load data into buffer
      CpwiLoad.LoadData_t(inFile, sd_pswBuffer, sd_wfeFormat);
      
      // Copy first sample to the last one (this is needed for linear interpolation)
      (ULONG&)(((UBYTE*)sd_pswBuffer)[slBufferSize]) = *(ULONG*)sd_pswBuffer;
    }
  }

  // Add to sound aware list
  _pSound->AddSoundAware(*this);
}

// Sound can't be written to file
void CSoundData::Write_t(CTStream *outFile)
{
  ASSERTALWAYS("Cannot write sounds!");
  throw TRANS("Cannot write sounds!");
}

// Get the description of this object.
CTString CSoundData::GetDescription(void)
{
  CTString str;
  str.PrintF("%dkHz %dbit %s %.2lfs", 
             sd_wfeFormat.nSamplesPerSec / 1000, 
             sd_wfeFormat.wBitsPerSample, 
             sd_wfeFormat.nChannels == 1 ? "mono" : "stereo",
             sd_dSecondsLength);
  return str;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class CLEAR method

// Free memory allocated for sound and Release DXBuffer
void CSoundData::Clear(void)
{
  // Synchronize access to sounds
  CSyncLock slSounds(&_pSound->sl_csSound, TRUE);

  // Clear BASE class
  CResource::Clear();

  // Free DXBuffer
  ClearBuffer();

  // If added as sound aware, remove it from sound aware list
  if (IsHooked()) {
    _pSound->RemoveSoundAware(*this);
  }
}

// Check if this kind of objects is auto-freed
BOOL CSoundData::IsAutoFreed(void)
{
  return FALSE;
}

// Get amount of memory used by this object
SLONG CSoundData::GetUsedMemory(void)
{
  SLONG slUsed = sizeof(*this);
  if (sd_pswBuffer != NULL) {
    ASSERT(sd_wfeFormat.nChannels == 1 || sd_wfeFormat.nChannels == 2);
    slUsed += sd_slBufferSampleSize * sd_wfeFormat.nChannels * 2; // all sounds are 16-bit
  }
  return slUsed;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Reference counting functions

// Add one reference
void CSoundData::AddReference(void)
{
  if (this != NULL) {
    Reference();
  }
}

// Remove one reference
void CSoundData::RemReference(void)
{
  if (this != NULL) {
    _pResourceMgr->Release(this);
  }
}

UBYTE CSoundData::GetType() const
{
  return TYPE_SOUND;
}