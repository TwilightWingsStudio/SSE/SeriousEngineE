/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Clipping functions

// Check if a polygon is to be visible
__forceinline ULONG CRenderer::GetPolygonVisibility(const CBrushPolygon &bpo)
{
  // Get transformed polygon's plane
  CWorkingPlane *pwplPolygonPlane = bpo.bpo_pbplPlane->bpl_pwplWorking;
  CWorkingPlane wplReverse;
  BOOL bInvertPolygon = FALSE;

  // If the polygon should be inverted or double sided
  if ((re_bRenderingShadows && !re_bDirectionalShadows && re_ubLightIllumination != 0 &&
        bpo.bpo_bppProperties.bpp_ubIlluminationType == re_ubLightIllumination) || 
        (re_pbrCurrent->br_pfsFieldSettings != NULL && !pwplPolygonPlane->wpl_bVisible))
  {
    bInvertPolygon = TRUE;
  }

  if (bInvertPolygon) {
    // Make temporary inverted polygon plane
    pwplPolygonPlane = &wplReverse;
    pwplPolygonPlane->wpl_plView = -bpo.bpo_pbplPlane->bpl_pwplWorking->wpl_plView;
    pwplPolygonPlane->wpl_bVisible = re_pbrCurrent->br_prProjection->IsViewerPlaneVisible(pwplPolygonPlane->wpl_plView);
  }

  // If the poly is double-sided and detail
  if (!re_bRenderingShadows && (bpo.bpo_ulFlags & BPOF_DOUBLESIDED) && (bpo.bpo_ulFlags & BPOF_DETAILPOLYGON)) {
    // It's definately visible
    return PDF_POLYGONVISIBLE;
  }

  // If the plane is invisible
  if (!pwplPolygonPlane->wpl_bVisible) {
    // Polygon is invisible
    return 0;
  }

  // If the polygon is invisible
  if ((bpo.bpo_ulFlags & BPOF_INVISIBLE) ||(re_bRenderingShadows && (bpo.bpo_ulFlags & BPOF_DOESNOTCASTSHADOW))) {
    // skip it
    return 0;
  }

  ULONG ulDirection = PDF_POLYGONVISIBLE;
  BOOL bProjectionInverted = re_prProjection->pr_bInverted;
  if (bProjectionInverted && !bInvertPolygon) {
    ulDirection |= PDF_FLIPEDGESPRE;
  } else if (!bProjectionInverted && bInvertPolygon){
    ulDirection |= PDF_FLIPEDGESPOST;
  }

  // Else, polygon is visible
  return ulDirection;
}

// Check if polygon is outside viewfrustum
__forceinline BOOL CRenderer::IsPolygonCulled(const CBrushPolygon &bpo)
{
  CBrushSector  &bsc = *bpo.bpo_pbscSector;
  // Setup initial mask
  ULONG ulMask = 0xFFFFFFFF;

  // for each vertex
  INDEX ctVtx = bpo.bpo_apbvxTriangleVertices.Count();
  {
    for (INDEX i = 0; i < ctVtx; i++)
    {
      CBrushVertex *pbvx = bpo.bpo_apbvxTriangleVertices[i];
      INDEX ivx = bsc.bsc_abvxVertices.Index(pbvx);
      
      // Get the outcodes for that vertex
      ULONG ulCode = re_avvxViewVertices[bsc.bsc_ivvx0 + ivx].vvx_ulOutcode;
      
      // And them to the mask
      ulMask &= ulCode;
    }
  }

  // If any bit in the mask is still set, it means that all points are out
  // wtr to that plane
  return ulMask;
}

// Find which portals should be rendered as portals or as pretenders
void CRenderer::FindPretenders(void)
{
  re_pbscCurrent->bsc_ispo0 = re_aspoScreenPolygons.Count();
  
  // For all polygons in sector
  FOREACHINSTATICARRAY(re_pbscCurrent->bsc_abpoPolygons, CBrushPolygon, itpo)
  {
    CBrushPolygon &bpo = *itpo;
    // Initially not rendered as portal
    bpo.bpo_ulFlags &= ~BPOF_RENDERASPORTAL;
    
    // If it is portal, 
    if (bpo.bpo_ulFlags & BPOF_PORTAL)
    {
      // Initially rendered as portal
      bpo.bpo_ulFlags|=BPOF_RENDERASPORTAL;

      // If could be a pretender
      if (bpo.bpo_bppProperties.bpp_uwPretenderDistance != 0)
      {
        // Get distance at which it is a pretender
        FLOAT fPretenderDistance = bpo.bpo_bppProperties.bpp_uwPretenderDistance;

        // For each vertex in the polygon
        INDEX ctVtx = bpo.bpo_apbvxTriangleVertices.Count();
        CBrushSector &bsc = *bpo.bpo_pbscSector;
        {
          for (INDEX i = 0; i < ctVtx; i++)
          {
            CBrushVertex *pbvx = bpo.bpo_apbvxTriangleVertices[i];
            INDEX ivx = bsc.bsc_abvxVertices.Index(pbvx);
            
            // Get distance of the vertex from the view plane
            FLOAT fx = re_avvxViewVertices[bsc.bsc_ivvx0 + ivx].vvx_vView(1);
            FLOAT fy = re_avvxViewVertices[bsc.bsc_ivvx0 + ivx].vvx_vView(2);
            FLOAT fz = re_avvxViewVertices[bsc.bsc_ivvx0 + ivx].vvx_vView(3);
            FLOAT fD = fx * fx + fy * fy + fz * fz;
            
            // If nearer than allowed pretender distance
            if (fD<fPretenderDistance * fPretenderDistance) {
              // this polygon is not a pretender
              goto nextpolygon;
            }
          }
        }
        
        // If all vertices passed the check, mark as pretender
        bpo.bpo_ulFlags &= ~BPOF_RENDERASPORTAL;
      }
    }
nextpolygon:;
  }
}

// Make screen polygons for nondetail polygons in current sector
void CRenderer::MakeNonDetailScreenPolygons(void)
{
  _pfRenderProfile->StartTimer(CRenderProfile::PTI_MAKENONDETAILSCREENPOLYGONS);

  re_pbscCurrent->bsc_ispo0 = re_aspoScreenPolygons.Count();
  
  // Detail polygons are not skipped if rendering shadows
  const ULONG ulDetailMask = re_bRenderingShadows ? 0 : BPOF_DETAILPOLYGON;

  // for all polygons in sector
  FOREACHINSTATICARRAY(re_pbscCurrent->bsc_abpoPolygons, CBrushPolygon, itpo)
  {
    CBrushPolygon &bpo = *itpo;

    // If polygon does not contribute to the visibility determination
    if ((bpo.bpo_ulFlags & ulDetailMask) &&!(bpo.bpo_ulFlags & BPOF_RENDERASPORTAL)) {
      // Skip it
      continue;
    }

    // No screen polygon by default
    bpo.bpo_pspoScreenPolygon = NULL;

    // Skip if the polygon is not visible
    ASSERT(!IsPolygonCulled(bpo));  // cannot be culled yet!
    const ULONG ulVisible = GetPolygonVisibility(bpo);
    if (ulVisible == 0) {
      continue;
    }

    _sfStats.IncrementCounter(CStatForm::SCI_POLYGONS);
    _pfRenderProfile->IncrementCounter(CRenderProfile::PCI_NONDETAILPOLYGONS);

    // Make screen polygon for the polygon
    CScreenPolygon &spo = *MakeScreenPolygon(bpo);

    // Add its edges
    MakeInitialPolygonEdges(bpo, spo, ulVisible);
  }

  // Remember number of polygons in sector
  re_pbscCurrent->bsc_ctspo = re_aspoScreenPolygons.Count()-re_pbscCurrent->bsc_ispo0;

  _pfRenderProfile->StopTimer(CRenderProfile::PTI_MAKENONDETAILSCREENPOLYGONS);
}

// Make screen polygons for detail polygons in current sector
void CRenderer::MakeDetailScreenPolygons(void)
{
  // If rendering shadows or not rendering detail polygons
  if (re_bRenderingShadows || !wld_bRenderDetailPolygons) {
    // Do nothing
    return;
  }

  _pfRenderProfile->StartTimer(CRenderProfile::PTI_MAKEDETAILSCREENPOLYGONS);
  
  // For all polygons in sector
  FOREACHINSTATICARRAY(re_pbscCurrent->bsc_abpoPolygons, CBrushPolygon, itpo)
  {
    CBrushPolygon &bpo = *itpo;

    // If polygon is not detail
    if (!(bpo.bpo_ulFlags&BPOF_DETAILPOLYGON) || (bpo.bpo_ulFlags&BPOF_RENDERASPORTAL)) {
      // skip it
      continue;
    }

    // No screen polygon by default
    bpo.bpo_pspoScreenPolygon = NULL;

    // skip if the polygon is not visible
    if (GetPolygonVisibility(bpo) == 0) {
      continue;
    }
    
    // Skip if outside the frustum
    if ((re_pbscCurrent->bsc_ulFlags & BSCF_NEEDSCLIPPING) && IsPolygonCulled(bpo)) {
      continue;
    }
    
    _sfStats.IncrementCounter(CStatForm::SCI_DETAILPOLYGONS);
    _pfRenderProfile->IncrementCounter(CRenderProfile::PCI_DETAILPOLYGONS);

    // Make screen polygon for the polygon
    CScreenPolygon &spo = *MakeScreenPolygon(bpo);

    // If it is portal
    if (spo.IsPortal()) {
      // Pass it immediately
      PassPortal(spo);
    } else {
      // Add polygon to scene polygons for rendering
      AddPolygonToScene(&spo);
    }
  }
  _pfRenderProfile->StopTimer(CRenderProfile::PTI_MAKEDETAILSCREENPOLYGONS);
}

// Make initial edges for a polygon
void CRenderer::MakeInitialPolygonEdges(CBrushPolygon &bpo, CScreenPolygon &spo, BOOL ulDirection)
{
  // Get number of edges
  INDEX ctEdges = bpo.bpo_abpePolygonEdges.Count();
  spo.spo_ubDirectionFlags = ulDirection & PDF_FLIPEDGESPOST;
  BOOL bInvert = (ulDirection & PDF_FLIPEDGESPRE) != 0;
  
  // Remember edge vertex start and count
  spo.spo_ctEdgeVx = ctEdges * 2;
  spo.spo_iEdgeVx0 = re_aiEdgeVxClipSrc.Count();

  // Create edge vertices
  INDEX *ai = re_aiEdgeVxClipSrc.Push(ctEdges * 2);

  // For each edge
  for (INDEX iEdge = 0; iEdge < ctEdges; iEdge++)
  {
    // Set the two vertices
    CBrushPolygonEdge &bpe = bpo.bpo_abpePolygonEdges[iEdge];
    CWorkingEdge &wed = *bpe.bpe_pbedEdge->bed_pwedWorking;
    if (bpe.bpe_bReverse ^ bInvert) {
      ai[iEdge * 2 + 0] = wed.wed_iwvx1+re_iViewVx0;
      ai[iEdge * 2 + 1] = wed.wed_iwvx0+re_iViewVx0;
    } else {
      ai[iEdge * 2 + 0] = wed.wed_iwvx0+re_iViewVx0;
      ai[iEdge * 2 + 1] = wed.wed_iwvx1+re_iViewVx0;
    }
  }
}

// Make final edges for all polygons in current sector
void CRenderer::MakeFinalPolygonEdges(void)
{
  _pfRenderProfile->StartTimer(CRenderProfile::PTI_MAKEFINALPOLYGONEDGES);
  
  // For each polygon
  INDEX ispo0 = re_pbscCurrent->bsc_ispo0;
  INDEX ispoTop = re_pbscCurrent->bsc_ispo0 + re_pbscCurrent->bsc_ctspo;
  for (INDEX ispo = ispo0; ispo < ispoTop; ispo++)
  {
    CScreenPolygon &spo = re_aspoScreenPolygons[ispo];

    // If polygon has no edges skip it
    if (spo.spo_ctEdgeVx == 0) {
      continue;
    }

    INDEX iEdgeVx0New = re_aiEdgeVxMain.Count();
    INDEX *piVertices = re_aiEdgeVxMain.Push(spo.spo_ctEdgeVx);

    // For each vertex
    INDEX ivxTop = spo.spo_iEdgeVx0 + spo.spo_ctEdgeVx;
    for (INDEX ivx = spo.spo_iEdgeVx0; ivx < ivxTop; ivx++)
    {
      // Copy to final array
      *piVertices++ = re_aiEdgeVxClipSrc[ivx];
    }

    // Remember new edge vertex positions
    spo.spo_iEdgeVx0 = iEdgeVx0New;
  }

  // clear temporary arrays
  re_aiEdgeVxClipSrc.PopAll();
  re_aiEdgeVxClipDst.PopAll();
  _pfRenderProfile->StopTimer(CRenderProfile::PTI_MAKEFINALPOLYGONEDGES);
}

// Cclip all polygons to one clip plane
void CRenderer::ClipToOnePlane(const FLOATplane3D &plView)
{
  // Remember clip plane
  re_plClip = plView;
  
  // No need for clipping if no vertices are outside
  ASSERT(re_pbscCurrent->bsc_ulFlags & BSCF_NEEDSCLIPPING);
  if (!MakeOutcodes()) {
    return;
  }

  // For each polygon
  INDEX ispo0 = re_pbscCurrent->bsc_ispo0;
  INDEX ispoTop = re_pbscCurrent->bsc_ispo0+re_pbscCurrent->bsc_ctspo;
  for (INDEX ispo = ispo0; ispo < ispoTop; ispo++)
  {
    // Clip to the plane
    ClipOnePolygon(re_aspoScreenPolygons[ispo]);
  }

  // Swap edge buffers
  Swap(re_aiEdgeVxClipSrc.sa_Count, re_aiEdgeVxClipDst.sa_Count);
  Swap(re_aiEdgeVxClipSrc.sa_Array, re_aiEdgeVxClipDst.sa_Array);
  Swap(re_aiEdgeVxClipSrc.sa_UsedCount, re_aiEdgeVxClipDst.sa_UsedCount);
  re_aiEdgeVxClipDst.PopAll();
}

// Clip all polygons to all clip planes of a projection
void CRenderer::ClipToAllPlanes(CAnyProjection3D &pr)
{
  _pfRenderProfile->StartTimer(CRenderProfile::PTI_CLIPTOALLPLANES);
  
  // Clip to up/down/left/right clip planes
  FLOATplane3D pl;
  pl = pr->pr_plClipU;  pl.Offset(-0.001f);  ClipToOnePlane(pl);
  pl = pr->pr_plClipD;  pl.Offset(-0.001f);  ClipToOnePlane(pl);
  pl = pr->pr_plClipL;  pl.Offset(-0.001f);  ClipToOnePlane(pl);
  pl = pr->pr_plClipR;  pl.Offset(-0.001f);  ClipToOnePlane(pl);
  
  // Clip to near clip plane
  ClipToOnePlane(FLOATplane3D(FLOAT3D(0, 0, -1), pr->pr_NearClipDistance));
  
  // Clip to far clip plane if existing
  if (pr->pr_FarClipDistance > 0) {
    ClipToOnePlane(FLOATplane3D(FLOAT3D(0, 0, 1), -pr->pr_FarClipDistance));
  }

  // If projection is mirrored or warped
  if (pr->pr_bMirror||pr->pr_bWarp) {
    // Clip to mirror plane
    ClipToOnePlane(pr->pr_plMirrorView);
  }
  _pfRenderProfile->StopTimer(CRenderProfile::PTI_CLIPTOALLPLANES);
}

// Make outcodes for current clip plane for all active vertices
__forceinline BOOL CRenderer::MakeOutcodes(void)
{
  SLONG slMask = 0;
  // For each active view vertex
  INDEX iVxTop = re_avvxViewVertices.Count();
  for (INDEX ivx = re_iViewVx0; ivx < iVxTop; ivx++) 
 {
    CViewVertex &vvx = re_avvxViewVertices[ivx];
    
    // Calculate the distance
    vvx.vvx_fD = re_plClip.PointDistance(vvx.vvx_vView);
    
    // Calculate the outcode
    const ULONG ulOutCode = (*(SLONG*)&vvx.vvx_fD) & 0x80000000;
    
    // Add to the outcode of the vertex
    vvx.vvx_ulOutcode = (vvx.vvx_ulOutcode >> 1) | ulOutCode;
    
    // Add to mask
    slMask |= ulOutCode;
  }
  // If any was negative, return true -- needs clipping
  return slMask;
}

// Clip one polygon to current clip plane
void CRenderer::ClipOnePolygon(CScreenPolygon &spo)
{
//
// NOTE: There is one ugly problem with this loop.
// I don't know any better way to fix it, so I have commented it.
// If the vertex references (vvx0 and vvx1) are taken _before_ pushing the vvxNew,
// it can cause access violation, because pushing can move array in memory.
// Therefore, it is important to take the references _after_ the Push() call.
  
  INDEX iEdgeVx0New = re_aiEdgeVxClipDst.Count();

  // For each edge
  INDEX ivxTop = spo.spo_iEdgeVx0+spo.spo_ctEdgeVx;
  for (INDEX ivx = spo.spo_iEdgeVx0; ivx < ivxTop; ivx += 2)
  {
    INDEX ivx0 = re_aiEdgeVxClipSrc[ivx + 0];
    INDEX ivx1 = re_aiEdgeVxClipSrc[ivx + 1];

    // Get vertices
    FLOAT fD0 = re_avvxViewVertices[ivx0].vvx_fD;
    FLOAT fD1 = re_avvxViewVertices[ivx1].vvx_fD;
    if (fD0 <= 0)
    {
      // If both are back - no screen edge remains
      if (fD1 <= 0) {
        continue;
        
      // If first is back, second front
      } else {
        // Make new vertex
        INDEX ivxNew = re_avvxViewVertices.Count();
        CViewVertex &vvxNew = re_avvxViewVertices.Push();
        CViewVertex &vvx0 = re_avvxViewVertices[ivx0];  // must do this after push, see note above!
        CViewVertex &vvx1 = re_avvxViewVertices[ivx1];  // must do this after push, see note above!
        
        // Clip first
        FLOAT fDivisor = 1.0f / (fD0 - fD1);
        FLOAT fFactor = fD0 * fDivisor;
        vvxNew.vvx_vView(1) = vvx0.vvx_vView(1) - (vvx0.vvx_vView(1) - vvx1.vvx_vView(1)) * fFactor;
        vvxNew.vvx_vView(2) = vvx0.vvx_vView(2) - (vvx0.vvx_vView(2) - vvx1.vvx_vView(2)) * fFactor;
        vvxNew.vvx_vView(3) = vvx0.vvx_vView(3) - (vvx0.vvx_vView(3) - vvx1.vvx_vView(3)) * fFactor;
        
        // Remember new edge
        re_aiEdgeVxClipDst.Push() = ivxNew;
        re_aiEdgeVxClipDst.Push() = ivx1;
        
        // Add new vertex to clip buffer
        re_aiClipBuffer.Push() = ivxNew;
      }
    } else {
      // If first is front, second back
      if ((SLONG&)fD1 <= 0)
      {
        // Make new vertex
        INDEX ivxNew = re_avvxViewVertices.Count();
        CViewVertex &vvxNew = re_avvxViewVertices.Push();
        CViewVertex &vvx0 = re_avvxViewVertices[ivx0];  // must do this after push, see note above!
        CViewVertex &vvx1 = re_avvxViewVertices[ivx1];  // must do this after push, see note above!
        
        // Clip second
        FLOAT fDivisor = 1.0f / (fD0 - fD1);
        FLOAT fFactor = fD1 * fDivisor;
        vvxNew.vvx_vView(1) = vvx1.vvx_vView(1) - (vvx0.vvx_vView(1) - vvx1.vvx_vView(1)) * fFactor;
        vvxNew.vvx_vView(2) = vvx1.vvx_vView(2) - (vvx0.vvx_vView(2) - vvx1.vvx_vView(2)) * fFactor;
        vvxNew.vvx_vView(3) = vvx1.vvx_vView(3) - (vvx0.vvx_vView(3) - vvx1.vvx_vView(3)) * fFactor;
        
        // Remember new edge
        re_aiEdgeVxClipDst.Push() = ivx0;
        re_aiEdgeVxClipDst.Push() = ivxNew;
        
        // Add new vertex to clip buffer
        re_aiClipBuffer.Push() = ivxNew;
        
        
      // If both are front
      } else {
        // Just copy the edge
        re_aiEdgeVxClipDst.Push() = ivx0;
        re_aiEdgeVxClipDst.Push() = ivx1;
      }
    }
  }

  // If there is anything in clip buffer
  if (re_aiClipBuffer.Count() > 0) {
    // Generate clip edges
    GenerateClipEdges(spo);
  }

  // Remember new edge vertex positions
  spo.spo_ctEdgeVx = re_aiEdgeVxClipDst.Count() - iEdgeVx0New;
  spo.spo_iEdgeVx0 = iEdgeVx0New;
}

// Compare two vertices for quick-sort.
static UBYTE *_aVertices = NULL;
static int qsort_CompareVertices_plus(const void *ppvVertex0, const void *ppvVertex1)
{
  INDEX ivx0 = *(const INDEX*)ppvVertex0;
  INDEX ivx1 = *(const INDEX*)ppvVertex1;
  FLOAT f0 = *(FLOAT*)(_aVertices + ivx0 * sizeof(CViewVertex));
  FLOAT f1 = *(FLOAT*)(_aVertices + ivx1 * sizeof(CViewVertex));
  if (f0 < f1) {
    return -1;
  } else if (f0 > f1) {
    return +1;
  } else {
    return  0;
  }
}

static int qsort_CompareVertices_minus(const void *ppvVertex0, const void *ppvVertex1)
{
  INDEX ivx0 = *(const INDEX*)ppvVertex0;
  INDEX ivx1 = *(const INDEX*)ppvVertex1;
  FLOAT f0 = *(FLOAT*)(_aVertices + ivx0 * sizeof(CViewVertex));
  FLOAT f1 = *(FLOAT*)(_aVertices + ivx1 * sizeof(CViewVertex));
  if (f0 < f1) {
    return +1;
  } else if (f0 > f1) {
    return -1;
  } else {
    return  0;
  }
}

// Generate clip edges for one polygon
void CRenderer::GenerateClipEdges(CScreenPolygon &spo)
{
  ASSERT(re_aiClipBuffer.Count() > 0);

  FLOATplane3D &plPolygonPlane = spo.spo_pbpoBrushPolygon->bpo_pbplPlane->bpl_pwplWorking->wpl_plView;
  
  // Calculate the clip buffer direction in 3d as:  clip_normal_vector x polygon_normal_vector
  FLOAT3D vClipDir = ((FLOAT3D &)re_plClip) * ((FLOAT3D &)plPolygonPlane);

  // Get max axis
  INDEX iMaxAxis = 1;
  FLOAT fMaxAbs = Abs(vClipDir(1));
  if (Abs(vClipDir(2)) > fMaxAbs) {
    iMaxAxis = 2;
    fMaxAbs = Abs(vClipDir(2));
  }
  
  if (Abs(vClipDir(3)) > fMaxAbs) {
    iMaxAxis = 3;
    fMaxAbs = Abs(vClipDir(3));
  }

  _aVertices = (UBYTE*) &re_avvxViewVertices[0].vvx_vView(iMaxAxis);

  INDEX *aIndices = &re_aiClipBuffer[0];
  INDEX ctIndices = re_aiClipBuffer.Count();

  // There must be even number of vertices in the buffer
  ASSERT(ctIndices%2 == 0);

  // If the sign of axis is negative
  if (vClipDir(iMaxAxis) < 0) {
    // Sort them inversely
    qsort(aIndices, ctIndices, sizeof(INDEX), qsort_CompareVertices_minus);
    
  // If it is negative
  } else {
    // Sort them normally
    qsort(aIndices, ctIndices, sizeof(INDEX), qsort_CompareVertices_plus);
  }

  // For each two vertices
  for (INDEX iClippedVertex = 0; iClippedVertex < ctIndices; iClippedVertex += 2)
  {
    // Add the edge
    re_aiEdgeVxClipDst.Push() = aIndices[iClippedVertex + 0];
    re_aiEdgeVxClipDst.Push() = aIndices[iClippedVertex + 1];
  }

  // Clear the clip buffer
  re_aiClipBuffer.PopAll();
}