/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Shell.h>
#include <Engine/Network/CommunicationInterface.h>
#include <Engine/Network/Network.h>
#include <Engine/Network/PlayerBuffer.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/Server.h>
#include <Engine/Network/SessionState.h>
#include <Engine/Network/ServerCommands.h>

extern INDEX ser_bReportSyncOK   = FALSE;
extern INDEX ser_bReportSyncBad  = TRUE;
extern INDEX ser_bReportSyncLate = FALSE;
extern INDEX ser_bReportSyncEarly = FALSE;
extern INDEX ser_bPauseOnSyncBad = FALSE;
extern INDEX ser_iKickOnSyncBad = 10;
extern INDEX ser_bKickOnSyncLate = 1;
extern INDEX ser_iRememberBehind = 3000;
extern INDEX ser_iExtensiveSyncCheck = 0;
extern INDEX ser_bClientsMayPause = TRUE;
extern FLOAT ser_tmSyncCheckFrequency = 1.0f;
extern INDEX ser_iSyncCheckBuffer = 60;
extern INDEX ser_bEnumeration  = TRUE;
extern INDEX ser_bPingGameAgent = TRUE;
extern FLOAT ser_tmKeepAlive = 0.1f;
extern FLOAT ser_tmPingUpdate = 3.0f;
extern INDEX ser_bWaitFirstPlayer = 0;
extern INDEX ser_iMaxAllowedBPS = 8000;
extern CTString ser_strIPMask = "";
extern CTString ser_strNameMask = "";
extern INDEX ser_bInverseBanning = FALSE;
extern CTString ser_strMOTD = "";

static CTString ToLower(CTString &strResult)
{
  char *pch = strResult.Data();
  const INDEX ctLen = strResult.Length();

  for (INDEX i = 0; i < ctLen; i++) {
    pch[i] = tolower(pch[i]);
  }

  return strResult;
}

static CTString RemoveSubstring(CTString &strFull, CTString &strSub)
{
  CTString strFullL = ToLower(strFull);
  CTString strSubL = ToLower(strSub);

  const char *pchFullL = strFullL;
  const char *pchSubL = strSubL;
  const char *pchFound = strstr(pchFullL, pchSubL);

  if (pchFound == NULL || strlen(strSub) == 0) {
    return strFull;
  }

  INDEX iOffset = pchFound-pchFullL;
  INDEX iLenFull = strFull.Length();
  INDEX iLenSub = strSub.Length();

  CTString strLeft = strFull;
  strLeft.TrimRight(iOffset);
  CTString strRight = strFull;
  strRight.TrimLeft(iLenFull-iOffset-iLenSub);
  return strLeft+strRight;
}

static void KickClientCfunc(void* pArgs)
{
  INDEX iClient = NEXTARGUMENT(INDEX);
  CTString strReason = *NEXTARGUMENT(CTString*);
  CServerCommands::KickClient(iClient, strReason);
}

static void KickByNameCfunc(void* pArgs)
{
  CTString strName = *NEXTARGUMENT(CTString*);
  CTString strReason = *NEXTARGUMENT(CTString*);
  CServerCommands::KickByName(strName, strReason);
}

void CServerCommands::Initialize()
{
  // Runtime settings.
  _pShell->DeclareSymbol("user INDEX ser_bReportSyncOK;",    &ser_bReportSyncOK);
  _pShell->DeclareSymbol("user INDEX ser_bReportSyncBad;",   &ser_bReportSyncBad);
  _pShell->DeclareSymbol("user INDEX ser_bReportSyncLate;",  &ser_bReportSyncLate);
  _pShell->DeclareSymbol("user INDEX ser_bReportSyncEarly;", &ser_bReportSyncEarly);
  _pShell->DeclareSymbol("user INDEX ser_bPauseOnSyncBad;",  &ser_bPauseOnSyncBad);
  _pShell->DeclareSymbol("user INDEX ser_iKickOnSyncBad;",   &ser_iKickOnSyncBad);
  _pShell->DeclareSymbol("user INDEX ser_bKickOnSyncLate;",  &ser_bKickOnSyncLate);
  _pShell->DeclareSymbol("user INDEX ser_iRememberBehind;", &ser_iRememberBehind);
  
  // Persistent settings.
  _pShell->DeclareSymbol("persistent user INDEX ser_iExtensiveSyncCheck;", &ser_iExtensiveSyncCheck);
  _pShell->DeclareSymbol("persistent user FLOAT ser_tmSyncCheckFrequency;", &ser_tmSyncCheckFrequency);
  _pShell->DeclareSymbol("persistent user INDEX ser_iSyncCheckBuffer;", &ser_iSyncCheckBuffer);
  _pShell->DeclareSymbol("persistent user INDEX ser_bClientsMayPause;", &ser_bClientsMayPause);
  _pShell->DeclareSymbol("persistent user INDEX ser_bEnumeration;",      &ser_bEnumeration);
  _pShell->DeclareSymbol("persistent user INDEX ser_bPingGameAgent;", &ser_bPingGameAgent);
  _pShell->DeclareSymbol("persistent user FLOAT ser_tmKeepAlive;", &ser_tmKeepAlive);
  _pShell->DeclareSymbol("persistent user FLOAT ser_tmPingUpdate;", &ser_tmPingUpdate);
  _pShell->DeclareSymbol("persistent user INDEX ser_bWaitFirstPlayer;", &ser_bWaitFirstPlayer);
  _pShell->DeclareSymbol("persistent user INDEX ser_iMaxAllowedBPS;", &ser_iMaxAllowedBPS);
  _pShell->DeclareSymbol("persistent user INDEX ser_iMaxAllowedBPS;", &ser_iMaxAllowedBPS);
  _pShell->DeclareSymbol("persistent user CTString ser_strIPMask;", &ser_strIPMask);
  _pShell->DeclareSymbol("persistent user CTString ser_strNameMask;", &ser_strNameMask);
  _pShell->DeclareSymbol("persistent user INDEX ser_bInverseBanning;", &ser_bInverseBanning);
  _pShell->DeclareSymbol("persistent user CTString ser_strMOTD;", &ser_strMOTD);
  
  // Control commands.
  _pShell->DeclareSymbol("user void KickClient(INDEX, CTString);", &KickClientCfunc);
  _pShell->DeclareSymbol("user void KickByName(CTString, CTString);", &KickByNameCfunc);
  _pShell->DeclareSymbol("user void ListClients(void);", &ListClients);
  _pShell->DeclareSymbol("user void ListPlayers(void);", &ListPlayers);
  _pShell->DeclareSymbol("user void AddIPMask(CTString);", &AddIPMask);
  _pShell->DeclareSymbol("user void RemIPMask(CTString);", &RemIPMask);
  _pShell->DeclareSymbol("user void AddNameMask(CTString);", &AddNameMask);
  _pShell->DeclareSymbol("user void RemNameMask(CTString);", &RemNameMask);
}

void CServerCommands::AddIPMask(void* pArgs)
{
  CTString strIP = *NEXTARGUMENT(CTString*);
  ser_strIPMask+= strIP + "\n";
}

void CServerCommands::RemIPMask(void* pArgs)
{
  CTString strIP = *NEXTARGUMENT(CTString*);
  ser_strIPMask = RemoveSubstring(ser_strIPMask, strIP + "\n");
}

void CServerCommands::AddNameMask(void* pArgs)
{
  CTString strName = *NEXTARGUMENT(CTString*);
  ser_strNameMask += strName + "\n";
}

void CServerCommands::RemNameMask(void* pArgs)
{
  CTString strName = *NEXTARGUMENT(CTString*);
  ser_strNameMask = RemoveSubstring(ser_strNameMask, strName + "\n");
}

void CServerCommands::ListClients()
{
  CInfoF("client list:\n");
  if (!_pNetwork->IsServer()) {
    CWarningF("  <not a server>\n");
    return;
  }

  CInfoF("  CLID# players ip\n");
  CInfoF("  ----------------------------------------\n");
  
  INDEX ctClients = 0;

  for (INDEX iSession = 0; iSession<_pNetwork->ga_srvServer.srv_assoSessions.Count(); iSession++)
  {
    CSessionSocket &sso = _pNetwork->ga_srvServer.srv_assoSessions[iSession];

    // Skip inactive sessions.
    if (iSession > 0 && !sso.IsActive()) {
      continue;
    }

    CInfoF("    %-2d     %-2d   %s\n", iSession, _pNetwork->ga_srvServer.GetPlayerCountForClient(iSession), _cmiComm.Server_GetClientName(iSession));
    ctClients++;
  }
  
  if (ctClients == 0) {
    CInfoF("    There are no clients!\n");
  }
  
  CInfoF("  ----------------------------------------\n");
}

void CServerCommands::ListPlayers()
{
  CInfoF("player list:\n");

  if (!_pNetwork->IsServer()) {
    CInfoF("  <not a server>\n");
    return;
  }

  CInfoF("  CLID# PLID# name\n");
  CInfoF("  ----------------------\n");

  for (INDEX iPlayer = 0; iPlayer < _pNetwork->ga_srvServer.srv_aplbPlayers.Count(); iPlayer++)
  {
    CPlayerBuffer &plb = _pNetwork->ga_srvServer.srv_aplbPlayers[iPlayer];

    if (plb.IsActive()) {
      CInfoF("    %-2d    %-2d  %s\n", plb.GetClient(), iPlayer, plb.GetCharacter().GetNameForPrinting());
    }
  }

  CInfoF("  ----------------------\n");
}

void CServerCommands::KickClient(ULONG iClient, const CTString &strReason)
{
  if (!_pNetwork->IsServer()) {
    CWarningF( TRANS("Only server can kick people!\n"));
    return;
  }

  iClient = Clamp(iClient, ULONG(0), ULONG(NET_MAXGAMECOMPUTERS));

  if (!_pNetwork->ga_srvServer.srv_assoSessions[iClient].IsActive()) {
    CWarningF(TRANS("Client not connected!\n"));
    return;
  }

  if (iClient == 0) {
    CWarningF(TRANS("Can't kick local client!\n"));
    return;
  }

  CInfoF(TRANS("Kicking %d with explanation '%s'...\n"), iClient, strReason);
  _pNetwork->ga_srvServer.SendDisconnectMessage(iClient, "Admin: "+strReason);
}

void CServerCommands::KickByName(const CTString &strName, const CTString &strReason)
{
  if (!_pNetwork->IsServer()) {
    CWarningF(TRANS("Only server can kick people!\n"));
    return;
  }

  for (INDEX iPlayer = 0; iPlayer < _pNetwork->ga_srvServer.srv_aplbPlayers.Count(); iPlayer++)
  {
    CPlayerBuffer &plb = _pNetwork->ga_srvServer.srv_aplbPlayers[iPlayer];

    if (plb.IsActive() && plb.GetCharacter().GetNameForPrinting().Undecorated().Matches(strName)) {
      KickClient(plb.GetClient(), strReason);
    }
  }
}