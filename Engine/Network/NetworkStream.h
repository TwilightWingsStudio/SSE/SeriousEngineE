/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_NETWORKSTREAM_H
#define SE_INCL_NETWORKSTREAM_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

/*
 * Stream of message blocks that can be sent across network.
 */
class CNetworkStream
{
  public:
    enum Result {
      R_OK = 1,
      R_BLOCKMISSING,           // block is missing in the stream
      R_BLOCKNOTRECEIVEDYET,    // block is not yet received
    };

  public:
    CListHead ns_lhBlocks;   // list of blocks of this stream (higher sequences first)

    // Add a block that is already allocated to the stream.
    void AddAllocatedBlock(CNetworkStreamBlock *pnsbBlock);

  public:
    // Constructor.
    CNetworkStream(void);

    // Destructor.
    ~CNetworkStream(void);

    // Clear the object (remove all blocks).
    void Clear(void);

    // Copy from another network stream.
    void Copy(CNetworkStream &nsOther);

    // get number of blocks used by this object
    INDEX GetUsedBlocks(void);

    // get amount of memory used by this object
    SLONG GetUsedMemory(void);

    // get index of newest sequence stored
    INDEX GetNewestSequence(void);


    // Add a block to the stream (makes a copy of block).
    void AddBlock(CNetworkStreamBlock &nsbBlock);

    // Read a block as a submessage from a message and add it to the stream.
    void ReadBlock(CNetworkMessage &nmMessage);

    // Get a block from stream by its sequence number.
    CNetworkStream::Result GetBlockBySequence(INDEX iSequenceNumber, CNetworkStreamBlock *&pnsbBlock);

    // Find oldest block after given one (for batching missing sequences).
    INDEX GetOldestSequenceAfter(INDEX iSequenceNumber);


    // Write given number of newest blocks to a message.
    INDEX WriteBlocksToMessage(CNetworkMessage &nmMessage, INDEX ctBlocks);

    // Remove all blocks but the given number of newest ones.
    void RemoveOlderBlocks(INDEX ctBlocksToKeep);

    // Remove all blocks with sequence older than given.
    void RemoveOlderBlocksBySequence(INDEX iLastSequenceToKeep);
};

#endif /* include-once check. */