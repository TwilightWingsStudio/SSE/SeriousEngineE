/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATEMESSAGE_ENTITY_H
#define SE_INCL_STATEMESSAGE_ENTITY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Abstract (de)serializer class for any message that affects entity state.
class ENGINE_API CEntityStateMessage : public CStateMessage
{
  public:
    ULONG sm_ulEntityId;
    
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_BAD;
    }
    
    inline ULONG GetEntityId() const
    {
      return sm_ulEntityId;
    }      

    CEntityStateMessage();
    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CCreateEntityMessage : public CEntityStateMessage
{
  public:
    UWORD sm_uwEntityClassId;
    FLOAT3D sm_vPosition;
    ANGLE3D sm_aOrientation;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_CREATE;
    }
  
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CCopyEntityMessage : public CEntityStateMessage
{
  public:
    ULONG sm_ulTargetEntityId;
    FLOAT3D sm_vPosition;
    ANGLE3D sm_aOrientation;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_COPY;
    }

    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CTeleportEntityMessage : public CEntityStateMessage
{
  public:
    FLOAT3D sm_vPosition;
    ANGLE3D sm_aOrientation;
    FLOAT3D sm_vTranslation;
    ANGLE3D sm_aOrientation2;    
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_TELEPORT;
    }

    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CSetParentEntityMessage : public CEntityStateMessage
{
  public:
    ULONG sm_ulTargetEntityId;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SETPARENT;
    }

    CSetParentEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CDestroyEntityMessage : public CEntityStateMessage
{
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_DESTROY;
    }

    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};


//! TODO: Add comment.
class ENGINE_API CEventEntityMessage : public CEntityStateMessage
{
  public:
    SLONG sm_slEventCode;
    ULONG sm_ulEventDataSize;
    void *sm_pEventData;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_EVENT;
    }
    
    inline SLONG GetEventCode() const
    {
      return sm_slEventCode;
    }

    CEventEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CInitializeEntityMessage : public CEventEntityMessage
{
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_INITIALIZE;
    }

    CInitializeEntityMessage();
};

//! Operation with sound near the entity.
class ENGINE_API CSoundEntityMessage : public CEntityStateMessage
{
  public:
    ULONG sm_ulComponentOffset;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_BAD;
    }
    
    virtual UBYTE GetVolume() const
    {
      return 0x0;
    }
    
    virtual UBYTE GetPitch() const
    {
      return 0x0;
    }
    
    virtual UWORD GetFalloff() const
    {
      return 0;
    }
    
    virtual UWORD GetHotSpot() const
    {
      return 0;
    }
    
    inline ULONG GetComponentOffset() const
    {
      return sm_ulComponentOffset;
    }

    CSoundEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! Operation with sound near the entity.
class ENGINE_API CPlaySoundEntityMessage : public CSoundEntityMessage
{
  public:
    ULONG sm_ulSoundComponent;
    UBYTE sm_ubVolume;
    UBYTE sm_ubPitch;
    UWORD sm_ulFallOff;
    UWORD sm_ulHotSpot;
    SLONG sm_slPlayType;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SOUND_PLAY;
    }
    
    virtual UBYTE GetVolume() const
    {
      return sm_ubVolume;
    }
    
    virtual UBYTE GetPitch() const
    {
      return sm_ubPitch;
    }
    
    virtual UWORD GetFalloff() const
    {
      return sm_ulFallOff;
    }
    
    virtual UWORD GetHotSpot() const
    {
      return sm_ulHotSpot;
    }

    CPlaySoundEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! Operation with sound near the entity.
class ENGINE_API CStopSoundEntityMessage : public CSoundEntityMessage
{
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SOUND_STOP;
    }
};

//! Operation with sound near the entity.
class ENGINE_API CVolumeSoundEntityMessage : public CSoundEntityMessage
{
  public:
    UBYTE sm_ubVolume;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SOUND_VOLUME;
    }
    
    virtual UBYTE GetVolume() const
    {
      return sm_ubVolume;
    }

    CVolumeSoundEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! Operation with sound near the entity.
class ENGINE_API CPitchSoundEntityMessage : public CSoundEntityMessage
{
  public:
    UBYTE sm_ubPitch;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SOUND_PITCH;
    }

    virtual UBYTE GetPitch() const
    {
      return sm_ubPitch;
    }
    
    CPitchSoundEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! Operation with sound near the entity.
class ENGINE_API CRangeSoundEntityMessage : public CSoundEntityMessage
{
  public:
    UWORD sm_ulFallOff;
    UWORD sm_ulHotSpot;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SOUND_RANGE;
    }
    
    virtual UWORD GetFalloff() const
    {
      return sm_ulFallOff;
    }
    
    virtual UWORD GetHotSpot() const
    {
      return sm_ulHotSpot;
    }
    
    CRangeSoundEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

//! Operation with sound near the entity.
class ENGINE_API CFadeSoundEntityMessage : public CSoundEntityMessage
{
  public:
    UBYTE sm_ubVolume;
    FLOAT sm_fTime;
  
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ENTITY_SOUND_FADE;
    }

    virtual UBYTE GetVolume() const
    {
      return sm_ubVolume;
    }
    
    CFadeSoundEntityMessage();
    void Read_t(CNetworkMessage &nmMessage);
    void Write_t(CNetworkMessage &nmMessage);
};

#endif