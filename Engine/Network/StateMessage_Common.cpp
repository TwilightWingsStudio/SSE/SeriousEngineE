/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Shell.h>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Network/Network.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/SessionState.h>

CPauseMessage::CPauseMessage()
{
  sm_bNewState = FALSE;
}

void CPauseMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_SEQ_PAUSE);

  nmMessage >> (INDEX&)sm_bNewState;
  nmMessage >> sm_strPauser;
}

void CPauseMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_SEQ_PAUSE);

  nmMessage << INDEX(sm_bNewState);
  nmMessage << sm_strPauser;
}

CAllActionsMessage::CAllActionsMessage()
{
  sm_aActions.New(NET_MAXGAMEPLAYERS);
}

void CAllActionsMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_SEQ_ALLACTIONS);
  
  nmMessage >> sm_tmPacket;
  
    
  /*
  CSessionState &ses = _pNetwork->ga_sesSessionState;

  FOREACHINSTATICARRAY(sm_aActions, CPlayerAction, itpa)
  {
    nmMessage.Read(itpa, sizeof(CPlayerAction);
  }
  */
}

void CAllActionsMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_SEQ_ALLACTIONS);
  
  nmMessage << sm_tmPacket;

/*
  FOREACHINSTATICARRAY(sm_aActions, CPlayerAction, itpa)
  {
    nmMessage.Write(itpa, sizeof(CPlayerAction);
  }
  */
}
