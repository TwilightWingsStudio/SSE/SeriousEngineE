/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SESSIONSOCKET_H
#define SE_INCL_SESSIONSOCKET_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Timer.h>

//! Parameters for a client.
class ENGINE_API CSessionSocketParams
{
  public:
    INDEX ssp_iBufferActions;
    INDEX ssp_iMaxBPS;
    INDEX ssp_iMinBPS;

  public:
    //! Constructor.
    CSessionSocketParams(void);

    //! Reset the data.
    void Clear(void);

    //! Check if up to date with current params.
    BOOL IsUpToDate(void);

    //! Update.
    void Update(void);

    // Message operations.
    friend CNetworkMessage &operator<<(CNetworkMessage &nm, CSessionSocketParams &ssp);
    friend CNetworkMessage &operator>>(CNetworkMessage &nm, CSessionSocketParams &ssp);
};

#define SSOF_VIP            (1L << 0)
#define SSOF_MUTED          (1L << 1)

//! Connection data for each session connected to a server.
class ENGINE_API CSessionSocket
{
  public:
    enum State
    {
      STATE_IDLE = 0,
      STATE_CONNECTING,
      STATE_CONNECTED,
    };
  
  public:
    enum State sso_eState;
    ULONG sso_ulFlags;
    CTimerValue sso_tvMessageReceived;
    TIME sso_tmLastSyncReceived;
    INDEX sso_iDisconnectedState;
    INDEX sso_iLastSentSequence;
    INDEX sso_ctBadSyncs;   // counter of bad sync in row
    CTimerValue sso_tvLastMessageSent;    // for sending keep-alive messages
    CTimerValue sso_tvLastPingSent;       // for sending ping
    CNetworkStream sso_nsBuffer;  // stream of blocks buffered for sending
    CSessionSocketParams sso_sspParams; // parameters that the client wants
    INDEX sso_ctLocalPlayers;     // number of players that this client will connect

  public:
    //! Constructor.
    CSessionSocket(void);

    //! Destructor.
    ~CSessionSocket(void);

    //! Reset the data.
    void Clear(void);

    //! TODO: Add comment.
    void Activate(void);

    //! TODO: Add comment.
    void Deactivate(void);

    //! TODO: Add comment.
    BOOL IsActive() const;
    
    //! TODO: Add comment.
    BOOL IsReadyForStream() const;

    //! TODO: Add comment.
    BOOL IsVIP() const;

    //! TODO: Add comment.
    BOOL IsMuted() const;
    
    //! TODO: Add comment.
    void SetMuted(BOOL bNewState);

    //! TODO: Add comment.
    INDEX GetPlayerCount() const;
};

#endif  /* include-once check. */
