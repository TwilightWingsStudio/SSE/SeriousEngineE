/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_NETWORKPROTOCOL_H
#define SE_INCL_NETWORKPROTOCOL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Messages from server to all clients.
typedef enum MsgTypeSeq
{
  //! Error!
  MSG_SEQ_BAD = 0,

  //! Game was paused/unpaused.
  MSG_SEQ_PAUSE,
  
  //! Packed actions of all players from server to clients.
  MSG_SEQ_ALLACTIONS,

  //! Packed player action for specified player.
  MSG_SEQ_PLAYER_ACTION,

  //! Instructions for adding a new player to session states.
  MSG_SEQ_PLAYER_ADD,

  //! Instructions for removing a new player from session states.
  MSG_SEQ_PLAYER_REMOVE,

  //! A player has changed character.
  MSG_SEQ_PLAYER_CHARACTERCHANGE,

  //! Create a new entity.
  MSG_SEQ_ENTITY_CREATE,

  //! Make copy of existing entity.
  MSG_SEQ_ENTITY_COPY,

  //! Set position, orientation, velocity.
  MSG_SEQ_ENTITY_TELEPORT,
  
  //! Set parent for existing entity.
  MSG_SEQ_ENTITY_SETPARENT,

  //! End of entity existance.
  MSG_SEQ_ENTITY_DESTROY,

  //! Initialize existing entity.
  MSG_SEQ_ENTITY_INITIALIZE,

  //! Make entity handle the event.
  MSG_SEQ_ENTITY_EVENT,

  //! Play sound at the entity.
  MSG_SEQ_ENTITY_SOUND_PLAY,
  
  //! Stop sound at the entity.
  MSG_SEQ_ENTITY_SOUND_STOP,
  
  //! Set volume of sound at the entity.
  MSG_SEQ_ENTITY_SOUND_VOLUME,
  
  //! Set pitch of sound at the entity.
  MSG_SEQ_ENTITY_SOUND_PITCH,
  
  //! Set range of sound at the entity.
  MSG_SEQ_ENTITY_SOUND_RANGE,
  
  //! Fade sound at the entity.
  MSG_SEQ_ENTITY_SOUND_FADE,

  //! Amount of valid message types.
  MSG_SEQ_LAST,
} SEQ_MSGTYPE;

//! Messages from client to server.
typedef enum MsgTypeC2S
{
  //! Error!
  MSG_C2S_BAD = 0,

  //! Just nothing. Used as keep-alive.
  MSG_C2S_NOP,

  //! Action packet with buttons and axes.
  MSG_C2S_ACTION,

  //! Data to check if current state is valid.
  MSG_C2S_SYNCCHECK,

  //! Netscript.
  MSG_C2S_NETSETTINGS,
  
  //! Statedelta request.
  MSG_C2S_STATEDELTA,

  //! Connect to integrated server.
  MSC_C2S_CONNECTLOCAL,

  //! Connect to remote server.
  MSG_C2S_CONNECTREMOTE, // should be 7
  
  //! Request to add player.
  MSG_C2S_CONNECTPLAYER,
  
  //! Request CRC list.
  MSG_C2S_CRCLIST,

  //! Reply for CRC check.
  MSG_C2C_CRCCHECK,
  
  //! Request to toggle pause.
  MSG_C2S_PAUSE,
  
  //! Request to change player character.
  MSG_C2S_CHARACTERCHANGE,
  
  //! Request to resend specifict game stream blocks.
  MSG_C2S_GAMESTREAMRESEND,
  
  //! Request to write something into chat.
  MSG_C2S_CHAT,
  
  //! Request to execute admin command.
  MSG_C2S_ADMINCOMMAND,
  
  //! Disconnection confirmation.
  MSG_C2S_DISCONNECTEDREPLY,
  
  //! Amount of valid message types.
  MSG_C2S_LAST,
} C2S_MSGTYPE;

//! Messages from server to client.
typedef enum MsgTypeS2C
{
  //! Error!
  MSG_S2C_BAD = 0,

  //! Just nothing. Used as keep-alive.
  MSG_S2C_NOP,
  
  //! Print something into console.
  MSG_S2C_PRINT,

  //! Notify client that it was disconnected.
  MSG_S2C_DISCONNECTED, // should be 3

  //! Current state delta from original.
  MSG_S2C_STATEDELTA,

  //! Packet with one or more game stream blocks.
  MSG_S2C_GAMESTREAMBLOCKS,

  //! Player successfully added.
  MSG_S2C_CONNECTPLAYER,

  //! List with files for CRC check.
  MSG_S2C_CRCCHECK,

  //! Message with pings for all players.
  MSG_S2C_PINGS,

  //! Add chat message.
  MSG_S2C_CHAT,

  //! Results of the console command.
  MSG_S2C_ADMINRESPONSE,
  
  //! Amount of valid message types.
  MSG_S2C_LAST,
  
  //!
  MSG_S2C_CONNECTLOCAL,
  
  //!
  MSG_S2C_CONNECTREMOTE,

  //! Rcon
  MSG_S2C_EXTRA = '/',
} S2C_MSGTYPE;

#endif