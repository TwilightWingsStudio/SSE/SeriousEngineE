/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Shell.h>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Network/Network.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/SessionState.h>
#include <Engine/Network/StateMessage.h>
#include <Engine/Network/StateMessage_Player.h>

CPlayerStateMessage::CPlayerStateMessage()
{
  sm_iPlayer = -1;
}

void CPlayerStateMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  nmMessage >> sm_iPlayer;
}

void CPlayerStateMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  nmMessage << sm_iPlayer;
}

void CActionPlayerMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Read_t(nmMessage);
  nmMessage >> sm_paAction;
}

void CActionPlayerMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Write_t(nmMessage);
  nmMessage << sm_paAction;
}

void CAddPlayerMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Read_t(nmMessage);
  nmMessage >> sm_pcCharacter;
}

void CAddPlayerMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Write_t(nmMessage);
  nmMessage << sm_pcCharacter;
}

void CRemovePlayerMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Read_t(nmMessage);
}

void CRemovePlayerMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  nmMessage << sm_iPlayer;
}

void CCharacterChangePlayerMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Read_t(nmMessage);
  nmMessage >> sm_pcCharacter;
}

void CCharacterChangePlayerMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CPlayerStateMessage::Write_t(nmMessage);
  nmMessage << sm_pcCharacter;
}