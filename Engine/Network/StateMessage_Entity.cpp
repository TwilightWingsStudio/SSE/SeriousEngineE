/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Console.h>

#include <Engine/Network/StateMessage.h>
#include <Engine/Network/StateMessage_Entity.h>

CEntityStateMessage::CEntityStateMessage()
{
  sm_ulEntityId = ULONG(0xFFFFFFFF);
}

void CEntityStateMessage::Read_t(CNetworkMessage &nmMessage)
{
  nmMessage >> sm_ulEntityId;
}

void CEntityStateMessage::Write_t(CNetworkMessage &nmMessage)
{
  nmMessage << sm_ulEntityId;
}

void CCreateEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Read_t(nmMessage);

  nmMessage >> sm_uwEntityClassId;

  // Position
  nmMessage >> sm_vPosition(1);
  nmMessage >> sm_vPosition(2);
  nmMessage >> sm_vPosition(3);
}

void CCreateEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Write_t(nmMessage);
  
  nmMessage << sm_uwEntityClassId;

  // Position
  nmMessage << sm_vPosition(1);
  nmMessage << sm_vPosition(2);
  nmMessage << sm_vPosition(3);
}

void CCopyEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Read_t(nmMessage);
  
  nmMessage >> sm_ulTargetEntityId;

  // Position
  nmMessage >> sm_vPosition(1);
  nmMessage >> sm_vPosition(2);
  nmMessage >> sm_vPosition(3);
  
  // Orientation
  nmMessage >> sm_aOrientation(1);
  nmMessage >> sm_aOrientation(2);
  nmMessage >> sm_aOrientation(3);
}

void CCopyEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Write_t(nmMessage);
  
  nmMessage << sm_ulTargetEntityId;

  // Position
  nmMessage << sm_vPosition(1);
  nmMessage << sm_vPosition(2);
  nmMessage << sm_vPosition(3);

  // Orientation
  nmMessage << sm_aOrientation(1);
  nmMessage << sm_aOrientation(2);
  nmMessage << sm_aOrientation(3);
}

void CTeleportEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CEntityStateMessage::Read_t(nmMessage);
  
  // Position
  nmMessage >> sm_vPosition(1);
  nmMessage >> sm_vPosition(2);
  nmMessage >> sm_vPosition(3);
}

void CTeleportEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Write_t(nmMessage);
  
  // Position
  nmMessage << sm_vPosition(1);
  nmMessage << sm_vPosition(2);
  nmMessage << sm_vPosition(3);
}

CSetParentEntityMessage::CSetParentEntityMessage() : CEntityStateMessage()
{
  sm_ulTargetEntityId = ULONG(0xFFFFFFFF);
}

void CSetParentEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CEntityStateMessage::Read_t(nmMessage);
  nmMessage >> sm_ulTargetEntityId;
}

void CSetParentEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CEntityStateMessage::Write_t(nmMessage);
  nmMessage << sm_ulTargetEntityId;
}

void CDestroyEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Read_t(nmMessage);
}

void CDestroyEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Write_t(nmMessage);
}

CEventEntityMessage::CEventEntityMessage()
{
  sm_slEventCode = 0;
  sm_ulEventDataSize = 0;
  sm_pEventData = NULL;
}

void CEventEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Read_t(nmMessage);

  // ubTypeId, ulEntityId, slEventCode
  sm_ulEventDataSize = nmMessage.nm_slSize - sizeof(UBYTE) - sizeof(ULONG) - sizeof(SLONG);
  sm_pEventData = AllocMemory(sm_ulEventDataSize);

  nmMessage >> sm_slEventCode;
  nmMessage.Read((UBYTE *)sm_pEventData, sm_ulEventDataSize);
}

void CEventEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CEntityStateMessage::Write_t(nmMessage);
  nmMessage << sm_slEventCode;
  nmMessage.Write((UBYTE *)sm_pEventData, sm_ulEventDataSize);
}

CInitializeEntityMessage::CInitializeEntityMessage()
{
  sm_slEventCode = 0;
  sm_ulEventDataSize = 0;
  sm_pEventData = NULL;
}

CSoundEntityMessage::CSoundEntityMessage()
{
  sm_ulComponentOffset = 0xFFFFFFFF;
}

void CSoundEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  CEntityStateMessage::Read_t(nmMessage);
  nmMessage >> sm_ulComponentOffset;
}

void CSoundEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  CEntityStateMessage::Write_t(nmMessage);
  nmMessage << sm_ulComponentOffset;
}

CPlaySoundEntityMessage::CPlaySoundEntityMessage()
{
  sm_ulSoundComponent = 0xFFFFFFFF;
  sm_ubVolume = 0xFF;
  sm_ubPitch = 0xFF;
  sm_ulFallOff = 0xFFFF;
  sm_ulHotSpot = 0xFFFF;
  sm_slPlayType = 0;
}

void CPlaySoundEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Read_t(nmMessage);
  nmMessage >> sm_ulSoundComponent;
  nmMessage >> sm_ubVolume;
  nmMessage >> sm_ubPitch;
  nmMessage >> sm_ulFallOff;
  nmMessage >> sm_ulHotSpot;
  nmMessage >> sm_slPlayType;
}

void CPlaySoundEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Write_t(nmMessage);
  nmMessage << sm_ulSoundComponent;
  nmMessage << sm_ubVolume;
  nmMessage << sm_ubPitch;
  nmMessage << sm_ulFallOff;
  nmMessage << sm_ulHotSpot;
  nmMessage << sm_slPlayType;
}

CVolumeSoundEntityMessage::CVolumeSoundEntityMessage()
{
  sm_ubVolume = 0xFF;
}

void CVolumeSoundEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Read_t(nmMessage);
  nmMessage >> sm_ubVolume;
}

void CVolumeSoundEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Write_t(nmMessage);
  nmMessage << sm_ubVolume;
}

CPitchSoundEntityMessage::CPitchSoundEntityMessage()
{
  sm_ubPitch = 0xFF;
}

void CPitchSoundEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Read_t(nmMessage);
  nmMessage >> sm_ubPitch;
}

void CPitchSoundEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CSoundEntityMessage::Write_t(nmMessage);
  nmMessage << sm_ubPitch;
}

CRangeSoundEntityMessage::CRangeSoundEntityMessage()
{
  sm_ulFallOff = 0xFFFF;
  sm_ulHotSpot = 0xFFFF;
}

void CRangeSoundEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CSoundEntityMessage::Read_t(nmMessage);
  nmMessage >> sm_ulFallOff;
  nmMessage >> sm_ulHotSpot;
}

void CRangeSoundEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Write_t(nmMessage);
  nmMessage << sm_ulFallOff;
  nmMessage << sm_ulHotSpot;
}

CFadeSoundEntityMessage::CFadeSoundEntityMessage()
{
  sm_ubVolume = 0xFF;
  sm_fTime = 0.0F;
}

void CFadeSoundEntityMessage::Read_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());
  
  CSoundEntityMessage::Read_t(nmMessage);
  nmMessage >> sm_ubVolume;
  nmMessage >> sm_fTime;
}

void CFadeSoundEntityMessage::Write_t(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == GetType());

  CSoundEntityMessage::Write_t(nmMessage);
  nmMessage << sm_ubVolume;
  nmMessage << sm_fTime;
}