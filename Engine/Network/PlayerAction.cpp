/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Network/NetworkMessage.h>

#include <Core/Math/Functions.h>
#include <Core/Base/CRC.h>
#include <Core/IO/Stream.h>

CPlayerAction::CPlayerAction(void)
{
  Clear();
}

/*
 * Clear the object (this sets up no actions).
 */
void CPlayerAction::Clear(void)
{
  pa_vTranslation = FLOAT3D(0.0f, 0.0f, 0.0f);
  pa_aRotation = ANGLE3D(0, 0, 0);
  pa_aViewRotation = ANGLE3D(0, 0, 0);
  pa_ulButtons = 0;
  pa_llCreated = 0;
}

// normalize action (remove invalid floats like -0)
void CPlayerAction::Normalize(void)
{
  volatile FLOAT *pf = (FLOAT*)&pa_vTranslation;

  for (INDEX i = 0; i < 9; i++)
  {
    if (*pf == 0) {
      *pf = 0;
    }

    pf++;
  }
}
// create a checksum value for sync-check
void CPlayerAction::ChecksumForSync(ULONG &ulCRC)
{
  CRC_AddBlock(ulCRC, (UBYTE*)this, sizeof(this));
}

#define DUMPVECTOR(v) \
  strm.FPrintF_t(#v ":  %g,%g,%g %08x,%08x,%08x\n", \
    (v)(1), (v)(2), (v)(3), (ULONG&)(v)(1), (ULONG&)(v)(2), (ULONG&)(v)(3))
#define DUMPLONG(l) \
  strm.FPrintF_t(#l ":  %08x\n", l)

// dump sync data to text file
void CPlayerAction::DumpSync_t(CTStream &strm) 
{
  DUMPVECTOR(pa_vTranslation);
  DUMPVECTOR(pa_aRotation);
  DUMPVECTOR(pa_aViewRotation);
  DUMPLONG(pa_ulButtons);
}

void CPlayerAction::Lerp(const CPlayerAction &pa0, const CPlayerAction &pa1, FLOAT fFactor)
{
  pa_vTranslation  = ::Lerp(pa0.pa_vTranslation , pa1.pa_vTranslation , fFactor);
  pa_aRotation     = ::Lerp(pa0.pa_aRotation    , pa1.pa_aRotation    , fFactor);
  pa_aViewRotation = ::Lerp(pa0.pa_aViewRotation, pa1.pa_aViewRotation, fFactor);
  pa_ulButtons = pa1.pa_ulButtons;
}

// player action compression algorithm:
// - all axes (9 of them) are compressed as one bit telling whether the axis value is used
// if that bit is ==0, then the axis value is 0.0, if it is 1, the value follows in next 32 
// bits
// - the flags are compressed by preceding them with a bit sequence telling how many bits
// are saved after that:
//   (0)          1       = no bits follow, value is 0
//   (1)          01      = no bits follow, value is 1
//   (2-3)        001     = 1 bit follows, value is 1x where x is the given bit
//   (4-15)       0001    = 4 bit value follows
//   (16-255)     00001   = 8 bit value follows
//   (256-65535)  000001  = 16 bit value follows
//   (65536-)     000000  = 32 bit value follows
// note: above bits are ordered in reverse as they come when scanning bit by bit

/* Write an object into message. */
CNetworkMessage &operator<<(CNetworkMessage &nm, const CPlayerAction &pa)
{
  nm.Write(&pa.pa_llCreated, sizeof(pa.pa_llCreated));

  const ULONG *pul = (const ULONG*)&pa.pa_vTranslation;

  for (INDEX i = 0; i < 9; i++)
  {
    if (*pul == 0) {
      UBYTE ub = 0;
      nm.WriteBits(&ub, 1);

    } else {
      UBYTE ub=1;
      nm.WriteBits(&ub, 1);
      nm.WriteBits(pul, 32);
    }

    pul++;
  }

  ULONG ulFlags = pa.pa_ulButtons;

  // (0)          1       = no bits follow, value is 0
  if (ulFlags == 0) {
    UBYTE ub = 1;
    nm.WriteBits(&ub, 1);

  // (1)          01      = no bits follow, value is 1
  } else if (ulFlags == 1) {
    UBYTE ub = 2;
    nm.WriteBits(&ub, 2);

  // (2-3)        001     = 1 bit follows, value is 1x where x is the given bit
  } else if (ulFlags <= 3) {
    UBYTE ub = 4;
    nm.WriteBits(&ub, 3);
    nm.WriteBits(&ulFlags, 1);

  // (4-15)       0001    = 4 bit value follows
  } else if (ulFlags <= 15) {
    UBYTE ub = 8;
    nm.WriteBits(&ub, 4);
    nm.WriteBits(&ulFlags, 4);

  // (16-255)     00001   = 8 bit value follows
  } else if (ulFlags <= 255) {
    UBYTE ub = 16;
    nm.WriteBits(&ub, 5);
    nm.WriteBits(&ulFlags, 8);

  // (256-65535)  000001  = 16 bit value follows
  } else if (ulFlags <= 65535) {
    UBYTE ub = 32;
    nm.WriteBits(&ub, 6);
    nm.WriteBits(&ulFlags, 16);

  // (65536-)     000000  = 32 bit value follows
  } else {
    UBYTE ub = 0;
    nm.WriteBits(&ub, 6);
    nm.WriteBits(&ulFlags, 32);
  }

  return nm;
}

/* Read an object from message. */
CNetworkMessage &operator>>(CNetworkMessage &nm, CPlayerAction &pa)
{
  nm.Read(&pa.pa_llCreated, sizeof(pa.pa_llCreated));

  ULONG *pul = (ULONG*)&pa.pa_vTranslation;

  for (INDEX i = 0; i < 9; i++)
  {
    UBYTE ub = 0;
    nm.ReadBits(&ub, 1);

    if (ub == 0) {
      *pul = 0;
    } else {
      nm.ReadBits(pul, 32);
    }

    pul++;
  }

  // find number of zero bits for flags
  INDEX iZeros = 0;

  for (; iZeros < 6; iZeros++)
  {
    UBYTE ub = 0;
    nm.ReadBits(&ub, 1);

    if (ub != 0) {
      break;
    }
  }

  ULONG ulFlags = 0;

  // now read flags according to the number of bits
  // (0)          1       = no bits follow, value is 0
  if (iZeros == 0) {
    ulFlags = 0;

  // (1)          01      = no bits follow, value is 1
  } else if (iZeros == 1) {
    ulFlags = 1;

  // (2-3)        001     = 1 bit follows, value is 1x where x is the given bit
  } else if (iZeros == 2) {
    ulFlags = 0;
    nm.ReadBits(&ulFlags, 1);
    ulFlags |= 2;

  // (4-15)       0001    = 4 bit value follows
  } else if (iZeros == 3) {
    ulFlags = 0;
    nm.ReadBits(&ulFlags, 4);

  // (16-255)     00001   = 8 bit value follows
  } else if (iZeros == 4) {
    ulFlags = 0;
    nm.ReadBits(&ulFlags, 8);

  // (256-65535)  000001  = 16 bit value follows
  } else if (iZeros == 5) {
    ulFlags = 0;
    nm.ReadBits(&ulFlags, 16);

  // (65536-)     000000  = 32 bit value follows
  } else {
    ulFlags = 0;
    nm.ReadBits(&ulFlags, 32);
  }

  pa.pa_ulButtons = ulFlags;
  return nm;
}

/* Write an object into stream. */
CTStream &operator<<(CTStream &strm, const CPlayerAction &pa)
{
  strm.Write_t(&pa, sizeof(pa));
  return strm;
}

/* Read an object from stream. */
CTStream &operator>>(CTStream &strm, CPlayerAction &pa)
{
  strm.Read_t(&pa, sizeof(pa));
  return strm;
}
