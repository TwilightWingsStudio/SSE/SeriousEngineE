/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PLAYERACTION_H
#define SE_INCL_PLAYERACTION_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class ENGINE_API CPlayerAction
{
  public:
    // order is important for compression and normalization - do not reorder!
    FLOAT3D pa_vTranslation;
    ANGLE3D pa_aRotation;
    ANGLE3D pa_aViewRotation;
    ULONG pa_ulButtons;       // 32 bits for action buttons (application defined)
      // keep flags that are likely to be changed/set more often at lower bits,
      // so that better compression can be achieved for network transmission
    __int64 pa_llCreated;     // when was created (for ping calc.) in ms

  public:
    //! Constructor.
    CPlayerAction(void);

    //! Clear the object (this sets up no actions).
    void Clear(void);

    //! Normalize action (remove invalid floats like -0)
    void Normalize(void);

    //! Create a checksum value for sync-check
    void ChecksumForSync(ULONG &ulCRC);

    //! Dump sync data to text file
    void DumpSync_t(CTStream &strm);  // throw char *

    // TODO: Add comment.
    void Lerp(const CPlayerAction &pa0, const CPlayerAction &pa1, FLOAT fFactor);

    //! Write an object into message.
    friend CNetworkMessage &operator<<(CNetworkMessage &nm, const CPlayerAction &pa);

    //! Read an object from message.
    friend CNetworkMessage &operator>>(CNetworkMessage &nm, CPlayerAction &pa);

    //! Write an object into stream.
    friend CTStream &operator<<(CTStream &strm, const CPlayerAction &pa);

    //! Read an object from stream.
    friend CTStream &operator>>(CTStream &strm, CPlayerAction &pa);
};

#endif /* include-once check. */