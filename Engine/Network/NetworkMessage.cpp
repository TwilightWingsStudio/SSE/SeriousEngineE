/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Network/NetworkMessage.h>
#include <Core/Compression/CompressionManager.h>

#include <Core/Math/Functions.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Console.h>
#include <Core/Base/CTString.h>
#include <Core/IO/Stream.h>
#include <Core/Base/ErrorTable.h>
#include <Core/Base/ErrorReporting.h>

#include <Core/Base/ListIterator.inl>


/*
static struct ErrorCode ErrorCodes[] = {
// message types
  ERRORCODE(MSG_REQ_ENUMSERVERS, "MSG_REQ_ENUMSERVERS"),
  ERRORCODE(MSG_SERVERINFO, "MSG_SERVERINFO"),
  ERRORCODE(MSG_INF_DISCONNECTED, "MSG_INF_DISCONNECTED"),
  ERRORCODE(MSG_REQ_CONNECTLOCALSESSIONSTATE, "MSG_REQ_CONNECTLOCALSESSIONSTATE"),
  ERRORCODE(MSG_REP_CONNECTLOCALSESSIONSTATE, "MSG_REP_CONNECTLOCALSESSIONSTATE"),
  ERRORCODE(MSG_REQ_CONNECTREMOTESESSIONSTATE, "MSG_REQ_CONNECTREMOTESESSIONSTATE"),
  ERRORCODE(MSG_REP_CONNECTREMOTESESSIONSTATE, "MSG_REP_CONNECTREMOTESESSIONSTATE"),
  ERRORCODE(MSG_REQ_STATEDELTA, "MSG_REQ_STATEDELTA"),
  ERRORCODE(MSG_REQ_STATEDELTA, "MSG_REP_STATEDELTA"),
  ERRORCODE(MSG_REQ_CONNECTPLAYER, "MSG_REQ_CONNECTPLAYER"),
  ERRORCODE(MSG_REP_CONNECTPLAYER, "MSG_REP_CONNECTPLAYER"),
  ERRORCODE(MSG_ACTION, "MSG_ACTION"),
  ERRORCODE(MSG_ACTIONPREDICT, "MSG_ACTIONPREDICT"),
  ERRORCODE(MSG_SEQ_ALLACTIONS, "MSG_SEQ_ALLACTIONS"),   
  ERRORCODE(MSG_SEQ_PLAYER_ADD, "MSG_SEQ_PLAYER_ADD"),    
  ERRORCODE(MSG_SEQ_PLAYER_REMOVE, "MSG_SEQ_PLAYER_REMOVE"),    
  ERRORCODE(MSG_GAMESTREAMBLOCKS, "MSG_GAMESTREAMBLOCKS"), 
  ERRORCODE(MSG_REQUESTGAMESTREAMRESEND, "MSG_REQUESTGAMESTREAMRESEND"),
};

extern struct ErrorTable MessageTypes = ERRORTABLE(ErrorCodes);
*/

/////////////////////////////////////////////////////////////////////
// CNetworkMessage

/*
 * Constructor for empty message (for receiving).
 */
CNetworkMessage::CNetworkMessage(void)
{
  // allocate message buffer
  nm_slMaxSize = MAX_NETWORKMESSAGE_SIZE;
  nm_pubMessage = (UBYTE*) AllocMemory(nm_slMaxSize);

  // mangle pointer and size so that it could not be accidentally read/written
  nm_pubPointer = NULL;
  nm_iBit = -1;
  nm_slSize = -1;
}

// reinit a message that is to be sent (to write different contents)
void CNetworkMessage::Reinit(void)
{
  // init read/write pointer and size
  nm_slMaxSize = MAX_NETWORKMESSAGE_SIZE;
  nm_pubPointer = nm_pubMessage;
  nm_iBit = 0;
  nm_slSize = 0;

  // write the type
  UBYTE ubType = nm_ubType;
  Write(&ubType, sizeof(ubType));
}

/*
 * Constructor for initializing message that is to be sent.
 */
CNetworkMessage::CNetworkMessage(UBYTE ubType)
{
  // allocate message buffer
  nm_slMaxSize = MAX_NETWORKMESSAGE_SIZE;
  nm_pubMessage = (UBYTE*) AllocMemory(nm_slMaxSize);

  // init read/write pointer and size
  nm_pubPointer = nm_pubMessage;
  nm_iBit = 0;
  nm_slSize = 0;

  // remember the type
  nm_ubType = ubType;

  // write the type
  Write(&ubType, sizeof(ubType));
}

/* Copying. */
CNetworkMessage::CNetworkMessage(const CNetworkMessage &nmOriginal)
{
  // allocate message buffer
  nm_slMaxSize = nmOriginal.nm_slMaxSize;
  nm_pubMessage = (UBYTE*) AllocMemory(nm_slMaxSize);

  // init read/write pointer and size
  nm_pubPointer = nm_pubMessage + (nmOriginal.nm_pubPointer-nmOriginal.nm_pubMessage);
  nm_iBit = nmOriginal.nm_iBit;
  nm_slSize = nmOriginal.nm_slSize;

  // copy from original
  memcpy(nm_pubMessage, nmOriginal.nm_pubMessage, nm_slSize);

  // remember the type
  nm_ubType = nmOriginal.nm_ubType;
}

void CNetworkMessage::operator=(const CNetworkMessage &nmOriginal)
{
  if (nm_slMaxSize != nmOriginal.nm_slMaxSize) {
    if (nm_pubMessage != NULL) {
      FreeMemory(nm_pubMessage);
    }

    // allocate message buffer
    nm_slMaxSize = nmOriginal.nm_slMaxSize;
    nm_pubMessage = (UBYTE*) AllocMemory(nm_slMaxSize);
  }

  // init read/write pointer and size
  nm_pubPointer = nm_pubMessage + sizeof(UBYTE);// + (nmOriginal.nm_pubPointer-nmOriginal.nm_pubMessage);
  nm_iBit = 0;
  nm_slSize = nmOriginal.nm_slSize;

  // copy from original
  memcpy(nm_pubMessage, nmOriginal.nm_pubMessage, nm_slSize);

  // remember the type
  nm_ubType = nmOriginal.nm_ubType;
}

/*
 * Destructor.
 */
CNetworkMessage::~CNetworkMessage(void)
{
  ASSERT(nm_pubMessage != NULL);

  if (nm_pubMessage != NULL) {
    FreeMemory(nm_pubMessage);
  }
}

/*
 * Ignore the contents of this message.
 */
void CNetworkMessage::IgnoreContents(void)
{
}

/* Check if end of message. */
BOOL CNetworkMessage::IsAtEnd(void)
{
  ASSERTMSG((nm_pubPointer - nm_pubMessage) <= nm_slSize, "Message over-reading!");

  return (nm_pubPointer - nm_pubMessage) >= nm_slSize;
}

// read/write functions
void CNetworkMessage::Read(void *pvBuffer, SLONG slSize)
{
  if (nm_pubPointer + slSize > nm_pubMessage + nm_slSize) {
    CWarningF(TRANS("Warning: Message over-reading!\n"));
    ASSERT(FALSE);
    memset(pvBuffer, 0, slSize);
    return;
  }

  memcpy(pvBuffer, nm_pubPointer, slSize);
  nm_pubPointer += slSize;
  nm_iBit = 0;
}

void CNetworkMessage::Write(const void *pvBuffer, SLONG slSize)
{
  if (nm_pubPointer + slSize > nm_pubMessage + nm_slMaxSize) {
    CWarningF(TRANS("Warning: Message over-writing!\n"));
    ASSERT(FALSE);
    return;
  }

  memcpy(nm_pubPointer, pvBuffer, slSize);
  nm_pubPointer += slSize;
  nm_iBit = 0;
  nm_slSize += slSize;
}

CNetworkMessage &CNetworkMessage::operator>>(CTString &str)
{
  // start reading string from message
  str = "";
  nm_iBit = 0;

  // repeat
  for (;;)
  {
    // if reached end of message (this happens when we read string-only messages)
    if (nm_pubPointer - nm_pubMessage >= nm_slSize) {
      // stop
      return *this;
    }

    // get next char
    char strChar[2];
    strChar[0] = *nm_pubPointer++;
    strChar[1] = 0;

    // if end of string
    if (strChar[0] == 0) {
      // stop
      return *this;
    // if normal char
    } else {
      // append to the string
      str += strChar; // FIXME reallocation may cause performance drop.
    }
  }
}

CNetworkMessage &CNetworkMessage::operator<<(const CTString &str)
{
  // start writing string to message
  nm_iBit = 0;
  const char *pstr = str.ConstData();

  // repeat
  for (;;)
  {
    // if reached one byte before end of message
    if (nm_pubPointer - nm_pubMessage >= nm_slMaxSize - 1) {
      // put the end marker
      *nm_pubPointer++ = 0;
      nm_slSize++;

      // report error and stop
      CWarningF(TRANS("Warning: Message over-writing!\n"));
      ASSERT(FALSE);

      return *this;
    }

    // get next char
    const char chr = *pstr++;

    // write it to message
    *nm_pubPointer++ = chr;
    nm_slSize++;

    // if end
    if (chr == 0) {
      // stop
      return *this;
    }
  }
}

/*
 * Insert a sub-message into this message.
 */
void CNetworkMessage::InsertSubMessage(const CNetworkMessage &nmSubMessage)
{
  // write sub-message size
  operator << (nmSubMessage.nm_slSize);

  // write the contents of the sub-message
  Write(nmSubMessage.nm_pubMessage, nmSubMessage.nm_slSize);
}

/*
 * Extract a sub-message from this message.
 */
void CNetworkMessage::ExtractSubMessage(CNetworkMessage &nmSubMessage)
{
  // read sub-message size
  operator >> (nmSubMessage.nm_slSize);

  // read the contents of the sub-message
  Read(nmSubMessage.nm_pubMessage, nmSubMessage.nm_slSize);

  // init the submessage read/write pointer
  nmSubMessage.nm_pubPointer = nmSubMessage.nm_pubMessage;
  nmSubMessage.nm_iBit = 0;

  // get the submessage type
  UBYTE ubType = 0;
  nmSubMessage >> ubType;
  nmSubMessage.nm_ubType = ubType;
}

// rewind message to start, so that written message can be read again
void CNetworkMessage::Rewind(void)
{
  nm_pubPointer = nm_pubMessage + sizeof(UBYTE);
  nm_iBit = 0;
}

/*
 * Pack a message to another message (message type is left untouched).
 */
void CNetworkMessage::Pack(CNetworkMessage &nmPacked, CompressionMethod eCompressionMethod)
{
  // get size and pointers for packing, leave the message type alone
  SLONG slUnpackedSize = nm_slSize - sizeof(UBYTE);
  void *pvUnpacked     = nm_pubMessage + sizeof(UBYTE);

  SLONG slPackedSize    = nmPacked.nm_slMaxSize - sizeof(UBYTE);
  void *pvPacked        = nmPacked.nm_pubMessage + sizeof(UBYTE);

  // pack it there
  //ASSERT(comp.NeededDestinationSize(slUnpackedSize) <= slPackedSize);
  BOOL bSucceded = _pCompressionMgr->Pack(pvPacked, slPackedSize, pvUnpacked, slUnpackedSize, eCompressionMethod);
  ASSERT(bSucceded);

  // set up the destination message size
  nmPacked.nm_slSize = slPackedSize + sizeof(UBYTE);
}
/*
 * Unpack a message to another message (message type is left untouched).
 */
void CNetworkMessage::Unpack(CNetworkMessage &nmUnpacked, CompressionMethod eCompressionMethod)
{
  // get size and pointers for unpacking, leave the message type alone
  SLONG slPackedSize = nm_slSize - sizeof(UBYTE);
  void *pvPacked     = nm_pubMessage + sizeof(UBYTE);

  SLONG slUnpackedSize = nmUnpacked.nm_slMaxSize - sizeof(UBYTE);
  void *pvUnpacked     = nmUnpacked.nm_pubMessage + sizeof(UBYTE);

  // unpack it there
  BOOL bSucceeded = _pCompressionMgr->Unpack(pvUnpacked, slUnpackedSize, pvPacked, slPackedSize, eCompressionMethod);
  ASSERT(bSucceeded);

  // set up the destination message size
  nmUnpacked.nm_slSize = slUnpackedSize + sizeof(UBYTE);
}

// NOTE:
// compression type bits in the messages are different than compression type cvar values
// this is to keep backward compatibility with old demos saved with full compression
void CNetworkMessage::PackDefault(CNetworkMessage &nmPacked)
{
  extern INDEX net_iCompression;

  if (net_iCompression == 2) {
    Pack(nmPacked, COMPRESSION_METHOD_DEFLATE);
    (int&)nmPacked.nm_ubType |= 0 << 6;

  } else if (net_iCompression == 1) {
    Pack(nmPacked, COMPRESSION_METHOD_LZRW1);
    (int&)nmPacked.nm_ubType |= 1 << 6;

  } else {
    // no packing
    SLONG slUnpackedSize = nm_slSize - sizeof(UBYTE);
    void *pvUnpacked     = nm_pubMessage + sizeof(UBYTE);
    void *pvPacked       = nmPacked . nm_pubMessage + sizeof(UBYTE);
    nmPacked.nm_slSize   = slUnpackedSize + sizeof(UBYTE);
    memcpy(pvPacked, pvUnpacked, slUnpackedSize);
    (int&)nmPacked.nm_ubType |= 2 << 6;
  }

  nmPacked.nm_pubMessage[0] = (UBYTE)nmPacked.nm_ubType;

  /*  
  // pack with RLE and LZ
  CNetworkMessage nmPackedRLE(GetType());
  CRLEBBCompressor compRLE;
  Pack(nmPackedRLE ,compRLE);
  CLZCompressor compLZ;
  nmPackedRLE.Pack(nmPacked, compLZ);
  //*/
}

void CNetworkMessage::UnpackDefault(CNetworkMessage &nmUnpacked)
{
  switch (nm_ubType >> 6)
  {
    case 0: {
      Unpack(nmUnpacked, COMPRESSION_METHOD_DEFLATE);
    } break;

    case 1: {
      Unpack(nmUnpacked, COMPRESSION_METHOD_LZRW1);
    } break;

    default:
    case 2: {
      // no unpacking
      SLONG slPackedSize = nm_slSize - sizeof(UBYTE);
      void *pvPacked     = nm_pubMessage + sizeof(UBYTE);
      void *pvUnpacked   = nmUnpacked.nm_pubMessage + sizeof(UBYTE);
      nmUnpacked.nm_slSize = slPackedSize + sizeof(UBYTE);
      memcpy(pvUnpacked, pvPacked, slPackedSize);
    } break;
  }

  /*
  // unpack with LZ and RLE
  CNetworkMessage nmUnpackedLZ(GetType());
  CLZCompressor compLZ;
  Unpack(nmUnpackedLZ,compLZ);
  CRLEBBCompressor compRLE;
  nmUnpackedLZ.Unpack(nmUnpacked, compRLE);
  //*/
}

// dump message to console
void CNetworkMessage::Dump(void)
{
  CInfoF("Message size: %d\n", nm_slSize);
  CInfoF("Message contents:");

  for (INDEX iByte = 0; iByte < nm_slSize; iByte++)
  {
    if (iByte % 16 == 0) {
      CInfoF("\n");
    }

    CInfoF("%02x", nm_pubMessage[iByte]);
  }

  CInfoF("\n--\n");
}

// shrink message buffer to exactly fit contents
void CNetworkMessage::Shrink(void)
{
  // remember original pointer offset
  SLONG slOffset = nm_pubPointer-nm_pubMessage;

  // allocate message buffer
  ShrinkMemory((void**)&nm_pubMessage, nm_slSize);
  nm_slMaxSize = nm_slSize;

  // set new pointer
  nm_pubPointer = nm_pubMessage + slOffset;
}

// copy one bit from a byte to another byte
static inline void CopyBit(const UBYTE &ubSrc, INDEX iSrc, UBYTE &ubDst, INDEX iDst)
{
  if (ubSrc & (1 << iSrc)) {
    ubDst |= (1 << iDst);

  } else {
    ubDst &= ~(1 << iDst);
  }
}

void CNetworkMessage::ReadBits(void *pvBuffer, INDEX ctBits)
{
  UBYTE *pubDstByte = (UBYTE *)pvBuffer;

  // for each bit
  INDEX iDstBit = 0;

  for (INDEX iBit = 0; iBit < ctBits; iBit++)
  {
    // get next bit and byte in message
    UBYTE *pubSrcByte = nm_pubPointer;
    INDEX iSrcBit = nm_iBit;

    // if bit is 0 - start of byte
    if (nm_iBit == 0) {
      // we will start reading from this byte, advance message to next byte
      nm_pubPointer++;
    // if bit is not 0 - inside byte
    } else {
      // we will read from previous byte, keep message as-is
      pubSrcByte--;
    }

    // copy the bit
    CopyBit(*pubSrcByte, iSrcBit, *pubDstByte, iDstBit);

    // go to next bit
    nm_iBit++;
    if (nm_iBit >= 8) {
      nm_iBit = 0;
    }

    iDstBit++;
    if (iDstBit >= 8) {
      iDstBit = 0;
      pubDstByte++;
    }
  }
}

void CNetworkMessage::WriteBits(const void *pvBuffer, INDEX ctBits)
{
  const UBYTE *pubSrcByte = (const UBYTE *)pvBuffer;

  // for each bit
  INDEX iSrcBit = 0;

  for (INDEX iBit = 0; iBit < ctBits; iBit++)
  {
    // get next bit and byte in message
    UBYTE *pubDstByte = nm_pubPointer;
    INDEX iDstBit = nm_iBit;

    // if bit is 0 - start of byte
    if (nm_iBit == 0) {
      // we will start writing to this byte, advance message to next byte
      nm_pubPointer++;
      nm_slSize++;

    // if bit is not 0 - inside byte
    } else {
      // we will write to previous byte, keep message as-is
      pubDstByte--;
    }

    // copy the bit
    CopyBit(*pubSrcByte, iSrcBit, *pubDstByte, iDstBit);

    // go to next bit
    nm_iBit++;

    if (nm_iBit >= 8) {
      nm_iBit = 0;
    }

    iSrcBit++;

    if (iSrcBit >= 8) {
      iSrcBit = 0;
      pubSrcByte++;
    }
  }
}
