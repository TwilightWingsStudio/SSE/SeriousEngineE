/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SERVER_H
#define SE_INCL_SERVER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Threading/Synchronization.h>
#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/SessionState.h>
#include <Core/Templates/StaticArray.h>

//! Server, manages game joining and similar, routes messages from PlayerSource to PlayerTarget 
class CServer
{
  public:
    BOOL srv_bActive;                        // set if started

    TStaticArray<CSessionSocket> srv_assoSessions;  // client computers in game
    TStaticArray<CPlayerBuffer> srv_aplbPlayers;  // player buffers for all clients in game
    TStaticArray<CSyncCheck> srv_ascChecks;       // remembered sync checks

    TIME srv_tmLastProcessedTick;            // last tick when all actions have been resent
    INDEX srv_iLastProcessedSequence;        // sequence of last sent game stream block

    BOOL srv_bPause;      // set while game is paused
    BOOL srv_bGameFinished; // set while game is finished
    FLOAT srv_fServerStep;  // counter for smooth time slowdown/speedup

  public:
    // Send disconnect message to some client.
    void SendDisconnectMessage(INDEX iClient, const char *strExplanation, BOOL bStream = FALSE);

    // Get total number of active players.
    INDEX GetPlayerCount(void);

    // Get number of active vip players.
    INDEX GetVipPlayerCount(void);

    // Get total number of active clients.
    INDEX GetClientCount(void);

    // Get number of active vip clients.
    INDEX GetVipClientCount(void);

    // Get number of active observers.
    INDEX GetObserverCount(void);

    // Get number of active players on one client.
    INDEX GetPlayerCountForClient(INDEX iClient);

    // Find first inactive player.
    CPlayerBuffer *FirstInactivePlayer(void);

    // Check if some character name already exists in this session.
    BOOL CharacterNameIsUsed(CPlayerCharacter &pcCharacter);

  public:
    void OnBad(INDEX iClient, CNetworkMessage &nm);
    
    void OnNop(INDEX iClient, CNetworkMessage &nm);
  
    void OnRepDisconnected(INDEX iClient, CNetworkMessage &nm);

    // Send initialization info to local client.
    void OnConnectLocalSessionState(INDEX iClient, CNetworkMessage &nm);

    // Send initialization info to remote client.
    void OnConnectRemoteSessionState(INDEX iClient, CNetworkMessage &nm);

    // Send session state data to remote client.
    void OnReqStateDelta(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnReqConnectPlayer(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnReqCharacterChange(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnAction(INDEX iClient, CNetworkMessage &nmMessage);

    void OnSyncCheck(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnReqGameStreamResend(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnReqPause(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnSetClientSettings(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnChatIn(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnCrcList(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnCrcCheck(INDEX iClient, CNetworkMessage &nmMessage);
    
    void OnAdminCommand(INDEX iClient, CNetworkMessage &nmMessage);


    // Send one regular batch of sequences to a client.
    void SendGameStreamBlocks(INDEX iClient);

    // Resend a batch of game stream blocks to a client.
    void ResendGameStreamBlocks(INDEX iClient, INDEX iSequence0, INDEX ctSequences);


    // Add a new sync check to buffer.
    void AddSyncCheck(const CSyncCheck &sc);

    // Try to find a sync check for given time in the buffer (-1==too old, 0==found, 1==toonew).
    INDEX FindSyncCheck(TIME tmTick, CSyncCheck &sc);


    // Make allaction messages for one tick.
    void MakeAllActions(void);

    // Add a block to streams for all sessions.
    void AddBlockToAllSessions(CNetworkStreamBlock &nsb);

    // Find a mask of all players on a certain client.
    ULONG MaskOfPlayersOnClient(INDEX iClient);

  public:
    // Constructor.
    CServer(void);

    // Destructor.
    ~CServer();

    // Start server.
    void Start_t(void);

    // Stop server. 
    void Stop(void);

    // Run server loop.
    void ServerLoop(void);

    // Make synchronization test message and add it to game stream.
    void MakeSynchronisationCheck(void);

    // Handle incoming network messages.
    void HandleAll();
    void HandleAllForAClient(INDEX iClient);
    void HandleClientDisconected(INDEX iClient);
    void Handle(INDEX iClient, CNetworkMessage &nm);
};

#endif  /* include-once check. */
