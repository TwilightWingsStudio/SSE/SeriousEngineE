/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Math/Functions.h>

#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/SessionSocket.h>

extern INDEX cli_iBufferActions;
extern INDEX cli_iMaxBPS;
extern INDEX cli_iMinBPS;

//! Constructor.
CSessionSocket::CSessionSocket(void)
{
  sso_eState = STATE_IDLE;
  sso_ulFlags = 0;
  sso_iDisconnectedState = 0;
  sso_iLastSentSequence  = -1;
  sso_ctBadSyncs = 0;
  sso_tvLastMessageSent.Clear();
  sso_tvLastPingSent.Clear();
}

CSessionSocket::~CSessionSocket(void)
{
  sso_eState = STATE_IDLE;
  sso_ulFlags = 0;
  sso_iLastSentSequence  = -1;
  sso_ctBadSyncs = 0;
  sso_tvLastMessageSent.Clear();
  sso_tvLastPingSent.Clear();
}

void CSessionSocket::Clear(void)
{
  sso_eState = STATE_IDLE;
  sso_ulFlags = 0;
  sso_tvMessageReceived.Clear();
  sso_tmLastSyncReceived = -1.0f;
  sso_iLastSentSequence  = -1;
  sso_tvLastMessageSent.Clear();
  sso_tvLastPingSent.Clear();
  sso_nsBuffer.Clear();
  sso_iDisconnectedState = 0;
  sso_ctBadSyncs = 0;
  sso_sspParams.Clear();
}

void CSessionSocket::Activate(void)
{
#if DEBUG_SYNCSTREAMDUMPING
  ClearDumpStream();
#endif

  ASSERT(!IsActive());

  sso_eState = STATE_CONNECTING;
  sso_ulFlags = 0;
  sso_tvMessageReceived.Clear();
  sso_tmLastSyncReceived = -1.0f;
  sso_iLastSentSequence  = -1;
  sso_tvLastMessageSent.Clear();
  sso_tvLastPingSent.Clear();
  sso_iDisconnectedState = 0;
  sso_ctBadSyncs = 0;
  sso_sspParams.Clear();
//  sso_nsBuffer.Clear();
}

void CSessionSocket::Deactivate()
{
  sso_iDisconnectedState = 0;
  sso_iLastSentSequence  = -1;
  sso_tvLastMessageSent.Clear();
  sso_tvLastPingSent.Clear();
  sso_ctBadSyncs = 0;
  sso_nsBuffer.Clear();
  sso_sspParams.Clear();
  sso_ulFlags = 0;
  sso_eState = STATE_IDLE;
}

BOOL CSessionSocket::IsActive() const
{
  return sso_eState != STATE_IDLE;
}

BOOL CSessionSocket::IsVIP() const
{
  return sso_ulFlags & SSOF_VIP;
}

BOOL CSessionSocket::IsMuted() const
{
  return sso_ulFlags & SSOF_MUTED;
}

void CSessionSocket::SetMuted(BOOL bNewState)
{
  if (bNewState) {
    sso_ulFlags |= SSOF_MUTED;
    return;
  }
  
  sso_ulFlags &= ~SSOF_MUTED;
}

BOOL CSessionSocket::IsReadyForStream() const
{
  return sso_eState == STATE_CONNECTED;
}

INDEX CSessionSocket::GetPlayerCount() const
{
  return sso_ctLocalPlayers;
}

CSessionSocketParams::CSessionSocketParams(void)
{
  Clear();
}

void CSessionSocketParams::Clear(void)
{
  ssp_iBufferActions = 2;
  ssp_iMaxBPS = 4000;
  ssp_iMinBPS = 1000;
}

static void ClampParams(void)
{
  cli_iBufferActions = Clamp(cli_iBufferActions, INDEX(1), INDEX(20));
  cli_iMaxBPS = Clamp(cli_iMaxBPS, INDEX(100), INDEX(1000000));
  cli_iMinBPS = Clamp(cli_iMinBPS, INDEX(100), INDEX(1000000));
}

// check if up to date with current params
BOOL CSessionSocketParams::IsUpToDate(void)
{
  ClampParams();
  return 
    ssp_iBufferActions == cli_iBufferActions &&
    ssp_iMaxBPS == cli_iMaxBPS &&
    ssp_iMinBPS == cli_iMinBPS;
}

// update
void CSessionSocketParams::Update(void)
{
  ClampParams();
  ssp_iBufferActions = cli_iBufferActions;
  ssp_iMaxBPS = cli_iMaxBPS;
  ssp_iMinBPS = cli_iMinBPS;
}
