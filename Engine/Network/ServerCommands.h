/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SERVERCOMMANDS_H
#define SE_INCL_SERVERCOMMANDS_H

class ENGINE_API CServerCommands
{
  public:
    static void Initialize();
  
    static void AddIPMask(void* pArgs);
    static void RemIPMask(void* pArgs);
    static void AddNameMask(void* pArgs);
    static void RemNameMask(void* pArgs);
    static void ListClients();
    static void ListPlayers();
    static void KickClient(ULONG iClient, const CTString &strReason);
    static void KickByName(const CTString &strName, const CTString &strReason);
};

#endif  /* include-once check. */