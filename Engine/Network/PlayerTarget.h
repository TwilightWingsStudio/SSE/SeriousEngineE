/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PLAYERTARGET_H
#define SE_INCL_PLAYERTARGET_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Threading/Synchronization.h>
#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/ActionBuffer.h>

//! Player target, located in each session state; receiving actions
class CPlayerTarget
{
  public:
    BOOL plt_bActive;                     // set if this player exists
    CEntity *plt_penEntity;         // entity used by this player
    CSyncMutex plt_csAction;       // access to player action
    CPlayerAction plt_paPreLastAction;
    CPlayerAction plt_paLastAction;       // last action received (used for delta-unpacking)
    CActionBuffer plt_abPrediction;       // buffer of sent actions (used for prediction)
    FLOAT3D plt_vPredictorPos;            // last position of predictor - for range calculations

  public:
    //! Default constructor.
    CPlayerTarget(void);

    //! Destructor.
    ~CPlayerTarget(void);

    //! Activate player target for a new player.
    void Activate(void);

    //! Deactivate player target for removed player.
    void Deactivate(void);

    //! Attach an entity to this player.
    void AttachEntity(CEntity *penClientEntity);

    //! Apply action packet to current actions.
    void ApplyActionPacket(const CPlayerAction &paDelta);

    //! Remember prediction action.
    void PrebufferActionPacket(const CPlayerAction &paPrediction);

    //! Flush prediction actions that were already processed.
    void FlushProcessedPredictions(void);

    //! Apply predicted action with given index.
    void ApplyPredictedAction(INDEX iAction, FLOAT fFactor);

    //! Read player information from a stream.
    void Read_t(CTStream *pstr);   // throw char *

    //! Write player information into a stream.
    void Write_t(CTStream *pstr);  // throw char *

    //! Inform entity that it was disconnected. Wrapper to prevent using GetEntity & type conversion.
    void EmitDisconnect();
    
    //! Inform entity about new action.
    void EmitApplyAction(const CPlayerAction & pa, FLOAT tmLatency);
    
    //! Inform entity that character was changed.
    void EmitCharacterChanged(const CPlayerCharacter &pcNew);
    
    //! Set character at entity.
    void SetCharacter(const CPlayerCharacter &pcNew);
    
    //! Set ping at entity.
    void SetPing(FLOAT tmPing);

  // GETTERS
  public:
    //! Returns player info for gameagent.
    void GetGameAgentPlayerInfo(INDEX iPlayer, CTString& strOut);

    //! Returns player info for legacy protocol.
    void GetMSLegacyPlayerInf(INDEX iPlayer, CTString& strOut);

    //! Get maximum number of actions that can be predicted.
    INDEX GetNumberOfPredictions(void);

    //! Returns player character.
    const CPlayerCharacter &GetCharacter(void) const;

    //! Returns player name for printing.
    CTString GetPlayerName(void);

    //! Check if this player is active.
    BOOL IsActive(void) const
    {
      return plt_bActive;
    };
    
    //! Returns pointer to entity.
    CEntity* GetEntity()
    {
      return plt_penEntity;
    }

};

#endif  /* include-once check. */
