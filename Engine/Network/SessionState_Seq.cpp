/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ErrorTable.h>
#include <Core/Base/Translation.h>
#include <Core/Base/CRCTable.h>
#include <Core/Base/Shell.h>
#include <Core/Math/Float.h>
#include <Core/IO/Compression.h>
#include <Core/Base/Console.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Base/ListIterator.inl>
#include <Core/Base/CRC.h>

#include <Engine/Build.h>
#include <Engine/Network/Network.h>
#include <Engine/Network/Server.h>
#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/Diff.h>
#include <Engine/Network/SessionState.h>
#include <Engine/Network/PlayerSource.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/NetworkProfile.h>
#include <Engine/World/PhysicsProfile.h>
#include <Engine/Network/CommunicationInterface.h>
#include <Engine/Entities/InternalClasses.h>
#include <Engine/Entities/EntityProperties.h>
#include <Engine/Network/LevelChange.h>

#include <Engine/Sound/SoundObject.h>

extern INDEX net_bReportSeqErrors;

void CSessionState::ProcessBad(CStateMessage *pStateMessage)
{
  ASSERTALWAYS(FALSE);
}

void CSessionState::ProcessPause(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_PAUSE);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence
  
  CPauseMessage *pCasted = static_cast<CPauseMessage *>(pStateMessage);

  _pNetwork->ga_World.DeletePredictors();

  BOOL bPauseBefore = ses_bPause;
  ses_bPause = pCasted->sm_bNewState;

  // if paused by some other machine
  if (pCasted->sm_strPauser != TRANS("Local machine")) {
    // report who paused
    if (ses_bPause != bPauseBefore) {
      if (ses_bPause) {
        CInfoF(TRANS("Paused by '%s'\n"), pCasted->sm_strPauser);

      } else {
        CInfoF(TRANS("Unpaused by '%s'\n"), pCasted->sm_strPauser);
      }
    }
  }

  // must keep wanting current state
  ses_bWantPause = ses_bPause;
}

void CSessionState::ProcessAllActions(CNetworkMessage &nmMessage, CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ALLACTIONS);

  // read time from packet
  TIME tmPacket;
  nmMessage >> tmPacket;    // packet time

  // time must be greater by one than that on the last packet received
  TIME tmTickQuantum = _pTimer->TickQuantum;
  TIME tmPacketDelta = tmPacket-ses_tmLastProcessedTick;

  if (! (Abs(tmPacketDelta-tmTickQuantum) < (tmTickQuantum/10.0f)) ) {
    // report debug info
    CDebugF(
      TRANS("Session state: Mistimed MSG_ALLACTIONS: Last received tick %g, this tick %g\n"),
      ses_tmLastProcessedTick, tmPacket);
  }

  // remember the received tick
  ses_tmLastProcessedTick = tmPacket;

  // NOTE: if we got a tick, it means that all players have joined
  // don't wait for new players any more
  ses_bWaitAllPlayers = FALSE;

  // delete all predictors
  _pNetwork->ga_World.DeletePredictors();
  // process the tick
  ProcessGameTick(nmMessage, tmPacket);
}

void CSessionState::ProcessPlayerAction(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_PLAYER_ACTION);
  
  CActionPlayerMessage *pCasted = static_cast<CActionPlayerMessage *>(pStateMessage);
  
  CPlayerTarget &plt = ses_apltPlayers[pCasted->GetPlayerId()];
  plt.ApplyActionPacket(pCasted->sm_paAction);
}

void CSessionState::ProcessPlayerAdd(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_PLAYER_ADD);
  
  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence
  
  CAddPlayerMessage *pCasted = static_cast<CAddPlayerMessage *>(pStateMessage);
  CPlayerTarget &plt = ses_apltPlayers[pCasted->GetPlayerId()];

  // delete all predictors
  _pNetwork->ga_World.DeletePredictors();

  // activate the player
  plt.Activate();

  // if there is no entity with that character in the world
  CEntity *penNewPlayer = _pNetwork->ga_World.FindEntityWithCharacter(pCasted->sm_pcCharacter);

  if (penNewPlayer == NULL) {
    // create an entity for it
    CPlacement3D pl(FLOAT3D(0.0f, 0.0f, 0.0f), ANGLE3D(0, 0, 0));

    try {
      CTFileName fnmPlayer = CTString("Classes\\PlayerController.ecl"); // this must not be a dependency!
      penNewPlayer = _pNetwork->ga_World.CreateEntity_t(pl, fnmPlayer);
    } catch (char *strError) {
      (void)strError;
      FatalError(TRANS("Cannot load Player class:\n%s"), strError);
    }

    plt.AttachEntity(penNewPlayer); // attach entity to client data
    plt.SetCharacter(pCasted->sm_pcCharacter); // attach the character to it
    penNewPlayer->Initialize(); // prepare the entity

    if (!_pNetwork->IsPlayerLocal(penNewPlayer)) {
      CInfoF(TRANS("%s joined\n"), plt.GetPlayerName());
    }

  } else {
    plt.AttachEntity(penNewPlayer); // attach entity to client data
    plt.EmitCharacterChanged(pCasted->sm_pcCharacter); // make it update its character

    if (!_pNetwork->IsPlayerLocal(penNewPlayer)) {
      CInfoF(TRANS("%s rejoined\n"), plt.GetPlayerName());
    }
  }
}

void CSessionState::ProcessPlayerRemove(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_PLAYER_REMOVE);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence
  
  CRemovePlayerMessage *pCasted = static_cast<CRemovePlayerMessage *>(pStateMessage);
  CPlayerTarget &plt = ses_apltPlayers[pCasted->GetPlayerId()];

  // delete all predictors
  _pNetwork->ga_World.DeletePredictors();

  CInfoF(TRANS("%s left\n"), plt.GetPlayerName());
  plt.EmitDisconnect(); // inform entity of disconnnection
  plt.Deactivate(); // deactivate the player

  // handle all the sent events
  ses_bAllowRandom = TRUE;
  CEntity::HandleSentEvents();
  ses_bAllowRandom = FALSE;
}

void CSessionState::ProcessPlayerCharacterChange(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_PLAYER_CHARACTERCHANGE);
  
  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence
  
  CCharacterChangePlayerMessage *pCasted = static_cast<CCharacterChangePlayerMessage *>(pStateMessage);
  CPlayerTarget &plt = ses_apltPlayers[pCasted->GetPlayerId()];

  // delete all predictors
  _pNetwork->ga_World.DeletePredictors();
  plt.EmitCharacterChanged(pCasted->sm_pcCharacter); // change the character

  // handle all the sent events
  ses_bAllowRandom = TRUE;
  CEntity::HandleSentEvents();
  ses_bAllowRandom = FALSE;
}

void CSessionState::ProcessEntityCreate(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_CREATE);
  
  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence

  CCreateEntityMessage *pCasted = static_cast<CCreateEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());
  
  if (pen != NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for existing entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return; // Do nothing if already exists.
  }

  // TODO: Add logic.
  // TODO: Find class by its Id.
}

void CSessionState::ProcessEntityCopy(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_COPY);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence

  CCopyEntityMessage *pCasted = static_cast<CCopyEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());
  
  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  // TODO: Add logic.
}

void CSessionState::ProcessEntityTeleport(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_TELEPORT);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence

  CTeleportEntityMessage *pCasted = static_cast<CTeleportEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  // TODO: Add logic.
}

void CSessionState::ProcessEntitySetParent(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SETPARENT);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence

  CSetParentEntityMessage *pCasted = static_cast<CSetParentEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CEntity *penTarget = pCasted->GetEntityId() == 0xFFFFFFFF ? NULL : _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());
  
  pen->SetParent(penTarget);
  
  ses_bAllowRandom = TRUE;
  CEntity::HandleSentEvents();
  ses_bAllowRandom = FALSE;
}

void CSessionState::ProcessEntityDestroy(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_DESTROY);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence

  CDestroyEntityMessage *pCasted = static_cast<CDestroyEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  pen->Destroy(); // Mark as removed.

  ses_bAllowRandom = TRUE;
  CEntity::HandleSentEvents();
  ses_bAllowRandom = FALSE;
}

void CSessionState::ProcessEntityInitialize(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_INITIALIZE);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence

  CInitializeEntityMessage *pCasted = static_cast<CInitializeEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  // TODO: Add logic.
}

void CSessionState::ProcessEntityEvent(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_EVENT);

  _pNetwork->AddNetGraphValue(NGET_NONACTION, 1.0f); // non-action sequence
  
  CEventEntityMessage *pCasted = static_cast<CEventEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());
  
  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  // TODO: Improve with finding event over all loaded classes.
  CLibEntityEvent *pLibEvent = pen->en_pecClass->ec_pdecDLLClass->EventForId(pCasted->GetEventCode());
  
  if (pLibEvent == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid event, EventCode: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEventCode());
    }

    return;
  }

  // delete all predictors
  _pNetwork->ga_World.DeletePredictors();

  // Make instance of event.
  CEntityEvent *pEvent = pLibEvent->dee_New();
  memcpy((UBYTE*)pEvent + sizeof(CEntityEvent), pCasted->sm_pEventData, pCasted->sm_ulEventDataSize);

  ses_bAllowRandom = TRUE;
  pen->SendEvent(*pEvent);
  CEntity::HandleSentEvents();
  ses_bAllowRandom = FALSE;
}

void CSessionState::ProcessEntitySoundPlay(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SOUND_PLAY);

  CSoundEntityMessage *pCasted = static_cast<CSoundEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CSoundObject* pSoundObject = reinterpret_cast<CSoundObject* >(((UBYTE*)pen) + pCasted->GetComponentOffset());

  // TODO: Add logic.
}

void CSessionState::ProcessEntitySoundStop(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SOUND_STOP);

  CSoundEntityMessage *pCasted = static_cast<CSoundEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CSoundObject* pSoundObject = reinterpret_cast<CSoundObject* >(((UBYTE*)pen) + pCasted->GetComponentOffset());

  pSoundObject->Stop();
}

void CSessionState::ProcessEntitySoundVolume(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SOUND_VOLUME);

  CSoundEntityMessage *pCasted = static_cast<CSoundEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CSoundObject* pSoundObject = reinterpret_cast<CSoundObject* >(((UBYTE*)pen) + pCasted->GetComponentOffset());

  // TODO: Add logic.
  //pSoundObject->SetVolume(fVolume);
}

void CSessionState::ProcessEntitySoundPitch(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SOUND_PITCH);

  CSoundEntityMessage *pCasted = static_cast<CSoundEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CSoundObject* pSoundObject = reinterpret_cast<CSoundObject* >(((UBYTE*)pen) + pCasted->GetComponentOffset());
  FLOAT fPitch = pCasted->GetPitch() / 63.0f;
  pSoundObject->SetPitch(fPitch);
}

void CSessionState::ProcessEntitySoundRange(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SOUND_RANGE);

  CSoundEntityMessage *pCasted = static_cast<CSoundEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CSoundObject* pSoundObject = reinterpret_cast<CSoundObject* >(((UBYTE*)pen) + pCasted->GetComponentOffset());
  
  // TODO: Add logic.
  //pSoundObject->SetRange(pCasted->GetHotSpot(), pCasted->GetFalloff());
}

void CSessionState::ProcessEntitySoundFade(CStateMessage *pStateMessage)
{
  ASSERT(pStateMessage->GetType() == MSG_SEQ_ENTITY_SOUND_FADE);

  CSoundEntityMessage *pCasted = static_cast<CSoundEntityMessage *>(pStateMessage);
  CEntity *pen = _pNetwork->ga_World.EntityFromID(pCasted->GetEntityId());

  if (pen == NULL) {
    if (net_bReportSeqErrors) {
      CWarningF("Warning: %s received for invalid entity, EntityID: 0x%X\n", CStateMessage::GetName(pStateMessage->GetType()), pCasted->GetEntityId());
    }

    return;
  }

  CSoundObject* pSoundObject = reinterpret_cast<CSoundObject* >(((UBYTE*)pen) + pCasted->GetComponentOffset());

  // TODO: Add logic.
  //pSoundObject->Fade(fVolume, tmPeriod);
}