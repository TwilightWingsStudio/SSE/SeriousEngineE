/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_NETWORKMESSAGE_H
#define SE_INCL_NETWORKMESSAGE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Core/Math/Vector.h>
#include <Engine/Network/NetworkProtocol.h>

extern struct ErrorTable MessageTypes;

//! Holder for network message, can be read/written like a stream.
class ENGINE_API CNetworkMessage
{
  public:
    UBYTE nm_ubType;                  // type of this message

  #define MAX_NETWORKMESSAGE_SIZE 2048      // max. length of message buffer
    UBYTE *nm_pubMessage;       // the message data itself
    SLONG nm_slMaxSize;         // size of message buffer

    UBYTE *nm_pubPointer;       // pointer for reading/writing message
    SLONG nm_slSize;            // size of message
    INDEX nm_iBit;              // next bit index to read/write (0 if not reading/writing bits)

  public:
    //! Constructor for empty message (for receiving).
    CNetworkMessage(void);

    //! Constructor for initializing message that is to be sent.
    CNetworkMessage(UBYTE ubType);

    //! Copy constructor.
    CNetworkMessage(const CNetworkMessage &nmOriginal);

    //! Copy operator.
    void operator=(const CNetworkMessage &nmOriginal);

    //! Destructor.
    ~CNetworkMessage(void);

    //! Reinit a message that is to be sent (to write different contents).
    void Reinit(void);

    //! Ignore the contents of this message.
    void IgnoreContents(void);

    //! Dump message to console.
    void Dump(void);

    //! Get the type of this message.
    inline UBYTE GetType(void) const
    {
      ASSERT(this != NULL);
      return nm_ubType & 0x3F; // 6 bits
    };

    //! Check if end of message.
    BOOL IsAtEnd(void);

    //! Rewind message to start, so that written message can be read again.
    void Rewind(void);

    /* Pack a message to another message (message type is left untouched). */
    void Pack(CNetworkMessage &nmPacked, enum CompressionMethod eCompressionMethod);
    void PackDefault(CNetworkMessage &nmPacked);

    /* Unpack a message to another message (message type is left untouched). */
    void Unpack(CNetworkMessage &nmUnpacked, enum CompressionMethod eCompressionMethod);
    void UnpackDefault(CNetworkMessage &nmUnpacked);

    // read/write functions
    void Read(void *pvBuffer, SLONG slSize);
    void Write(const void *pvBuffer, SLONG slSize);
    void ReadBits(void *pvBuffer, INDEX ctBits);
    void WriteBits(const void *pvBuffer, INDEX ctBits);

    /* Read an object from message. */
    inline CNetworkMessage &operator>>(float  &f) { Read( &f, sizeof( f)); return *this; }
    inline CNetworkMessage &operator>>(ULONG &ul) { Read(&ul, sizeof(ul)); return *this; }
    inline CNetworkMessage &operator>>(UWORD &uw) { Read(&uw, sizeof(uw)); return *this; }
    inline CNetworkMessage &operator>>(UBYTE &ub) { Read(&ub, sizeof(ub)); return *this; }
    inline CNetworkMessage &operator>>(SLONG &sl) { Read(&sl, sizeof(sl)); return *this; }
    inline CNetworkMessage &operator>>(SWORD &sw) { Read(&sw, sizeof(sw)); return *this; }
    inline CNetworkMessage &operator>>(SBYTE &sb) { Read(&sb, sizeof(sb)); return *this; }
    CNetworkMessage &operator>>(CTString &str);

    /* Write an object into message. */
    inline CNetworkMessage &operator<<(const float  &f) { Write( &f, sizeof( f)); return *this; }
    inline CNetworkMessage &operator<<(const double &d) { Write( &d, sizeof( d)); return *this; }
    inline CNetworkMessage &operator<<(const ULONG &ul) { Write(&ul, sizeof(ul)); return *this; }
    inline CNetworkMessage &operator<<(const UWORD &uw) { Write(&uw, sizeof(uw)); return *this; }
    inline CNetworkMessage &operator<<(const UBYTE &ub) { Write(&ub, sizeof(ub)); return *this; }
    inline CNetworkMessage &operator<<(const SLONG &sl) { Write(&sl, sizeof(sl)); return *this; }
    inline CNetworkMessage &operator<<(const SWORD &sw) { Write(&sw, sizeof(sw)); return *this; }
    inline CNetworkMessage &operator<<(const SBYTE &sb) { Write(&sb, sizeof(sb)); return *this; }
    CNetworkMessage &operator<<(const CTString &str);

    //! Insert a sub-message into this message.
    void InsertSubMessage(const CNetworkMessage &nmSubMessage);

    //! Extract a sub-message from this message.
    void ExtractSubMessage(CNetworkMessage &nmSubMessage);

    //! Shrink message buffer to exactly fit contents.
    void Shrink(void);
};

#include <Engine/Network/NetworkStreamBlock.h>
#include <Engine/Network/NetworkStream.h>
#include <Engine/Network/PlayerAction.h>

#endif  /* include-once check. */
