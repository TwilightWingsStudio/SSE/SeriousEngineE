/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATEMESSAGE_PLAYER_H
#define SE_INCL_STATEMESSAGE_PLAYER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Abstract (de)serializer class for any message that affects player state.
class ENGINE_API CPlayerStateMessage : public CStateMessage
{
  public:
    INDEX sm_iPlayer;
    
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_BAD;
    }
    
    inline INDEX GetPlayerId() const
    {
      return sm_iPlayer;
    }      

    //! Constructor.
    CPlayerStateMessage();
    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CActionPlayerMessage : public CPlayerStateMessage
{
  public:
    CPlayerAction sm_paAction;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_PLAYER_ACTION;
    }

    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CAddPlayerMessage : public CPlayerStateMessage
{
  public:
    CPlayerCharacter sm_pcCharacter;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_PLAYER_ADD;
    }

    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CRemovePlayerMessage : public CPlayerStateMessage
{
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_PLAYER_REMOVE;
    }

    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

//! TODO: Add comment.
class ENGINE_API CCharacterChangePlayerMessage : public CPlayerStateMessage
{
  public:
    CPlayerCharacter sm_pcCharacter;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_PLAYER_CHARACTERCHANGE;
    }

    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

#endif