/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATEMESSAGE_H
#define SE_INCL_STATEMESSAGE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticArray.h>

#include <Engine/Entities/PlayerCharacter.h>

#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/NetworkProtocol.h>
#include <Engine/Network/PlayerAction.h>

//! Base class for representing state messages.
class ENGINE_API CStateMessage
{
  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_BAD;
    }

    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
    virtual void Dump();

    static const char *GetName(UBYTE ubType);
};

//! Factory class for creating state messages from a certain type.
class ENGINE_API CStateMessageFactory
{
  public:
    static CStateMessage *Create(enum MsgTypeSeq eMessageType);
};

#endif