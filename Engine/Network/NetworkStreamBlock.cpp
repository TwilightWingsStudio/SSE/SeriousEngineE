/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Network/NetworkMessage.h>

#include <Core/IO/Stream.h>

/*
 * Constructor for receiving -- uninitialized block.
 */
CNetworkStreamBlock::CNetworkStreamBlock(void)
  : CNetworkMessage()
  , nsb_iSequenceNumber(-1)
{
}

/*
 * Constructor for sending -- empty packet with given type and sequence.
 */
CNetworkStreamBlock::CNetworkStreamBlock(MsgTypeSeq mtType, INDEX iSequenceNumber)
  : CNetworkMessage(mtType)
  , nsb_iSequenceNumber(iSequenceNumber)
{
}

/*
 * Read a block from a received message.
 */
void CNetworkStreamBlock::ReadFromMessage(CNetworkMessage &nmToRead)
{
  // read sequence number from message
  nmToRead>>nsb_iSequenceNumber;
  ASSERT(nsb_iSequenceNumber >= 0);

  // read the block as a submessage
  nmToRead.ExtractSubMessage(*this);
}

/*
 * Add a block to a message to send.
 */
void CNetworkStreamBlock::WriteToMessage(CNetworkMessage &nmToWrite)
{
  // write sequence number to message
  ASSERT(nsb_iSequenceNumber >= 0);
  nmToWrite<<nsb_iSequenceNumber;

  // write the block as a submessage
  nmToWrite.InsertSubMessage(*this);
}

/*
 * Remove the block from stream. */
void CNetworkStreamBlock::RemoveFromStream(void)
{
  nsb_lnInStream.Remove();
}

INDEX CNetworkStreamBlock::GetSequenceNumber() const
{
  return nsb_iSequenceNumber;
}

/* Read/write the block from file stream. */
void CNetworkStreamBlock::Write_t(CTStream &strm)
{
  strm << nsb_iSequenceNumber; // write sequence number
  strm << nm_slSize; // write block size
  strm.Write_t(nm_pubMessage, nm_slSize); // write block contents
}

void CNetworkStreamBlock::Read_t(CTStream &strm)
{
  // read sequence number
  strm >> nsb_iSequenceNumber;

  // read block size
  strm >> nm_slSize;

  // read block contents
  strm.Read_t(nm_pubMessage, nm_slSize);

  // init the message read/write pointer
  nm_pubPointer = nm_pubMessage;
  nm_iBit = 0;

  // get the message type
  UBYTE ubType = 0;
  (*this) >> ubType;
  nm_ubType = ubType;
}
