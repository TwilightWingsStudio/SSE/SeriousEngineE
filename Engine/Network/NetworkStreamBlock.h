/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_NETWORKSTREAMBLOCK_H
#define SE_INCL_NETWORKSTREAMBLOCK_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

/*
 * A message block used for streaming data across network.
 *
 * These can be received duplicated or misordered. They
 * are resequenced at the receive side as needed. Can be sent more than one
 * together as submessages in a message and duplicated across messages as a
 * compensation for eventual packet loss.
 */
class ENGINE_API CNetworkStreamBlock : public CNetworkMessage
{
  public:
    CListNode nsb_lnInStream;     // node in list of blocks in stream
    INDEX nsb_iSequenceNumber;    // index for sorting in list

  public:
    //! Constructor for receiving -- uninitialized block.
    CNetworkStreamBlock(void);

    //! Constructor for sending -- empty packet with given type and sequence.
    CNetworkStreamBlock(MsgTypeSeq mtType, INDEX iSequenceNumber);

    //! Read a block from a received message.
    void ReadFromMessage(CNetworkMessage &nmToRead);

    //! Add a block to a message to send.
    void WriteToMessage(CNetworkMessage &nmToWrite);

    //! Remove the block from stream.
    void RemoveFromStream(void);
    
    //! Get sequence number of this stream block.
    INDEX GetSequenceNumber() const;

    //! Read from stream.
    void Read_t(CTStream &strm); // throw char *
    
    //! Write to stream.
    void Write_t(CTStream &strm); // throw char *
};

#endif /* include-once check. */