/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATEMESSAGE_COMMON_H
#define SE_INCL_STATEMESSAGE_COMMON_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Toggle pause for simulation/state.
class ENGINE_API CPauseMessage : public CStateMessage
{
  public:
    BOOL sm_bNewState;
    CTString sm_strPauser;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_PAUSE;
    }

    CPauseMessage();
    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

//! All player actions.
class ENGINE_API CAllActionsMessage : public CStateMessage
{
  public:
    TIME sm_tmPacket;
    //CPlayerAction sm_aActions[NET_MAXGAMEPLAYERS];
    TStaticArray<CPlayerAction> sm_aActions;

  public:
    virtual enum MsgTypeSeq GetType() const
    {
      return MSG_SEQ_ALLACTIONS;
    }

    CAllActionsMessage();
    virtual void Read_t(CNetworkMessage &nmMessage);
    virtual void Write_t(CNetworkMessage &nmMessage);
};

#endif