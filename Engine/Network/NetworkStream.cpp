/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Network/NetworkMessage.h>

#include <Core/Base/ListIterator.inl>

/*
 * Constructor.
 */
CNetworkStream::CNetworkStream(void)
{
}

/*
 * Destructor.
 */
CNetworkStream::~CNetworkStream(void)
{
  // report number of blocks left in stream
  //_RPT1(_CRT_WARN, "Destructing stream, %d blocks contained.\n", ns_lhBlocks.Count());
  // remove all blocks
  Clear();
}

/*
 * Clear the object (remove all blocks).
 */
void CNetworkStream::Clear(void)
{
  // for each block in list
  FORDELETELIST(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsbInList)
  {
    // remove it from list
    itnsbInList->nsb_lnInStream.Remove();
    // delete it
    delete &*itnsbInList;
  }
}

/* Copy from another network stream. */
void CNetworkStream::Copy(CNetworkStream &nsOther)
{
  // for each block in list
  FOREACHINLIST(CNetworkStreamBlock, nsb_lnInStream, nsOther.ns_lhBlocks, itnsb)
  {
    // add it here
    AddBlock(*itnsb);
  }
}

// get number of blocks used by this object
INDEX CNetworkStream::GetUsedBlocks(void)
{
  return ns_lhBlocks.Count();
}

// get amount of memory used by this object
SLONG CNetworkStream::GetUsedMemory(void)
{
  SLONG slMem = 0;

  // for each block in list
  FOREACHINLIST(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsb)
  {
    // add its usage
    slMem += sizeof(CNetworkStreamBlock)+itnsb->nm_slMaxSize;
  }

  return slMem;
}

// get index of newest sequence stored
INDEX CNetworkStream::GetNewestSequence(void)
{
  // if the stream is empty
  if (ns_lhBlocks.IsEmpty()) {
    // return dummy
    return -1;
  }

  // get head of list
  CNetworkStreamBlock *pnsb = LIST_HEAD(ns_lhBlocks, CNetworkStreamBlock, nsb_lnInStream);

  // return its index
  return pnsb->nsb_iSequenceNumber;
}

/*
 * Add a block that is already allocated to the stream.
 */
void CNetworkStream::AddAllocatedBlock(CNetworkStreamBlock *pnsbBlock)
{
  // search all blocks already in list
  FOREACHINLISTKEEP(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsbInList)
  {
    // if the block in list has same sequence as the one to add
    if (itnsbInList->GetSequenceNumber() == pnsbBlock->GetSequenceNumber()) {
      // just discard the new block
      delete pnsbBlock;
      return;
    }

    // if the block in list has lower sequence than the one to add
    if (itnsbInList->GetSequenceNumber() < pnsbBlock->GetSequenceNumber()) {
      // stop searching
      break;
    }
  }

  // add the new block before current one
  itnsbInList.InsertBeforeCurrent(pnsbBlock->nsb_lnInStream);
}

/*
 * Add a block to the stream.
 */
void CNetworkStream::AddBlock(CNetworkStreamBlock &nsbBlock)
{
  CNetworkStreamBlock *pnsbCopy = new CNetworkStreamBlock(nsbBlock); // create a copy of the block
  pnsbCopy->Shrink(); // shrink it
  AddAllocatedBlock(pnsbCopy); // add it to the list
}

/*
 * Read a block as a submessage from a message and add it to the stream.
 */
void CNetworkStream::ReadBlock(CNetworkMessage &nmMessage)
{
  // create an empty block
  CNetworkStreamBlock *pnsbRead = new CNetworkStreamBlock();
  pnsbRead->ReadFromMessage(nmMessage); // read it from message
  pnsbRead->Shrink(); // shrink it
  AddAllocatedBlock(pnsbRead); // add it to the list
}

/*
 * Get a block from stream by its sequence number.
 */
CNetworkStream::Result CNetworkStream::GetBlockBySequence(
  INDEX iSequenceNumber, CNetworkStreamBlock *&pnsbBlock)
{
  BOOL bNewerFound = FALSE;

  // search all blocks in list
  FOREACHINLIST(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsbInList)
  {
    CNetworkStreamBlock &nsbInList = *itnsbInList;

    // if the block in list has newer sequence
    if (nsbInList.GetSequenceNumber() >= iSequenceNumber) {
      // remember that at least one newer block was found
      bNewerFound = TRUE;
    }

    // if the block in list has wanted sequence
    if (nsbInList.GetSequenceNumber() == iSequenceNumber) {
      // return it
      pnsbBlock = itnsbInList;
      return R_OK;
    }

    // if the block in list has older sequence
    if (nsbInList.GetSequenceNumber() < iSequenceNumber) {
      // stop searching
      break;
    }
  }

  // ...if none found

  // if some block of newer sequence number was found
  if (bNewerFound) {
    // return that the block is missing (probably should be resent)
    pnsbBlock = NULL;
    return R_BLOCKMISSING;

  // if no newer blocks were found
  } else {
    // we assume that the wanted block is not yet received
    pnsbBlock = NULL;
    return R_BLOCKNOTRECEIVEDYET;
  }
}

// find oldest block after given one (for batching missing sequences)
INDEX CNetworkStream::GetOldestSequenceAfter(INDEX iSequenceNumber)
{
  // block are sorted newer first, so we just remember the last block found
  // until we find the given one
  INDEX iOldest = iSequenceNumber;

  FOREACHINLIST(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsb)
  {
    CNetworkStreamBlock &nsb = *itnsb;

    if (nsb.GetSequenceNumber() < iSequenceNumber) {
      break;

    } else {
      iOldest = nsb.GetSequenceNumber();
    }
  }

  return iOldest;
}

/*
 * Write given number of newest blocks to a message.
 */
INDEX CNetworkStream::WriteBlocksToMessage(CNetworkMessage &nmMessage, INDEX ctBlocks)
{
  // for given number of newest blocks in list
  INDEX iBlock = 0;

  FOREACHINLIST(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsbInList)
  {
    // write the block to message
    itnsbInList->WriteToMessage(nmMessage);
    iBlock++;

    if (iBlock >= ctBlocks) {
      return iBlock;
    }
  }

  return iBlock;
}

/*
 * Remove all blocks but the given number of newest ones.
 */
void CNetworkStream::RemoveOlderBlocks(INDEX ctBlocksToKeep)
{
  // for each block in list
  INDEX iBlock = 0;

  FORDELETELIST(CNetworkStreamBlock, nsb_lnInStream, ns_lhBlocks, itnsbInList)
  {
    iBlock++;

    // if it is older that given count
    if (iBlock > ctBlocksToKeep) {
      itnsbInList->nsb_lnInStream.Remove(); // remove it from list
      delete &*itnsbInList; // delete it
    }
  }
}

/* Remove all blocks with sequence older than given. */
void CNetworkStream::RemoveOlderBlocksBySequence(INDEX iLastSequenceToKeep)
{
  // while there are any blocks in the list
  while (!ns_lhBlocks.IsEmpty())
  {
    // get the tail of the list
    CNetworkStreamBlock *pnsb = LIST_TAIL(ns_lhBlocks, CNetworkStreamBlock, nsb_lnInStream);

    // if it is not too old
    if (pnsb->GetSequenceNumber() >= iLastSequenceToKeep) {
      // stop
      break;
    }

    // remove the tail
    delete pnsb;
  };
}
