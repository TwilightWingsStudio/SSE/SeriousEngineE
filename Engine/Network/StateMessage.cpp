/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Shell.h>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Network/Network.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/SessionState.h>

CStateMessage *CStateMessageFactory::Create(enum MsgTypeSeq eMessageType)
{
  switch (eMessageType)
  {
    // Common
    case MSG_SEQ_PAUSE: return new CPauseMessage();
    case MSG_SEQ_ALLACTIONS: return new CAllActionsMessage();

    // Player
    case MSG_SEQ_PLAYER_ACTION: return new CActionPlayerMessage();
    case MSG_SEQ_PLAYER_ADD: return new CAddPlayerMessage();
    case MSG_SEQ_PLAYER_REMOVE: return new CRemovePlayerMessage();
    case MSG_SEQ_PLAYER_CHARACTERCHANGE: return new CCharacterChangePlayerMessage();

    // Entity
    case MSG_SEQ_ENTITY_CREATE: return new CCreateEntityMessage();
    case MSG_SEQ_ENTITY_COPY: return new CCopyEntityMessage();
    case MSG_SEQ_ENTITY_TELEPORT: return new CTeleportEntityMessage();
    case MSG_SEQ_ENTITY_SETPARENT: return new CSetParentEntityMessage();
    case MSG_SEQ_ENTITY_DESTROY: return new CDestroyEntityMessage();
    case MSG_SEQ_ENTITY_INITIALIZE: return new CInitializeEntityMessage();
    case MSG_SEQ_ENTITY_EVENT: return new CEventEntityMessage();

    // Entity.Sound
    case MSG_SEQ_ENTITY_SOUND_PLAY: return new CPlaySoundEntityMessage();
    case MSG_SEQ_ENTITY_SOUND_STOP: return new CStopSoundEntityMessage();
    case MSG_SEQ_ENTITY_SOUND_VOLUME: return new CVolumeSoundEntityMessage();
    case MSG_SEQ_ENTITY_SOUND_PITCH: return new CPitchSoundEntityMessage();
    case MSG_SEQ_ENTITY_SOUND_RANGE: return new CRangeSoundEntityMessage();
    case MSG_SEQ_ENTITY_SOUND_FADE: return new CFadeSoundEntityMessage();
  }
  
  return NULL;
}

void CStateMessage::Read_t(CNetworkMessage &nmMessage)
{
}

void CStateMessage::Write_t(CNetworkMessage &nmMessage)
{
}

void CStateMessage::Dump()
{
}

const char *CStateMessage::GetName(UBYTE ubType)
{
  static const char *MESSAGE_NAMES[MSG_SEQ_LAST] = {
    "MSG_SEQ_BAD",
    "MSG_SEQ_PAUSE",
    "MSG_SEQ_ALLACTIONS",
    "MSG_SEQ_PLAYER_ACTION",
    "MSG_SEQ_PLAYER_ADD",
    "MSG_SEQ_PLAYER_REMOVE",
    "MSG_SEQ_PLAYER_CHARACTERCHANGE",
    "MSG_SEQ_ENTITY_CREATE",
    "MSG_SEQ_ENTITY_COPY",
    "MSG_SEQ_ENTITY_SETPARENT",
    "MSG_SEQ_ENTITY_DESTROY",
    "MSG_SEQ_ENTITY_INITIALIZE",
    "MSG_SEQ_ENTITY_EVENT",
    "MSG_SEQ_ENTITY_SOUND_PLAY",
    "MSG_SEQ_ENTITY_SOUND_STOP",
    "MSG_SEQ_ENTITY_SOUND_VOLUME",
    "MSG_SEQ_ENTITY_SOUND_PITCH",
    "MSG_SEQ_ENTITY_SOUND_RANGE",
    "MSG_SEQ_ENTITY_SOUND_FADE",
  };

  if (ubType > MSG_SEQ_BAD && ubType < MSG_SEQ_LAST) {
    return MESSAGE_NAMES[ubType];
  }

  return "<invalid>";
}