/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Shell.h>
#include <Engine/Network/ClientCommands.h>

extern INDEX cli_bEmulateDesync  = FALSE;
extern INDEX cli_bDumpSync       = FALSE;
extern INDEX cli_bDumpSyncEachTick = FALSE;
extern INDEX cli_bAutoAdjustSettings = FALSE;
extern FLOAT cli_tmAutoAdjustThreshold = 2.0f;
extern INDEX cli_bPrediction = FALSE;
extern INDEX cli_iMaxPredictionSteps = 10;
extern INDEX cli_bPredictIfServer = FALSE;
extern INDEX cli_bPredictLocalPlayers = TRUE;
extern INDEX cli_bPredictRemotePlayers = FALSE;
extern FLOAT cli_fPredictEntitiesRange = 20.0f;
extern INDEX cli_bLerpActions = FALSE;
extern INDEX cli_bReportPredicted = FALSE;
extern INDEX cli_iSendBehind = 3;
extern INDEX cli_iPredictionFlushing = 1;
extern FLOAT cli_fPredictionFilter = 0.5f;

//
extern INDEX cli_iBufferActions = 1;
extern INDEX cli_iMaxBPS = 4000;
extern INDEX cli_iMinBPS = 0;

void CClientCommands::Initialize()
{
  _pShell->DeclareSymbol("user INDEX cli_bEmulateDesync;",  &cli_bEmulateDesync);
  _pShell->DeclareSymbol("user INDEX cli_bDumpSync;",       &cli_bDumpSync);
  _pShell->DeclareSymbol("user INDEX cli_bDumpSyncEachTick;",&cli_bDumpSyncEachTick);
  
  // Persistent settings.
  _pShell->DeclareSymbol("persistent user INDEX cli_bLerpActions;", &cli_bLerpActions);
  _pShell->DeclareSymbol("persistent user INDEX cli_bReportPredicted;", &cli_bReportPredicted);
  
  _pShell->DeclareSymbol("persistent user INDEX cli_bAutoAdjustSettings;",   &cli_bAutoAdjustSettings);
  _pShell->DeclareSymbol("persistent user FLOAT cli_tmAutoAdjustThreshold;", &cli_tmAutoAdjustThreshold);
  _pShell->DeclareSymbol("persistent user INDEX cli_bPrediction;",           &cli_bPrediction);
  _pShell->DeclareSymbol("persistent user INDEX cli_iMaxPredictionSteps;",   &cli_iMaxPredictionSteps);
  _pShell->DeclareSymbol("persistent user INDEX cli_bPredictIfServer;",      &cli_bPredictIfServer);
  _pShell->DeclareSymbol("persistent user INDEX cli_bPredictLocalPlayers;",  &cli_bPredictLocalPlayers);
  _pShell->DeclareSymbol("persistent user INDEX cli_bPredictRemotePlayers;", &cli_bPredictRemotePlayers);
  _pShell->DeclareSymbol("persistent user FLOAT cli_fPredictEntitiesRange;", &cli_fPredictEntitiesRange);
  _pShell->DeclareSymbol("persistent user FLOAT cli_fPredictionFilter;", &cli_fPredictionFilter);
  _pShell->DeclareSymbol("persistent user INDEX cli_iSendBehind;", &cli_iSendBehind);
  _pShell->DeclareSymbol("persistent user INDEX cli_iPredictionFlushing;", &cli_iPredictionFlushing);

  _pShell->DeclareSymbol("persistent user INDEX cli_iBufferActions;",  &cli_iBufferActions);
  _pShell->DeclareSymbol("persistent user INDEX cli_iMaxBPS;",     &cli_iMaxBPS);
  _pShell->DeclareSymbol("persistent user INDEX cli_iMinBPS;",     &cli_iMinBPS);
}