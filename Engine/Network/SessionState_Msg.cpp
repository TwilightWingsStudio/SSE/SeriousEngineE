/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ErrorTable.h>
#include <Core/Base/Translation.h>
#include <Core/Base/CRCTable.h>
#include <Core/Base/Shell.h>
#include <Core/Math/Float.h>
#include <Core/IO/Compression.h>
#include <Core/Base/Console.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Base/ListIterator.inl>
#include <Core/Base/CRC.h>

#include <Engine/Build.h>
#include <Engine/Network/CommunicationInterface.h>
#include <Engine/Network/Network.h>
#include <Engine/Network/Server.h>
#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/Diff.h>
#include <Engine/Network/SessionState.h>
#include <Engine/Network/NetworkProfile.h>
#include <Engine/World/PhysicsProfile.h>
#include <Engine/Entities/InternalClasses.h>

void CSessionState::OnBad(CNetworkMessage &nmMessage)
{
  ASSERTALWAYS(FALSE);
}

void CSessionState::OnNop(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_NOP);

  // just remember time
  ses_tvMessageReceived = _pTimer->GetHighPrecisionTimer();
  _pNetwork->AddNetGraphValue(NGET_NONACTION, 0.5f); // non-action sequence
}

void CSessionState::OnPrint(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_PRINT);
  
  CTString strMessage;
  nmMessage >> strMessage;
  CInfoF("%s\n", strMessage);
}

void CSessionState::OnDisconnected(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_DISCONNECTED);
  
  // confirm disconnect
  CNetworkMessage nmConfirmDisconnect(MSG_C2S_DISCONNECTEDREPLY);			
  _pNetwork->SendToServerReliable(nmConfirmDisconnect);

  // report the reason
  CTString strReason;
  nmMessage >> strReason;
  ses_strDisconnected = strReason;
  CInfoF(TRANS("Disconnected: %s\n"), strReason);

  // disconnect
  _cmiComm.Client_Close();
}

void CSessionState::OnGameStreamBlocks(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_GAMESTREAMBLOCKS);

  ses_tvMessageReceived = _pTimer->GetHighPrecisionTimer();
  ses_bWaitingForServer = FALSE;

  // unpack the message
  CNetworkMessage nmUnpackedBlocks(MSG_S2C_GAMESTREAMBLOCKS);
  nmMessage.UnpackDefault(nmUnpackedBlocks);

  // while there are some more blocks in the message
  while (!nmUnpackedBlocks.IsAtEnd())
  {
    // read a block to the gamestream
    ses_nsGameStream.ReadBlock(nmUnpackedBlocks);
  }
}

void CSessionState::OnPings(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_PINGS);
  
  for (INDEX i = 0; i < NET_MAXGAMEPLAYERS; i++)
  {
    CPlayerTarget &plt = ses_apltPlayers[i];
    BOOL bHas = 0;
    nmMessage.ReadBits(&bHas, 1);

    if (bHas) {
      if (plt.IsActive() && plt.GetEntity() != NULL) {
        INDEX iPing = 0;
        nmMessage.ReadBits(&iPing, 10);
        plt.SetPing(iPing / 1000.0f);
      }
    }
  }
}

void CSessionState::OnChat(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_CHAT);

  // read the message
  ULONG ulFrom;
  CTString strFrom;
  nmMessage >> ulFrom;

  if (ulFrom == 0) {
    nmMessage >> strFrom;
  }

  CTString strMessage;
  nmMessage >> strMessage;

  // print it
  PrintChatMessage(ulFrom, strFrom, strMessage);
}

void CSessionState::OnAdminResponse(CNetworkMessage &nmMessage)
{
  ASSERT(nmMessage.GetType() == MSG_S2C_ADMINRESPONSE);  

  // just print it
  CTString strResponse;
  nmMessage >> strResponse;
  CInfoF("%s", "|" + strResponse + "\n");
}
