/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Console.h>
#include <Core/IO/Stream.h>
#include <Core/IO/Resource.h>
#include <Core/IO/ResourceStock.h>

#include <Engine/Resources/ResourceManager.h>
#include <Engine/Resources/Stocks.h>

CResourceManager *_pResourceMgr = NULL;

CResourceStock *CResourceManager::StockForType(CResource::Type eType)
{
  switch (eType)
  {
    case CResource::TYPE_ENTITYCLASS: return _pEntityClassStock;
    case CResource::TYPE_TEXTURE: return _pTextureStock;
    case CResource::TYPE_ANIMATION: return _pAnimStock;
    case CResource::TYPE_MODEL: return _pModelStock;
    case CResource::TYPE_SOUND: return _pSoundStock;
    case CResource::TYPE_SKELMESH: return _pMeshStock;
    case CResource::TYPE_SKELETON: return _pSkeletonStock;
    case CResource::TYPE_SHADER: return _pShaderStock;
    case CResource::TYPE_BONEANIMSET: return _pAnimSetStock;
    case CResource::TYPE_MODELCONFIGURATION: return _pModelConfigStock;
    case CResource::TYPE_MODULE: return _pModuleStock;
  }
  
  return NULL;
}

CResourceManager::CResourceManager()
{
  // Common stocks
  _pAnimStock          = new CResourceStock_CAnimData;
  _pTextureStock       = new CResourceStock_CTextureData;
  _pSoundStock         = new CResourceStock_CSoundData;
  _pModelStock         = new CResourceStock_CModelData;
  _pEntityClassStock   = new CResourceStock_CEntityClass;
  
  // Skeletal Mesh
  _pMeshStock          = new CResourceStock_CSkelMesh;
  _pSkeletonStock      = new CResourceStock_CSkeleton;
  _pAnimSetStock       = new CResourceStock_CBoneAnimSet;
  _pShaderStock        = new CResourceStock_CShaderClass;

  _pModelConfigStock   = new CResourceStock_CModelConfiguration;

  _pModuleStock        = new CResourceStock_FModule;
}

CResourceManager::~CResourceManager()
{
  // Common stocks
  delete _pEntityClassStock;   _pEntityClassStock = NULL;
  delete _pModelStock;         _pModelStock       = NULL; 
  delete _pSoundStock;         _pSoundStock       = NULL; 
  delete _pTextureStock;       _pTextureStock     = NULL; 
  
  // Ska
  delete _pAnimStock;          _pAnimStock        = NULL; 
  delete _pMeshStock;          _pMeshStock        = NULL; 
  delete _pSkeletonStock;      _pSkeletonStock    = NULL; 
  delete _pAnimSetStock;       _pAnimSetStock     = NULL; 
  delete _pShaderStock;        _pShaderStock      = NULL; 

  delete _pModelConfigStock;   _pModelConfigStock      = NULL;

  delete _pModuleStock;        _pModuleStock      = NULL;
}

void CResourceManager::FreeUnusedStocks()
{
  StockForType(CResource::TYPE_MODELCONFIGURATION)->FreeUnused();
  
  for (int i = CResource::TYPE_INVALID + 1; i < CResource::TYPE_LAST; i++)
  {
    CResource::Type eType = (CResource::Type)i;
    CResourceStock *pStock = StockForType(eType);

    if (pStock) {
      pStock->FreeUnused();
    }
  }
}

void CResourceManager::StockDump(void)
{
  try {
    CTFileStream strm;
    CTFileName fnm = CTString("Temp\\StockDump.txt");

    strm.Create_t(fnm);

    for (INDEX i = CResource::TYPE_INVALID + 1; i < CResource::TYPE_LAST; i++)
    {
      enum CResource::Type ert = (CResource::Type)i;
      CResourceStock *pStock = StockForType(ert);

      if (pStock == NULL) {
        continue;
      }

      strm.FPrintF_t("%s\n", pStock->GetName());
      pStock->DumpMemoryUsage_t(strm);
    }

    CInfoF("Dumped to '%s'\n", CTString(fnm));

  } catch (char *strError) {
    CErrorF("Error: %s\n", strError);
  }
}

CResource *CResourceManager::Obtain_t(CResource::Type eType, const CTFileName &fnmFileName)
{
  CResourceStock *pStock = StockForType(eType);

  if (pStock == NULL) {
    ThrowF_t(TRANS("Can't load resource '%s' (%d) is of unknown type!"), (CTString&)fnmFileName, eType);
    return NULL;
  }

  return pStock->Obtain_t(fnmFileName);
}

void CResourceManager::Release(CResource *pObject)
{
  const CResource::Type eType = (CResource::Type)pObject->GetType();
  CResourceStock *pStock = StockForType(eType);
  
  if (pStock == NULL) {
    return;
  }

  pStock->Release(pObject);
}