/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Resources/ReplaceFile.h>
#include <Core/IO/Stream.h>
#include <Core/Base/ErrorReporting.h>
#include <Engine/Anim/Anim.h>
#include <Core/Base/Shell.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Sound/SoundObject.h>
#include <Core/Templates/DynamicContainer.cpp>

#include <Engine/Resources/ResourceManager.h>

#include <Core/Base/ListIterator.inl>

#define FILTER_ECL            "Entity Class Links (*.ecl)\0*.ecl\0"
#define FILTER_TEX            "Textures (*.tex)\0*.tex\0"
#define FILTER_MDL            "Models (*.mdl)\0*.mdl\0"
#define FILTER_ANI            "Animations (*.ani)\0*.ani\0"
#define FILTER_END            "\0"

BOOL _bFileReplacingApplied;

#ifndef NDEBUG
  #define ENGINEGUI_DLL_NAME "EngineGUID.dll"
#else
  #define ENGINEGUI_DLL_NAME "EngineGUI.dll"
#endif

extern INDEX wed_bUseBaseForReplacement;

static CTFileName CallFileRequester(char *achrTitle, char *achrSelectedFile, char *pFilter)
{
  typedef CTFileName FileRequester_t(
    char *pchrTitle, 
    char *pchrFilters,
    char *pchrRegistry,
    char *pchrDefaultFileSelected);

  HMODULE hGUI = GetModuleHandleA(ENGINEGUI_DLL_NAME);

  if (hGUI == NULL) {
    WarningMessage(TRANS("Cannot load %s:\n%s\nCannot replace files!"), 
      ENGINEGUI_DLL_NAME, GetWindowsError(GetLastError()));
    return CTString("");
  }

  FileRequester_t *pFileRequester = (FileRequester_t*)GetProcAddress(hGUI, 
    "?FileRequester@@YA?AVCTFileName@@PAD000@Z");

  if (pFileRequester == NULL) {
    WarningMessage(TRANS("Error in %s:\nFileRequester() function not found\nCannot replace files!"),
      ENGINEGUI_DLL_NAME);

    return CTString("");
  }

  return pFileRequester( achrTitle, pFilter, "Replace file directory", achrSelectedFile);
}

BOOL GetReplacingFile(CTFileName fnSourceFile, CTFileName &fnReplacingFile,
                      char *pFilter)
{
  // don't replace files if this console variable is set
  if (!wed_bUseBaseForReplacement) {
    return FALSE;
  }

  CTString strBaseForReplacingFiles;

  // try to find replacing texture in base
  try
  {
  	char achrLine[256];
  	char achrSource[256];
  	char achrRemap[256];
        
    // open file containing file names for replacing textures
    CTFileStream fsBase;
    CTFileName fnBaseName = CTString("Data\\BaseForReplacingFiles.txt");
    fsBase.Open_t( fnBaseName);

    while (!fsBase.AtEOF())
    {
      fsBase.GetLine_t(achrLine, 256);
      sscanf(achrLine, "\"%[^\"]\" \"%[^\"]\"", achrSource, achrRemap);

      if (CTString(achrSource) == achrRemap) {
        continue; // skip remaps to self
      }

      if (CTString(achrSource) == fnSourceFile)
      {
        fnReplacingFile = CTString( achrRemap);
        return TRUE;
      }
    }
    fsBase.Close();
  }
  // if file containing base can't be opened
  catch(char *strError)
  {
    (void) strError;
  }

  CTString strTitle;
  strTitle.PrintF(TRANS("For:\"%s\""), (CTString&)fnSourceFile);

  // call file requester for substituting file
  CTString strDefaultFile;
  strDefaultFile = fnSourceFile.FileName() + fnSourceFile.FileExt();
  fnReplacingFile = CallFileRequester(strTitle.Data(), strDefaultFile.Data(), pFilter);

  if (fnReplacingFile == "") return FALSE;

  try
  {
    // add new remap to end of remapping base file
    CTFileName fnBaseName = CTString("Data\\BaseForReplacingFiles.txt");
    CTString strBase;

    if (FileExists(fnBaseName))
    {
      strBase.Load_t(fnBaseName);
    }

    CTString strNewRemap;
    strNewRemap.PrintF( "\"%s\" \"%s\"\n", (CTString&)fnSourceFile, (CTString&)fnReplacingFile);
    strBase += strNewRemap;
    strBase.Save_t( fnBaseName);
  }
  catch( char *strError)
  {
    WarningMessage( strError);
    return FALSE;
  }

  return TRUE;
}

//! Search for the replacement class.
void GetReplacingClassFile_t(CTFileName &fnmClass) {
  for (;;) {
    try {
      // class doesn't exist
      if (!FileExists(fnmClass)) {
        ThrowF_t(TRANS("Entity Class Link file \"%s\" doesn't exist!"), fnmClass.ConstData());
      }
      return;

    } catch (char *strError) {
      (void)strError;
      CTFileName fnmReplace;

      // if class wasn't found, ask for a replacement
      if (GetReplacingFile(fnmClass, fnmReplace, FILTER_ECL FILTER_END)) {
        // replacing class was provided
        fnmClass = fnmReplace;

      } else {
        ThrowF_t(TRANS("Cannot find substitution for \"%s\""), fnmClass.ConstData());
      }
    }
  }
};

void SetTextureWithPossibleReplacing_t(CTextureObject &to, CTFileName &fnmTexture)
{
  // try to load texture
  for (;;)
  {
    try
    {
      to.SetData_t(fnmTexture);
      break;
    }
    catch( char *strError)
    {
      (void) strError;
      // if texture was not found, ask for replacing texture
      CTFileName fnReplacingTexture;

      if (GetReplacingFile( fnmTexture, fnReplacingTexture, FILTER_TEX FILTER_END))
      {
        // if replacing texture was provided, repeat reading of polygon's textures
        fnmTexture = fnReplacingTexture;
      }
      else
      {
        if (_pShell->GetINDEX("wed_bUseGenericTextureReplacement")) {
          fnmTexture = CTString("Textures\\Editor\\Default.tex");
          to.SetData_t(fnmTexture);
        } else {
          ThrowF_t(TRANS("Unable to load world because texture \"%s\" can't be found."), (CTString&)fnmTexture);
        }
      }
    }
  }
}

// read/write a texture object
void ReadTextureObject_t(CTStream &strm, CTextureObject &to)
{
  // read model texture data filename
  CTFileName fnTexture;
  strm >> fnTexture;

  // try to load texture
  for (;;)
  {
    try {
      // set the texture data
      to.SetData_t(fnTexture);
      break;
    } catch( char *strError) {
      (void) strError;

      CTFileName fnReplacingTexture;

      // if texture was not found, ask for replacing texture
      if (GetReplacingFile(fnTexture, fnReplacingTexture, FILTER_TEX FILTER_END)) {
        // replacing texture was provided
        fnTexture = fnReplacingTexture;
      } else {
        ThrowF_t( TRANS("Cannot find substitution for \"%s\""), (CTString&)fnTexture);
      }
    }
  }

  // read main texture anim object
  to.Read_t(&strm);
}

void SkipTextureObject_t(CTStream &strm)
{
  // skip texture filename
  CTFileName fnDummy;
  strm >> fnDummy;

  // skip texture object
  CTextureObject toDummy;
  toDummy.Read_t(&strm);
}

void WriteTextureObject_t(CTStream &strm, CTextureObject &to)
{
  // write model texture filename
  CTextureData *ptd = (CTextureData *)to.GetData();

  if (ptd != NULL) {
    strm << ptd->GetName();
  } else {
    strm << CTFileName(CTString(""));
  }

  // write texture anim object
  to.Write_t(&strm);
}

// read a model object from a file together with its model data filename
void ReadModelObject_t(CTStream &strm, CModelObject &mo)
{
  // read model data filename
  CTFileName fnModel;
  strm >> fnModel;

  // try to load model
  for (;;)
  {
    try {
      // set the model data
      mo.SetData_t(fnModel);
      break;
    } catch( char *strError) {
      (void) strError;

      CTFileName fnReplacingModel;

      // if model was not found, ask for replacing model
      if (GetReplacingFile( fnModel, fnReplacingModel, FILTER_MDL FILTER_END)) {
        // replacing model was provided
        fnModel = fnReplacingModel;
      } else {
        ThrowF_t( TRANS("Cannot find substitution for \"%s\""), (CTString&)fnModel);
      }
    }
  }

  // read model anim object
  mo.Read_t(&strm);

  // if model object has multiple textures
  if (strm.PeekID_t() == CChunkID("MTEX")) { // 'multi-texturing'
    // read all textures
    strm.ExpectID_t("MTEX");  // 'multi-texturing''
    ReadTextureObject_t(strm, mo.mo_toTexture);
    ReadTextureObject_t(strm, mo.mo_toBump);
    ReadTextureObject_t(strm, mo.mo_toReflection);
    ReadTextureObject_t(strm, mo.mo_toSpecular);

  // if model has single texture (old format) then read main texture
  } else {
    ReadTextureObject_t(strm, mo.mo_toTexture);
  }

  // if model object has attachments
  if (strm.PeekID_t() == CChunkID("ATCH")) { // 'attachments'
    // read attachments header
    strm.ExpectID_t("ATCH");  // 'attachments'
    INDEX ctAttachments;
    strm>>ctAttachments;

    // for each attachment
    for (INDEX iAttachment = 0; iAttachment<ctAttachments; iAttachment++)
    {
      // read its position and create the attachment
      INDEX iPosition;
      strm >> iPosition;

      CAttachmentModelObject *pamo = mo.AddAttachmentModel(iPosition);
      if (pamo != NULL) {
        // read its placement and model
        strm >> pamo->amo_plRelative;
        ReadModelObject_t(strm, pamo->amo_moModelObject);
      } else {
        // skip its placement and model
        CPlacement3D plDummy;
        strm >> plDummy;
        SkipModelObject_t(strm);
      }
    }
  }
}

void SkipModelObject_t(CTStream &strm)
{
  CTFileName fnDummy;
  CModelObject moDummy;

  // skip model data filename
  strm >> fnDummy;
  // skip model object
  moDummy.Read_t(&strm);

  // if model object has multiple textures
  if (strm.PeekID_t() == CChunkID("MTEX")) { // 'multi-texturing'
    // skip all textures
    strm.ExpectID_t("MTEX");  // 'multi-texturing''
    SkipTextureObject_t(strm);  // texture
    SkipTextureObject_t(strm);  // bump
    SkipTextureObject_t(strm);  // reflection
    SkipTextureObject_t(strm);  // specular

  // if model has single texture (old format)
  } else {
    // skip main texture
    SkipTextureObject_t(strm);
  }

  // if model object has attachments
  if (strm.PeekID_t() == CChunkID("ATCH")) { // 'attachments'
    // read attachments header
    strm.ExpectID_t("ATCH");  // 'attachments'
    INDEX ctAttachments;
    strm >> ctAttachments;

    // for each attachment skip its position, placement and model
    for (INDEX iAttachment = 0; iAttachment < ctAttachments; iAttachment++)
    {
      INDEX iPosition;
      strm >> iPosition;

      CPlacement3D plDummy;
      strm >> plDummy;

      SkipModelObject_t(strm);
    }
  }
}

void WriteModelObject_t(CTStream &strm, CModelObject &mo)
{
  // write model data filename
  CAnimData *pad = (CAnimData *)mo.GetData();

  if (pad != NULL) {
    strm << pad->GetName();
  } else {
    strm << CTFileName(CTString(""));
  }

  // write model anim object
  mo.Write_t(&strm);

  // write all textures
  strm.WriteID_t("MTEX");  // 'multi-texturing''
  WriteTextureObject_t(strm, mo.mo_toTexture);
  WriteTextureObject_t(strm, mo.mo_toBump);
  WriteTextureObject_t(strm, mo.mo_toReflection);
  WriteTextureObject_t(strm, mo.mo_toSpecular);

  // if model object has attachments
  if (!mo.mo_lhAttachments.IsEmpty())
  {
    // write attachments header
    strm.WriteID_t("ATCH");  // 'attachments'
    strm << mo.mo_lhAttachments.Count();

    // for each attachment
    FOREACHINLIST( CAttachmentModelObject, amo_lnInMain, mo.mo_lhAttachments, itamo)
    {
      CAttachmentModelObject *pamo = itamo;

      // write its position, placement and model
      strm << pamo->amo_iAttachedPosition;
      strm << pamo->amo_plRelative;
      WriteModelObject_t(strm, pamo->amo_moModelObject);
    }
  }
}

// read an anim object from a file together with its anim data filename
void ReadAnimObject_t(CTStream &strm, CAnimObject &ao)
{
  // read anim data filename
  CTFileName fnAnim;
  strm>>fnAnim;

  // try to load anim
  for (;;)
  {
    try {
      // set the anim data
      ao.SetData_t(fnAnim);
      break;
    } catch (char *strError) {
      (void) strError;
   
      CTFileName fnReplacingAnim;

      // if anim was not found, ask for replacing anim
      if (GetReplacingFile( fnAnim, fnReplacingAnim, FILTER_ANI FILTER_END)) {
        // replacing anim was provided
        fnAnim = fnReplacingAnim;
      } else {
        ThrowF_t( TRANS("Cannot find substitution for \"%s\""), (CTString&)fnAnim);
      }
    }
  }

  // read anim object
  ao.Read_t(&strm);
}

void SkipAnimObject_t(CTStream &strm)
{
  CTFileName fnDummy;
  CAnimObject aoDummy;

  // skip anim data filename
  strm >> fnDummy;
  // skip anim object
  aoDummy.Read_t(&strm);
}

void WriteAnimObject_t(CTStream &strm, CAnimObject &ao)
{
  // write anim data filename
  CAnimData *pad = (CAnimData *)ao.GetData();

  if (pad != NULL) {
    strm << pad->GetName();
  } else {
    strm << CTFileName(CTString(""));
  }

  // write anim object
  ao.Write_t(&strm);
}

// read a sound object from a file together with its sound data filename
// NOTE: sound objects cannot be replaced
void ReadSoundObject_t(CTStream &strm, CSoundObject &so)
{
  so.Read_t(&strm);
}

void SkipSoundObject_t(CTStream &strm)
{
  CSoundObject soDummy;
  soDummy.Read_t(&strm);
}

void WriteSoundObject_t(CTStream &strm, CSoundObject &so)
{
  so.Write_t(&strm);
}
