/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Anim/Anim.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Meshes/BoneAnimSet.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Meshes/SkelMesh.h>
#include <Engine/Graphics/Shader.h>
#include <Engine/Meshes/Skeleton.h>
#include <Engine/Sound/SoundData.h>
#include <Engine/Graphics/Texture.h>

#include <Engine/Meshes/ModelConfiguration.h>

#include <Core/Modules/Module.h>

#include <Core/IO/Resource.h>
#include <Core/IO/ResourceStock.h>

#include <Engine/Resources/Stocks.h>

CResource *CResourceStock_CAnimData::New()
{
  return new CAnimData();
}

CResource *CResourceStock_CBoneAnimSet::New()
{
  return new CBoneAnimSet();
}

CResource *CResourceStock_CEntityClass::New()
{
  return new CEntityClass();
}

CResource *CResourceStock_CSkelMesh::New()
{
  return new CSkelMesh();
}

CResource *CResourceStock_CModelData::New()
{
  return new CModelData();
}

CResource *CResourceStock_CShaderClass::New()
{
  return new CShaderClass();
}

CResource *CResourceStock_CSkeleton::New()
{
  return new CSkeleton();
}

CResource *CResourceStock_CSoundData::New()
{
  return new CSoundData();
}

CResource *CResourceStock_CTextureData::New()
{
  return new CTextureData();
}

CResource *CResourceStock_CModelConfiguration::New()
{
  return new CModelConfiguration();
}

CResource *CResourceStock_FModule::New()
{
  return new FModule();
}
