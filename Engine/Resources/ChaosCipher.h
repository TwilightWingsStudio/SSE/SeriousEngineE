/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CHAOSCIPHER_H
#define SE_INCL_CHAOSCIPHER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#define CHAOS_CODE_MESH_1 13
#define CHAOS_CODE_MESH_2 23
#define CHAOS_CODE_MESH_3 19
#define CHAOS_CODE_MESH_4 29
#define CHAOS_CODE_MESH_5 5

class CChaosMeshCipher
{
  public:
    CChaosMeshCipher(UBYTE ubChecker)
    {
      _ubChecker = ubChecker;
    }
    
    template<class Type>
    inline Type Encode(Type val)
    {
      if(sizeof(Type) == 4)
      {
        ULONG ulChecker = 0;
        _ubChecker += (UBYTE)CHAOSCODE_MESH_1;
        ulChecker |= ULONG(_ubChecker) << 24;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_2;
        ulChecker |= ULONG(_ubChecker) << 16;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_3;
        ulChecker |= ULONG(_ubChecker) << 8;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_4;
        ulChecker |= ULONG(_ubChecker) << 0;
        _ubChecker += CHAOS_CODE_MESH_5;
        return Type( val ^ ulChecker);
      } else {
        return val;
      }
    }

    template<class Type>
    inline Type Decode(Type val)
    {
      if(sizeof(Type) == 4)
      {
        ULONG ulChecker = 0;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_1;
        ulChecker |= ULONG(_ubChecker) << 24;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_2;
        ulChecker |= ULONG(_ubChecker) << 16;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_3;
        ulChecker |= ULONG(_ubChecker) << 8;
        _ubChecker += (UBYTE)CHAOS_CODE_MESH_4;
        ulChecker |= ULONG(_ubChecker) << 0;
        _ubChecker += CHAOS_CODE_MESH_5;
        return Type( val ^ ulChecker);
      } else {
        return val;
      }
    }
  
  public:
    UBYTE _ubChecker;
};

#define CHAOS_CODE_TEX_1 17
#define CHAOS_CODE_TEX_2 02
#define CHAOS_CODE_TEX_3 41
#define CHAOS_CODE_TEX_4 01
#define CHAOS_CODE_TEX_5 6

class CChaosTexCipher
{
  public:
    CChaosTexCipher(UBYTE ubChecker)
    {
      _ubChecker = ubChecker;
    }
    
    template<class Type>
    inline Type Encode(Type val)
    {
      if(sizeof(Type) == 4)
      {
        ULONG ulChecker = 0;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_1;
        ulChecker |= ULONG(_ubChecker) << 24;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_2;
        ulChecker |= ULONG(_ubChecker) << 16;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_3;
        ulChecker |= ULONG(_ubChecker) << 8;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_4;
        ulChecker |= ULONG(_ubChecker) << 0;
        _ubChecker += CHAOS_CODE_TEX_5;
        return Type( val ^ ulChecker);
      } else {
        return val;
      }
    }

    template<class Type>
    inline Type Decode(Type val)
    {
      if(sizeof(Type) == 4)
      {
        ULONG ulChecker = 0;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_1;
        ulChecker |= ULONG(_ubChecker) << 24;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_2;
        ulChecker |= ULONG(_ubChecker) << 16;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_3;
        ulChecker |= ULONG(_ubChecker) << 8;
        _ubChecker += (UBYTE)CHAOS_CODE_TEX_4;
        ulChecker |= ULONG(_ubChecker) << 0;
        _ubChecker += CHAOS_CODE_TEX_5;
        return Type( val ^ ulChecker);
      } else {
        return val;
      }
    }
  
  public:
    UBYTE _ubChecker;
};

#endif /* include-once check. */