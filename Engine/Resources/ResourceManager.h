/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_RESOURCEMANAGER_H
#define SE_INCL_RESOURCEMANAGER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/Resource.h>
#include <Core/IO/ResourceStock.h>

class ENGINE_API CResourceManager
{
  public:
    //! Constructor.
    CResourceManager();
    
    //! Destructor.
    ~CResourceManager();

    static void FreeUnusedStocks();
    static void StockDump();

    static CResourceStock *StockForType(enum CResource::Type eType);
    static CResource *Obtain_t(enum CResource::Type eType, const CTFileName &fnmFileName);
    static void Release(CResource *pObject);
};

ENGINE_API extern CResourceManager *_pResourceMgr;

#endif