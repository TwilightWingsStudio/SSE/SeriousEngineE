#ifndef SE_INCL_STOCKS_H
#define SE_INCL_STOCKS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class CResourceStock_CAnimData : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CAnimData";
    }
};

class CResourceStock_CBoneAnimSet : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CBoneAnimSet";
    }
};

class CResourceStock_CEntityClass : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CEntityClass";
    }
};

class CResourceStock_CSkelMesh : public CResourceStock
{
  CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CSkelMesh";
    }
};

class CResourceStock_CModelData : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CModelData";
    }
};

class CResourceStock_CShaderClass : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CShaderClass";
    }
};

class CResourceStock_CSkeleton : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CSkeleton";
    }
};

class CResourceStock_CSoundData : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CSoundData";
    }
};

class CResourceStock_CTextureData : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CTextureData";
    }
};

class CResourceStock_CModelConfiguration : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "CModelConfiguration";
    }
};

class CResourceStock_FModule : public CResourceStock
{
  protected:
    CResource *New() override;

  public:
    virtual const char *GetName() const
    {
      return "FModule";
    }
};

#endif  /* include-once check. */