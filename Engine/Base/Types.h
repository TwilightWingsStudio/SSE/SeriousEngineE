/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TYPES_H
#define SE_INCL_TYPES_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/EngineApi.h>
#include <Core/Base/Types.h>

typedef long int CUSTOMDATA;

// class predeclarations
class CAnimData;
class CAnimObject;
class CBoneAnimSet;
class CAnyProjection3D;
class CBrush3D;
class CBrushArchive;
class CBrushEdge;
class CBrushEdge;
class CBrushMip;
class CBrushMip;
class CBrushPlane;
class CBrushPolygon;
class CBrushPolygonEdge;
class CBrushSector;
class CBrushShadowLayer;
class CBrushShadowMap;
class CBrushTexture;
class CBrushTexture;
class CBrushVertex;
class CBrushVertex;
class CTerrain;
class CTerrainTile;
class CTerrainLayer;
class CTerrainArchive;
class CCastRay;
class CClipMove;
class CClipTest;
class CCollisionInfo;
class CContentType;
class CDisplayMode;
class CDrawPort;
class CNode;
class CEntity;
class CEntityPointer;
class CEntityProperty;
class CEntityResource;
class CEntityPropertyEnumType;
class CEntityClass;
class CFieldSettings;
class CFontData;
class CFontCharData;
class CGfxLibrary;
class CImageInfo;
class CIsometricProjection3D;
class CLastPositions;
class CLayerMaker;
class CLayerMixer;
class CLightSource;
class CLiveEntity;
class CSkelMesh;
class CMessageDispatcher;
class CModelInstance;
class CModelObject;
class CModelData;
class CMovableBrushEntity;
class CMovableEntity;
class CMovableModelEntity;
class CNetworkMessage;
class CNetworkNode;
class CNetworkStream;
class CNetworkStreamBlock;
class CPlanarGradients;
class CPlayerAction;
class CPlayerBuffer;
class CPlayerCharacter;
class CPlayerSource;
class CPlayerTarget;
class CRationalEntity;
class CRaster;
class CRenderer;
class CScreenEdge;
class CScreenPolygon;
class CServer;
class CSessionState;
class CShadingInfo;
class CShadowMap;
class CShadowMapMaker;
class CSkeleton;
class CStaticLightSource;
class CSoundData;
class CSoundLibrary;
class CSoundObject;
class CSoundListener;
class CSoundParameters;
class CSoundParameters3D;
class CSurfaceType;
class CTextureData;
class CTextureObject;
class CViewPort;
class CWorkingVertex;
class CWorkingPlane;
class CWorkingEdge;
class CWorld;

class CAbstractModelInstance;
class CModelConfiguration;
class CLibEntityClass;
class CLibEntityEvent;
class CShaderClass;

#define CDLLEntityClass CLibEntityClass
#define CDllEntityEvent CLibEntityEvent

//! Flags that enable specific features during world serialization.
enum WorldStreamFlags
{
  //! Will be checked if serialization performed during simulation.
  WSF_SIMULATION = (1L << 0),

  //! Will be checked if serialization performed through editor.
  WSF_EDITOR = (1L << 1),

  //! Write only! Used for saves/demos/statedelta to refer to DICT from a WLD source.
  WSF_IMPORTDICTIONARY = (1L << 2),

  //! Nitro Family specific! Skip extra flags within skeletal rendering objects.
  WSF_NITROFAMILYBMC = (1L << 3),
};

#endif  /* include-once check. */
