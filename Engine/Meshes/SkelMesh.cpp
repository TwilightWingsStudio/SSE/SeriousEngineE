/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include "SkelMesh.h"

#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Core/Math/Projection.h>
#include <Engine/Graphics/DrawPort.h>
#include <Engine/Graphics/Shader.h>
#include <Core/Templates/StaticArray.h>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Resources/ResourceManager.h>

INDEX AreVerticesDiferent(INDEX iCurentIndex, INDEX iLastIndex);

// TODO: Move to header
/*
 * TODO: Add comment here
 */
struct VertexLocator
{
  INDEX vl_iIndex;
  INDEX vl_iSubIndex;
};

#define MAX_WEIGHTS_PER_SURFACE 25

struct VertexWeights
{
  FLOAT vw_afWeights[4];
  INDEX vw_aiIndices[4];
};

// TODO: Move to header
/*
 * TODO: Add comment here
 */
struct SortArray
{
  INDEX sa_iNewIndex;
  INDEX sa_iSurfaceIndex;
  TStaticArray<struct VertexLocator> sa_aWeightMapList;
  TStaticArray<struct VertexLocator> sa_aMorphMapList;
};

TStaticArray <struct SortArray> _aSortArray;
TStaticArray <INDEX> _aiOptimizedIndex;
TStaticArray <INDEX> _aiSortedIndex;

CSkelMeshLod *pSkelMeshLod; // curent mesh lod (for quick sort)
CSkelMeshLod mshOptimized;

// TODO: Add comment here
CSkelMesh::CSkelMesh()
{
  msh_iVersion = 0xFFFFFFFF;
}

// TODO: Add comment here
CSkelMesh::~CSkelMesh()
{
}

// Release old shader and obtain new shader for mesh surface (expand ShaderParams if needed)
void ChangeSurfaceShader_t(MeshSurface &msrf,CTString fnNewShader)
{
  CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SHADER, fnNewShader);
  CShaderClass *pShaderNew = static_cast<CShaderClass *>(pser);

  ASSERT(pShaderNew != NULL);

  if (msrf.msrf_pShader != NULL) {
    _pResourceMgr->Release(msrf.msrf_pShader);
  }
  msrf.msrf_pShader = pShaderNew;
  
  // Get new shader description
  ShaderDesc shDesc;
  msrf.msrf_pShader->GetShaderDesc(shDesc);
  
  // If needed expand size of arrays for new shader - reset new values!!!!
  INDEX ctOldTextureIDs = msrf.msrf_ShadingParams.sp_aiTextureIDs.Count();
  INDEX ctNewTextureIDs = shDesc.sd_astrTextureNames.Count();
  INDEX ctOldUVMaps     = msrf.msrf_ShadingParams.sp_aiTexCoordsIndex.Count();
  INDEX ctNewUVMaps     = shDesc.sd_astrTexCoordNames.Count();
  INDEX ctOldColors     = msrf.msrf_ShadingParams.sp_acolColors.Count();
  INDEX ctNewColors     = shDesc.sd_astrColorNames.Count();
  INDEX ctOldFloats     = msrf.msrf_ShadingParams.sp_afFloats.Count();
  INDEX ctNewFloats     = shDesc.sd_astrFloatNames.Count();
  
  if (ctOldTextureIDs < ctNewTextureIDs)
  {
    // Expand texture IDs array
    msrf.msrf_ShadingParams.sp_aiTextureIDs.Expand(ctNewTextureIDs);
    
    // Set new texture IDs to 0
    for (INDEX itx = ctOldTextureIDs; itx < ctNewTextureIDs; itx++)
    {
      msrf.msrf_ShadingParams.sp_aiTextureIDs[itx] = -1;
    }
  }
  
  // Expand array of uvmaps if needed
  if (ctOldUVMaps < ctNewUVMaps)
  {
    // Expand uvmaps IDs array
    msrf.msrf_ShadingParams.sp_aiTexCoordsIndex.Expand(ctNewUVMaps);
    
    // Set new uvmaps indices to 0
    for (INDEX itxc = ctOldUVMaps; itxc < ctNewUVMaps; itxc++)
    {
      msrf.msrf_ShadingParams.sp_aiTexCoordsIndex[itxc] = 0;
    }
  }
  
  // Expand array of colors if needed
  if (ctOldColors < ctNewColors)
  {
    // Expand color array
    msrf.msrf_ShadingParams.sp_acolColors.Expand(ctNewColors);
    
    // Set new colors indices white
    for (INDEX icol = ctOldUVMaps; icol < ctNewColors; icol++)
    {
      msrf.msrf_ShadingParams.sp_acolColors[icol] = 0xFFFFFFFF;
    }
  }
  
  // Expand array of floats if needed
  if (ctOldFloats < ctNewFloats)
  {
    // Expand float array
    msrf.msrf_ShadingParams.sp_afFloats.Expand(ctNewFloats);
    
    // Set new floats to 0
    for (INDEX ifl = ctOldFloats; ifl < ctNewFloats; ifl++)
    {
      msrf.msrf_ShadingParams.sp_afFloats[ifl] = 1;
    }
  }
}

// Quck sort func for comparing vertices
static int qsort_CompareArray(const void *pVx1, const void *pVx2)
{
  INDEX *n1 = ((INDEX*)pVx1);
  INDEX *n2 = ((INDEX*)pVx2);
  return AreVerticesDiferent(*n1,*n2);
}

// Clear array of sort vertices
void ClearSortArray(INDEX ctOldVertices)
{
  for (int iv = 0; iv < ctOldVertices; iv++)
  {
    _aSortArray[iv].sa_aWeightMapList.Clear();
    _aSortArray[iv].sa_aMorphMapList.Clear();
  }
  _aiOptimizedIndex.Clear();
  _aiSortedIndex.Clear();
  _aSortArray.Clear();
}

// Optimize mesh
void CSkelMesh::Optimize(void)
{
  INDEX ctmshlods = msh_aLods.Count();
  for (int imshlod = 0; imshlod < ctmshlods; imshlod++)
  {
    // Optimize each lod in mesh
    OptimizeLod(msh_aLods[imshlod]);
  }
}

static void FillVertexWeightInfos(CSkelMeshLod &mlod);

// Optimize lod of mesh
void CSkelMesh::OptimizeLod(CSkelMeshLod &mLod)
{
  INDEX ctVertices = mLod.mlod_aVertices.Count();
  INDEX ctSurfaces = mLod.mlod_aSurfaces.Count();
  INDEX ctUVMaps = mLod.mlod_aUVMaps.Count();
  INDEX ctWeightMaps = mLod.mlod_aWeightMaps.Count();
  INDEX ctMorphMaps = mLod.mlod_aMorphMaps.Count();

  if (ctVertices <= 0) {
    return;
  }
  
  // Create array for sorting
  _aSortArray.New(ctVertices);
  _aiSortedIndex.New(ctVertices);
  _aiOptimizedIndex.New(ctVertices);
  
  // Put original vertex indices in SortArray
  for (int iv = 0; iv < ctVertices; iv++)
  {
    _aiSortedIndex[iv] = iv;
  }
  
  // Loop each surface and expand SurfaceList in SortArray
  int is = 0;
  for (; is < ctSurfaces; is++)
  {
    INDEX ctts = mLod.mlod_aSurfaces[is].msrf_aTriangles.Count();
    for (int its = 0; its < ctts; its++)
    {
      MeshTriangle &mtTriangle = mLod.mlod_aSurfaces[is].msrf_aTriangles[its];

      _aSortArray[mtTriangle.iVertex[0]].sa_iSurfaceIndex = is;
      _aSortArray[mtTriangle.iVertex[1]].sa_iSurfaceIndex = is;
      _aSortArray[mtTriangle.iVertex[2]].sa_iSurfaceIndex = is;
    }
  }
  
  // Loop each weightmap and expand sa_aWeightMapList in SortArray
  for (INDEX iw = 0; iw < ctWeightMaps; iw++)
  {
    // Loop each wertex weight array in weight map array
    INDEX ctwm = mLod.mlod_aWeightMaps[iw].mwm_aVertexWeight.Count();
    for (INDEX iwm = 0; iwm < ctwm; iwm++)
    {
      MeshVertexWeight &mwwWeight = mLod.mlod_aWeightMaps[iw].mwm_aVertexWeight[iwm];
      
      // Get curent list num of weightmaps  
      INDEX ctWeightMapList = _aSortArray[mwwWeight.mww_iVertex].sa_aWeightMapList.Count();
      
      // Expand array of sufrace lists for 1
      _aSortArray[mwwWeight.mww_iVertex].sa_aWeightMapList.Expand(ctWeightMapList + 1);
      // Set vl_iIndex to index of surface
      // Set vl_iSubIndex to index in triangle set
      VertexLocator &vxLoc = _aSortArray[mwwWeight.mww_iVertex].sa_aWeightMapList[ctWeightMapList];
      vxLoc.vl_iIndex = iw;
      vxLoc.vl_iSubIndex = iwm;
    }
  }
  
  // Loop each morphmap and expand sa_aMorphMapList in SortArray
  for (INDEX im = 0; im < ctMorphMaps; im++)
  {
    // Loop each morph map in array
    INDEX ctmm = mLod.mlod_aMorphMaps[im].mmp_aMorphMap.Count();
    for (INDEX imm = 0; imm < ctmm; imm++)
    {
      MeshVertexMorph &mwmMorph = mLod.mlod_aMorphMaps[im].mmp_aMorphMap[imm];
      
      // Get curent list num of morphmaps  
      INDEX ctMorphMapList = _aSortArray[mwmMorph.mwm_iVxIndex].sa_aMorphMapList.Count();
      
      // Expand array of sufrace lists for 1
      _aSortArray[mwmMorph.mwm_iVxIndex].sa_aMorphMapList.Expand(ctMorphMapList + 1);
      // Set vl_iIndex to index of surface
      // Set vl_iSubIndex to index in triangle set
      VertexLocator &vxLoc = _aSortArray[mwmMorph.mwm_iVxIndex].sa_aMorphMapList[ctMorphMapList];
      vxLoc.vl_iIndex = im;
      vxLoc.vl_iSubIndex = imm;
    }
  }
  
  // Set global pSkelMeshLod pointer used by quicksort
  pSkelMeshLod = &mLod;
  
  // Sort array
  qsort(&_aiSortedIndex[0], ctVertices, sizeof(&_aiSortedIndex[0]), qsort_CompareArray);
  
  // Compare vertices
  INDEX iDiferentVertices = 1;
  INDEX iLastIndex = _aiSortedIndex[0];
  _aSortArray[iLastIndex].sa_iNewIndex = 0;
  _aiOptimizedIndex[0] = iLastIndex;
  
  for (INDEX isa = 1; isa < ctVertices; isa++)
  {
    INDEX iCurentIndex = _aiSortedIndex[isa];
    
    // Check if vertices are diferent
    if (AreVerticesDiferent(iLastIndex,iCurentIndex)) {
      // Add Curent index to Optimized index array
      _aiOptimizedIndex[iDiferentVertices] = iCurentIndex;
      iDiferentVertices++;
      iLastIndex = iCurentIndex;
    }
    _aSortArray[iCurentIndex].sa_iNewIndex = iDiferentVertices - 1;
  }

  // Create new mesh
  INDEX ctNewVertices = iDiferentVertices;
  mshOptimized.mlod_aVertices.New(ctNewVertices);
  mshOptimized.mlod_aNormals.New(ctNewVertices);
  mshOptimized.mlod_aUVMaps.New(ctUVMaps);
  for (INDEX iuvm = 0;iuvm < ctUVMaps; iuvm++)
  {
    mshOptimized.mlod_aUVMaps[iuvm].muv_aTexCoords.New(ctNewVertices);
  }

  // Add new vertices and normals to mshOptimized
  for (INDEX iNewVx = 0; iNewVx < ctNewVertices; iNewVx++)
  {
    mshOptimized.mlod_aVertices[iNewVx] = mLod.mlod_aVertices[_aiOptimizedIndex[iNewVx]];
    mshOptimized.mlod_aNormals[iNewVx] = mLod.mlod_aNormals[_aiOptimizedIndex[iNewVx]];
    for (INDEX iuvm = 0; iuvm < ctUVMaps;iuvm++)
    {
      //FIXME: // ???
      mshOptimized.mlod_aUVMaps[iuvm].muv_iID = mLod.mlod_aUVMaps[iuvm].muv_iID;
      mshOptimized.mlod_aUVMaps[iuvm].muv_aTexCoords[iNewVx] =
        mLod.mlod_aUVMaps[iuvm].muv_aTexCoords[_aiOptimizedIndex[iNewVx]];
    }
  }
  
  // Remap surface triangles
  for (is = 0; is < ctSurfaces; is++)
  {
    MeshSurface &msrf = mLod.mlod_aSurfaces[is];
    INDEX iMinIndex = ctNewVertices + 1;
    INDEX iMaxIndex = -1;
    INDEX ctts = msrf.msrf_aTriangles.Count();
    
    // For each triangle in this surface
    INDEX its = 0;
    for (; its < ctts; its++)
    {
      MeshTriangle &mtTriangle = msrf.msrf_aTriangles[its];
      // For each vertex in triangle
      for (INDEX iv = 0;iv < 3; iv++)
      {
        mtTriangle.iVertex[iv] = _aSortArray[mtTriangle.iVertex[iv]].sa_iNewIndex;
        
        // Find first index in this surface
        if (mtTriangle.iVertex[iv] < iMinIndex) {
          iMinIndex = mtTriangle.iVertex[iv];
        }
        
        // Find last index in this surface
        if (mtTriangle.iVertex[iv] > iMaxIndex) {
          iMaxIndex = mtTriangle.iVertex[iv];
        }
      }
    }
    
    // Remember first index in vertices array
    msrf.msrf_iFirstVertex = iMinIndex;
    
    // Remember vertices count
    msrf.msrf_ctVertices = iMaxIndex - iMinIndex + 1;

    // For each triangle in surface
    for (its = 0; its < ctts; its++)
    {
      MeshTriangle &mtTriangle = msrf.msrf_aTriangles[its];
      
      // For each vertex in triangle
      for (INDEX iv = 0; iv < 3; iv++)
      {
        // Substract vertex index in triangle with first vertex in surface
        mtTriangle.iVertex[iv] -= msrf.msrf_iFirstVertex;
        ASSERT(mtTriangle.iVertex[iv] < msrf.msrf_ctVertices);
      }
    }
  }

  // Remap weightmaps
  mshOptimized.mlod_aWeightMaps.New(ctWeightMaps);
  
  // Expand wertex veights array for each vertex
  INDEX ivx = 0;
  for (;ivx < ctNewVertices; ivx++)
  {
    INDEX ioptVx = _aiOptimizedIndex[ivx];
    for (INDEX iwl = 0;iwl < _aSortArray[ioptVx].sa_aWeightMapList.Count(); iwl++)
    {
      VertexLocator &wml = _aSortArray[ioptVx].sa_aWeightMapList[iwl];
      INDEX wmIndex = wml.vl_iIndex;
      INDEX wwIndex = wml.vl_iSubIndex;
      INDEX ctww = mshOptimized.mlod_aWeightMaps[wmIndex].mwm_aVertexWeight.Count();
      MeshWeightMap &mwm = mshOptimized.mlod_aWeightMaps[wmIndex];
      MeshVertexWeight &mww = mLod.mlod_aWeightMaps[wmIndex].mwm_aVertexWeight[wwIndex];

      mwm.mwm_iID = mLod.mlod_aWeightMaps[wmIndex].mwm_iID;
      mwm.mwm_aVertexWeight.Expand(ctww + 1);
      mwm.mwm_aVertexWeight[ctww].mww_fWeight = mww.mww_fWeight;
      mwm.mwm_aVertexWeight[ctww].mww_iVertex = ivx;
    }
  }

  // Remap morphmaps
  mshOptimized.mlod_aMorphMaps.New(ctMorphMaps);
  
  // Expand morph maps array for each vertex
  for (ivx = 0;ivx < ctNewVertices; ivx++)
  {
    INDEX ioptVx = _aiOptimizedIndex[ivx];
    for (INDEX iml = 0; iml < _aSortArray[ioptVx].sa_aMorphMapList.Count(); iml++)
    {
      VertexLocator &mml = _aSortArray[ioptVx].sa_aMorphMapList[iml];
      INDEX mmIndex = mml.vl_iIndex;
      INDEX mwmIndex = mml.vl_iSubIndex;
      INDEX ctmwm = mshOptimized.mlod_aMorphMaps[mmIndex].mmp_aMorphMap.Count();
      MeshMorphMap &mmm = mshOptimized.mlod_aMorphMaps[mmIndex];
      MeshVertexMorph &mwm = mLod.mlod_aMorphMaps[mmIndex].mmp_aMorphMap[mwmIndex];

      mmm.mmp_iID = mLod.mlod_aMorphMaps[mmIndex].mmp_iID;
      mmm.mmp_bRelative = mLod.mlod_aMorphMaps[mmIndex].mmp_bRelative;

      mmm.mmp_aMorphMap.Expand(ctmwm + 1);
      mmm.mmp_aMorphMap[ctmwm].mwm_iVxIndex = ivx;
      mmm.mmp_aMorphMap[ctmwm].mwm_x = mwm.mwm_x;
      mmm.mmp_aMorphMap[ctmwm].mwm_y = mwm.mwm_y;
      mmm.mmp_aMorphMap[ctmwm].mwm_z = mwm.mwm_z;
      mmm.mmp_aMorphMap[ctmwm].mwm_nx = mwm.mwm_nx;
      mmm.mmp_aMorphMap[ctmwm].mwm_ny = mwm.mwm_ny;
      mmm.mmp_aMorphMap[ctmwm].mwm_nz = mwm.mwm_nz;
    }
  }

  mLod.mlod_aVertices.CopyArray(mshOptimized.mlod_aVertices);
  mLod.mlod_aNormals.CopyArray(mshOptimized.mlod_aNormals);
  mLod.mlod_aMorphMaps.CopyArray(mshOptimized.mlod_aMorphMaps);
  mLod.mlod_aWeightMaps.CopyArray(mshOptimized.mlod_aWeightMaps);
  mLod.mlod_aUVMaps.CopyArray(mshOptimized.mlod_aUVMaps);

  // Clear memory
  ClearSortArray(ctVertices);
  mshOptimized.mlod_aVertices.Clear();
  mshOptimized.mlod_aNormals.Clear();
  mshOptimized.mlod_aWeightMaps.Clear();
  mshOptimized.mlod_aMorphMaps.Clear();
  mshOptimized.mlod_aUVMaps.Clear();
  mshOptimized.mlod_aVertexWeights.Clear();
  
  // Indices are surface relative.
  mLod.mlod_ulFlags |= ML_SURFACE_RELATIVE_INDICES;

  // Fill vertex weights.
  FillVertexWeightInfos(mLod);
}

INDEX AreVerticesDiferent(INDEX iCurentIndex, INDEX iLastIndex)
{
#define CHECK(x,y) if (((x)-(y))!=0) return ((x)-(y))
#define CHECKF(x,y) if (((x)-(y))!=0) return Sgn((x)-(y))
  
  // Сheck surfaces
  CHECK(_aSortArray[iCurentIndex].sa_iSurfaceIndex,_aSortArray[iLastIndex].sa_iSurfaceIndex);
  
  // Сheck vertices
  CHECKF(pSkelMeshLod->mlod_aVertices[iCurentIndex].y,pSkelMeshLod->mlod_aVertices[iLastIndex].y);
  CHECKF(pSkelMeshLod->mlod_aVertices[iCurentIndex].x,pSkelMeshLod->mlod_aVertices[iLastIndex].x);
  CHECKF(pSkelMeshLod->mlod_aVertices[iCurentIndex].z,pSkelMeshLod->mlod_aVertices[iLastIndex].z);
  
  // Сheck normals
  CHECKF(pSkelMeshLod->mlod_aNormals[iCurentIndex].ny,pSkelMeshLod->mlod_aNormals[iLastIndex].ny);
  CHECKF(pSkelMeshLod->mlod_aNormals[iCurentIndex].nx,pSkelMeshLod->mlod_aNormals[iLastIndex].nx);
  CHECKF(pSkelMeshLod->mlod_aNormals[iCurentIndex].nz,pSkelMeshLod->mlod_aNormals[iLastIndex].nz);
  
  // Сheck uvmaps
  INDEX ctUVMaps = pSkelMeshLod->mlod_aUVMaps.Count();
  for (INDEX iuvm = 0; iuvm < ctUVMaps; iuvm++)
  {
    CHECKF(pSkelMeshLod->mlod_aUVMaps[iuvm].muv_aTexCoords[iCurentIndex].u,
           pSkelMeshLod->mlod_aUVMaps[iuvm].muv_aTexCoords[iLastIndex].u);
    CHECKF(pSkelMeshLod->mlod_aUVMaps[iuvm].muv_aTexCoords[iCurentIndex].v,
           pSkelMeshLod->mlod_aUVMaps[iuvm].muv_aTexCoords[iLastIndex].v);
  }
  
  // Сount weight and morph maps
  INDEX ctwmCurent  = _aSortArray[iCurentIndex].sa_aWeightMapList.Count();
  INDEX ctwmLast    = _aSortArray[iLastIndex].sa_aWeightMapList.Count();
  INDEX ctmmCurent  = _aSortArray[iCurentIndex].sa_aMorphMapList.Count();
  INDEX ctmmLast    = _aSortArray[iLastIndex].sa_aMorphMapList.Count();
  
  // Сheck if vertices have same weight and morph maps count
  CHECK(ctwmCurent,ctwmLast);
  CHECK(ctmmCurent,ctmmLast);
  
  // Сheck if vertices have same weight map factors
  for (INDEX iwm = 0; iwm < ctwmCurent; iwm++)
  {
    // Get weight map indices
    INDEX iwmCurent = _aSortArray[iCurentIndex].sa_aWeightMapList[iwm].vl_iIndex;
    INDEX iwmLast   = _aSortArray[iLastIndex].sa_aWeightMapList[iwm].vl_iIndex;
    
    // Get wertex weight indices
    INDEX iwwCurent = _aSortArray[iCurentIndex].sa_aWeightMapList[iwm].vl_iSubIndex;
    INDEX iwwLast   = _aSortArray[iLastIndex].sa_aWeightMapList[iwm].vl_iSubIndex;
    
    // If weight map factors are diferent
    CHECKF(pSkelMeshLod->mlod_aWeightMaps[iwmCurent].mwm_aVertexWeight[iwwCurent].mww_fWeight,
           pSkelMeshLod->mlod_aWeightMaps[iwmLast].mwm_aVertexWeight[iwwLast].mww_fWeight);
  }

  // Check if vertices have same morph map factors
  for (INDEX imm = 0;imm < ctmmCurent; imm++)
  {
    // Get morph map indices
    INDEX immCurent = _aSortArray[iCurentIndex].sa_aMorphMapList[imm].vl_iIndex;
    INDEX immLast   = _aSortArray[iLastIndex].sa_aMorphMapList[imm].vl_iIndex;
    
    // Get mesh vertex morph indices
    INDEX imwmCurent  = _aSortArray[iCurentIndex].sa_aMorphMapList[imm].vl_iSubIndex;
    INDEX imwmLast    = _aSortArray[iLastIndex].sa_aMorphMapList[imm].vl_iSubIndex;
    
    // If mesh morph map params are diferent return
    CHECKF(pSkelMeshLod->mlod_aMorphMaps[immCurent].mmp_aMorphMap[imwmCurent].mwm_x,
           pSkelMeshLod->mlod_aMorphMaps[immLast].mmp_aMorphMap[imwmLast].mwm_x);
    CHECKF(pSkelMeshLod->mlod_aMorphMaps[immCurent].mmp_aMorphMap[imwmCurent].mwm_y,
           pSkelMeshLod->mlod_aMorphMaps[immLast].mmp_aMorphMap[imwmLast].mwm_y);
    CHECKF(pSkelMeshLod->mlod_aMorphMaps[immCurent].mmp_aMorphMap[imwmCurent].mwm_z,
           pSkelMeshLod->mlod_aMorphMaps[immLast].mmp_aMorphMap[imwmLast].mwm_z);
    CHECKF(pSkelMeshLod->mlod_aMorphMaps[immCurent].mmp_aMorphMap[imwmCurent].mwm_nx,
           pSkelMeshLod->mlod_aMorphMaps[immLast].mmp_aMorphMap[imwmLast].mwm_nx);
    CHECKF(pSkelMeshLod->mlod_aMorphMaps[immCurent].mmp_aMorphMap[imwmCurent].mwm_ny,
           pSkelMeshLod->mlod_aMorphMaps[immLast].mmp_aMorphMap[imwmLast].mwm_ny);
    CHECKF(pSkelMeshLod->mlod_aMorphMaps[immCurent].mmp_aMorphMap[imwmCurent].mwm_nz,
           pSkelMeshLod->mlod_aMorphMaps[immLast].mmp_aMorphMap[imwmLast].mwm_nz);
  }
  return 0;
}

// Normalize weights in mlod
void CSkelMesh::NormalizeWeightsInLod(CSkelMeshLod &mlod)
{
#if 0
  TStaticArray<float> aWeightFactors;
  int ctvtx = mlod.mlod_aVertices.Count();
  int ctwm = mlod.mlod_aWeightMaps.Count();
  
  // Create array for weights
  aWeightFactors.New(ctvtx);
  memset(&aWeightFactors[0], 0, sizeof(aWeightFactors[0]) * ctvtx);
  int iwm = 0;
  for (; iwm < ctwm; iwm++)
  {
    MeshWeightMap &mwm = mlod.mlod_aWeightMaps[iwm];
    for (int iww = 0; iww < mwm.mwm_aVertexWeight.Count(); iww++)
    {
      MeshVertexWeight &mwh = mwm.mwm_aVertexWeight[iww];
      aWeightFactors[mwh.mww_iVertex] += mwh.mww_fWeight;
    }
  }

  for (iwm = 0; iwm < ctwm; iwm++)
  {
    MeshWeightMap &mwm = mlod.mlod_aWeightMaps[iwm];
    for (int iww = 0; iww < mwm.mwm_aVertexWeight.Count(); iww++)
    {
      MeshVertexWeight &mwh = mwm.mwm_aVertexWeight[iww];
      mwh.mww_fWeight /= aWeightFactors[mwh.mww_iVertex];
    }
  }
  
  // Clear weight array
  aWeightFactors.Clear();
#endif
}

// Normalize weights in mesh
void CSkelMesh::NormalizeWeights()
{
#if 0
  INDEX ctmlods = msh_aLods.Count();
  for (INDEX imlod = 0; imlod < ctmlods; imlod++)
  {
    // Normalize each lod
    NormalizeWeightsInLod(msh_aLods[imlod]);
  }
#endif
}

// Add new mesh lod to mesh
void CSkelMesh::AddMeshLod(CSkelMeshLod &mlod)
{
  INDEX ctmlods = msh_aLods.Count();
  msh_aLods.Expand(ctmlods + 1);
  msh_aLods[ctmlods] = mlod;
}

// Remove mesh lod from mesh
void CSkelMesh::RemoveMeshLod(CSkelMeshLod *pmlodRemove)
{
  INDEX ctmlod = msh_aLods.Count();
  // Create temp space for skeleton lods
  TStaticArray<class CSkelMeshLod> aTempMLODs;
  aTempMLODs.New(ctmlod - 1);
  INDEX iIndexSrc = 0;

  // For each skeleton lod in skeleton
  for (INDEX imlod = 0; imlod < ctmlod; imlod++)
  {
    CSkelMeshLod *pmlod = &msh_aLods[imlod];
    // Copy all skeleton lods except the selected one
    if (pmlod != pmlodRemove) {
      aTempMLODs[iIndexSrc] = *pmlod;
      iIndexSrc++;
    }
  }
  
  // Copy temp array of skeleton lods back in skeleton
  msh_aLods.CopyArray(aTempMLODs);
  
  // Clear temp skleletons lods array
  aTempMLODs.Clear();
}

// Clear mesh
void CSkelMesh::Clear(void)
{
  // For each LOD
  INDEX ctmlod = msh_aLods.Count();
  for (INDEX imlod = 0; imlod < ctmlod; imlod++)
  {
    // For each surface, clear the triangles list
    CSkelMeshLod &mlod = msh_aLods[imlod];
    INDEX ctsrf = mlod.mlod_aSurfaces.Count();
    for (INDEX isrf = 0; isrf < ctsrf; isrf++)
    {
      MeshSurface &msrf = mlod.mlod_aSurfaces[isrf];
      msrf.msrf_aTriangles.Clear();
      
      // Release shader form stock
      if (msrf.msrf_pShader != NULL) {
        _pResourceMgr->Release(msrf.msrf_pShader);
      }
      msrf.msrf_pShader = NULL;
    }
    
    // Clear the surfaces array
    mlod.mlod_aSurfaces.Clear();
    
    // For each uvmap, clear the texcord list
    INDEX ctuvm = mlod.mlod_aUVMaps.Count();
    for (INDEX iuvm = 0; iuvm < ctuvm; iuvm++)
    {
      mlod.mlod_aUVMaps[iuvm].muv_aTexCoords.Clear();
    }
    mlod.mlod_aUVMaps.Clear();    // Clear the uvmaps array
    mlod.mlod_aVertices.Clear();  // Clear the vertices array
    mlod.mlod_aNormals.Clear();   // Clear the normals array
  }
  
  // In the end, clear all LODs
  msh_aLods.Clear();
}

// Count used memory
SLONG CSkelMesh::GetUsedMemory(void)
{
  SLONG slMemoryUsed = sizeof(*this);
  INDEX ctmlods = msh_aLods.Count();
  for (INDEX imlod = 0; imlod < ctmlods; imlod++)
  {
    CSkelMeshLod &mlod = msh_aLods[imlod];
    slMemoryUsed += sizeof(mlod);
    slMemoryUsed += mlod.mlod_aVertices.Count() * sizeof(MeshVertex);
    slMemoryUsed += mlod.mlod_aNormals.Count() * sizeof(MeshNormal);
    slMemoryUsed += mlod.mlod_aVertexWeights.Count() * sizeof(MeshVertexWeightInfo);

    // For each uvmap
    INDEX ctuvmaps = mlod.mlod_aUVMaps.Count();
    for (INDEX iuvm = 0; iuvm < ctuvmaps; iuvm++)
    {
      MeshUVMap &uvmap = mlod.mlod_aUVMaps[iuvm];
      slMemoryUsed += sizeof(uvmap);
      slMemoryUsed += uvmap.muv_aTexCoords.Count() * sizeof(MeshTexCoord);
    }

    // For each surface
    INDEX ctmsrf = mlod.mlod_aSurfaces.Count();
    for (INDEX imsrf = 0; imsrf < ctmsrf; imsrf++)
    {
      MeshSurface &msrf = mlod.mlod_aSurfaces[imsrf];
      slMemoryUsed += sizeof(msrf);
      slMemoryUsed += msrf.msrf_aTriangles.Count() * sizeof(MeshTriangle);
      slMemoryUsed += sizeof(ShaderParams);
      slMemoryUsed += sizeof(INDEX) * msrf.msrf_ShadingParams.sp_aiTextureIDs.Count();
      slMemoryUsed += sizeof(INDEX) * msrf.msrf_ShadingParams.sp_aiTexCoordsIndex.Count();
      slMemoryUsed += sizeof(COLOR) * msrf.msrf_ShadingParams.sp_acolColors.Count();
      slMemoryUsed += sizeof(FLOAT) * msrf.msrf_ShadingParams.sp_afFloats.Count();
    }
    
    // For each weight map
    INDEX ctwm = mlod.mlod_aWeightMaps.Count();
    for (INDEX iwm = 0; iwm < ctwm; iwm++)
    {
      MeshWeightMap &mwm = mlod.mlod_aWeightMaps[iwm];
      slMemoryUsed += sizeof(mwm);
      slMemoryUsed += mwm.mwm_aVertexWeight.Count() * sizeof(MeshVertexWeight);
    }
    
    // For each morphmap
    INDEX ctmm = mlod.mlod_aMorphMaps.Count();
    for (INDEX imm = 0; imm < ctmm; imm++)
    {
      MeshMorphMap &mmm = mlod.mlod_aMorphMaps[imm];
      slMemoryUsed += sizeof(mmm);
      slMemoryUsed += mmm.mmp_aMorphMap.Count() * sizeof(MeshVertexMorph);
    }
  }
  return slMemoryUsed;
}

static INDEX FindMeshVertex(const CSkelMeshLod &mlod, const INDEX iFindVertex, const INDEX iFirstVertex)
{
  // ASSERT(iFindVertex<=iFirstVertex);
  const MeshVertex &mvxSearched = mlod.mlod_aVertices[iFindVertex];
  const INDEX ctVertices = mlod.mlod_aVertices.Count();

  for (INDEX iVtx = iFirstVertex; iVtx < ctVertices; iVtx++) 
  {
    const MeshVertex &mvx = mlod.mlod_aVertices[iVtx];

    if (mvx.x == mvxSearched.x && mvx.y == mvxSearched.y && mvx.z == mvxSearched.z) 
    {
      return iVtx;
    }
  }

  return -1; // vertex does not exists
}

static void ValidateWeights(CSkelMeshLod &mlod)
{
  const INDEX ctmvx = mlod.mlod_aVertices.Count();

  for (INDEX imvx = 0; imvx < ctmvx; imvx++) 
  {
    const MeshVertex &mvx = mlod.mlod_aVertices[imvx];
    const MeshVertexWeightInfo &mvwi = mlod.mlod_aVertexWeights[imvx];
    
    INDEX ivxFound = 0;
    INDEX ctRepeats = 0;

    while (TRUE) 
    {
      ivxFound = FindMeshVertex(mlod,imvx,ivxFound);

      if (ivxFound == (-1)) 
      {
        break;
      } else {
        const MeshVertex &mvxFound = mlod.mlod_aVertices[ivxFound];
        const MeshVertexWeightInfo &mvwiFound = mlod.mlod_aVertexWeights[ivxFound];

        for (INDEX i = 0; i < 4; i++) 
        {
          // ASSERT(mvwiFound.mvwi_aubIndices[i] == mvwi.mvwi_aubIndices[i]);
          ASSERT(mvwiFound.mvwi_aubWeights[i] == mvwi.mvwi_aubWeights[i]);
        }

        ivxFound++;
        ctRepeats++;
      }
    }

    // CWarningF("Vertex %d - %d repeates\n", imvx, ctRepeats);
  }
}

// returns surface relative weight map index if it exists, or adds new one to surface msrf_aubRelIndexTable array
static INDEX GetRelativeWeightIndex(CSkelMeshLod &mlod, MeshSurface &msrf, INDEX iAbsWeightIndex)
{
  // search all weight maps maped as relative indices in given surface
  const INDEX ctrmwm = msrf.msrf_aubRelIndexTable.Count();

  for (INDEX irmwm = 0; irmwm < ctrmwm; irmwm++) 
  {
    const INDEX imwm = msrf.msrf_aubRelIndexTable[irmwm];
    if (imwm == iAbsWeightIndex) 
    {
      return irmwm;
    }
  }

  /*
  if(msrf.msrf_aubRelIndexTable.Count() > MAX_WEIGHTS_PER_SURFACE) 
  {
    CPrintF("%s - Surface '%s' uses more than %d weights supported by hardware\n",
            mlod.mlod_fnSourceFile.ConstData(), ska_GetStringFromTable(msrf.msrf_iSurfaceID).ConstData(), MAX_WEIGHTS_PER_SURFACE);
  }
  */
  
  const INDEX ctItems = msrf.msrf_aubRelIndexTable.Count();
  msrf.msrf_aubRelIndexTable.Expand(ctItems + 1);
  UBYTE &ubNew = msrf.msrf_aubRelIndexTable[ctItems];
  ubNew = (UBYTE)iAbsWeightIndex;
  return ctItems;
  
  ASSERTALWAYS("Invalid weightmap index");
  CWarningF("Surface %s searching for invalid weightmap index %d\n", ska_GetStringFromTable(msrf.msrf_iSurfaceID).ConstData(), iAbsWeightIndex);
  return 0;
}

static void NormalizeWeightsInfosInLod(CSkelMeshLod &mlod)
{
  const BOOL bNormalizedWeights = mlod.GetFlags() & ML_NORMALIZED_WEIGHTS;
  const INDEX ctmvx = mlod.mlod_aVertexWeights.Count();

  for (INDEX imvx = 0;  imvx < ctmvx; imvx++) 
  {
    MeshVertexWeightInfo &mvwi = mlod.mlod_aVertexWeights[imvx];
    INDEX ctWeightsTotal = 0;
    INDEX iLastWeight = -1;

    for(INDEX i = 0; i < 4; i++) 
    {
      if(mvwi.mvwi_aubWeights[i] > 0) 
      {
        ctWeightsTotal += mvwi.mvwi_aubWeights[i];
        iLastWeight = i;
      }
    }

    if(iLastWeight >= 0) {
      INDEX iDiff = 255 - ctWeightsTotal;
      mvwi.mvwi_aubWeights[iLastWeight] += iDiff;
      ASSERT(mvwi.mvwi_aubWeights[0] + mvwi.mvwi_aubWeights[1] + 
        mvwi.mvwi_aubWeights[2] + mvwi.mvwi_aubWeights[3] == 255);
    } else {
      // ASSERT(FALSE);
    }
  }
}

// Fill vertex weights infos for lod (absolute weight map indices and weight factors).
static void FillVertexWeightInfos(CSkelMeshLod &mlod)
{
  // Fill vertex weight infos
  const INDEX ctVtx = mlod.mlod_aVertices.Count();
  const INDEX ctmwm = mlod.mlod_aWeightMaps.Count();
  TStaticArray<INDEX> actWeightsPerVertex;
  TStaticArray<VertexWeights> aVertexWeights;
  INDEX ctInvalidWeights = 0;
  
  ASSERT(ctVtx >= 0);

  if (ctmwm <= 0) {
    mlod.mlod_ulFlags |= ML_USE_VERTEX_PROGRAM;
    return;
  }
  
  // Indices must be relative!
  ASSERT(mlod.GetFlags() & ML_SURFACE_RELATIVE_INDICES);
  
  actWeightsPerVertex.New(ctVtx);
  aVertexWeights.New(ctVtx);

  // Reset all weights to 0
  memset(&aVertexWeights[0], 0, sizeof(VertexWeights) * ctVtx);
  // Reset all weight count to 0
  memset(&actWeightsPerVertex[0], 0, sizeof(INDEX) * ctVtx);
  
  // for each weight map in mesh lod
  for (INDEX imwm = 0; imwm < ctmwm; imwm++) 
  {
    const MeshWeightMap &mwm = mlod.mlod_aWeightMaps[imwm];

    // for each vertex weight in weight map
    const INDEX ctmvw = mwm.mwm_aVertexWeight.Count();

    for (INDEX imvw = 0; imvw < ctmvw; imvw++) 
    {
      const MeshVertexWeight &mvw = mwm.mwm_aVertexWeight[imvw];
      const INDEX imvx = mvw.mww_iVertex;
      const FLOAT fvw  = mvw.mww_fWeight;
      
      VertexWeights &vw = aVertexWeights[imvx];
      
      if (fvw > vw.vw_afWeights[3]) 
      {
        if (vw.vw_afWeights[3] > 0.0f) {
          ctInvalidWeights++;
        } else {
          actWeightsPerVertex[imvx]++;
        }

        ASSERT(actWeightsPerVertex[imvx] <= 4);
        vw.vw_afWeights[3] = fvw;
        vw.vw_aiIndices[3] = imwm;
      }

      if (fvw > vw.vw_afWeights[2]) {
        Swap(vw.vw_afWeights[3], vw.vw_afWeights[2]);
        Swap(vw.vw_aiIndices[3], vw.vw_aiIndices[2]);
      }

      if (fvw > vw.vw_afWeights[1]) {
        Swap(vw.vw_afWeights[2], vw.vw_afWeights[1]);
        Swap(vw.vw_aiIndices[2], vw.vw_aiIndices[1]);
      }

      if (fvw > vw.vw_afWeights[0]) {
        Swap(vw.vw_afWeights[1], vw.vw_afWeights[0]);
        Swap(vw.vw_aiIndices[1], vw.vw_aiIndices[0]);
      }

      ASSERT(vw.vw_afWeights[0] >= vw.vw_afWeights[1] &&
        vw.vw_afWeights[1] >= vw.vw_afWeights[2] &&
        vw.vw_afWeights[2] >= vw.vw_afWeights[3] && 
        vw.vw_afWeights[0] >= vw.vw_afWeights[3]);
    }
  }
  
  mlod.mlod_aVertexWeights.Clear();
  mlod.mlod_aVertexWeights.New(ctVtx);
  memset(&mlod.mlod_aVertexWeights[0], 0, sizeof(MeshVertexWeightInfo) * ctVtx);
  
  INDEX ctSurfaceReadyForVP = 0;
  
  const INDEX ctSurfaceInLod = mlod.mlod_aSurfaces.Count();

  // for each surface in mesh lod
  for (INDEX iSurface = 0; iSurface < ctSurfaceInLod; iSurface++ )
  {
    MeshSurface &msrf = mlod.mlod_aSurfaces[iSurface];
    msrf.msrf_aubRelIndexTable.Clear();
    
    // for each vertex in surface weight map
    const INDEX iFirstVertex = msrf.msrf_iFirstVertex;
    const INDEX ctVtx = msrf.msrf_ctVertices;

    // prepare vertex weight infos for hardware vertex shaders
    for (INDEX imvx = 0; imvx < ctVtx; imvx++) 
    {
      MeshVertexWeightInfo &mvwi = mlod.mlod_aVertexWeights[imvx + iFirstVertex];
      VertexWeights &vw = aVertexWeights[imvx + iFirstVertex];
      INDEX ctwpv = actWeightsPerVertex[imvx + iFirstVertex];
      ASSERT(ctwpv <= 4);

      FLOAT fTotalWeights = 0;
      INDEX iwpv;

      for (iwpv = 0; iwpv < ctwpv; iwpv++)
      {
        fTotalWeights += vw.vw_afWeights[iwpv];
      }

      for (iwpv = 0; iwpv < ctwpv; iwpv++)
      {
        ASSERT(vw.vw_aiIndices[iwpv] > 0 || vw.vw_afWeights[iwpv] > 0.0f);
        const INDEX iAbsWMapIndex = vw.vw_aiIndices[iwpv];
        const INDEX imwm = GetRelativeWeightIndex(mlod, msrf, iAbsWMapIndex);
        // ASSERT(msrf.msrf_aubRelIndexTable.Count() <= MAX_WEIGHTS_PER_SURFACE);

        // Normalize weights 
        FLOAT fmvw = vw.vw_afWeights[iwpv] / fTotalWeights;
        ASSERT(fmvw >= 0.0f && fmvw <= 1.0f);
        ASSERT(imwm <= 255);

        // clamp vertex factor to 1.0f
        UBYTE ubWeight = NormFloatToByte(fmvw);
        mvwi.mvwi_aubIndices[iwpv] = imwm;
        mvwi.mvwi_aubWeights[iwpv] = ubWeight;
      }

#if _DEBUG
      UBYTE ubLast = 255;
      SLONG slTotal = 0;

      for (iwpv = 0; iwpv < ctwpv; iwpv++ )
      {
        if  (mvwi.mvwi_aubWeights[iwpv] > 0) 
        {
          ASSERT(mvwi.mvwi_aubWeights[iwpv]<=ubLast);
          ubLast = mvwi.mvwi_aubWeights[iwpv];
          slTotal += ubLast;
        }
      }
#endif
    }

    const INDEX ctBonePerSurface = msrf.msrf_aubRelIndexTable.Count();

    if (ctBonePerSurface <= MAX_WEIGHTS_PER_SURFACE) {
      ctSurfaceReadyForVP++;
    } else {
      CWarningF("Bone count per surface is %d, this must be less or equal 25.", ctBonePerSurface);
    }
  }

  NormalizeWeightsInfosInLod(mlod);
  mlod.mlod_ulFlags |= ML_NORMALIZED_WEIGHTS;

#if _DEBUG
  ValidateWeights(mlod);
#endif

  if (ctSurfaceReadyForVP == ctSurfaceInLod) {
    mlod.mlod_ulFlags |= ML_USE_VERTEX_PROGRAM;
  }
}

UBYTE CSkelMesh::GetType() const
{
  return TYPE_SKELMESH;
}