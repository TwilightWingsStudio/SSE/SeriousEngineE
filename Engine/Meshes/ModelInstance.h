/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MODELINSTANCE_H
#define SE_INCL_MODELINSTANCE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Objects/GameObject.h>
#include <Engine/Meshes/AbstractModelInstance.h>
#include <Engine/Meshes/Skeleton.h>
#include <Engine/Meshes/BoneAnimSet.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Core/Math/AABBox.h>
#include <Core/Templates/StaticStackArray.h>
#include <Core/Templates/StaticArray.h>
#include <Core/Templates/DynamicContainer.h>
#include <Engine/Graphics/Shader.h>

// Numbers are same as in models.h
#define SKA_HEIGHT_EQ_WIDTH 0
#define SKA_LENGTH_EQ_WIDTH 1
#define SKA_LENGTH_EQ_HEIGHT 2

// Special assert for ska
#ifdef SKADEBUG
  #ifdef NDEBUG
    #define SKAASSERT(__ignore) ((void)0)
  #else
    #define SKAASSERT(expr) ASSERT(expr)
  #endif
#else
  #define SKAASSERT(__ignore) ((void)0)
#endif

//! Represents collision box that belongs to model.
struct CollisionBox
{
    CollisionBox() {};

    CollisionBox(FLOAT3D vMin,FLOAT3D vMax)
    {
      SetMin(vMin);
      SetMax(vMax);
  //    SetName("Default");
    };

    inline FLOAT3D &Min()
    {
      return cb_vMin;
    }

    inline FLOAT3D &Max()
    {
      return cb_vMax;
    }

    inline void SetMin(FLOAT3D &vMin)
    {
      cb_vMin = vMin;
    }

    inline void SetMax(FLOAT3D &vMax)
    {
      cb_vMax = vMax;
    }

    inline void SetName(CTString strName)
    {
      cb_strName = strName;
      cb_iBoxID  = SM_StringToId(cb_strName);
    }

    inline const CTString &GetName()
    {
      return cb_strName;
    }

    inline const INDEX GetID()
    {
      return cb_iBoxID;
    }

  private:
    FLOAT3D  cb_vMin;
    FLOAT3D  cb_vMax;
    CTString cb_strName;
    INDEX    cb_iBoxID;
};

/*
 * TODO: Add comment here
 */
struct MeshInstance
{
  // [SEE]
  MeshInstance()
  {
    mi_pMesh = NULL; // Initialize it with zero!
  }
  
  CSkelMesh *mi_pMesh;
  TStaticArray<struct TextureInstance> mi_tiTextures;
};

/*
 * TODO: Add comment here
 */
struct TextureInstance
{
    TextureInstance &operator=(const TextureInstance &tiOther)
    {
      ti_iTextureID = tiOther.ti_iTextureID;
      TextureInstance &ti = (TextureInstance&)tiOther;

      CTString strTexName = ti.ti_toTexture.GetName();
      ti_toTexture.SetData_t(strTexName);
      ti.ti_toTexture.SetData(NULL);
      return *this;
    }

    INDEX GetID()
    {
      return ti_iTextureID;
    }

    void  SetName(CTString &strTexID)
    {
      ti_iTextureID = SM_StringToId(strTexID);
    }

  public:
    CTextureObject ti_toTexture;

  private:
    INDEX ti_iTextureID;
};

/*
 * TODO: Add comment here
 */
struct AnimQueue
{
  TStaticStackArray<struct AnimList> aq_Lists;  // Array of currently playing anim lists
};

/*
 * TODO: Add comment here
 */
struct AnimList
{
  FLOAT al_fStartTime;  // Time when this list was created
  FLOAT al_fFadeTime;   // Time when this list will fade in
  TStaticStackArray<struct PlayedAnim> al_PlayedAnims;  // Array of currently playing anims in this list
};

/*
 * TODO: Add comment here
 */
struct PlayedAnim
{
  FLOAT pa_fStartTime; // Time when this animation was started
  FLOAT pa_fSpeedMul;  // Speed multiplier
  INDEX pa_iAnimID;    // Animation id
  ULONG pa_ulFlags;    // Animation flags
  FLOAT pa_Strength;   // Animation strength
  INDEX pa_GroupID;    // Group ID
};

//! Represents model that can perform on scene.
class ENGINE_API CModelInstance : public CAbstractModelInstance
{
  public:
    //! Constructor.
    CModelInstance();

    //! Destructor.
    virtual ~CModelInstance();

    //! Copy constructor.
    CModelInstance(CModelInstance &miOther);

    //! Clone.
    void operator=(CModelInstance &miOther);

    //! Add child model instance
    void AddChild(CModelInstance *pmi, INDEX iParentBoneID = -1);

    //! Add new mesh to model instance
    void AddMesh_t(CTFileName fnMesh);

    //! Add new skeleton to model instance
    void AddSkeleton_t(CTFileName fnSkeleton);

    //! Add new animset to model instance
    void AddAnimSet_t(CTFileName fnAnimSet);

    //! Add new texture to model instance
    void AddTexture_t(CTFileName fnTexture, CTString strTexID, MeshInstance *pmshi);

    //! Remove child model instance
    void RemoveChild(CModelInstance *pmi);

    //! Remove one texture from model instance
    void RemoveTexture(TextureInstance *ptiRemove,MeshInstance *pmshi);

    //! Find mesh instance in model instance
    MeshInstance    *FindMeshInstance(INDEX iMeshID);

    //! Find texture instance in all mesh instances in model instance
    TextureInstance *FindTexureInstance(INDEX iTexID);

    //! Find texture instance in given mesh instance
    TextureInstance *FindTexureInstance(INDEX iTexID, MeshInstance &mshi);

    //! Copy mesh instance for other model instance
    void CopyMeshInstance(CModelInstance &miOther);

    //! Get child of model instance
    CModelInstance *GetChild(INDEX iChildID, BOOL bRecursive = FALSE);

    //! Set parent bone of model instance
    void SetParentBone(INDEX iParentBoneID);

    //! Get parent of model instance
    CModelInstance *GetParent(CModelInstance *pmiStartFrom);

    //! Get first parent that does not reference its child model instance
    CModelInstance *GetFirstNonReferencedParent(CModelInstance *pmiRoot);

    //! Change parent of model instance
    void ChangeParent(CModelInstance *pmiOldParent, CModelInstance *pmiNewParent);

    //! Model instance offsets from parent model
    void SetOffset(FLOAT fOffset[6]);
    void SetOffsetRot(ANGLE3D aRot);
    void SetOffsetPos(FLOAT3D vPos);
    FLOAT3D GetOffsetPos();
    ANGLE3D GetOffsetRot();

    //! Stretch model instance.
    void StretchModel(const FLOAT3D &vStretch);

    //! Stretch model instance without attachments.
    void StretchSingleModel(const FLOAT3D &vStretch);

    //! Add new cloned anim state
    void NewClonedState(FLOAT fFadeTime);

    //! Add new clear anim state
    void NewClearState(FLOAT fFadeTime);

    //! Sets name of model instance
    void SetName(CTString strName);

    //! Gets name of model instance
    const CTString &GetName();

    //! Gets id of model instance
    const INDEX &GetID();

    //! Add animation to last anim queue
    void AddAnimation(INDEX iAnimID, DWORD dwFlags, FLOAT fStrength, INDEX iGroupID, FLOAT fSpeedMul = 1.0f);

    //! Remove all animations before last animation that has fully faded in
    void RemovePassedAnimsFromQueue(void);

    //! Remove animation from anim queue
    void RemAnimation(INDEX iAnimID);

    //! Remove all animations from anim queue with same ID
    void RemAnimsWithID(INDEX iGroupID);

    //! Stop all animations in anim queue
    void StopAllAnimations(FLOAT fFadeTime);

    //! Offset all animations in anim queue
    void OffSetAnimationQueue(TIME fOffsetTime);

    //! Find animation by ID
    BOOL FindAnimationByID(int iAnimID, INDEX *piAnimSetIndex, INDEX *piAnimIndex);

    //! Find first animation of all animations in ModelInstance (safety function)
    INDEX FindFirstAnimationID();

    //! Get animation length
    FLOAT GetAnimLength(INDEX iAnimID);

    //! Check if given animation is currently playing
    BOOL IsAnimationPlaying(INDEX iAnimID);

    //! Add flags to animation playing in anim queue
    BOOL AddFlagsToPlayingAnim(INDEX iAnimID, ULONG ulFlags);

    //! Returns model color.
    COLOR &GetModelColor(void);

    //! Changes model color.
    void SetModelColor(COLOR colNewColor);

    BOOL HasAlpha(void);
    BOOL HasShadow(FLOAT fMipFactor);
    BOOL IsModelVisible(FLOAT fMipFactor);
    void AddSimpleShadow(const FLOAT fIntensity, const FLOATplane3D &plShadowPlane);
    void GetModelVertices(TStaticStackArray<FLOAT3D> &avVertices, FLOATmatrix3D &mRotation, FLOAT3D &vPosition,
                          FLOAT fNormalOffset, FLOAT fDistance);

    //! Returns collision box by its index.
    CollisionBox &GetCollisionBox(INDEX icb);

    //! Returns active collision box.
    CollisionBox &GetCurrentCollisionBox();

    //! Returns active collision box.
    void GetCurrentCollisionBox(FLOATaabbox3D &paabbox);
    void GetAllFramesBBox(FLOATaabbox3D &aabbox);
    FLOAT3D GetCollisionBoxMin(ULONG ulCollisionBox = 0);
    FLOAT3D GetCollisionBoxMax(ULONG ulCollisionBox = 0);
    INDEX GetCollisionBoxDimensionEquality(ULONG ulCollisionBox = 0);
    INDEX GetCollisionBoxIndex(INDEX iBoxID);

    //! Add new collision box to model instance
    void AddCollisionBox(CTString strName,FLOAT3D vMin,FLOAT3D vMax);

    //! Remove collision box from model instance
    void RemoveCollisionBox(INDEX iIndex);

    //! Copy data from another instance of same class.
    virtual void Copy(CAbstractModelInstance &mi);

    //! Synchronize with another model (copy animations/attachments positions etc from there)
    void Synchronize(CModelInstance &miOther);

    //! Clear model instance
    void Clear();

    //! Flag for parser to remember source file names (used only in ska studio)
    static void EnableSrcRememberFN(BOOL bEnable);

    //! Count used memory
    SLONG GetUsedMemory(void);

    //! Returns instance type.
    virtual ModelInstanceType GetInstanceType() const
    {
      return MIT_SKELETAL;
    }

  public:
    CSkeleton *mi_psklSkeleton;                     // pointer to skeleton object
    TStaticArray<struct MeshInstance> mi_aMeshInst; // array of mesh instances
    TStaticArray<struct CollisionBox> mi_cbAABox;    // array of collision boxes
    TDynamicContainer<class CBoneAnimSet> mi_aAnimSet;  // array of animsets
    TDynamicContainer<class CModelInstance> mi_cmiChildren; // array of child model instances
    class CModelConfiguration *mi_pInStock; // [SEE]

    AnimQueue mi_aqAnims;   // current animation queue for this model instance
    QVect mi_qvOffset;      // current offset from parent model instance
    INDEX mi_iParentBoneID; // ID of parent bone in parent model instance
    INDEX mi_iCurentBBox;   // index of current collision box in collision box array
    COLOR mi_colModelColor; // color of this model instance
    FLOAT3D mi_vStretch;    // stretch of this model instance
    CollisionBox mi_cbAllFramesBBox; // all frames collision box
    CTFileName mi_fnSourceFile;     // source file name of this model instance (used only for ska studio)

  private:
    INDEX mi_iModelID;      // ID of this model instance (this is ID for mi_strName)
    CTString mi_strName;    // name of this model instance
};

//! Parse smc file in existing model instance
ENGINE_API void ParseSmcFile_t(CModelInstance &mi, const CTString &fnSmcFile);

//! Create model instance and read from a config file into it
ENGINE_API CModelInstance *LoadModelInstance_t(const CTFileName &fnConfigFile);

//! Create empty model instance
ENGINE_API CModelInstance *CreateModelInstance(CTString strName);

//! Delete model instance
ENGINE_API void DeleteModelInstance(CModelInstance *pmi);

//! Calculate fading factor for animation list
ENGINE_API FLOAT CalculateFadeFactor(AnimList &alList);

// read/write a ska model from a file
void WriteModelInstance_t(CTStream &strm, CModelInstance &mi, BOOL bFromStock=TRUE);
void ReadModelInstance_t(CTStream &strm, CModelInstance &mi, BOOL bMarkInStock=TRUE);
void SkipModelInstance_t(CTStream &strm);

#endif  /* include-once check. */