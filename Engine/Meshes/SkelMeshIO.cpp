/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include "SkelMesh.h"

#define MESH_VERSION_OLD  12
#define MESH_VERSION_CURRENT 16
#define MESH_ID       "MESH"

#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Engine/Graphics/Shader.h>
#include <Core/Templates/StaticArray.h>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Resources/ChaosCipher.h>
#include <Engine/Resources/ResourceManager.h>

static void ReadShaderParams_t(MeshSurface &msrf, CTStream *istrFile, INDEX iFileVersion)
{
  INDEX cttx,cttc,ctcol,ctfl;
  
  (*istrFile) >> cttx;                                                // Read texture count
  (*istrFile) >> cttc;                                                // Read texture coords count
  (*istrFile) >> ctcol;                                               // Read color count
  (*istrFile) >> ctfl;                                                // Read float count

  CShaderClass *pshMeshShader = NULL;
  ShaderParams *pshpShaderParams = NULL;
  CShaderClass shDummyShader;            // dummy shader if shader is not found
  ShaderParams shpDummyShaderParams;// dummy shader params if shader is not found
  
  CTString strShaderName;
  (*istrFile) >> strShaderName;                                       // Read shader name
  
  // Try to load shader
  try{
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SHADER, strShaderName);
    msrf.msrf_pShader = static_cast<CShaderClass *>(pser);
    pshMeshShader = msrf.msrf_pShader;
    pshpShaderParams = &msrf.msrf_ShadingParams;
  } catch(char *strErr) {
    CErrorF("%s\n",strErr);
    msrf.msrf_pShader = NULL;
    pshMeshShader = &shDummyShader;
    pshpShaderParams = &shpDummyShaderParams;
  }

  // If mesh shader exisits
  if (msrf.msrf_pShader != NULL)
  {
    // Get shader description
    ShaderDesc shDesc;
    msrf.msrf_pShader->GetShaderDesc(shDesc);
    
    // Check if saved params count match shader params count
    if (shDesc.sd_astrTextureNames.Count() > cttx) {
      ThrowF_t("File '%s'\nWrong texture count %d", istrFile->GetDescription().ConstData(), cttx);
    }
    
    if (shDesc.sd_astrTexCoordNames.Count() > cttc) {
      ThrowF_t("File '%s'\nWrong uvmaps count %d", istrFile->GetDescription().ConstData(), cttc);
    }
    
    if (shDesc.sd_astrColorNames.Count() > ctcol) {
      ThrowF_t("File '%s'\nWrong colors count %d", istrFile->GetDescription().ConstData(), ctcol);
    }
    
    if (shDesc.sd_astrFloatNames.Count() > ctfl) {
      ThrowF_t("File '%s'\nWrong floats count %d", istrFile->GetDescription().ConstData(), ctfl);
    }
  }

  // Create arrays for shader params
  pshpShaderParams->sp_aiTextureIDs.New(cttx);
  pshpShaderParams->sp_aiTexCoordsIndex.New(cttc);
  pshpShaderParams->sp_acolColors.New(ctcol);
  pshpShaderParams->sp_afFloats.New(ctfl);

  // Read shader texture IDs
  for (INDEX itx = 0; itx < cttx; itx++)
  {
    CTString strTexID;
    (*istrFile) >> strTexID;                                          // Read shader texture ID
    INDEX iTexID = SM_StringToId(strTexID);
     pshpShaderParams->sp_aiTextureIDs[itx] = iTexID;
  }
  
  // Read shader texture coords indices
  for (INDEX itc = 0; itc < cttc; itc++)
  {
    INDEX iTexCoorsIndex;
    (*istrFile) >> iTexCoorsIndex;                                    // Read shader texture coords ID
    pshpShaderParams->sp_aiTexCoordsIndex[itc] = iTexCoorsIndex;
  }
  
  // Read shader colors
  for (INDEX icol = 0; icol < ctcol; icol++)
  {
    COLOR colColor;
    (*istrFile) >> colColor;                                          // Read shader color
    pshpShaderParams->sp_acolColors[icol] = colColor;
  }
  
  // Read shader floats
  for (INDEX ifl = 0; ifl < ctfl; ifl++)
  {
    FLOAT fFloat;
    (*istrFile) >> fFloat;                                            // Read shader float
    pshpShaderParams->sp_afFloats[ifl] = fFloat;
  }
  
  // There were no flags in shader before ver 12
  if (iFileVersion > 11) {
    ULONG ulFlags;
    (*istrFile) >> ulFlags;
    pshpShaderParams->sp_ulFlags = ulFlags;
  } else {
    pshpShaderParams->sp_ulFlags = 0;
  }
}

static void ReadShaderParams17_t(MeshSurface &msrf, CTStream *istrFile, CChaosMeshCipher &cipher)
{
  INDEX cttx,cttc,ctcol,ctfl;
  
  (*istrFile) >> ctcol;                                               // Read color count
  (*istrFile) >> ctfl;                                                // Read float count
  (*istrFile) >> cttx;                                                // Read texture count
  (*istrFile) >> cttc;                                                // Read texture coords count
  
  ctcol = cipher.Decode(ctcol);
  ctfl = cipher.Decode(ctfl);
  cttx = cipher.Decode(cttx);
  cttc = cipher.Decode(cttc);

  CShaderClass *pshMeshShader = NULL;
  ShaderParams *pshpShaderParams = NULL;
  CShaderClass shDummyShader;            // dummy shader if shader is not found
  ShaderParams shpDummyShaderParams;// dummy shader params if shader is not found
  
  CTString strShaderName;
  (*istrFile) >> strShaderName;                                       // Read shader name
  
  // Try to load shader
  try{
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SHADER, strShaderName);
    msrf.msrf_pShader = static_cast<CShaderClass *>(pser);
    pshMeshShader = msrf.msrf_pShader;
    pshpShaderParams = &msrf.msrf_ShadingParams;
  } catch(char *strErr) {
    CErrorF("%s\n",strErr);
    msrf.msrf_pShader = NULL;
    pshMeshShader = &shDummyShader;
    pshpShaderParams = &shpDummyShaderParams;
  }

  // If mesh shader exisits
  if (msrf.msrf_pShader != NULL)
  {
    // Get shader description
    ShaderDesc shDesc;
    msrf.msrf_pShader->GetShaderDesc(shDesc);
    
    // Check if saved params count match shader params count
    if (shDesc.sd_astrTextureNames.Count() > cttx) {
      ThrowF_t("File '%s'\nWrong texture count %d", istrFile->GetDescription().ConstData(), cttx);
    }
    
    if (shDesc.sd_astrTexCoordNames.Count() > cttc) {
      ThrowF_t("File '%s'\nWrong uvmaps count %d", istrFile->GetDescription().ConstData(), cttc);
    }
    
    if (shDesc.sd_astrColorNames.Count() > ctcol) {
      ThrowF_t("File '%s'\nWrong colors count %d", istrFile->GetDescription().ConstData(), ctcol);
    }
    
    if (shDesc.sd_astrFloatNames.Count() > ctfl) {
      ThrowF_t("File '%s'\nWrong floats count %d", istrFile->GetDescription().ConstData(), ctfl);
    }
  }

  // Create arrays for shader params
  pshpShaderParams->sp_aiTextureIDs.New(cttx);
  pshpShaderParams->sp_aiTexCoordsIndex.New(cttc);
  pshpShaderParams->sp_acolColors.New(ctcol);
  pshpShaderParams->sp_afFloats.New(ctfl);

  // Read shader texture IDs
  for (INDEX itx = 0; itx < cttx; itx++)
  {
    CTString strTexID;
    (*istrFile) >> strTexID;                                          // Read shader texture ID
    INDEX iTexID = SM_StringToId(strTexID);
     pshpShaderParams->sp_aiTextureIDs[itx] = iTexID;
  }

  ULONG ulFlags;
  (*istrFile) >> ulFlags;
  ulFlags = cipher.Decode(ulFlags);
  pshpShaderParams->sp_ulFlags = ulFlags;
  
    // Read shader colors
  for (INDEX icol = 0; icol < ctcol; icol++)
  {
    COLOR colColor;
    (*istrFile) >> colColor;                                          // Read shader color
    colColor = cipher.Decode(colColor);
    pshpShaderParams->sp_acolColors[icol] = colColor;
  }
  
  // Read shader floats
  for (INDEX ifl = 0; ifl < ctfl; ifl++)
  {
    FLOAT fFloat;
    (*istrFile) >> fFloat;                                            // Read shader float
    pshpShaderParams->sp_afFloats[ifl] = fFloat;
  }
  
  // Read shader texture coords indices
  for (INDEX itc = 0; itc < cttc; itc++)
  {
    INDEX iTexCoorsIndex;
    (*istrFile) >> iTexCoorsIndex;                                    // Read shader texture coords ID
    iTexCoorsIndex = cipher.Decode(iTexCoorsIndex);
    pshpShaderParams->sp_aiTexCoordsIndex[itc] = iTexCoorsIndex;
  }
}

static void WriteShaderParams_t(const MeshSurface &mSrf, CTStream *ostrFile)
{
  ASSERT(mSrf.msrf_pShader->GetShaderDesc != NULL);
  
  ShaderDesc shDesc;
  mSrf.msrf_pShader->GetShaderDesc(shDesc);
  const INDEX ctShaTex = shDesc.sd_astrTextureNames.Count();
  const INDEX ctShaUV = shDesc.sd_astrTexCoordNames.Count();
  const INDEX ctShaCol = shDesc.sd_astrColorNames.Count();
  const INDEX ctShaFloat = shDesc.sd_astrFloatNames.Count();
  
  ASSERT(ctShaTex <= mSrf.msrf_ShadingParams.sp_aiTextureIDs.Count());
  ASSERT(ctShaUV <= mSrf.msrf_ShadingParams.sp_aiTexCoordsIndex.Count());
  ASSERT(ctShaCol <= mSrf.msrf_ShadingParams.sp_acolColors.Count());
  ASSERT(ctShaFloat <= mSrf.msrf_ShadingParams.sp_afFloats.Count());
  
  (*ostrFile) << ctShaTex;                                                // Write texture count 
  (*ostrFile) << ctShaUV;                                                // Write texture coords count 
  (*ostrFile) << ctShaCol;                                               // Write color count 
  (*ostrFile) << ctShaFloat;                                                // Write float count 
  
  CTString strShaderName;
  strShaderName = mSrf.msrf_pShader->GetName();
  (*ostrFile) << strShaderName;
  
  // Write shader texture IDs
  for (INDEX itx = 0; itx < ctShaTex; itx++)
  {
    const INDEX iTexID = mSrf.msrf_ShadingParams.sp_aiTextureIDs[itx];
    (*ostrFile) << SM_IdToString(iTexID);
  }
  
  // Write shader texture coords indices
  for (INDEX itc = 0; itc < ctShaUV; itc++)
  {
    const INDEX iTexCoorsIndex = mSrf.msrf_ShadingParams.sp_aiTexCoordsIndex[itc];
    (*ostrFile) << iTexCoorsIndex;
  }
  
  // Write shader colors
  for (INDEX icol = 0; icol < ctShaCol; icol++)
  {
    const COLOR colColor = mSrf.msrf_ShadingParams.sp_acolColors[icol];
    (*ostrFile) << colColor;
  }
  
  // Write shader floats
  for (INDEX ifl = 0; ifl < ctShaFloat; ifl++)
  {
    const FLOAT fFloat = mSrf.msrf_ShadingParams.sp_afFloats[ifl];
    (*ostrFile) << fFloat;
  }
  
  const ULONG ulFlags = mSrf.msrf_ShadingParams.sp_ulFlags;
  (*ostrFile) << ulFlags;
}

static void ReadMeshVersion12_t(CSkelMesh *pMesh, CTStream *istrFile)
{
  INDEX iFileVersion = pMesh->msh_iVersion;
  
  INDEX ctmlods;
  
  (*istrFile) >> ctmlods;                                                   // Read mlod count
  
  // For each lod in mesh
  for (INDEX imlod = 0; imlod < ctmlods; imlod++)
  {
    // expand mlod count for one 
    INDEX ctSkelMeshLods = pMesh->msh_aLods.Count();
    pMesh->msh_aLods.Expand(ctSkelMeshLods + 1);
    CSkelMeshLod &mLod = pMesh->msh_aLods[ctSkelMeshLods];

    INDEX ctVtx;   // vertex count
    INDEX ctUV;   // uvmaps count
    INDEX ctSf;   // surfaces count
    INDEX ctWM;   // weight maps count
    INDEX ctMM;   // morph maps count

    (*istrFile) >> mLod.mlod_fnSourceFile;                                  // Read source file name
    (*istrFile) >> mLod.mlod_fMaxDistance;                                  // Read max distance
    (*istrFile) >> mLod.mlod_ulFlags;                                       // Read flags

    // :)
    if (iFileVersion <= 11) {
      mLod.mlod_ulFlags = 0;
    }
    
    if (mLod.mlod_ulFlags == 0xCDCDCDCD) {
      mLod.mlod_ulFlags = 0;
    }

    (*istrFile) >> ctVtx;                                                    // Read vertex count
    
    // Create vertex and normal arrays
    mLod.mlod_aVertices.New(ctVtx);
    mLod.mlod_aNormals.New(ctVtx);
    
    // Read vertices
    {
      // Vertices saved in 16-byte where 4 bytes just trash.
      struct MeshVertexOld *pVtxBuffer = new MeshVertexOld[ctVtx];
      
      // We load it to buffer and then put into new MeshVertex structures.
      istrFile->Read_t(pVtxBuffer, sizeof(MeshVertexOld) * ctVtx);
      
      // Copy them.
      for (INDEX i = 0; i < ctVtx; i++)
      {
        mLod.mlod_aVertices[i].x = pVtxBuffer[i].x;
        mLod.mlod_aVertices[i].y = pVtxBuffer[i].y;
        mLod.mlod_aVertices[i].z = pVtxBuffer[i].z;
      }

      delete pVtxBuffer;
    }
    
    // Read normals
    {
      // Normals saved in (4 * 4 bytes) where last 4 bytes just trash.
      struct MeshNormalOld *pNormalBuffer = new MeshNormalOld[ctVtx];
      
      // We load it to buffer and then covert into MeshNormal (3 * 4 bytes).
      istrFile->Read_t(pNormalBuffer, sizeof(MeshNormalOld) * ctVtx);
      
      // Copy them.
      for (INDEX i = 0; i < ctVtx; i++)
      {
        mLod.mlod_aNormals[i].nx = pNormalBuffer[i].nx;
        mLod.mlod_aNormals[i].ny = pNormalBuffer[i].ny;
        mLod.mlod_aNormals[i].nz = pNormalBuffer[i].nz;
      }

      delete pNormalBuffer;
    }

    (*istrFile) >> ctUV;                                                    // Read uvmaps count
    
    // Create array for uvmaps
    mLod.mlod_aUVMaps.New(ctUV);
    
    // Read uvmaps
    for (int iuv = 0; iuv < ctUV; iuv++)
    {
      CTString strNameID;
      (*istrFile) >> strNameID;                                             // Read uvmap ID
      mLod.mlod_aUVMaps[iuv].muv_iID = SM_StringToId(strNameID);
      
      // Create array for uvmaps texcordinates
      mLod.mlod_aUVMaps[iuv].muv_aTexCoords.New(ctVtx);
      istrFile->Read_t(&mLod.mlod_aUVMaps[iuv].muv_aTexCoords[0],
                        sizeof(MeshTexCoord) * ctVtx);                       // Read uvmap texcordinates
    }
    
    (*istrFile) >> ctSf;                                                    // Read surfaces count
    
    // Create array for surfaces
    mLod.mlod_aSurfaces.New(ctSf);
    
    // Read surfaces
    for (INDEX isf = 0; isf < ctSf; isf++)
    {
      INDEX ctTris;
      MeshSurface &msrf = mLod.mlod_aSurfaces[isf];
      msrf.msrf_ulFlags = 0; // NOTE: That is not written to old meshes!

      CTString strSurfaceID;
      (*istrFile) >> strSurfaceID;                                          // Read surface ID
      msrf.msrf_iSurfaceID = SM_StringToId(strSurfaceID);
      (*istrFile) >> msrf.msrf_iFirstVertex;                                // Read first vertex
      (*istrFile) >> msrf.msrf_ctVertices;                                  // Read vertices count
      (*istrFile) >> ctTris;                                                // Read tris count

      // Create triangles array
      mLod.mlod_aSurfaces[isf].msrf_aTriangles.New(ctTris);
      istrFile->Read_t(&mLod.mlod_aSurfaces[isf].msrf_aTriangles[0],
                       sizeof(MeshTriangle) * ctTris);                      // Read triangles

      INDEX bShaderExists;
      (*istrFile) >> bShaderExists;                                         // Read bool that this surface has a shader

      // If shader exists read its params
      if (bShaderExists) {
        ReadShaderParams_t(msrf, istrFile, iFileVersion);
      } else {
        // This surface does not have shader
        msrf.msrf_pShader = NULL;
      }
    }

    (*istrFile) >> ctWM;                                                    // Read weightmaps count
    
    // Create weightmap array
     mLod.mlod_aWeightMaps.New(ctWM);
     
     // Read each weightmap
    for (INDEX iwm = 0; iwm < ctWM; iwm++)
    {
      CTString pstrNameID;
      (*istrFile) >> pstrNameID;                                            // Read weightmap ID
      mLod.mlod_aWeightMaps[iwm].mwm_iID = SM_StringToId(pstrNameID);

      INDEX ctWw;
      (*istrFile) >> ctWw;                                                  // Read wertex weight count
      
      // Create wertex weight array
      mLod.mlod_aWeightMaps[iwm].mwm_aVertexWeight.New(ctWw);
      istrFile->Read_t(&mLod.mlod_aWeightMaps[iwm].mwm_aVertexWeight[0],
                       sizeof(MeshVertexWeight) * ctWw);                    // Read wertex weights
    }

    (*istrFile) >> ctMM;                                                    // Read morphmap count
    
    // Create morphmaps array
    mLod.mlod_aMorphMaps.New(ctMM);
    
    // Read morphmaps
    for (INDEX iMM = 0; iMM < ctMM; iMM++)
    {
      MeshMorphMap &mMap = mLod.mlod_aMorphMaps[iMM];

      CTString pstrNameID;
      (*istrFile) >> pstrNameID; // Read morphmap ID
      mMap.mmp_iID = SM_StringToId(pstrNameID);
      (*istrFile) >> mMap.mmp_bRelative; // Read bRelative

      // Read morph sets count
      INDEX ctMorphSets;
      (*istrFile) >> ctMorphSets;
      
      // Create morps sets array
      mMap.mmp_aMorphMap.New(ctMorphSets);
      
      struct MeshVertexMorphOld *pMorphSets = new MeshVertexMorphOld[ctMorphSets];
      
      istrFile->Read_t(&pMorphSets[0], sizeof(MeshVertexMorphOld) * ctMorphSets);                  // Read morph sets
    
      for (INDEX i = 0; i < ctMorphSets; i++)
      {
        memcpy((char *)&mMap.mmp_aMorphMap[i], (char *)&pMorphSets[i], sizeof(MeshVertexMorph));
      }
      
      delete pMorphSets;
    }
  }
}

// [SEE] SkaMesh BM v16 Loader
static void ReadMeshVersion16_t(CSkelMesh *pMesh, CTStream *istrFile)
{
  INDEX iFileVersion = pMesh->msh_iVersion;
  
  SLONG slMeshSize = 0;
  INDEX ctLod;

  // Read size of mesh.
  (*istrFile) >> slMeshSize;  
  (*istrFile) >> ctLod;    
  
  // Allocate new array for LODs.
  if (ctLod > 0) {
    pMesh->msh_aLods.New(ctLod);
  }
  
  // For each LOD...
  for (INDEX iLod = 0; iLod < ctLod; iLod++)
  {
    INDEX ctVtx, ctUV, ctSrf, ctWM, ctMM, ctWI;

    CSkelMeshLod &mLod = pMesh->msh_aLods[iLod];

    // Mesh LOD summary.
    (*istrFile) >> ctVtx; // Vertices.
    (*istrFile) >> ctUV;  // UV Maps.
    (*istrFile) >> ctSrf; // Surfaces.
    (*istrFile) >> ctWM;  // Weight Maps.
    (*istrFile) >> ctMM;  // Morph Maps.
    (*istrFile) >> ctWI;  // Weight Infos.

    (*istrFile) >> mLod.mlod_fnSourceFile; // Read source file name
    (*istrFile) >> mLod.mlod_fMaxDistance; // Read max distance
    (*istrFile) >> mLod.mlod_ulFlags;      // Read flags
    
    // Create vertex and normal arrays
    mLod.mlod_aVertices.New(ctVtx);
    mLod.mlod_aNormals.New(ctVtx);

    // Load vertices and normals.
    istrFile->Read_t(&mLod.mlod_aVertices[0], sizeof(MeshVertex) * ctVtx);
    istrFile->Read_t(&mLod.mlod_aNormals[0], sizeof(MeshNormal) * ctVtx);

    // Load UV maps.
    {
      mLod.mlod_aUVMaps.New(ctUV); // Create array of UV maps.

      // For each UV map...
      for (INDEX iUV = 0; iUV < ctUV; iUV++)
      {
        MeshUVMap &mUV = mLod.mlod_aUVMaps[iUV];
        
        // Read UV identifier.
        CTString strNameID;
        (*istrFile) >> strNameID;
        mLod.mlod_aUVMaps[iUV].muv_iID = SM_StringToId(strNameID);
        
        // Create array for uvmaps tex coords.
        mUV.muv_aTexCoords.New(ctVtx);
        istrFile->Read_t(&mUV.muv_aTexCoords[0], sizeof(MeshTexCoord) * ctVtx);
      }
    }
    
    {
      // Create array of surfaces.
      mLod.mlod_aSurfaces.New(ctSrf);
      
      for (INDEX iSrf = 0; iSrf < ctSrf; iSrf++)
      {
        INDEX ctTris;
        CTString strSurfaceID;
        
        MeshSurface &mSrf = mLod.mlod_aSurfaces[iSrf];
        
        // Read surface data.
        (*istrFile) >> strSurfaceID;           // Read surface ID
        (*istrFile) >> mSrf.msrf_ulFlags;      // Read surface flags
        (*istrFile) >> mSrf.msrf_iFirstVertex; // Read first vertex
        (*istrFile) >> mSrf.msrf_ctVertices;   // Read vertices count
        (*istrFile) >> ctTris;                 // Read tris count

        mSrf.msrf_iSurfaceID = SM_StringToId(strSurfaceID);
        
        // Create triangles array.
        mSrf.msrf_aTriangles.New(ctTris);
        
        struct MeshTriangle16 *pTriangles = new MeshTriangle16[ctTris];

        // Read triangles.
        istrFile->Read_t(&pTriangles[0], sizeof(MeshTriangle16) * ctTris);
        
        for (INDEX i = 0; i < ctTris; i++) 
        {
          mSrf.msrf_aTriangles[i].iVertex[0] = pTriangles[i].iVertex[0];
          mSrf.msrf_aTriangles[i].iVertex[1] = pTriangles[i].iVertex[1];
          mSrf.msrf_aTriangles[i].iVertex[2] = pTriangles[i].iVertex[2];
        }
        
        delete pTriangles;
        
        // Read table with Relative Weight Map Indices.
        INDEX ctRWMI;
        (*istrFile) >> ctRWMI;
        
        if (ctRWMI > 0) {
          mSrf.msrf_aubRelIndexTable.New(ctRWMI);
          istrFile->Read_t(&mSrf.msrf_aubRelIndexTable[0], sizeof(UBYTE) * ctRWMI);
        }

        INDEX bShaderExists;
        (*istrFile) >> bShaderExists;                                         // Read bool that this surface has a shader
        
        // TODO: Split this code that loads shader params.
        
        // If shader exists read its params
        if (bShaderExists) {
          ReadShaderParams_t(mSrf, istrFile, iFileVersion);
        } else {
          // This surface does not have shader
          mSrf.msrf_pShader = NULL;
        }
      }
    }
    
    // Weight Maps.
    {
     mLod.mlod_aWeightMaps.New(ctWM);
     
       // Read each weightmap
      for (INDEX iwm = 0; iwm < ctWM; iwm++)
      {
        CTString strNameID;
        INDEX ctWw;
        
        (*istrFile) >> strNameID;                                            // Read weightmap ID
        (*istrFile) >> ctWw;                                                  // Read wertex weight count

        mLod.mlod_aWeightMaps[iwm].mwm_iID = SM_StringToId(strNameID);
        
        // Create wertex weight array
        mLod.mlod_aWeightMaps[iwm].mwm_aVertexWeight.New(ctWw);
        istrFile->Read_t(&mLod.mlod_aWeightMaps[iwm].mwm_aVertexWeight[0],
                         sizeof(MeshVertexWeight) * ctWw);                    // Read wertex weights
      }
    }
    
    // Morph Maps.
    {
      mLod.mlod_aMorphMaps.New(ctMM);
      
      for (INDEX iMM = 0; iMM < ctMM; iMM++)
      {
        CTString strNameID;
        INDEX ctMorphSets;
        
        MeshMorphMap &mMap = mLod.mlod_aMorphMaps[iMM];
        
        (*istrFile) >> strNameID; 
        (*istrFile) >> mMap.mmp_bRelative; // Read bRelative
        (*istrFile) >> ctMorphSets;
      
        mLod.mlod_aMorphMaps[iMM].mmp_iID = SM_StringToId(strNameID);
        
        // create morps sets array
        mMap.mmp_aMorphMap.New(ctMorphSets);
        istrFile->Read_t(&mMap.mmp_aMorphMap[0], sizeof(MeshVertexMorph) * ctMorphSets);
      }
    }
    
    // Weight Infos.
    if (ctWI) {
      mLod.mlod_aVertexWeights.New(ctWI);
      istrFile->Read_t(&mLod.mlod_aVertexWeights[0], sizeof(MeshVertexWeightInfo) * ctWI); // TODO: These guys aren't used. Solve it!
    }
  } // for each load
}

// [SEE] SkaMesh BM v17 (LastChaos) Loader
static void ReadMeshVersion17_t(CSkelMesh *pMesh, CTStream *istrFile)
{
  CChaosMeshCipher cipher(17);
  
  INDEX iFileVersion = pMesh->msh_iVersion;
  
  SLONG slMeshSize = 0;
  INDEX ctLod;

  // Read size of mesh.
  (*istrFile) >> slMeshSize;  
  (*istrFile) >> ctLod;
  ctLod = cipher.Decode(ctLod);
  
  // Allocate new array for LODs.
  if (ctLod > 0) {
    pMesh->msh_aLods.New(ctLod);
  }
  
  // For each LOD...
  for (INDEX iLod = 0; iLod < ctLod; iLod++)
  {
    INDEX ctVtx, ctUV, ctSrf, ctWM, ctMM, ctWI;

    CSkelMeshLod &mLod = pMesh->msh_aLods[iLod];

    // Mesh LOD summary.
    (*istrFile) >> ctWI;  // Weight Infos.
    (*istrFile) >> ctWM;  // Weight Maps.
    (*istrFile) >> ctUV;  // UV Maps.
    (*istrFile) >> ctVtx; // Vertices.
    (*istrFile) >> ctSrf; // Surfaces.
    (*istrFile) >> ctMM;  // Morph Maps.
    
    ctWI = cipher.Decode(ctWI);
    ctWM = cipher.Decode(ctWM);
    ctUV = cipher.Decode(ctUV);
    ctVtx = cipher.Decode(ctVtx);
    ctSrf = cipher.Decode(ctSrf);
    ctMM = cipher.Decode(ctMM);

    (*istrFile) >> mLod.mlod_fnSourceFile; // Read source file name
    (*istrFile) >> mLod.mlod_fMaxDistance; // Read max distance
    (*istrFile) >> mLod.mlod_ulFlags;      // Read flags

    mLod.mlod_ulFlags = cipher.Decode(mLod.mlod_ulFlags);
    
    // Create vertex and normal arrays
    mLod.mlod_aVertices.New(ctVtx);
    mLod.mlod_aNormals.New(ctVtx);

    // Load vertices and normals.
    istrFile->Read_t(&mLod.mlod_aVertices[0], sizeof(MeshVertex) * ctVtx);
    istrFile->Read_t(&mLod.mlod_aNormals[0], sizeof(MeshNormal) * ctVtx);

    // Load UV maps.
    {
      mLod.mlod_aUVMaps.New(ctUV); // Create array of UV maps.

      // For each UV map...
      for (INDEX iUV = 0; iUV < ctUV; iUV++)
      {
        MeshUVMap &mUV = mLod.mlod_aUVMaps[iUV];
        
        // Read UV identifier.
        CTString strNameID;
        (*istrFile) >> strNameID;
        mLod.mlod_aUVMaps[iUV].muv_iID = SM_StringToId(strNameID);
        
        // Create array for uvmaps tex coords.
        mUV.muv_aTexCoords.New(ctVtx);
        istrFile->Read_t(&mUV.muv_aTexCoords[0], sizeof(MeshTexCoord) * ctVtx);
      }
    }
    
    {
      // Create array of surfaces.
      mLod.mlod_aSurfaces.New(ctSrf);
      
      for (INDEX iSrf = 0; iSrf < ctSrf; iSrf++)
      {
        INDEX ctTris;
        CTString strSurfaceID;
        
        MeshSurface &mSrf = mLod.mlod_aSurfaces[iSrf];
        
        // Read surface data.
        (*istrFile) >> mSrf.msrf_iFirstVertex; // Read first vertex
        (*istrFile) >> mSrf.msrf_ctVertices;   // Read vertices count
        (*istrFile) >> ctTris;                 // Read tris count
        
        mSrf.msrf_iFirstVertex = cipher.Decode(mSrf.msrf_iFirstVertex);
        mSrf.msrf_ctVertices = cipher.Decode(mSrf.msrf_ctVertices);
        ctTris = cipher.Decode(ctTris);
        
        // Create triangles array.
        mSrf.msrf_aTriangles.New(ctTris);
        
        struct MeshTriangle16 *pTriangles = new MeshTriangle16[ctTris];

        // Read triangles.
        istrFile->Read_t(&pTriangles[0], sizeof(MeshTriangle16) * ctTris);
        
        for (INDEX i = 0; i < ctTris; i++) 
        {
          mSrf.msrf_aTriangles[i].iVertex[0] = pTriangles[i].iVertex[0];
          mSrf.msrf_aTriangles[i].iVertex[1] = pTriangles[i].iVertex[1];
          mSrf.msrf_aTriangles[i].iVertex[2] = pTriangles[i].iVertex[2];
        }
        
        delete pTriangles;
        
        (*istrFile) >> strSurfaceID;           // Read surface ID
        mSrf.msrf_iSurfaceID = SM_StringToId(strSurfaceID);
        
        (*istrFile) >> mSrf.msrf_ulFlags;      // Read surface flags
        mSrf.msrf_ulFlags = cipher.Decode(mSrf.msrf_ulFlags);
        
        // Read table with Relative Weight Map Indices.
        INDEX ctRWMI;
        (*istrFile) >> ctRWMI;
        ctRWMI = cipher.Decode(ctRWMI);
        
        if (ctRWMI > 0) {
          mSrf.msrf_aubRelIndexTable.New(ctRWMI);
          istrFile->Read_t(&mSrf.msrf_aubRelIndexTable[0], sizeof(UBYTE) * ctRWMI);
        }

        INDEX bShaderExists;
        (*istrFile) >> bShaderExists;                                         // Read bool that this surface has a shader
        bShaderExists = cipher.Decode(bShaderExists);

        // If shader exists read its params
        if (bShaderExists) {
          ReadShaderParams17_t(mSrf, istrFile, cipher);
        } else {
          // This surface does not have shader
          mSrf.msrf_pShader = NULL;
        }
      }
    }
    
    // Weight Maps.
    {
     mLod.mlod_aWeightMaps.New(ctWM);
     
       // Read each weightmap
      for (INDEX iwm = 0; iwm < ctWM; iwm++)
      {
        CTString strNameID;
        INDEX ctWw;
        
        (*istrFile) >> strNameID;                                            // Read weightmap ID
        (*istrFile) >> ctWw;                                                  // Read wertex weight count
        ctWw = cipher.Decode(ctWw);

        mLod.mlod_aWeightMaps[iwm].mwm_iID = SM_StringToId(strNameID);
        
        // Create wertex weight array
        mLod.mlod_aWeightMaps[iwm].mwm_aVertexWeight.New(ctWw);
        istrFile->Read_t(&mLod.mlod_aWeightMaps[iwm].mwm_aVertexWeight[0],
                         sizeof(MeshVertexWeight) * ctWw);                    // Read wertex weights
      }
    }
    
    // Morph Maps.
    {
      mLod.mlod_aMorphMaps.New(ctMM);
      
      for (INDEX iMM = 0; iMM < ctMM; iMM++)
      {
        CTString strNameID;
        INDEX ctMorphSets;
        
        MeshMorphMap &mMap = mLod.mlod_aMorphMaps[iMM];
        
        (*istrFile) >> strNameID; 
        (*istrFile) >> mMap.mmp_bRelative; // Read bRelative
        (*istrFile) >> ctMorphSets;

        mMap.mmp_bRelative = cipher.Decode(mMap.mmp_bRelative);
        ctMorphSets = cipher.Decode(ctMorphSets);
      
        mLod.mlod_aMorphMaps[iMM].mmp_iID = SM_StringToId(strNameID);
        
        // create morps sets array
        mMap.mmp_aMorphMap.New(ctMorphSets);
        istrFile->Read_t(&mMap.mmp_aMorphMap[0], sizeof(MeshVertexMorph) * ctMorphSets);
      }
    }
    
    // Weight Infos.
    if (ctWI) {
      mLod.mlod_aVertexWeights.New(ctWI);
      istrFile->Read_t(&mLod.mlod_aVertexWeights[0], sizeof(MeshVertexWeightInfo) * ctWI); // TODO: These guys aren't used. Solve it!
    }
  } // for each load
}

static void WriteMeshVersion12_t(CSkelMesh *pMesh, CTStream *ostrFile)
{
  INDEX ctmlods = pMesh->msh_aLods.Count();
  ostrFile->WriteID_t(CChunkID(MESH_ID));                                   // Write id
  (*ostrFile) << (ULONG)MESH_VERSION_OLD;                                   // Write version
  (*ostrFile) << ctmlods;                                                   // Write mlod count
  
  // For each lod in mesh
  for (INDEX iLod = 0; iLod < ctmlods; iLod++)
  {
    const CSkelMeshLod &mLod = pMesh->GetLod(iLod);

    // Prepare component counts.
    const INDEX ctVtx = mLod.mlod_aVertices.Count();
    const INDEX ctUV = mLod.mlod_aUVMaps.Count();
    const INDEX ctSrf = mLod.mlod_aSurfaces.Count();
    const INDEX ctWM = mLod.mlod_aWeightMaps.Count();
    const INDEX ctMM = mLod.mlod_aMorphMaps.Count();
    
    (*ostrFile) << mLod.mlod_fnSourceFile;                                  // Write source file name
    (*ostrFile) << mLod.mlod_fMaxDistance;                                  // Write max distance
    (*ostrFile) << mLod.mlod_ulFlags;                                       // Write flags
    (*ostrFile) << ctVtx;                                                    // Write wertex count

    // Write vertices
    {
      // Vertices saved in 16-byte where 4 bytes just trash.
      struct MeshVertexOld *pVtxBuffer = new MeshVertexOld[ctVtx];

      // Copy data.
      for (INDEX i = 0; i < ctVtx; i++)
      {
        const MeshVertex &vxSrc = mLod.mlod_aVertices[i];

        pVtxBuffer[i].x = vxSrc.x;
        pVtxBuffer[i].y = vxSrc.y;
        pVtxBuffer[i].z = vxSrc.z;
        //pVtxBuffer[i].shade = 0;
      }
      
      ostrFile->Write_t(&pVtxBuffer[0], sizeof(MeshVertexOld) * ctVtx);  // Write wertices

      delete pVtxBuffer;
    }

    // Write normals
    {
      // Normals saved in (4 * 4 bytes) where last 4 bytes just trash.
      struct MeshNormalOld *pNormalBuffer = new MeshNormalOld[ctVtx];

      // Copy data.
      for (INDEX i = 0; i < ctVtx; i++)
      {
        const MeshNormal &mnSrc = mLod.mlod_aNormals[i];

        pNormalBuffer[i].nx = mnSrc.nx;
        pNormalBuffer[i].ny = mnSrc.ny;
        pNormalBuffer[i].nz = mnSrc.nz;
        pNormalBuffer[i].dummy = 0;
      }

      ostrFile->Write_t(&pNormalBuffer[0], sizeof(MeshNormalOld) * ctVtx);   // Write normals

      delete pNormalBuffer;
    }

    (*ostrFile) << ctUV;                                                      // Write uvmaps count

    // Write uvmaps
    for (int iMap = 0; iMap < ctUV; iMap++)
    {
      const MeshUVMap &mMap = mLod.mlod_aUVMaps[iMap];
      CTString strNameID = SM_IdToString(mMap.muv_iID);
      
      // Write data.
      (*ostrFile) << strNameID;
      ostrFile->Write_t(&mMap.muv_aTexCoords[0], sizeof(MeshTexCoord) * ctVtx);
    }

    ostrFile->Write_t(&ctSrf, sizeof(INDEX));                                 // Write surfaces count
    // Write surfaces
    for (INDEX iSrf = 0; iSrf < ctSrf; iSrf++)
    {
      const MeshSurface &mSrf = mLod.mlod_aSurfaces[iSrf];
      const INDEX ctTris = mSrf.msrf_aTriangles.Count();
      CTString strSurfaceID = SM_IdToString(mSrf.msrf_iSurfaceID);

      (*ostrFile) << strSurfaceID;                                          // Write surface ID
      (*ostrFile) << mSrf.msrf_iFirstVertex;                                // Write first vertex
      (*ostrFile) << mSrf.msrf_ctVertices;                                  // Write vertices count
      (*ostrFile) << ctTris;                                                // Write tris count

      ostrFile->Write_t(&mSrf.msrf_aTriangles[0], sizeof(MeshTriangle) * ctTris);

      const INDEX bShaderExists = (mSrf.msrf_pShader != NULL);
      (*ostrFile) << bShaderExists;                                         // Write bool that this surface has a shader
      
      if (bShaderExists) {
        WriteShaderParams_t(mSrf, ostrFile);
      }
    }

    (*ostrFile) << ctWM;                                                    // Write weightmaps count

    // For each weightmap in array..
    for (INDEX iMap = 0; iMap < ctWM; iMap++)
    {
      const MeshWeightMap &mWeightMap = mLod.mlod_aWeightMaps[iMap];
      const INDEX ctWeight = mWeightMap.mwm_aVertexWeight.Count();
      CTString pstrNameID = SM_IdToString(mWeightMap.mwm_iID);

      // Write data...
      (*ostrFile) << pstrNameID;
      (*ostrFile) << ctWeight;
      
      if (ctWeight > 0) {
        ostrFile->Write_t(&mWeightMap.mwm_aVertexWeight[0], sizeof(MeshVertexWeight) * ctWeight);
      }
    }

    (*ostrFile) << ctMM;                                                    // Write morphmaps count
    for (INDEX iMap = 0; iMap < ctMM; iMap++)
    {
      const MeshMorphMap &mMap = mLod.mlod_aMorphMaps[iMap];
      const INDEX ctSet = mMap.mmp_aMorphMap.Count();
      CTString pstrNameID = SM_IdToString(mMap.mmp_iID);
      
      // Write data.
      (*ostrFile) << pstrNameID;
      (*ostrFile) << mMap.mmp_bRelative;
      (*ostrFile) << ctSet;
      
      // Convert and write morph sets.
      {
        struct MeshVertexMorphOld *pSets = new MeshVertexMorphOld[ctSet];
        
        for (INDEX iSet = 0; iSet < ctSet; iSet++)
        {
          memcpy(&pSets[iSet], &mMap.mmp_aMorphMap[iSet], sizeof(MeshVertexMorph));
        }
        
        if (ctSet > 0) {
          ostrFile->Write_t(&pSets[0], sizeof(MeshVertexMorphOld) * ctSet);
        }
        
        delete pSets;
      }
    }
  }
}

// [SEE]
static void WriteMeshVersion16_t(CSkelMesh *pMesh, CTStream *ostrFile)
{
  const INDEX ctLods = pMesh->msh_aLods.Count();
  
  // Write magic and version...
  ostrFile->WriteID_t(CChunkID(MESH_ID));
  (*ostrFile) << (ULONG)MESH_VERSION_CURRENT;

  SLONG slMeshSizePos = ostrFile->GetPos_t();
  (*ostrFile) << SLONG(0); // Mesh size.

  // Write Lod count...
  (*ostrFile) << ctLods;

  for (INDEX iLod = 0; iLod < ctLods; iLod++)
  {
    const CSkelMeshLod &mLod = pMesh->GetLod(iLod);
    
    // Prepare component counts.
    const INDEX ctVtx = mLod.mlod_aVertices.Count();
    const INDEX ctUV = mLod.mlod_aUVMaps.Count();
    const INDEX ctSrf = mLod.mlod_aSurfaces.Count();
    const INDEX ctWM = mLod.mlod_aWeightMaps.Count();
    const INDEX ctMM = mLod.mlod_aMorphMaps.Count();
    const INDEX ctWI = mLod.mlod_aVertexWeights.Count();

    // Write LOD header.
    (*ostrFile) << ctVtx;
    (*ostrFile) << ctUV;
    (*ostrFile) << ctSrf;
    (*ostrFile) << ctWM;
    (*ostrFile) << ctMM;
    (*ostrFile) << ctWI;
    (*ostrFile) << mLod.mlod_fnSourceFile;
    (*ostrFile) << mLod.mlod_fMaxDistance;
    (*ostrFile) << mLod.mlod_ulFlags;
    
    // Write LOD data.
    if (ctVtx > 0) {
      ostrFile->Write_t(&mLod.mlod_aVertices[0], sizeof(MeshVertex) * ctVtx);
      ostrFile->Write_t(&mLod.mlod_aNormals[0], sizeof(MeshNormal) * ctVtx);
    }
    
    // Write uv maps.
    for (int iMap = 0; iMap < ctUV; iMap++)
    {
      const MeshUVMap &mMap = mLod.mlod_aUVMaps[iMap];
      
      CTString strNameID = SM_IdToString(mLod.mlod_aUVMaps[iMap].muv_iID);
      (*ostrFile) << strNameID;
      ostrFile->Write_t(&mMap.muv_aTexCoords[0], sizeof(MeshTexCoord) * ctVtx);
    }
    
    // Write surfaces.
    for (INDEX iSrf = 0; iSrf < ctSrf; iSrf++)
    {
      const MeshSurface &mSrf = mLod.mlod_aSurfaces[iSrf];
      const INDEX ctTris = mSrf.msrf_aTriangles.Count();
      CTString strSurfaceID = SM_IdToString(mSrf.msrf_iSurfaceID);
      
      (*ostrFile) << strSurfaceID;
      (*ostrFile) << mSrf.msrf_ulFlags;
      (*ostrFile) << mSrf.msrf_iFirstVertex;
      (*ostrFile) << mSrf.msrf_ctVertices;
      (*ostrFile) << ctTris;
      
      // Convert and write triangles.
      {
        struct MeshTriangle16 *pTriangles = new MeshTriangle16[ctTris];
        
        for (INDEX i = 0; i < ctTris; i++)
        {
          pTriangles[i].iVertex[0] = mSrf.msrf_aTriangles[i].iVertex[0];
          pTriangles[i].iVertex[1] = mSrf.msrf_aTriangles[i].iVertex[1];
          pTriangles[i].iVertex[2] = mSrf.msrf_aTriangles[i].iVertex[2];
        }
        
        ostrFile->Write_t(&pTriangles[0], sizeof(MeshTriangle16) * ctTris);
        
        delete pTriangles;
      }
      
      const INDEX ctRWMI = mSrf.msrf_aubRelIndexTable.Count();
      (*ostrFile) << ctRWMI;
      ostrFile->Write_t(&mSrf.msrf_aubRelIndexTable[0], sizeof(UBYTE) * ctRWMI);

      //
      INDEX bShaderExists = (mSrf.msrf_pShader != NULL);
      (*ostrFile) << bShaderExists;
      
      if (bShaderExists) {
        WriteShaderParams_t(mSrf, ostrFile);
      }
    }
    
    // For each weightmap in array..
    for (INDEX iMap = 0; iMap < ctWM; iMap++)
    {
      const MeshWeightMap &mMap = mLod.mlod_aWeightMaps[iMap];
      const INDEX ctWeight = mMap.mwm_aVertexWeight.Count();
      CTString pstrNameID = SM_IdToString(mMap.mwm_iID);

      // Write data...
      (*ostrFile) << pstrNameID;
      (*ostrFile) << ctWeight;
      ostrFile->Write_t(&mMap.mwm_aVertexWeight[0], sizeof(MeshVertexWeight) * ctWeight);      
    }
    
    // For each morphmap in array...
    for (INDEX iMap = 0; iMap < ctMM; iMap++)
    {
      const MeshMorphMap &mMap = mLod.mlod_aMorphMaps[iMap];
      const INDEX ctSet = mMap.mmp_aMorphMap.Count();
      CTString pstrNameID = SM_IdToString(mMap.mmp_iID);
      
      // Write data...
      (*ostrFile) << pstrNameID;
      (*ostrFile) << mMap.mmp_bRelative;
      (*ostrFile) << ctSet;
      ostrFile->Write_t(&mMap.mmp_aMorphMap[0], sizeof(MeshVertexMorph) * ctSet);
    }
    
    if (ctWI > 0) {
      ostrFile->Write_t(&mLod.mlod_aVertexWeights[0], sizeof(MeshVertexWeightInfo) * ctWI);
    }
  }
  
  SLONG slEndPos = ostrFile->GetPos_t();
  ostrFile->SetPos_t(slMeshSizePos);
  (*ostrFile) << SLONG(slEndPos - slMeshSizePos);
  ostrFile->SetPos_t(slEndPos);
}

//Read from stream
void CSkelMesh::Read_t(CTStream *istrFile)
{
  istrFile->ExpectID_t(CChunkID(MESH_ID));                                  // Read chunk id
  (*istrFile) >> msh_iVersion;                                              // Check file version
  
  // If file version is older than 11.
  if (msh_iVersion < 11) {
    ThrowF_t(TRANS("File '%s'.\nInvalid Mesh file version.\nFound Ver \"%d\" that is too old!\n"),
             istrFile->GetDescription().ConstData(), msh_iVersion);
    return;
  }
  
  if (msh_iVersion == 17) {
    ReadMeshVersion17_t(this, istrFile);
    return;
  }
  
  if (msh_iVersion > 12) {
    ReadMeshVersion16_t(this, istrFile);
  } else {
    ReadMeshVersion12_t(this, istrFile);
  }
}

// Write to stream
void CSkelMesh::Write_t(CTStream *ostrFile)
{
  if (msh_iVersion == 11 || msh_iVersion == 12) {
    WriteMeshVersion12_t(this, ostrFile);
    return;
  }

  WriteMeshVersion16_t(this, ostrFile);
}

void CSkelMeshLod::Write_AM_t(CTStream *ostrFile)
{
  ostrFile->PutString_t("SE_MESH 0.1;\n\n");
  
  // Vertices
  ostrFile->FPrintF_t("VERTICES %d\n{\n", mlod_aVertices.Count());

  for (INDEX i = 0; i < mlod_aSurfaces.Count(); i++)
  {
    const MeshSurface &srf = mlod_aSurfaces[i];
    for (INDEX j = srf.msrf_iFirstVertex; j < srf.msrf_iFirstVertex + srf.msrf_ctVertices; ++j)
    {
      const MeshVertex &vtx = mlod_aVertices[j];
      ostrFile->FPrintF_t("\t%f, %f, %f;\n", vtx.x, vtx.y, vtx.z);
    }
  }

  ostrFile->PutString_t("}\n\n");
  
  // Normals
  ostrFile->FPrintF_t("NORMALS %d\n{\n", mlod_aVertices.Count());

  for (INDEX i = 0; i < mlod_aSurfaces.Count(); i++)
  {
    const MeshSurface &srf = mlod_aSurfaces[i];
    for (INDEX j = srf.msrf_iFirstVertex; j < srf.msrf_iFirstVertex + srf.msrf_ctVertices; ++j)
    {
      const MeshNormal &norm = mlod_aNormals[j];
      ostrFile->FPrintF_t("\t%f, %f, %f;\n", norm.nx, norm.ny, norm.nz);
    }
  }

  ostrFile->PutString_t("}\n\n");
  
  // Tex Maps
  ostrFile->FPrintF_t("UVMAPS %d\n{\n", mlod_aUVMaps.Count());

  for (INDEX i = 0; i < mlod_aUVMaps.Count(); i++)
  {
    const MeshUVMap &texmap = mlod_aUVMaps[i];
    ostrFile->FPrintF_t("\t{\n\t\tNAME \"%s\";\n", SM_IdToString(mlod_aUVMaps[i].muv_iID));
    ostrFile->FPrintF_t("\t\tTEXCOORDS %d\n\t\t{\n", mlod_aVertices.Count());

    for (INDEX j = 0; j < mlod_aVertices.Count(); j++)
    {
      const MeshTexCoord& tc = texmap.muv_aTexCoords[j];
      ostrFile->FPrintF_t("\t\t\t%f, %f;\n", tc.u, tc.v);
    }

    ostrFile->PutString_t("\t\t}\n\t}\n");
  }

  ostrFile->PutString_t("}\n\n");

  // Surfaces
  ostrFile->FPrintF_t("SURFACES %d\n{\n", mlod_aSurfaces.Count());

  for (INDEX i = 0; i < mlod_aSurfaces.Count(); i++)
  {
    const MeshSurface &srf = mlod_aSurfaces[i];

    ostrFile->FPrintF_t("\t{\n\t\tNAME \"%s\";\n", SM_IdToString(srf.msrf_iSurfaceID));
    ostrFile->FPrintF_t("\t\tTRIANGLE_SET %d\n\t\t{\n", srf.msrf_aTriangles.Count());

    for (INDEX j = 0; j < srf.msrf_aTriangles.Count(); j++)
    {
      const MeshTriangle &triangle = srf.msrf_aTriangles[j];
      int fv = (int)srf.msrf_iFirstVertex;

      ostrFile->FPrintF_t("\t\t\t%d, %d, %d;\n", triangle.iVertex[0] + fv, triangle.iVertex[1] + fv, triangle.iVertex[2] + fv);
    }

    ostrFile->PutString_t("\t\t}\n\t}\n");
  }

  ostrFile->PutString_t("}\n\n");

  // Weight Maps
  ostrFile->FPrintF_t("WEIGHTS %d\n{\n", mlod_aWeightMaps.Count());

  for (INDEX i = 0; i < mlod_aWeightMaps.Count(); i++)
  {
    const MeshWeightMap &wmap = mlod_aWeightMaps[i];
    
    ostrFile->FPrintF_t("\t{\n\t\tNAME \"%s\";\n", SM_IdToString(wmap.mwm_iID));
    ostrFile->FPrintF_t("\t\tWEIGHT_SET %d\n\t\t{\n", wmap.mwm_aVertexWeight.Count());

    for (INDEX j = 0; j < mlod_aWeightMaps[i].mwm_aVertexWeight.Count(); j++)
    {
      const MeshVertexWeight &weight = wmap.mwm_aVertexWeight[j];
      ostrFile->FPrintF_t("\t\t\t{ %d; %f; }\n", weight.mww_iVertex, weight.mww_fWeight);
    }

    ostrFile->PutString_t("\t\t}\n\t}\n");
  }

  ostrFile->PutString_t("}\n\n");

  // Morph Maps
  const INDEX ctMorphMap  = mlod_aMorphMaps.Count();

  ostrFile->FPrintF_t("MORPHS %d\n", ctMorphMap);
  ostrFile->FPrintF_t("{\n");
  
  for (INDEX iMorphMap = 0; iMorphMap < ctMorphMap; iMorphMap++)
  {
    const MeshMorphMap &mmap = mlod_aMorphMaps[iMorphMap];
    const INDEX ctMorph = mmap.mmp_aMorphMap.Count();
    const BOOL bRelative = mmap.mmp_bRelative;
    CTString strNameID = SM_IdToString(mmap.mmp_iID);
    
    ostrFile->FPrintF_t("  {\n");
    ostrFile->FPrintF_t("    NAME \"%s\";\n", strNameID);
    ostrFile->FPrintF_t("    RELATIVE %s;\n", bRelative ? "TRUE" : "FALSE");
    ostrFile->FPrintF_t("    MORPH_SET %d\n", ctMorph);
    ostrFile->FPrintF_t("    {\n");

    // Write morph vertices from morph maps.
    for (INDEX iMorph = 0; iMorph < ctMorph; iMorph++)
    {
      const MeshVertexMorph &mwm = mmap.mmp_aMorphMap[iMorph];
      ostrFile->FPrintF_t("      { %d; %f, %f, %f; %f, %f, %f; }\n", mwm.mwm_iVxIndex, mwm.mwm_x, mwm.mwm_y, mwm.mwm_z, mwm.mwm_nx, mwm.mwm_ny, mwm.mwm_nz);
    }

    ostrFile->FPrintF_t("    }\n");
    
    ostrFile->FPrintF_t("  }\n");
  }
  
  ostrFile->FPrintF_t("}\n\n");

  ostrFile->FPrintF_t("SE_MESH_END;\n");
};

void CSkelMeshLod::Write_SHP_t(CTStream *ostrFile)
{
  INDEX ctsrf = mlod_aSurfaces.Count();
  ostrFile->FPrintF_t("SHADER_PARAMS 1.0;\nSHADER_SURFACES %d\n{\n", ctsrf);

  for (INDEX isrf = 0; isrf < ctsrf; isrf++)
  {
    const MeshSurface &msrf = mlod_aSurfaces[isrf];
    const ShaderParams *pShdParams = &msrf.msrf_ShadingParams;
    CTString strShaderName;

    if (msrf.msrf_pShader != NULL) {
      strShaderName = msrf.msrf_pShader->GetName();
    }

    CTString strSurfName = SM_IdToString(msrf.msrf_iSurfaceID);
    
    ostrFile->FPrintF_t("  SHADER_SURFACE \"%s\";\n  {\n", strSurfName.ConstData());
    ostrFile->FPrintF_t("    SHADER_NAME \"%s\";\n", strShaderName.ConstData());

    // write texture names
    INDEX cttx = pShdParams->sp_aiTextureIDs.Count();
    ostrFile->FPrintF_t("    SHADER_TEXTURES %d\n    {\n",cttx);
    for (INDEX itx = 0; itx < cttx; itx++)
    {
      CTString strTextID = SM_IdToString(pShdParams->sp_aiTextureIDs[itx]);
      ostrFile->FPrintF_t("      \"%s\";\n", strTextID.ConstData());
    }
    ostrFile->FPrintF_t("    };\n");

    // write uvmaps
    INDEX cttxc = pShdParams->sp_aiTexCoordsIndex.Count();
    ostrFile->FPrintF_t("    SHADER_UVMAPS %d\n    {\n",cttxc);
    for (INDEX itxc = 0; itxc < cttxc; itxc++)
    {
      ostrFile->FPrintF_t("      %d;\n", pShdParams->sp_aiTexCoordsIndex[itxc]);
    }
    ostrFile->FPrintF_t("    };\n");

    // write colors
    INDEX ctcol=pShdParams->sp_acolColors.Count();
    ostrFile->FPrintF_t("    SHADER_COLORS %d\n    {\n",ctcol);
    for (INDEX icol = 0; icol < ctcol; icol++)
    {
      CTString strColor = CTString(0, "%.8X", pShdParams->sp_acolColors[icol]);
      ostrFile->FPrintF_t("      0x%s;\n", strColor.ConstData());
    }
    ostrFile->FPrintF_t("    };\n");

    //write floats
    INDEX ctfl=pShdParams->sp_afFloats.Count();
    ostrFile->FPrintF_t("    SHADER_FLOATS %d\n    {\n",ctfl);
    for (INDEX ifl = 0; ifl < ctfl; ifl++)
    {
      ostrFile->FPrintF_t("      %g;\n", pShdParams->sp_afFloats[ifl]);
    }
    ostrFile->FPrintF_t("    };\n");

    // write flags
    CTString strFlags = CTString(0,"%.8X",pShdParams->sp_ulFlags);
    ostrFile->FPrintF_t("    SHADER_FLAGS 0x%s;\n", strFlags.ConstData());

    // close surface 
    ostrFile->FPrintF_t("  };\n");
  }

  ostrFile->FPrintF_t("};\nSHADER_PARAMS_END\n");
}
