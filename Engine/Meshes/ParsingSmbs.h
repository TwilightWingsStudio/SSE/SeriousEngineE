/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

// needed for parser and scanner
extern int yylex(void);
extern void yyerror(char *s);
extern int syyparse(void);
extern void syyrestart(FILE *f);

#define YY_NEVER_INTERACTIVE 1

#define SMC_MAX_INCLUDE_LEVEL 32

// temporary values for parsing
extern INDEX _yy_iIndex;
extern CModelInstance *_yy_mi;

void SMC_PushBuffer(const char *strName, const char *strBuffer, BOOL bParserEnd);
BOOL SMC_PopBuffer(void);
const char *SMC_GetBufferName(void);
int SMC_GetBufferLineNumber(void);
const char *SMC_GetBufferContents(void);
int SMC_GetBufferStackDepth(void);
void SMC_CountOneLine(void);

// [SEE]
extern CModelObject *_yy_mo;
extern int vyyparse(void);
extern void vyyrestart(FILE *f);

void VMC_PushBuffer(const char *strName, const char *strBuffer, BOOL bParserEnd);
BOOL VMC_PopBuffer(void);
const char *VMC_GetBufferName(void);
int VMC_GetBufferLineNumber(void);
const char *VMC_GetBufferContents(void);
int VMC_GetBufferStackDepth(void);
void VMC_CountOneLine(void);