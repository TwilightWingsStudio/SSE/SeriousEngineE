/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

ENGINE_API extern INDEX SM_StringToId(CTString strName);
ENGINE_API extern INDEX SM_FindId(CTString strName);
ENGINE_API extern CTString SM_IdToString(INDEX iIndex);

// For compatibility.
#define ska_GetIDFromStringTable SM_StringToId 
#define ska_GetStringFromTable   SM_IdToString
#define ska_FindStringInTable    SM_FindId
