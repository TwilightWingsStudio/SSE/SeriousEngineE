/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SKELETON_H
#define SE_INCL_SKELETON_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Math/Quaternion.h>
#include <Core/Base/CTString.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Placement.h>
#include <Engine/Graphics/Texture.h>
#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/StaticArray.h>
#include <Core/IO/Resource.h>

/*
 * TODO: Add comment here
 */
struct QVect
{
  FLOAT3D vPos;
  FLOATquat3D qRot;
};

//! Collection of bones for given LOD.
class ENGINE_API CSkeletonLod
{
  public:
    //! Write to stream in text format.
    void Write_AS_t(CTStream *ostrFile); // throw char *

  public:

    FLOAT slod_fMaxDistance;                        // distance in witch this lod is visible
    TStaticArray<struct SkeletonBone> slod_aBones;  // array of bones for this lod
    CTString slod_fnSourceFile;                     // source filename of ascii skleton lod
};

//! One bone at skeleton.
struct ENGINE_API SkeletonBone
{
  INDEX sb_iID;               // ID of bone
  INDEX sb_iParentID;         // ID of parent bone
  Matrix12 sb_mAbsPlacement;  // default bone placement
  QVect sb_qvRelPlacement;    // default bone placement (same as mAbsPlacement)
  FLOAT sb_fOffSetLen;
  FLOAT sb_fBoneLength;       // length of bone
};

//! A resource that contains one or more skeleton LODs. 
class ENGINE_API CSkeleton : public CResource
{
  public:
    //! Constructor.
    CSkeleton();

    //! Destructor.
    ~CSkeleton();
    
    //! Find bone in skeleton lod
    INDEX FindBoneInLOD(INDEX iBoneID,INDEX iSkeletonLod);
    
    //! Sorts bones in skeleton so parent bones are allways before child bones in array.
    void SortSkeleton();
    void SortSkeletonRecursive(INDEX iParentID, INDEX iSkeletonLod);
    
    //! Calculate absolute transformations for all bones in this lod
    void CalculateAbsoluteTransformations(INDEX iSkeletonLod);
    
    //! Add skeleton lod to skeleton
    void AddSkletonLod(CSkeletonLod &slod);
    
    //! Remove skleton lod form skeleton
    void RemoveSkeletonLod(CSkeletonLod *pslodRemove);

    //! Read from stream.
    void Read_t(CTStream *istrFile); // throw char *
    
    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char *
    
    //! Free memory.
    void Clear(void);
    
    //! Returns amount of used RAM.
    SLONG GetUsedMemory(void);
    
    //! TODO: Add comment.
    virtual UBYTE GetType() const;

  public:
    TStaticArray<class CSkeletonLod>  skl_aSkeletonLODs;
};

#endif  /* include-once check. */