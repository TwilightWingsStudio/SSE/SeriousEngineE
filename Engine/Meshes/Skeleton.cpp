/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include "Skeleton.h"

#include <Engine/Graphics/DrawPort.h>
#include <Core/Math/Projection.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Engine/Meshes/SM_Render.h>
#include <Core/IO/Stream.h>
#include <Core/Templates/DynamicContainer.cpp>

#define SKELETON_VERSION  6
#define SKELETON_ID       "SKEL"

TStaticArray<struct SkeletonBone> _aSortArray;
INDEX ctSortBones;

// Default constructor
CSkeleton::CSkeleton()
{
}

// Default destructor
CSkeleton::~CSkeleton()
{
}

// Find bone in skeleton lod
INDEX CSkeleton::FindBoneInLOD(INDEX iBoneID, INDEX iSkeletonLod)
{
  ASSERT(iSkeletonLod >= 0);
  INDEX ctslods = skl_aSkeletonLODs.Count();
  if (ctslods < 1) {
    return -1;
  }
  
  CSkeletonLod &slod = skl_aSkeletonLODs[iSkeletonLod];
  INDEX ctb = slod.slod_aBones.Count();
  
  // For each bone in skeleton
  for (INDEX isb = 0; isb < ctb; isb++)
  {
    // If bone id match
    if (slod.slod_aBones[isb].sb_iID == iBoneID) {
      // return index in array of bones
      return isb;
    }
  }
  return -1;
}

// Sorts bones in skeleton so parent bones are allways before child bones in array
void CSkeleton::SortSkeleton()
{
  // Sort each lod in skeleton
  INDEX ctslods = skl_aSkeletonLODs.Count();
  
  // For each lod in skeleton
  for (INDEX islod = 0; islod < ctslods; islod++)
  {
    // Create SkeletonBones array for sorting array
    _aSortArray.New(skl_aSkeletonLODs[islod].slod_aBones.Count());
    
    // Start sort for parent bone
    SortSkeletonRecursive(-1, islod);
    
    // Just copy sorted array
    skl_aSkeletonLODs[islod].slod_aBones.CopyArray(_aSortArray);
    
    // Clear array
    _aSortArray.Clear();
    
    // Calculate abs transforms for bones in this lod
    CalculateAbsoluteTransformations(islod);
  }
}

// Sort skeleton bones in lod so parent bones are before child bones in array
void CSkeleton::SortSkeletonRecursive(INDEX iParentID, INDEX iSkeletonLod)
{
  // reset global counter for sorted bones if this bone is parent bone
  if (iParentID == (-1)) {
    ctSortBones = 0;
  }
  
  CSkeletonLod &slod = skl_aSkeletonLODs[iSkeletonLod];
  INDEX ctsb = slod.slod_aBones.Count();
  
  // For each bone in lod
  for (INDEX isb = 0; isb < ctsb; isb++)
  {
    SkeletonBone &sb = slod.slod_aBones[isb];
    if (sb.sb_iParentID == iParentID)
    {
      // Just copy data to _aSortArray
      _aSortArray[ctSortBones].sb_iID = sb.sb_iID;
      _aSortArray[ctSortBones].sb_iParentID = sb.sb_iParentID;
      _aSortArray[ctSortBones].sb_fBoneLength = sb.sb_fBoneLength;
      memcpy(&_aSortArray[ctSortBones].sb_mAbsPlacement, &sb.sb_mAbsPlacement, sizeof(float) * 12);
      memcpy(&_aSortArray[ctSortBones].sb_qvRelPlacement, &sb.sb_qvRelPlacement, sizeof(QVect));
      _aSortArray[ctSortBones].sb_fOffSetLen = sb.sb_fOffSetLen;
      
      // Increase couter for sorted bones
      ctSortBones++;
    }
  }
  
  // Sort children bones
  for (INDEX icsb = 0; icsb < ctsb; icsb++)
  {
    SkeletonBone &sb = slod.slod_aBones[icsb];
    if (sb.sb_iParentID == iParentID) {
      SortSkeletonRecursive(sb.sb_iID, iSkeletonLod);
    }
  }
}

// Calculate absolute transformations for all bones in this lod
void CSkeleton::CalculateAbsoluteTransformations(INDEX iSkeletonLod)
{
  INDEX ctbones = skl_aSkeletonLODs[iSkeletonLod].slod_aBones.Count();
  CSkeletonLod &slod = skl_aSkeletonLODs[iSkeletonLod];
  
  // For each bone
  for (INDEX isb = 0; isb < ctbones; isb++)
  {
    SkeletonBone &sb = slod.slod_aBones[isb];
    INDEX iParentID = sb.sb_iParentID;
    INDEX iParentIndex = FindBoneInLOD(iParentID, iSkeletonLod);
    if (iParentID > (-1)) {
      SkeletonBone &sbParent = slod.slod_aBones[iParentIndex];
      MatrixMultiplyCP(sb.sb_mAbsPlacement, sbParent.sb_mAbsPlacement, sb.sb_mAbsPlacement);
    }
  }
}

// Add skeleton lod to skeleton
void CSkeleton::AddSkletonLod(CSkeletonLod &slod)
{
  INDEX ctlods = skl_aSkeletonLODs.Count();
  skl_aSkeletonLODs.Expand(ctlods + 1);
  skl_aSkeletonLODs[ctlods] = slod;
}

// Remove skleton lod form skeleton
void CSkeleton::RemoveSkeletonLod(CSkeletonLod *pslodRemove)
{
  INDEX ctslod = skl_aSkeletonLODs.Count();
  
  // Create temp space for skeleton lods
  TStaticArray<class CSkeletonLod> aTempSLODs;
  aTempSLODs.New(ctslod - 1);
  INDEX iIndexSrc = 0;

  // For each skeleton lod in skeleton
  for (INDEX islod = 0; islod < ctslod; islod++)
  {
    CSkeletonLod *pslod = &skl_aSkeletonLODs[islod];
    
    // Copy all skeleton lods except the selected one
    if (pslod != pslodRemove) {
      aTempSLODs[iIndexSrc] = *pslod;
      iIndexSrc++;
    }
  }
  
  // Copy temp array of skeleton lods back in skeleton
  skl_aSkeletonLODs.CopyArray(aTempSLODs);
  
  // Clear temp skleletons lods array
  aTempSLODs.Clear();
}

// Write to stream
void CSkeleton::Write_t(CTStream *ostrFile)
{
  INDEX ctslods = skl_aSkeletonLODs.Count();
  ostrFile->WriteID_t(CChunkID(SKELETON_ID));                       // Write id
  (*ostrFile) << (INDEX)SKELETON_VERSION;                           // Write version
  (*ostrFile) << ctslods;                                           // Write lods count
  
  // For each lod in skeleton
  for (INDEX islod = 0; islod < ctslods; islod++)
  {
    CSkeletonLod &slod = skl_aSkeletonLODs[islod];
    (*ostrFile) << slod.slod_fnSourceFile;                          // Write source file name
    (*ostrFile) << slod.slod_fMaxDistance;                          // Write MaxDistance

    INDEX ctb = slod.slod_aBones.Count();
    (*ostrFile) << ctb;                                             // Write bone count
    
    // Write skeleton bones
    for (INDEX ib = 0; ib < ctb; ib++)
    {
      CTString strNameID = SM_IdToString(slod.slod_aBones[ib].sb_iID);
      CTString strParentID = SM_IdToString(slod.slod_aBones[ib].sb_iParentID);
      SkeletonBone &sb = slod.slod_aBones[ib];
      (*ostrFile) << strNameID;                                     // Write ID
      (*ostrFile) << strParentID;                                   // Write Parent ID
      //(*ostrFile) << slod.slod_aBones[ib].sb_iParentIndex;
      ostrFile->Write_t(&sb.sb_mAbsPlacement, sizeof(FLOAT) * 12);  // Write AbsPlacement matrix
      ostrFile->Write_t(&sb.sb_qvRelPlacement, sizeof(QVect));      // Write RelPlacement Qvect stuct
      (*ostrFile) << sb.sb_fOffSetLen;                              // Write offset len
      (*ostrFile) << sb.sb_fBoneLength;                             // Write bone length
    }
  }
}

// Read from stream
void CSkeleton::Read_t(CTStream *istrFile)
{
  INDEX iFileVersion;
  INDEX ctslods;
  istrFile->ExpectID_t(CChunkID(SKELETON_ID));                      // Read chunk id
  (*istrFile) >> iFileVersion;                                      // Check file version
  if (iFileVersion != SKELETON_VERSION) {
		ThrowF_t(TRANS("File '%s'.\nInvalid skeleton file version.\nExpected Ver \"%d\" but found \"%d\"\n"),
                   istrFile->GetDescription().ConstData(), SKELETON_VERSION, iFileVersion);
  }

  (*istrFile) >> ctslods;                                           // Read skeleton lod count
  if (ctslods > 0) {
    skl_aSkeletonLODs.Expand(ctslods);
  }
  
  // For each skeleton lod
  for (INDEX islod = 0; islod < ctslods; islod++)
  {
    CSkeletonLod &slod = skl_aSkeletonLODs[islod];
    (*istrFile) >> slod.slod_fnSourceFile;                          // Read source file name
    (*istrFile) >> slod.slod_fMaxDistance;                          // Read MaxDistance

    INDEX ctb;
    (*istrFile) >> ctb;                                             // Read bone count
    
    // Create bone array
    slod.slod_aBones.New(ctb);
    
    // Read skeleton bones
    for (INDEX ib = 0; ib < ctb; ib++)
    {
      CTString strNameID;
      CTString strParentID;
      SkeletonBone &sb = slod.slod_aBones[ib];
      (*istrFile) >> strNameID;                                     // read ID
      (*istrFile) >> strParentID;                                   // read Parent ID
      //(*istrFile) >> slod.slod_aBones[ib].sb_iParentIndex ;
      sb.sb_iID = SM_StringToId(strNameID);
      sb.sb_iParentID = SM_StringToId(strParentID);
      
      istrFile->Read_t(&sb.sb_mAbsPlacement, sizeof(FLOAT) * 12);   // Read AbsPlacement matrix
      istrFile->Read_t(&sb.sb_qvRelPlacement, sizeof(QVect));       // Read RelPlacement Qvect stuct
      (*istrFile) >> sb.sb_fOffSetLen;                              // Read offset len
      (*istrFile) >> sb.sb_fBoneLength;                             // Read bone length
    }
  }
}

// Clear skeleton
void CSkeleton::Clear(void)
{
  // For each LOD
  for (INDEX islod = 0; islod < skl_aSkeletonLODs.Count(); islod++)
  {
    // Clear bones array
    skl_aSkeletonLODs[islod].slod_aBones.Clear();
  }
  
  // Clear all lods
  skl_aSkeletonLODs.Clear();
}

// Count used memory
SLONG CSkeleton::GetUsedMemory(void)
{
  SLONG slMemoryUsed = sizeof(*this);
  INDEX ctslods = skl_aSkeletonLODs.Count();
  
  // For each skeleton lods
  for (INDEX islod = 0; islod < ctslods; islod++)
  {
    CSkeletonLod &slod = skl_aSkeletonLODs[islod];
    slMemoryUsed += sizeof(slod);
    slMemoryUsed += slod.slod_aBones.Count() * sizeof(SkeletonBone);
  }
  return slMemoryUsed;
}

UBYTE CSkeleton::GetType() const
{
  return TYPE_SKELETON;
}

void CSkeletonLod::Write_AS_t(CTStream *ostrFile)
{
  const INDEX ctBone = slod_aBones.Count();
  
  ostrFile->FPrintF_t("SE_SKELETON 0.1;\n\n");

  // Write its bones.
  ostrFile->FPrintF_t("BONES %d\n", ctBone);
  ostrFile->FPrintF_t("{\n");

  for (int iBone = 0; iBone < ctBone; iBone++)
  {
    SkeletonBone &bone = slod_aBones[iBone];

    ostrFile->FPrintF_t("  NAME \"%s\";\n", SM_IdToString(bone.sb_iID));
    ostrFile->FPrintF_t("  PARENT \"%s\";\n", SM_IdToString(bone.sb_iParentID));
    ostrFile->FPrintF_t("  LENGTH %f;\n", bone.sb_fBoneLength);
    ostrFile->PutString_t("  {\n");

    ostrFile->PutString_t("    ");

    Matrix12 matrix;
    QVectToMatrix12(matrix, bone.sb_qvRelPlacement);

    for (int i = 0; i < 12; i++) {
      ostrFile->FPrintF_t("%f%s", matrix[i], (i == 11 ? ";" : ", "));
    }

    ostrFile->PutString_t("\n");
    ostrFile->PutString_t("  }\n");
  }

  ostrFile->PutString_t("}\n\n");
  ostrFile->PutString_t("SE_SKELETON_END;");
};
