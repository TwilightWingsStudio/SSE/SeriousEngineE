/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>

#include <Engine/Resources/ReplaceFile.h>
#include <Engine/Meshes/ModelConfiguration.h>
#include <Engine/Meshes/ModelInstance.h>
#include <Engine/Meshes/ParsingSmbs.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Resources/ResourceManager.h>

CModelObject *_yy_mo = NULL;

extern BOOL SetModelFromConfig_t(CModelObject *pmo, CTStream &strm, CTString &strName, BOOL bPreview);

CModelConfiguration::CModelConfiguration() : mic_pObject(NULL)
{
}

CModelConfiguration::~CModelConfiguration()
{
  const enum ModelInstanceType eType = GetInstanceType();

  switch (eType)
  {
    case MIT_INVALID: return;

    case MIT_VERTEX: {
      delete mic_pModelObject;
      mic_pModelObject = NULL;
      return;
    } break;

    case MIT_SKELETAL: {
      // When deleting model instance do not release model instance serial (this)
      if (mic_pModelInstance->mi_pInStock != NULL) {
        ASSERT(mic_pModelInstance->mi_pInStock->GetReferenceCount() == 0);
      }

      mic_pModelInstance->mi_pInStock = NULL;
      DeleteModelInstance(mic_pModelInstance); // delete model instance 
      mic_pModelInstance = NULL; // reset pointer to model instance
    } break;
  }
}

void CModelConfiguration::Read_t(CTStream *istrFile)
{
  ASSERT(mic_pObject == NULL);

  const CTFileName &fnmConfigFile = istrFile->GetDescription();

  // AMC
  if (fnmConfigFile.FileExt() == ".amc") {
    CTString strName;
    mic_pModelObject = new CModelObject;
    SetModelFromConfig_t(mic_pModelObject, *istrFile, strName, FALSE);

  // VMC
  } else if (fnmConfigFile.FileExt() == ".vmc") {
    mic_pModelObject = new CModelObject;
    CTFileName fnFileName = fnmConfigFile;
    try {
      fnFileName.RemoveApplicationPath_t();
    } catch (char *) {} // FIXME: Nani?

    CTString strIncludeFile;
    strIncludeFile.Load_t(fnFileName);

    _yy_mo = mic_pModelObject;
    VMC_PushBuffer(fnFileName, strIncludeFile, TRUE);
    vyyparse();

  // BMC or SMC
  } else {
    mic_pModelInstance = LoadModelInstance_t(fnmConfigFile);

    ASSERT(mic_pModelInstance != NULL);
    ASSERT(mic_pModelInstance->mi_pInStock == NULL);
    mic_pModelInstance->mi_pInStock = this;
    ASSERT(mic_pModelInstance != NULL);
  }
}

void CModelConfiguration::Write_t(CTStream *ostrFile)
{
  ASSERTALWAYS("CModelConfiguration cannot be writen");
}

void CModelConfiguration::Clear(void)
{
  ASSERTALWAYS("CModelConfiguration cannot be cleared");
}

SLONG CModelConfiguration::GetUsedMemory(void)
{
  if (mic_pObject == NULL) {
    return sizeof(*this);
  }

  if (GetInstanceType() == MIT_VERTEX) {
    return sizeof(*this) + mic_pModelObject->GetUsedMemory();
  } else {
    return sizeof(*this) + mic_pModelInstance->GetUsedMemory();
  }
}

CTString CModelConfiguration::GetDescription(void)
{
  return CTString("");
}

UBYTE CModelConfiguration::GetType() const
{
  return TYPE_MODELCONFIGURATION;
}