/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/Meshes/BoneAnimSet.h>
#include <Core/Templates/StaticArray.h>
#include <Core/Base/CTString.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>
#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>
#include <Core/Math/Functions.h>
#include <Engine/Meshes/SM_Render.h> // QVectToMatrix12

#define ANIMSET_VERSION  14
#define ANIMSET_ID       "ANIM"

extern BOOL _bSkaAllRotations;

// Compres normal
static void CompressAxis(const FLOAT3D &vNormal, UWORD &ubH, UWORD &ubP)
{
  ANGLE h, p;

  const FLOAT &x = vNormal(1);
  const FLOAT &y = vNormal(2);
  const FLOAT &z = vNormal(3);

  // Calculate pitch
  p = ASin(y);

  // if y is near +1 or -1
  if (y > 0.99999 || y < -0.99999) {
    // Heading is irrelevant
    h = 0;
  } else {
    // Calculate heading
    h = ATan2(-x, -z);
  }

  h = (h / 360.0f) + 0.5f;
  p = (p / 360.0f) + 0.5f;
  ASSERT(h >= 0 && h <= 1);
  ASSERT(p >= 0 && p <= 1);
  
  // FIXME: Explain this
  ubH = UWORD(h * 65535);
  ubP = UWORD(p * 65535);
}

// Read from stream
void CBoneAnimSet::Read_t(CTStream *istrFile)
{
  INDEX iFileVersion;
  istrFile->ExpectID_t(CChunkID(ANIMSET_ID));                       // Read chunk id
  (*istrFile) >> iFileVersion;                                      // Check file version
  if (iFileVersion != ANIMSET_VERSION) {
    ThrowF_t(TRANS("File '%s'.\nInvalid animset file version. Expected Ver \"%d\" but found \"%d\"\n"),
             istrFile->GetDescription().ConstData(), ANIMSET_VERSION, iFileVersion);
  }
  INDEX ctan;
  (*istrFile) >> ctan;                                              // Read anims count
  
  // Create anims array
  as_Anims.New(ctan);

  for (int ian = 0; ian < ctan; ian++)
  {
    CBoneAnimation &an = as_Anims[ian];
    CTString pstrNameID;
    (*istrFile) >> an.an_fnSourceFile;                              // Read anim source file
    (*istrFile >> pstrNameID);                                      // Read Anim ID
    an.an_iID = SM_StringToId(pstrNameID);
    (*istrFile) >> an.an_fSecPerFrame;                              // Read secperframe
    (*istrFile) >> an.an_iFrames;                                   // Read num of frames
    (*istrFile) >> an.an_fTreshold;                                 // Read treshold
    (*istrFile) >> an.an_bCompresed;                                // Read if compresion is used
    (*istrFile) >> an.an_bCustomSpeed;                              // Read bool if animstion uses custom speed
    
    INDEX ctbe;
    INDEX ctme;
    (*istrFile) >> ctbe;                                            // Read bone envelopes count
    
    // Create bone envelopes array
    an.an_abeBones.New(ctbe);
    
    // Read bone envelopes
    for (int ibe = 0; ibe < ctbe; ibe++)
    {
      BoneEnvelope &be = an.an_abeBones[ibe];
      CTString pstrNameID;
      (*istrFile) >> pstrNameID;
      be.be_iBoneID = SM_StringToId(pstrNameID);         // Read bone envelope ID
      istrFile->Read_t(&be.be_mDefaultPos[0], sizeof(FLOAT) * 12);  // Read default pos(matrix12)

      INDEX ctp;
      (*istrFile) >> ctp;                                           // Read pos array
      be.be_apPos.New(ctp);
      for (INDEX ip = 0; ip < ctp; ip++)
      {
        istrFile->Read_t(&be.be_apPos[ip], sizeof(AnimPos));
      }
      
      INDEX ctr;
      (*istrFile) >> ctr;                                           // Read rot array count

      if (!an.an_bCompresed) {
        // Create array for uncompresed rotations
        be.be_arRot.New(ctr);
      } else {
        // If flag is set to remember uncompresed rotations
        if (_bSkaAllRotations) {
          // Create array for uncompresed rotations
          be.be_arRot.New(ctr);
        }
        
        // Create array for compresed rotations
        be.be_arRotOpt.New(ctr);
      }

      for (INDEX ir = 0; ir < ctr; ir++)
      {
        AnimRot arRot;// = be.be_arRot[ir];
        istrFile->Read_t(&arRot, sizeof(AnimRot));

        if (!an.an_bCompresed) {
          be.be_arRot[ir] = arRot;
        } else {
          if (_bSkaAllRotations) {
            // Fill uncompresed rotations
            be.be_arRot[ir] = arRot;
          }
          
          // Optimize quaternions
          FLOAT3D vAxis;
          ANGLE aAngle;
          UWORD ubH, ubP;
          FLOATquat3D &qRot = arRot.ar_qRot;
          AnimRotOpt &aroRot = be.be_arRotOpt[ir];
          qRot.ToAxisAngle(vAxis,aAngle);
          CompressAxis(vAxis, ubH, ubP);

          // Compress angle
          aroRot.aro_aAngle = aAngle * ANG_COMPRESIONMUL;
          aroRot.aro_iFrameNum = arRot.ar_iFrameNum;
          aroRot.aro_ubH = ubH;
          aroRot.aro_ubP = ubP;
          be.be_arRotOpt[ir] = aroRot;
        }
      }

      (*istrFile) >> be.be_OffSetLen;                               // Read offsetlen
    }

    (*istrFile) >> ctme;                                            // Read morph envelopes
    
    // Create morph envelopes array
    an.an_ameMorphs.New(ctme);
    
    // Read morph envelopes
    for (int ime = 0; ime < ctme; ime++)
    {
      MorphEnvelope &me = an.an_ameMorphs[ime];
      CTString pstrNameID;
      (*istrFile) >> pstrNameID;                                    // Read morph envelope ID
      me.me_iMorphMapID = SM_StringToId(pstrNameID);
      INDEX ctmf;
      (*istrFile) >> ctmf;                                          // Read morph factors count
      
      // Create morph factors array
      me.me_aFactors.New(ctmf);
      istrFile->Read_t(&me.me_aFactors[0], sizeof(FLOAT) * ctmf);   // Read morph factors
    }
  }
}

// Write to stream
void CBoneAnimSet::Write_t(CTStream *ostrFile)
{ 
  ostrFile->WriteID_t(CChunkID(ANIMSET_ID));                        // Write id
  (*ostrFile) << (INDEX)ANIMSET_VERSION;                            // Write version

  INDEX ctan = as_Anims.Count();
  (*ostrFile) << ctan;                                              // Write animation count

  for (int ian = 0; ian < ctan; ian++)
  {
    CBoneAnimation &an = as_Anims[ian];
    CTString pstrNameID = SM_IdToString(an.an_iID);
    (*ostrFile) << an.an_fnSourceFile;                              // Write anim source file
    (*ostrFile) << pstrNameID;                                      // Write anim id
    (*ostrFile) << an.an_fSecPerFrame;                              // Write secperframe
    (*ostrFile) << an.an_iFrames;                                   // Write num of frames
    (*ostrFile) << an.an_fTreshold;                                 // Write treshold
    (*ostrFile) << an.an_bCompresed;                                // Write if compresion is used
    (*ostrFile) << an.an_bCustomSpeed;                              // Write bool if animstion uses custom speed
    
    
    INDEX ctbe = an.an_abeBones.Count();
    INDEX ctme = an.an_ameMorphs.Count();
    (*ostrFile) << ctbe;                                            // Write bone envelopes count

    // For each bone envelope
    for (int ibe = 0; ibe < ctbe; ibe++)
    {
      BoneEnvelope &be = an.an_abeBones[ibe];
      CTString pstrNameID = SM_IdToString(be.be_iBoneID);
      (*ostrFile) << pstrNameID;                                    // Write bone envelope ID     
      ostrFile->Write_t(&be.be_mDefaultPos[0], sizeof(FLOAT) * 12); // Write default pos(matrix12)
      
      // Count positions
      INDEX ctp = be.be_apPos.Count();
      (*ostrFile) << ctp;                                           // Write position count
      
      // For each position
      for (INDEX ip = 0; ip < ctp; ip++)
      {
        ostrFile->Write_t(&be.be_apPos[ip], sizeof(AnimPos));       // Write position
      }
      
      // Count rotations
      INDEX ctRotations = be.be_arRot.Count();
      (*ostrFile) << ctRotations;                                   // Write rotation count
      
      // For each rotation
      for (INDEX ir = 0; ir < ctRotations; ir++)
      {
        AnimRot &arRot = be.be_arRot[ir];
        ostrFile->Write_t(&arRot, sizeof(AnimRot));                 // Write rotation
      }

      INDEX ctOptRotations = be.be_arRotOpt.Count();
      if (ctOptRotations > 0)
      {
        // OPTIMISED ROTATIONS ARE NOT SAVED !!!
        // use RememberUnCompresedRotatations();
        ASSERT(ctRotations >= ctOptRotations);
      }
      (*ostrFile) << be.be_OffSetLen;                               // Write offsetlen
    }
    
    (*ostrFile) << ctme;                                            // Write morph envelopes
    for (int ime = 0; ime < ctme; ime++)
    {
      MorphEnvelope &me = an.an_ameMorphs[ime];
      CTString pstrNameID = SM_IdToString(me.me_iMorphMapID);
      (*ostrFile) << pstrNameID;                                    // Write morph map ID
      
      INDEX ctmf = me.me_aFactors.Count();
      (*ostrFile) << ctmf;                                          // Write morph factors count
      ostrFile->Write_t(&me.me_aFactors[0], sizeof(FLOAT) * ctmf);  // Write morph factors
    }
  }
}

// [Cecil] SKAExport: Fixed return type from BOOL to INDEX
static inline INDEX FindBonePosForFrame(const BoneEnvelope &env, INDEX iFrame) {
  for (INDEX iPos = 0; iPos < env.be_apPos.Count(); iPos++)
  {
    const AnimPos &pos = env.be_apPos[iPos];
    
    if (pos.ap_iFrameNum == iFrame) {
      return iPos;
    }
  }
  
  return -1;
};

// [Cecil] SKAExport: Fixed return type from BOOL to INDEX
static inline INDEX FindBoneRotForFrame(const BoneEnvelope &env, INDEX iFrame) {
  for (INDEX iRot = 0; iRot < env.be_arRot.Count(); iRot++)
  {
    const AnimRot &pos = env.be_arRot[iRot];
    
    if (pos.ar_iFrameNum == iFrame) {
      return iRot;
    }
  }
  
  return -1;
};

void CBoneAnimation::Write_AA_t(CTStream *strm)
{
  strm->FPrintF_t("SE_ANIM 0.1;\n\n");
  strm->FPrintF_t("SEC_PER_FRAME %f;\n", an_fSecPerFrame);
  strm->FPrintF_t("FRAMES %d;\n", an_iFrames);
  strm->FPrintF_t("ANIM_ID \"%s\";\n\n", SM_IdToString(an_iID));
  strm->FPrintF_t("BONEENVELOPES %d\n{\n", an_abeBones.Count());

  for (int iBone = 0; iBone < an_abeBones.Count(); iBone++)
  {
    BoneEnvelope &bone = an_abeBones[iBone];

    strm->FPrintF_t("  NAME \"%s\"\n", SM_IdToString(bone.be_iBoneID));
    
    strm->FPrintF_t("  DEFAULT_POSE {");

    for (int iDefPos = 0; iDefPos < 12; iDefPos++) {
      strm->FPrintF_t("%f%s", bone.be_mDefaultPos[iDefPos], (iDefPos == 11 ? ";" : ", "));
    }

    strm->FPrintF_t("}\n");
    strm->FPrintF_t("  {\n");

    for (int iFrame = 0; iFrame < an_iFrames; iFrame++)
    {
      QVect qv;

      const INDEX iPos = FindBonePosForFrame(bone, iFrame);
      const INDEX iRot = FindBoneRotForFrame(bone, iFrame);

      if (iPos >= 0) {
        qv.vPos = bone.be_apPos[iPos].ap_vPos;
      }

      if (iRot >= 0) {
        qv.qRot = bone.be_arRot[iRot].ar_qRot;
      }
      
      Matrix12 matrix;
      QVectToMatrix12(matrix, qv);
      
      strm->FPrintF_t("  ");

      for (int i = 0; i < 12; i++) {
        strm->FPrintF_t("%f%s", matrix[i], (i == 11 ? ";" : ", "));
      }

      strm->FPrintF_t("\n");
    }

    strm->FPrintF_t("  }\n");
  }

  strm->FPrintF_t("}\n\n");

  const INDEX ctFrame = an_iFrames;
  const INDEX ctMorphEnvelope = an_ameMorphs.Count();

  strm->FPrintF_t("MORPHENVELOPES %d\n", ctMorphEnvelope);
  strm->FPrintF_t("{\n");
  
  for (INDEX iEnvelope = 0; iEnvelope < ctMorphEnvelope; iEnvelope++)
  {
    const MorphEnvelope &env = an_ameMorphs[iEnvelope];
    CTString strEnvelopeID = SM_IdToString(env.me_iMorphMapID);
    
    strm->FPrintF_t("  NAME \"%s\"\n", strEnvelopeID);
    strm->FPrintF_t("  {\n");

    // Write morph factor for each animation frame.
    for (INDEX iFrame = 0; iFrame < ctFrame; iFrame++)
    {
      strm->FPrintF_t("    %f;\n", env.me_aFactors[iFrame]);
    }
    
    strm->FPrintF_t("  }\n");
  }
  
  strm->FPrintF_t("}\n");
  strm->FPrintF_t("SE_ANIM_END;\n");
};
