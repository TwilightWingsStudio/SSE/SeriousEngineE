/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/Templates/StaticStackArray.h>
#include <Core/Base/CTString.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Core/Templates/StaticStackArray.cpp>

// FIXME: Move to header
/*
 * TODO: Add comment here
 */
struct stTable
{
  INDEX st_iID;
  CTString strName;
};

TStaticStackArray<struct stTable> _arStringTable;

// Add index in table
INDEX AddIndexToTable(CTString strName)
{
  _arStringTable.Push();

  INDEX ctStrings = _arStringTable.Count();
  _arStringTable[ctStrings - 1].strName = strName;
  _arStringTable[ctStrings - 1].st_iID = ctStrings;
  return ctStrings - 1;
}

// Find string in table and return his index, if not found add new and return his index
INDEX SM_StringToId(CTString strName)
{
  // FIXME: It's called "GetID", so why we add it? It's already get or create then get.
  // Call it ska_GetIDForSureFromStringTable '^..^'
  
  // FIXME: Can be reduced with ska_FindStringInTable
  // if(strName == "") {
  //   return -1;                                // Usefull cos AddIndexToTable hasn't this check
  // }
  // INDEX id = ska_FindStringInTable(strName);  // Double check here can be solved by changing FindString error vals 
  // if(id >= 0){
  //   return id;
  // } else {
  //   return AddIndexToTable(strName); 
  // }
  
  if (strName == "") {
    return -1;
  }
  
  INDEX ctStrings = _arStringTable.Count();

  for (INDEX i = 0; i < ctStrings;i++)
  {
    if (_arStringTable[i].strName == strName) {
      return i;
    }
  }

  return AddIndexToTable(strName);      
}

// Find string in table and return his index, if not found return -1
INDEX SM_FindId(CTString strName)
{
  // FIXME: So this one can be called GetID
  if (strName == "") {
    return -1;
  }
  
  INDEX ctStrings = _arStringTable.Count();
  for (INDEX i = 0; i < ctStrings; i++)
  {
    if (_arStringTable[i].strName == strName) {
      return i;
    }
  }
  return -1;
}

// Return name for index iIndex
CTString SM_IdToString(INDEX iIndex)
{
  INDEX ctStrings = _arStringTable.Count();

  if ((iIndex >= 0) && (iIndex <= ctStrings - 1))
  {
    return _arStringTable[iIndex].strName;
  }

  return "";
}
