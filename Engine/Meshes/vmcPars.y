%{
#include <Engine/StdH.h>

#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Shell.h>

#include <Core/Templates/DynamicContainer.cpp>

#include <Engine/Models/ModelData.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Meshes/SM_StringTable.h>
#include "ParsingSmbs.h"

extern BOOL bRememberSourceFN;
BOOL bOffsetAllreadySetMO = FALSE;
BOOL bStretchAlreadySetMO = FALSE;
%}

%{
#define YYERROR_VERBOSE 0
// if error occurs in parsing
void vyyerror(char *str)
{
  // just report the string
  _pShell->ErrorF("%s", str);
};
%}

/* BISON Declarations */

%union {
  int i;
  float f;
  const char *str;
  CModelObject *pmo;
  float f3[3];
  float f6[6];
}

%token <f> c_float
%token <i> c_int
%token <str> c_string
%token <pmo> c_modelobject

%type <f> float_const
%type <i> int_const
%type <pmo> k_ATTACHMENT
%type <f6> offset_opt
%type <f6> offset


%token k_SE_SMC
%token k_SE_END
%token k_NAME
%token k_TFNM
%token k_MESH
%token k_TEXTURES
%token k_CHILDREN
%token k_ATTACHMENT
%token k_OFFSET
%token k_STRETCH
%token k_ANIMATION
%token k_ALLFRAMESBBOX
%token k_COLOR

%start root_model

%%

/*/////////////////////////////////////////////////////////
 * Global structure of the source file.
 */

root_model
: offset_opt k_NAME c_string ';'
{
  if (_yy_mo == 0) {
    yyerror("_yy_mo = NULL");
  }

  // TODO: Do something with name.
}
'{' components '}'
;

components
: /*null*/
| component components 
;

component
: mesh
//| all_frames_bbox_opt
//| mdl_color_opt
| stretch_opt
| animation_opt
| child_model
;

mdl_color_opt
: /*null*/
| mdl_color
;

mdl_color
: k_COLOR c_int ';'
{
  COLOR c = $2;
  // _yy_mi->SetModelColor($2);
}
;

all_frames_bbox_opt
:/*null*/
| all_frames_bbox
;

all_frames_bbox
: k_ALLFRAMESBBOX float_const ',' float_const ',' float_const ',' float_const ',' float_const ',' float_const ';'
{
  // add new collision box to current model instance
  //_yy_mi->mi_cbAllFramesBBox.SetMin(FLOAT3D($2, $4, $6));
  //_yy_mi->mi_cbAllFramesBBox.SetMax(FLOAT3D($8,$10,$12));
}
;

offset_opt
:/*null*/
{
  // set offset with default offset values
  $$[0] = 0;
  $$[1] = 0;
  $$[2] = 0;
  $$[3] = 0;
  $$[4] = 0;
  $$[5] = 0;
}
| offset offset_opt
{
  // return new offset
  memcpy($$,$1,sizeof(float)*6);
}
;

offset
: k_OFFSET float_const ',' float_const ',' float_const ',' float_const ',' float_const ',' float_const ';' 
{
  // if offset is not set
  if (!bOffsetAllreadySetMO)
  {
    // set offset
    $$[0] = $2;
    $$[1] = $4;
    $$[2] = $6;
    $$[3] = $8;
    $$[4] = $10;
    $$[5] = $12;

    // mark it as set now
    bOffsetAllreadySetMO = TRUE;
  }
}
;

stretch_opt
: /*null*/
| stretch
;

stretch
: k_STRETCH float_const ',' float_const ',' float_const ';' 
{
  _yy_mo->StretchModelRelative(FLOAT3D($2, $4, $6));
}
;

mesh
: k_MESH k_TFNM c_string ';'
{
  _yy_mo->SetData_t((CTString)$3);
}
 textures_opt children_opt
;

animation_opt
:/*null*/
| animation
;

animation
: k_ANIMATION int_const ';' 
{
  _yy_mo->PlayAnim($2, AOF_LOOPING);
}
;

textures_opt
: /*null*/
| textures_opt textures
;

textures
: k_TEXTURES '{' textures_array'}'
;

textures_array
: /*null*/
| textures_array texture 
;

texture 
: c_string k_TFNM c_string ';' 
{
  CTString strKey($1);

  if (strKey == "Diffuse") {
    _yy_mo->mo_toTexture.SetData_t((CTString)$3);
  } else if (strKey == "Reflection") {
    _yy_mo->mo_toReflection.SetData_t((CTString)$3);
  } else if (strKey == "Specular") {
    _yy_mo->mo_toSpecular.SetData_t((CTString)$3);
  } else if (strKey == "Bump") {
    _yy_mo->mo_toBump.SetData_t((CTString)$3);
  }
}
;

children_opt
: /*null*/
| children_opt children
;

children
: k_CHILDREN '{' children_array '}'
;

children_array
: /*null*/
| children_array child_model 
;

child_model
: k_ATTACHMENT int_const ';' offset_opt k_NAME c_string ';'
{
  const INDEX iAttachmentId = $2;

  CModelData *pmd = (CModelData*)_yy_mo->GetData();
  if (iAttachmentId >= pmd->md_aampAttachedPosition.Count()) {
    ThrowF_t("Attachment %d does not exist in that model", iAttachmentId);
  };

  // remember current model instance in parent bone token
  $1 = _yy_mo;

  CAttachmentModelObject *pamo = _yy_mo->AddAttachmentModel(iAttachmentId);
  _yy_mo = &pamo->amo_moModelObject;

  if (bOffsetAllreadySetMO) {
    pamo->amo_plRelative = CPlacement3D(FLOAT3D($4[0], $4[1], $4[2]), FLOAT3D($4[3], $4[4], $4[5]));
  }

  bOffsetAllreadySetMO = FALSE;
  bStretchAlreadySetMO = FALSE;
}
// read child components
'{' components '}' 
{
   // set parent model instance to _yy_mo again
  _yy_mo = $1;
}
;

float_const
: c_float
{
  $$ = $1;
}
| c_int
{
  $$ = (float)$1;
}
;
  
int_const
: c_int
{
  $$ = $1;
}
;

%%