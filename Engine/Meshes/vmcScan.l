%option prefix="vyy"
%{
#include "ParsingSmbs.h"
#include "vmcPars.h"

#include <Core/Base/CTString.h>
#include <Core/Base/CTString.inl>
#include <Core/IO/FileName.h>
#include <Core/Base/ErrorReporting.h>

#include <Core/Templates/DynamicStackArray.cpp>

extern CTFileName _fnmApplicationPath;

int vyywrap(void)
{
  // no more buffers
  return 1;
};

// declarations for recursive SMC script parsing
struct BufferStackEntry {
  YY_BUFFER_STATE bse_bs;
  const char *bse_strName;
  const char *bse_strContents;
  int bse_iLineCt;
  BOOL bse_bParserEnd;
};

static BufferStackEntry _abseBufferStack[SMC_MAX_INCLUDE_LEVEL];
static int _ibsBufferStackTop = -1;

void VMC_PushBuffer(const char *strName, const char *strBuffer, BOOL bParserEnd)
{
  _ibsBufferStackTop++;

  _abseBufferStack[_ibsBufferStackTop].bse_strContents = strdup(strBuffer);
  _abseBufferStack[_ibsBufferStackTop].bse_strName = strdup(strName);
  _abseBufferStack[_ibsBufferStackTop].bse_iLineCt = 1;
  _abseBufferStack[_ibsBufferStackTop].bse_bParserEnd = bParserEnd;

  _abseBufferStack[_ibsBufferStackTop].bse_bs = vyy_scan_string(strBuffer);

  vyy_switch_to_buffer(_abseBufferStack[_ibsBufferStackTop].bse_bs);
}
BOOL VMC_PopBuffer(void)
{
  vyy_delete_buffer( _abseBufferStack[_ibsBufferStackTop].bse_bs);
  free((void*)_abseBufferStack[_ibsBufferStackTop].bse_strName);
  free((void*)_abseBufferStack[_ibsBufferStackTop].bse_strContents);
  BOOL bParserEnd = _abseBufferStack[_ibsBufferStackTop].bse_bParserEnd;

  _ibsBufferStackTop--;

  if (_ibsBufferStackTop>=0) {
    vyy_switch_to_buffer(_abseBufferStack[_ibsBufferStackTop].bse_bs);
  }
  return bParserEnd;
}
const char *VMC_GetBufferName(void)
{
  return _abseBufferStack[_ibsBufferStackTop].bse_strName;
}
int VMC_GetBufferLineNumber(void)
{
  return _abseBufferStack[_ibsBufferStackTop].bse_iLineCt;
}
int VMC_GetBufferStackDepth(void)
{
  return _ibsBufferStackTop;
}
const char *VMC_GetBufferContents(void)
{
  return _abseBufferStack[_ibsBufferStackTop].bse_strContents;
}
void VMC_CountOneLine(void)
{
  _abseBufferStack[_ibsBufferStackTop].bse_iLineCt++;
}
%}

%x COMMENT
%x INCLUDE

DIGIT		[0-9]
HEXDIGIT [0-9A-Fa-f]
DOUBLEQUOTE	\"
STRINGCONTENT	([^\"]|(\\\"))
NONEXP_FLT  ({DIGIT}+"."{DIGIT}*)
EXP_FLT (({DIGIT}+("."({DIGIT}*)?)?)("E"|"e")("+"|"-")?{DIGIT}+)

%%

"#INCLUDE"               BEGIN(INCLUDE);
"SE_SMC"                { return(k_SE_SMC); }
"SE_END"                { return(k_SE_END); }
"TFNM"                  { return(k_TFNM);   }
"NAME"                  { return(k_NAME);   }
"MESH"                  { return(k_MESH);   }
"TEXTURES"              { return(k_TEXTURES);}
"CHILDREN"              { return(k_CHILDREN);}
"ATTACHMENT"            { return(k_ATTACHMENT);}
"OFFSET"                { return(k_OFFSET);}
"STRETCH"               { return(k_STRETCH); }
"ANIMATION"             { return(k_ANIMATION); }
"COLOR"                 { return(k_COLOR);}
"ALLFRAMESBBOX"         { return(k_ALLFRAMESBBOX);}

<INCLUDE>[ \t]*"TFNM"?[ \t]*"\""      /* eat the whitespace and optional TFNM*/
<INCLUDE>[^"\""]*"\""   { /* got the include file name */

  if (VMC_GetBufferStackDepth() >= SMC_MAX_INCLUDE_LEVEL) {
    ThrowF_t("File '%s' line %d\nIncludes nested too deeply '%s'",VMC_GetBufferName(), VMC_GetBufferLineNumber(),yytext);
  }
  char strFileName[256];
  strcpy(strFileName, yytext);
  strFileName[strlen(strFileName)-1] = 0;

  CTString strIncludeFile;
  try {
    strIncludeFile.Load_t(CTString(strFileName));
    VMC_PushBuffer(strFileName, strIncludeFile, FALSE);

  } catch(char *strError) {
    (void)strError;
    ThrowF_t("File '%s'\n Could not open '%s' (line %d)",VMC_GetBufferName(), strFileName, VMC_GetBufferLineNumber());
  }
  BEGIN(INITIAL);
}
<INCLUDE>.    {  /* something unrecognized inside include statement */
  BEGIN(INITIAL);
  ThrowF_t("File '%s'\n Wrong syntax for include statement",VMC_GetBufferName());
}
<<EOF>> {
  if (VMC_PopBuffer()) {
    yyterminate();
  }
}


 /* single character operators and punctuations */
";"|","|"{"|"}" {
  return(yytext[0]);}

 /* constants */

"-"?{DIGIT}+                  { vyylval.i = atoi(yytext); return(c_int); }
"0x"{HEXDIGIT}+               { vyylval.i = strtoul(yytext+2, NULL, 16); return(c_int);}
"-"?{NONEXP_FLT}("f"|"F")?    { vyylval.f = (float) atof(yytext); return(c_float); }
"-"?{EXP_FLT}("f"|"F")?       { vyylval.f = (float) atof(yytext); return(c_float); }
"\""{STRINGCONTENT}*"\""  { 
  char *strNew;
  // remove double-quotes
  yytext[strlen(yytext)-1] = 0;
  strNew = yytext+1;
  vyylval.str = strNew;
  return(c_string); 
}

 /* eat up comments */
"/*"          { BEGIN(COMMENT); }
<COMMENT>"* /" { BEGIN(INITIAL); }
<COMMENT>.    {}
"//"[^\n]*\n { VMC_CountOneLine(); }

 /* eat up whitespace */
[ \t]+	 {
}
 /* eat up linefeeds and count lines in all conditions */
<*>\n	{
  VMC_CountOneLine();;
}

 /* for all unrecognized characters */
. {
  // report an error
  ThrowF_t("File '%s'\n Unrecognized character '%c' (line %d)", VMC_GetBufferName(), yytext[0], VMC_GetBufferLineNumber());
  //ThrowF_t("Unrecognized character '%c' in line %d)", yytext[0], _yy_iLine );
}

%%

