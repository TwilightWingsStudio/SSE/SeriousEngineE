/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SKELMESH_H
#define SE_INCL_SKELMESH_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/CTString.h>
#include <Core/IO/Resource.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Placement.h>
#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/StaticArray.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Graphics/Shader.h>
#include <Engine/Meshes/Mesh.h>

// Mesh Lod Flags
#define ML_HALF_FACE_FORWARD         (1UL << 0) // half face forward
#define ML_FULL_FACE_FORWARD         (1UL << 1) // full face forward
#define ML_USE_VERTEX_PROGRAM        (1UL << 2) // use hardware vertex shaders
#define ML_SURFACE_RELATIVE_INDICES  (1UL << 3) // vertex indices in surfaces are relative to that surface
#define ML_NORMALIZED_WEIGHTS        (1UL << 4) // are weights normalized

// Mesh Surface Flags
#define MS_MESH_SUBSURFACE   (1UL << 0) // this is mesh subsurface
#define MS_DYNAMIC_SURFACE   (1UL << 1) // this is dynamic surface (has morphs)

//! Class that represents one level of detail for skeletal mesh.
class ENGINE_API CSkelMeshLod
{
  public:
    //! Constructor.
    CSkelMeshLod()
    {
      mlod_fMaxDistance = -1;
      mlod_ulFlags =  0;
    };

    //! Destructor.
    ~CSkelMeshLod() {}
    
    inline FLOAT GetMaxDistance() const
    {
      return mlod_fMaxDistance;
    }

    inline ULONG GetFlags() const
    {
      return mlod_ulFlags;
    }

    //! Write to stream in text format.
    void Write_AM_t(CTStream *ostrFile); // throw char *

    //! Write shader params to stream in text format.
    void Write_SHP_t(CTStream *ostrFile);

  public:
    FLOAT mlod_fMaxDistance;
    ULONG mlod_ulFlags;

    TStaticArray<struct MeshVertex>    mlod_aVertices;   // vertices
    TStaticArray<struct MeshNormal>    mlod_aNormals;	   // normals
    TStaticArray<struct MeshUVMap>     mlod_aUVMaps;     // UV maps
    TStaticArray<struct MeshSurface>   mlod_aSurfaces;   // surfaces
    TStaticArray<struct MeshWeightMap> mlod_aWeightMaps; // weight maps
    TStaticArray<struct MeshMorphMap>  mlod_aMorphMaps;  // morph maps
    TStaticArray<struct MeshVertexWeightInfo> mlod_aVertexWeights;

    CTString mlod_fnSourceFile;// file name of ascii am file, used in Ska studio
};

//! Mesh vertex (legacy format).
struct ENGINE_API MeshVertexOld
{
  FLOAT x, y, z;
  ULONG dummy; // 16 byte alingment
};

//! Mesh normal (legacy format).
struct ENGINE_API MeshNormalOld
{
  FLOAT nx, ny, nz;
  ULONG dummy; // 16 byte alingment
};

/*
 * TODO: Add comment here
 */
struct ENGINE_API MeshUVMap
{
  ULONG muv_iID;
  TStaticArray<struct MeshTexCoord>  muv_aTexCoords; // texture coordinates
};

//! Describes mesh surface/material i.e. group of polygons.
struct ENGINE_API MeshSurface
{
  INDEX msrf_iFirstVertex;
  INDEX msrf_ctVertices;
  INDEX msrf_iSurfaceID;
  CShaderClass *msrf_pShader;
  ShaderParams msrf_ShadingParams;
  ULONG msrf_ulFlags;
  TStaticArray<UBYTE> msrf_aubRelIndexTable;           // Table of surface relative mesh weight map indices
  TStaticArray<struct MeshTriangle> msrf_aTriangles;		// list of triangles
};

/*
 * TODO: Add comment here
 */
struct ENGINE_API MeshTriangle
{
  INDEX iVertex[3];
};

/*
 * TODO: Add comment here
 */
struct MeshTriangle16
{
  UWORD iVertex[3];
};

/*
 * TODO: Add comment here
 */
struct ENGINE_API MeshWeightMap
{
  int mwm_iID;
  TStaticArray<struct MeshVertexWeight> mwm_aVertexWeight; // weight maps
};

/*
 * TODO: Add comment here
 */
struct ENGINE_API MeshVertexWeight
{
  INDEX mww_iVertex;      // absolute index of the vertex this weight refers to
  FLOAT mww_fWeight;      // weight for this bone [0.0 - 1.0]
};

/*
 * TODO: Add comment here
 */
struct ENGINE_API MeshMorphMap
{
  INDEX mmp_iID;
  BOOL  mmp_bRelative;
  TStaticArray<struct MeshVertexMorph> mmp_aMorphMap; // Morph maps
};

//! Morph Map Entry (Legacy).
struct ENGINE_API MeshVertexMorphOld
{
  INDEX mwm_iVxIndex;      // absolute index of the vertex this weight refers to
  FLOAT mwm_x;
  FLOAT mwm_y;
  FLOAT mwm_z;
  FLOAT mwm_nx;
  FLOAT mwm_ny;
  FLOAT mwm_nz;
  ULONG dummy;        // 32 byte padding
};

//! Morph Map Entry.
struct MeshVertexMorph
{
  INDEX mwm_iVxIndex;      // absolute index of the vertex this weight refers to
  FLOAT mwm_x;
  FLOAT mwm_y;
  FLOAT mwm_z;
  FLOAT mwm_nx;
  FLOAT mwm_ny;
  FLOAT mwm_nz;
};

#define MAX_BPV 4

struct MeshVertexWeightInfo
{
  UBYTE mvwi_aubIndices[MAX_BPV]; // max of 4 weight indices per vertex
  UBYTE mvwi_aubWeights[MAX_BPV]; // max of 4 weight factors per vertex
};


//! A resource that describes skeletal mesh.
class ENGINE_API CSkelMesh : public CMesh
{
  public:
    //! Constructor.
    CSkelMesh();

    //! Destructor.
    ~CSkelMesh();

    //! Optimize all LODs.
    void Optimize(void);

    //! Optimize one LOD.
    void OptimizeLod(CSkelMeshLod &mLod);
    void NormalizeWeights(void);
    void NormalizeWeightsInLod(CSkelMeshLod &mlod);

    //! Add LOD to mesh.
    void AddMeshLod(CSkelMeshLod &mlod);

    //! Remove LOD from mesh.
    void RemoveMeshLod(CSkelMeshLod *pmlodRemove);

    //! Returns reference to mesh lod.
    inline CSkelMeshLod &GetLod(ULONG i)
    {
      ASSERT(i < msh_aLods.Count());
      return msh_aLods[i];
    }

    //! Read from stream.
    void Read_t(CTStream *istrFile);  // throw char *

    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char *

    //! Free memory.
    void Clear(void);

    //! Returns amount of used RAM.
    SLONG GetUsedMemory(void);
    
    //! TODO: Add comment.
    virtual UBYTE GetType() const;

  public:
    ULONG msh_iVersion;
    TStaticArray<class CSkelMeshLod> msh_aLods;
};

ENGINE_API void ChangeSurfaceShader_t(MeshSurface &msrf,CTString fnNewShader);

#endif  /* include-once check. */