/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ANIMSET_H
#define SE_INCL_ANIMSET_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticArray.h>
#include <Core/IO/Resource.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Quaternion.h>

#define ANG_COMPRESIONMUL 182.041666666666666666666666666667f;

#define AN_LOOPING      (1UL << 0) // looping animation
#define AN_NORESTART    (1UL << 1) // dont restart anim
#define AN_PAUSED       (1UL << 2)
#define AN_CLEAR        (1UL << 3) // do new clear state before adding animation
#define AN_CLONE        (1UL << 4) // do new cloned state before adding animation
#define AN_NOGROUP_SORT (1UL << 5) // dont sort animations by groups

#define CLEAR_STATE_LENGTH  0.2f
#define CLONED_STATE_LENGTH  0.2f

/*
 * TODO: Add comment here
 */
struct AnimPos
{
  UWORD ap_iFrameNum;  // frame number
  FLOAT3D ap_vPos;     // bone pos
};

/*
 * TODO: Add comment here
 */
struct AnimRotOpt
{
  UWORD aro_iFrameNum;  //frame number
  UWORD aro_ubH,aro_ubP;
  ANGLE aro_aAngle;
};

/*
 * TODO: Add comment here
 */
struct AnimRot
{
  UWORD ar_iFrameNum;  //frame number
  FLOATquat3D ar_qRot; //bone rot
};

/*
 * TODO: Add comment here
 */
class ENGINE_API CBoneAnimation
{
  public:
    //! Write to stream in text format.
    void Write_AA_t(CTStream *ostrFile); // throw char *
  
  public:
    int an_iID;
    INDEX an_iFrames;
    FLOAT an_fSecPerFrame;
    FLOAT an_fTreshold;
    BOOL an_bCompresed;// are quaternions in animation compresed
    TStaticArray<struct MorphEnvelope> an_ameMorphs;
    TStaticArray<struct BoneEnvelope> an_abeBones;
    CTString an_fnSourceFile;// name of ascii aa file, used in Ska studio
    BOOL an_bCustomSpeed; // animation has custom speed set in animset list file, witch override speed from anim file
};

/*
 * TODO: Add comment here
 */
struct MorphEnvelope
{
  int me_iMorphMapID;
  TStaticArray<FLOAT> me_aFactors;
};

/*
 * TODO: Add comment here
 */
struct BoneEnvelope
{
  int be_iBoneID;
  Matrix12 be_mDefaultPos; // default pos
  TStaticArray<struct AnimPos> be_apPos;// array of compresed bone positions
  TStaticArray<struct AnimRot> be_arRot;// array if compresed bone rotations
  TStaticArray<struct AnimRotOpt> be_arRotOpt;// array if optimized compresed bone rotations
  FLOAT be_OffSetLen;
};

//! A resource that contains skeletal animations.
class ENGINE_API CBoneAnimSet : public CResource
{
  public:
    //! Constructor.
    CBoneAnimSet();

    //! Desctructor.
    ~CBoneAnimSet();

    //! Optimize all animations.
    void Optimize();

    //! Optimize specific animation.
    void OptimizeAnimation(CBoneAnimation &an, FLOAT fTreshold);

    //! Add animation to set.
    void AddAnimation(CBoneAnimation *pan);

    //! Remove animation from set.
    void RemoveAnimation(CBoneAnimation *pan);

    //! Read from stream.
    void Read_t(CTStream *istrFile); // throw char *

    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char *

    //! Clean up all the data.
    void Clear(void);

    //! Returns amount of used RAM.
    SLONG GetUsedMemory(void);
    
    //! TODO: Add comment.
    virtual UBYTE GetType() const;

  public:
    TStaticArray<class CBoneAnimation> as_Anims;
};

// If rotations are compresed does loader also fills array of uncompresed rotations
ENGINE_API void RememberUnCompresedRotatations(BOOL bRemember);

#endif  /* include-once check. */