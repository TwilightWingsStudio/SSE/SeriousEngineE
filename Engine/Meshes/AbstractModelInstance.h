/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ABSTRACTMODELINSTANCE_H
#define SE_INCL_ABSTRACTMODELINSTANCE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Objects/GameObject.h>

enum ModelInstanceType
{
  MIT_INVALID = 0,
  MIT_VERTEX,
  MIT_SKELETAL,
};

//! Represents model that can perform on scene.
class ENGINE_API CAbstractModelInstance : public CGameObject
{
  public:
    //! Copy data from another instance of same class.
    virtual void Copy(CAbstractModelInstance &mi) = 0;

    //! Returns instance type.
    virtual ModelInstanceType GetInstanceType() const = 0;

    //! Stretch model instance.
    virtual void StretchModel(const FLOAT3D &vStretch) = 0;

    //! Stretch model instance without attachments.
    virtual void StretchSingleModel(const FLOAT3D &vStretch) = 0;
};

#endif /* include-once check. */