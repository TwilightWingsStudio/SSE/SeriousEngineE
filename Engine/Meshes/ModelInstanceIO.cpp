/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Resources/ReplaceFile.h>
#include <Core/IO/Stream.h>
#include <Core/Base/ErrorReporting.h>
#include <Engine/Meshes/ModelInstance.h>
#include <Engine/Meshes/ModelConfiguration.h>
#include <Engine/Meshes/SkelMesh.h>
#include <Engine/Meshes/Skeleton.h>
#include <Engine/Meshes/BoneAnimSet.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Engine/Resources/ResourceManager.h>

static void ReadModelInstanceOld_t(CTStream &strm, CModelInstance &mi)
{
  strm.ExpectID_t("SKMI");
  // read model instance name
  CTString strModelInstanceName;
  strm>>strModelInstanceName;
  mi.SetName(strModelInstanceName);

  // read all mesh instances for this model instance
  INDEX ctmshi = 0; // mesh instance count
  INDEX ctti = 0;   // texture instance count
  INDEX ctas = 0;   // animset count
  INDEX ctcb = 0;   // collision boxes count
  INDEX ctal = 0;   // anim lists count
  INDEX ctpa = 0;   // played anims count
  INDEX ctcmi = 0;  // child model instance count

  strm >> ctmshi;
  mi.mi_aMeshInst.New(ctmshi);
  
  // for each mesh
  for (INDEX imshi = 0;imshi < ctmshi; imshi++)
  {
    MeshInstance &mshi = mi.mi_aMeshInst[imshi];
    CTFileName fnMesh;

    // read binary mesh file name
    strm >> fnMesh;
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SKELMESH, fnMesh);
    mshi.mi_pMesh = static_cast<CSkelMesh *>(pser);

    // read texture instances for this mesh instance
    strm >> ctti;
    mshi.mi_tiTextures.New(ctti);

    // for each texture instance
    for (INDEX iti = 0; iti < ctti; iti++)
    {
      // read texture file name and texture ID
      TextureInstance &ti = mshi.mi_tiTextures[iti];

      CTFileName fnTex;
      CTString strTexID;
      strm >> fnTex;
      strm >> strTexID;

      ti.SetName(strTexID);
      ti.ti_toTexture.SetData_t(fnTex);
    }
  }

  // read this model instance skeleton binary file name
  BOOL bHasSkeleton;
  mi.mi_psklSkeleton = NULL;

  strm >> bHasSkeleton;

  if (bHasSkeleton) {
    CTFileName fnSkeleton;
    strm >> fnSkeleton;
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SKELETON, fnSkeleton);
    mi.mi_psklSkeleton = static_cast<CSkeleton *>(pser);
  }

  // read animsets file names
  strm >> ctas;

  // for each animset in model instance
  for (INDEX ias = 0; ias < ctas; ias++)
  {
    // read animset binary file name
    CTFileName fnAnimSet;
    strm >> fnAnimSet;
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_BONEANIMSET, fnAnimSet);
    CBoneAnimSet *pas = static_cast<CBoneAnimSet *>(pser);
    mi.mi_aAnimSet.Add(pas);
  }

  // read collision boxes
  strm >> ctcb;
  mi.mi_cbAABox.New(ctcb);

  // for each collision box
  for (INDEX icb = 0; icb < ctcb; icb++)
  {
    CollisionBox &cb = mi.mi_cbAABox[icb];

    // read collision box
    strm>>cb.Min();
    strm>>cb.Max();
  }

  // read index of current collision box
  strm >> mi.mi_iCurentBBox;

  // read stretch
  strm >> mi.mi_vStretch;
  // read color
  strm >> mi.mi_colModelColor;

  // read animation queue
  AnimQueue &aq = mi.mi_aqAnims;
  strm >> ctal;
  if (ctal > 0) aq.aq_Lists.Push(ctal);

  // for each anim list
  for (INDEX ial = 0; ial < ctal; ial++)
  {
    AnimList &al = aq.aq_Lists[ial];

    // read anim list and get all played anims
    strm >> al.al_fStartTime;
    strm >> al.al_fFadeTime;
    strm >> ctpa;

    if (ctpa > 0) al.al_PlayedAnims.Push(ctpa);

    // for each played anim
    for (INDEX ipa = 0; ipa < ctpa; ipa++)
    {
      // save played anim
      PlayedAnim &pa = al.al_PlayedAnims[ipa];
      strm >> pa.pa_fStartTime;
      strm >> pa.pa_ulFlags;
      strm >> pa.pa_Strength;
      strm >> pa.pa_GroupID;

      CTString strPlayedAnimID;
      strm >> strPlayedAnimID;
      pa.pa_iAnimID = SM_StringToId(strPlayedAnimID);
    }
  }

  // read model instance offset and parent bone
  strm.Read_t(&mi.mi_qvOffset,sizeof(QVect));
  CTString strParenBoneID;
  strm >> strParenBoneID;
  mi.mi_iParentBoneID = SM_StringToId(strParenBoneID);

  // read model instance children
  strm >> ctcmi;

  // for each child model instance
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    // create empty model instance
    CModelInstance *pcmi = CreateModelInstance("Temp");
    // add as child to parent model isntance
    mi.mi_cmiChildren.Add(pcmi);
    // read child model instance
    ReadModelInstance_t(strm, *pcmi, FALSE);
  }
}

static void ReadMeshInstances_t(CTStream &strm, CModelInstance &mi)
{
  INDEX ctmshi = 0;
  INDEX ctti = 0;

  strm.ExpectID_t("MSHI");
  // Read mesh instance
  strm >> ctmshi;
  mi.mi_aMeshInst.New(ctmshi);

  // for each mesh
  for (INDEX imshi = 0; imshi < ctmshi; imshi++)
  {
    MeshInstance &mshi = mi.mi_aMeshInst[imshi];
    CTFileName fnMesh;

    strm.ExpectID_t("MESH");
    // read binary mesh file name
    strm >> fnMesh;
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SKELMESH, fnMesh);
    mshi.mi_pMesh = static_cast<CSkelMesh *>(pser);

    // read texture instances for this mesh instance
    strm.ExpectID_t("MITS");  // mesh instance texture instance
    strm >> ctti;
    mshi.mi_tiTextures.New(ctti);

    // for each texture instance
    for (INDEX iti = 0; iti < ctti; iti++)
    {
      // read texture file name and texture ID
      TextureInstance &ti = mshi.mi_tiTextures[iti];
      strm.ExpectID_t("TITX");  // texture instance texture
      CTFileName fnTex;
      CTString strTexID;
      strm >> fnTex;
      strm >> strTexID;

      ti.SetName(strTexID);
      ti.ti_toTexture.SetData_t(fnTex);
    }
  }
}

static void ReadSkeleton_t(CTStream &strm, CModelInstance &mi)
{
  // read this model instance skeleton binary file name
  BOOL bHasSkeleton;
  mi.mi_psklSkeleton = NULL;

  strm.ExpectID_t("SKEL");
  strm >> bHasSkeleton;

  if (bHasSkeleton) {
    CTFileName fnSkeleton;
    strm >> fnSkeleton;
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SKELETON, fnSkeleton);
    mi.mi_psklSkeleton = static_cast<CSkeleton *>(pser);
  }
}

static void ReadAnimSets_t(CTStream &strm, CModelInstance &mi)
{
  INDEX ctas = 0;
  strm.ExpectID_t("ANAS");
  // read animsets file names
  strm >> ctas;

  // for each animset in model instance
  for (INDEX ias = 0; ias < ctas; ias++)
  {
    // read animset binary file name
    CTFileName fnAnimSet;
    strm >> fnAnimSet;
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_BONEANIMSET, fnAnimSet);
    CBoneAnimSet *pas = static_cast<CBoneAnimSet *>(pser);
    mi.mi_aAnimSet.Add(pas);
  }
}

static void ReadAnimQueue_t(CTStream &strm, CModelInstance &mi)
{
  const BOOL bNitroFamily = strm.GetFlags() & WSF_NITROFAMILYBMC;

  INDEX ctal = 0;
  INDEX ctpa = 0;
  strm.ExpectID_t("MIAQ");  // model instance animation queue

  // read animation queue
  AnimQueue &aq = mi.mi_aqAnims;
  strm >> ctal;
  if (ctal > 0) aq.aq_Lists.Push(ctal);

  // for each anim list
  for (INDEX ial = 0; ial < ctal; ial++)
  {
    AnimList &al = aq.aq_Lists[ial];
    strm.ExpectID_t("AQAL");  // animation queue animation list
    // read anim list and get all played anims
    strm >> al.al_fStartTime;
    strm >> al.al_fFadeTime;
    strm >> ctpa;

    if (ctpa > 0) al.al_PlayedAnims.Push(ctpa);

    // for each played anim
    for (INDEX ipa = 0; ipa < ctpa; ipa++)
    {
      // read played anim
      PlayedAnim &pa = al.al_PlayedAnims[ipa];
      strm.ExpectID_t("ALPA");  // animation list played anim
      strm >> pa.pa_fStartTime;
      strm >> pa.pa_ulFlags;
      strm >> pa.pa_Strength;
      strm >> pa.pa_GroupID;
      
      // [SEE]
      if (bNitroFamily) {
        ULONG ulDummy;
        strm >> ulDummy;
      }

      CTString strPlayedAnimID;
      strm >> strPlayedAnimID;
      pa.pa_iAnimID = SM_StringToId(strPlayedAnimID);

      if (strm.PeekID_t() == CChunkID("PASP")) {
        strm.ExpectID_t("PASP");  // played animation speed
        strm >> pa.pa_fSpeedMul;
      }
    }
  }
}

static void ReadCollisionBoxes_t(CTStream &strm, CModelInstance &mi)
{
  INDEX ctcb = 0;
  strm.ExpectID_t("MICB");  // model instance collision boxes

  // read collision boxes
  strm >> ctcb;
  mi.mi_cbAABox.New(ctcb);

  // for each collision box
  for (INDEX icb = 0; icb < ctcb; icb++)
  {
    CollisionBox &cb = mi.mi_cbAABox[icb];
    CTString strName;

    // read collision box
    strm >> cb.Min();
    strm >> cb.Max();
    strm >> strName;
    cb.SetName(strName);
  }

  strm.ExpectID_t("AFBB");  // all frames bounding box
  // read all frames bounding box
  strm>>mi.mi_cbAllFramesBBox.Min();
  strm>>mi.mi_cbAllFramesBBox.Max();
}

static void ReadOffsetAndChildren_t(CTStream &strm, CModelInstance &mi)
{
  INDEX ctcmi = 0;
  strm.ExpectID_t("MIOF");  // model instance offset

  // read model instance offset and parent bone
  strm.Read_t(&mi.mi_qvOffset,sizeof(QVect));
  CTString strParenBoneID;
  strm >> strParenBoneID;
  mi.mi_iParentBoneID = SM_StringToId(strParenBoneID);

  strm.ExpectID_t("MICH");  // model instance child
  // read model instance children
  strm >> ctcmi;

  // for each child model instance
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    // create empty model instance
    CModelInstance *pcmi = CreateModelInstance("Temp");
    // add as child to parent model isntance
    mi.mi_cmiChildren.Add(pcmi);
    // read child model instance
    ReadModelInstance_t(strm, *pcmi, FALSE);
  }
}

static void ReadModelInstanceNew_t(CTStream &strm, CModelInstance &mi, BOOL bMarkInStock)
{
  strm.ExpectID_t("MI03");  // model instance 03
  
  // [SEE]
  // Read model instance source file name it its writen.
  if(strm.PeekID_t() == CChunkID("MISF")) {
    strm.ExpectID_t("MISF");

    CTFileName fnmSource;
    strm >> fnmSource;
    ASSERT(fnmSource != "");
    ASSERT(mi.mi_pInStock == NULL);
    
    if (bMarkInStock) {
      CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_MODELCONFIGURATION, fnmSource);
      mi.mi_pInStock = static_cast<CModelConfiguration *>(pser);
      ASSERT(mi.mi_pInStock != NULL);
    }
  }

  // Read model instance name
  CTString strModelInstanceName;
  strm >> strModelInstanceName;
  mi.SetName(strModelInstanceName);

  strm >> mi.mi_iCurentBBox; // read index of current collision box
  strm >> mi.mi_vStretch; // read stretch
  strm >> mi.mi_colModelColor; // read color

  ReadMeshInstances_t(strm,mi);
  ReadSkeleton_t(strm, mi);
  ReadAnimSets_t(strm, mi);
  ReadAnimQueue_t(strm, mi);
  ReadCollisionBoxes_t(strm, mi);
  ReadOffsetAndChildren_t(strm, mi);

  strm.ExpectID_t("ME03"); // model instance end 03
}

void ReadModelInstance_t(CTStream &strm, CModelInstance &mi, BOOL bMarkInStock)
{
  // is model instance writen in old format
  if (strm.PeekID_t() == CChunkID("SKMI")) {
    ReadModelInstanceOld_t(strm, mi);
  // is model instance writen in new format
  } else if (strm.PeekID_t() == CChunkID("MI03")) {
    ReadModelInstanceNew_t(strm, mi, bMarkInStock);
  // unknown format
  } else {
    strm.Throw_t("Unknown model instance format");
    ASSERT(FALSE);
  }
}

void SkipModelInstance_t(CTStream &strm)
{
  CModelInstance miDummy;
  ReadModelInstance_t(strm, miDummy, FALSE);
}

static void WriteMeshInstances_t(CTStream &strm, CModelInstance &mi)
{
  // write mesh instance header
  strm.WriteID_t("MSHI");

  // write all mesh instances for this model instance
  INDEX ctmshi=mi.mi_aMeshInst.Count();
  strm << ctmshi;

  // for each mesh
  for (INDEX imshi = 0; imshi < ctmshi; imshi++)
  {
    MeshInstance &mshi = mi.mi_aMeshInst[imshi];
    CTFileName fnMesh = mshi.mi_pMesh->GetName();
    strm.WriteID_t("MESH");
    // write binary mesh file name
    strm << fnMesh;

    strm.WriteID_t("MITS");
    // write texture instances for this mesh instance
    INDEX ctti = mshi.mi_tiTextures.Count();
    strm << ctti;

    // for each texture instance
    for (INDEX iti = 0; iti < ctti; iti++)
    {
      // write texture file name and texture ID
      TextureInstance &ti = mshi.mi_tiTextures[iti];
      CTextureData *ptd = (CTextureData*)ti.ti_toTexture.GetData();
      CTFileName fnTex = ptd->GetName();
      CTString strTexID = SM_IdToString(ti.GetID());
      strm.WriteID_t("TITX");
      strm << fnTex;
      strm << strTexID;
    }
  }
}

static void WriteSkeleton_t(CTStream &strm, CModelInstance &mi)
{
  // write this model instance skeleton binary file name
  CSkeleton *pSkeleton = mi.mi_psklSkeleton;
  BOOL bHasSkeleton = (pSkeleton != NULL);
  strm.WriteID_t("SKEL");
  strm << bHasSkeleton;

  if (bHasSkeleton) {
    CTFileName fnSkeleton = pSkeleton->GetName();
    strm<<fnSkeleton;
  }
}

static void WriteAnimSets(CTStream &strm, CModelInstance &mi)
{
  strm.WriteID_t("ANAS");
  // write animsets file names
  INDEX ctas = mi.mi_aAnimSet.Count();
  strm << ctas;

  // for each animset in model instance
  for (INDEX ias = 0; ias < ctas; ias++)
  {
    CBoneAnimSet &as = mi.mi_aAnimSet[ias];
    CTFileName fnAnimSet = as.GetName();
    // write animset binary file name
    strm << fnAnimSet;
  }
}

static void WriteCollisionBoxes(CTStream &strm, CModelInstance &mi)
{
  strm.WriteID_t("MICB");
  // write collision boxes and index of current collision box
  INDEX ctcb = mi.mi_cbAABox.Count();
  strm << ctcb;

  // for each collision box
  for (INDEX icb = 0; icb < ctcb; icb++)
  {
    CollisionBox &cb = mi.mi_cbAABox[icb];

    // write collision box
    strm << cb.Min();
    strm << cb.Max();
    strm << cb.GetName();
  }

  // write all frames bounding box
  strm.WriteID_t("AFBB");
  strm << mi.mi_cbAllFramesBBox.Min();
  strm << mi.mi_cbAllFramesBBox.Max();
}


static void WriteOffsetAndChildren(CTStream &strm, CModelInstance &mi)
{
  strm.WriteID_t("MIOF");  // model instance offset
  // write model instance offset and parent bone
  strm.Write_t(&mi.mi_qvOffset,sizeof(QVect));
  CTString strParenBoneID = SM_IdToString(mi.mi_iParentBoneID);
  strm << strParenBoneID;

  strm.WriteID_t("MICH");  // model instance child
  // write model instance children
  INDEX ctcmi = mi.mi_cmiChildren.Count();
  strm << ctcmi;

  // for each child model instance
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    CModelInstance &cmi = mi.mi_cmiChildren[icmi];
    // save child model instance
    WriteModelInstance_t(strm, cmi, FALSE);
  }
}

static void WriteAnimQueue_t(CTStream &strm, CModelInstance &mi)
{
  const BOOL bNitroFamily = strm.GetFlags() & WSF_NITROFAMILYBMC;

  strm.WriteID_t("MIAQ");  // model instance animation queue
  // write animation queue
  AnimQueue &aq = mi.mi_aqAnims;
  INDEX ctal = aq.aq_Lists.Count();
  strm << ctal;

  // for each anim list
  for (INDEX ial = 0; ial < ctal; ial++)
  {
    AnimList &al = aq.aq_Lists[ial];

    strm.WriteID_t("AQAL");  // animation queue animation list

    // save anim list and get all played anims
    strm << al.al_fStartTime;
    strm << al.al_fFadeTime;
    INDEX ctpa = al.al_PlayedAnims.Count();
    strm << ctpa;

    // for each played anim
    for (INDEX ipa = 0; ipa < ctpa; ipa++)
    {
      // save played anim
      PlayedAnim &pa = al.al_PlayedAnims[ipa];
      strm.WriteID_t("ALPA");  // animation list played anim
      strm << pa.pa_fStartTime;
      strm << pa.pa_ulFlags;
      strm << pa.pa_Strength;
      strm << pa.pa_GroupID;

      // [SEE]
      if (bNitroFamily) {
        ULONG ulDummy = 0;
        strm << ulDummy;
      }

      CTString strPlayedAnimID = SM_IdToString(pa.pa_iAnimID);
      strm << strPlayedAnimID;

      // write anim speed mul
      strm.WriteID_t("PASP");  // played animation speed
      strm << pa.pa_fSpeedMul;
    }
  }
}

void WriteModelInstance_t(CTStream &strm, CModelInstance &mi, BOOL bFromStock)
{
  strm.WriteID_t("MI03"); // model instance 03
  
  // [SEE]
  if (bFromStock && mi.mi_pInStock != NULL) {
    strm.WriteID_t("MISF");
    strm << mi.mi_pInStock->ser_FileName;
  }

  strm << mi.GetName(); // write model instance name
  strm << mi.mi_iCurentBBox; // write index of current collision box
  strm << mi.mi_vStretch; // write stretch
  strm << mi.mi_colModelColor; // write color

  WriteMeshInstances_t(strm,mi);
  WriteSkeleton_t(strm, mi);
  WriteAnimSets(strm, mi);
  WriteAnimQueue_t(strm, mi);
  WriteCollisionBoxes(strm, mi);
  WriteOffsetAndChildren(strm, mi);
  strm.WriteID_t("ME03"); // model instance end 03
}

//! Create model instance and obtain model instance from stock in itю
extern void ObtainModelInstance_t(CModelInstance &mi, const CTString &fnSmcFile)
{
  ASSERT(&mi != NULL);

  // Obtain model instance from stock
  CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_MODELCONFIGURATION, fnSmcFile);
  CModelConfiguration *pmis = static_cast<CModelConfiguration *>(pser);
  ASSERT(pmis!=NULL && pmis->GetModelInstance() != NULL);

  mi.Copy(*pmis->GetModelInstance()); // Make copy of model instance in stock (add new reference in stock)
  _pResourceMgr->Release(mi.mi_pInStock); // Remove one reference from stock
}

//! Obtain model instance from stock in existing model instanceю
extern CModelInstance *ObtainModelInstance_t(const CTString &fnSmcFile)
{
  CModelInstance *pmi = CreateModelInstance("");
  ObtainModelInstance_t(*pmi, fnSmcFile);
  return pmi;
}