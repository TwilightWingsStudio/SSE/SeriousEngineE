/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/Meshes/ModelInstance.h>
#include <Engine/Meshes/ModelConfiguration.h>
#include <Engine/Meshes/Skeleton.h>
#include <Engine/Meshes/SM_Render.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Engine/Meshes/ParsingSmbs.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>
#include <Core/Math/Quaternion.h>
#include <Core/Templates/DynamicStackArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>
#include <Engine/Resources/ResourceManager.h>

// Does parser remember smc source files?
BOOL bRememberSourceFN = FALSE;

// Pointer to model instance for parser
CModelInstance *_yy_mi = NULL;

// Calculate fade factor of animation list in animqueue
FLOAT CalculateFadeFactor(AnimList &alList)
{
  if (alList.al_fFadeTime == 0) {
    return 1.0f;
  }

  FLOAT fFadeFactor = (_pTimer->GetLerpedCurrentTick() - alList.al_fStartTime) / alList.al_fFadeTime;
  return Clamp(fFadeFactor, 0.0f, 1.0f);
}

// Create model instance
CModelInstance *CreateModelInstance(CTString strName)
{
  CModelInstance *pmi = new CModelInstance;
  pmi->SetName(strName);
  return pmi;
}

// TODO: Add comment here
void DeleteModelInstance(CModelInstance *pmi)
{
  ASSERT(pmi != NULL);
  
  // If model instance is valid
  if (pmi != NULL) {
    pmi->Clear(); // Clear model instance
    delete pmi;   // Delete model instance
    pmi = NULL;
  }
}

// Parse smc file in existing model instance
void ParseSmcFile_t(CModelInstance &mi, const CTString &fnSmcFile)
{
  // Clear given model instance before parsing
  mi.Clear();

  CTFileName fnFileName = fnSmcFile;
  try {
    fnFileName.RemoveApplicationPath_t();
  } catch (char *) {} // FIXME: Nani?

  CTString strIncludeFile;
  strIncludeFile.Load_t(fnFileName);

  _yy_mi = &mi;
  SMC_PushBuffer(fnFileName, strIncludeFile, TRUE);
  syyparse();
}

// Create model instance and read from a config file into it
CModelInstance *LoadModelInstance_t(const CTFileName &fnConfigFile)
{
  _yy_mi = NULL;
  
  // Create new model instance for parser
  _yy_mi = CreateModelInstance("Temp");

  // Parse binary (BMC) config
  if (fnConfigFile.FileExt() == ".bmc") {
    CTFileStream strm;
    strm.Open_t(fnConfigFile);
    
    // Remember filename to the source file just like the SMC parser does
    if (bRememberSourceFN) {
      _yy_mi->mi_fnSourceFile = fnConfigFile;
    }

    ReadModelInstance_t(strm, *_yy_mi, FALSE);
    strm.Close();
    
  // Parse ASCII (SMC) config
  } else {
    ParseSmcFile_t(*_yy_mi, fnConfigFile);
  }
  
  return _yy_mi;
}

// Default cinstructor
CModelInstance::CModelInstance()
{
  mi_pInStock = NULL; // [SEE]
  mi_psklSkeleton = NULL;
  mi_iParentBoneID = -1;
  mi_colModelColor = 0;
  mi_vStretch = FLOAT3D(1, 1, 1);
  mi_colModelColor = 0xFFFFFFFF;
  mi_aqAnims.aq_Lists.SetAllocationStep(1);
  mi_cmiChildren.SetAllocationStep(1);
  memset(&mi_qvOffset, 0, sizeof(QVect));
  mi_qvOffset.qRot.q_w = 1;
  mi_iCurentBBox = -1;
  // set default all frames bbox
//  mi_cbAllFramesBBox.SetName("All Frames Bounding box");
  mi_cbAllFramesBBox.SetMin(FLOAT3D(-0.5, 0, -0.5));
  mi_cbAllFramesBBox.SetMax(FLOAT3D(0.5, 2, 0.5));
  // Set default model instance name
//  SetName("Noname");
}

// Destructor
CModelInstance::~CModelInstance()
{
}

// Copy constructor
CModelInstance::CModelInstance(CModelInstance &miOther)
{
  // Forbiden
  ASSERT(FALSE);
}

// TODO: Add comment here
void CModelInstance::operator=(CModelInstance &miOther)
{
  // Forbiden
  ASSERT(FALSE);  // FIXME: Is it cause exception
}

// TODO: Add comment here
void CModelInstance::GetAllFramesBBox(FLOATaabbox3D &aabbox)
{
  aabbox = FLOATaabbox3D(mi_cbAllFramesBBox.Min(), mi_cbAllFramesBBox.Max());
}

// Fills curent collision box info
void CModelInstance::GetCurrentCollisionBox(FLOATaabbox3D &aabbox)
{
  CollisionBox &cb = GetCurrentCollisionBox();
  aabbox = FLOATaabbox3D(cb.Min(), cb.Max());
}

// TODO: Add comment here
CollisionBox &CModelInstance::GetCurrentCollisionBox()
{
  ASSERT(mi_iCurentBBox >= 0);
  ASSERT(mi_iCurentBBox < mi_cbAABox.Count());
  ASSERT(mi_cbAABox.Count() > 0);

  return mi_cbAABox[mi_iCurentBBox];
}

// TODO: Add comment here
INDEX CModelInstance::GetCollisionBoxIndex(INDEX iBoxID)
{
  INDEX ctcb = mi_cbAABox.Count();
  // For each existing box
  for (INT icb = 0; icb < ctcb; icb++)
  {
    CollisionBox &cb = mi_cbAABox[icb];
    // If this is searched box
    if (cb.GetID() == iBoxID) {
      // Return index of box
      return icb;
    }
  }
  
  // Collision box was not found, return default (0)
  SKAASSERT(FALSE);
  return 0;
}

// TODO: Add comment here
CollisionBox &CModelInstance::GetCollisionBox(INDEX icb)
{
  ASSERT(icb >= 0);
  ASSERT(icb < mi_cbAABox.Count());
  return mi_cbAABox[icb];
}

// TODO: Add comment here
FLOAT3D CModelInstance::GetCollisionBoxMin(ULONG ulCollisionBox/*=0*/)
{
  ulCollisionBox = ClampUp(ulCollisionBox, mi_cbAABox.Count() - 1);
  FLOAT3D vMin = mi_cbAABox[ulCollisionBox].Min();
  return vMin;
};

// TODO: Add comment here
FLOAT3D CModelInstance::GetCollisionBoxMax(ULONG ulCollisionBox/*=0*/)
{
  ulCollisionBox = ClampUp(ulCollisionBox, mi_cbAABox.Count() - 1);
  FLOAT3D vMax = mi_cbAABox[ulCollisionBox].Max();
  return vMax;
};

// TODO: Add comment here
// FIXME: returns HEIGHT_EQ_WIDTH, LENGHT_EQ_WIDTH or LENGHT_EQ_HEIGHT
INDEX CModelInstance::GetCollisionBoxDimensionEquality(ULONG ulCollisionBox/*=0*/)
{
  ulCollisionBox = ClampUp(ulCollisionBox, mi_cbAABox.Count() - 1);
  
  // Check if error is fixed
  //ASSERT(mi_cbAABox.Count() > iCollisionBox);

  CollisionBox &cb = this->mi_cbAABox[ulCollisionBox];
  FLOAT fWeigth = cb.Max()(1) - cb.Min()(1);
  FLOAT fHeight = cb.Max()(2) - cb.Min()(2);
  FLOAT fLength = cb.Max()(3) - cb.Min()(3);
  if (fLength == fHeight) {
    return SKA_LENGTH_EQ_HEIGHT;
  } else if (fHeight == fWeigth) {
    return SKA_HEIGHT_EQ_WIDTH;
  // default fLength == fWeight
  } else {
    return SKA_LENGTH_EQ_WIDTH;
  }
};

// Add collision box to model instance
void CModelInstance::AddCollisionBox(CTString strName,FLOAT3D vMin,FLOAT3D vMax)
{
  INDEX ctcb = mi_cbAABox.Count();
  mi_cbAABox.Expand(ctcb + 1);
  
  CollisionBox &cb = mi_cbAABox[ctcb];
  cb.SetName(strName);
  cb.SetMin(vMin);
  cb.SetMax(vMax);
  mi_iCurentBBox = 0;
}

// Remove collision box from model instance
void CModelInstance::RemoveCollisionBox(INDEX iIndex)
{
  INDEX ctcb = mi_cbAABox.Count();
  INDEX icbNew = 0;
  TStaticArray<struct CollisionBox> aCollisionBoxesTemp;
  aCollisionBoxesTemp.New(ctcb - 1);
  for (INDEX icb = 0; icb < ctcb; icb++)
  {
    if (iIndex != icb) { 
      aCollisionBoxesTemp[icbNew] = mi_cbAABox[icb];
      icbNew++;
    }
  }
  mi_cbAABox = aCollisionBoxesTemp;
}

// Add child to modelinstance
void CModelInstance::AddChild(CModelInstance *pmi, INDEX iParentBoneID /* = -1 */)
{
  SKAASSERT(pmi != NULL);
  if (pmi == NULL) {
    return;
  }
  
  mi_cmiChildren.Add(pmi);
  if (iParentBoneID > 0) {
    pmi->SetParentBone(iParentBoneID);
  }
}

// Remove model instance child
void CModelInstance::RemoveChild(CModelInstance *pmi)
{
  ASSERT(pmi != this);
  SKAASSERT(pmi != NULL);
  
  // Aditional check
  if (pmi == NULL || pmi == this) {
    return;
  }

  mi_cmiChildren.Remove(pmi);
}

// Set new parent bone index
void CModelInstance::SetParentBone(INDEX iParentBoneID)
{
  mi_iParentBoneID = iParentBoneID;
}

// Model instance offsets from parent model
void CModelInstance::SetOffset(FLOAT fOffset[6])
{
  FLOAT3D fRot(fOffset[3],fOffset[4],fOffset[5]);
  mi_qvOffset.qRot.FromEuler(fRot);
  mi_qvOffset.vPos = FLOAT3D(fOffset[0],fOffset[1],fOffset[2]);
}

// TODO: Add comment here
void CModelInstance::SetOffsetPos(FLOAT3D vPos)
{
  mi_qvOffset.vPos = vPos;
}

// TODO: Add comment here
void CModelInstance::SetOffsetRot(ANGLE3D aRot)
{
  mi_qvOffset.qRot.FromEuler(aRot);
}

// TODO: Add comment here
FLOAT3D CModelInstance::GetOffsetPos()
{
  return mi_qvOffset.vPos;
}

// TODO: Add comment here
ANGLE3D CModelInstance::GetOffsetRot()
{
  ANGLE3D aRot;
  FLOATmatrix3D mat;
  mi_qvOffset.qRot.ToMatrix(mat);
  DecomposeRotationMatrix(aRot,mat);
  return aRot;
}

// Stretch model instance
void CModelInstance::StretchModel(const FLOAT3D &vStretch)
{
  mi_vStretch = vStretch;
}

// Stretch model instance without attachments
void CModelInstance::StretchSingleModel(const FLOAT3D &vStretch)
{
  mi_vStretch = vStretch;
  
  // For each child of model instance
  INDEX ctch = mi_cmiChildren.Count();
  for (INDEX ich = 0; ich < ctch; ich++)
  {
    // Set new stretch of model instance
    CModelInstance &chmi = mi_cmiChildren[ich];
    chmi.StretchSingleModel(FLOAT3D(1 / vStretch(1),  1 / vStretch(2), 1 / vStretch(3)));
  }
}

// Add mesh to ModelInstance
void CModelInstance::AddMesh_t(CTFileName fnMesh)
{
  int ctMeshInst = mi_aMeshInst.Count();
  mi_aMeshInst.Expand(ctMeshInst + 1);
  memset(&mi_aMeshInst[ctMeshInst], 0, sizeof(mi_aMeshInst[ctMeshInst]));
  
  CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SKELMESH, fnMesh);
  mi_aMeshInst[ctMeshInst].mi_pMesh = static_cast<CSkelMesh *>(pser);
}

// Add skeleton to ModelInstance
void CModelInstance::AddSkeleton_t(CTFileName fnSkeleton)
{
  CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SKELETON, fnSkeleton);
  mi_psklSkeleton = static_cast<CSkeleton *>(pser);
}

// Add AnimSet to ModelInstance
void CModelInstance::AddAnimSet_t(CTFileName fnAnimSet)
{
  CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_BONEANIMSET, fnAnimSet);
  CBoneAnimSet *Anim = static_cast<CBoneAnimSet *>(pser);
  mi_aAnimSet.Add(Anim);
}

// Add texture to ModelInstance (if no mesh instance given, add texture to last mesh instance)
void CModelInstance::AddTexture_t(CTFileName fnTexture, CTString strTexID,MeshInstance *pmshi)
{
  if (pmshi == NULL)
  {
    INDEX ctMeshInst = mi_aMeshInst.Count();
    if (ctMeshInst <= 0) {
      throw("Error adding texture\nMesh instance does not exists");
    }
    pmshi = &mi_aMeshInst[ctMeshInst - 1];
  }

  INDEX ctTextInst = pmshi->mi_tiTextures.Count();
  pmshi->mi_tiTextures.Expand(ctTextInst + 1);
  pmshi->mi_tiTextures[ctTextInst].ti_toTexture.SetData_t(fnTexture);
  pmshi->mi_tiTextures[ctTextInst].SetName(strTexID);
}

// Remove one texture from model instance
void CModelInstance::RemoveTexture(TextureInstance *ptiRemove,MeshInstance *pmshi)
{
  ASSERT(pmshi != NULL);
  TStaticArray<struct TextureInstance> atiTextures;
  INDEX ctti=pmshi->mi_tiTextures.Count();
  atiTextures.New(ctti - 1);
  
  // For each texture instance in mesh instance
  INDEX iIndexSrc = 0;
  for (INDEX iti = 0; iti < ctti; iti++)
  {
    TextureInstance *pti = &pmshi->mi_tiTextures[iti];
    // If texture instance is different from selected one 
    if (pti != ptiRemove) {
      // Copy it to new array of texture isntances
      atiTextures[iIndexSrc] = pmshi->mi_tiTextures[iti];
      iIndexSrc++;
    }
  }
  // Copy new texture instances array in mesh instance
  pmshi->mi_tiTextures.CopyArray(atiTextures);
  
  // Clear temp texture isntances array
  atiTextures.Clear();
}

// Find texture instance in all mesh instances in model instance
TextureInstance *CModelInstance::FindTexureInstance(INDEX iTexID)
{
  // For each mesh instance
  INDEX ctmshi = mi_aMeshInst.Count();
  for (INDEX imshi = 0; imshi < ctmshi; imshi++)
  {
    MeshInstance &mshi = mi_aMeshInst[imshi];
    
    // For each texture instance in meshinstance
    INDEX ctti = mshi.mi_tiTextures.Count();
    for (INDEX iti = 0; iti < ctti; iti++)
    {
      TextureInstance &ti = mshi.mi_tiTextures[iti];
      // If this is texinstance that is beeing serched for
      if (ti.GetID() == iTexID) {
        // return it
        return &ti;
      }
    }
  }
  
  // Texture instance wasn't found
  return NULL;
}

// Find texture instance in given mesh instance
TextureInstance *CModelInstance::FindTexureInstance(INDEX iTexID, MeshInstance &mshi)
{
  // For each texture instance in given mesh instance
  INDEX ctti = mshi.mi_tiTextures.Count();
  for (INDEX iti = 0; iti < ctti; iti++)
  {
    TextureInstance &ti = mshi.mi_tiTextures[iti];
    // If this is texinstance that is beeing serched for
    if (ti.GetID() == iTexID) {
      // return it
      return &ti;
    }
  }
  
  // Texture instance wasn't found
  return NULL;
}

// Change parent of model instance
void CModelInstance::ChangeParent(CModelInstance *pmiOldParent, CModelInstance *pmiNewParent)
{
  SKAASSERT(pmiOldParent != NULL);
  SKAASSERT(pmiNewParent != NULL);

  if (pmiOldParent == NULL) {
    CWarningF("Model Instance doesn't have a parent\n");
    return;
  }
  
  if (pmiNewParent == NULL) {
    CWarningF("New parent of model instance is NULL\n");
    return;
  }
  
  pmiOldParent->mi_cmiChildren.Remove(this);
  pmiNewParent->mi_cmiChildren.Add(this);
}

// return parent of this model instance
// must suply first model instance in hierarchy cos model instance does not have its parent remembered
CModelInstance *CModelInstance::GetParent(CModelInstance *pmiStartFrom)
{
  ASSERT(pmiStartFrom != NULL);
  // Aditional check
  if (pmiStartFrom == NULL) {
    return NULL;
  }
  
  // If 'this' is member of pmiStartFrom return it
  if (pmiStartFrom->mi_cmiChildren.IsMember(this)) {
    return pmiStartFrom;
  }
  
  // Count childrent of pmiStartFrom
  INDEX ctcmi = pmiStartFrom->mi_cmiChildren.Count();
  
  // For each child of pmiStartFrom
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    // If some of children have 'this' as member return them as parent
    CModelInstance *pmiReturned = GetParent(&pmiStartFrom->mi_cmiChildren[icmi]);
    if (pmiReturned != NULL) {
      return pmiReturned;
    }
  }
  return NULL;
}

// returns child with specified id
/*CModelInstance *CModelInstance::GetChild(INDEX iChildID, BOOL bRecursive)
{
  INDEX ctcmi = mi_cmiChildren.Count();
  for (INDEX icmi=0;icmi<ctcmi;icmi++) {
    CModelInstance *pmi = &mi_cmiChildren[icmi];
    if (pmi->mi_iModelID == iChildID) {
      return pmi;
    }
  }
  return NULL;
}*/
CModelInstance *CModelInstance::GetChild(INDEX iChildID, BOOL bRecursive/*=FALSE*/)
{
  INDEX ctcmi = mi_cmiChildren.Count();
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    CModelInstance *pmi = &mi_cmiChildren[icmi];
    if (pmi->mi_iModelID == iChildID) {
      return pmi;
    }
    
    // If child has own children, go recursive
    if (bRecursive && pmi->mi_cmiChildren.Count() > 0)
    {      
      pmi = pmi->GetChild(iChildID, TRUE);
      if (pmi != NULL) {
        return pmi;
      }
    }  
  }
  return NULL;
}

// Returns parent that is not included in his parents smc file
CModelInstance *CModelInstance::GetFirstNonReferencedParent(CModelInstance *pmiRoot)
{
  ASSERT(this != NULL);
  ASSERT(pmiRoot != NULL);
  CModelInstance *pmiParent = this->GetParent(pmiRoot);
  CModelInstance *pmiLast = this;
  while (pmiParent != NULL)
  {
    if (pmiParent->mi_fnSourceFile != mi_fnSourceFile) {
      return pmiLast;
    }
    pmiLast = pmiParent;
    pmiParent = pmiParent->GetParent(pmiRoot);
  }
  return NULL;//return pmiRoot
}

// Add animation to ModelInstance
void CModelInstance::AddAnimation(INDEX iAnimID, ULONG ulFlags, FLOAT fStrength, INDEX iGroupID, FLOAT fSpeedMul/*=1.0f*/)
{

#ifdef SKADEBUG
  // See whether this animation even exists in the current skeleton
  INDEX iDummy1, iDummy2;
  if (!FindAnimationByID(iAnimID, &iDummy1, &iDummy2)) { 
    /*this ModelInstance does not contain the required animation!!!*/
    SKAASSERT(FALSE);
  }
#endif

  fSpeedMul = 1 / fSpeedMul;
  
  // FIXME: What's wrong with this nested conditions?
  // If no restart flag was set
  if (ulFlags & AN_NORESTART)
  {
    // If given animtion is allready playing
    if (IsAnimationPlaying(iAnimID))
    {
      if (ulFlags & AN_LOOPING) {
        AddFlagsToPlayingAnim(iAnimID,AN_LOOPING);
      }
      // Return without adding animtion
      return;
    }
  }

  // If flag for new cleared state is set
  if (ulFlags & AN_CLEAR) {
    // Do new clear state with default length
    NewClearState(CLEAR_STATE_LENGTH);
  // If flag for new cloned state is set
  } else if (ulFlags & AN_CLONE) {
    // Do new clear state with default length
    NewClonedState(CLONED_STATE_LENGTH);
  }

  // If anim queue is empty 
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  if (ctal == 0) {
    // Add new clear state
    NewClearState(0);
  }
  ctal = mi_aqAnims.aq_Lists.Count();
  AnimList &alList = mi_aqAnims.aq_Lists[ctal - 1];

  // If flag is set not to sort anims
  if (ulFlags & AN_NOGROUP_SORT) {
    // Just add new animations to end of list
    PlayedAnim &plAnim = alList.al_PlayedAnims.Push();
    plAnim.pa_iAnimID = iAnimID;
    plAnim.pa_fSpeedMul = fSpeedMul;
    plAnim.pa_fStartTime = _pTimer->CurrentTick();
    plAnim.pa_Strength = fStrength;
    plAnim.pa_ulFlags = ulFlags;
    plAnim.pa_GroupID = iGroupID;
    
  // No flag set, sort animation by groupID
  } else {
    // Add one animation to anim list
    alList.al_PlayedAnims.Push();
    INDEX ctpa = alList.al_PlayedAnims.Count();

    INDEX ipa = ctpa - 1;
    if (ipa > 0)
    {
      // For each old animation from last to first
      for (; ipa > 0; ipa--)
      {
        PlayedAnim &pa = alList.al_PlayedAnims[ipa - 1];
        PlayedAnim &paNext = alList.al_PlayedAnims[ipa];
        // If anim group id is larger than new group id
        if (pa.pa_GroupID > iGroupID) {
          // move animation in array to right
          paNext = pa;
        } else {
          break;
        }
      }
    }
    
    // Set new animation as current index in anim list
    PlayedAnim &plAnim = alList.al_PlayedAnims[ipa];
    plAnim.pa_iAnimID = iAnimID;
    plAnim.pa_fSpeedMul = fSpeedMul;
    plAnim.pa_fStartTime = _pTimer->CurrentTick();
    plAnim.pa_Strength = fStrength;
    plAnim.pa_ulFlags = ulFlags;
    plAnim.pa_GroupID = iGroupID;
  }
}

// Remove played anim from stack
void CModelInstance::RemAnimation(INDEX iAnimID)
{
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  // If anim queue is empty
  if (ctal < 1) {
    SKAASSERT(FALSE);
    // No anim to remove
    return;
  }

  // Get last anim list in queue
  AnimList &alList = mi_aqAnims.aq_Lists[ctal - 1];
  
  // Count played anims in anim list
  INDEX ctpa = alList.al_PlayedAnims.Count();
  
  // Loop each played anim in anim list
  for (int ipa = 0; ipa < ctpa; ipa++)
  {
    PlayedAnim &paAnim = alList.al_PlayedAnims[ipa];
    // Remove if same ID
    if (paAnim.pa_iAnimID == iAnimID)
    {
      // Copy all latter anims over this one
      for (int ira = ipa; ira < ctpa - 1; ira++)
      {
        alList.al_PlayedAnims[ira] = alList.al_PlayedAnims[ira + 1];
      }
      // Decrease played anims count
      ctpa--;
      
      // Remove last anim
      alList.al_PlayedAnims.Pop();
    }
  }
}

// Remove all anims with GroupID
void CModelInstance::RemAnimsWithID(INDEX iGroupID)
{
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  // If anim queue is empty
  if (ctal < 1) {
    SKAASSERT(FALSE);
    // No anim to remove
    return;
  }

  // Get last anim list in queue
  AnimList &alList = mi_aqAnims.aq_Lists[ctal - 1];
  
  // Count played anims in anim list
  INDEX ctpa = alList.al_PlayedAnims.Count();
  
  // Loop each played anim in anim list
  for (int ipa = 0; ipa < ctpa; ipa++)
  {
    PlayedAnim &paAnim = alList.al_PlayedAnims[ipa];
    // Remove if same Group ID
    if (paAnim.pa_GroupID == iGroupID)
    {
      // Copy all latter anims over this one
      for (int ira = ipa;ira < ctpa - 1; ira++)
      {
        alList.al_PlayedAnims[ira] = alList.al_PlayedAnims[ira + 1];
      }
      
      // Decrease played anims count
      ctpa--;
      
      // Remove last anim
      alList.al_PlayedAnims.Pop();
    }
  }
}

// Remove unused anims from queue
void CModelInstance::RemovePassedAnimsFromQueue()
{
  // Count AnimLists
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  
  // Find newes animlist that has fully faded in
  INDEX iFirstAnimList = -1;
  
  // For each anim list from last to first
  INDEX ial = ctal - 1;
  for (; ial >= 0; ial--)
  {
    AnimList &alList = mi_aqAnims.aq_Lists[ial];
    // Calculate fade factor for this animlist
    FLOAT fFadeFactor = CalculateFadeFactor(alList);
    
    // If factor is 1 remove all animlists before this one
    if (fFadeFactor >= 1.0f) {
      iFirstAnimList = ial;
      break;
    }
  }
  
  if (iFirstAnimList <= 0) {
    return;
  }
  
  // Move later anim lists to first pos
  for (ial=iFirstAnimList;ial<ctal;ial++)
  {
    mi_aqAnims.aq_Lists[ial-iFirstAnimList] = mi_aqAnims.aq_Lists[ial];
    mi_aqAnims.aq_Lists[ial].al_PlayedAnims.PopAll();
  }
  
  // Remove all Anim list before iFirstAnimList
  mi_aqAnims.aq_Lists.PopUntil(ctal - iFirstAnimList - 1);
}

// Create new state, copy last state in it and give it a fade time
void CModelInstance::NewClonedState(FLOAT fFadeTime)
{
  RemovePassedAnimsFromQueue();
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  if (ctal == 0) {
    // If anim queue is empty add new clear state
    NewClearState(fFadeTime);
    ctal = 1;
  }
  
  // Create new Anim list
  AnimList &alNewList = mi_aqAnims.aq_Lists.Push();
  alNewList.al_PlayedAnims.SetAllocationStep(1);
  AnimList &alList = mi_aqAnims.aq_Lists[ctal - 1];
  
  // Copy anims to new List
  alNewList.al_PlayedAnims = alList.al_PlayedAnims;
  alNewList.al_fFadeTime = fFadeTime;
  alNewList.al_fStartTime = _pTimer->CurrentTick();
}

// Create new cleared state and give it a fade time
void CModelInstance::NewClearState(FLOAT fFadeTime)
{
  RemovePassedAnimsFromQueue();
  
  // Add new empty list
  AnimList &alNewList = mi_aqAnims.aq_Lists.Push();
  alNewList.al_PlayedAnims.SetAllocationStep(1);
  alNewList.al_fFadeTime = fFadeTime;
  alNewList.al_fStartTime = _pTimer->CurrentTick();
  alNewList.al_PlayedAnims.PopAll();
}

// stop all animations in this model instance and its children
void CModelInstance::StopAllAnimations(FLOAT fFadeTime)
{
  INDEX ctmi = mi_cmiChildren.Count();
  for (INDEX imi = 0; imi < ctmi; imi++)
  {
    CModelInstance &cmi = mi_cmiChildren[imi];
    cmi.StopAllAnimations(fFadeTime);
  }
  NewClearState(fFadeTime);
}

// Offset all animations in anim queue
void CModelInstance::OffSetAnimationQueue(TIME fOffsetTime)
{
  // for each anim list in anim queue
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  for (INDEX ial = 0; ial < ctal; ial++)
  {
    AnimList &al = mi_aqAnims.aq_Lists[ial];
    // Modify anim list start time
    al.al_fStartTime += fOffsetTime;
  }
}

// Find animation by ID
BOOL CModelInstance::FindAnimationByID(int iAnimID, INDEX *piAnimSetIndex, INDEX *piAnimIndex)
{
  INDEX ctas = mi_aAnimSet.Count();
  if (ctas <= 0) {
    return FALSE;
  }
  
  // For each animset
  for (int ias = ctas - 1; ias >= 0; ias--)
  {
    CBoneAnimSet &asAnimSet = mi_aAnimSet[ias];
    INDEX ctan = asAnimSet.as_Anims.Count();
    
    // For each animation
    for (int ian = 0; ian < ctan; ian++)
    {
      CBoneAnimation &an = asAnimSet.as_Anims[ian];
      
      // If this is animation to find
      if (an.an_iID == iAnimID) {
        // Set pointers of indices to animset and animation
        *piAnimSetIndex = ias;
        *piAnimIndex = ian;
        
        // Retrun succesfully
        return TRUE;
      }
    }
  }
  // Animation was't found
  return FALSE;
}

// Find ID of first animation
INDEX CModelInstance::FindFirstAnimationID()
{
  INDEX ctas = mi_aAnimSet.Count();

  // For each animset.
  // There is no check on animset loading if it empty or not.
  // So we should check each one to get one that has at least one animation and return first anim out of it.
  for (int ias = 0; ias < ctas; ias++)
  {
    CBoneAnimSet &asAnimSet = mi_aAnimSet[ias];
    INDEX ctan = asAnimSet.as_Anims.Count();

    // If animset isn't empty.
    if (ctan > 0) {
      CBoneAnimation &an = asAnimSet.as_Anims[0];
      return an.an_iID;
    }
  }

  return -1; // Never should get here.
}

// Get animation length
FLOAT CModelInstance::GetAnimLength(INDEX iAnimID)
{
  INDEX iAnimSetIndex,iAnimIndex;
  FindAnimationByID(iAnimID, &iAnimSetIndex, &iAnimIndex);
  CBoneAnimSet &as = mi_aAnimSet[iAnimSetIndex];
  CBoneAnimation &an = as.as_Anims[iAnimIndex];
  return an.an_fSecPerFrame * an.an_iFrames;
}

// Check if given animation is currently playing
BOOL CModelInstance::IsAnimationPlaying(INDEX iAnimID)
{
  // Check last anim list if animation iAnimID exists in it
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  
  // If there are anim lists in animqueue
  if (ctal > 0)
  {
    // Check last one
    AnimList &al = mi_aqAnims.aq_Lists[ctal - 1];
    INDEX ctpa = al.al_PlayedAnims.Count();
    for (INDEX ipa = 0; ipa < ctpa; ipa++)
    {
      PlayedAnim &pa = al.al_PlayedAnims[ipa];
      if (pa.pa_iAnimID == iAnimID) {
        // found it
        return TRUE;
      }
    }
  }
  // This animation is currently not playing
  return FALSE;
}

// Add flags to animation playing in anim queue
BOOL CModelInstance::AddFlagsToPlayingAnim(INDEX iAnimID, ULONG ulFlags)
{
  // Check last anim list if animation iAnimID exists in it
  INDEX ctal = mi_aqAnims.aq_Lists.Count();
  
  // If there are anim lists in animqueue
  if (ctal > 0)
  {
    // check last one
    AnimList &al = mi_aqAnims.aq_Lists[ctal - 1];
    INDEX ctpa = al.al_PlayedAnims.Count();
    for (INDEX ipa = 0; ipa < ctpa; ipa++)
    {
      PlayedAnim &pa = al.al_PlayedAnims[ipa];
      if (pa.pa_iAnimID == iAnimID) {
        pa.pa_ulFlags |= ulFlags;
        // found it
        return TRUE;
      }
    }
  }
  
  // This animation is currently not playing
  return FALSE;
}

// Sets name of model instance
void CModelInstance::SetName(CTString strName)
{
  mi_strName = strName;
  mi_iModelID = SM_StringToId(strName);
}

// Gets name of model instance
const CTString &CModelInstance::GetName()
{
  return mi_strName;
}

// Gets id of model instance
const INDEX &CModelInstance::GetID()
{
  return mi_iModelID;
}

// Get vertex positions in absolute space
void CModelInstance::GetModelVertices(TStaticStackArray<FLOAT3D> &avVertices, FLOATmatrix3D &mRotation,
                                      FLOAT3D &vPosition, FLOAT fNormalOffset, FLOAT fDistance)
{
  RM_SetObjectPlacement(mRotation,vPosition);
  RM_GetModelVertices(*this, avVertices, mRotation, vPosition, fNormalOffset, fDistance);
}

// Model color
COLOR &CModelInstance::GetModelColor(void)
{
  return mi_colModelColor;
}

// Set model color
void CModelInstance::SetModelColor(COLOR colNewColor)
{
  mi_colModelColor = colNewColor;
  INDEX ctch = mi_cmiChildren.Count();
  
  // For each child
  for (INDEX ich = 0;ich < ctch; ich++)
  {
    CModelInstance &cmi = mi_cmiChildren[ich];
    // Set child model color 
    cmi.SetModelColor(colNewColor);
  }
}

// Test it the model has alpha blending
BOOL CModelInstance::HasAlpha(void)
{
  return (GetModelColor() & 0xFF) != 0xFF;
}

BOOL CModelInstance::IsModelVisible(FLOAT fMipFactor)
{
  #pragma message(">> IsModelVisible")
  return TRUE;
}

BOOL CModelInstance::HasShadow(FLOAT fMipFactor)
{
  #pragma message(">> HasShadow")
  return TRUE;
}

// Simple shadow rendering
void CModelInstance::AddSimpleShadow(const FLOAT fIntensity, const FLOATplane3D &plShadowPlane)
{

  // if shadows are not rendered for current mip, model is half/full face-forward,
  // intensitiy is too low or projection is not perspective - do nothing!
  //if (!HasShadow(1) || fIntensity<0.01f || !_aprProjection.IsPerspective()
  // || (rm.rm_pmdModelData->md_Flags&(MF_FACE_FORWARD|MF_HALF_FACE_FORWARD))) return;
  // ASSERT(_iRenderingType==1);
  ASSERT(fIntensity > 0 && fIntensity <= 1);
  // do some rendering
  // _pfModelProfile->StartTimer(CModelProfile::PTI_RENDERSIMPLESHADOW);
  // _pfModelProfile->IncrementTimerAveragingCounter(CModelProfile::PTI_RENDERSIMPLESHADOW);
  // _sfStats.IncrementCounter(CStatForm::SCI_MODELSHADOWS);
  // calculate projection model bounding box in object space (if needed)
  // CalculateBoundingBox(this, rm);
  // add one simple shadow to batch list
  RM_SetObjectMatrices(*this);
  RM_AddSimpleShadow_View(*this, fIntensity, plShadowPlane);
  // all done
  // _pfModelProfile->StopTimer(CModelProfile::PTI_RENDERSIMPLESHADOW);

}

// Copy mesh instance for other model instance
void CModelInstance::CopyMeshInstance(CModelInstance &miOther)
{
  INDEX ctmshi = miOther.mi_aMeshInst.Count();
  // For each mesh insntace
  for (INDEX imshi = 0; imshi < ctmshi; imshi++)
  {
    MeshInstance &mshiOther = miOther.mi_aMeshInst[imshi];
    
    // Add its mesh
    AddMesh_t(mshiOther.mi_pMesh->GetName());
    MeshInstance *pmshi = &mi_aMeshInst[imshi];

    INDEX ctti = mshiOther.mi_tiTextures.Count();
    // For each texture in mesh instance
    for (INDEX iti = 0; iti < ctti; iti++)
    {
      TextureInstance &tiOther = mshiOther.mi_tiTextures[iti];
      CTString strTexID = SM_IdToString(tiOther.GetID());
      // CTextureData *ptd = (CTextureData*)tiOther.ti_toTexture.GetData();
      AddTexture_t(tiOther.ti_toTexture.GetName(), strTexID, pmshi);
    }
  }
}

void CModelInstance::Copy(CAbstractModelInstance &mi)
{
  if (mi.GetInstanceType() != GetInstanceType()) {
    ASSERTALWAYS("CModelInstance::Copy called for different classes!");
    return;
  }

  CModelInstance *pmi = (CModelInstance *)(&mi);
  CModelInstance &miOther = *pmi;

  // Clear this instance - otherwise it won't work
  Clear();

  mi_aqAnims.aq_Lists = miOther.mi_aqAnims.aq_Lists;
  mi_iCurentBBox  = miOther.mi_iCurentBBox;
  mi_colModelColor = miOther.mi_colModelColor;
  mi_iParentBoneID = miOther.mi_iParentBoneID;
  mi_qvOffset = miOther.mi_qvOffset;
  mi_strName = miOther.mi_strName;
  mi_iModelID = miOther.mi_iModelID;
  mi_cbAABox = miOther.mi_cbAABox;
  mi_fnSourceFile = miOther.mi_fnSourceFile;
  mi_vStretch = miOther.mi_vStretch;
  
  // [SEE]
	if(miOther.mi_pInStock != NULL) {
		mi_pInStock = miOther.mi_pInStock;
		mi_pInStock->Reference();
	}

  // Copt mesh instance
  CopyMeshInstance(miOther);

  // if skeleton exists 
  if (miOther.mi_psklSkeleton != NULL) {
    // Copy skeleton
    AddSkeleton_t(miOther.mi_psklSkeleton->GetName());
  }

  // Copy animsets
  INDEX ctas = miOther.mi_aAnimSet.Count();
  // For each animset
  for (INDEX ias = 0;ias < ctas; ias++)
  {
    // Add animset to this model instance
    CBoneAnimSet &asOther = miOther.mi_aAnimSet[ias];
    AddAnimSet_t(asOther.GetName());
  }

  // Copy children
  INDEX ctch = miOther.mi_cmiChildren.Count();
  
  // For each child in other model instance
  for (INDEX ich = 0; ich < ctch; ich++)
  {
    CModelInstance &chmiOther = miOther.mi_cmiChildren[ich];
    CModelInstance *pchmi = CreateModelInstance("Temp");
    pchmi->Copy(chmiOther);
    AddChild(pchmi);
  }
}

// Synchronize with another model (copy animations/attachments positions etc from there)
void CModelInstance::Synchronize(CModelInstance &miOther)
{
  // Sync animations
  mi_aqAnims.aq_Lists = miOther.mi_aqAnims.aq_Lists;
  
  // Sync misc params
  mi_qvOffset      = miOther.mi_qvOffset;
  mi_iParentBoneID = miOther.mi_iParentBoneID;
  mi_colModelColor = miOther.mi_colModelColor;
  mi_vStretch      = miOther.mi_vStretch;

  // For each child in model instance
  INDEX ctchmi = mi_cmiChildren.Count();
  for (INDEX ichmi = 0; ichmi < ctchmi; ichmi++)
  {
    CModelInstance &chmi = mi_cmiChildren[ichmi];
    CModelInstance *pchmiOther = miOther.GetChild(chmi.GetID(), FALSE);
    // If both model instance have this child 
    if (pchmiOther != NULL) {
      // sync child
      chmi.Synchronize(*pchmiOther);
    }
  }
}

// Clear model instance
void CModelInstance::Clear(void)
{
  // [SEE]
	if(mi_pInStock != NULL) {
		_pResourceMgr->Release(mi_pInStock);
		mi_pInStock = NULL;
	}
  
  // For each child of this model instance
  INDEX ctcmi = mi_cmiChildren.Count();
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    // Delete child
    CModelInstance *pcmi = &mi_cmiChildren[0];
    mi_cmiChildren.Remove(pcmi);
    DeleteModelInstance(pcmi);
  }

  // Release all meshes in stock used by mi
  INDEX ctmshi = mi_aMeshInst.Count();
  for (INDEX imshi = 0; imshi < ctmshi; imshi++)
  {
    MeshInstance &mshi = mi_aMeshInst[imshi];
    CSkelMesh *pmesh = mshi.mi_pMesh;
    if (pmesh != NULL) {
      _pResourceMgr->Release(pmesh);
    }
    
    // Release all textures in stock used by mesh
    INDEX ctti = mshi.mi_tiTextures.Count();
    for (INDEX iti = 0; iti < ctti; iti++)
    {
      TextureInstance &ti = mshi.mi_tiTextures[iti];
      ti.ti_toTexture.SetData(NULL);
    }
  }
  mi_aMeshInst.Clear();
  
  // Release skeleton in stock used by mi(if it exist)
  if (mi_psklSkeleton != NULL) {
    _pResourceMgr->Release(mi_psklSkeleton);
    mi_psklSkeleton = NULL;
  }

  // Release all animsets in stock used by mi
  INDEX ctas = mi_aAnimSet.Count();
  for (INDEX ias = 0; ias < ctas; ias++)
  {
    _pResourceMgr->Release(&mi_aAnimSet[ias]);  
  }
  mi_aAnimSet.Clear();

  // Clear all collision boxes 
  mi_cbAABox.Clear();
  
  // Clear anim list
  mi_aqAnims.aq_Lists.Clear();
}

// Count used memory
SLONG CModelInstance::GetUsedMemory(void)
{
  SLONG slMemoryUsed = sizeof(*this);
  
  // Count mesh instances
  INDEX ctmshi = mi_aMeshInst.Count();
  for (INDEX imshi = 0; imshi < ctmshi; imshi++)
  {
    MeshInstance &mshi = mi_aMeshInst[imshi];
    slMemoryUsed += mshi.mi_tiTextures.Count() * sizeof(TextureInstance);
  }
  slMemoryUsed += mi_aMeshInst.Count() * sizeof(MeshInstance);
  
  // Count bounding boxes
  slMemoryUsed += mi_cbAABox.Count() * sizeof(CollisionBox);
  
  // Cound child model instances
  INDEX ctcmi = mi_cmiChildren.Count();
  for (INDEX icmi = 0; icmi < ctcmi; icmi++)
  {
    CModelInstance &cmi = mi_cmiChildren[icmi];
    slMemoryUsed += cmi.GetUsedMemory();
  }
  return slMemoryUsed;
}

// Set parser to remember file names for modelinstances
void CModelInstance::EnableSrcRememberFN(BOOL bEnable)
{
  bRememberSourceFN = bEnable;
}