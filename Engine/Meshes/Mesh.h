/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MESH_H
#define SE_INCL_MESH_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! One point in 3D space.
struct ENGINE_API MeshVertex
{
  FLOAT x, y, z;
};

//! 3D angle for each vertex.
struct ENGINE_API MeshNormal
{
  FLOAT nx, ny, nz;
};

//! UV coords for one vertex.
struct ENGINE_API MeshTexCoord
{
  FLOAT u, v;
};

//! Abstract class for any mesh resource.
class ENGINE_API CMesh : public CResource
{
  //! TODO: Implement later.
};

#endif