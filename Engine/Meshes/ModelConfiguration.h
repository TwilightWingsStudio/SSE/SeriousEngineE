/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ABSTRACTMODELCONFIG_H
#define SE_INCL_ABSTRACTMODELCONFIG_H

#ifdef PRAGMA_ONCE
	#pragma once
#endif

#include <Core/IO/Resource.h>

#include <Engine/Meshes/AbstractModelInstance.h>

//! Class that holds model instance in stock.
class ENGINE_API CModelConfiguration : public CResource
{
  public:
    //! Constructor.
    CModelConfiguration();
    
    //! Destructor.
    ~CModelConfiguration();

    //! Read model instance from stream.
    void Read_t(CTStream *istrFile);  // throw char *

    //! Write model instance in stream.
    void Write_t(CTStream *ostrFile); // throw char *

    //! Clear model instance.
    void Clear(void);

    //! Count used memory.
    SLONG GetUsedMemory(void);

    //! Get the description of this object.
    CTString GetDescription(void);

    //! Get current mesh type.
    inline enum ModelInstanceType GetInstanceType() const
    {
      if (mic_pAbstractModelInstance == NULL) {
        return MIT_INVALID;
      }

      return mic_pAbstractModelInstance->GetInstanceType();
    }

    //! Get abstract pointer.
    inline CAbstractModelInstance *GetAbstractInstance()
    {
      return mic_pAbstractModelInstance;
    }

    //! Get the data object.
    inline CModelObject *GetModelObject()
    {
      return mic_pModelObject;
    }

    //! Get the data object.
    inline CModelInstance *GetModelInstance()
    {
      return mic_pModelInstance;
    }

    //! Check if this kind of objects is auto-freed.
    BOOL IsAutoFreed(void)
    {
      return FALSE;
    };

    //! Get the resource type.
    virtual UBYTE GetType() const;

  public:
    union {
      CGameObject *mic_pObject;
      CAbstractModelInstance *mic_pAbstractModelInstance;
      CModelObject *mic_pModelObject;
      CModelInstance *mic_pModelInstance;
    };
};

#endif  /* include-once check. */
