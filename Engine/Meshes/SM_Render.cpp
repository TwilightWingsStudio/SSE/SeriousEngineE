/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/Base/Console.h>
#include <Core/Math/Projection.h>
#include <Core/Math/Float.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Quaternion.h>
#include <Core/Math/Geometry.inl>
#include <Core/Math/Clipping.inl>
#include <Engine/Meshes/ModelInstance.h>
#include <Engine/Meshes/SM_Render.h>
#include <Engine/Meshes/SkelMesh.h>
#include <Engine/Meshes/Skeleton.h>
#include <Engine/Meshes/BoneAnimSet.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Core/Templates/DynamicContainer.cpp>
#include <Engine/Graphics/Drawport.h>
#include <Engine/Graphics/Fog_internal.h>
#include <Core/Base/Statistics_Internal.h>

static CAnyProjection3D _aprProjection;
static CDrawPort *_pdp = NULL;
static enum FPUPrecisionType _fpuOldPrecision;
static INDEX _iRenderingType = 0; // 0=none, 1=view, 2=mask

static FLOAT3D _vLightDir;          // Light direction
static FLOAT3D _vLightDirInView;    // Light direction transformed in view space
static COLOR   _colAmbient;         // Ambient color
static COLOR   _colLight;           // Light color
static FLOAT   _fDistanceFactor;    // Distance to object from viewer
static Matrix12 _mObjectToAbs;      // object to absolute
static Matrix12 _mAbsToViewer;      // absolute to viewer
static Matrix12 _mObjToView;        // object to viewer
static Matrix12 _mObjToViewStretch; // object to viewer, stretch by root model instance stretch factor
static FLOATaabbox3D _bbRootAllFrames;                  // All frames bbox of root model instance

ULONG _ulFlags = RMF_SHOWTEXTURE;
static ULONG _ulRenFlags = 0;
static FLOAT _fCustomMlodDistance = -1; // custom distance for mesh lods
static FLOAT _fCustomSlodDistance = -1; // custom distance for skeleton lods

extern FLOAT sm_fLODMul;
extern FLOAT sm_fLODAdd;
extern INDEX sm_bRenderMesh;
extern INDEX sm_bTransformMesh;
extern INDEX sm_bMorphMesh;

// Mask shader (for rendering models' shadows to shadowmaps)
static CShaderClass _shMaskShader;

// Temporary rendering structures
static TStaticStackArray<struct RenModel> _aRenModels;
static TStaticStackArray<struct RenBone> _aRenBones;
static TStaticStackArray<struct RenMesh> _aRenMesh;
static TStaticStackArray<struct RenMorph> _aRenMorphs;
static TStaticStackArray<struct RenWeight> _aRenWeights;
static TStaticStackArray<struct MeshVertex> _aMorphedVtxs;
static TStaticStackArray<struct MeshNormal> _aMorphedNormals;
static TStaticStackArray<struct MeshVertex> _aFinalVtxs;
static TStaticStackArray<struct MeshNormal> _aFinalNormals;
static TStaticStackArray<struct GFXColor> _aMeshColors;
static TStaticStackArray<struct RenMatrix> _aWeightMatrices;
static TStaticStackArray<struct SurfaceMatrices> _aSurfacesMatrices;

// Vertex/Normal Buffer Pointers
static MeshVertex *_pavMeshVertices = NULL;      // vertices (untouched)
static MeshNormal *_panMeshNormals  = NULL;      // normals (untouched)
static MeshVertex *_pavMorphedVertices = NULL;   // morphed vertices
static MeshNormal *_panMorphedNormals = NULL;    // morphed normals
static MeshVertex *_pavWeightedVertices = NULL;  // final vertices (morphed + weighted)
static MeshNormal *_panWeightedNormals = NULL;   // final normals (morphed + weighted)
static MeshVertexWeightInfo *_pawMeshWeights  = NULL; // mesh lod weights  (from weight buffer)

static INDEX _ctFinalVertices;                   // final vertices count
BOOL _bTransformBonelessModelToViewSpace = TRUE; // are boneless models transformed to view space

// Pointers for bone adjustment function
static void (*_pAdjustBonesCallback)(void *pData) = NULL;
static void *_pAdjustBonesData = NULL;

// Pointers for shader params adjustment function
static void (*_pAdjustShaderParams)(void *pData, INDEX iSurfaceID, CShaderClass *pShader,ShaderParams &shParams) = NULL;
static void *_pAdjustShaderData = NULL;

static BOOL FindRenBone(RenModel &rm,int iBoneID,INDEX *piBoneIndex);
static void PrepareMeshForRendering(RenMesh &rmsh, INDEX iSkeletonlod);
static void CalculateRenderingData(CModelInstance &mi);
static void ClearRenArrays();

// Load our 3x4 matrix from old-fashioned matrix+vector combination
inline void MatrixVectorToMatrix12(Matrix12 &m12,const FLOATmatrix3D &m, const FLOAT3D &v)
{
  m12[0] = m(1, 1); m12[1] = m(1, 2); m12[ 2] = m(1, 3); m12[ 3] = v(1); 
  m12[4] = m(2, 1); m12[5] = m(2, 2); m12[ 6] = m(2, 3); m12[ 7] = v(2); 
  m12[8] = m(3, 1); m12[9] = m(3, 2); m12[10] = m(3, 3); m12[11] = v(3); 
}

// Convert matrix12 to old matrix 3x3 and vector
inline void Matrix12ToMatrixVector(FLOATmatrix3D &c, FLOAT3D &v, const Matrix12 &m12)
{
  c(1, 1) = m12[0]; c(1, 2) = m12[1]; c(1, 3) = m12[ 2]; v(1) = m12[ 3]; 
  c(2, 1) = m12[4]; c(2, 2) = m12[5]; c(2, 3) = m12[ 6]; v(2) = m12[ 7]; 
  c(3, 1) = m12[8]; c(3, 2) = m12[9]; c(3, 3) = m12[10]; v(3) = m12[11]; 
}

// Create matrix from vector without rotations
inline static void MakeStretchMatrix(Matrix12 &c, const FLOAT3D &v)
{
  c[0] = v(1); c[1] = 0.0f; c[ 2] = 0.0f; c[ 3] = 0.0f; 
  c[4] = 0.0f; c[5] = v(2); c[ 6] = 0.0f; c[ 7] = 0.0f; 
  c[8] = 0.0f; c[9] = 0.0f; c[10] = v(3); c[11] = 0.0f; 
}

// Remove rotation from matrix (make it front face)
inline static void RemoveRotationFromMatrix(Matrix12 &mat)
{
  mat[0] = 1; mat[1] = 0; mat[ 2] = 0; 
  mat[4] = 0; mat[5] = 1; mat[ 6] = 0; 
  mat[8] = 0; mat[9] = 0; mat[10] = 1;
}

// Set given matrix as identity matrix
inline static void MakeIdentityMatrix(Matrix12 &mat)
{
  memset(&mat,0,sizeof(mat));
  mat[0]  = 1;
  mat[5]  = 1;
  mat[10] = 1;
}

// Transform vector with given matrix
inline static void TransformVector(FLOAT3 &v, const Matrix12 &m)
{
  float x = v[0];
  float y = v[1];
  float z = v[2];
  v[0] = m[0] * x + m[1] * y + m[ 2] * z + m[ 3];
  v[1] = m[4] * x + m[5] * y + m[ 6] * z + m[ 7];
  v[2] = m[8] * x + m[9] * y + m[10] * z + m[11];
}

// TODO: Add comment here
inline void TransformVertex(GFXVertex &v, const Matrix12 &m)
{
  float x = v.x;
  float y = v.y;
  float z = v.z;
  v.x = m[0] * x + m[1] * y + m[ 2] * z + m[ 3];
  v.y = m[4] * x + m[5] * y + m[ 6] * z + m[ 7];
  v.z = m[8] * x + m[9] * y + m[10] * z + m[11];
}

// Rotate vector with given matrix (does not translate vector )
inline void RotateVector(FLOAT3 &v, const Matrix12 &m)
{
  float x = v[0];
  float y = v[1];
  float z = v[2];
  v[0] = m[0] * x + m[1] * y + m[ 2] * z;
  v[1] = m[4] * x + m[5] * y + m[ 6] * z;
  v[2] = m[8] * x + m[9] * y + m[10] * z;
}

// Copy one matrix12 to another
inline void MatrixCopy(Matrix12 &c, const Matrix12 &m)
{
  memcpy(&c,&m,sizeof(c));
}

// Convert 3x4 matrix to QVect 
inline void Matrix12ToQVect(QVect &qv,const Matrix12 &m12)
{
  FLOATmatrix3D m;
  m(1, 1) = m12[0]; m(1, 2) = m12[ 1]; m(1, 3) = m12[ 2]; 
  m(2, 1) = m12[4]; m(2, 2) = m12[ 5]; m(2, 3) = m12[ 6]; 
  m(3, 1) = m12[8]; m(3, 2) = m12[ 9]; m(3, 3) = m12[10]; 
  
  qv.qRot.FromMatrix(m);
  qv.vPos(1) = m12[3];
  qv.vPos(2) = m12[7];
  qv.vPos(3) = m12[11];
}

// Covert QVect to matrix 3x4
inline void QVectToMatrix12(Matrix12 &m12, const QVect &qv)
{
  FLOATmatrix3D m;
  qv.qRot.ToMatrix(m);
  MatrixVectorToMatrix12(m12, m, qv.vPos);
}

// Concatenate two 3x4 matrices C=(MxN)
inline void MatrixMultiply(Matrix12 &c,const Matrix12 &m, const Matrix12 &n)
{
  c[0] = m[0] * n[0] + m[1] * n[4] + m[2] * n[ 8];
  c[1] = m[0] * n[1] + m[1] * n[5] + m[2] * n[ 9];
  c[2] = m[0] * n[2] + m[1] * n[6] + m[2] * n[10];
  c[3] = m[0] * n[3] + m[1] * n[7] + m[2] * n[11] + m[3];

  c[4] = m[4] * n[0] + m[5] * n[4] + m[6] * n[ 8];
  c[5] = m[4] * n[1] + m[5] * n[5] + m[6] * n[ 9];
  c[6] = m[4] * n[2] + m[5] * n[6] + m[6] * n[10];
  c[7] = m[4] * n[3] + m[5] * n[7] + m[6] * n[11] + m[7];

  c[ 8] = m[8] * n[0] + m[9] * n[4] + m[10] * n[ 8];
  c[ 9] = m[8] * n[1] + m[9] * n[5] + m[10] * n[ 9];
  c[10] = m[8] * n[2] + m[9] * n[6] + m[10] * n[10];
  c[11] = m[8] * n[3] + m[9] * n[7] + m[10] * n[11] + m[11];
}

// Multiply two matrices into first one
inline void MatrixMultiplyCP(Matrix12 &c,const Matrix12 &m, const Matrix12 &n)
{
  Matrix12 mTemp;
  MatrixMultiply(mTemp,m,n);
  MatrixCopy(c,mTemp);
}

// Make transpose matrix 
inline void MatrixTranspose(Matrix12 &r, const Matrix12 &m)
{
  r[ 0] = m[ 0];
  r[ 5] = m[ 5];
  r[10] = m[10];
  r[ 3] = m[ 3];
  r[ 7] = m[ 7];
  r[11] = m[11];

  r[1] = m[4];
  r[2] = m[8];
  r[4] = m[1];
  r[8] = m[2];
  r[6] = m[9];
  r[9] = m[6];

  r[ 3] = -r[0] * m[3] - r[1] * m[7] - r[ 2] * m[11];
  r[ 7] = -r[4] * m[3] - r[5] * m[7] - r[ 6] * m[11];
  r[11] = -r[8] * m[3] - r[9] * m[7] - r[10] * m[11];
}

// set view matrix
__forceinline extern void SetViewMatrix(Matrix12 &mView12)
{
  FLOAT mView16[16];
  mView16[ 0] = mView12[ 0];  mView16[ 1] = mView12[ 4];  mView16[ 2] = mView12[ 8];  mView16[ 3] = 0;
  mView16[ 4] = mView12[ 1];  mView16[ 5] = mView12[ 5];  mView16[ 6] = mView12[ 9];  mView16[ 7] = 0;
  mView16[ 8] = mView12[ 2];  mView16[ 9] = mView12[ 6];  mView16[10] = mView12[10];  mView16[11] = 0;
  mView16[12] = mView12[ 3];  mView16[13] = mView12[ 7];  mView16[14] = mView12[11];  mView16[15] = 1;
  gfxSetViewMatrix(mView16);
}

// LOD factor management
void RM_SetCurrentDistance(FLOAT fDistFactor)
{
  _fCustomMlodDistance = fDistFactor;
  _fCustomSlodDistance = fDistFactor;
}

// TODO: Add comment here
FLOAT RM_GetMipFactor(void)
{
  return 0;
}


// Fill given array with array of transformed vertices
void RM_GetModelVertices(CModelInstance &mi, TStaticStackArray<FLOAT3D> &avVertices, FLOATmatrix3D &mRotation,
                         FLOAT3D &vPosition, FLOAT fNormalOffset, FLOAT fDistance)
{
  // Transform all vertices in view space
  BOOL bTemp = _bTransformBonelessModelToViewSpace;
  _bTransformBonelessModelToViewSpace = TRUE;

  // Only root model instances
  ASSERT(mi.mi_iParentBoneID == -1);
  
  // Remember parent bone ID
  INDEX iOldParentBoneID = mi.mi_iParentBoneID;
  
  // Set parent bone ID as -1
  mi.mi_iParentBoneID = -1;
  
  // Reset abs to viewer matrix
  MakeIdentityMatrix(_mAbsToViewer);
  RM_SetCurrentDistance(fDistance);
  CalculateRenderingData(mi);

  // For each ren model
  INDEX ctrmsh = _aRenModels.Count();
  for (int irmsh = 1; irmsh < ctrmsh; irmsh++)
  {
    RenModel &rm = _aRenModels[irmsh];
    INDEX ctmsh = rm.rm_iFirstMesh + rm.rm_ctMeshes;
    
    // For each mesh in renmodel
    for (int imsh = rm.rm_iFirstMesh; imsh < ctmsh; imsh++)
    {
      // Prepare mesh for rendering
      RenMesh &rmsh = _aRenMesh[imsh];
      PrepareMeshForRendering(rmsh, rm.rm_iSkeletonLODIndex);
      INDEX ctvtx = _ctFinalVertices;
      INDEX ctvtxGiven = avVertices.Count();
      avVertices.Push(ctvtx);
      
      // For each vertex in prepared mesh
      for (INDEX ivtx = 0; ivtx < ctvtx; ivtx++)
      {
        #pragma message(">> Fix this")
        FLOAT3D vVtx = FLOAT3D(_pavWeightedVertices[ivtx].x, _pavWeightedVertices[ivtx].y, _pavWeightedVertices[ivtx].z);
        FLOAT3D vNor = FLOAT3D(_panWeightedNormals[ivtx].nx, _panWeightedNormals[ivtx].ny, _panWeightedNormals[ivtx].nz);
        
        // Add vertex to given vertex array
        avVertices[ivtx + ctvtxGiven] = vVtx + (vNor * fNormalOffset);
      }
    }
  }
  
  // Restore old bone parent ID
  mi.mi_iParentBoneID = iOldParentBoneID;
  ClearRenArrays();
  _bTransformBonelessModelToViewSpace = bTemp;
}

// TODO: Add comment here
FLOAT RM_TestRayCastHit(CModelInstance &mi, FLOATmatrix3D &mRotation, FLOAT3D &vPosition, const FLOAT3D &vOrigin,
                        const FLOAT3D &vTarget, FLOAT fOldDistance, INDEX *piBoneID)
{
  FLOAT fDistance = 1E6f;
  int i = 0;
  i++; // LUL

  BOOL bTemp = _bTransformBonelessModelToViewSpace;
  _bTransformBonelessModelToViewSpace = TRUE;

  // ASSERT((CProjection3D *)_aprProjection!=NULL);
  RM_SetObjectPlacement(mRotation,vPosition);
  // Reset abs to viewer matrix
  MakeIdentityMatrix(_mAbsToViewer);
  
  // Allways use the first LOD
  RM_SetCurrentDistance(0);
  CalculateRenderingData(mi);
  
  // For each ren model
  INDEX ctrmsh = _aRenModels.Count();
  for (int irmsh = 1; irmsh < ctrmsh; irmsh++)
  {
    RenModel &rm = _aRenModels[irmsh];
    INDEX ctmsh = rm.rm_iFirstMesh + rm.rm_ctMeshes;
    
    // For each mesh in renmodel
    for (int imsh = rm.rm_iFirstMesh; imsh < ctmsh; imsh++)
    {
      // Prepare mesh for rendering
      RenMesh &rmsh = _aRenMesh[imsh];
      PrepareMeshForRendering(rmsh,rm.rm_iSkeletonLODIndex);
      CSkelMeshLod &mshlod = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex];
      INDEX ctsurf = mshlod.mlod_aSurfaces.Count();
      for (int isurf = 0; isurf < ctsurf; isurf++)
      {
        MeshSurface &mshsurf = mshlod.mlod_aSurfaces[isurf];
        INDEX cttri = mshsurf.msrf_aTriangles.Count();
        for (int itri = 0; itri < cttri; itri++)
        {
          Vector<FLOAT,3> vVertex0(_pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[0]].x,
                                   _pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[0]].y,
                                   _pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[0]].z);

          Vector<FLOAT,3> vVertex1(_pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[1]].x,
                                   _pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[1]].y,
                                   _pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[1]].z);

          Vector<FLOAT,3> vVertex2(_pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[2]].x,
                                   _pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[2]].y,
                                   _pavWeightedVertices[mshsurf.msrf_aTriangles[itri].iVertex[2]].z);

          Plane <float, 3> plTriPlane(vVertex0, vVertex1, vVertex2);
          FLOAT fDistance0 = plTriPlane.PointDistance(vOrigin);
          FLOAT fDistance1 = plTriPlane.PointDistance(vTarget);

          // If the ray hits the polygon plane
          if (fDistance0 >= 0 && fDistance0 >= fDistance1)
          {
            // Calculate fraction of line before intersection
            FLOAT fFraction = fDistance0 / (fDistance0 - fDistance1);
            
            // Calculate intersection coordinate
            FLOAT3D vHitPoint = vOrigin + (vTarget - vOrigin) * fFraction;
            
            // Calculate intersection distance
            FLOAT fHitDistance = (vHitPoint - vOrigin).Length();
            
            // If the hit point can not be new closest candidate
            if (fHitDistance > fOldDistance) {
              // skip this triangle
              continue;
            }

            // Find major axes of the polygon plane
            INDEX iMajorAxis1, iMajorAxis2;
            GetMajorAxesForPlane(plTriPlane, iMajorAxis1, iMajorAxis2);

            // Create an intersector
            CIntersector isIntersector(vHitPoint(iMajorAxis1), vHitPoint(iMajorAxis2));

            // Check intersections for all three edges of the polygon
            isIntersector.AddEdge(vVertex0(iMajorAxis1), vVertex0(iMajorAxis2),
                                  vVertex1(iMajorAxis1), vVertex1(iMajorAxis2));

            isIntersector.AddEdge(vVertex1(iMajorAxis1), vVertex1(iMajorAxis2),
                                  vVertex2(iMajorAxis1), vVertex2(iMajorAxis2));
            isIntersector.AddEdge(vVertex2(iMajorAxis1), vVertex2(iMajorAxis2),
                                  vVertex0(iMajorAxis1), vVertex0(iMajorAxis2));
            
            // if the polygon is intersected by the ray, and it is the closest intersection so far
            if (isIntersector.IsIntersecting() && (fHitDistance < fDistance))
            {
              // Remember hit coordinates
              fDistance = fHitDistance;

              // do we neet to find the bone hit by the ray?
              if (piBoneID != NULL)
              {
                INDEX iClosestVertex;
                // find the vertex closest to the intersection
                FLOAT fDist0 = (vHitPoint - vVertex0).Length();
                FLOAT fDist1 = (vHitPoint - vVertex1).Length();
                FLOAT fDist2 = (vHitPoint - vVertex2).Length();
                if (fDist0 < fDist1)
                {
                  if (fDist0 < fDist2) {
                    iClosestVertex = mshsurf.msrf_aTriangles[itri].iVertex[0];
                  } else {
                    iClosestVertex = mshsurf.msrf_aTriangles[itri].iVertex[2];
                  }
                } else {
                  if (fDist1 < fDist2) {
                    iClosestVertex = mshsurf.msrf_aTriangles[itri].iVertex[1];
                  } else {
                    iClosestVertex = mshsurf.msrf_aTriangles[itri].iVertex[2];
                  }               
                }
              
                // Now find the weightmap with the largest weight for this vertex
                INDEX ctwmaps = mshlod.mlod_aWeightMaps.Count();
                FLOAT fMaxVertexWeight = 0.0f;
                INDEX iMaxWeightMap = -1;
                for (int iwmap = 0; iwmap < ctwmaps; iwmap++)
                {
                  MeshWeightMap& wtmap = mshlod.mlod_aWeightMaps[iwmap];
                  INDEX ctvtx = wtmap.mwm_aVertexWeight.Count();
                  for (int ivtx = 0; ivtx < ctvtx; ivtx++)
                  {
                    if ((wtmap.mwm_aVertexWeight[ivtx].mww_iVertex == iClosestVertex) &&
                        (wtmap.mwm_aVertexWeight[ivtx].mww_fWeight > fMaxVertexWeight))
                    {
                      fMaxVertexWeight = wtmap.mwm_aVertexWeight[ivtx].mww_fWeight;
                      iMaxWeightMap = wtmap.mwm_iID;
                      break;
                    } 
                  }
                }
                *piBoneID = iMaxWeightMap;
              }
            }
          }
        }
      }
    }
  }
  ClearRenArrays();
  _bTransformBonelessModelToViewSpace = bTemp;

  return fDistance;
}

// Add simple model shadow
void RM_AddSimpleShadow_View(CModelInstance &mi, const FLOAT fIntensity, const FLOATplane3D &plShadowPlane)
{
  // _pfModelProfile->StartTimer(CModelProfile::PTI_VIEW_RENDERSIMPLESHADOW);
  // _pfModelProfile->IncrementTimerAveragingCounter(CModelProfile::PTI_VIEW_RENDERSIMPLESHADOW);

  // Get viewer in absolute space
  FLOAT3D vViewerAbs = _aprProjection->ViewerPlacementR().pl_PositionVector;
  
  // If shadow destination plane is not visible, don't cast shadows
  if (plShadowPlane.PointDistance(vViewerAbs) < 0.01f) {
    // _pfModelProfile->StopTimer(CModelProfile::PTI_VIEW_RENDERSIMPLESHADOW);
    return;
  }

  // _pfModelProfile->StartTimer(CModelProfile::PTI_VIEW_SIMP_CALC);
  // _pfModelProfile->IncrementTimerAveragingCounter(CModelProfile::PTI_VIEW_SIMP_CALC);

  // Get shadow plane in object space
  FLOATmatrix3D mAbsToObj;
  FLOAT3D vAbsToObj;

  // Fix this
  Matrix12ToMatrixVector(mAbsToObj, vAbsToObj, _mObjectToAbs);
  FLOATplane3D plShadowPlaneObj = (plShadowPlane - vAbsToObj) * !mAbsToObj;

  // Project object handle so we can calc how it is far away from viewer
  FLOAT3D vRef = plShadowPlaneObj.ProjectPoint(FLOAT3D(0, 0, 0));
  TransformVector(vRef.vector,_mObjToViewStretch);
  plShadowPlaneObj.pl_distance += ClampDn(-vRef(3) * 0.001f, 0.01f); // move plane towards the viewer a bit to avoid z-fighting

  FLOATaabbox3D box;
  mi.GetCurrentCollisionBox(box);
  
  // Find points on plane nearest to bounding box edges
  FLOAT3D vMin = box.Min() * 1.25f;
  FLOAT3D vMax = box.Max() * 1.25f;
  
  // Enlarge shadow for 1st person view
  if (_ulRenFlags & SRMF_SPECTATOR) {
    vMin *= 2;
    vMax *= 2;
  }
  
  FLOAT3D v00 = plShadowPlaneObj.ProjectPoint(FLOAT3D(vMin(1), vMin(2), vMin(3)));
  FLOAT3D v01 = plShadowPlaneObj.ProjectPoint(FLOAT3D(vMin(1), vMin(2), vMax(3)));
  FLOAT3D v10 = plShadowPlaneObj.ProjectPoint(FLOAT3D(vMax(1), vMin(2), vMin(3)));
  FLOAT3D v11 = plShadowPlaneObj.ProjectPoint(FLOAT3D(vMax(1), vMin(2), vMax(3)));
  TransformVector(v00.vector, _mObjToViewStretch);
  TransformVector(v01.vector, _mObjToViewStretch);
  TransformVector(v10.vector, _mObjToViewStretch);
  TransformVector(v11.vector, _mObjToViewStretch);

  // calc done
  // _pfModelProfile->StopTimer(CModelProfile::PTI_VIEW_SIMP_CALC);

  // _pfModelProfile->StartTimer(CModelProfile::PTI_VIEW_SIMP_COPY);
  // _pfModelProfile->IncrementTimerAveragingCounter(CModelProfile::PTI_VIEW_SIMP_COPY);

  // prepare color
  ASSERT(fIntensity >= 0 && fIntensity <= 1);
  ULONG ulAAAA = NormFloatToByte(fIntensity);
  ulAAAA |= (ulAAAA << 8) | (ulAAAA << 16); // alpha isn't needed

  // Add to vertex arrays
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);
  
  // Vertices
  pvtx[0].x = v00(1);  pvtx[0].y = v00(2);  pvtx[0].z = v00(3);
  pvtx[2].x = v11(1);  pvtx[2].y = v11(2);  pvtx[2].z = v11(3);
  if (_ulRenFlags & SRMF_INVERTED) { // must re-adjust order for mirrored projection
    pvtx[1].x = v10(1);  pvtx[1].y = v10(2);  pvtx[1].z = v10(3);
    pvtx[3].x = v01(1);  pvtx[3].y = v01(2);  pvtx[3].z = v01(3);
  } else {
    pvtx[1].x = v01(1);  pvtx[1].y = v01(2);  pvtx[1].z = v01(3);
    pvtx[3].x = v10(1);  pvtx[3].y = v10(2);  pvtx[3].z = v10(3);
  }
  
  // Texture coords
  ptex[0].s = 0;  ptex[0].t = 0;
  ptex[1].s = 0;  ptex[1].t = 1;
  ptex[2].s = 1;  ptex[2].t = 1;
  ptex[3].s = 1;  ptex[3].t = 0;
  
  // Colors
  pcol[0].abgr = ulAAAA;
  pcol[1].abgr = ulAAAA;
  pcol[2].abgr = ulAAAA;
  pcol[3].abgr = ulAAAA;

  // If this model has fog
  if (_ulRenFlags & SRMF_FOG)
  {
    // For each vertex in shadow quad
    GFXTexCoord tex;
    for (INDEX i = 0; i < 4; i++)
    {
      GFXVertex &vtx = pvtx[i];
      // Get distance along viewer axis and fog axis and map to texture and attenuate shadow color
      const FLOAT fH = vtx.x * _fog_vHDirView(1) + vtx.y * _fog_vHDirView(2) + vtx.z * _fog_vHDirView(3);
      tex.s = -vtx.z * _fog_fMulZ;
      tex.t = (fH + _fog_fAddH) * _fog_fMulH;
      pcol[i].AttenuateRGB(GetFogAlpha(tex) ^ 255);
    }
  }
  // If this model has haze
  if (_ulRenFlags & SRMF_HAZE)
  {
    // For each vertex in shadow quad
    for (INDEX i = 0; i < 4; i++)
    {
      // Get distance along viewer axis map to texture  and attenuate shadow color
      const FLOAT fS = (_haze_fAdd - pvtx[i].z) * _haze_fMul;
      pcol[i].AttenuateRGB(GetHazeAlpha(fS) ^ 255);
    }
  }

  // one simple shadow added to rendering queue
  // _pfModelProfile->StopTimer(CModelProfile::PTI_VIEW_SIMP_COPY);
  // _pfModelProfile->StopTimer(CModelProfile::PTI_VIEW_RENDERSIMPLESHADOW);
}

// Set callback function for bone adjustment
void RM_SetBoneAdjustCallback(void (*pAdjustBones)(void *pData), void *pData)
{
  _pAdjustBonesCallback = pAdjustBones;
  _pAdjustBonesData = pData;
}

// TODO: Add comment here
void RM_SetShaderParamsAdjustCallback(void (*pAdjustShaderParams)(void *pData, INDEX iSurfaceID, CShaderClass *pShader,
                                                                  ShaderParams &spParams),
                                      void *pData)
{
  _pAdjustShaderParams = pAdjustShaderParams;
  _pAdjustShaderData = pData;
}

// Show gound for ska studio
void RM_RenderGround(CTextureObject &to)
{
  gfxSetConstantColor(0xFFFFFFFF);
  gfxEnableDepthTest();
  gfxEnableDepthWrite();
  gfxDisableAlphaTest();
  gfxDisableBlend();
  gfxCullFace(GFX_NONE);
  CTextureData *ptd = (CTextureData*)to.GetData();
  ptd->SetAsCurrent();

  FLOAT3D vVtx = FLOAT3D(45, 0, 45);

  GFXVertex vBoxVtxs[4];
  GFXTexCoord tcBoxTex[4];
  INDEX aiIndices[6];

  // Set ground vertices
  vBoxVtxs[0].x =  vVtx(1); vBoxVtxs[0].y =  vVtx(2); vBoxVtxs[0].z = -vVtx(3);
  vBoxVtxs[1].x = -vVtx(1); vBoxVtxs[1].y =  vVtx(2); vBoxVtxs[1].z = -vVtx(3);
  vBoxVtxs[2].x = -vVtx(1); vBoxVtxs[2].y =  vVtx(2); vBoxVtxs[2].z =  vVtx(3);
  vBoxVtxs[3].x =  vVtx(1); vBoxVtxs[3].y =  vVtx(2); vBoxVtxs[3].z =  vVtx(3);
  
  // Set ground texcoords
  tcBoxTex[0].u =  vVtx(1); tcBoxTex[0].v =  0;
  tcBoxTex[1].u =        0; tcBoxTex[1].v =  0;
  tcBoxTex[2].u =        0; tcBoxTex[2].v =  vVtx(3);
  tcBoxTex[3].u =  vVtx(1); tcBoxTex[3].v =  vVtx(3);

  for (INDEX ivx = 0; ivx < 4; ivx++)
  {
    TransformVertex(vBoxVtxs[ivx], _mAbsToViewer);
  }
  aiIndices[0] = 0; aiIndices[1] = 2; aiIndices[2] = 1;
  aiIndices[3] = 0; aiIndices[4] = 3; aiIndices[5] = 2;

  gfxSetVertexArray(vBoxVtxs, 4, FALSE);
  gfxSetTexCoordArray(tcBoxTex, FALSE);
  gfxDrawElements(6, aiIndices);
}

// Render wirerame bounding box
static void RenderWireframeBox(FLOAT3D vMinVtx, FLOAT3D vMaxVtx, COLOR col)
{
  // Prepare wireframe settings
  gfxDisableTexture();
  
  // Fill vertex array so it represents bounding box
  FLOAT3D vBoxVtxs[8];
  vBoxVtxs[0] = FLOAT3D(vMinVtx(1), vMinVtx(2), vMinVtx(3));
  vBoxVtxs[1] = FLOAT3D(vMaxVtx(1), vMinVtx(2), vMinVtx(3));
  vBoxVtxs[2] = FLOAT3D(vMaxVtx(1), vMinVtx(2), vMaxVtx(3));
  vBoxVtxs[3] = FLOAT3D(vMinVtx(1), vMinVtx(2), vMaxVtx(3));
  vBoxVtxs[4] = FLOAT3D(vMinVtx(1), vMaxVtx(2), vMinVtx(3));
  vBoxVtxs[5] = FLOAT3D(vMaxVtx(1), vMaxVtx(2), vMinVtx(3));
  vBoxVtxs[6] = FLOAT3D(vMaxVtx(1), vMaxVtx(2), vMaxVtx(3));
  vBoxVtxs[7] = FLOAT3D(vMinVtx(1), vMaxVtx(2), vMaxVtx(3));

  for (INDEX iwx = 0; iwx < 8; iwx++) 
  {
    TransformVector(vBoxVtxs[iwx].vector, _mObjToViewStretch);
  }
  
  // Connect vertices into lines of bounding box
  INDEX iBoxLines[12][2];
  iBoxLines[ 0][0] = 0;  iBoxLines[ 0][1] = 1;  iBoxLines[ 1][0] = 1;  iBoxLines[ 1][1] = 2;
  iBoxLines[ 2][0] = 2;  iBoxLines[ 2][1] = 3;  iBoxLines[ 3][0] = 3;  iBoxLines[ 3][1] = 0;
  iBoxLines[ 4][0] = 0;  iBoxLines[ 4][1] = 4;  iBoxLines[ 5][0] = 1;  iBoxLines[ 5][1] = 5;
  iBoxLines[ 6][0] = 2;  iBoxLines[ 6][1] = 6;  iBoxLines[ 7][0] = 3;  iBoxLines[ 7][1] = 7;
  iBoxLines[ 8][0] = 4;  iBoxLines[ 8][1] = 5;  iBoxLines[ 9][0] = 5;  iBoxLines[ 9][1] = 6;
  iBoxLines[10][0] = 6;  iBoxLines[10][1] = 7;  iBoxLines[11][0] = 7;  iBoxLines[11][1] = 4;
  
  // For all vertices in bounding box
  for (INDEX i = 0; i < 12; i++)
  {
    // Get starting and ending vertices of one line
    FLOAT3D &v0 = vBoxVtxs[iBoxLines[i][0]];
    FLOAT3D &v1 = vBoxVtxs[iBoxLines[i][1]];
    _pdp->DrawLine3D(v0, v1, col);
  } 
}

// Render bounding box
static void RenderBox(FLOAT3D vMinVtx, FLOAT3D vMaxVtx, COLOR col)
{
  // Prepare settings
  gfxDisableTexture();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
  gfxCullFace(GFX_NONE);
  gfxDisableDepthWrite();

  gfxSetConstantColor(col);
  
  // Fill vertex array so it represents bounding box
  GFXVertex vBoxVtxs[8];
  vBoxVtxs[0].x = vMinVtx(1); vBoxVtxs[0].y = vMaxVtx(2); vBoxVtxs[0].z = vMinVtx(3);
  vBoxVtxs[1].x = vMinVtx(1); vBoxVtxs[1].y = vMaxVtx(2); vBoxVtxs[1].z = vMaxVtx(3);
  vBoxVtxs[2].x = vMaxVtx(1); vBoxVtxs[2].y = vMaxVtx(2); vBoxVtxs[2].z = vMinVtx(3);
  vBoxVtxs[3].x = vMaxVtx(1); vBoxVtxs[3].y = vMaxVtx(2); vBoxVtxs[3].z = vMaxVtx(3);

  vBoxVtxs[4].x = vMinVtx(1); vBoxVtxs[4].y = vMinVtx(2); vBoxVtxs[4].z = vMinVtx(3);
  vBoxVtxs[5].x = vMinVtx(1); vBoxVtxs[5].y = vMinVtx(2); vBoxVtxs[5].z = vMaxVtx(3);
  vBoxVtxs[6].x = vMaxVtx(1); vBoxVtxs[6].y = vMinVtx(2); vBoxVtxs[6].z = vMinVtx(3);
  vBoxVtxs[7].x = vMaxVtx(1); vBoxVtxs[7].y = vMinVtx(2); vBoxVtxs[7].z = vMaxVtx(3);

  for (INDEX iwx = 0; iwx < 8; iwx++)
  {
    TransformVertex(vBoxVtxs[iwx],_mObjToViewStretch);
  }
  INDEX aiIndices[36];
  aiIndices[ 0] = 0; aiIndices[ 1] = 3; aiIndices[ 2] = 1;
  aiIndices[ 3] = 0; aiIndices[ 4] = 2; aiIndices[ 5] = 3;
  aiIndices[ 6] = 5; aiIndices[ 7] = 1; aiIndices[ 8] = 3;
  aiIndices[ 9] = 7; aiIndices[10] = 5; aiIndices[11] = 3;
  aiIndices[12] = 2; aiIndices[13] = 7; aiIndices[14] = 3;
  aiIndices[15] = 6; aiIndices[16] = 7; aiIndices[17] = 2;
  aiIndices[18] = 4; aiIndices[19] = 2; aiIndices[20] = 0;
  aiIndices[21] = 4; aiIndices[22] = 6; aiIndices[23] = 2;
  aiIndices[24] = 5; aiIndices[25] = 0; aiIndices[26] = 1;
  aiIndices[27] = 5; aiIndices[28] = 4; aiIndices[29] = 0;
  aiIndices[30] = 4; aiIndices[31] = 5; aiIndices[32] = 7;
  aiIndices[33] = 6; aiIndices[34] = 4; aiIndices[35] = 7;

  gfxSetVertexArray(vBoxVtxs, 8, FALSE);
  gfxDrawElements(36, aiIndices);

  gfxDisableBlend();
  gfxEnableDepthTest();

  RenderWireframeBox(vMinVtx, vMaxVtx, C_BLACK | CT_OPAQUE);
  gfxEnableDepthWrite();
  gfxDisableDepthBias();
}

// Render bounding box on screen
void RM_RenderCollisionBox(CModelInstance &mi, CollisionBox &cb, COLOR col)
{
  //CollisionBox &cb = mi.GetCollisionBox(icb);
  gfxSetViewMatrix(NULL);
  if (RM_GetFlags() & RMF_WIREFRAME) {
    RenderWireframeBox(cb.Min(), cb.Max(), col | CT_OPAQUE);
  } else {
    gfxEnableBlend();
    gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
    RenderBox(cb.Min(), cb.Max(), col | 0x7F);
    gfxDisableBlend();
  }
}

// Draw wireframe mesh on screen
static void RenderMeshWireframe(RenMesh &rmsh)
{
  CSkelMeshLod &mlod = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex];
  
  // Count surfaces in mesh
  INDEX ctsrf = mlod.mlod_aSurfaces.Count();
  
  // For each surface
  for (INDEX isrf = 0; isrf < ctsrf; isrf++)
  {
    MeshSurface &msrf = mlod.mlod_aSurfaces[isrf];
    COLOR colErrColor = 0xCDCDCDFF;
    
    // Surface has no shader, just show vertices
    shaClean();
    shaSetVertexArray((GFXVertex*)&_pavWeightedVertices[msrf.msrf_iFirstVertex], msrf.msrf_ctVertices);
    shaSetIndices(&msrf.msrf_aTriangles[0].iVertex[0], msrf.msrf_aTriangles.Count() * 3);
    shaSetTexture(-1);
    shaSetColorArray(&colErrColor,1);
    shaSetColor(0);
    shaDisableBlend();
    shaRender();
    shaClean();
  }
}

// Render model wireframe
static void RenderModelWireframe(RenModel &rm)
{
  INDEX ctmsh = rm.rm_iFirstMesh + rm.rm_ctMeshes;
  // for each mesh in renmodel
  for (int imsh = rm.rm_iFirstMesh; imsh < ctmsh; imsh++)
  {
    // Render mesh
    RenMesh &rmsh = _aRenMesh[imsh];
    PrepareMeshForRendering(rmsh,rm.rm_iSkeletonLODIndex);
    RenderMeshWireframe(rmsh);
  }
}

// Render normals
static void RenderNormals()
{
  // Only if rendering to view
  if (_iRenderingType != 1) {
    return;
  }
  
  gfxDisableTexture();
  INDEX ctNormals = _aFinalNormals.Count();
  for (INDEX ivx = 0;ivx < ctNormals; ivx++)
  {
    FLOAT3D vNormal = FLOAT3D(_panWeightedNormals[ivx].nx, _panWeightedNormals[ivx].ny, _panWeightedNormals[ivx].nz);
    // vNormal.Normalize();
    FLOAT3D vVtx1 = FLOAT3D(_pavWeightedVertices[ivx].x, _pavWeightedVertices[ivx].y, _pavWeightedVertices[ivx].z);
    FLOAT3D vVtx2 = vVtx1 + (vNormal / 5);
    _pdp->DrawLine3D(vVtx1, vVtx2, 0xFFFFFFFF);
  }
}

// Render one renbone
static void RenderBone(RenBone &rb, COLOR col)
{
  FLOAT fSize = rb.rb_psbBone->sb_fBoneLength / 20;
  FLOAT3D vBoneStart = FLOAT3D(rb.rb_mBonePlacement[3], rb.rb_mBonePlacement[7], rb.rb_mBonePlacement[11]);
  FLOAT3D vBoneEnd = FLOAT3D(0, 0, -rb.rb_psbBone->sb_fBoneLength);
  FLOAT3D vRingPt[4];
  
  vRingPt[0] = FLOAT3D(-fSize, -fSize, -fSize * 2);
  vRingPt[1] = FLOAT3D( fSize, -fSize, -fSize * 2);
  vRingPt[2] = FLOAT3D( fSize,  fSize, -fSize * 2);
  vRingPt[3] = FLOAT3D(-fSize,  fSize, -fSize * 2);
  
  TransformVector(vBoneEnd.vector, rb.rb_mBonePlacement);
  TransformVector(vRingPt[0].vector, rb.rb_mBonePlacement);
  TransformVector(vRingPt[1].vector, rb.rb_mBonePlacement);
  TransformVector(vRingPt[2].vector, rb.rb_mBonePlacement);
  TransformVector(vRingPt[3].vector, rb.rb_mBonePlacement);

  // connect start point of bone with end point
  INDEX il = 0;
  for (; il < 4; il++) {
    _pdp->DrawLine3D(vBoneStart, vRingPt[il], col);
    _pdp->DrawLine3D(vBoneEnd, vRingPt[il], col);
  }

  // Вraw ring
  for (il = 0; il < 3; il++)
  {
    _pdp->DrawLine3D(vRingPt[il],vRingPt[il + 1],col);
  }
  _pdp->DrawLine3D(vRingPt[0], vRingPt[3], col);
}

// Render one bone in model instance
void RM_RenderBone(CModelInstance &mi,INDEX iBoneID)
{
  UBYTE ubFillColor = 127;
  TStaticStackArray<INDEX> aiRenModelIndices;
  TStaticStackArray<INDEX> aiRenMeshIndices;

  CalculateRenderingData(mi);

  gfxEnableBlend();
  gfxEnableDepthTest();

  INDEX iBoneIndex = -1; // index of selected bone in renbone array
  INDEX iWeightIndex = -1; // index of weight that have same id as bone
  
  // Find all renmeshes that uses this bone weightmap
  INDEX ctrm = _aRenModels.Count();
  
  // For each renmodel
  for (INDEX irm = 1; irm < ctrm; irm++)
  {
    RenModel &rm = _aRenModels[irm];
    
    // Try to find bone in this renmodel
    if (FindRenBone(rm,iBoneID,&iBoneIndex))
    {
      // For each renmesh in rm
      INDEX ctmsh = rm.rm_iFirstMesh+rm.rm_ctMeshes;
      for (INDEX imsh = rm.rm_iFirstMesh; imsh < ctmsh; imsh++)
      {
        RenMesh &rm = _aRenMesh[imsh];
        // For each weightmap in this renmesh
        INDEX ctwm = rm.rmsh_iFirstWeight + rm.rmsh_ctWeights;
        for (INDEX iwm = rm.rmsh_iFirstWeight; iwm < ctwm; iwm++)
        {
          RenWeight &rw = _aRenWeights[iwm];
          // If weight map id is same as bone id
          if (rw.rw_pwmWeightMap->mwm_iID == iBoneID)
          {
            INDEX &irmi = aiRenModelIndices.Push();
            INDEX &irmshi = aiRenMeshIndices.Push();
            
            // Rememeber this weight map 
            irmi = irm;
            irmshi = imsh;
            iWeightIndex = iwm;
          }
        }
      }
    }
  }

  // if weightmap is found
  if (iWeightIndex >= 0)
  {
    // show wertex weights for each mesh that uses this bones weightmap
    INDEX ctmshi = aiRenMeshIndices.Count();
    for (INDEX imshi = 0; imshi < ctmshi; imshi++)
    {
      INDEX iMeshIndex = aiRenMeshIndices[imshi]; // index of mesh that uses selected bone
      INDEX iModelIndex = aiRenModelIndices[imshi]; // index of model in witch is mesh
      RenModel &rm = _aRenModels[iModelIndex];
      RenMesh &rmsh = _aRenMesh[iMeshIndex];
      CSkelMeshLod &mlod = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex];
      
      // Create array of color
      INDEX ctVertices = mlod.mlod_aVertices.Count();
      _aMeshColors.PopAll();
      _aMeshColors.Push(ctVertices);
      memset(&_aMeshColors[0], ubFillColor, sizeof(_aMeshColors[0]) * ctVertices);
      // Prepare this mesh for rendering
      PrepareMeshForRendering(rmsh,rm.rm_iSkeletonLODIndex);

      // all vertices by default are not visible (have alpha set to 0 )
      for (INDEX ivx = 0; ivx < ctVertices; ivx++)
      {
        _aMeshColors[ivx].a = 0;
      }
    
      INDEX ctwm = rmsh.rmsh_iFirstWeight + rmsh.rmsh_ctWeights;
      
      // For each weightmap in this mesh
      for (INDEX irw = rmsh.rmsh_iFirstWeight; irw < ctwm; irw++)
      {
        RenWeight &rw = _aRenWeights[irw];
        if (rw.rw_iBoneIndex != iBoneIndex) {
          continue;
        }
        INDEX ctvw = rw.rw_pwmWeightMap->mwm_aVertexWeight.Count();
        // For each vertex in this veight
        for (int ivw = 0; ivw < ctvw; ivw++)
        {
          // Modify color and alpha value of this vertex 
          MeshVertexWeight &vw = rw.rw_pwmWeightMap->mwm_aVertexWeight[ivw];
          INDEX ivx = vw.mww_iVertex;
          _aMeshColors[ivx].r = 255;
          _aMeshColors[ivx].g = 127;
          _aMeshColors[ivx].b = 0;
          _aMeshColors[ivx].a += vw.mww_fWeight * 255; // _aMeshColors[ivx].a = 255;
        }
      }

      // Count surfaces in mesh
      INDEX ctsrf = mlod.mlod_aSurfaces.Count();
      
      // For each surface
      for (INDEX isrf = 0; isrf < ctsrf; isrf++)
      {
        MeshSurface &msrf = mlod.mlod_aSurfaces[isrf];
        shaSetVertexArray((GFXVertex*)&_pavWeightedVertices[msrf.msrf_iFirstVertex], msrf.msrf_ctVertices);
        shaSetNormalArray((GFXNormal*)&_panWeightedNormals[msrf.msrf_iFirstVertex]);
        shaSetIndices(&msrf.msrf_aTriangles[0].iVertex[0], msrf.msrf_aTriangles.Count() * 3);
        shaSetTexture(-1);
        shaCalculateLight();
        GFXColor *paColors = shaGetColorArray();
        
        // Replace current color array with weight color array
        memcpy(paColors, &_aMeshColors[msrf.msrf_iFirstVertex], sizeof(COLOR) * msrf.msrf_ctVertices);
        shaEnableBlend();
        shaBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
        
        // Render surface
        shaRender();
        shaClean();
      }
    }
  }
  
  // draw bone
  if (iBoneIndex >= 0)
  {
    gfxSetViewMatrix(NULL);
    gfxDisableDepthTest();
    
    // Show bone in yellow color
    RenderBone(_aRenBones[iBoneIndex], 0xFFFF00FF);
  }

  gfxDisableBlend();
  aiRenModelIndices.Clear();
  aiRenMeshIndices.Clear();     
  ClearRenArrays();
}

// Render skeleton hierarchy
static void RenderSkeleton(void)
{
  gfxSetViewMatrix(NULL);
  
  // For each bone, except the dummy one
  for (int irb = 1; irb < _aRenBones.Count(); irb++)
  {
    RenBone &rb = _aRenBones[irb];
    RenderBone(rb, 0x5A5ADCFF); // render in blue color
  }
}

// TODO: Add comment here
static void RenderActiveBones(RenModel &rm)
{
  CModelInstance *pmi = rm.rm_pmiModel;
  if (pmi == NULL) {
    return;
  }
  
  // Count animlists
  INDEX ctal = pmi->mi_aqAnims.aq_Lists.Count();
  
  // Find newes animlist that has fully faded in
  INDEX iFirstAnimList = 0;
  
  // Loop from newer to older
  INDEX ial = ctal - 1;
  for (; ial >= 0; ial--)
  {
    AnimList &alList = pmi->mi_aqAnims.aq_Lists[ial];
    
    // Calculate fade factor
    FLOAT fFadeFactor = CalculateFadeFactor(alList);
    if (fFadeFactor >= 1.0f) {
      iFirstAnimList = ial;
      break;
    }
  }
  
  // For each anim list after iFirstAnimList
  for (ial = iFirstAnimList; ial < ctal; ial++)
  {
    AnimList &alList = pmi->mi_aqAnims.aq_Lists[ial];
    INDEX ctpa = alList.al_PlayedAnims.Count();
    
    // For each played anim
    for (INDEX ipa = 0; ipa < ctpa; ipa++)
    {
      PlayedAnim &pa = alList.al_PlayedAnims[ipa];
      INDEX iAnimSet,iAnimIndex;
      pmi->FindAnimationByID(pa.pa_iAnimID, &iAnimSet, &iAnimIndex);
      CBoneAnimSet &as = pmi->mi_aAnimSet[iAnimSet];
      CBoneAnimation &an = as.as_Anims[iAnimIndex];
      INDEX ctbe = an.an_abeBones.Count();
      
      // For each bone envelope
      for (INDEX ibe = 0; ibe < ctbe; ibe++)
      {
        BoneEnvelope &be = an.an_abeBones[ibe];
        INDEX iBoneIndex = 0;
        
        // Try to find renbone for this bone envelope
        if (FindRenBone(rm, be.be_iBoneID, &iBoneIndex)) {
          RenBone &rb = _aRenBones[iBoneIndex];
          
          // Render bone
          RenderBone(rb, 0x00FF00FF);
        }
      }

    }
  }
}

// TODO: Add comment here
static void RenderActiveBones(void)
{
  gfxSetViewMatrix(NULL);
  
  // For each renmodel
  INDEX ctrm = _aRenModels.Count();
  for (INT irm = 0; irm < ctrm; irm++)
  {
    RenModel &rm = _aRenModels[irm];
    RenderActiveBones(rm);
  }
}

// Get render flags for model
ULONG &RM_GetRenderFlags()
{
  return _ulRenFlags;
}

// Set new flag
void RM_SetFlags(ULONG ulNewFlags)
{
  _ulFlags = ulNewFlags;
}

// Get curent flags
ULONG RM_GetFlags()
{
  return _ulFlags;
}

// Add flag
void RM_AddFlag(ULONG ulFlag)
{
   _ulFlags |= ulFlag;
}

// Remove flag
void RM_RemoveFlag(ULONG ulFlag)
{
  _ulFlags &= ~ulFlag;
}

// Find texture data by id 
static void FindTextureData(CTextureObject **ptoTextures, INDEX iTextureID, MeshInstance &mshi)
{
  // For each texture instances
  INDEX ctti = mshi.mi_tiTextures.Count();
  for (INDEX iti = 0; iti < ctti; iti++)
  {
    TextureInstance &ti = mshi.mi_tiTextures[iti];
    if (ti.GetID() == iTextureID) {
      *ptoTextures = &ti.ti_toTexture;
      return;
    }
  }
  *ptoTextures = NULL;
}

// Find frame (binary) index in compresed array of rotations, positions or opt_rotations
static INDEX FindFrame(UBYTE *pFirstMember, INDEX iFind, INDEX ctfn, UINT uiSize)
{
  INDEX iHigh = ctfn - 1;
  INDEX iLow = 0;
  INDEX iMid;

  UWORD iHighFrameNum = *(UWORD*)(pFirstMember + (uiSize * iHigh));
  if (iFind == iHighFrameNum) {
    return iHigh;
  }
  
  while (TRUE)
  {
    iMid = (iHigh + iLow) / 2;
    UWORD iMidFrameNum = *(UWORD*)(pFirstMember + (uiSize * iMid));
    UWORD iMidFrameNumPlusOne = *(UWORD*)(pFirstMember + (uiSize * (iMid + 1)));
    if (iFind < iMidFrameNum) {
      iHigh = iMid;
    } else if ((iMid == iHigh) || (iMidFrameNumPlusOne > iFind)) {
      return iMid;
    } else {
      iLow = iMid;
    }
  }
}

// Find renbone in given renmodel
static BOOL FindRenBone(RenModel &rm, int iBoneID, INDEX *piBoneIndex)
{
  int ctb = rm.rm_iFirstBone + rm.rm_ctBones;
  
  // For each renbone in this ren model
  for (int ib = rm.rm_iFirstBone; ib < ctb; ib++)
  {
    // If bone id's match 
    if (iBoneID == _aRenBones[ib].rb_psbBone->sb_iID) {
      // Return index of this renbone
      *piBoneIndex = ib;
      return TRUE;
    }
  }
  return FALSE;
}

// Find renbone in whole array on renbones
RenBone *RM_FindRenBone(INDEX iBoneID)
{
  INDEX ctrb=_aRenBones.Count();
  // For each renbone
  for (INDEX irb = 1; irb < ctrb; irb++)
  {
    RenBone &rb = _aRenBones[irb];
    
    // If bone id's match
    if (rb.rb_psbBone->sb_iID == iBoneID) {
      // Return this renbone
      return &rb;
    }
  }
  return NULL;
}

// Return array of renbones
RenBone *RM_GetRenBoneArray(INDEX &ctrb)
{
  ctrb = _aRenBones.Count();
  if (ctrb > 0) {
    return &_aRenBones[0];
  } else {
    return NULL;
  }
}

// Find renmoph in given renmodel
static BOOL FindRenMorph(RenModel &rm, int iMorphID, INDEX *piMorphIndex)
{
  // For each renmesh in given renmodel
  INDEX ctmsh = rm.rm_iFirstMesh + rm.rm_ctMeshes;
  for (INDEX irmsh = rm.rm_iFirstMesh; irmsh < ctmsh; irmsh++)
  {
    // For each renmorph in this renmesh
    INDEX ctmm = _aRenMesh[irmsh].rmsh_iFirstMorph + _aRenMesh[irmsh].rmsh_ctMorphs;
    for (INDEX imm = _aRenMesh[irmsh].rmsh_iFirstMorph; imm < ctmm; imm++)
    {
      // If id's match
      if (iMorphID == _aRenMorphs[imm].rmp_pmmmMorphMap->mmp_iID) {
        // return this renmorph
        *piMorphIndex = imm;
        return TRUE;
      }
    }
  }
  // Renmorph was not found
  return FALSE;
}

// Find bone by ID (bone index must be set!)
static BOOL FindBone(int iBoneID, INDEX *piBoneIndex, CModelInstance *pmi, INDEX iSkeletonLod)
{
  // if model instance does not have skeleton
  if (pmi->mi_psklSkeleton == NULL) {
    return FALSE;
  }
  
  // If current skeleton lod is invalid
  if (iSkeletonLod < 0) {
    return FALSE;
  }

  INDEX ctslods = pmi->mi_psklSkeleton->skl_aSkeletonLODs.Count();
  // If skeleton lods count is invalid
  if (ctslods < 1) {
    return FALSE;
  }
  
  // if skeleton lod is larger than lod count
  if (iSkeletonLod >= ctslods) {
    // Use skeleton finest skeleton lod
    #pragma message(">> Check if this is ok")
    iSkeletonLod = 0;
    ASSERT(FALSE);
  }

  CSkeletonLod &slod = pmi->mi_psklSkeleton->skl_aSkeletonLODs[iSkeletonLod];
  
  // For each bone in skeleton lod
  for (int i = 0; i < slod.slod_aBones.Count(); i++)
  {
    // Check if bone id's match
    if (iBoneID == slod.slod_aBones[i].sb_iID) {
      // Bone index is allready set just return true
      return TRUE;
    }
    *piBoneIndex += 1;
  }

  // For each child of given model instance
  INDEX ctmich = pmi->mi_cmiChildren.Count();
  for (INDEX imich = 0; imich < ctmich; imich++)
  {
    // Try to find bone in child model instance
    if (FindBone(iBoneID, piBoneIndex, &pmi->mi_cmiChildren[imich], iSkeletonLod)) { 
      return TRUE;
    }
  }
  // Bone was not found
  return FALSE;
}

// Decompres axis for quaternion if animations are optimized
static void DecompressAxis(FLOAT3D &vNormal, UWORD ubH, UWORD ubP)
{
  ANGLE h = (ubH / 65535.0f) * 360.0f - 180.0f;
  ANGLE p = (ubP / 65535.0f) * 360.0f - 180.0f;

  FLOAT &x = vNormal(1);
  FLOAT &y = vNormal(2);
  FLOAT &z = vNormal(3);

  x = -Sin(h) * Cos(p);
  y =  Sin(p);
  z = -Cos(h) * Cos(p);
}

// Initialize batch model rendering
void RM_BeginRenderingView(CAnyProjection3D &apr, CDrawPort *pdp)
{
  // Remember parameters
  _iRenderingType = 1;
  _pdp = pdp;
  
  // Prepare and set the projection
  apr->ObjectPlacementL() = CPlacement3D(FLOAT3D(0, 0, 0), ANGLE3D(0, 0, 0));
  apr->Prepare();
  
  // In case of mirror projection, move mirror clip plane a bit father from the mirrored models,
  // so we have less clipping (for instance, player feet)
  if (apr->pr_bMirror) {
    apr->pr_plMirrorView.pl_distance -= 0.06f; // -0.06 is because entire projection is offseted by +0.05
  }
  _aprProjection = apr;
  _pdp->SetProjection(_aprProjection);

  // Remember the abs to viewer transformation
  MatrixVectorToMatrix12(_mAbsToViewer,
                         _aprProjection->pr_ViewerRotationMatrix,
                         -_aprProjection->pr_vViewerPosition * _aprProjection->pr_ViewerRotationMatrix);

  // Make FPU precision low
  _fpuOldPrecision = GetFPUPrecision(); 
  SetFPUPrecision(FPT_24BIT);
}

// Cleanup after batch model rendering
void RM_EndRenderingView(BOOL bRestoreOrtho/*=TRUE*/)
{
  ASSERT(_iRenderingType == 1 && _pdp != NULL);

  // Assure that FPU precision was low all the model rendering time, then revert to old FPU precision
  ASSERT(GetFPUPrecision() == FPT_24BIT);
  SetFPUPrecision(_fpuOldPrecision);

  // Back to 2D projection?
  if (bRestoreOrtho) {
    _pdp->SetOrtho();
  }
  _pdp->SetOrtho();
  _iRenderingType = 0;
  _pdp = NULL;
}

// For mark renderer
extern CAnyProjection3D _aprProjection;
extern UBYTE *_pubMask;
extern SLONG _slMaskWidth;
extern SLONG _slMaskHeight;

// Begin model rendering to shadow mask
void RM_BeginModelRenderingMask(CAnyProjection3D &prProjection, UBYTE *pubMask, SLONG slMaskWidth, SLONG slMaskHeight)
{
  ASSERT(_iRenderingType == 0);
  _iRenderingType = 2;
  _aprProjection = prProjection;
  _pubMask = pubMask;
  _slMaskWidth = slMaskWidth; 
  _slMaskHeight = slMaskHeight; 

  // Prepare and set the projection
  _aprProjection->ObjectPlacementL() = CPlacement3D(FLOAT3D(0, 0, 0), ANGLE3D(0, 0, 0));
  _aprProjection->Prepare();
  
  // Remember the abs to viewer transformation
  MatrixVectorToMatrix12(_mAbsToViewer,
                         _aprProjection->pr_ViewerRotationMatrix,
                         -_aprProjection->pr_vViewerPosition * _aprProjection->pr_ViewerRotationMatrix);

  // Set mask shader
  extern void InternalShader_Mask(void);
  extern void InternalShaderDesc_Mask(ShaderDesc &shDesc);
  _shMaskShader.ShaderFunc = InternalShader_Mask;
  _shMaskShader.GetShaderDesc = InternalShaderDesc_Mask;
}

// End model rendering to shadow mask
void RM_EndModelRenderingMask(void)
{
  ASSERT(_iRenderingType == 2);
  _iRenderingType = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Setup light color
void RM_SetLightColor(COLOR colAmbient, COLOR colLight)
{
  _colAmbient = colAmbient;
  _colLight = colLight;
}

// Setup light direction
void RM_SetLightDirection(FLOAT3D &vLightDir)
{
  _vLightDir = vLightDir * (-1);
}

// Calculate object matrices for givem model instance
void RM_SetObjectMatrices(CModelInstance &mi)
{
  ULONG ulFlags = RM_GetRenderFlags();

  // Adjust clipping to frustum
  if (ulFlags & SRMF_INSIDE) {
    gfxDisableClipping();
  } else {
    gfxEnableClipping();
  }

  // Adjust clipping to mirror-plane (if any)
  extern INDEX gap_iOptimizeClipping;
  // FIXME: Use &&
  if ((CProjection3D*)_aprProjection != NULL)
  {
    if (gap_iOptimizeClipping > 0 && (_aprProjection->pr_bMirror || _aprProjection->pr_bWarp))
    {
      if (ulFlags & SRMF_INMIRROR) {
        gfxDisableClipPlane();
        gfxFrontFace(GFX_CCW);
      } else {
        gfxEnableClipPlane();
        gfxFrontFace(GFX_CW);
      }
    }
  }

  MatrixMultiply(_mObjToView,_mAbsToViewer, _mObjectToAbs);

  Matrix12 mStretch;
  MakeStretchMatrix(mStretch, mi.mi_vStretch);
  MatrixMultiply(_mObjToViewStretch,_mObjToView,mStretch);
}

// Setup object position
void RM_SetObjectPlacement(const CPlacement3D &pl)
{
  FLOATmatrix3D m;
  MakeRotationMatrixFast(m, pl.pl_OrientationAngle);
  MatrixVectorToMatrix12(_mObjectToAbs, m, pl.pl_PositionVector);
}

// Setup object position
void RM_SetObjectPlacement(const FLOATmatrix3D &m, const FLOAT3D &v)
{
  MatrixVectorToMatrix12(_mObjectToAbs, m, v);
}

// Sets custom mesh lod
void RM_SetCustomMeshLodDistance(FLOAT fMeshLod)
{
  _fCustomMlodDistance = fMeshLod;
}

// Sets custom skeleton lod
void RM_SetCustomSkeletonLodDistance(FLOAT fSkeletonLod)
{
  _fCustomSlodDistance = fSkeletonLod;
}

// Returns index of skeleton lod at given distance
INDEX GetSkeletonLOD(CSkeleton &sk, FLOAT fDistance)
{
  FLOAT fMinDistance = 1000000.0f;
  INDEX iSkeletonLod = -1;

  // If custom lod distance is set
  if (_fCustomSlodDistance != -1) {
    // Set object distance as custom distance
    fDistance = _fCustomSlodDistance;
  }
  
  // For each lod in skeleton
  INDEX ctslods = sk.skl_aSkeletonLODs.Count();
  for (INDEX islod = 0; islod < ctslods; islod++)
  {
    CSkeletonLod &slod = sk.skl_aSkeletonLODs[islod];
    
    // Adjust lod distance by custom settings
    FLOAT fLodMaxDistance = slod.slod_fMaxDistance * sm_fLODMul + sm_fLODAdd;

    // Check if this lod max distance is smaller than distance to object
    if (fDistance < fLodMaxDistance && fLodMaxDistance < fMinDistance) {
      // Remember this lod
      fMinDistance = fLodMaxDistance;
      iSkeletonLod = islod;
    }
  }
  return iSkeletonLod;
}

// Returns index of mesh lod at given distance
INDEX GetSkelMeshLod(CSkelMesh &msh, FLOAT fDistance)
{
  FLOAT fMinDistance = 1000000.0f;
  INDEX iMeshLod = -1;

  // If custom lod distance is set
  if (_fCustomMlodDistance != -1) {
    // Set object distance as custom distance
    fDistance = _fCustomMlodDistance;
  }
  
  // For each lod in mesh
  INDEX ctmlods = msh.msh_aLods.Count();
  for (INDEX imlod = 0;imlod < ctmlods; imlod++)
  {
    CSkelMeshLod &mlod = msh.msh_aLods[imlod];
    // adjust lod distance by custom settings
    FLOAT fLodMaxDistance = mlod.mlod_fMaxDistance * sm_fLODMul + sm_fLODAdd;

    // Check if this lod max distance is smaller than distance to object
    if (fDistance < fLodMaxDistance && fLodMaxDistance < fMinDistance){
      // Remember this lod
      fMinDistance = fLodMaxDistance;
      iMeshLod = imlod;
    }
  }
  return iMeshLod;
}

// Create first dummy model that serves as parent for the entire hierarchy
void MakeRootModel(void)
{
  // Create the model with one bone
  RenModel &rm = _aRenModels.Push();
  rm.rm_pmiModel = NULL;
  rm.rm_iFirstBone = 0;
  rm.rm_ctBones = 1;
  rm.rm_iParentBoneIndex = -1;
  rm.rm_iParentModelIndex = -1;
  
  // Add the default bone
  RenBone &rb = _aRenBones.Push();
  rb.rb_iParentIndex = -1;
  rb.rb_psbBone = NULL;
  memset(&rb.rb_apPos, 0, sizeof(AnimPos));
  memset(&rb.rb_arRot, 0, sizeof(AnimRot));
}

// Build model hierarchy
static INDEX BuildHierarchy(CModelInstance *pmiModel, INDEX irmParent)
{
  INDEX ctrm = _aRenModels.Count();
  // Add one renmodel
  RenModel &rm = _aRenModels.Push();
  RenModel &rmParent = _aRenModels[irmParent];

  rm.rm_pmiModel = pmiModel;
  rm.rm_iParentModelIndex = irmParent;
  rm.rm_iNextSiblingModel = -1;
  rm.rm_iFirstBone = _aRenBones.Count();
  rm.rm_ctBones = 0;

  // If this model is root model
  if (pmiModel->mi_iParentBoneID == (-1)) {
    // Set is parent bone index as 0
    rm.rm_iParentBoneIndex = rmParent.rm_iFirstBone;
  
  // Model instance is attached to another model's bone 
  } else {
    INDEX iParentBoneIndex = -1;
    
    // Does parent model insntance has a skeleton
    if (rmParent.rm_pmiModel->mi_psklSkeleton != NULL && rmParent.rm_iSkeletonLODIndex >= 0)  {
      // Get index of parent bone
      iParentBoneIndex = rmParent.rm_pmiModel->mi_psklSkeleton->FindBoneInLOD(pmiModel->mi_iParentBoneID, rmParent.rm_iSkeletonLODIndex);
    
    // Model instance does not have skeleton
    } else {
      // Do not draw this model
      _aRenModels.Pop();
      return -1;
    }
    
    // If parent bone index was not found (not visible in current lod)
    if (iParentBoneIndex == (-1)) {
      // Do not draw this model
      _aRenModels.Pop();
      return -1;
    
    // Parent bone exists and its visible
    } else {
      // Set this model parent bone index in array of renbones
      rm.rm_iParentBoneIndex = iParentBoneIndex + rmParent.rm_iFirstBone;
    }
  }
 
  // If this model instance has skeleton
  if (pmiModel->mi_psklSkeleton != NULL)
  {
    // Adjust mip factor in case of dynamic stretch factor
    FLOAT fDistFactor = _fDistanceFactor;
    FLOAT3D &vStretch = pmiModel->mi_vStretch;
    
    // If model is stretched 
    if (vStretch != FLOAT3D(1, 1, 1)){
      // Calculate new distance factor
      fDistFactor = fDistFactor / Max(vStretch(1), Max(vStretch(2), vStretch(3)));
    }
    
    // Calculate its current skeleton lod
    rm.rm_iSkeletonLODIndex = GetSkeletonLOD(*pmiModel->mi_psklSkeleton, fDistFactor);
    
    // If current skeleton lod is valid and visible
    if (rm.rm_iSkeletonLODIndex > -1)
    {
      // Count all bones in this skeleton
      INDEX ctsb = pmiModel->mi_psklSkeleton->skl_aSkeletonLODs[rm.rm_iSkeletonLODIndex].slod_aBones.Count();
      
      // For each bone in skeleton
      for (INDEX irb = 0; irb < ctsb; irb++)
      {
        SkeletonBone *pSkeletonBone = &pmiModel->mi_psklSkeleton->skl_aSkeletonLODs[rm.rm_iSkeletonLODIndex].slod_aBones[irb];
        
        // Add one renbone
        RenBone &rb = _aRenBones.Push();
        rb.rb_psbBone = pSkeletonBone;
        rb.rb_iRenModelIndex = ctrm;
        rm.rm_ctBones++;
        
        // Add default bone position (used if no animations)
        rb.rb_apPos.ap_vPos = pSkeletonBone->sb_qvRelPlacement.vPos;
        rb.rb_arRot.ar_qRot = pSkeletonBone->sb_qvRelPlacement.qRot;

        // If this is root bone for this model instance
        if (pSkeletonBone->sb_iParentID == (-1)) {
          // Set its parent bone index to be parent bone of this model instance
          rb.rb_iParentIndex = rm.rm_iParentBoneIndex;
          
        // This is child bone
        } else {
          // Get parent index in array of renbones
          INDEX rb_iParentIndex = pmiModel->mi_psklSkeleton->FindBoneInLOD(pSkeletonBone->sb_iParentID, rm.rm_iSkeletonLODIndex);
          rb.rb_iParentIndex = rb_iParentIndex + rm.rm_iFirstBone;
        }
      }
    }
  }
  
  rm.rm_iFirstMesh = _aRenMesh.Count();
  rm.rm_ctMeshes = 0;

  INDEX ctm = pmiModel->mi_aMeshInst.Count();
  // For each mesh instance in this model instance
  for (INDEX im = 0;im < ctm; im++)
  {
    // Adjust mip factor in case of dynamic stretch factor
    FLOAT fDistFactor = _fDistanceFactor;
    FLOAT3D &vStretch = pmiModel->mi_vStretch;
    
    // If model is stretched 
    if (vStretch != FLOAT3D(1, 1, 1)) {
      // Calculate new distance factor
      fDistFactor = fDistFactor / Max(vStretch(1), Max(vStretch(2), vStretch(3))); // Log2(Max(vStretch(1),Max(vStretch(2),vStretch(3))));
    }

    // Calculate current mesh lod
    INDEX iMeshLodIndex = GetSkelMeshLod(*pmiModel->mi_aMeshInst[im].mi_pMesh,fDistFactor);
    
    // If mesh lod is visible
    if (iMeshLodIndex > -1)
    {
      // add one ren mesh
      RenMesh &rmsh = _aRenMesh.Push();
      rm.rm_ctMeshes++;
      rmsh.rmsh_iRenModelIndex = ctrm;
      rmsh.rmsh_pMeshInst = &pmiModel->mi_aMeshInst[im];
      rmsh.rmsh_iFirstMorph = _aRenMorphs.Count();
      rmsh.rmsh_iFirstWeight = _aRenWeights.Count();
      rmsh.rmsh_ctMorphs = 0;
      rmsh.rmsh_ctWeights = 0;
      rmsh.rmsh_bTransToViewSpace = FALSE;
      
      // Set mesh lod index for this ren mesh
      rmsh.rmsh_iSkelMeshLodIndex = iMeshLodIndex;

      // For each morph map in this mesh lod
      INDEX ctmm = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex].mlod_aMorphMaps.Count();
      for (INDEX imm = 0; imm < ctmm; imm++)
      {
        // Add this morph map in array of renmorphs
        RenMorph &rm = _aRenMorphs.Push();
        rmsh.rmsh_ctMorphs++;
        rm.rmp_pmmmMorphMap = &rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex].mlod_aMorphMaps[imm];
        rm.rmp_fFactor = 0;
      }

      // For each weight map in this mesh lod
      INDEX ctw = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex].mlod_aWeightMaps.Count();
      for (INDEX iw = 0; iw < ctw; iw++)
      {
        // Add this weight map in array of renweights
        RenWeight &rw = _aRenWeights.Push();
        MeshWeightMap &mwm = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex].mlod_aWeightMaps[iw];
        rw.rw_pwmWeightMap = &mwm;
        rmsh.rmsh_ctWeights++;
        rw.rw_iBoneIndex = rm.rm_iFirstBone;
        
        // Find bone of this weight in current skeleton lod and get its index for this renweight
        if (!FindBone(mwm.mwm_iID, &rw.rw_iBoneIndex, pmiModel, rm.rm_iSkeletonLODIndex)) {
          // If bone not found, set boneindex in renweight to -1
          rw.rw_iBoneIndex = -1;
        }
      }
    }
  }

  rm.rm_iFirstChildModel = -1;
  
  // For each child in this model instance
  INDEX ctmich = pmiModel->mi_cmiChildren.Count();
  for (int imich = 0; imich < ctmich; imich++)
  {
    // Build hierarchy for child model instance
    INDEX irmChildIndex = BuildHierarchy(&pmiModel->mi_cmiChildren[imich], ctrm);
    
    // If child is visible 
    if (irmChildIndex != (-1)) {
      // Set model sibling
      _aRenModels[irmChildIndex].rm_iNextSiblingModel = rm.rm_iFirstChildModel;
      rm.rm_iFirstChildModel = irmChildIndex;
    }
  }
  return ctrm;
}

// Calculate transformations for all bones on already built hierarchy
static void CalculateBoneTransforms()
{
  // Put basic transformation in first dummy bone
  MatrixCopy(_aRenBones[0].rb_mTransform, _mObjToView);
  MatrixCopy(_aRenBones[0].rb_mStrTransform, _aRenBones[0].rb_mTransform);

  // If callback function was specified
  if (_pAdjustBonesCallback != NULL) {
    // Call callback function
    _pAdjustBonesCallback(_pAdjustBonesData);
  }

  Matrix12 mStretch;
  
  // For each renbone after first dummy one
  int irb = 1;
  for (; irb < _aRenBones.Count(); irb++)
  {
    Matrix12 mRelPlacement;
    Matrix12 mOffset;
    RenBone &rb = _aRenBones[irb];
    RenBone &rbParent = _aRenBones[rb.rb_iParentIndex];
    
    // Convert QVect of placement to matrix12
    QVect qv;
    qv.vPos = rb.rb_apPos.ap_vPos;
    qv.qRot = rb.rb_arRot.ar_qRot;
    QVectToMatrix12(mRelPlacement,qv);

    // If this is root bone
    if (rb.rb_psbBone->sb_iParentID == (-1))
    {
      // Stretch root bone
      RenModel &rm= _aRenModels[rb.rb_iRenModelIndex];
      MakeStretchMatrix(mStretch, rm.rm_pmiModel->mi_vStretch);
      

      RenModel &rmParent = _aRenModels[rb.rb_iRenModelIndex];
      QVectToMatrix12(mOffset, rmParent.rm_pmiModel->mi_qvOffset);
      
      // Add offset to root bone
      MatrixMultiplyCP(mRelPlacement,mOffset,mRelPlacement);

      Matrix12 mStrParentBoneTrans;
      
      // Create stretch matrix with parent bone transformations
      MatrixMultiplyCP(mStrParentBoneTrans, rbParent.rb_mStrTransform,mStretch);
      
      // Transform bone using stretch parent's transform, relative placement
      MatrixMultiply(rb.rb_mStrTransform, mStrParentBoneTrans, mRelPlacement);
      MatrixMultiply(rb.rb_mTransform, rbParent.rb_mTransform, mRelPlacement);
    } else {
      // Transform bone using parent's transform and relative placement
      MatrixMultiply(rb.rb_mStrTransform, rbParent.rb_mStrTransform, mRelPlacement);
      MatrixMultiply(rb.rb_mTransform, rbParent.rb_mTransform, mRelPlacement);
    }
    // Remember tranform matrix of bone placement for bone rendering
    MatrixCopy(rb.rb_mBonePlacement, rb.rb_mStrTransform);
  }

  // For each renmodel after first dummy one
  for (int irm = 1; irm < _aRenModels.Count(); irm++)
  {
    // Remember transforms for bone-less models for every renmodel, except the dummy one
    Matrix12 mOffset;
    Matrix12 mStretch;
    RenModel &rm = _aRenModels[irm];

    QVectToMatrix12(mOffset, rm.rm_pmiModel->mi_qvOffset);
    MakeStretchMatrix(mStretch, rm.rm_pmiModel->mi_vStretch);

    MatrixMultiply(rm.rm_mTransform, _aRenBones[rm.rm_iParentBoneIndex].rb_mTransform, mOffset);
    MatrixMultiply(rm.rm_mStrTransform, _aRenBones[rm.rm_iParentBoneIndex].rb_mStrTransform, mOffset);
    MatrixMultiplyCP(rm.rm_mStrTransform, rm.rm_mStrTransform, mStretch);
  }

  Matrix12 mInvert;
  
  // For each renbone
  for (irb = 1; irb < _aRenBones.Count(); irb++)
  {
    RenBone &rb = _aRenBones[irb];
    // Multiply every transform with invert matrix of bone abs placement
    MatrixTranspose(mInvert, rb.rb_psbBone->sb_mAbsPlacement);
    
    // Create two versions of transform matrices, stretch and normal for vertices and normals
    MatrixMultiplyCP(_aRenBones[irb].rb_mStrTransform, _aRenBones[irb].rb_mStrTransform, mInvert);
    MatrixMultiplyCP(_aRenBones[irb].rb_mTransform, _aRenBones[irb].rb_mTransform, mInvert);
  }
}

// Match animations in anim queue for bones
static void MatchAnims(RenModel &rm)
{
  const FLOAT fLerpedTick = _pTimer->GetLerpedCurrentTick();

  // Return if no animsets
  INDEX ctas = rm.rm_pmiModel->mi_aAnimSet.Count();
  if (ctas == 0) {
    return;
  }
  
  // Count animlists
  INDEX ctal = rm.rm_pmiModel->mi_aqAnims.aq_Lists.Count();
  
  // Find newes animlist that has fully faded in
  INDEX iFirstAnimList = 0;
  
  // Loop from newer to older
  INDEX ial = ctal - 1;
  for (; ial >= 0; ial--)
  {
    AnimList &alList = rm.rm_pmiModel->mi_aqAnims.aq_Lists[ial];
    // Calculate fade factor
    FLOAT fFadeFactor = CalculateFadeFactor(alList);
    if (fFadeFactor >= 1.0f) {
      iFirstAnimList = ial;
      break;
    }
  }

  // For each anim list after iFirstAnimList
  for (ial = iFirstAnimList; ial < ctal; ial++)
  {
    AnimList &alList = rm.rm_pmiModel->mi_aqAnims.aq_Lists[ial];
    AnimList *palListNext = NULL;
    if (ial + 1 < ctal) {
      palListNext = &rm.rm_pmiModel->mi_aqAnims.aq_Lists[ial + 1];
    }
    
    // Calculate fade factor
    FLOAT fFadeFactor = CalculateFadeFactor(alList);

    INDEX ctpa = alList.al_PlayedAnims.Count();
    
    // For each played anim in played anim list
    for (int ipa = 0; ipa < ctpa; ipa++)
    {
      FLOAT fTime = fLerpedTick;
      PlayedAnim &pa = alList.al_PlayedAnims[ipa];
      BOOL bAnimLooping = pa.pa_ulFlags & AN_LOOPING;

      INDEX iAnimSetIndex;
      INDEX iAnimIndex;
      
      // Find anim by ID in all anim sets within this model
      if (rm.rm_pmiModel->FindAnimationByID(pa.pa_iAnimID,&iAnimSetIndex,&iAnimIndex))
      {
        // If found, animate bones
        CBoneAnimation &an = rm.rm_pmiModel->mi_aAnimSet[iAnimSetIndex].as_Anims[iAnimIndex];
        
        // Calculate end time for this animation list
        FLOAT fFadeInEndTime = alList.al_fStartTime + alList.al_fFadeTime;

        // If there is a newer anmimation list
        if (palListNext != NULL) {
          // Freeze time of this one to never overlap with the newer list
          fTime = ClampUp(fTime, palListNext->al_fStartTime);
        }

        // Calculate time passed since the animation started
        FLOAT fTimeOffset = fTime - pa.pa_fStartTime;
        
        // If this animation list is fading in
        if (fLerpedTick < fFadeInEndTime) {
          // Offset the time so that it is paused at the end of fadein interval
          fTimeOffset += fFadeInEndTime - fLerpedTick;
        }

        FLOAT f = fTimeOffset / (an.an_fSecPerFrame * pa.pa_fSpeedMul);

        INDEX iCurentFrame;
        INDEX iAnimFrame,iNextAnimFrame;
        
        if (bAnimLooping) {
          f = fmod(f, an.an_iFrames);
          iCurentFrame = INDEX(f);
          iAnimFrame = iCurentFrame % an.an_iFrames;
          iNextAnimFrame = (iCurentFrame + 1) % an.an_iFrames;
        } else {
          if (f > an.an_iFrames) {
            f = an.an_iFrames - 1;
          }
          iCurentFrame = INDEX(f);
          iAnimFrame = ClampUp(iCurentFrame, an.an_iFrames - 1L);
          iNextAnimFrame = ClampUp(iCurentFrame + 1L, an.an_iFrames - 1L);
        }
        
        // For each bone envelope
        INDEX ctbe = an.an_abeBones.Count();
        for (int ibe = 0; ibe < ctbe; ibe++)
        {
          INDEX iBoneIndex;
          // Find its renbone in array of renbones
          if (FindRenBone(rm,an.an_abeBones[ibe].be_iBoneID, &iBoneIndex))
          {
            RenBone &rb = _aRenBones[iBoneIndex];
            BoneEnvelope &be = an.an_abeBones[ibe];

            INDEX iRotFrameIndex;
            INDEX iNextRotFrameIndex;
            INDEX iRotFrameNum;
            INDEX iNextRotFrameNum;
            FLOAT fSlerpFactor;
            FLOATquat3D qRot;
            FLOATquat3D qRotCurrent;
            FLOATquat3D qRotNext;
            FLOATquat3D *pqRotCurrent;
            FLOATquat3D *pqRotNext;
            
            // If animation is not compresed
            if (!an.an_bCompresed)
            {
              AnimRot *arFirst = &be.be_arRot[0];
              INDEX ctfn = be.be_arRot.Count();
              
              // Find index of closest frame
              iRotFrameIndex = FindFrame((UBYTE*)arFirst, iAnimFrame, ctfn, sizeof(AnimRot));
              
              // Get index of next frame
              if (bAnimLooping) {
                iNextRotFrameIndex = (iRotFrameIndex + 1) % be.be_arRot.Count();
              } else {
                iNextRotFrameIndex = ClampUp(iRotFrameIndex + 1L, INDEX(be.be_arRot.Count() - 1));
              }
              
              iRotFrameNum = be.be_arRot[iRotFrameIndex].ar_iFrameNum;
              iNextRotFrameNum = be.be_arRot[iNextRotFrameIndex].ar_iFrameNum;
              pqRotCurrent = &be.be_arRot[iRotFrameIndex].ar_qRot;
              pqRotNext = &be.be_arRot[iNextRotFrameIndex].ar_qRot;
              
            // Animation is not compresed
            } else {
              AnimRotOpt *aroFirst = &be.be_arRotOpt[0];
              INDEX ctfn = be.be_arRotOpt.Count();
              iRotFrameIndex = FindFrame((UBYTE*)aroFirst, iAnimFrame, ctfn, sizeof(AnimRotOpt));

              // Get index of next frame
              if (bAnimLooping) { 
                iNextRotFrameIndex = (iRotFrameIndex + 1L) % be.be_arRotOpt.Count();
              } else {
                iNextRotFrameIndex = ClampUp(iRotFrameIndex + 1L, INDEX(be.be_arRotOpt.Count() - 1));
              }

              AnimRotOpt &aroRot = be.be_arRotOpt[iRotFrameIndex];
              AnimRotOpt &aroRotNext = be.be_arRotOpt[iNextRotFrameIndex];
              iRotFrameNum = aroRot.aro_iFrameNum;
              iNextRotFrameNum = aroRotNext.aro_iFrameNum;
              FLOAT3D vAxis;
              ANGLE aAngle;

              // Decompress angle
              aAngle = aroRot.aro_aAngle / ANG_COMPRESIONMUL;
              DecompressAxis(vAxis, aroRot.aro_ubH, aroRot.aro_ubP);
              qRotCurrent.FromAxisAngle(vAxis, aAngle);

              aAngle = aroRotNext.aro_aAngle / ANG_COMPRESIONMUL;
              DecompressAxis(vAxis, aroRotNext.aro_ubH, aroRotNext.aro_ubP);
              qRotNext.FromAxisAngle(vAxis, aAngle);
              pqRotCurrent = &qRotCurrent;
              pqRotNext = &qRotNext;
            }

            if (iNextRotFrameNum <= iRotFrameNum) {
              // Calculate slerp factor for rotations
              fSlerpFactor = (f - iRotFrameNum) / (an.an_iFrames - iRotFrameNum);
            } else {
              // Calculate slerp factor for rotations
              fSlerpFactor = (f - iRotFrameNum) / (iNextRotFrameNum - iRotFrameNum);
            }
            
            // Calculate rotation for bone beetwen current and next frame in animation
            qRot = Slerp<FLOAT>(fSlerpFactor, *pqRotCurrent, *pqRotNext);
            
            // and currently playing animation 
            rb.rb_arRot.ar_qRot = Slerp<FLOAT>(fFadeFactor * pa.pa_Strength, rb.rb_arRot.ar_qRot, qRot);

            AnimPos *apFirst = &be.be_apPos[0];
            INDEX ctfn = be.be_apPos.Count();
            INDEX iPosFrameIndex = FindFrame((UBYTE*)apFirst, iAnimFrame, ctfn, sizeof(AnimPos));

            INDEX iNextPosFrameIndex;
            // Is animation looping
            if (bAnimLooping) { 
              iNextPosFrameIndex = (iPosFrameIndex + 1) % be.be_apPos.Count();
            } else {
              iNextPosFrameIndex = ClampUp(iPosFrameIndex + 1L, INDEX(be.be_apPos.Count() - 1));
            }

            INDEX iPosFrameNum = be.be_apPos[iPosFrameIndex].ap_iFrameNum;
            INDEX iNextPosFrameNum = be.be_apPos[iNextPosFrameIndex].ap_iFrameNum;

            FLOAT fLerpFactor;
            if (iNextPosFrameNum <= iPosFrameNum) {
              fLerpFactor = (f - iPosFrameNum) / (an.an_iFrames - iPosFrameNum);
            } else {
              fLerpFactor = (f - iPosFrameNum) / (iNextPosFrameNum - iPosFrameNum);
            }
            
            FLOAT3D vPos;
            FLOAT3D vBonePosCurrent = be.be_apPos[iPosFrameIndex].ap_vPos;
            FLOAT3D vBonePosNext = be.be_apPos[iNextPosFrameIndex].ap_vPos;

            // If bone envelope and bone have some length 
            if ((be.be_OffSetLen > 0) && (rb.rb_psbBone->sb_fOffSetLen > 0)) {
              // Size bone to fit bone envelope
              vBonePosCurrent *= (rb.rb_psbBone->sb_fOffSetLen / be.be_OffSetLen);
              vBonePosNext *= (rb.rb_psbBone->sb_fOffSetLen / be.be_OffSetLen);
            }

            // Calculate position for bone beetwen current and next frame in animation
            vPos = Lerp(vBonePosCurrent, vBonePosNext, fLerpFactor);
            
            // And currently playing animation 
            rb.rb_apPos.ap_vPos = Lerp(rb.rb_apPos.ap_vPos, vPos, fFadeFactor * pa.pa_Strength);
          }
        }

        // For each morphmap
        for (INDEX im = 0; im < an.an_ameMorphs.Count(); im++)
        {
          INDEX iMorphIndex;
          // Find it in renmorph
          if (FindRenMorph(rm,an.an_ameMorphs[im].me_iMorphMapID, &iMorphIndex))
          {
            // Lerp morphs
            FLOAT &fCurFactor = an.an_ameMorphs[im].me_aFactors[iAnimFrame];
            FLOAT &fLastFactor = an.an_ameMorphs[im].me_aFactors[iNextAnimFrame];
            FLOAT fFactor = Lerp(fCurFactor, fLastFactor, f - iAnimFrame);

            _aRenMorphs[iMorphIndex].rmp_fFactor = Lerp(_aRenMorphs[iMorphIndex].rmp_fFactor,
                                                       fFactor,
                                                       fFadeFactor * pa.pa_Strength);
          }
        }
      }
    }
  }
}

// Array of pointers to texure data for shader
static TStaticStackArray<class CTextureObject*> _patoTextures;
static TStaticStackArray<struct GFXTexCoord*> _paTexCoords;

// Draw mesh on screen
static void RenderMesh(RenMesh &rmsh, RenModel &rm, ULONG ulShaderRenFlags)
{
  ASSERT(_pavWeightedVertices != NULL);
  ASSERT(_panWeightedNormals != NULL);
  
  if (!sm_bRenderMesh) {
    return;
  }
  
  // TODO: SetViewMatrix(rmsh.rmsh_mStrObjToView);

  CSkelMeshLod &mlod = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex];
  
  // Count surfaces in mesh
  INDEX ctsrf = mlod.mlod_aSurfaces.Count();
  
  // For each surface
  for (INDEX isrf = 0; isrf < ctsrf; isrf++)
  {
    MeshSurface &msrf = mlod.mlod_aSurfaces[isrf];
    CShaderClass  *pShader = msrf.msrf_pShader;
    if (_iRenderingType == 2) {
      pShader = &_shMaskShader; // force mask shader for rendering to shadowmaps
    }

    // If this surface has valid shader and show texure flag is set
    if (pShader != NULL && (RM_GetFlags() & RMF_SHOWTEXTURE))
    {
      // Create copy of shading params
      ShaderParams *pShaderParams = &msrf.msrf_ShadingParams;
      ShaderParams spForAdjustment;

      // If callback function was specified
      if (_pAdjustShaderParams != NULL) {
        // Call callback function
        spForAdjustment = msrf.msrf_ShadingParams;
        _pAdjustShaderParams(_pAdjustShaderData, msrf.msrf_iSurfaceID, pShader, spForAdjustment);
        pShaderParams = &spForAdjustment;
      }

      // Clamp surface texture count to max number of textrues in mesh
      INDEX cttx = pShaderParams->sp_aiTextureIDs.Count();
      INDEX cttxMax = rmsh.rmsh_pMeshInst->mi_tiTextures.Count();
      // cttx = ClampUp(cttx,cttxMax);

      _patoTextures.PopAll();
      if (cttx > 0) {
        _patoTextures.Push(cttx);
      }
      
      // for each texture ID
      for (INDEX itx = 0; itx < cttx; itx++)
      {
        // Find texture in mesh and get pointer to texture by texture ID
        FindTextureData(&_patoTextures[itx], pShaderParams->sp_aiTextureIDs[itx], *rmsh.rmsh_pMeshInst);
      }

      // Count uvmaps
      INDEX ctuvm = pShaderParams->sp_aiTexCoordsIndex.Count();
      // ctuvm = ClampUp(ctuvm,mlod.mlod_aUVMaps.Count());
      
      _paTexCoords.PopAll();
      if (ctuvm > 0) {
        _paTexCoords.Push(ctuvm);
      }
      
      // For each uvamp
      for (INDEX iuvm = 0; iuvm < ctuvm; iuvm++)
      {
        // Set pointer of uvmap in array of uvmaps for shader
        INDEX iuvmIndex = pShaderParams->sp_aiTexCoordsIndex[iuvm];
        
        // If mesh lod has this uv map
        if (iuvmIndex < mlod.mlod_aUVMaps.Count()) {
          _paTexCoords[iuvm] = (GFXTexCoord*)&mlod.mlod_aUVMaps[iuvmIndex].muv_aTexCoords[msrf.msrf_iFirstVertex];
        } else {
          _paTexCoords[iuvm] = NULL;
        }
      }

      INDEX ctTextures = _patoTextures.Count();
      INDEX ctTexCoords = _paTexCoords.Count();
      INDEX ctColors = pShaderParams->sp_acolColors.Count();
      INDEX ctFloats = pShaderParams->sp_afFloats.Count();

      // Begin model rendering
      const BOOL bModelSetupTimer = _sfStats.CheckTimer(CStatForm::STI_MODELSETUP);
      if (bModelSetupTimer) _sfStats.StopTimer(CStatForm::STI_MODELSETUP);
      _sfStats.StartTimer(CStatForm::STI_MODELRENDERING);

      shaBegin(_aprProjection, pShader);
      shaSetVertexArray((GFXVertex*)&_pavWeightedVertices[msrf.msrf_iFirstVertex],msrf.msrf_ctVertices);
      shaSetNormalArray((GFXNormal*)&_panWeightedNormals[msrf.msrf_iFirstVertex]);
      shaSetIndices(&msrf.msrf_aTriangles[0].iVertex[0], msrf.msrf_aTriangles.Count() * 3);
      shaSetFlags(msrf.msrf_ShadingParams.sp_ulFlags);
      shaSetRenFlags(ulShaderRenFlags);
      
      // If mesh is transformed to view space
      if (rmsh.rmsh_bTransToViewSpace)
      {
        #pragma message(">> FIX THIS !!!")
        // no ObjToView matrix is needed in shader so set empty matrix
        Matrix12 mIdentity;
        MakeIdentityMatrix(mIdentity);
        shaSetObjToViewMatrix(mIdentity);
        Matrix12 mInvObjToAbs;
        MatrixTranspose(mInvObjToAbs,_mAbsToViewer);
        shaSetObjToAbsMatrix(mInvObjToAbs);
      } else {
        // Give shader current ObjToView matrix
        shaSetObjToViewMatrix(_mObjToView);
        shaSetObjToAbsMatrix(_mObjectToAbs);
      }

      // Set light parametars
      shaSetLightColor(_colAmbient,_colLight);
      shaSetLightDirection(_vLightDirInView);
      
      // Set model color
      shaSetModelColor(rm.rm_pmiModel->mi_colModelColor);
      
      if (ctTextures > 0) {
        shaSetTextureArray(&_patoTextures[0], ctTextures);
      }
      if (ctTexCoords > 0) {
        shaSetUVMapsArray(&_paTexCoords[0], ctTexCoords);
      }
      if (ctColors > 0){
        shaSetColorArray(&pShaderParams->sp_acolColors[0], ctColors);
      }
      if (ctFloats > 0){
        shaSetFloatArray(&pShaderParams->sp_afFloats[0], ctFloats);
      }
      shaEnd();

      _sfStats.StopTimer(CStatForm::STI_MODELRENDERING);
      if (bModelSetupTimer) {
        _sfStats.StartTimer(CStatForm::STI_MODELSETUP);
      }
    
    // Surface has no shader or textures are turned off
    } else {
      COLOR colErrColor = 0xCDCDCDFF;
      
      // Surface has no shader, just show vertices using custom simple shader 
      shaSetVertexArray((GFXVertex*)&_pavWeightedVertices[msrf.msrf_iFirstVertex], msrf.msrf_ctVertices);
      shaSetNormalArray((GFXNormal*)&_panWeightedNormals[msrf.msrf_iFirstVertex]);
      shaSetIndices(&msrf.msrf_aTriangles[0].iVertex[0], msrf.msrf_aTriangles.Count() * 3);
      shaSetTexture(-1);
      shaSetColorArray(&colErrColor, 1);

      shaSetLightColor(_colAmbient, _colLight);
      shaSetLightDirection(_vLightDirInView);
      shaSetModelColor(rm.rm_pmiModel->mi_colModelColor);

      shaDisableBlend();
      shaEnableDepthTest();
      shaEnableDepthWrite();
      shaSetColor(0);
      shaCalculateLight();
      shaRender();
      shaClean();
    }
  }
}

__forceinline static void TransformVector_copy( FLOAT3 &vDst, FLOAT3 &vSrc, const Matrix12 &m)
{
  const FLOAT x = vSrc[0];
  const FLOAT y = vSrc[1];
  const FLOAT z = vSrc[2];
  vDst[0] = m[0]*x + m[1]*y + m[ 2]*z + m[ 3];
  vDst[1] = m[4]*x + m[5]*y + m[ 6]*z + m[ 7];
  vDst[2] = m[8]*x + m[9]*y + m[10]*z + m[11];
}

__forceinline static void TransformVector_copy( FLOAT3 &vDst, FLOAT3 &vSrc, const Matrix12 &m, FLOAT fWeight)
{
  const FLOAT x = vSrc[0];
  const FLOAT y = vSrc[1];
  const FLOAT z = vSrc[2];
  vDst[0] = (m[0]*x + m[1]*y + m[ 2]*z + m[ 3]) * fWeight;
  vDst[1] = (m[4]*x + m[5]*y + m[ 6]*z + m[ 7]) * fWeight;
  vDst[2] = (m[8]*x + m[9]*y + m[10]*z + m[11]) * fWeight;
}

__forceinline static void TransformVector_add( FLOAT3 &vDst, FLOAT3 &vSrc, const Matrix12 &m, FLOAT fWeight)
{
  const FLOAT x = vSrc[0];
  const FLOAT y = vSrc[1];
  const FLOAT z = vSrc[2];
  vDst[0] += (m[0]*x + m[1]*y + m[ 2]*z + m[ 3]) * fWeight;
  vDst[1] += (m[4]*x + m[5]*y + m[ 6]*z + m[ 7]) * fWeight;
  vDst[2] += (m[8]*x + m[9]*y + m[10]*z + m[11]) * fWeight;
}

__forceinline static void RotateVector_copy( FLOAT3 &vDst, FLOAT3 &vSrc, const Matrix12 &m)
{
  const FLOAT x = vSrc[0];
  const FLOAT y = vSrc[1];
  const FLOAT z = vSrc[2];
  vDst[0] = m[0]*x + m[1]*y + m[ 2]*z;
  vDst[1] = m[4]*x + m[5]*y + m[ 6]*z;
  vDst[2] = m[8]*x + m[9]*y + m[10]*z;
}

__forceinline static void RotateVector_copy( FLOAT3 &vDst, FLOAT3 &vSrc, const Matrix12 &m, FLOAT fWeight)
{
  const FLOAT x = vSrc[0];
  const FLOAT y = vSrc[1];
  const FLOAT z = vSrc[2];
  vDst[0] = (m[0]*x + m[1]*y + m[ 2]*z) * fWeight;
  vDst[1] = (m[4]*x + m[5]*y + m[ 6]*z) * fWeight;
  vDst[2] = (m[8]*x + m[9]*y + m[10]*z) * fWeight;
}

__forceinline static void RotateVector_add( FLOAT3 &vDst, FLOAT3 &vSrc, const Matrix12 &m, FLOAT fWeight)
{
  const FLOAT x = vSrc[0];
  const FLOAT y = vSrc[1];
  const FLOAT z = vSrc[2];
  vDst[0] += (m[0]*x + m[1]*y + m[ 2]*z) * fWeight;
  vDst[1] += (m[4]*x + m[5]*y + m[ 6]*z) * fWeight;
  vDst[2] += (m[8]*x + m[9]*y + m[10]*z) * fWeight;
}

// Prepares transformation matrices for each weight in surface in given mesh lod
inline static void PrepareMeshMatrices(const RenMesh &rmsh, const CSkelMeshLod &mlod)
{
  // for each surface
  INDEX ctmsrf = mlod.mlod_aSurfaces.Count();

  for (INDEX imsrf = 0; imsrf < ctmsrf; imsrf++) 
  {
    const MeshSurface &msrf = mlod.mlod_aSurfaces[imsrf];
    const INDEX ctwm = msrf.msrf_aubRelIndexTable.Count();
    
    // Add new SurfaceMatrices
    SurfaceMatrices &sm = _aSurfacesMatrices.Push();
    sm.sm_iFirstMatrix = _aWeightMatrices.Count();
    sm.sm_iSurfaceID = msrf.msrf_iSurfaceID;
    sm.sm_ctMatrices = ctwm;
    
    // Add two matrices to array of model matrices
    RenMatrix *prmMatrices;

    if (ctwm > 0) 
    {
      prmMatrices = _aWeightMatrices.Push(ctwm*2); // vertex + normal matrices
    }
    
    // Set weight matrix order by order of weight indices in surface
    for (INDEX iwm = 0; iwm < ctwm; iwm++) 
    {
      const INDEX imwm = msrf.msrf_aubRelIndexTable[iwm];
      const RenWeight &rw = _aRenWeights[rmsh.rmsh_iFirstWeight + imwm];

      // if no RenBone for this RenWeight.
      if(rw.rw_iBoneIndex == (-1)) 
      {
        // Use models default transformation matrix.
        const RenModel &rm = _aRenModels[rmsh.rmsh_iRenModelIndex];
        MatrixCopy(prmMatrices[iwm].rm_mTransform, rm.rm_mStrTransform);
        MatrixCopy(prmMatrices[iwm + ctwm].rm_mTransform, rm.rm_mTransform);
      }
      else 
      {
        // Use transformation matrix from RenBone.
        const RenBone &rb = _aRenBones[rw.rw_iBoneIndex];
        MatrixCopy(prmMatrices[iwm].rm_mTransform, rb.rb_mStrTransform);
        MatrixCopy(prmMatrices[iwm + ctwm].rm_mTransform, rb.rb_mTransform);
      }
    }
  }
}

#define SE_BM_NEW_WEIGHTS

// Prepare ren mesh for rendering
static void PrepareMeshForRendering(RenMesh &rmsh, INDEX iSkeletonLod)
{
  // Set curent mesh lod
  CSkelMeshLod &mlod = rmsh.rmsh_pMeshInst->mi_pMesh->msh_aLods[rmsh.rmsh_iSkelMeshLodIndex];
  
  // Clear vertices array
  _aMorphedVtxs.PopAll();
  _aMorphedNormals.PopAll();
  _aFinalVtxs.PopAll();
  _aFinalNormals.PopAll();
  _aWeightMatrices.PopAll();
  _aSurfacesMatrices.PopAll();
  
  // Reset variables
  _pavMeshVertices = &mlod.mlod_aVertices[0];
  _panMeshNormals  = &mlod.mlod_aNormals[0];
  _pawMeshWeights = mlod.mlod_aVertexWeights.Count() > 0 ? &mlod.mlod_aVertexWeights[0] : NULL;
  _pavMorphedVertices  = NULL;
  _panMorphedNormals   = NULL;
  _pavWeightedVertices = NULL;
  _panWeightedNormals  = NULL;
  
  // Reset light direction
  _vLightDirInView = _vLightDir;

  // Get vertices count
  INDEX ctVertices = mlod.mlod_aVertices.Count();
  
  // Allocate memory for vertices
  _aFinalVtxs.Push(ctVertices);
  _aFinalNormals.Push(ctVertices);
  
  // Remember final vertex count
  _ctFinalVertices = ctVertices;

  // Set final vertices and normals to 0
  memset(&_aFinalVtxs[0], 0, sizeof(MeshVertex) * ctVertices);
  memset(&_aFinalNormals[0], 0, sizeof(MeshNormal) * ctVertices);

  // Indices must be surface relative
  #ifdef SE_BM_NEW_WEIGHTS
  if (rmsh.rmsh_pMeshInst->mi_pMesh->msh_iVersion >= 16) {
    ASSERT(mlod.GetFlags() & ML_SURFACE_RELATIVE_INDICES);
  }
  #endif

  // Fetch some numbers.
  INDEX ctBones = 0;
  INDEX ctRenMorphs  = rmsh.rmsh_ctMorphs;
  INDEX ctRenWeights = rmsh.rmsh_ctWeights;
  INDEX iLastRenWeight = rmsh.rmsh_iFirstWeight + rmsh.rmsh_ctWeights;
  INDEX iLastRenMorph = rmsh.rmsh_iFirstMorph + rmsh.rmsh_ctMorphs;

  BOOL bMeshUsesMorphs = FALSE;

  CSkeleton *pskl = _aRenModels[rmsh.rmsh_iRenModelIndex].rm_pmiModel->mi_psklSkeleton;

  // If skeleton for this model exists and its currently visible
  if ((pskl != NULL) && (iSkeletonLod > -1)) {
    // count bones in skeleton
    ctBones = pskl->skl_aSkeletonLODs[iSkeletonLod].slod_aBones.Count();
  }
  
  // If mesh have some morph maps.
  if (sm_bTransformMesh && sm_bMorphMesh && ctRenMorphs > 0) {
    for (INDEX irmrh = rmsh.rmsh_iFirstMorph; irmrh < iLastRenMorph; irmrh++) 
    {
      const RenMorph &rm = _aRenMorphs[irmrh];

      if(_aRenMorphs[irmrh].rmp_fFactor >= 0.001f) 
      {
        bMeshUsesMorphs = TRUE;
        break;
      }
    }

    if (bMeshUsesMorphs) {
      _pavMorphedVertices = _aMorphedVtxs.Push(ctVertices);
      _panMorphedNormals = _aMorphedNormals.Push(ctVertices);
      
      // Copy original vertices/normals to buffer for further transformations.
      memcpy(&_pavMorphedVertices[0], &_pavMeshVertices[0], sizeof(MeshVertex) * ctVertices);
      memcpy(&_panMorphedNormals[0], &_panMeshNormals[0], sizeof(MeshNormal) * ctVertices);
    }
  }

  // Blend vertices and normals for each RenMorph 
  if (bMeshUsesMorphs) {
    for (int irm = rmsh.rmsh_iFirstMorph; irm < iLastRenMorph; irm++)
    {
      RenMorph &rm = _aRenMorphs[irm];
      
      // Blend only if factor is > 0
      if (rm.rmp_fFactor < 0.001f) {
        continue;
      }

      // For each vertex and normal in morphmap
      for (int ivx = 0; ivx < rm.rmp_pmmmMorphMap->mmp_aMorphMap.Count(); ivx++)
      {
        // Blend vertices and normals
        if (rm.rmp_pmmmMorphMap->mmp_bRelative)
        {
          // Blend relative (new = cur + f*(dst-src))
          const INDEX vtx = rm.rmp_pmmmMorphMap->mmp_aMorphMap[ivx].mwm_iVxIndex;
          const MeshVertex &mvSrc = mlod.mlod_aVertices[vtx];
          const MeshNormal &mnSrc = mlod.mlod_aNormals[vtx];
          const MeshVertexMorph &mvmDst = rm.rmp_pmmmMorphMap->mmp_aMorphMap[ivx];

          // Blend vertices
          _aMorphedVtxs[vtx].x += rm.rmp_fFactor * (mvmDst.mwm_x - mvSrc.x);
          _aMorphedVtxs[vtx].y += rm.rmp_fFactor * (mvmDst.mwm_y - mvSrc.y);
          _aMorphedVtxs[vtx].z += rm.rmp_fFactor * (mvmDst.mwm_z - mvSrc.z);

          // Blend normals
          _aMorphedNormals[vtx].nx += rm.rmp_fFactor * (mvmDst.mwm_nx - mnSrc.nx);
          _aMorphedNormals[vtx].ny += rm.rmp_fFactor * (mvmDst.mwm_ny - mnSrc.ny);
          _aMorphedNormals[vtx].nz += rm.rmp_fFactor * (mvmDst.mwm_nz - mnSrc.nz);
        } else {
          // Blend absolute (1-f)*cur + f*dst
          const INDEX vtx = rm.rmp_pmmmMorphMap->mmp_aMorphMap[ivx].mwm_iVxIndex;
          const MeshVertex &mvSrc = mlod.mlod_aVertices[vtx];
          const MeshVertexMorph &mvmDst = rm.rmp_pmmmMorphMap->mmp_aMorphMap[ivx];

          // Blend vertices
          _aMorphedVtxs[vtx].x = (1.0f - rm.rmp_fFactor) * _aMorphedVtxs[vtx].x + rm.rmp_fFactor * mvmDst.mwm_x;
          _aMorphedVtxs[vtx].y = (1.0f - rm.rmp_fFactor) * _aMorphedVtxs[vtx].y + rm.rmp_fFactor * mvmDst.mwm_y;
          _aMorphedVtxs[vtx].z = (1.0f - rm.rmp_fFactor) * _aMorphedVtxs[vtx].z + rm.rmp_fFactor * mvmDst.mwm_z;

          // Blend normals
          _aMorphedNormals[vtx].nx = (1.0f - rm.rmp_fFactor) * _aMorphedNormals[vtx].nx + rm.rmp_fFactor * mvmDst.mwm_nx;
          _aMorphedNormals[vtx].ny = (1.0f - rm.rmp_fFactor) * _aMorphedNormals[vtx].ny + rm.rmp_fFactor * mvmDst.mwm_ny;
          _aMorphedNormals[vtx].nz = (1.0f - rm.rmp_fFactor) * _aMorphedNormals[vtx].nz + rm.rmp_fFactor * mvmDst.mwm_nz;
        }
      }
    }
  }

  // If there is skeleton attached to this mesh transform all vertices
  if (sm_bTransformMesh && ctBones > 0 && ctRenWeights > 0)
  {
    #ifdef SE_BM_NEW_WEIGHTS
    if (rmsh.rmsh_pMeshInst->mi_pMesh->msh_iVersion >= 16) {
      PrepareMeshMatrices(rmsh, mlod);

      // assign final vertices and normals
      //_pavWeightedVertices = _aFinalVtxs.Push(_ctMeshVertices);
      //_panWeightedNormals  = _aFinalNormals.Push(_ctMeshVertices);
      _pavWeightedVertices = &_aFinalVtxs[0];
      _panWeightedNormals = &_aFinalNormals[0];

      const INDEX ctSurfaces = mlod.mlod_aSurfaces.Count();

      // for each surface
      for (INDEX imsrf = 0; imsrf < ctSurfaces; imsrf++) 
      {
        const MeshSurface &msrf = mlod.mlod_aSurfaces[imsrf];
        const SurfaceMatrices &sm = _aSurfacesMatrices[imsrf];
        const RenMatrix *prmFirst = &_aWeightMatrices[sm.sm_iFirstMatrix];
        
        ASSERT(sm.sm_ctMatrices == msrf.msrf_aubRelIndexTable.Count());
        const INDEX ctrm = sm.sm_ctMatrices;
        const INDEX ivxFirst = msrf.msrf_iFirstVertex;
        const INDEX ctvx = msrf.msrf_ctVertices;
        const INDEX iLastVertexAtSrf = ivxFirst + ctvx;
        const INDEX ctWeightsPerVertex = 4;//GetWeightsPerVertex(msrf,_fDistanceFactor);
        const BOOL bDynamicSurface = msrf.msrf_ulFlags & MS_DYNAMIC_SURFACE;

        MeshVertex *pmvSrcVertices;
        MeshNormal *pmnSrcNormals;
        MeshVertex *pmvDstVertices;
        MeshNormal *pmnDstNormals;
        
        // If this is dynamic surface use morphed vertices and normals.
        if(bDynamicSurface && bMeshUsesMorphs)  {
          pmvSrcVertices = _pavMorphedVertices;
          pmnSrcNormals  = _panMorphedNormals;
        // If no morphs active use mesh vertices and mormals).
        } else {
          pmvSrcVertices = _pavMeshVertices;
          pmnSrcNormals  = _panMeshNormals;
        }

        pmvDstVertices = _pavWeightedVertices;
        pmnDstNormals  = _panWeightedNormals;

        // must not write to source
        ASSERT(pmvDstVertices != _pavMeshVertices);
        ASSERT(pmnDstNormals != _panMeshNormals);
        
        
        // If using only 1 weight per vertex...
        if (ctWeightsPerVertex == 1) {

          // for all vertices in surface
          for (INDEX ivx = ivxFirst; ivx < iLastVertexAtSrf; ivx++) 
          {
            const MeshVertexWeightInfo &mvwi = _pawMeshWeights[ivx];
            const INDEX iBoneIndex = mvwi.mvwi_aubIndices[0];
            const Matrix12 &mTransform = prmFirst[iBoneIndex].rm_mTransform;
            const Matrix12 &mRotate    = prmFirst[iBoneIndex+ctrm].rm_mTransform;
            
            // Transform vertices
            TransformVector_copy((FLOAT3&)pmvDstVertices[ivx], (FLOAT3&)pmvSrcVertices[ivx], mTransform);
            // Rotate normals
            RotateVector_copy((FLOAT3&)pmnDstNormals[ivx], (FLOAT3&)pmnSrcNormals[ivx], mRotate);
          }

        // If using 2 weights per vertex...
        } else if (ctWeightsPerVertex == 2) {

          for (INDEX ivx = ivxFirst; ivx < iLastVertexAtSrf; ivx++) 
          {
            const MeshVertexWeightInfo &mvwi = _pawMeshWeights[ivx];
            const INDEX iBoneIndex1 = mvwi.mvwi_aubIndices[0];
            const INDEX iBoneIndex2 = mvwi.mvwi_aubIndices[1];
            const FLOAT fWeight1 = NormByteToFloat(mvwi.mvwi_aubWeights[0]);
            const FLOAT fWeight2 = 1.0f - fWeight1;
            const Matrix12 &mTransform1 = prmFirst[iBoneIndex1].rm_mTransform;
            const Matrix12 &mTransform2 = prmFirst[iBoneIndex2].rm_mTransform;
            const Matrix12 &mRotate1    = prmFirst[iBoneIndex1 + ctrm].rm_mTransform;
            const Matrix12 &mRotate2    = prmFirst[iBoneIndex2 + ctrm].rm_mTransform;
            MeshVertex vx;
            MeshNormal nx;

            // Transform vertices
            TransformVector_copy((FLOAT3&)vx,(FLOAT3&)pmvSrcVertices[ivx], mTransform1, fWeight1);
            TransformVector_add((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform2, fWeight2);
            pmvDstVertices[ivx] = vx;

            // Rotate normals
            RotateVector_copy((FLOAT3&)nx,(FLOAT3&)pmnSrcNormals[ivx], mRotate1, fWeight1);
            RotateVector_add((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate2, fWeight2);
            pmnDstNormals[ivx] = nx;
          }
        
        // If using 3 weights per vertex...
        } else if (ctWeightsPerVertex == 3) {

          for (INDEX ivx = ivxFirst; ivx < iLastVertexAtSrf; ivx++) 
          {
            const MeshVertexWeightInfo &mvwi = _pawMeshWeights[ivx];
            const INDEX iBoneIndex1 = mvwi.mvwi_aubIndices[0];
            const INDEX iBoneIndex2 = mvwi.mvwi_aubIndices[1];
            const INDEX iBoneIndex3 = mvwi.mvwi_aubIndices[2];
            const FLOAT fWeight1 = NormByteToFloat(mvwi.mvwi_aubWeights[0]);
            const FLOAT fWeight2 = NormByteToFloat(mvwi.mvwi_aubWeights[1]);
            const FLOAT fWeight3 = 1.0f - (fWeight1 + fWeight2);
            const Matrix12 &mTransform1 = prmFirst[iBoneIndex1].rm_mTransform;
            const Matrix12 &mTransform2 = prmFirst[iBoneIndex2].rm_mTransform;
            const Matrix12 &mTransform3 = prmFirst[iBoneIndex3].rm_mTransform;
            const Matrix12 &mRotate1    = prmFirst[iBoneIndex1 + ctrm].rm_mTransform;
            const Matrix12 &mRotate2    = prmFirst[iBoneIndex2 + ctrm].rm_mTransform;
            const Matrix12 &mRotate3    = prmFirst[iBoneIndex3 + ctrm].rm_mTransform;
            
            MeshVertex vx;
            MeshNormal nx;

            // Transform vertices
            TransformVector_copy((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform1, fWeight1);
            TransformVector_add((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform2, fWeight2);
            TransformVector_add((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform3, fWeight3);
            pmvDstVertices[ivx] = vx;

            // Rotate normals
            RotateVector_copy((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate1, fWeight1);
            RotateVector_add((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate2, fWeight2);
            RotateVector_add((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate3, fWeight3);
            pmnDstNormals[ivx] = nx;
          }
        
        // if using 4 weights per vertex...
        } else if (ctWeightsPerVertex == 4) {

          for (INDEX ivx = ivxFirst; ivx < iLastVertexAtSrf; ivx++) 
          {
            const MeshVertexWeightInfo &mvwi = _pawMeshWeights[ivx];
            const INDEX iBoneIndex1 = mvwi.mvwi_aubIndices[0];
            const INDEX iBoneIndex2 = mvwi.mvwi_aubIndices[1];
            const INDEX iBoneIndex3 = mvwi.mvwi_aubIndices[2];
            const INDEX iBoneIndex4 = mvwi.mvwi_aubIndices[3];
            const FLOAT fWeight1 = NormByteToFloat(mvwi.mvwi_aubWeights[0]);
            const FLOAT fWeight2 = NormByteToFloat(mvwi.mvwi_aubWeights[1]);
            const FLOAT fWeight3 = NormByteToFloat(mvwi.mvwi_aubWeights[2]);
            const FLOAT fWeight4 = 1.0f - (fWeight1 + fWeight2 + fWeight3);
            const Matrix12 &mTransform1 = prmFirst[iBoneIndex1].rm_mTransform;
            const Matrix12 &mTransform2 = prmFirst[iBoneIndex2].rm_mTransform;
            const Matrix12 &mTransform3 = prmFirst[iBoneIndex3].rm_mTransform;
            const Matrix12 &mTransform4 = prmFirst[iBoneIndex4].rm_mTransform;
            const Matrix12 &mRotate1    = prmFirst[iBoneIndex1 + ctrm].rm_mTransform;
            const Matrix12 &mRotate2    = prmFirst[iBoneIndex2 + ctrm].rm_mTransform;
            const Matrix12 &mRotate3    = prmFirst[iBoneIndex3 + ctrm].rm_mTransform;
            const Matrix12 &mRotate4    = prmFirst[iBoneIndex4 + ctrm].rm_mTransform;
            
            MeshVertex vx;
            MeshNormal nx;

            // Transform vertices
            TransformVector_copy((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform1, fWeight1);
            TransformVector_add((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform2, fWeight2);
            TransformVector_add((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform3, fWeight3);
            TransformVector_add((FLOAT3&)vx, (FLOAT3&)pmvSrcVertices[ivx], mTransform4, fWeight4);
            pmvDstVertices[ivx] = vx;

            // Rotate normals
            RotateVector_copy((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate1, fWeight1);
            RotateVector_add((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate2, fWeight2);
            RotateVector_add((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate3, fWeight3);
            RotateVector_add((FLOAT3&)nx, (FLOAT3&)pmnSrcNormals[ivx], mRotate4, fWeight4);
            pmnDstNormals[ivx] = nx;
          }

        }
      }

    } else
    #endif // SE_BM_NEW_WEIGHTS
    {

      MeshVertex *pSrcVertices = &_pavMeshVertices[0];
      MeshNormal *pSrcNormals = &_panMeshNormals[0];

      // If we have morph maps then use morphed vertices/normals.
      if (bMeshUsesMorphs) {
        pSrcVertices = &_pavMorphedVertices[0];
        pSrcNormals = &_panMorphedNormals[0];
      }

      // For each renweight
      for (int irw = rmsh.rmsh_iFirstWeight; irw < iLastRenWeight; irw++)
      {
        RenWeight &rw = _aRenWeights[irw];
        Matrix12 mTransform;
        Matrix12 mStrTransform;
        
        // If no bone for this weight 
        if (rw.rw_iBoneIndex == (-1)) {
          // Transform vertex using default model transform matrix (for boneless models)
          MatrixCopy(mStrTransform, _aRenModels[rmsh.rmsh_iRenModelIndex].rm_mStrTransform);
          MatrixCopy(mTransform,    _aRenModels[rmsh.rmsh_iRenModelIndex].rm_mTransform);
        } else {
          // Use bone transform matrix
          MatrixCopy(mStrTransform, _aRenBones[rw.rw_iBoneIndex].rb_mStrTransform);
          MatrixCopy(mTransform,    _aRenBones[rw.rw_iBoneIndex].rb_mTransform);
        }

        // If this is front face mesh remove rotation from transfrom matrix
        if (mlod.GetFlags() & ML_FULL_FACE_FORWARD) {
          RemoveRotationFromMatrix(mStrTransform);
        }

        // For each vertex in this weight
        INDEX ctvw = rw.rw_pwmWeightMap->mwm_aVertexWeight.Count();
        for (int ivw = 0; ivw < ctvw; ivw++)
        {
          MeshVertexWeight &vw = rw.rw_pwmWeightMap->mwm_aVertexWeight[ivw];
          INDEX ivx = vw.mww_iVertex;
          MeshVertex mv = pSrcVertices[ivx];
          MeshNormal mn = pSrcNormals[ivx];
          
          // Transform vertex and normal with this weight transform matrix
          TransformVector((FLOAT3&)mv, mStrTransform);
          RotateVector((FLOAT3&)mn, mTransform); // Don't stretch normals

          // Add new values to final vertices
          _aFinalVtxs[ivx].x += mv.x * vw.mww_fWeight;
          _aFinalVtxs[ivx].y += mv.y * vw.mww_fWeight;
          _aFinalVtxs[ivx].z += mv.z * vw.mww_fWeight;
          _aFinalNormals[ivx].nx += mn.nx * vw.mww_fWeight;
          _aFinalNormals[ivx].ny += mn.ny * vw.mww_fWeight;
          _aFinalNormals[ivx].nz += mn.nz * vw.mww_fWeight;
        }
      }

      _pavWeightedVertices = &_aFinalVtxs[0];
      _panWeightedNormals  = &_aFinalNormals[0];
    }
    
    // Mesh is in view space so transform light to view space
    RotateVector(_vLightDirInView.vector,_mObjToView);
    
    // Set flag that mesh is in view space
    rmsh.rmsh_bTransToViewSpace = TRUE;
    
    // Reset view matrix bacause model is allready transformed in view space
    gfxSetViewMatrix(NULL);
    
  // If no skeleton
  } else {
    MeshVertex *pSrcVertices = &_pavMeshVertices[0];
    MeshNormal *pSrcNormals = &_panMeshNormals[0];

    // If we have morph maps then use morphed vertices/normals.
    if (bMeshUsesMorphs) {
      pSrcVertices = &_pavMorphedVertices[0];
      pSrcNormals = &_panMorphedNormals[0];
    }

    // If flag is set to transform all vertices to view space
    if (_bTransformBonelessModelToViewSpace)
    {
      RenModel &rm = _aRenModels[rmsh.rmsh_iRenModelIndex];

      // Transform every vertex using default model transform matrix (for boneless models)
      Matrix12 mTransform;
      Matrix12 mStrTransform;
      MatrixCopy(mTransform,    rm.rm_mTransform);
      MatrixCopy(mStrTransform, rm.rm_mStrTransform);

      // If this is front face mesh remove rotation from transfrom matrix
      if (mlod.GetFlags() & ML_FULL_FACE_FORWARD) {
        RemoveRotationFromMatrix(mStrTransform);
      }
      
      // For each vertex
      for (int ivx = 0; ivx < ctVertices; ivx++)
      {
        TransformVector_copy((FLOAT3&)_aFinalVtxs[ivx], (FLOAT3&)pSrcVertices[ivx], mStrTransform);
        RotateVector_copy((FLOAT3&)_aFinalNormals[ivx], (FLOAT3&)pSrcNormals[ivx], mTransform);
      }
  
      _pavWeightedVertices = &_aFinalVtxs[0];
      _panWeightedNormals  = &_aFinalNormals[0];
      
      // Mesh is in view space so transform light to view space
      RotateVector(_vLightDirInView.vector, _mObjToView);
      
      // Set flag that mesh is in view space
      rmsh.rmsh_bTransToViewSpace = TRUE;
      
      // Reset view matrix bacause model is allready transformed in view space
      gfxSetViewMatrix(NULL);
      
    // Leave vertices in obj space
    } else {
      RenModel &rm = _aRenModels[rmsh.rmsh_iRenModelIndex];

      _pavWeightedVertices = _pavMeshVertices;
      _panWeightedNormals = _panMeshNormals;

      #pragma message(">> Fix face forward meshes, when objects are left in object space")

      SetViewMatrix(rm.rm_mStrTransform);

      RenBone &rb = _aRenBones[rm.rm_iParentBoneIndex];
      RotateVector(_vLightDirInView.vector, rb.rb_mBonePlacement);
      
      // Mark this mesh as in object space
      rmsh.rmsh_bTransToViewSpace = FALSE;
    }
  }
}

// Render one ren model
static void RenderModel_View(RenModel &rm)
{
  ASSERT(_iRenderingType == 1);
  const BOOL bShowNormals = RM_GetFlags() & RMF_SHOWNORMALS;
  const BOOL bRenderFog = RM_GetRenderFlags() & SRMF_FOG;
  const BOOL bRenderHaze = RM_GetRenderFlags() & SRMF_HAZE;
  
  // prepare shader flags
  ULONG ulShaderRenFlags = 0;

  // Extensive check to toggle fog/haze.
  if (bRenderFog || bRenderHaze) 
  {
    // Init fog and haze shared params by all surfaces
    shaInitSharedFogAndHazeParams(bRenderFog, bRenderHaze, _aprProjection, _mObjToView, _mObjectToAbs);

    // check bounding box of model against fog
    if (bRenderFog && shaHasFog(_bbRootAllFrames)) {
      // this model has no fog after all
      ulShaderRenFlags |= SHA_RMF_FOG;
    }

    // check bounding box of model against haze
    if (bRenderHaze && shaHasHaze(_bbRootAllFrames)) {
      // this model has no haze after all
      ulShaderRenFlags |= SHA_RMF_HAZE;
    }
  }

  // Reset texture wrapping before rendering the model
  gfxSetTextureWrapping(GFX_REPEAT, GFX_REPEAT);

  // For each mesh in renmodel
  INDEX ctmsh = rm.rm_iFirstMesh + rm.rm_ctMeshes;
  for (int imsh = rm.rm_iFirstMesh; imsh < ctmsh; imsh++)
  {
    RenMesh &rmsh = _aRenMesh[imsh];
    
    // Prepare mesh for rendering
    PrepareMeshForRendering(rmsh,rm.rm_iSkeletonLODIndex);
    
    // Render mesh
    RenderMesh(rmsh, rm, ulShaderRenFlags);
    
    // Show normals in required
    if (bShowNormals) {
      RenderNormals();
    }
  }
}

// Render one ren model to shadowmap
static void RenderModel_Mask(RenModel &rm)
{
  ASSERT(_iRenderingType == 2);
  
  // Flag to transform all vertices in view space
  const BOOL bTemp = _bTransformBonelessModelToViewSpace;
  _bTransformBonelessModelToViewSpace = TRUE;
  RM_SetCurrentDistance(0);

  INDEX ctmsh = rm.rm_iFirstMesh + rm.rm_ctMeshes;
  
  // For each mesh in renmodel
  for (int imsh = rm.rm_iFirstMesh; imsh < ctmsh; imsh++)
  {
    // Render mesh
    RenMesh &rmsh = _aRenMesh[imsh];
    PrepareMeshForRendering(rmsh, rm.rm_iSkeletonLODIndex);
    RenderMesh(rmsh, rm, 0);
  }

  // Done
  _bTransformBonelessModelToViewSpace = bTemp;
}

// Get bone abs position
BOOL RM_GetRenBoneAbs(CModelInstance &mi, INDEX iBoneID, RenBone &rb)
{
  // Do not transform to view space
  MakeIdentityMatrix(_mAbsToViewer);
  CalculateRenderingData(mi);
  INDEX ctrb = _aRenBones.Count();
  
  // For each render bone after dummy one
  for (INDEX irb = 1; irb < ctrb; irb++)
  {
    RenBone &rbone = _aRenBones[irb];
    
    // Check if this is serched bone
    if (rbone.rb_psbBone->sb_iID == iBoneID) {
      rb = rbone;
      ClearRenArrays();
      return TRUE;
    }
  }
  
  // Clear ren arrays
  ClearRenArrays();
  return FALSE;
}

// Returns true if bone exists and sets two given vectors as start and end point of specified bone
BOOL RM_GetBoneAbsPosition(CModelInstance &mi,INDEX iBoneID, FLOAT3D &vStartPoint, FLOAT3D &vEndPoint)
{
  // Do not transform to view space
  MakeIdentityMatrix(_mAbsToViewer);
  
  // Use higher lod for bone finding
  RM_SetCurrentDistance(0);
  CalculateRenderingData(mi);
  INDEX ctrb = _aRenBones.Count();
  
  // For each render bone after dummy one
  for (INDEX irb = 1; irb < ctrb; irb++)
  {
    RenBone &rb = _aRenBones[irb];
    // Check if this is serched bone
    if (rb.rb_psbBone->sb_iID == iBoneID)
    {
      vStartPoint = FLOAT3D(0, 0, 0);
      vEndPoint   = FLOAT3D(0, 0, rb.rb_psbBone->sb_fBoneLength);
      TransformVector(vStartPoint.vector,rb.rb_mBonePlacement);
      TransformVector(vEndPoint.vector,rb.rb_mBonePlacement);
      ClearRenArrays();
      return TRUE;
    }
  }
  
  // Clear ren arrays
  ClearRenArrays();
  return FALSE;
}

// Calculate complete rendering data for model instance
static void CalculateRenderingData(CModelInstance &mi)
{
  RM_SetObjectMatrices(mi);

  // remeber all frames bbox 
  mi.GetAllFramesBBox(_bbRootAllFrames);
  
  // Distance to model is z param in objtoview matrix 
  _fDistanceFactor = -_mObjToView[11];

  // Create first dummy model that serves as parent for the entire hierarchy
  MakeRootModel();
  
  // Build entire hierarchy with children
  BuildHierarchy(&mi, 0);

  INDEX ctrm = _aRenModels.Count();
  
  // For each renmodel 
  for (int irm = 1; irm < ctrm; irm++)
  {
    // Match model animations
    MatchAnims(_aRenModels[irm]);
  }
  
  // Calculate transformations for all bones on already built hierarchy
  CalculateBoneTransforms();
}

// Render one SKA model with its children
void RM_RenderSKA(CModelInstance &mi)
{
  // Calculate all rendering data for this model instance
  //if (_iRenderingType==2) CalculateRenderingData(mi, 0);
  //else 
  CalculateRenderingData(mi);

  // For each renmodel
  INDEX ctrmsh = _aRenModels.Count();
  for (int irmsh = 1; irmsh < ctrmsh; irmsh++)
  {
    RenModel &rm = _aRenModels[irmsh];
    
    // Set object matrices
    RM_SetObjectMatrices(*rm.rm_pmiModel);
    
    // Render this model
    if (_iRenderingType == 1) {
      RenderModel_View(rm);
    } else {
      RenderModel_Mask(rm);
    }
  }
  
  // Done if cluster shadows were rendered
  if (_iRenderingType == 2) {
    // reset arrays
    ClearRenArrays();
    return;
  }

  // No cluster shadows - see if anything else needs to be rendered
  ASSERT(_iRenderingType == 1);

  // If render wireframe is requested
  if (RM_GetFlags() & RMF_WIREFRAME)
  {
    gfxDisableTexture();
    
    // Set polygon offset
    gfxPolygonMode(GFX_LINE);
    gfxEnableDepthBias();

    // For each ren model 
    INDEX ctrmsh = _aRenModels.Count();
    for (int irmsh = 1; irmsh < ctrmsh; irmsh++)
    {
      RenModel &rm = _aRenModels[irmsh];
      
      // Render renmodel in wireframe
      RenderModelWireframe(rm);
    }

    // Restore polygon offset
    gfxDisableDepthBias();
    gfxPolygonMode(GFX_FILL);
  }

  extern INDEX sm_bShowCollision;
  extern INDEX sm_bShowSkeleton;

  // Show skeleton
  if (sm_bShowSkeleton || RM_GetFlags() & RMF_SHOWSKELETON) {
    gfxDisableTexture();
    gfxDisableDepthTest();
    
    // Render skeleton
    RenderSkeleton();
    gfxEnableDepthTest();
  }
  
  #pragma message(">> Add sm_bShowActiveBones")
  if (/*sm_bShowActiveBones || */ RM_GetFlags() & RMF_SHOWACTIVEBONES) {
    gfxDisableTexture();
    gfxDisableDepthTest();
    
    // Render only active bones
    RenderActiveBones();
    gfxEnableDepthTest();
  }

  // Show root model instance collision box
  if (sm_bShowCollision)
  {
    RM_SetObjectMatrices(mi);
    if (mi.mi_cbAABox.Count() > 0) {
      CollisionBox &cb = mi.GetCurrentCollisionBox();
      RM_RenderCollisionBox(mi, cb, C_mlGREEN);
    }
  }

  // Reset arrays
  ClearRenArrays();
}

// Clear all ren arrays
static void ClearRenArrays()
{
  _pAdjustBonesCallback = NULL;
  _pAdjustBonesData = NULL;
  _pAdjustShaderParams = NULL;
  _pAdjustShaderData = NULL;

  // Clear all arrays
  _aRenModels.PopAll();
  _aRenBones.PopAll();
  _aRenMesh.PopAll();
  _aRenWeights.PopAll();
  _aRenMorphs.PopAll();
  _fCustomMlodDistance = -1;
  _fCustomSlodDistance = -1;
}