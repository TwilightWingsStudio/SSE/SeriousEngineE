/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#pragma once

// set this to 1 to enable checks whether somethig is deleted while iterating some array/container
#define CHECKARRAYLOCKING 0

#ifdef _WIN32
  #ifndef PLATFORM_WIN32
    #define PLATFORM_WIN32 1
  #endif
#endif

#include <stdlib.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include <math.h>
#include <search.h>   // for qsort
#include <float.h>    // for FPU control

/* rcg10042001 !!! FIXME: Move these somewhere. */
#if (defined PLATFORM_WIN32)
  #include <conio.h>
  #include <crtdbg.h>
  #include <winsock2.h>
  #include <windows.h>
  #include <mmsystem.h> // for timers
#endif

#include <Engine/EngineApi.h>
#include <Engine/Base/Types.h>

#include <Engine/Input/Input.h>
#include <Engine/Input/KeyNames.h>
#include <Core/Base/Updateable.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/ErrorTable.h>
#include <Engine/Resources/ReplaceFile.h>
#include <Core/IO/Stream.h>
#include <Core/Base/Lists.h>
#include <Core/Base/Timer.h>
#include <Core/Base/ListIterator.inl>
#include <Core/Base/Console.h>
#include <Core/Base/Console_internal.h>
#include <Core/Base/Shell_internal.h>
#include <Core/Base/Shell.h>
#include <Core/Base/Statistics.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/WinRegistry.h>
#include <Engine/Input/IFeel.h>

#include <Engine/Base/ProgressHook.h>

#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityCollision.h>
#include <Engine/Entities/EntityProperties.h>
#include <Engine/Entities/Entity.h>
#include <Engine/Entities/InternalClasses.h>
#include <Engine/Entities/LastPositions.h>
#include <Engine/Entities/EntityCollision.h>
#include <Engine/Entities/ShadingInfo.h>
#include <Engine/Entities/FieldSettings.h>
#include <Engine/Entities/Precaching.h>

#include <Engine/Light/LightSource.h>
#include <Engine/Light/LensFlares.h>
#include <Engine/Light/Shadows_internal.h>
#include <Engine/Light/Gradient.h>

#include <Core/Math/Geometry.inl>
#include <Core/Math/Clipping.inl>
#include <Core/Math/FixInt.h>
#include <Core/Math/Float.h>
#include <Core/Math/Object3D.h>
#include <Core/Math/Functions.h>
#include <Core/Math/Quaternion.h>
#include <Core/Math/Projection.h>
#include <Core/Math/Projection_DOUBLE.h>

#include <Engine/Network/Network.h>
#include <Engine/Network/Server.h>
#include <Engine/Network/NetworkMessage.h>
#include <Engine/Network/PlayerSource.h>
#include <Engine/Network/PlayerBuffer.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/SessionState.h>
#include <Engine/Network/NetworkProfile.h>

#include <Engine/Brushes/Brush.h>
#include <Engine/Brushes/BrushTransformed.h>
#include <Engine/Brushes/BrushArchive.h>

#include <Engine/Terrain/Terrain.h>

#include <Engine/World/World.h>
#include <Engine/World/WorldEditingProfile.h>
#include <Engine/World/WorldRayCasting.h>
#include <Engine/World/PhysicsProfile.h>
#include <Engine/World/WorldSettings.h>
#include <Engine/World/WorldCollision.h>

#include <Engine/Rendering/Render.h>
#include <Engine/Rendering/Render_internal.h>

#include <Engine/Models/ModelObject.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Models/EditModel.h>
#include <Engine/Models/RenderModel.h>

#include <Engine/Meshes/ModelInstance.h>
#include <Engine/Meshes/SkelMesh.h>
#include <Engine/Meshes/Skeleton.h>
#include <Engine/Meshes/BoneAnimSet.h>
#include <Engine/Meshes/SM_StringTable.h>
#include <Engine/Meshes/SM_Render.h>

#include <Engine/Sound/SoundObject.h>
#include <Engine/Sound/SoundLibrary.h>
#include <Engine/Sound/SoundListener.h>

#include <Engine/Graphics/Texture.h>
#include <Core/Graphics/Color.h>
#include <Core/Graphics/Image.h>
#include <Core/Graphics/ImageManager.h>
#include <Engine/Graphics/Font.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/ViewPort.h>
#include <Engine/Graphics/DrawPort.h>
#include <Engine/Graphics/ImageInfo.h>
#include <Engine/Graphics/RenderScene.h>
#include <Engine/Graphics/RenderPoly.h>
#include <Engine/Graphics/Fog.h>
#include <Engine/Graphics/Stereo.h>

#include <Core/Templates/BSP.h>
#include <Core/Templates/BSP_internal.h>
#include <Core/Templates/DynamicStackArray.h>
#include <Core/Templates/DynamicStackArray.cpp>
#include <Core/Templates/LinearAllocator.h>
#include <Core/Templates/LinearAllocator.cpp>
#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.h>
#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/StaticArray.h>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/StaticStackArray.h>
#include <Core/Templates/StaticStackArray.cpp>
#include <Core/Templates/Selection.h>
#include <Core/Templates/Selection.cpp>


// some global stuff
ENGINE_API void SE_InitEngine( CTString strGameID);
ENGINE_API void SE_EndEngine(void);
ENGINE_API void SE_LoadDefaultFonts(void);
ENGINE_API void SE_UpdateWindowHandle( HWND hwndWindowed);
ENGINE_API void SE_PretouchIfNeeded(void);

extern ENGINE_API CTString _strEngineBuild;  // not valid before InitEngine()!
extern ENGINE_API ULONG _ulEngineBuildMajor;
extern ENGINE_API ULONG _ulEngineBuildMinor;

extern ENGINE_API CTString _strLogFile;

// temporary vars for adjustments
ENGINE_API extern FLOAT tmp_af[10];
ENGINE_API extern INDEX tmp_ai[10];
ENGINE_API extern INDEX tmp_i;
ENGINE_API extern INDEX tmp_fAdd;
