/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/Texture.h>

#include <Core/IO/Stream.h>
#include <Core/Base/Timer.h>
#include <Core/Base/Console.h>
#include <Core/Math/Functions.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/ImageInfo.h>
#include <Engine/Graphics/TextureEffects.h>

#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Resources/ResourceManager.h>

#include <Core/Base/Statistics_Internal.h>


extern INDEX tex_iNormalQuality;
extern INDEX tex_iAnimationQuality;
extern INDEX tex_iNormalSize;
extern INDEX tex_iAnimationSize;
extern INDEX tex_iEffectSize;

extern INDEX gap_bAllowSingleMipmap;
extern FLOAT gfx_tmProbeDecay;

// Special mode flag when loading texture for exporting
extern BOOL _bTextureExportMode = FALSE;

// Singleton object for texture settings
struct TextureSettings TS = {0};

#define TEXFMT_NONE 0

// Determine (or assume) support for OpenGL and Direct3D texture internal formats
extern void DetermineSupportedTextureFormats(GfxAPIType eAPI)
{
  if (eAPI == GAT_OGL) {
    TS.ts_tfRGB8   = GL_RGB8;
    TS.ts_tfRGBA8  = GL_RGBA8;
    TS.ts_tfRGB5   = GL_RGB5;
    TS.ts_tfRGBA4  = GL_RGBA4;
    TS.ts_tfRGB5A1 = GL_RGB5_A1;
    TS.ts_tfLA8    = GL_LUMINANCE8_ALPHA8;
    TS.ts_tfL8     = GL_LUMINANCE8;
  }
 
#ifdef SE1_D3D8
  if (eAPI == GAT_D3D8) {
    extern D3DFORMAT FindClosestFormat_D3D(D3DFORMAT d3df);
    TS.ts_tfRGBA8  = FindClosestFormat_D3D(D3DFMT_A8R8G8B8);
    TS.ts_tfRGB8   = FindClosestFormat_D3D(D3DFMT_X8R8G8B8);
    TS.ts_tfRGB5   = FindClosestFormat_D3D(D3DFMT_R5G6B5);
    TS.ts_tfRGB5A1 = FindClosestFormat_D3D(D3DFMT_A1R5G5B5);
    TS.ts_tfRGBA4  = FindClosestFormat_D3D(D3DFMT_A4R4G4B4);
    TS.ts_tfLA8    = FindClosestFormat_D3D(D3DFMT_A8L8);
    TS.ts_tfL8     = FindClosestFormat_D3D(D3DFMT_L8);
  }
#endif // SE1_D3D8
}

// Update all relevant texture parameters
extern void UpdateTextureSettings(void)
{
  // Determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Set texture formats and compression
  TS.ts_tfRGB8 = TS.ts_tfRGBA8 = NONE;
  TS.ts_tfRGB5 = TS.ts_tfRGBA4 = TS.ts_tfRGB5A1 = NONE;
  TS.ts_tfLA8  = TS.ts_tfL8 = NONE;
  DetermineSupportedTextureFormats(eAPI);

  // Clamp and adjust texture compression type
  INDEX iTCType = 0;
  const ULONG ulGfxFlags = _pGfx->gl_ulFlags;
  const BOOL bHasTC = (ulGfxFlags&GLF_TEXTURECOMPRESSION); 
  if (eAPI == GAT_OGL && bHasTC)
  {
    // OpenGL
    extern INDEX ogl_iTextureCompressionType;  // 0=none, 1=default (ARB), 2=S3TC, 3=FXT1, 4=legacy S3TC
    INDEX &iTC = ogl_iTextureCompressionType;
    iTC = Clamp(iTC, 0L, 4L);
    if (iTC == 3 && !(ulGfxFlags & GLF_EXTC_FXT1)) {
      iTC = 2;
    }
    
    if (iTC == 2 && !(ulGfxFlags & GLF_EXTC_S3TC)) {
      iTC = 3;
    }
    
    if ((iTC == 2 || iTC == 3) && !((ulGfxFlags & GLF_EXTC_FXT1) || (ulGfxFlags & GLF_EXTC_S3TC))) {
      iTC = 1;
    }
    
    if (iTC == 1 && !(ulGfxFlags & GLF_EXTC_ARB)) {
      iTC = 4;
    }
    
    if (iTC == 4 && !(ulGfxFlags & GLF_EXTC_LEGACY)) {
      iTC = 0; // khm ... 
    }
    
    iTCType = iTC;   // set it
  }
  
  // Direct3D (just force DXTC - it's the only one)
#ifdef SE1_D3D8
  if (eAPI == GAT_D3D8 && bHasTC) {
    iTCType = 5;
  }
#endif // SE1_D3D8

  // Clamp and cache cvar
  extern INDEX tex_bCompressAlphaChannel; 
  if (tex_bCompressAlphaChannel) {
    tex_bCompressAlphaChannel = 1; 
  }
  const BOOL bCAC = tex_bCompressAlphaChannel;  

  // Set members
  switch (iTCType)
  {
    case 1:  // ARB
      TS.ts_tfCRGBA = GL_COMPRESSED_RGBA_ARB;
      TS.ts_tfCRGB  = GL_COMPRESSED_RGB_ARB;
      break;  

    case 2:  // S3TC
      TS.ts_tfCRGBA = bCAC ? GL_COMPRESSED_RGBA_S3TC_DXT5_EXT : GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
      TS.ts_tfCRGB  = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
      break; 

  #ifdef SE1_3DFX
    case 3:  // FXT1
      TS.ts_tfCRGBA = GL_COMPRESSED_RGBA_FXT1_3DFX;
      TS.ts_tfCRGB  = GL_COMPRESSED_RGB_FXT1_3DFX;
      break;
  #endif // SE1_3DFX

    case 4:  // LEGACY
      TS.ts_tfCRGBA = bCAC ? GL_COMPRESSED_RGB4_COMPRESSED_ALPHA4_S3TC : GL_COMPRESSED_RGBA_S3TC;
      TS.ts_tfCRGB  = GL_COMPRESSED_RGB_S3TC;
      break;

  #ifdef SE1_D3D8
    case 5:  // DXTC
      extern D3DFORMAT FindClosestFormat_D3D(D3DFORMAT d3df);
      TS.ts_tfCRGBA = bCAC ? FindClosestFormat_D3D(D3DFMT_DXT5) : FindClosestFormat_D3D(D3DFMT_DXT3);
      TS.ts_tfCRGB  = D3DFMT_DXT1;
      break;
  #endif // SE1_D3D8

    default: // none
      TS.ts_tfCRGBA = NONE;
      TS.ts_tfCRGB  = NONE;
      break;
  }
  // Adjust if need to compress opaque textures as transparent
  extern INDEX tex_bAlternateCompression; 
  if (tex_bAlternateCompression) {
    tex_bAlternateCompression = 1; 
    TS.ts_tfCRGB = TS.ts_tfCRGBA;
  }

  // Clamp texture quality
  INDEX iMinQuality = 0;
  INDEX iMaxQuality = iTCType > 0 ? 3 : 2;
  if (!(_pGfx->gl_ulFlags & GLF_32BITTEXTURES)) {
    iMinQuality = iMaxQuality = 1;
  }
  TS.ts_iNormQualityO  = Clamp((INDEX)(tex_iNormalQuality / 10), iMinQuality, iMaxQuality); 
  TS.ts_iNormQualityA  = Clamp((INDEX)(tex_iNormalQuality % 10), iMinQuality, iMaxQuality); 
  TS.ts_iAnimQualityO  = Clamp((INDEX)(tex_iAnimationQuality / 10), iMinQuality, iMaxQuality); 
  TS.ts_iAnimQualityA  = Clamp((INDEX)(tex_iAnimationQuality % 10), iMinQuality, iMaxQuality); 
  tex_iNormalQuality    = TS.ts_iNormQualityO * 10 + TS.ts_iNormQualityA;
  tex_iAnimationQuality = TS.ts_iAnimQualityO * 10 + TS.ts_iAnimQualityA;
  
  // Clamp texture size
  tex_iNormalSize    = Clamp(tex_iNormalSize, 5L, 11L);
  tex_iAnimationSize = Clamp(tex_iAnimationSize, 5L, 9L);
  TS.ts_pixNormSize = 1L << (tex_iNormalSize * 2);
  TS.ts_pixAnimSize = 1L << (tex_iAnimationSize * 2);

  // Determine maximum texel-byte ratio
  INDEX iOQ = tex_iNormalQuality / 10;
  if (iOQ == 0) {
    iOQ = 2; 
  } else if (iOQ == 3) {
    iOQ = 0;
  }
  
  INDEX iAQ = tex_iNormalQuality % 10;
  if (iAQ == 0) {
   iAQ = 2; 
  } else if (iAQ == 3) {
    iAQ = 0;
  }
  
  INDEX iTexMul = 2 * Max(iOQ,iAQ);
  if (iTexMul == 0) {
    iTexMul = 1;
  }
  
  if (tex_iNormalSize <= 6 && iTexMul == 1) {
    iTexMul = 2;
  }
  
  if (tex_iNormalSize <= 5 && iTexMul == 2) {
    iTexMul = 4;
  }
  
  TS.ts_iMaxBytesPerTexel = iTexMul;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation of CTextureData routines

// Constructor.
CTextureData::CTextureData()
{
  td_ulFlags = NONE;
  td_mexWidth  = 0;
  td_mexHeight = 0;
  td_tvLastDrawn = 0I64;
  td_iFirstMipLevel  = 0;
  td_ctFineMipLevels = 0;

  td_ctFrames    = 0;
  td_slFrameSize = 0;
  td_ulInternalFormat = TEXFMT_NONE;
  td_ulProbeObject = NONE;
  td_pulObjects = NULL;
  td_ulObject = NONE;
  td_pulFrames = NULL;

  td_pubBuffer1      = NULL; // reset effect buffers
  td_pubBuffer2      = NULL;
  td_pixBufferWidth  = 0;
  td_pixBufferHeight = 0;
  td_ptdBaseTexture  = NULL; // no base texture by default
  td_ptegEffect      = NULL; // no effect data

  td_iRenderFrame = -1;
  CAnimData::DefaultAnimation();
  _bTextureExportMode = FALSE;
}

// Destructor.
CTextureData::~CTextureData()
{
  Clear();
}

// Converts mip level to the one of allowed by texture
INDEX CTextureData::ClampMipLevel(FLOAT fMipFactor) const
{
  INDEX res = (INDEX)fMipFactor;
  INDEX iLastMipLevel = GetNoOfMipmaps(GetPixWidth(), GetPixHeight()) - 1 + td_iFirstMipLevel;
  res = Clamp(res, td_iFirstMipLevel, iLastMipLevel);
  return(res);
}

// This promotes 16bit internal format to corresponding 32bit
static ULONG PromoteTo32bitFormat(ULONG ulFormat)
{
  if (ulFormat == TS.ts_tfRGB5) {
    return TS.ts_tfRGB8;
  }
  
  if (ulFormat == TS.ts_tfRGBA4 || ulFormat == TS.ts_tfRGB5A1) {
    return TS.ts_tfRGBA8;
  }
  return ulFormat;
}

// Force texture to be re-loaded (if needed) in corresponding manner
void CTextureData::Force(ULONG ulTexFlags) 
{
  ASSERT(td_ctFrames > 0);
  const BOOL bReload = (td_pulFrames == NULL && (ulTexFlags&TEX_STATIC)) ||
                       ((td_ulFlags & TEX_DISPOSED) && (ulTexFlags & TEX_CONSTANT)) ||
                       ((td_ulFlags & TEX_SATURATED) && (ulTexFlags & TEX_KEEPCOLOR));
                       
  td_ulFlags |= ulTexFlags & (TEX_CONSTANT | TEX_STATIC | TEX_KEEPCOLOR);
  if (bReload) {
    Reload();
  }
}

// Set texture to be as current for accelerator and eventually upload it to accelerator's memory
void CTextureData::SetAsCurrent(INDEX iFrameNo/*=0*/, BOOL bForceUpload/*=FALSE*/)
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  ASSERT(iFrameNo < td_ctFrames);
  BOOL bNeedUpload = bForceUpload;
  BOOL bNoDiscard  = TRUE;
  PIX  pixWidth  = GetPixWidth();
  PIX  pixHeight = GetPixHeight();

  // eventually re-adjust LOD bias
  extern FLOAT _fCurrentLODBias;
  const FLOAT fWantedLODBias = _pGfx->gl_fTextureLODBias;
  extern void UpdateLODBias(const FLOAT fLODBias);
  if (td_ulFlags&TEX_CONSTANT)
  {
    // Тon-adjustable textures don't tolerate positive LOD bias
    if (_fCurrentLODBias > 0) {
      UpdateLODBias(0);
    } else if (_fCurrentLODBias > fWantedLODBias) {
      UpdateLODBias(fWantedLODBias);
    }
  } else if (td_ulFlags & TEX_EQUALIZED) {
    // Equilized textures don't tolerate negative LOD bias
    if (_fCurrentLODBias < 0) {
      UpdateLODBias(0);
    } else if (_fCurrentLODBias<fWantedLODBias) {
      UpdateLODBias(fWantedLODBias);
    }
  } else if (_fCurrentLODBias != fWantedLODBias) {
    // All other textures must take LOD bias into account
    UpdateLODBias(fWantedLODBias);
  }

  // Determine probing
  extern BOOL ProbeMode(CTimerValue tvLast);
  BOOL bUseProbe = ProbeMode(td_tvLastDrawn);

  // If we have an effect texture
  if (td_ptegEffect != NULL)
  { 
    ASSERT(iFrameNo == 0); // effect texture must have only one frame
    
    // Get max allowed effect texture dimension
    PIX pixClampAreaSize = 1L << 16L;
    tex_iEffectSize = Clamp(tex_iEffectSize, 4L, 8L);
    if (!(td_ulFlags & TEX_CONSTANT)) {
      pixClampAreaSize = 1L << (tex_iEffectSize * 2);
    }
    INDEX iWantedMipLevel = td_iFirstMipLevel
                          + ClampTextureSize(pixClampAreaSize, _pGfx->gl_pixMaxTextureDimension, pixWidth, pixHeight);
                          
    // Check whether wanted mip level is beyond last mip-level
    iWantedMipLevel = ClampMipLevel(iWantedMipLevel);

    // Default adjustment for mapping
    pixWidth  >>= iWantedMipLevel-td_iFirstMipLevel;
    pixHeight >>= iWantedMipLevel-td_iFirstMipLevel;
    ASSERT(pixWidth > 0 && pixHeight > 0);

    // Eventually adjust water effect texture size (if larger than base)
    if (td_ptegEffect->IsWater())
    {
      INDEX iMipDiff = Min(FastLog2(td_ptdBaseTexture->GetPixWidth()) - FastLog2(pixWidth),
                           FastLog2(td_ptdBaseTexture->GetPixHeight()) - FastLog2(pixHeight));
      iWantedMipLevel = iMipDiff;
      if (iMipDiff < 0) {
        pixWidth  >>= (-iMipDiff);
        pixHeight >>= (-iMipDiff);
        iWantedMipLevel = 0;
        ASSERT(pixWidth > 0 && pixHeight > 0);
      }
    }
    
    // If current frame size differs from the previous one
    SLONG slFrameSize = GetMipmapOffset(15, pixWidth, pixHeight) *BYTES_PER_TEXEL;
    if (td_pulFrames == NULL || td_slFrameSize != slFrameSize)
    {
      // (re)allocate the frame buffer
      if (td_pulFrames != NULL) {
        FreeMemory(td_pulFrames);
      }
      td_pulFrames = (ULONG*)AllocMemory(slFrameSize);
      td_slFrameSize = slFrameSize;
      bNoDiscard = FALSE;
    }

    // If not calculated for this tick (must be != to test for time rewinding)
    if (td_ptegEffect->teg_updTexture.LastUpdateTime() != _pTimer->CurrentTick()) {
      // Discard eventual cached frame and calculate new frame
      MarkChanged();
      td_ptegEffect->Animate();
      bNeedUpload = TRUE;
      
      // Make sure that effect and base textures are static
      Force(TEX_STATIC);
      td_ptdBaseTexture->Force(TEX_STATIC);
      
      // Copy some flags from base texture to effect texture
      td_ulFlags |= td_ptdBaseTexture->td_ulFlags & (TEX_ALPHACHANNEL | TEX_TRANSPARENT | TEX_GRAY);
      
      // Render effect texture
      td_ptegEffect->Render(iWantedMipLevel, pixWidth, pixHeight);
      
      // Determine internal format
      ULONG ulNewFormat;
      if (td_ulFlags & TEX_GRAY)
      {
        if (td_ulFlags & TEX_ALPHACHANNEL) {
          ulNewFormat = TS.ts_tfLA8;
        } else {
          ulNewFormat = TS.ts_tfL8;
        }
      } else {
        if (td_ulFlags & TEX_TRANSPARENT) {
          ulNewFormat = TS.ts_tfRGB5A1;
        } else if (td_ulFlags & TEX_ALPHACHANNEL) {
          ulNewFormat = TS.ts_tfRGBA4;
        } else {
          ulNewFormat = TS.ts_tfRGB5;
        }
      }
      
      // Effect texture can be in 32-bit quality only if base texture hasn't been dithered
      extern INDEX tex_bFineEffect;      
      if (tex_bFineEffect && (td_ptdBaseTexture->td_ulFlags & TEX_DITHERED)) {
        ulNewFormat = PromoteTo32bitFormat(ulNewFormat);
      }
      
      // Internal format changed? - must discard!
      if (td_ulInternalFormat != ulNewFormat) {
        td_ulInternalFormat = ulNewFormat;
        bNoDiscard = FALSE;
      }
    }
    
    // Effect texture cannot have probing
    bUseProbe = FALSE;
  }

  // Prepare effect cvars
  extern INDEX tex_bDynamicMipmaps;
  extern INDEX tex_iEffectFiltering; 
  if (tex_bDynamicMipmaps) {
    tex_bDynamicMipmaps = 1;
  }
  tex_iEffectFiltering = Clamp(tex_iEffectFiltering, -6L, +6L);

  // Determine whether texture has single mipmap
  if (gap_bAllowSingleMipmap)
  {
    // Effect textures are treated differently
    if (td_ptegEffect != NULL) {
      td_tpLocal.tp_bSingleMipmap = !tex_bDynamicMipmaps;
    } else {
      td_tpLocal.tp_bSingleMipmap = (td_ctFineMipLevels < 2);
    }
  } else {
    // Single mipmap is not allowed
    td_tpLocal.tp_bSingleMipmap = FALSE;  
  }

  // Effect texture might need dynamic mipmaps creation
  if (bNeedUpload && td_ptegEffect != NULL)
  {
    _sfStats.StartTimer(CStatForm::STI_EFFECTRENDER);
    const INDEX iTexFilter = td_ptegEffect->IsWater() ? NONE : tex_iEffectFiltering;  // don't filter water textures
    if (td_tpLocal.tp_bSingleMipmap)
    { 
      // No mipmaps?
      if (iTexFilter != NONE) { 
       FilterBitmap(iTexFilter, td_pulFrames, td_pulFrames, pixWidth, pixHeight);
      }
    } else {
      // Mipmaps!
      const INDEX ctFine = tex_bDynamicMipmaps ? 15 : 0; // whether they're fine or coarse still depends on cvar
      MakeMipmaps(ctFine, td_pulFrames, pixWidth,pixHeight, iTexFilter);
    }
    
    // Done with effect
    _sfStats.StopTimer(CStatForm::STI_EFFECTRENDER);
  } 

  // If not already generated, generate bind number(s) and force upload
  const PIX pixTextureSize = pixWidth * pixHeight;
  if ((td_ctFrames > 1 && td_pulObjects == NULL) || (td_ctFrames <= 1 && td_ulObject == NONE))
  {
    // Check whether frames are present
    ASSERT(td_pulFrames != NULL && td_pulFrames[0] != 0xDEADBEEF); 

    if (td_ctFrames > 1)
    {
      // Animation textures
      td_pulObjects = (ULONG*)AllocMemory(td_ctFrames * sizeof(td_ulProbeObject));
      for (INDEX i = 0; i < td_ctFrames; i++) {
        gfxGenerateTexture(td_pulObjects[i]);
      }
    } else {
      // Single-frame textures
      gfxGenerateTexture(td_ulObject);
    }
    
    // Generate probe texture (if needed)
    ASSERT(td_ulProbeObject == NONE);
    if (td_ptegEffect == NULL && pixTextureSize > 16 * 16) {
      gfxGenerateTexture(td_ulProbeObject);
    }
    
    // Must do initial uploading
    bNeedUpload = TRUE;
    bNoDiscard  = FALSE;
  }

  // Constant textures cannot be probed either
  if (td_ulFlags & TEX_CONSTANT) {
    gfxDeleteTexture(td_ulProbeObject);
  }
  if (td_ulProbeObject == NONE) {
    bUseProbe = FALSE;
  }

  // Update statistics if not updated already for this frame
  if (td_iRenderFrame != _pGfx->gl_iFrameNumber)
  {
    td_iRenderFrame = _pGfx->gl_iFrameNumber;
    
    // Determine size and update
    SLONG slBytes = pixWidth * pixHeight * gfxGetFormatPixRatio(td_ulInternalFormat);
    if (!td_tpLocal.tp_bSingleMipmap) {
      slBytes = slBytes * 4 / 3;
    }
    
    _sfStats.IncrementCounter(CStatForm::SCI_TEXTUREBINDS, 1);
    _sfStats.IncrementCounter(CStatForm::SCI_TEXTUREBINDBYTES, slBytes);
  }

  // If needs to be uploaded
  if (bNeedUpload)
  { 
    // Check whether frames are present
    ASSERT(td_pulFrames != NULL && td_pulFrames[0] != 0xDEADBEEF);

    // Must discard uploaded texture if single mipmap flag has been changed
    const BOOL bLastSingleMipmap = td_ulFlags & TEX_SINGLEMIPMAP;
    bNoDiscard = (bNoDiscard && bLastSingleMipmap == td_tpLocal.tp_bSingleMipmap);
    
    // Update flag
    if (td_tpLocal.tp_bSingleMipmap) {
      td_ulFlags |= TEX_SINGLEMIPMAP;
    } else {
      td_ulFlags &= ~TEX_SINGLEMIPMAP;
    }

    // Upload all texture frames
    ASSERT(td_ulInternalFormat != TEXFMT_NONE);
    if (td_ctFrames > 1)
    {
      // animation textures
      for (INDEX iFr = 0; iFr < td_ctFrames; iFr++)
      { 
        // Determine frame offset and upload texture frame
        ULONG *pulCurrentFrame = td_pulFrames + (iFr * td_slFrameSize / BYTES_PER_TEXEL);
        gfxSetTexture(td_pulObjects[iFr], td_tpLocal);
        gfxUploadTexture(pulCurrentFrame, pixWidth, pixHeight, td_ulInternalFormat, bNoDiscard);
      }
    } else {
      // Single-frame textures
      gfxSetTexture(td_ulObject, td_tpLocal);
      gfxUploadTexture(td_pulFrames, pixWidth, pixHeight, td_ulInternalFormat, bNoDiscard);
    }
    
    // Upload probe texture if exist
    if (td_ulProbeObject != NONE) {
      PIX pixProbeWidth  = pixWidth;
      PIX pixProbeHeight = pixHeight;
      ULONG *pulProbeFrame = td_pulFrames;
      GetMipmapOfSize(16 * 16, pulProbeFrame, pixProbeWidth, pixProbeHeight);
      gfxSetTexture(td_ulProbeObject, td_tpLocal);
      gfxUploadTexture(pulProbeFrame, pixProbeWidth, pixProbeHeight, TS.ts_tfRGBA4, FALSE);
    }
    
    // Clear local texture parameters because we need to correct later texture setting
    td_tpLocal.Clear();
    
    // Free frames' memory if allowed
    if (!(td_ulFlags & TEX_STATIC)) {
      FreeMemory(td_pulFrames);
      td_pulFrames = NULL;
    }
    
    // Done uploading
    ASSERT((td_ctFrames > 1 && td_pulObjects != NULL) || (td_ctFrames == 1 && td_ulObject != NONE));
    return;
  }

  // Do special case for animated textures when parameters re-initialization is required
  if (td_ctFrames > 1 && !td_tpLocal.IsEqual(_tpGlobal[0]))
  {
    // Must reset local texture parameters for each frame of animated texture
    for (INDEX iFr = 0; iFr < td_ctFrames; iFr++)
    {
      td_tpLocal.Clear();
      gfxSetTexture(td_pulObjects[iFr], td_tpLocal);
    }
  }
  
  // Set corresponding probe or texture frame as current
  ULONG ulTexObject = td_ulObject; // single-frame
  if( td_ctFrames > 1) ulTexObject = td_pulObjects[iFrameNo]; // animation

  if (bUseProbe)
  {
    // Set probe if burst value doesn't allow real texture
    if (_pGfx->gl_slAllowedUploadBurst < 0)
    {  
      CTexParams tpTmp = td_tpLocal;
      ASSERT(td_ulProbeObject != NONE);
      gfxSetTexture(td_ulProbeObject, tpTmp);
      //extern INDEX _ctProbeTexs;
      //_ctProbeTexs++;
      //CDebugF("Probed!\n");
      return;
    }
    
    // Reduce allowed burst value
    _pGfx->gl_slAllowedUploadBurst -= pixWidth * pixHeight * 4; // assume 32-bit textures (don't ask driver!)
  } 
  
  // Set real texture and mark that this texture has been drawn
  gfxSetTexture(ulTexObject, td_tpLocal);
  MarkDrawn();

  // Debug check
  ASSERT((td_ctFrames > 1 && td_pulObjects != NULL) || (td_ctFrames <= 1 && td_ulObject != NONE));
}

// Unbind texture from accelerator's memory
void CTextureData::Unbind(void)
{
  // Reset mark
  td_tvLastDrawn = 0I64;

  // Only if bound
  if (td_ulObject == NONE) {
    ASSERT(td_ulProbeObject == NONE);
    return;
  }
  
  // Free frame number(s)
  if (td_ctFrames > 1)
  { 
    // Animation
    for (INDEX iFrame = 0; iFrame < td_ctFrames; iFrame++) {
      gfxDeleteTexture(td_pulObjects[iFrame]);
    }
    
    FreeMemory(td_pulObjects);
    td_pulObjects = NULL;
  } else { 
    // Single-frame
    gfxDeleteTexture(td_ulObject);
  }
  
  // Delete probe texture, too
  gfxDeleteTexture(td_ulProbeObject);
}

// Free memory allocated for texture (if any)
void CTextureData::Clear(void)
{
  // Unbind texture from OpenGL or Direct3D memory
  Unbind();

  // Free allocated memory and reset pointer
  if (td_pulFrames != NULL && td_slFrameSize != 0) {
    FreeMemory(td_pulFrames);
    td_pulFrames = NULL;
    td_slFrameSize = 0;
  }

  // Free memory allocated for texture effect buffers
  FreeEffectBuffers();

  // Release base texture if it exists
  if (td_ptdBaseTexture != NULL) {
    _pResourceMgr->Release(td_ptdBaseTexture);
    td_ptdBaseTexture = NULL;
  }
  
  // Free global effect data if it exists
  if (td_ptegEffect != NULL) {
    delete td_ptegEffect;
    td_ptegEffect = NULL;
  }

  // Reset texture parameters
  td_tpLocal.Clear();
  
  // Clear animation
  CAnimData::Clear();

  // Reset variables (but keep some flags)
  td_ctFrames = 0;
  td_mexWidth  = 0;
  td_mexHeight = 0;
  td_tvLastDrawn = 0I64;
  td_iFirstMipLevel  = 0;
  td_ctFineMipLevels = 0;
  td_pixBufferWidth  = 0;
  td_pixBufferHeight = 0;
  td_ulInternalFormat = TEXFMT_NONE;
  td_iRenderFrame = -1;
  td_ulFlags &= TEX_CONSTANT|TEX_STATIC|TEX_KEEPCOLOR;
}

// Reference counting (override from CAnimData)
void CTextureData::RemReference_internal(void)
{
  _pResourceMgr->Release(this);
}

// Obtain texture and set it for this object
void CTextureObject::SetData_t(const CTFileName &fnmTexture)
{
  // If the filename is empty
  if (fnmTexture == "") {
    // Release current texture
    SetData(NULL);

  // If the filename is not empty
  } else {
    // Obtain it (adds one reference)
    CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_TEXTURE, fnmTexture);
    CTextureData *ptd = static_cast<CTextureData *>(pser);
    
    // Set it as data (adds one more reference, and remove old reference)
    SetData(ptd);
    
    // Release it (removes one reference)
    _pResourceMgr->Release(ptd);
    
    // total reference count +1+1-1 = +1 for new data -1 for old data
  }
}

// Check if this kind of objects is auto-freed
BOOL CTextureData::IsAutoFreed(void)
{
  // Cannot be
  return FALSE;
}

// Get amount of memory used by this object
SLONG CTextureData::GetUsedMemory(void)
{
  // Readout texture object
  ULONG ulTexObject = td_ulObject;
  if (td_ctFrames > 1) {
    ulTexObject = td_pulObjects[0];
  }

  // Add structure size and anim block size
  SLONG slUsed = sizeof(*this) + CAnimData::GetUsedMemory() - sizeof(CAnimData);
  
  // Add effect buffers and static memory if exist
  if (td_pubBuffer1 != NULL) {
    slUsed += 2 * GetEffectBufferSize(); // two buffers
  }
  
  if ((td_ulFlags & TEX_STATIC) && td_pulFrames != NULL) {
    slUsed += td_ctFrames * td_slFrameSize;
  }

  // Add eventual uploaded size and finito
  const SLONG slUploadSize = gfxGetTextureSize(ulTexObject, !td_tpLocal.tp_bSingleMipmap);
  return slUsed + td_ctFrames * slUploadSize; 
}

// Get texel from texture's largest mip-map
COLOR CTextureData::GetTexel(MEX mexU, MEX mexV)
{
  // If the texture is not static
  if (!(td_ulFlags & TEX_STATIC) && !(td_ulFlags & TEX_CONSTANT)) {
    // Print warning
    ASSERTALWAYS("GetTexel: Texture needs to be static and constant.");
    CWarningF("GetTexel: '%s' was not static and/or constant!\n", GetName().ConstData());
  }

  // Make sure that the texture is static
  Force(TEX_STATIC|TEX_CONSTANT);
  
  // Convert dimensions to pixels
  PIX pixU = mexU >> td_iFirstMipLevel;
  PIX pixV = mexV >> td_iFirstMipLevel;
  pixU &= GetPixWidth() - 1;
  pixV &= GetPixHeight() - 1;
  ASSERT(pixU >= 0 && pixU<GetPixWidth());
  ASSERT(pixV >= 0 && pixV<GetPixHeight());
  
  // Read texel from texture
  return ByteSwap32(*(ULONG*)(td_pulFrames + pixV * GetPixWidth() + pixU));
}

// Copy (and eventually convert to floats) one row from texture to an array (iChannel is 1=R,2=G,3=B,4=A)
void CTextureData::FetchRow(PIX pixRow, void *pvDst, INDEX iChannel/*=4*/, BOOL bConvertToFloat/*=TRUE*/)
{
  // If the texture is not static
  if (!(td_ulFlags & TEX_STATIC) && !(td_ulFlags & TEX_CONSTANT)) {
    // Print warning
    ASSERTALWAYS("FetchRow: Texture needs to be static and constant.");
    CWarningF("FetchRow: '%s' was not static and/or constant!\n", GetName().ConstData());
  }
  
  // Workaround: make sure that the texture is static
  Force(TEX_STATIC | TEX_CONSTANT);

  // Determine row offset and loop thru row pixels
  ULONG *pulSrc = td_pulFrames + pixRow*GetPixWidth();
  for (INDEX iCol = 0; iCol < GetPixWidth(); iCol++)
  {
    const UBYTE ubPix = ((UBYTE*)pulSrc)[iCol * 4 + iChannel - 1];
    if (bConvertToFloat) {
      ((FLOAT*)pvDst)[iCol] = NormByteToFloat(ubPix);
    } else {
      ((UBYTE*)pvDst)[iCol] = ubPix;
    }
  }
}

// Get pointer to one row of texture
ULONG *CTextureData::GetRowPointer(PIX pixRow)
{
  // If the texture is not static
  if (!(td_ulFlags & TEX_STATIC) && !(td_ulFlags & TEX_CONSTANT)) {
    // Print warning
    ASSERTALWAYS("GetRowPointer: Texture needs to be static and constant.");
    CWarningF("GetRowPointer: '%s' was not static and/or constant!\n", GetName().ConstData());
  }
  
  // Workaround: make sure that the texture is static
  Force(TEX_STATIC | TEX_CONSTANT);
  return (td_pulFrames + pixRow * GetPixWidth());
}

// Get string description of texture size, mips and parameters
CTString CTextureData::GetDescription(void)
{
  // Get all parameters
  MEX mexSizeU = GetWidth();
  MEX mexSizeV = GetHeight();
  PIX pixSizeU = GetPixWidth();
  MEX pixSizeV = GetPixHeight();
  FLOAT fSizeU = METERS_MEX(mexSizeU);
  FLOAT fSizeV = METERS_MEX(mexSizeV);
  INDEX ctFineMips  = GetNoOfFineMips();
  INDEX ctTotalMips = GetNoOfMips();

  // Print size and mips
  CTString strSizeM;
  if (fSizeU == int(fSizeU) && fSizeV == int(fSizeV)) {
    strSizeM.PrintF("%dx%dm", int(fSizeU), int(fSizeV));
  } else {
    strSizeM.PrintF("%.2fx%.2fm", fSizeU, fSizeV);
  }
  CTString str;
  str.PrintF("%s(%dx%d) %d/%d", strSizeM, pixSizeU, pixSizeV, ctFineMips, ctTotalMips);

  // Print flags
  CTString strFlags = "";
  if (td_ulFlags & TEX_ALPHACHANNEL) {
    strFlags += "A";
  }
  
  if (td_ulFlags & TEX_EQUALIZED) {
    strFlags += "E";
  }
  
  if (td_ulFlags & TEX_32BIT) { 
    strFlags += "H";
  }
  
  if (td_ulFlags & TEX_WASOLD) {
    strFlags += "!";
  }
  
  // If there are any flags, add blank before flags
  if (strFlags != "") {
    str = CTString(" ") + str;
  }

  CAnimInfo aiInfo;
  GetAnimInfo(0, aiInfo);
  CTString strAnims = "";
  if (ad_NumberOfAnims > 1 || aiInfo.ai_NumberOfFrames > 1) {
    strAnims.PrintF(" %d(%d)anim", ad_NumberOfAnims, aiInfo.ai_NumberOfFrames);
  }

  // Return combined string
  return str + strFlags + strAnims;
}

// Returns dimension of effect buffers and size in bytes (of one, not both)
ULONG CTextureData::GetEffectBufferSize()
{
  ASSERT(td_ptegEffect != NULL);
  PIX pixWidth  = td_pixBufferWidth; 
  PIX pixHeight = td_pixBufferHeight;

  ULONG ulSize = pixWidth * pixHeight * sizeof(UBYTE);
  
  // Eventual adjustment for water effect type
  if (td_ptegEffect->IsWater()) {
    ulSize = pixWidth * (pixHeight + 2) * sizeof(SWORD);
  }

  return ulSize;
}

// Initializes td_pixBufferWidth & td_pixBufferHeight
void CTextureData::InitEffectBufferDimensions()
{
  // Initialize as default
  PIX pixWidth  = GetPixWidth(); 
  PIX pixHeight = GetPixHeight();

  // If water effect type
  if (td_ptegEffect->IsWater()) {
    // Adjust size for water type effect (width or height must be 64)
    if (pixWidth > pixHeight) {
      pixHeight = (PIX)((FLOAT)pixHeight / pixWidth * 64.0f);
      pixWidth  = 64;
    } else {
      pixWidth  = (PIX)((FLOAT)pixWidth / pixHeight * 64.0f);
      pixHeight = 64;
    }
  }
  
  // Set 
  td_pixBufferWidth  = pixWidth;
  td_pixBufferHeight = pixHeight;
}

// Allocates and resets effect buffers
ULONG CTextureData::AllocEffectBuffers()
{
  // Free if already allocated
  FreeEffectBuffers();
  
  // Determine size of effect buffers 
  ULONG ulSize = GetEffectBufferSize();
  
  // Allocate and reset buffers (memory walling!)
  td_pubBuffer1 = (UBYTE*)AllocMemory(ulSize + 8);
  td_pubBuffer2 = (UBYTE*)AllocMemory(ulSize + 8);
  memset(td_pubBuffer1, 0, ulSize);
  memset(td_pubBuffer2, 0, ulSize);

  return ulSize;
}

// Free effect buffers' memory
void CTextureData::FreeEffectBuffers()
{
  if (td_pubBuffer1 != NULL) {
    FreeMemory(td_pubBuffer1);
    td_pubBuffer1 = NULL;
  }
  
  if (td_pubBuffer2 != NULL) {
    FreeMemory(td_pubBuffer2);
    td_pubBuffer2 = NULL;
  }
}

UBYTE CTextureData::GetType() const
{
  return TYPE_TEXTURE;
}