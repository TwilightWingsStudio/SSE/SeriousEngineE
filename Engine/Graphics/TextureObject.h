/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TEXTUREOBJECT_H
#define SE_INCL_TEXTUREOBJECT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Engine/Anim/Anim.h>
#include <Engine/Graphics/GfxLibrary.h>

//! An instance of a texture object.
class ENGINE_API CTextureObject : public CAnimObject
{  
  public:
    //! Constructor
    CTextureObject(void);
    
    //! Copy from another object of same class
    void Copy(CTextureObject &toOther);
    
    //! Returns width in mexels.
    MEX GetWidth(void) const;

    //! Returns height in mexels.
    MEX GetHeight(void) const;
    
    //! Returns texture flags.
    ULONG GetFlags(void) const;
    
    //! Read from stream.
    void Read_t(CTStream *istrFile); // throw char *
    
    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char * //	write functions

    //! Obtain texture and set it for this object.
    void SetData_t(const CTFileName &fnmTexture); // throw char *
    
    //! Get filename of texture or empty string if no texture.
    const CTFileName &GetName(void);
};

#endif  /* include-once check. */
