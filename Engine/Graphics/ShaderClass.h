/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SHADERCLASS_H
#define SE_INCL_SHADERCLASS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Modules/ModuleClass.h>

// Shader flags
#define BASE_DOUBLE_SIDED (1UL<<0) // Double sided
#define BASE_FULL_BRIGHT  (1UL<<1) // Full bright

//! Shader description.
struct ShaderDesc
{
  TStaticArray<class CTString> sd_astrTextureNames;
  TStaticArray<class CTString> sd_astrTexCoordNames;
  TStaticArray<class CTString> sd_astrColorNames;
  TStaticArray<class CTString> sd_astrFloatNames;
  TStaticArray<class CTString> sd_astrFlagNames;
  CTString sd_strShaderInfo;
};

//! Shader parameters.
struct ShaderParams
{
  //! Constructor.
  ShaderParams()
  {
    sp_ulFlags = 0;
  }
  
  //! Destructor.
  ~ShaderParams()
  {
    sp_aiTextureIDs.Clear();
    sp_aiTexCoordsIndex.Clear();
    sp_acolColors.Clear();
    sp_afFloats.Clear();
  }
  
  TStaticArray<INDEX> sp_aiTextureIDs;
  TStaticArray<INDEX> sp_aiTexCoordsIndex;
  TStaticArray<COLOR> sp_acolColors;
  TStaticArray<FLOAT> sp_afFloats;
  ULONG               sp_ulFlags;
};

// TODO: Add comment here
class ENGINE_API CShaderClass : public FModuleClass
{
  public:
    //! Constructor
    CShaderClass();

    //! Destructor
    ~CShaderClass();

    //! Pointer to shader function.
    void (*ShaderFunc)(void);
    
    //! Pointer to shader description getter.
    void (*GetShaderDesc)(ShaderDesc &shDesc);

    //! Read from stream.
    void Read_t(CTStream *istrFile); // throw char *
    
    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char *
    
    //! Clear the data.
    void Clear(void);
    
    //! Returns amount of used memory in bytes.
    SLONG GetUsedMemory(void);
    
    //! TODO: Add comment.
    virtual UBYTE GetType() const;
};

#if (defined _MSC_VER)
 #define DECLSPEC_DLLEXPORT _declspec (dllexport)
#else
 #define DECLSPEC_DLLEXPORT
#endif

#define SHADER_MAIN(name) \
  extern "C" void DECLSPEC_DLLEXPORT Shader_##name (void);\
  SYMBOLLOCATOR(Shader_##name);\
  extern "C" void DECLSPEC_DLLEXPORT Shader_##name (void)

#define SHADER_DESC(name,x) \
  extern "C" void DECLSPEC_DLLEXPORT Shader_Desc_##name (x);\
  SYMBOLLOCATOR(Shader_Desc_##name);\
  extern "C" void DECLSPEC_DLLEXPORT Shader_Desc_##name (x)

#endif  /* include-once check. */