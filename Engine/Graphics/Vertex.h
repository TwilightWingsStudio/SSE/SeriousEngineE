/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_VERTEX_H
#define SE_INCL_VERTEX_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/ByteSwap.h>

//! Standard vertex.
struct GFXVertex3
{
  FLOAT x, y, z;
};

//! Standard normal.
struct GFXNormal3
{
  FLOAT nx, ny, nz;
};

//! Standard UV coord.
struct GFXTexCoord
{
  union
  {
    struct { FLOAT u,v; };
    struct { FLOAT s,t; };
  };
};

//! Used for shadows.
struct GFXTexCoord4
{
  FLOAT s,t,r,q;
};

//! 32-bit color.
struct GFXColor
{
  union 
  {
    struct { UBYTE r,g,b,a; };
    struct { ULONG abgr;    };  // reverse order - use ByteSwap32()!
  };

  //! Dummy constructor.
  GFXColor() {};

  //! Default constructor.
  GFXColor(COLOR col)
  {
    abgr = ByteSwap32(col);
  }
  
  //! Set the color.
  __forceinline void Set(COLOR col)
  {
    abgr = ByteSwap32(col);
  }

  //! Multiply two colors in the RGBA format.
  void MultiplyRGBA(const GFXColor &col1, const GFXColor &col2)
  {
    r = (ULONG(col1.r) * col2.r) >> 8;
    g = (ULONG(col1.g) * col2.g) >> 8;
    b = (ULONG(col1.b) * col2.b) >> 8;
    a = (ULONG(col1.a) * col2.a) >> 8;
  }

  //! Multiply two colors in the RGB format.
  void MultiplyRGB(const GFXColor &col1, const GFXColor &col2)
  {
    r = (ULONG(col1.r) * col2.r) >> 8;
    g = (ULONG(col1.g) * col2.g) >> 8;
    b = (ULONG(col1.b) * col2.b) >> 8;
  }

  //! Multiply two colors in the RGB format and copy alpha channel from the first color.
  void MultiplyRGBCopyA1(const GFXColor &col1, const GFXColor &col2)
  {
    r = (ULONG(col1.r) * col2.r) >> 8;
    g = (ULONG(col1.g) * col2.g) >> 8;
    b = (ULONG(col1.b) * col2.b) >> 8;
    a = col1.a;
  }

  //! Brighten the color by a factor.
  void AttenuateRGB(ULONG ulA)
  {
    r = (ULONG(r) * ulA) >> 8;
    g = (ULONG(g) * ulA) >> 8;
    b = (ULONG(b) * ulA) >> 8;
  }

  //! Make alpha channel more opaque by a factor.
  void AttenuateA(ULONG ulA)
  {
    a = (ULONG(a) * ulA) >> 8;
  }
};


#define GFXVertex GFXVertex3

//! Vertex with color/shade. Used for terrain rendering.
struct GFXVertex4
{
  //! Constructor.
  GFXVertex4()
  {
  }
  
  FLOAT x,y,z;
  
  union
  {
    struct { struct GFXColor col; };
    struct { SLONG shade; };
  };
};


#define GFXNormal GFXNormal3

#endif  /* include-once check. */