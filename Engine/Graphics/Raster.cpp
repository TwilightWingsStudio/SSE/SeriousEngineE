/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/Raster.h>

#include <Core/Base/ListIterator.inl>
#include <Engine/Graphics/DrawPort.h>
#include <Engine/Graphics/GfxLibrary.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Raster functions
 
// Constructor for given size.
CRaster::CRaster(PIX ulWidth, PIX ulHeight, ULONG ulFlags) 
  : ra_MainDrawPort()
{
  // Remember width and height
  ra_Width  = ulWidth;
  ra_Height = ulHeight;

  // Clear uninitialized fields
  ra_LockCount = 0;

  // Set flags
  ra_Flags = ulFlags;

  // Link the main draw port.
  ra_MainDrawPort.dp_Raster = this;
  ra_pvpViewPort = NULL;
  
  // Calculate main draw port dimensions.
  ra_MainDrawPort.RecalculateDimensions();
}

// Destructor.
CRaster::~CRaster(void)
{
  // Raster must be unlocked before destroying it
	ASSERT(ra_LockCount == 0);
}

// Lock for drawing.
BOOL CRaster::Lock()
{
  ASSERT(this != NULL);
  ASSERT(ra_LockCount >= 0);

  // If raster size is too small in some axis
  if (ra_Width < 1 || ra_Height < 1) {
    // Do not allow locking
    ASSERTALWAYS("Raster size to small to be locked!");
    return FALSE;
  }

  // If allready locked
  if (ra_LockCount > 0) {
    // Just increment counter
    ra_LockCount++;
    return TRUE;
    
  // If not already locked
  } else {
    // Try to lock with driver
    BOOL bLocked = _pGfx->LockRaster(this);
    
    // If succeeded in locking
    if (bLocked) {
      // Set the counter to 1
      ra_LockCount = 1;
      return TRUE;
    }
    
    // Lock not ok
    return FALSE;
  }
}

// Unlock after drawing.
void CRaster::Unlock()
{
  ASSERT(this != NULL);
  ASSERT(ra_LockCount > 0);

  // Decrement counter
  ra_LockCount--;
  
  // If reached zero
  if (ra_LockCount == 0 ) {
    // Unlock it with driver
    _pGfx->UnlockRaster(this);
  }
}

// Change Raster size.
void CRaster::Resize(PIX pixWidth, PIX pixHeight)
{
  ASSERT(pixWidth > 0 && pixHeight > 0);
  if (pixWidth <= 0) {
    pixWidth  = 1;
  }
  
  if (pixHeight <= 0) {
    pixHeight = 1;
  }
  
  ra_Width  = pixWidth;
  ra_Height = pixHeight;
  
  ra_MainDrawPort.RecalculateDimensions();
}