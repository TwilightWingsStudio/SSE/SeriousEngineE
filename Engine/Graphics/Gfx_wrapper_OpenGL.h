/* Copyright (c) 2022 by Dreamy Cecil & ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_GFX_OGL_H
#define SE_INCL_GFX_OGL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/Graphics/GfxInterface.h>

class IGfxOpenGL : public IGfxInterface
{
  public:
    //! Unique graphical API type.
    GfxAPIType GetType(void) const override {
      return GAT_OGL;
    };

  //! Graphical API methods.
  public:

    //! Enable operations.
    void EnableDepthWrite(void) override;
    void EnableDepthBias(void) override;
    void EnableDepthTest(void) override;
    void EnableAlphaTest(void) override;
    void EnableBlend(void) override;
    void EnableDither(void) override;
    void EnableTexture(void) override;
    void EnableClipping(void) override;
    void EnableClipPlane(void) override;

    //! Disable operations.
    void DisableDepthWrite(void) override;
    void DisableDepthBias(void) override;
    void DisableDepthTest(void) override;
    void DisableAlphaTest(void) override;
    void DisableBlend(void) override;
    void DisableDither(void) override;
    void DisableTexture(void) override;
    void DisableClipping(void) override;
    void DisableClipPlane(void) override;

    //! Set blending operations.
    void BlendFunc(GfxBlend eSrc, GfxBlend eDst) override;

    //! Set depth buffer compare mode.
    void DepthFunc(GfxComp eFunc) override;

    //! Set depth buffer range.
    void DepthRange(FLOAT fMin, FLOAT fMax) override;

    //! Color mask control (use CT_RMASK, CT_GMASK, CT_BMASK, CT_AMASK to enable specific channels).
    void SetColorMask(ULONG ulColorMask) override;

  //! Projections.
  public:

    //! Set face culling.
    void CullFace(GfxFace eFace) override;
    void FrontFace(GfxFace eFace) override;

    //! Set custom clip plane (if NULL, disable it).
    void ClipPlane(const DOUBLE *pdPlane) override;

    //! Set orthographic matrix.
    void SetOrtho(const FLOAT fLeft, const FLOAT fRight, const FLOAT fTop,  const FLOAT fBottom, const FLOAT fNear, const FLOAT fFar, const BOOL bSubPixelAdjust) override;

    //! Set frustrum matrix.
    void SetFrustum(const FLOAT fLeft, const FLOAT fRight, const FLOAT fTop,  const FLOAT fBottom, const FLOAT fNear, const FLOAT fFar) override;

    //! Set view matrix.
    void SetViewMatrix(const FLOAT *pfMatrix) override;

    //! Set texture matrix.
    void SetTextureMatrix(const FLOAT *pfMatrix) override;

    //! Polygon mode (point, line or fill).
    void PolygonMode(GfxPolyMode ePolyMode) override;

  //! Textures.
  public:

    //! Set texture wrapping mode.
    void SetTextureWrapping(enum GfxWrap eWrapU, enum GfxWrap eWrapV) override;

    //! Set texture modulation mode (1X or 2X).
    void SetTextureModulation(INDEX iScale) override;

    //! Generate texture for API.
    void GenerateTexture(ULONG &ulTexObject) override;

    //! Unbind texture from API.
    void DeleteTexture(ULONG &ulTexObject) override;

  //! Vertex arrays.
  public:

    //! Prepare vertex array for API.
    void SetVertexArray(void *pvtx, INDEX ctVtx, BOOL bWithShade) override;

  #ifdef SE1_TRUFORM
    //! Prepare normal array for API.
    void SetNormalArray(GFXNormal *pnor) override;
  #endif

    //! Prepare UV array for API.
    void SetTexCoordArray(GFXTexCoord *ptex, BOOL b4) override; // b4 = projective mapping (4 FLOATs)

    //! Prepare color array for API.
    void SetColorArray(GFXColor *pcol) override;

    //! Draw prepared arrays.
    void DrawElements(INDEX ctElem, INDEX *pidx) override;

    //! Set constant color for subsequent rendering (until first SetColorArray() call!).
    void SetConstantColor(COLOR col) override;

    //! Color array usage control.
    void EnableColorArray(void) override;
    void DisableColorArray(void) override;

  //! Miscellaneous.
  public:
  
    //! Force finish of rendering queue.
    void Finish(void) override;

    //! Compiled vertex array control.
    void LockArrays(void) override;

  #ifdef SE1_TRUFORM
    //! Toggle truform.
    void EnableTruform(void) override;
    void DisableTruform(void) override;
  #endif
};

#endif  /* include-once check. */
