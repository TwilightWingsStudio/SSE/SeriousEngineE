/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

// Dummy function (one size fits all:)
static void none_void(void)
{
  ASSERT(_pGfx->gl_eCurrentAPI == GAT_NONE);
}

// DUMMY FUNCTIONS FOR NONE API
static void none_BlendFunc(GfxBlend eSrc, GfxBlend eDst) { NOTHING; }
static void none_DepthFunc(GfxComp eFunc) { NOTHING; };
static void none_DepthRange(FLOAT fMin, FLOAT fMax) { NOTHING; };
static void none_CullFace(GfxFace eFace) { NOTHING; };
static void none_ClipPlane(const DOUBLE *pdViewPlane) { NOTHING; };
static void none_SetOrtho(  const FLOAT fLeft, const FLOAT fRight, const FLOAT fTop, const FLOAT fBottom, const FLOAT fNear, const FLOAT fFar, const BOOL bSubPixelAdjust) { NOTHING; };
static void none_SetFrustum(const FLOAT fLeft, const FLOAT fRight, const FLOAT fTop, const FLOAT fBottom, const FLOAT fNear, const FLOAT fFar) { NOTHING; };
static void none_SetMatrix(const FLOAT *pfMatrix) { NOTHING; };
static void none_PolygonMode(GfxPolyMode ePolyMode) { NOTHING; };
static void none_SetTextureWrapping(enum GfxWrap eWrapU, enum GfxWrap eWrapV) { NOTHING; };
static void none_SetTextureModulation(INDEX iScale) { NOTHING; };
static void none_GenDelTexture(ULONG &ulTexObject) { NOTHING; };
static void none_SetVertexArray(void *pvtx, INDEX ctVtx, BOOL bWithShade) { NOTHING; };
static void none_SetNormalArray(GFXNormal *pnor) { NOTHING; };
static void none_SetTexCoordArray(GFXTexCoord *ptex, BOOL b4) { NOTHING; };
static void none_SetColorArray(GFXColor *pcol) { NOTHING; };
static void none_DrawElements(INDEX ctElem, INDEX *pidx) { NOTHING; };
static void none_SetConstantColor(COLOR col) { NOTHING; };
static void none_SetColorMask(ULONG ulColorMask) { NOTHING; };

static void Null_SetFunctionPointers()
{
  gfxEnableDepthWrite     = &none_void;
  gfxEnableDepthBias      = &none_void;
  gfxEnableDepthTest      = &none_void;
  gfxEnableAlphaTest      = &none_void;
  gfxEnableBlend          = &none_void;
  gfxEnableDither         = &none_void;
  gfxEnableTexture        = &none_void;
  gfxEnableClipping       = &none_void;
  gfxEnableClipPlane      = &none_void;
  gfxEnableTruform        = &none_void;
  gfxDisableDepthWrite    = &none_void;
  gfxDisableDepthBias     = &none_void;
  gfxDisableDepthTest     = &none_void;
  gfxDisableAlphaTest     = &none_void;
  gfxDisableBlend         = &none_void;
  gfxDisableDither        = &none_void;
  gfxDisableTexture       = &none_void;
  gfxDisableClipping      = &none_void;
  gfxDisableClipPlane     = &none_void;
  gfxDisableTruform       = &none_void;
  gfxBlendFunc            = &none_BlendFunc;
  gfxDepthFunc            = &none_DepthFunc;
  gfxDepthRange           = &none_DepthRange;
  gfxCullFace             = &none_CullFace;
  gfxFrontFace            = &none_CullFace;
  gfxClipPlane            = &none_ClipPlane;
  gfxSetOrtho             = &none_SetOrtho;
  gfxSetFrustum           = &none_SetFrustum;
  gfxSetTextureMatrix     = &none_SetMatrix;
  gfxSetViewMatrix        = &none_SetMatrix;
  gfxPolygonMode          = &none_PolygonMode;
  gfxSetTextureWrapping   = &none_SetTextureWrapping;
  gfxSetTextureModulation = &none_SetTextureModulation;
  gfxGenerateTexture      = &none_GenDelTexture;
  gfxDeleteTexture        = &none_GenDelTexture;   
  gfxSetVertexArray       = &none_SetVertexArray;  
  gfxSetNormalArray       = &none_SetNormalArray;  
  gfxSetTexCoordArray     = &none_SetTexCoordArray;
  gfxSetColorArray        = &none_SetColorArray;   
  gfxDrawElements         = &none_DrawElements;    
  gfxSetConstantColor     = &none_SetConstantColor;
  gfxEnableColorArray     = &none_void;
  gfxDisableColorArray    = &none_void;
  gfxFinish               = &none_void;
  gfxLockArrays           = &none_void;
  gfxSetColorMask         = &none_SetColorMask;
}