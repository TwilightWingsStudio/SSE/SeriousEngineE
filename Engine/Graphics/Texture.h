/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TEXTURE_H
#define SE_INCL_TEXTURE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/Graphics/TextureData.h>
#include <Engine/Graphics/TextureObject.h>

//! Create texture from file and save it in the same location.
ENGINE_API extern void CreateTexture_t(const CTFileName &inFileName, MEX inMex, INDEX inMipmaps, BOOL bForce32bit);

//! Create texture from file and save it in specified location.
ENGINE_API extern void CreateTexture_t(const CTFileName &inFileName, const CTFileName &outFileName, MEX inMex,
                                       INDEX inMipmaps, BOOL bForce32bit);

//! Process texture creation script.
ENGINE_API extern void ProcessScript_t(const CTFileName &inFileName);

#endif  /* include-once check. */
