/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/DrawPort.h>

#include <Core/Base/Memory.h>
#include <Core/Base/CRC.h>
#include <Core/Math/Functions.h>
#include <Core/Math/Projection.h>
#include <Core/Math/AABBox.h>
#include <Engine/Graphics/Raster.h>
#include <Engine/Graphics/GfxProfile.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/ImageInfo.h>
#include <Core/Graphics/Color.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Graphics/ViewPort.h>
#include <Engine/Graphics/Font.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/StaticStackArray.cpp>

extern INDEX gfx_bDecoratedText;
extern INDEX ogl_iFinish;
extern INDEX d3d_iFinish;

////////////////////////////////////////////////////////////////////////////////////////////
// RECT HANDLING ROUTINES

// TODO: Add comment here
static BOOL ClipToDrawPort(const CDrawPort *pdp, PIX &pixI, PIX &pixJ, PIX &pixW, PIX &pixH)
{
  if (pixI < 0) {
    pixW += pixI;
    pixI = 0;
  } else if (pixI >= pdp->GetWidth()) {
    return FALSE;
  }

  if (pixJ < 0) {
    pixH += pixJ;
    pixJ = 0;
  } else if (pixJ >= pdp->GetHeight()) {
    return FALSE;
  }

  if (pixW < 1 || pixH < 1) {
    return FALSE;
  }

  if ((pixI + pixW) > pdp->GetWidth())  {
    pixW = pdp->GetWidth()  - pixI;
  }
  if ((pixJ + pixH) > pdp->GetHeight()) {
    pixH = pdp->GetHeight() - pixJ;
  }

  ASSERT(pixI >= 0 && pixI < pdp->GetWidth());
  ASSERT(pixJ >= 0 && pixJ < pdp->GetHeight());
  ASSERT(pixW > 0  && pixH > 0);
  return TRUE;
}

// Set scissor (clipping) to window inside drawport
static void SetScissor(const CDrawPort *pdp, PIX pixI, PIX pixJ, PIX pixW, PIX pixH)
{
  ASSERT(pixI >= 0 && pixI < pdp->GetWidth());
  ASSERT(pixJ >= 0 && pixJ < pdp->GetHeight());
  ASSERT(pixW > 0  && pixH > 0);

  const PIX pixInvMinJ = pdp->dp_Raster->ra_Height - (pdp->dp_MinJ+pixJ+pixH);
  pglScissor(pdp->dp_MinI+pixI, pixInvMinJ, pixW, pixH);
  ASSERT(pglIsEnabled(GL_SCISSOR_TEST));
  OGL_CHECKERROR;
}

// Reset scissor (clipping) to whole drawport
static void ResetScissor(const CDrawPort *pdp)
{
  const PIX pixMinSI = pdp->dp_ScissorMinI;
  const PIX pixMaxSI = pdp->dp_ScissorMaxI;
  const PIX pixMinSJ = pdp->dp_Raster->ra_Height - 1 - pdp->dp_ScissorMaxJ;
  const PIX pixMaxSJ = pdp->dp_Raster->ra_Height - 1 - pdp->dp_ScissorMinJ;

  pglScissor(pixMinSI, pixMinSJ, pixMaxSI-pixMinSI + 1, pixMaxSJ - pixMinSJ + 1);
  ASSERT(pglIsEnabled(GL_SCISSOR_TEST));
  OGL_CHECKERROR;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// DRAWPORT ROUTINES

CDrawPort CDrawPort::CreateSubDrawPort(const PIXaabbox2D &rect) const
{
  CDrawPort dpResult = *this; // Copy all the data.
  const CDrawPort *pdpBase = this;

  // Force dimensions
  dpResult.dp_MinI   = rect.Min()(1) + pdpBase->dp_MinI;
  dpResult.dp_MinJ   = rect.Min()(2) + pdpBase->dp_MinJ;
  dpResult.dp_Width  = rect.Size()(1);
  dpResult.dp_Height = rect.Size()(2);
  dpResult.dp_MaxI   = dpResult.dp_MinI + dpResult.dp_Width  - 1; // NOTE: Will be removed.
  dpResult.dp_MaxJ   = dpResult.dp_MinJ + dpResult.dp_Height - 1; // NOTE: Will be removed.

  // Clip scissor to origin drawport
  dpResult.dp_ScissorMinI = Max(dpResult.dp_MinI, pdpBase->dp_MinI);
  dpResult.dp_ScissorMinJ = Max(dpResult.dp_MinJ, pdpBase->dp_MinJ);
  dpResult.dp_ScissorMaxI = Min(dpResult.dp_MaxI, pdpBase->dp_MaxI);
  dpResult.dp_ScissorMaxJ = Min(dpResult.dp_MaxJ, pdpBase->dp_MaxJ);

  if (dpResult.dp_ScissorMinI > dpResult.dp_ScissorMaxI) {
    dpResult.dp_ScissorMinI = dpResult.dp_ScissorMaxI = 0;
  }

  if (dpResult.dp_ScissorMinJ > dpResult.dp_ScissorMaxJ) {
    dpResult.dp_ScissorMinJ = dpResult.dp_ScissorMaxJ = 0;
  }

  // Set relative dimensions to make it contain the whole raster
  dpResult.dp_MinIOverRasterSizeI  = (DOUBLE)dpResult.dp_MinI   / dp_Raster->ra_Width;
  dpResult.dp_MinJOverRasterSizeJ  = (DOUBLE)dpResult.dp_MinJ   / dp_Raster->ra_Height;
  dpResult.dp_SizeIOverRasterSizeI = (DOUBLE)dpResult.GetWidth()  / dp_Raster->ra_Width;
  dpResult.dp_SizeJOverRasterSizeJ = (DOUBLE)dpResult.GetHeight() / dp_Raster->ra_Height;

  return dpResult;
}

CDrawPort CDrawPort::CreateSubDrawPortRatio(const FLOATaabbox2D &rect) const
{
  PIX pixMinI = rect.Min()(1) * GetWidth();
  PIX pixMinJ = rect.Min()(2) * GetHeight();
  PIX pixMaxI = rect.Max()(1) * GetWidth();
  PIX pixMaxJ = rect.Max()(2) * GetHeight();

  return CreateSubDrawPort(PIXaabbox2D( PIX2D(pixMinI, pixMinJ), PIX2D(pixMaxI, pixMaxJ) ));
}

// Set cloned drawport dimensions using ratio
void CDrawPort::InitCloned(const CDrawPort *pdpBase, DOUBLE rMinI, DOUBLE rMinJ, DOUBLE rSizeI, DOUBLE rSizeJ)
{
  CDrawPort *pdp = (CDrawPort *)pdpBase;
  *this = pdp->CreateSubDrawPortRatio(FLOATaabbox2D(FLOAT2D(rMinI, rMinJ), FLOAT2D(rMinI + rSizeI, rMinJ + rSizeJ) ));
}

// Default constructor.
CDrawPort::CDrawPort()
{
  // Remember the raster structures
  dp_Raster = NULL;
  dp_fWideAdjustment = 1.0f;
  dp_bRenderingOverlay = FALSE;

  // Reset dimensions and scissor.
  dp_ScissorMinI = dp_MinI = 0;
  dp_ScissorMinJ = dp_MinJ = 0;
  dp_ScissorMaxI = dp_MaxI = 0;
  dp_ScissorMaxJ = dp_MaxJ = 0;
  dp_Width = dp_Height = 0;

  // Set relative dimensions to make it contain the whole raster
  dp_MinIOverRasterSizeI  = 0.0;
  dp_MinJOverRasterSizeJ  = 0.0;
  dp_SizeIOverRasterSizeI = 1.0;
  dp_SizeJOverRasterSizeJ = 1.0;

  // Clear unknown values
  dp_FontData = NULL;
  dp_pixTextCharSpacing = 1;
  dp_pixTextLineSpacing = 0;
  dp_fTextScaling = 1.0f;
  dp_fTextAspect  = 1.0f;
  dp_iTextMode    = 1;
  dp_ulBlendingRA = 0;
  dp_ulBlendingGA = 0;
  dp_ulBlendingBA = 0;
  dp_ulBlendingA  = 0;
}

CDrawPort::CDrawPort(const CDrawPort *pdpOriginal)
{
  CDrawPort *pdp = (CDrawPort *)pdpOriginal;
  *this = *pdp;
}

// Clone a drawport using ratio
CDrawPort::CDrawPort(const CDrawPort *pdpBase, DOUBLE rMinI, DOUBLE rMinJ, DOUBLE rSizeI, DOUBLE rSizeJ)
{
  InitCloned(pdpBase, rMinI,rMinJ, rSizeI,rSizeJ);
}

//! Clone a drawport and create smaller within.
CDrawPort::CDrawPort(const CDrawPort *pdpBase, const PIXaabbox2D &box)
{
  CDrawPort *pdp = (CDrawPort *)pdpBase;
  
  *this = pdp->CreateSubDrawPort(box);
}

//! Assignment operation.
CDrawPort &CDrawPort::operator=(const CDrawPort &dpOriginal)
{
  dp_Raster = dpOriginal.dp_Raster;

  // Force dimemnsions.
  dp_MinI = dpOriginal.dp_MinI;
  dp_MinJ = dpOriginal.dp_MinJ;
  dp_MaxI = dpOriginal.dp_MaxI;
  dp_MaxJ = dpOriginal.dp_MaxJ;
  dp_Width = dpOriginal.dp_Width; // NOTE: Will be removed.
  dp_Height = dpOriginal.dp_Height; // NOTE: Will be removed.

  // Clip scissor to origin drawport.
  dp_ScissorMinI = dpOriginal.dp_ScissorMinI;
  dp_ScissorMinJ = dpOriginal.dp_ScissorMinJ;
  dp_ScissorMaxI = dpOriginal.dp_ScissorMaxI;
  dp_ScissorMaxJ = dpOriginal.dp_ScissorMaxJ;

  // Clone some var.
  dp_FontData = dpOriginal.dp_FontData;
  dp_pixTextCharSpacing = dpOriginal.dp_pixTextCharSpacing;
  dp_pixTextLineSpacing = dpOriginal.dp_pixTextLineSpacing;
  dp_fTextScaling = dpOriginal.dp_fTextScaling;
  dp_fTextAspect  = dpOriginal.dp_fTextAspect;
  dp_iTextMode    = dpOriginal.dp_iTextMode;
  dp_fWideAdjustment   = dpOriginal.dp_fWideAdjustment;
  dp_bRenderingOverlay = dpOriginal.dp_bRenderingOverlay;

  // Set relative dimensions to make it contain the whole raster.
  dp_MinIOverRasterSizeI  = dpOriginal.dp_MinIOverRasterSizeI;
  dp_MinJOverRasterSizeJ  = dpOriginal.dp_MinJOverRasterSizeJ;
  dp_SizeIOverRasterSizeI = dpOriginal.dp_SizeIOverRasterSizeI;
  dp_SizeJOverRasterSizeJ = dpOriginal.dp_SizeJOverRasterSizeJ;

  // Reset rest of vars.
  dp_ulBlendingRA = 0;
  dp_ulBlendingGA = 0;
  dp_ulBlendingBA = 0;
  dp_ulBlendingA  = 0;

  return *this;
}

// Check if a drawport is dualhead
BOOL CDrawPort::IsDualHead(void) const
{
  return GetWidth() * 3 == GetHeight() * 8;
}

// Check if a drawport is already wide screen
BOOL CDrawPort::IsWideScreen(void) const
{
  return GetWidth() * 9 == GetHeight() * 16;
}

// Returns unique drawports number
ULONG CDrawPort::GetID(void) const
{
  ULONG ulCRC;
  CRC_Start(  ulCRC);
  CRC_AddLONGLONG(ulCRC, (u64)dp_Raster);
  CRC_AddLONG(ulCRC, (ULONG)dp_MinI);
  CRC_AddLONG(ulCRC, (ULONG)dp_MinJ);
  CRC_AddLONG(ulCRC, (ULONG)dp_MaxI);
  CRC_AddLONG(ulCRC, (ULONG)dp_MaxJ);
  CRC_AddLONG(ulCRC, (ULONG)dp_ScissorMinI);
  CRC_AddLONG(ulCRC, (ULONG)dp_ScissorMinJ);
  CRC_AddLONG(ulCRC, (ULONG)dp_ScissorMaxI);
  CRC_AddLONG(ulCRC, (ULONG)dp_ScissorMaxJ);
  CRC_Finish( ulCRC);
  return ulCRC;
}

// Dualhead cloning
CDrawPort::CDrawPort(const CDrawPort *pdpBase, BOOL bLeft)
{
  // If it is not a dualhead drawport
  if (!pdpBase->IsDualHead()) {
    // Always use entire drawport
    InitCloned(pdpBase, 0, 0, 1, 1);

  // If dualhead is on then use left or right
  } else {
    if (bLeft) {
      InitCloned(pdpBase, 0, 0, 0.5, 1);
    } else {
      InitCloned(pdpBase, 0.5, 0, 0.5, 1);
    }
  }
}

// Wide-screen cloning
void CDrawPort::MakeWideScreen(CDrawPort *pdp)
{
  // Already wide?
  if (IsWideScreen()) {
    pdp->InitCloned(this, 0, 0, 1, 1);
    return;
  } else { // Make wide!
    // Get size
    const PIX pixSizeI = GetWidth();
    const PIX pixSizeJ = GetHeight();

    // Make horiz width
    PIX pixSizeJW = pixSizeI * 9 / 16;

    // If already too wide
    if (pixSizeJW > pixSizeJ - 10) {
      // No wide screen
      pdp->InitCloned(this, 0, 0, 1, 1);
      return;
    }

    // Clear upper and lower blanks
    const PIX pixJ0 = (pixSizeJ - pixSizeJW) / 2;
    if (Lock()) {
      Fill(0, 0, pixSizeI, pixJ0, C_BLACK | CT_OPAQUE);
      Fill(0, pixJ0 + pixSizeJW, pixSizeI, pixJ0, C_BLACK | CT_OPAQUE);
      Unlock();
    }

    // Init
    pdp->InitCloned(this, 0, FLOAT(pixJ0) / pixSizeJ, 1, FLOAT(pixSizeJW) / pixSizeJ);
    pdp->dp_fWideAdjustment = 9.0f / 12.0f;
  }
}

// Recalculate pixel dimensions from relative dimensions and raster size.
void CDrawPort::RecalculateDimensions(void)
{
  const PIX pixRasterSizeI = dp_Raster->ra_Width;
  const PIX pixRasterSizeJ = dp_Raster->ra_Height;
  dp_Width  = (PIX)(dp_SizeIOverRasterSizeI * pixRasterSizeI); // NOTE: Will be removed.
  dp_Height = (PIX)(dp_SizeJOverRasterSizeJ * pixRasterSizeJ); // NOTE: Will be removed.
  dp_ScissorMinI = dp_MinI = (PIX)(dp_MinIOverRasterSizeI * pixRasterSizeI);
  dp_ScissorMinJ = dp_MinJ = (PIX)(dp_MinJOverRasterSizeJ * pixRasterSizeJ);
  dp_ScissorMaxI = dp_MaxI = dp_MinI + GetWidth()  - 1;
  dp_ScissorMaxJ = dp_MaxJ = dp_MinJ + GetHeight() - 1;
}

// Set orthogonal projection
void CDrawPort::SetOrtho(void) const
{
  // Finish all pending render-operations (if required)
  ogl_iFinish = Clamp(ogl_iFinish, 0L, 3L);
  d3d_iFinish = Clamp(d3d_iFinish, 0L, 3L);

  if ((ogl_iFinish == 3 && _pGfx->GetCurrentAPI() == GAT_OGL)
   || (d3d_iFinish == 3 && _pGfx->GetCurrentAPI() == GAT_D3D8))
  {
    gfxFinish();
  }

  // Prepare ortho dimensions
  const PIX pixMinI  = dp_MinI;
  const PIX pixMinSI = dp_ScissorMinI;
  const PIX pixMaxSI = dp_ScissorMaxI;
  const PIX pixMaxJ  = dp_Raster->ra_Height - 1 - dp_MinJ;
  const PIX pixMinSJ = dp_Raster->ra_Height - 1 - dp_ScissorMaxJ;
  const PIX pixMaxSJ = dp_Raster->ra_Height - 1 - dp_ScissorMinJ;

  // Init matrices (D3D needs sub-pixel adjustment)
  gfxSetOrtho(pixMinSI - pixMinI, pixMaxSI - pixMinI + 1,
              pixMaxJ - pixMaxSJ, pixMaxJ - pixMinSJ + 1, 0.0f, -1.0f, TRUE);
  gfxDepthRange(0, 1);
  gfxSetViewMatrix(NULL);

  // Disable face culling, custom clip plane and truform
  gfxCullFace(GFX_NONE);
  gfxDisableClipPlane();
#ifdef SE1_TRUFORM
  gfxDisableTruform();
#endif // SE1_TRUFORM
}

// Set given projection
void CDrawPort::SetProjection(CAnyProjection3D &apr) const
{
  // Finish all pending render-operations (if required)
  ogl_iFinish = Clamp(ogl_iFinish, 0L, 3L);
  d3d_iFinish = Clamp(d3d_iFinish, 0L, 3L);

  if ((ogl_iFinish == 3 && _pGfx->GetCurrentAPI() == GAT_OGL)
   || (d3d_iFinish == 3 && _pGfx->GetCurrentAPI() == GAT_D3D8))
  {
    gfxFinish();
  }

  // If isometric projection
  if (apr.IsIsometric())
  {
    CIsometricProjection3D &ipr = (CIsometricProjection3D&) * apr;
    const FLOAT2D vMin  = ipr.pr_ScreenBBox.Min() - ipr.pr_ScreenCenter;
    const FLOAT2D vMax  = ipr.pr_ScreenBBox.Max() - ipr.pr_ScreenCenter;
    const FLOAT fFactor = 1.0f / (ipr.ipr_ZoomFactor * ipr.pr_fViewStretch);
    const FLOAT fNear   = ipr.pr_NearClipDistance;
    const FLOAT fLeft   = +vMin(1) * fFactor;
    const FLOAT fRight  = +vMax(1) * fFactor;
    const FLOAT fTop    = -vMin(2) * fFactor;
    const FLOAT fBottom = -vMax(2) * fFactor;

    // If far clip plane is not specified use maximum expected dimension of the world
    FLOAT fFar = ipr.pr_FarClipDistance;
    if (fFar < 0) {
      fFar = 1E5f;  // max size 32768, 3D (sqrt(3)), rounded up
    }
    gfxSetOrtho(fLeft, fRight, fTop, fBottom, fNear, fFar, FALSE);

  } else { // if perspective projection

    ASSERT(apr.IsPerspective());
    CPerspectiveProjection3D &ppr = (CPerspectiveProjection3D&) * apr;
    const FLOAT fNear   = ppr.pr_NearClipDistance;
    const FLOAT fLeft   = ppr.pr_plClipL(3) / ppr.pr_plClipL(1) * fNear;
    const FLOAT fRight  = ppr.pr_plClipR(3) / ppr.pr_plClipR(1) * fNear;
    const FLOAT fTop    = ppr.pr_plClipU(3) / ppr.pr_plClipU(2) * fNear;
    const FLOAT fBottom = ppr.pr_plClipD(3) / ppr.pr_plClipD(2) * fNear;

    // If far clip plane is not specified use maximum expected dimension of the world
    FLOAT fFar = ppr.pr_FarClipDistance;
    if (fFar < 0) {
      fFar = 1E5f;  // max size 32768, 3D (sqrt(3)), rounded up
    }
    gfxSetFrustum(fLeft, fRight, fTop, fBottom, fNear, fFar);
  }

  // Set some rendering params
  gfxDepthRange(apr->pr_fDepthBufferNear, apr->pr_fDepthBufferFar);
  gfxCullFace(GFX_BACK);
  gfxSetViewMatrix(NULL);

#ifdef SE1_TRUFORM
  gfxDisableTruform();
#endif // SE1_TRUFORM

  // If projection is mirrored/warped and mirroring is allowed
  if (apr->pr_bMirror || apr->pr_bWarp)
  {
    // Set custom clip plane 0 to mirror plane
    gfxEnableClipPlane();
    DOUBLE adViewPlane[4];
    adViewPlane[0] = +apr->pr_plMirrorView(1);
    adViewPlane[1] = +apr->pr_plMirrorView(2);
    adViewPlane[2] = +apr->pr_plMirrorView(3);
    adViewPlane[3] = -apr->pr_plMirrorView.Distance();
    gfxClipPlane(adViewPlane); // NOTE: view clip plane is multiplied by inverse modelview matrix at time when specified

  } else { // if projection is not mirrored
    // Just disable custom clip plane 0
    gfxDisableClipPlane();
  }
}

// Implementation for some drawport routines that uses raster class
void CDrawPort::Unlock(void)
{
  dp_Raster->Unlock();
  _pGfx->UnlockDrawPort(this);
}

// TODO: Add comment here
BOOL CDrawPort::Lock(void)
{
  _pfGfxProfile->StartTimer(CGfxProfile::PTI_LOCKDRAWPORT);
  BOOL bRasterLocked = dp_Raster->Lock();
  if (bRasterLocked)
  {
    // Try to lock drawport with driver
    BOOL bDrawportLocked = _pGfx->LockDrawPort(this);
    if (!bDrawportLocked) {
      dp_Raster->Unlock();
      bRasterLocked = FALSE;
    }
  }

  // Done
  _pfGfxProfile->StopTimer(CGfxProfile::PTI_LOCKDRAWPORT);
  return bRasterLocked;
}

///////////////////////////////////////////////////////////////////////////////////
// DRAWING ROUTINES

// Draw one point
void CDrawPort::DrawPoint(PIX pixI, PIX pixJ, COLOR col, PIX pixRadius/*=1*/) const
{
  // Check API and radius
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  ASSERT(pixRadius >= 0);
  if (pixRadius == 0) {
    return; // do nothing if radius is 0
  }

  // Setup rendering mode
  gfxDisableTexture();
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  // Set point color/alpha and radius
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);
  const FLOAT fR = pixRadius;

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    const FLOAT fI = pixI + 0.5f;
    const FLOAT fJ = pixJ + 0.5f;
    glCOLOR(col);
    pglPointSize(fR);
    pglBegin(GL_POINTS);
      pglVertex2f(fI,fJ);
    pglEnd();
    OGL_CHECKERROR;
  } // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr;
    const FLOAT fI = pixI + 0.75f;
    const FLOAT fJ = pixJ + 0.75f;
    const ULONG d3dColor = rgba2argb(col);
    CTVERTEX avtx = {fI, fJ, 0, d3dColor, 0, 0};
    hr = _pGfx->gl_pd3dDevice->SetRenderState(D3DRS_POINTSIZE, *((DWORD*)&fR));
    D3D_CHECKERROR(hr);

    // Set vertex shader and draw
    d3dSetVertexShader(D3DFVF_CTVERTEX);
    hr = _pGfx->gl_pd3dDevice->DrawPrimitiveUP(D3DPT_POINTLIST, 1, &avtx, sizeof(CTVERTEX));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
}

// Draw one point in 3D
void CDrawPort::DrawPoint3D(FLOAT3D v, COLOR col, FLOAT fRadius/*=1.0f*/) const
{
  // Check API and radius
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  ASSERT(fRadius >= 0);

  if (fRadius == 0) {
    return; // do nothing if radius is 0
  }

  // Setup rendering mode
  gfxDisableTexture();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  // Set point color/alpha
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    glCOLOR(col);
    pglPointSize(fRadius);
    pglBegin(GL_POINTS);
      pglVertex3f(v(1), v(2), v(3));
    pglEnd();
    OGL_CHECKERROR;
  } // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr;
    const ULONG d3dColor = rgba2argb(col);
    CTVERTEX avtx = {v(1), v(2), v(3), d3dColor, 0, 0};
    hr = _pGfx->gl_pd3dDevice->SetRenderState(D3DRS_POINTSIZE, *((DWORD*)&fRadius));
    D3D_CHECKERROR(hr);

    // Set vertex shader and draw
    d3dSetVertexShader(D3DFVF_CTVERTEX);
    hr = _pGfx->gl_pd3dDevice->DrawPrimitiveUP(D3DPT_POINTLIST, 1, &avtx, sizeof(CTVERTEX));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
}

// Draw one line
void CDrawPort::DrawLine(PIX pixI0, PIX pixJ0, PIX pixI1, PIX pixJ1, COLOR col, ULONG typ/*=_FULL*/) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Setup rendering mode
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  FLOAT fD;
  INDEX iTexFilter, iTexAnisotropy;
  if (typ == _FULL_) {
    // no pattern - just disable texturing
    gfxDisableTexture();
    fD = 0;
  } else {
    // Revert to simple point-sample filtering without mipmaps
    INDEX iNewFilter = 10, iNewAnisotropy = 1;
    gfxGetTextureFiltering(iTexFilter, iTexAnisotropy);
    gfxSetTextureFiltering(iNewFilter, iNewAnisotropy);

    // Prepare line pattern and mapping
    extern void gfxSetPattern(ULONG ulPattern);
    gfxSetPattern(typ);
    fD = Max(Abs(pixI0 - pixI1), Abs(pixJ0 - pixJ1)) / 32.0f;
  }

  // Set line color/alpha and go go go
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);

  // OpenGL
  if (eAPI == GAT_OGL) {
    const FLOAT fI0 = pixI0 + 0.5f;  const FLOAT fJ0 = pixJ0 + 0.5f;
    const FLOAT fI1 = pixI1 + 0.5f;  const FLOAT fJ1 = pixJ1 + 0.5f;
    glCOLOR(col);
    pglBegin(GL_LINES);
      pglTexCoord2f(0, 0);  pglVertex2f(fI0, fJ0);
      pglTexCoord2f(fD, 0); pglVertex2f(fI1, fJ1);
    pglEnd();
    OGL_CHECKERROR;
  } // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr;
    const FLOAT fI0 = pixI0 + 0.75f;  const FLOAT fJ0 = pixJ0 + 0.75f;
    const FLOAT fI1 = pixI1 + 0.75f;  const FLOAT fJ1 = pixJ1 + 0.75f;
    const ULONG d3dColor = rgba2argb(col);
    CTVERTEX avtxLine[2] = {
      {fI0, fJ0, 0, d3dColor,  0, 0},
      {fI1, fJ1, 0, d3dColor, fD, 0} };

    // Set vertex shader and draw
    d3dSetVertexShader(D3DFVF_CTVERTEX);
    hr = _pGfx->gl_pd3dDevice->DrawPrimitiveUP(D3DPT_LINELIST, 1, avtxLine, sizeof(CTVERTEX));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
  // Revert to old filtering
  if (typ != _FULL_) {
    gfxSetTextureFiltering(iTexFilter, iTexAnisotropy);
  }
}



// Draw one line in 3D
void CDrawPort::DrawLine3D(FLOAT3D v0, FLOAT3D v1, COLOR col) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // setup rendering mode
  gfxDisableTexture();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  // set line color/alpha and go go go
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);
  // OpenGL
  if (eAPI == GAT_OGL) {
    glCOLOR(col);
    pglBegin(GL_LINES);
      pglVertex3f(v0(1),v0(2),v0(3));
      pglVertex3f(v1(1),v1(2),v1(3));
    pglEnd();
    OGL_CHECKERROR;
  } // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr;
    const ULONG d3dColor = rgba2argb(col);
    CTVERTEX avtxLine[2] = {
      {v0(1), v0(2), v0(3), d3dColor, 0, 0},
      {v1(1), v1(2), v1(3), d3dColor, 0, 0} };

    // Set vertex shader and draw
    d3dSetVertexShader(D3DFVF_CTVERTEX);
    hr = _pGfx->gl_pd3dDevice->DrawPrimitiveUP(D3DPT_LINELIST, 1, avtxLine, sizeof(CTVERTEX));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
}

// Draw border
void CDrawPort::DrawBorder(PIX pixI, PIX pixJ, PIX pixWidth, PIX pixHeight, COLOR col, ULONG typ/*=_FULL_*/) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Setup rendering mode
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  // For non-full lines, must have
  FLOAT fD;
  INDEX iTexFilter, iTexAnisotropy;
  if (typ == _FULL_) {
    // no pattern - just disable texturing
    gfxDisableTexture();
    fD = 0;
  } else {
    // Revert to simple point-sample filtering without mipmaps
    INDEX iNewFilter = 10, iNewAnisotropy = 1;
    gfxGetTextureFiltering(iTexFilter, iTexAnisotropy);
    gfxSetTextureFiltering(iNewFilter, iNewAnisotropy);

    // Prepare line pattern
    extern void gfxSetPattern(ULONG ulPattern);
    gfxSetPattern(typ);
    fD = Max(pixWidth, pixHeight) / 32.0f;
  }

  // Set line color/alpha and go go go
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);
  const FLOAT fI0 = pixI + 0.5f;
  const FLOAT fJ0 = pixJ + 0.5f;
  const FLOAT fI1 = pixI - 0.5f + pixWidth;
  const FLOAT fJ1 = pixJ - 0.5f + pixHeight;

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    glCOLOR(col);
    pglBegin(GL_LINES);
      pglTexCoord2f(0, 0); pglVertex2f(fI0, fJ0);     pglTexCoord2f(fD, 0);  pglVertex2f(fI1,  fJ0);    // up
      pglTexCoord2f(0, 0); pglVertex2f(fI1, fJ0);     pglTexCoord2f(fD, 0);  pglVertex2f(fI1,  fJ1);    // right
      pglTexCoord2f(0, 0); pglVertex2f(fI0, fJ1);     pglTexCoord2f(fD, 0);  pglVertex2f(fI1 + 1, fJ1); // down
      pglTexCoord2f(0, 0); pglVertex2f(fI0, fJ0 + 1); pglTexCoord2f(fD, 0);  pglVertex2f(fI0,  fJ1);    // left
    pglEnd();
    OGL_CHECKERROR;
  }
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr;
    const ULONG d3dColor = rgba2argb(col);
    CTVERTEX avtxLines[8] = { // setup lines
      {fI0, fJ0,  0, d3dColor, 0, 0}, {fI1,  fJ0, 0, d3dColor, fD, 0},      // up
      {fI1, fJ0,  0, d3dColor, 0, 0}, {fI1,  fJ1, 0, d3dColor, fD, 0},      // right
      {fI0, fJ1,  0, d3dColor, 0, 0}, {fI1 + 1, fJ1, 0, d3dColor, fD, 0},   // down
      {fI0, fJ0 + 1, 0, d3dColor, 0, 0}, {fI0,  fJ1, 0, d3dColor, fD, 0} }; // left

    // Set vertex shader and draw
    d3dSetVertexShader(D3DFVF_CTVERTEX);
    hr = _pGfx->gl_pd3dDevice->DrawPrimitiveUP(D3DPT_LINELIST, 4, avtxLines, sizeof(CTVERTEX));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
  // Revert to old filtering
  if (typ != _FULL_) {
    gfxSetTextureFiltering(iTexFilter, iTexAnisotropy);
  }
}

// Fill part of a drawport with a given color
void CDrawPort::Fill(PIX pixI, PIX pixJ, PIX pixWidth, PIX pixHeight, COLOR col) const
{
  // If color is tranlucent
  if (((col & CT_AMASK) >> CT_ASHIFT) != CT_OPAQUE)
  { // Draw thru polygon
    Fill(pixI, pixJ, pixWidth, pixHeight, col, col, col, col);
    return;
  }

  // Clip and eventually reject
  const BOOL bInside = ClipToDrawPort(this, pixI, pixJ, pixWidth, pixHeight);
  if (!bInside) {
    return;
  }

  // Draw thru fast clear for opaque colors
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);

  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    // Do fast filling
    SetScissor(this, pixI, pixJ, pixWidth, pixHeight);
    UBYTE ubR, ubG, ubB;
    ColorToRGB(col, ubR, ubG, ubB);
    pglClearColor(ubR / 255.0f, ubG / 255.0f, ubB / 255.0f, 1.0f);
    pglClear(GL_COLOR_BUFFER_BIT);
    ResetScissor(this);
  }
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr;
    // must convert coordinates to raster (i.e. surface)
    pixI += dp_MinI;
    pixJ += dp_MinJ;
    const PIX pixRasterW = dp_Raster->ra_Width;
    const PIX pixRasterH = dp_Raster->ra_Height;
    const ULONG d3dColor = rgba2argb(col);
    // do fast filling
    if (pixI == 0 && pixJ == 0 && pixWidth == pixRasterW && pixHeight == pixRasterH) {
      hr = _pGfx->gl_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET, d3dColor, 0, 0);
    } else {
      D3DRECT d3dRect = { pixI, pixJ, pixI+pixWidth, pixJ+pixHeight };
      hr = _pGfx->gl_pd3dDevice->Clear(1, &d3dRect, D3DCLEAR_TARGET, d3dColor, 0, 0);
    } // done
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
}

// Fill part of a drawport with a four corner colors
void CDrawPort::Fill(PIX pixI, PIX pixJ, PIX pixWidth, PIX pixHeight,
                     COLOR colUL, COLOR colUR, COLOR colDL, COLOR colDR) const
{
  // Clip and eventually reject
  const BOOL bInside = ClipToDrawPort(this, pixI, pixJ, pixWidth, pixHeight);
  if (!bInside) {
    return;
  }

  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Setup rendering mode
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
  gfxDisableAlphaTest();
  gfxDisableTexture();

  // Prepare colors and coords
  colUL = AdjustColor(colUL, _slTexHueShift, _slTexSaturation);
  colUR = AdjustColor(colUR, _slTexHueShift, _slTexSaturation);
  colDL = AdjustColor(colDL, _slTexHueShift, _slTexSaturation);
  colDR = AdjustColor(colDR, _slTexHueShift, _slTexSaturation);
  const FLOAT fI0 = pixI;  const FLOAT fI1 = pixI + pixWidth;
  const FLOAT fJ0 = pixJ;  const FLOAT fJ1 = pixJ + pixHeight;

  // Render rectangle
  if (eAPI == GAT_OGL) {
    // Thru OpenGL
    gfxResetArrays();
    GFXVertex   *pvtx = _avtxCommon.Push(4);
    GFXTexCoord *ptex = _atexCommon.Push(4);
    GFXColor    *pcol = _acolCommon.Push(4);
    const GFXColor glcolUL(colUL);  const GFXColor glcolUR(colUR);
    const GFXColor glcolDL(colDL);  const GFXColor glcolDR(colDR);

    // Add to element list and flush (the toilet!:)
    pvtx[0].x = fI0;  pvtx[0].y = fJ0;  pvtx[0].z = 0;  pcol[0] = glcolUL;
    pvtx[1].x = fI0;  pvtx[1].y = fJ1;  pvtx[1].z = 0;  pcol[1] = glcolDL;
    pvtx[2].x = fI1;  pvtx[2].y = fJ1;  pvtx[2].z = 0;  pcol[2] = glcolDR;
    pvtx[3].x = fI1;  pvtx[3].y = fJ0;  pvtx[3].z = 0;  pcol[3] = glcolUR;
    gfxFlushQuads();
  }
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    // Thru Direct3D
    HRESULT hr;
    const ULONG d3dColUL = rgba2argb(colUL);  const ULONG d3dColUR = rgba2argb(colUR);
    const ULONG d3dColDL = rgba2argb(colDL);  const ULONG d3dColDR = rgba2argb(colDR);
    CTVERTEX avtxTris[6] = {
      {fI0, fJ0, 0, d3dColUL, 0, 0}, {fI0, fJ1, 0, d3dColDL, 0, 1}, {fI1, fJ1, 0, d3dColDR, 1, 1},
      {fI0, fJ0, 0, d3dColUL, 0, 0}, {fI1, fJ1, 0, d3dColDR, 1, 1}, {fI1, fJ0, 0, d3dColUR, 1, 0} };

    // Set vertex shader and draw
    d3dSetVertexShader(D3DFVF_CTVERTEX);
    hr = _pGfx->gl_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, 2, avtxTris, sizeof(CTVERTEX));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8
}


// Fill an entire drawport with a given color
void CDrawPort::Fill(COLOR col) const
{
  // If color is tranlucent
  if (((col&CT_AMASK)>>CT_ASHIFT) != CT_OPAQUE)
  {
    // Draw thru polygon
    Fill(0, 0, GetWidth(), GetHeight(), col, col, col, col);
    return;
  }

  // Draw thru fast clear for opaque colors
  col = AdjustColor(col, _slTexHueShift, _slTexSaturation);

  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    // Do fast filling
    UBYTE ubR, ubG, ubB;
    ColorToRGB(col, ubR,ubG,ubB);
    pglClearColor(ubR / 255.0f, ubG / 255.0f, ubB / 255.0f, 1.0f);
    pglClear(GL_COLOR_BUFFER_BIT);
  }
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    const ULONG d3dColor = rgba2argb(col);
    HRESULT hr = _pGfx->gl_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET, d3dColor, 0, 0);
    D3D_CHECKERROR(hr);
  }
#endif SE1_D3D8
}

// Fill a part of Z-Buffer with a given value
void CDrawPort::FillZBuffer(PIX pixI, PIX pixJ, PIX pixWidth, PIX pixHeight, FLOAT zval) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // clip and eventually reject
  const BOOL bInside = ClipToDrawPort(this, pixI, pixJ, pixWidth, pixHeight);
  if (!bInside) {
    return;
  }

  gfxEnableDepthWrite();

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    // Fast clearing thru scissor
    SetScissor(this, pixI, pixJ, pixWidth, pixHeight);
    pglClearDepth(zval);
    pglClearStencil(0);

    // Must clear stencil buffer too in case it exist (we don't need it) for the performance sake
    pglClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    ResetScissor(this);
    OGL_CHECKERROR;
  }
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8)
  {
    D3DRECT d3dRect = { pixI, pixJ, pixI + pixWidth, pixJ + pixHeight };
    HRESULT hr = _pGfx->gl_pd3dDevice->Clear(1, &d3dRect, D3DCLEAR_ZBUFFER, 0, zval, 0);
    D3D_CHECKERROR(hr);
  }
#endif SE1_D3D8
}

// Fill an entire Z-Buffer with a given value
void CDrawPort::FillZBuffer(FLOAT zval) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  gfxEnableDepthWrite();

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    // Fill whole z-buffer
    pglClearDepth(zval);
    pglClearStencil(0);
    pglClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  }
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    HRESULT hr = _pGfx->gl_pd3dDevice->Clear(0, NULL, D3DCLEAR_ZBUFFER, 0, zval, 0);
    D3D_CHECKERROR(hr);
  }
#endif SE1_D3D8
}

// Grab screen
void CDrawPort::GrabScreen(class CImageInfo &iiGrabbedImage, INDEX iGrabZBuffer/*=0*/) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  extern INDEX ogl_bGrabDepthBuffer;
  const BOOL bGrabDepth = eAPI == GAT_OGL && ((iGrabZBuffer == 1 && ogl_bGrabDepthBuffer) || iGrabZBuffer == 2);

  // Prepare image info's dimensions
  iiGrabbedImage.Clear();
  iiGrabbedImage.ii_Width  = GetWidth();
  iiGrabbedImage.ii_Height = GetHeight();
  iiGrabbedImage.ii_BitsPerPixel = bGrabDepth ? 32 : 24;

  // Allocate memory for 24-bit raw picture and copy buffer context
  const PIX pixPicSize = iiGrabbedImage.ii_Width * iiGrabbedImage.ii_Height;
  const SLONG slBytes  = pixPicSize * iiGrabbedImage.ii_BitsPerPixel / 8;
  iiGrabbedImage.ii_Picture = (UBYTE*)AllocMemory(slBytes);
  memset(iiGrabbedImage.ii_Picture, 128, slBytes);

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    // Determine drawport starting location inside raster
    const PIX pixStartI = dp_MinI;
    const PIX pixStartJ = dp_Raster->ra_Height-(dp_MinJ + GetHeight());
    pglReadPixels(pixStartI, pixStartJ, GetWidth(), GetHeight(), GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)iiGrabbedImage.ii_Picture);
    OGL_CHECKERROR;

    // Grab z-buffer to alpha channel, if needed
    if (bGrabDepth)
    {
      // Grab
      FLOAT *pfZBuffer = (FLOAT*)AllocMemory(pixPicSize * sizeof(FLOAT));
      pglReadPixels(pixStartI, pixStartJ, GetWidth(), GetHeight(), GL_DEPTH_COMPONENT, GL_FLOAT, (GLvoid*)pfZBuffer);
      OGL_CHECKERROR;

      // Convert
      UBYTE *pubZBuffer = (UBYTE*)pfZBuffer;
      for (INDEX i = 0; i < pixPicSize; i++) {
        pubZBuffer[i] = 255 - NormFloatToByte(pfZBuffer[i]);
      }

      // Add as alpha channel
      AddAlphaChannel(iiGrabbedImage.ii_Picture, (ULONG*)iiGrabbedImage.ii_Picture,
                      iiGrabbedImage.ii_Width * iiGrabbedImage.ii_Height, pubZBuffer);
      FreeMemory(pfZBuffer);
    }

    // Flip image vertically
    FlipBitmap(iiGrabbedImage.ii_Picture, iiGrabbedImage.ii_Picture,
               iiGrabbedImage.ii_Width, iiGrabbedImage.ii_Height, 1, iiGrabbedImage.ii_BitsPerPixel == 32);
  }

  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    // Get back buffer
    HRESULT hr;
    D3DLOCKED_RECT rectLocked;
    D3DSURFACE_DESC surfDesc;
    LPDIRECT3DSURFACE8 pBackBuffer;
    const BOOL bFullScreen = _pGfx->gl_ulFlags & GLF_FULLSCREEN;

    if (bFullScreen) {
      hr = _pGfx->gl_pd3dDevice->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
    } else {
      hr = dp_Raster->ra_pvpViewPort->vp_pSwapChain->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
    }

    D3D_CHECKERROR(hr);
    pBackBuffer->GetDesc(&surfDesc);
    ASSERT(surfDesc.Width == dp_Raster->ra_Width && surfDesc.Height == dp_Raster->ra_Height);
    const RECT rectToLock = { dp_MinI, dp_MinJ, dp_MaxI + 1, dp_MaxJ + 1 };
    hr = pBackBuffer->LockRect(&rectLocked, &rectToLock, D3DLOCK_READONLY);
    D3D_CHECKERROR(hr);

    // Prepare to copy'n'convert
    SLONG slColSize;
    UBYTE *pubSrc = (UBYTE*)rectLocked.pBits;
    UBYTE *pubDst = iiGrabbedImage.ii_Picture;

    // Loop thru rows
    for (INDEX j = 0; j < GetHeight(); j++)
    {
      // Loop thru pixles in row
      for (INDEX i = 0; i < GetWidth(); i++)
      {
        UBYTE ubR,ubG,ubB;
        extern COLOR UnpackColor_D3D(UBYTE *pd3dColor, D3DFORMAT d3dFormat, SLONG &slColorSize);
        COLOR col = UnpackColor_D3D(pubSrc, surfDesc.Format, slColSize);
        ColorToRGB(col, ubR, ubG, ubB);
        *pubDst++ = ubR;
        *pubDst++ = ubG;
        *pubDst++ = ubB;
        pubSrc += slColSize;
      }
      // Advance modulo
      pubSrc += rectLocked.Pitch - (GetWidth() * slColSize);
    }
    // All done
    pBackBuffer->UnlockRect();
    D3DRELEASE(pBackBuffer, TRUE);
  }
#endif // SE1_D3D8
}

//! Functions for getting depth of points on drawport.
BOOL CDrawPort::IsPointVisible(PIX pixI, PIX pixJ, FLOAT fOoK, INDEX iID, INDEX iMirrorLevel/*=0*/) const
{
  // Must have raster!
  if (dp_Raster == NULL) {
    ASSERT(FALSE);
    return FALSE;
  }

  // If the point is out or at the edge of drawport, it is not visible by default
  if (pixI < 1 || pixI > GetWidth() - 2 ||
      pixJ < 1 || pixJ > GetHeight() - 2) {
    return FALSE;
  }

  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Use delayed mechanism for checking
  extern BOOL CheckDepthPoint(const CDrawPort *pdp, PIX pixI, PIX pixJ, FLOAT fOoK, INDEX iID, INDEX iMirrorLevel = 0);
  return CheckDepthPoint(this, pixI, pixJ, fOoK, iID, iMirrorLevel);
}

//! Render one lens flare.
void CDrawPort::RenderLensFlare(CTextureObject *pto, FLOAT fI, FLOAT fJ,
                                FLOAT fSizeI, FLOAT fSizeJ, ANGLE aRotation, COLOR colLight) const
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Setup rendering mode
  gfxEnableDepthTest();
  gfxDisableDepthWrite();
  gfxEnableBlend();
  gfxBlendFunc(GFX_ONE, GFX_ONE);
  gfxDisableAlphaTest();
  gfxResetArrays();
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);

  // Find lens location and dimension
  const FLOAT fRI   = fSizeI * 0.5f;
  const FLOAT fRJ   = fSizeJ * 0.5f;
  const FLOAT fSinA = SinFast(aRotation);
  const FLOAT fCosA = CosFast(aRotation);
  const FLOAT fRICosA = fRI * +fCosA;
  const FLOAT fRJSinA = fRJ * -fSinA;
  const FLOAT fRISinA = fRI * +fSinA;
  const FLOAT fRJCosA = fRJ * +fCosA;

  // Get texture parameters for current frame and needed mip factor and upload texture
  CTextureData *ptd = (CTextureData*)pto->GetData();
  ptd->SetAsCurrent(pto->GetFrame());

  // Set lens color
  colLight = AdjustColor(colLight, _slShdHueShift, _slShdSaturation);
  const GFXColor glcol(colLight);

  // Prepare coordinates of the rectangle
  pvtx[0].x = fI - fRICosA + fRJSinA;  pvtx[0].y = fJ - fRISinA + fRJCosA;  pvtx[0].z = 0.01f;
  pvtx[1].x = fI - fRICosA - fRJSinA;  pvtx[1].y = fJ - fRISinA - fRJCosA;  pvtx[1].z = 0.01f;
  pvtx[2].x = fI + fRICosA - fRJSinA;  pvtx[2].y = fJ + fRISinA - fRJCosA;  pvtx[2].z = 0.01f;
  pvtx[3].x = fI + fRICosA + fRJSinA;  pvtx[3].y = fJ + fRISinA + fRJCosA;  pvtx[3].z = 0.01f;
  ptex[0].s = 0;  ptex[0].t = 0;
  ptex[1].s = 0;  ptex[1].t = 1;
  ptex[2].s = 1;  ptex[2].t = 1;
  ptex[3].s = 1;  ptex[3].t = 0;
  pcol[0] = glcol;
  pcol[1] = glcol;
  pcol[2] = glcol;
  pcol[3] = glcol;
  // Render it
  _pGfx->gl_ctWorldTriangles += 2;
  gfxFlushQuads();
}


/////////////////////////////////////////////////////////////////////////////////////////////////
// Routines for manipulating drawport's text capabilites

// Sets font to be used to printout some text on this drawport
// WARNING: this resets text spacing, scaling and mode variables
void CDrawPort::SetFont(CFontData *pfd)
{
  // Check if we're using font that's not even loaded yet
  ASSERT(pfd != NULL);
  dp_FontData = pfd;
  dp_pixTextCharSpacing = pfd->fd_pixCharSpacing;
  dp_pixTextLineSpacing = pfd->fd_pixLineSpacing;
  dp_fTextScaling = 1.0f;
  dp_fTextAspect  = 1.0f;
  dp_iTextMode    = 1;
};


// Returns width of the longest line in text string
ULONG CDrawPort::GetTextWidth(const CTString &strText) const
{
  // Prepare scaling factors
  PIX   pixCellWidth    = dp_FontData->fd_pixCharWidth;
  SLONG fixTextScalingX = FloatToInt(dp_fTextScaling * dp_fTextAspect * 65536.0f);

  // calculate width of entire text line
  PIX pixStringWidth = 0, pixOldWidth = 0;
  PIX pixCharStart = 0, pixCharEnd = pixCellWidth;
  INDEX ctCharsPrinted = 0;
  for (INDEX i = 0; i < (INDEX)strlen(strText); i++)
  {
    // Get current letter
    unsigned char chrCurrent = strText[i];

    // Next line situation?
    if (chrCurrent == '\n')
    {
      if (pixOldWidth < pixStringWidth) {
        pixOldWidth = pixStringWidth;
      }
      pixStringWidth = 0;
      continue;
    }
    // Special char encountered and allowed?
    else if (chrCurrent == '^' && dp_iTextMode != -1) {
      // get next char
      chrCurrent = strText[++i];
      switch (chrCurrent)
      {
        // skip corresponding number of characters
        case 'c':  i += FindZero((UBYTE*)&strText[i], 6);  continue;
        case 'a':  i += FindZero((UBYTE*)&strText[i], 2);  continue;
        case 'f':  i += 1;  continue;
        case 'b':  case 'i':  case 'r':  case 'o':
        case 'C':  case 'A':  case 'F':  case 'B':  case 'I':  i+=0;  continue;
        default:   break; // if we get here this means that ^ or an unrecognized special code was specified
      }
    } else if (chrCurrent == '\t') {
      continue;    // ignore tab
    }

    // Add current letter's width to result width
    if (!dp_FontData->fd_bFixedWidth) {
      // Proportional font case
      pixCharStart = dp_FontData->fd_fcdFontCharData[chrCurrent].fcd_pixStart;
      pixCharEnd   = dp_FontData->fd_fcdFontCharData[chrCurrent].fcd_pixEnd;
    }
    pixStringWidth += (((pixCharEnd - pixCharStart) * fixTextScalingX) >> 16) + dp_pixTextCharSpacing;
    ctCharsPrinted++;
  }
  // Determine largest width
  if (pixStringWidth < pixOldWidth) {
    pixStringWidth = pixOldWidth;
  }
  return pixStringWidth;
}

// Writes text string on drawport (left aligned if not forced otherwise)
void CDrawPort::PutText(const CTString &strText, PIX pixX0, PIX pixY0, const COLOR colBlend) const
{
  // Check API and adjust position for D3D by half pixel
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Skip drawing if text falls above or below draw port
  if (pixY0 > GetHeight() || pixX0 > GetWidth()) {
    return;
  }

  _pfGfxProfile->StartTimer(CGfxProfile::PTI_PUTTEXT);
  char acTmp[7]; // needed for strtoul()
  char *pcDummy;
  INDEX iRet;

  // Cache char and texture dimensions
  const FLOAT fTextScalingX   = dp_fTextScaling * dp_fTextAspect;
  const SLONG fixTextScalingX = FloatToInt(fTextScalingX   * 65536.0f);
  const SLONG fixTextScalingY = FloatToInt(dp_fTextScaling * 65536.0f);
  const PIX pixCellWidth  = dp_FontData->fd_pixCharWidth;
  const PIX pixCharHeight = dp_FontData->fd_pixCharHeight - 1;
  const PIX pixScaledWidth  = (pixCellWidth   * fixTextScalingX) >> 16;
  const PIX pixScaledHeight = (pixCharHeight  * fixTextScalingY) >> 16;

  // Prepare font texture
  gfxSetTextureWrapping(GFX_REPEAT, GFX_REPEAT);
  CTextureData &td = *dp_FontData->fd_ptdTextureData;
  td.SetAsCurrent();

  // Setup rendering mode
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  // Calculate and apply correction factor
  FLOAT fCorrectionU = 1.0f / td.GetPixWidth();
  FLOAT fCorrectionV = 1.0f / td.GetPixHeight();
  INDEX ctMaxChars = (INDEX)strlen(strText);

  // Determine text color
  GFXColor glcolDefault(AdjustColor(colBlend, _slTexHueShift, _slTexSaturation));
  GFXColor glcol = glcolDefault;
  ULONG ulAlphaDefault = (colBlend & CT_AMASK) >> CT_ASHIFT;;  // for flasher
  ASSERT(dp_iTextMode == -1 || dp_iTextMode == 0 || dp_iTextMode == +1);

  // Prepare some text control and output vars
  INDEX ctCharsPrinted = 0, ctBoldsPrinted = 0;
  BOOL bBold    = FALSE;
  BOOL bItalic  = FALSE;
  INDEX iFlash  = 0;
  ULONG ulAlpha = ulAlphaDefault;
  TIME tmFrame  = _pGfx->gl_tvFrameTime.GetSeconds();
  BOOL bParse = dp_iTextMode == 1;

  // Prepare arrays
  gfxResetArrays();
  GFXVertex   *pvtx = _avtxCommon.Push(2 * ctMaxChars * 4);  // 2* because of bold
  GFXTexCoord *ptex = _atexCommon.Push(2 * ctMaxChars * 4);
  GFXColor    *pcol = _acolCommon.Push(2 * ctMaxChars * 4);

  // Loop thru chars
  PIX pixAdvancer = ((pixCellWidth*fixTextScalingX)>>16) +dp_pixTextCharSpacing;
  PIX pixStartX = pixX0;
  for (INDEX iChar = 0; iChar < ctMaxChars; iChar++)
  {
    // Get current char
    unsigned char chrCurrent = strText[iChar];
    // If at end of current line
    if (chrCurrent == '\n')
    {
      // Advance to next line
      pixX0  = pixStartX;
      pixY0 += pixScaledHeight+dp_pixTextLineSpacing;
      if (pixY0 > GetHeight()) {
        break;
      }

      // Skip to next char
      continue;
    // Special char encountered and allowed?
    } else if (chrCurrent == '^' && dp_iTextMode != -1) {
      // Get next char
      chrCurrent = strText[++iChar];
      COLOR col;
      switch (chrCurrent)
      {
        // Color change?
        case 'c':
          strncpy(acTmp, &strText[iChar + 1], 6);
          iRet = FindZero((UBYTE*)&strText[iChar + 1], 6);
          iChar += iRet;
          if (!bParse || iRet < 6) {
            continue;
          }
          acTmp[6] = '\0'; // terminate string
          col = strtoul(acTmp, &pcDummy, 16) << 8;
          col = AdjustColor(col, _slTexHueShift, _slTexSaturation);
          glcol.Set(col | glcol.a); // do color change but keep original alpha
          continue;

        // Alpha change?
        case 'a':
          strncpy(acTmp, &strText[iChar + 1], 2);
          iRet = FindZero((UBYTE*)&strText[iChar + 1], 2);
          iChar += iRet;
          if (!bParse || iRet < 2) {
            continue;
          }
          acTmp[2] = '\0'; // terminate string
          ulAlpha = strtoul(acTmp, &pcDummy, 16);
          continue;

        // Flash?
        case 'f':
          chrCurrent = strText[++iChar];
          if (bParse) {
            iFlash = 1 + 2 * Clamp((INDEX)(chrCurrent - '0'), 0L, 9L);
          }
          continue;

        // Reset all?
        case 'r':
          bBold   = FALSE;
          bItalic = FALSE;
          iFlash  = 0;
          glcol   = glcolDefault;
          ulAlpha = ulAlphaDefault;
          continue;

        // Simple codes ...
        case 'o':  bParse = bParse && gfx_bDecoratedText;  continue;  // allow console override settings?
        case 'b':  if (bParse) bBold   = TRUE;  continue;  // bold?
        case 'i':  if (bParse) bItalic = TRUE;  continue;  // italic?
        case 'C':  glcol   = glcolDefault;      continue;  // color reset?
        case 'A':  ulAlpha = ulAlphaDefault;    continue;  // alpha reset?
        case 'B':  bBold   = FALSE;             continue;  // no bold?
        case 'I':  bItalic = FALSE;             continue;  // italic?
        case 'F':  iFlash  = 0;                 continue;  // no flash?

        default:   break;
      } // unrecognized special code or just plain ^

      if (chrCurrent != '^')
      {
        iChar--; break;
      }

    // Ignore tab
    } else if (chrCurrent == '\t') {
      continue;
    }

    // Det current location and dimensions
    const CFontCharData &fcdCurrent = dp_FontData->fd_fcdFontCharData[chrCurrent];
    const PIX pixCharX = fcdCurrent.fcd_pixXOffset;
    const PIX pixCharY = fcdCurrent.fcd_pixYOffset;
    const PIX pixCharStart = fcdCurrent.fcd_pixStart;
    const PIX pixCharEnd   = fcdCurrent.fcd_pixEnd;
    PIX pixXA; // adjusted starting X location of printout

    // Determine corresponding char width and position adjustments
    if (dp_FontData->fd_bFixedWidth) {
      // For fixed font
      pixXA = pixX0 - ((pixCharStart * fixTextScalingX) >> 16)
              + (((pixScaledWidth << 16) - ((pixCharEnd - pixCharStart) * fixTextScalingX) + 0x10000) >> 17);
    } else {
      // For proportional font
      pixXA = pixX0 - ((pixCharStart * fixTextScalingX) >> 16);
      pixAdvancer = (((pixCharEnd - pixCharStart) * fixTextScalingX) >> 16) + dp_pixTextCharSpacing;
    }

    // Out of screen (left) ?
    if (pixXA > GetWidth() || (pixXA + pixCharEnd) < 0) {
      // Skip to next char
      pixX0 += pixAdvancer;
      continue;
    }

    // Adjust alpha for flashing
    if (iFlash > 0) {
      glcol.a = ulAlpha*(sin(iFlash*tmFrame)*0.5f+0.5f);
    } else {
      glcol.a = ulAlpha;
    }

    // Prepare coordinates for screen and texture
    const FLOAT fX0 = pixXA;  const FLOAT fX1 = fX0 + pixScaledWidth;
    const FLOAT fY0 = pixY0;  const FLOAT fY1 = fY0 + pixScaledHeight;
    const FLOAT fU0 = pixCharX * fCorrectionU;  const FLOAT fU1 = (pixCharX + pixCellWidth)  * fCorrectionU;
    const FLOAT fV0 = pixCharY * fCorrectionV;  const FLOAT fV1 = (pixCharY + pixCharHeight) * fCorrectionV;
    pvtx[0].x = fX0;  pvtx[0].y = fY0;  pvtx[0].z = 0;
    pvtx[1].x = fX0;  pvtx[1].y = fY1;  pvtx[1].z = 0;
    pvtx[2].x = fX1;  pvtx[2].y = fY1;  pvtx[2].z = 0;
    pvtx[3].x = fX1;  pvtx[3].y = fY0;  pvtx[3].z = 0;
    ptex[0].s = fU0;  ptex[0].t = fV0;
    ptex[1].s = fU0;  ptex[1].t = fV1;
    ptex[2].s = fU1;  ptex[2].t = fV1;
    ptex[3].s = fU1;  ptex[3].t = fV0;
    pcol[0] = glcol;
    pcol[1] = glcol;
    pcol[2] = glcol;
    pcol[3] = glcol;

    // Adjust for italic
    if (bItalic) {
      const FLOAT fAdjustX = fTextScalingX * (fY1 - fY0) * 0.2f;  // 20% slanted
      pvtx[0].x += fAdjustX;
      pvtx[3].x += fAdjustX;
    }

    // Advance to next vetrices group
    pvtx += 4;
    ptex += 4;
    pcol += 4;

    // Add bold char
    if (bBold)
    {
      const FLOAT fAdjustX = fTextScalingX * ((FLOAT)pixCellWidth) * 0.1f;  // 10% fat (extra light mayonnaise:)
      pvtx[0].x = pvtx[0-4].x + fAdjustX;  pvtx[0].y = fY0;  pvtx[0].z = 0;
      pvtx[1].x = pvtx[1-4].x + fAdjustX;  pvtx[1].y = fY1;  pvtx[1].z = 0;
      pvtx[2].x = pvtx[2-4].x + fAdjustX;  pvtx[2].y = fY1;  pvtx[2].z = 0;
      pvtx[3].x = pvtx[3-4].x + fAdjustX;  pvtx[3].y = fY0;  pvtx[3].z = 0;
      ptex[0].s = fU0;    ptex[0].t = fV0;
      ptex[1].s = fU0;    ptex[1].t = fV1;
      ptex[2].s = fU1;    ptex[2].t = fV1;
      ptex[3].s = fU1;    ptex[3].t = fV0;
      pcol[0] = glcol;
      pcol[1] = glcol;
      pcol[2] = glcol;
      pcol[3] = glcol;
      pvtx += 4;
      ptex += 4;
      pcol += 4;
      ctBoldsPrinted++;
    }

    // Advance to next char
    pixX0 += pixAdvancer;
    ctCharsPrinted++;
  }

  // Adjust vertex arrays size according to chars that really got printed out
  ctCharsPrinted += ctBoldsPrinted;
  _avtxCommon.PopUntil(ctCharsPrinted * 4 - 1);
  _atexCommon.PopUntil(ctCharsPrinted * 4 - 1);
  _acolCommon.PopUntil(ctCharsPrinted * 4 - 1);
  gfxFlushQuads();

  // All done
  _pfGfxProfile->StopTimer(CGfxProfile::PTI_PUTTEXT);
}


// Writes text string on drawport (centered arround X)
void CDrawPort::PutTextC(const CTString &strText, PIX pixX0, PIX pixY0, const COLOR colBlend/*=0xFFFFFFFF*/) const
{
  PutText(strText, pixX0-GetTextWidth(strText) / 2, pixY0, colBlend);
}

// Writes text string on drawport (centered arround X and Y)
void CDrawPort::PutTextCXY(const CTString &strText, PIX pixX0, PIX pixY0, const COLOR colBlend/*=0xFFFFFFFF*/) const
{
  PIX pixTextWidth  = GetTextWidth(strText);
  PIX pixTextHeight = dp_FontData->fd_pixCharHeight * dp_fTextScaling;
  PutText(strText, pixX0-pixTextWidth / 2, pixY0-pixTextHeight / 2, colBlend);
}

// Writes text string on drawport (right-aligned)
void CDrawPort::PutTextR(const CTString &strText, PIX pixX0, PIX pixY0, const COLOR colBlend/*=0xFFFFFFFF*/) const
{
  PutText(strText, pixX0 - GetTextWidth(strText), pixY0, colBlend);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Routines for putting and getting textures strictly in 2D

//! Plain texture display.
void CDrawPort::PutTexture(class CTextureObject *pTO, const PIXaabbox2D &boxScreen,
                           const COLOR colBlend/*=0xFFFFFFFF*/) const
{
  PutTexture(pTO, boxScreen, colBlend, colBlend, colBlend, colBlend);
}

//! Texture display with trimmed sides.
void CDrawPort::PutTexture(class CTextureObject *pTO, const PIXaabbox2D &boxScreen,
                           const MEXaabbox2D &boxTexture, const COLOR colBlend/*=0xFFFFFFFF*/) const
{
  PutTexture(pTO, boxScreen, boxTexture, colBlend, colBlend, colBlend, colBlend);
}

//! Texture display with different colors for each corner.
void CDrawPort::PutTexture(class CTextureObject *pTO, const PIXaabbox2D &boxScreen,
                           const COLOR colUL, const COLOR colUR, const COLOR colDL, const COLOR colDR) const
{
  MEXaabbox2D boxTexture(MEX2D(0, 0), MEX2D(pTO->GetWidth(), pTO->GetHeight()));
  PutTexture(pTO, boxScreen, boxTexture, colUL, colUR, colDL, colDR);
}

//! Texture display with trimmed sides and different colors for each corner.
void CDrawPort::PutTexture(class CTextureObject *pTO, const PIXaabbox2D &boxScreen, const MEXaabbox2D &boxTexture,
                           const COLOR colUL, const COLOR colUR, const COLOR colDL, const COLOR colDR) const
{
  _pfGfxProfile->StartTimer(CGfxProfile::PTI_PUTTEXTURE);

  // Extract screen and texture coordinates
  const PIX pixI0 = boxScreen.Min()(1);  const PIX pixI1 = boxScreen.Max()(1);
  const PIX pixJ0 = boxScreen.Min()(2);  const PIX pixJ1 = boxScreen.Max()(2);

  // If whole texture is out of drawport then skip it (just to reduce OpenGL call overhead)
  if (pixI0 > GetWidth() || pixJ0 > GetHeight() || pixI1 < 0 || pixJ1 < 0) {
    _pfGfxProfile->StopTimer(CGfxProfile::PTI_PUTTEXTURE);
    return;
  }

  // Check API and adjust position for D3D by half pixel
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  FLOAT fI0 = pixI0;  FLOAT fI1 = pixI1;
  FLOAT fJ0 = pixJ0;  FLOAT fJ1 = pixJ1;

  // Prepare texture
  gfxSetTextureWrapping(GFX_REPEAT, GFX_REPEAT);
  CTextureData *ptd = (CTextureData*)pTO->GetData();
  ptd->SetAsCurrent(pTO->GetFrame());

  // Setup rendering mode
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
  gfxResetArrays();
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);

  // Extract texture coordinates and apply correction factor
  const PIX pixWidth  = ptd->GetPixWidth();
  const PIX pixHeight = ptd->GetPixHeight();
  FLOAT fCorrectionU, fCorrectionV;
  (SLONG&)fCorrectionU = (127 - FastLog2(pixWidth))  << 23; // fCorrectionU = 1.0f / ptd->GetPixWidth()
  (SLONG&)fCorrectionV = (127 - FastLog2(pixHeight)) << 23; // fCorrectionV = 1.0f / ptd->GetPixHeight()
  FLOAT fU0 = (boxTexture.Min()(1) >> ptd->td_iFirstMipLevel) * fCorrectionU;
  FLOAT fU1 = (boxTexture.Max()(1) >> ptd->td_iFirstMipLevel) * fCorrectionU;
  FLOAT fV0 = (boxTexture.Min()(2) >> ptd->td_iFirstMipLevel) * fCorrectionV;
  FLOAT fV1 = (boxTexture.Max()(2) >> ptd->td_iFirstMipLevel) * fCorrectionV;

  // If not tiled
  const BOOL bTiled = Abs(fU0 - fU1) > 1 || Abs(fV0 - fV1) > 1;
  if (!bTiled) {
    // Slight adjust for sub-pixel precision
    fU0 += +0.25f * fCorrectionU;
    fU1 += -0.25f * fCorrectionU;
    fV0 += +0.25f * fCorrectionV;
    fV1 += -0.25f * fCorrectionV;
  }

  // Prepare colors
  const GFXColor glcolUL(AdjustColor(colUL, _slTexHueShift, _slTexSaturation));
  const GFXColor glcolUR(AdjustColor(colUR, _slTexHueShift, _slTexSaturation));
  const GFXColor glcolDL(AdjustColor(colDL, _slTexHueShift, _slTexSaturation));
  const GFXColor glcolDR(AdjustColor(colDR, _slTexHueShift, _slTexSaturation));

  // Prepare coordinates of the rectangle
  pvtx[0].x = fI0;  pvtx[0].y = fJ0;  pvtx[0].z = 0;
  pvtx[1].x = fI0;  pvtx[1].y = fJ1;  pvtx[1].z = 0;
  pvtx[2].x = fI1;  pvtx[2].y = fJ1;  pvtx[2].z = 0;
  pvtx[3].x = fI1;  pvtx[3].y = fJ0;  pvtx[3].z = 0;
  ptex[0].s = fU0;  ptex[0].t = fV0;
  ptex[1].s = fU0;  ptex[1].t = fV1;
  ptex[2].s = fU1;  ptex[2].t = fV1;
  ptex[3].s = fU1;  ptex[3].t = fV0;
  pcol[0] = glcolUL;
  pcol[1] = glcolDL;
  pcol[2] = glcolDR;
  pcol[3] = glcolUR;
  gfxFlushQuads();
  _pfGfxProfile->StopTimer(CGfxProfile::PTI_PUTTEXTURE);
}


// Prepares texture and rendering arrays
void CDrawPort::InitTexture(class CTextureObject *pTO, const BOOL bClamp/*=FALSE*/) const
{
  // Prepare
  if (pTO != NULL)
  {
    // Has texture
    CTextureData *ptd = (CTextureData*)pTO->GetData();
    GfxWrap eWrap = GFX_REPEAT;
    if (bClamp) {
      eWrap = GFX_CLAMP;
    }
    gfxSetTextureWrapping(eWrap, eWrap);
    ptd->SetAsCurrent(pTO->GetFrame());
  } else {
    // No texture
    gfxDisableTexture();
  }

  // Setup rendering mode
  gfxDisableDepthTest();
  gfxDisableDepthWrite();
  gfxDisableAlphaTest();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);

  // Prepare arrays
  gfxResetArrays();
}


// Adds one full texture to rendering queue
void CDrawPort::AddTexture(const FLOAT fI0, const FLOAT fJ0, const FLOAT fI1, const FLOAT fJ1, const COLOR col) const
{
  const GFXColor glCol(AdjustColor(col, _slTexHueShift, _slTexSaturation));
  const INDEX iStart = _avtxCommon.Count();
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);
  INDEX       *pelm = _aiCommonElements.Push(6);
  pvtx[0].x = fI0;  pvtx[0].y = fJ0;  pvtx[0].z = 0;
  pvtx[1].x = fI0;  pvtx[1].y = fJ1;  pvtx[1].z = 0;
  pvtx[2].x = fI1;  pvtx[2].y = fJ1;  pvtx[2].z = 0;
  pvtx[3].x = fI1;  pvtx[3].y = fJ0;  pvtx[3].z = 0;
  ptex[0].s = 0;    ptex[0].t = 0;
  ptex[1].s = 0;    ptex[1].t = 1;
  ptex[2].s = 1;    ptex[2].t = 1;
  ptex[3].s = 1;    ptex[3].t = 0;
  pcol[0] = glCol;  pcol[1] = glCol;  pcol[2] = glCol;  pcol[3] = glCol;
  pelm[0] = iStart + 0;  pelm[1] = iStart + 1;  pelm[2] = iStart + 2;
  pelm[3] = iStart + 2;  pelm[4] = iStart + 3;  pelm[5] = iStart + 0;
}

// Adds one part of texture to rendering queue
void CDrawPort::AddTexture(const FLOAT fI0, const FLOAT fJ0, const FLOAT fI1, const FLOAT fJ1,
                           const FLOAT fU0, const FLOAT fV0, const FLOAT fU1, const FLOAT fV1, const COLOR col) const
{
  const GFXColor glCol(AdjustColor(col, _slTexHueShift, _slTexSaturation));
  const INDEX iStart = _avtxCommon.Count();
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);
  INDEX       *pelm = _aiCommonElements.Push(6);
  pvtx[0].x = fI0;  pvtx[0].y = fJ0;  pvtx[0].z = 0;
  pvtx[1].x = fI0;  pvtx[1].y = fJ1;  pvtx[1].z = 0;
  pvtx[2].x = fI1;  pvtx[2].y = fJ1;  pvtx[2].z = 0;
  pvtx[3].x = fI1;  pvtx[3].y = fJ0;  pvtx[3].z = 0;
  ptex[0].s = fU0;  ptex[0].t = fV0;
  ptex[1].s = fU0;  ptex[1].t = fV1;
  ptex[2].s = fU1;  ptex[2].t = fV1;
  ptex[3].s = fU1;  ptex[3].t = fV0;
  pcol[0] = glCol;
  pcol[1] = glCol;
  pcol[2] = glCol;
  pcol[3] = glCol;
  pelm[0] = iStart + 0;  pelm[1] = iStart + 1;  pelm[2] = iStart + 2;
  pelm[3] = iStart + 2;  pelm[4] = iStart + 3;  pelm[5] = iStart + 0;
}

// Adds one triangle to rendering queue
void CDrawPort::AddTriangle(const FLOAT fI0, const FLOAT fJ0,
                            const FLOAT fI1, const FLOAT fJ1,
                            const FLOAT fI2, const FLOAT fJ2, const COLOR col) const
{
  const GFXColor glCol(AdjustColor(col, _slTexHueShift, _slTexSaturation));
  const INDEX iStart = _avtxCommon.Count();
  GFXVertex   *pvtx = _avtxCommon.Push(3);
  GFXTexCoord *ptex = _atexCommon.Push(3);
  GFXColor    *pcol = _acolCommon.Push(3);
  INDEX       *pelm = _aiCommonElements.Push(3);
  pvtx[0].x = fI0;  pvtx[0].y = fJ0;  pvtx[0].z = 0;
  pvtx[1].x = fI1;  pvtx[1].y = fJ1;  pvtx[1].z = 0;
  pvtx[2].x = fI2;  pvtx[2].y = fJ2;  pvtx[2].z = 0;
  pcol[0] = glCol;
  pcol[1] = glCol;
  pcol[2] = glCol;
  pelm[0] = iStart + 0;
  pelm[1] = iStart + 1;
  pelm[2] = iStart + 2;
}

// Adds one textured quad (up-left start, counter-clockwise)
void CDrawPort::AddTexture(const FLOAT fI0, const FLOAT fJ0, const FLOAT fU0, const FLOAT fV0, const COLOR col0,
                           const FLOAT fI1, const FLOAT fJ1, const FLOAT fU1, const FLOAT fV1, const COLOR col1,
                           const FLOAT fI2, const FLOAT fJ2, const FLOAT fU2, const FLOAT fV2, const COLOR col2,
                           const FLOAT fI3, const FLOAT fJ3, const FLOAT fU3, const FLOAT fV3, const COLOR col3) const
{
  const GFXColor glCol0(AdjustColor(col0, _slTexHueShift, _slTexSaturation));
  const GFXColor glCol1(AdjustColor(col1, _slTexHueShift, _slTexSaturation));
  const GFXColor glCol2(AdjustColor(col2, _slTexHueShift, _slTexSaturation));
  const GFXColor glCol3(AdjustColor(col3, _slTexHueShift, _slTexSaturation));
  const INDEX iStart = _avtxCommon.Count();
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);
  INDEX       *pelm = _aiCommonElements.Push(6);
  pvtx[0].x = fI0;  pvtx[0].y = fJ0;  pvtx[0].z = 0;
  pvtx[1].x = fI1;  pvtx[1].y = fJ1;  pvtx[1].z = 0;
  pvtx[2].x = fI2;  pvtx[2].y = fJ2;  pvtx[2].z = 0;
  pvtx[3].x = fI3;  pvtx[3].y = fJ3;  pvtx[3].z = 0;
  ptex[0].s = fU0;  ptex[0].t = fV0;
  ptex[1].s = fU1;  ptex[1].t = fV1;
  ptex[2].s = fU2;  ptex[2].t = fV2;
  ptex[3].s = fU3;  ptex[3].t = fV3;
  pcol[0] = glCol0;
  pcol[1] = glCol1;
  pcol[2] = glCol2;
  pcol[3] = glCol3;
  pelm[0] = iStart + 0;  pelm[1] = iStart + 1;  pelm[2] = iStart + 2;
  pelm[3] = iStart + 2;  pelm[4] = iStart + 3;  pelm[5] = iStart + 0;
}

// Renders all textures from rendering queue and flushed rendering arrays
void CDrawPort::FlushRenderingQueue(void) const
{
  gfxFlushElements();
  gfxResetArrays();
}

// Blends screen with accumulation color
void CDrawPort::BlendScreen(void)
{
  if (dp_ulBlendingA == 0) return;

  ULONG fix1oA = 65536 / dp_ulBlendingA;
  ULONG ulRA = (dp_ulBlendingRA * fix1oA) >> 16;
  ULONG ulGA = (dp_ulBlendingGA * fix1oA) >> 16;
  ULONG ulBA = (dp_ulBlendingBA * fix1oA) >> 16;
  ULONG ulA  = ClampUp(dp_ulBlendingA, 255UL);
  COLOR colBlending = RGBAToColor(ulRA, ulGA, ulBA, ulA);

  // Blend drawport (thru z-buffer because of elimination of pixel artefacts)
  gfxEnableDepthTest();
  gfxDisableDepthWrite();
  gfxEnableBlend();
  gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
  gfxDisableAlphaTest();
  gfxDisableTexture();

  // Prepare color
  colBlending = AdjustColor(colBlending, _slTexHueShift, _slTexSaturation);
  GFXColor glcol(colBlending);

  // Set arrays
  gfxResetArrays();
  GFXVertex   *pvtx = _avtxCommon.Push(4);
  GFXTexCoord *ptex = _atexCommon.Push(4);
  GFXColor    *pcol = _acolCommon.Push(4);
  const INDEX iW = GetWidth();
  const INDEX iH = GetHeight();
  pvtx[0].x =  0;  pvtx[0].y =  0;  pvtx[0].z = 0.01f;
  pvtx[1].x =  0;  pvtx[1].y = iH;  pvtx[1].z = 0.01f;
  pvtx[2].x = iW;  pvtx[2].y = iH;  pvtx[2].z = 0.01f;
  pvtx[3].x = iW;  pvtx[3].y =  0;  pvtx[3].z = 0.01f;
  pcol[0] = glcol;
  pcol[1] = glcol;
  pcol[2] = glcol;
  pcol[3] = glcol;
  gfxFlushQuads();

  // Reset accumulation color
  dp_ulBlendingRA = 0;
  dp_ulBlendingGA = 0;
  dp_ulBlendingBA = 0;
  dp_ulBlendingA  = 0;
}