/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TEXTUREDATA_H
#define SE_INCL_TEXTUREDATA_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Lists.h>
#include <Engine/Anim/Anim.h>
#include <Engine/Graphics/GfxLibrary.h>

#define BYTES_PER_TEXEL 4   // all textures in engine are 4 bytes per pixel

// td_ulFlags bits
enum TextureFlags
{
  // (on disk and in memory)
  TEX_ALPHACHANNEL    = (1UL << 0), // texture has alpha channel (for old version support)
  TEX_32BIT           = (1UL << 1), // texture needs to be in 32-bit quality uploaded if can
  TEX_COMPRESSED      = (1UL << 2), // texture uses S3TC/DXTn compression
  TEX_TRANSPARENT     = (1UL << 3), // only one bit of alpha channel is enough (internal format GL_RGB5_A1)
  TEX_EQUALIZED       = (1UL << 4), // texture has 128-gray last mipmap (i.e. can be discarded in shade mode)
  TEX_COMPRESSEDALPHA = (1UL << 5), // compressed texture has interpolated alpha (DXT5)

  // (only in memory)
  TEX_STATIC       = (1UL << 7), // remain loaded after being bound (i.e. uploaded - for base textures)
  TEX_CONSTANT     = (1UL << 8), // cannot be changed (no mip-map disposing, no LOD biasing, no colorizing, nothing!)
  TEX_GRAY         = (1UL << 9), // grayscale texture
  TEX_WHITE        = (1UL << 10),  // completely white texture (believe me, there are some cases)
  TEX_KEEPCOLOR    = (1UL << 11),  // don't (de)saturate (for heightmaps and such!)
  TEX_SINGLEMIPMAP = (1UL << 18),  // set if last uploading was in single-mipmap
  TEX_PROBED       = (1UL << 19),  // set if last binding was as probe-texture

  // (flags that shows if texture mipmaps has been changed)
  TEX_DISPOSED    = (1UL << 20),  // largest mip-map(s) has been left-out
  TEX_DITHERED    = (1UL << 21),  // dithering has been applied on this texture
  TEX_FILTERED    = (1UL << 22),  // flitering has been applied on this texture
  TEX_SATURATED   = (1UL << 23),  // saturation has been adjusted on this texture
  TEX_COLORIZED   = (1UL << 24),  // mipmaps has been colorized on this texture
  TEX_WASOLD      = (1UL << 30),  // loaded from old format (version 3)
};

//! Bitmap data for a class of texture objects
class ENGINE_API CTextureData : public CAnimData
{
  public:
    ULONG td_ulFlags;             // see defines
    MEX   td_mexWidth, td_mexHeight; // texture dimensions
    INDEX td_iFirstMipLevel;      // the highest quality mip level
    INDEX td_ctFineMipLevels;     // number of bilineary created mip levels
    SLONG td_slFrameSize;         // sum of sizes of all mip-maps for one frame
    INDEX td_ctFrames;            // number of different frames

    class CTexParams td_tpLocal;  // local texture parameters
    ULONG td_ulInternalFormat;    // format in which texture will be uploaded
    CTimerValue td_tvLastDrawn;   // timer for probing
    ULONG td_ulProbeObject;
    union {
      ULONG  td_ulObject;
      ULONG *td_pulObjects;
    };
    ULONG *td_pulFrames;          // all frames with their mip-maps and private palettes
    UBYTE *td_pubBuffer1, *td_pubBuffer2;       // buffers for effect textures
    PIX td_pixBufferWidth, td_pixBufferHeight;  // effect buffer dimensions
    class CTextureData *td_ptdBaseTexture;      // base texure for effects (if any)
    class CTextureEffectGlobal *td_ptegEffect;  // all data for effect textures

    INDEX td_iRenderFrame; // frame number currently rendering (for profiling)

  public:
    //! Constructor
    CTextureData();
    
    //! Destructor
    ~CTextureData();

    //! Reference counting (override from CAnimData)
    void RemReference_internal();

    //! Converts global mip level to the corresponding one of texture
    INDEX ClampMipLevel(FLOAT fMipFactor) const;

    //! Returns texture width in mexels.
    inline MEX GetWidth() const
    { 
      return td_mexWidth; 
    };

    //! Returns texture height in mexels.
    inline MEX GetHeight() const
    {
      return td_mexHeight;
    };

    //! Returns texture width in pixels.
    inline PIX GetPixWidth() const
    {
      return td_mexWidth >> td_iFirstMipLevel;
    };

    //! Returns texture height in pixels.
    inline PIX GetPixHeight() const
    {
      return td_mexHeight >> td_iFirstMipLevel;
    };
    
    //! Returns texture flags.
    inline ULONG GetFlags() const 
    {
      return td_ulFlags;
    };
    
    //! Returns number of mips
    inline ULONG GetNoOfMips(void) const
    {
      return GetNoOfMipmaps(GetPixWidth(), GetPixHeight());
    };
    
    //! Returns number of fine mips.
    inline ULONG GetNoOfFineMips(void) const
    { 
      return td_ctFineMipLevels;
    };

    
    //! Mark that texture has been used
    inline void MarkDrawn(void)
    {
      td_tvLastDrawn = _pTimer->GetHighPrecisionTimer();
    };

    //! Get string description of texture size, mips and parameters
    CTString GetDescription(void);

    //! Sets new texture mex width and changes height remaining texture's aspect ratio
    inline void ChangeSize(MEX mexNewWidth)
    {
      td_mexHeight = MEX(((FLOAT)mexNewWidth) / td_mexWidth * td_mexHeight);
      td_mexWidth  = mexNewWidth;
    };

    //! Check if texture frame(s) has been somehow altered (dithering, filtering, saturation, colorizing...)
    inline BOOL IsModified(void)
    {
      return td_ulFlags & (TEX_DISPOSED | TEX_DITHERED | TEX_FILTERED | TEX_SATURATED | TEX_COLORIZED);
    };
    
    //! Export finest mipmap of one texture's frame to imageinfo
    void ExportFrame_t(class CImageInfo &iiExportedImage, INDEX iFrame);

    //! Export finest mipmap of one texture's frame to image
    void ExportFrame_t(class CImage &iExportedImage, INDEX iFrame);

    //! Set texture frame as current for accelerator (this will upload texture that needs or wants uploading)
    void SetAsCurrent(INDEX iFrameNo = 0, BOOL bForceUpload = FALSE);

    //! Creates new effect texture with one frame
    void CreateEffectTexture(PIX pixWidth, PIX pixHeight, MEX mexWidth, CTextureData *ptdBaseTexture, ULONG ulGlobalEffect);

    //! Creates new texture with one frame
    void Create_t(const CImageInfo *pII, MEX mexWanted, INDEX ctFineMips, BOOL bForce32bit);
    
    //! Adds one frame to created texture
    void AddFrame_t(const CImageInfo *pII);

    //! Remove texture from gfx API (driver)
    void Unbind(void);
    
    //! Free memory allocated for texture
    void Clear(void);

    
    //! Read texture from file.
    void Read_t(CTStream *inFile);
    
    //! Write texture to file.
    void Write_t(CTStream *outFile);
    
    //! Force texture to be re-loaded (if needed) in corresponding manner
    void Force(ULONG ulTexFlags);

    //! Get texel from texture's largest mip-map
    COLOR GetTexel(MEX mexU, MEX mexV);
    
    //! Copy (and eventually convert to floats) one row from texture to an array (iChannel is 1=R,2=G,3=B,4=A)
    void  FetchRow(PIX pixRow, void *pfDst, INDEX iChannel = 4, BOOL bConvertToFloat = TRUE);
    
    //! Get pointer to one row of texture
    ULONG *GetRowPointer(PIX pixRow);

  // IO
  public:
    //! Check if this kind of objects is auto-freed
    virtual BOOL IsAutoFreed(void);
    
    //! Get amount of memory used by this object
    virtual SLONG GetUsedMemory(void);
    
    //! TODO: Add comment.
    virtual UBYTE GetType() const;
  
  private:
    //! Returns dimension of effect buffers and size in bytes (of one, not both).
    ULONG GetEffectBufferSize(void);
  
    //! Initializes td_pixBufferWidth & td_pixBufferHeight
    void InitEffectBufferDimensions(void);
  
    //! Allocates and resets effect buffers.
    ULONG AllocEffectBuffers(void);
  
    //! Free effect buffers' memory.
    void FreeEffectBuffers(void);
};


#endif  /* include-once check. */
