/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/ViewPort.h>

#include <Engine/Graphics/GfxProfile.h>
#include <Core/Base/Statistics_Internal.h>

//#include <d3dx8math.h>
//#pragma comment(lib, "d3dx8.lib")

//#include <d3dx8tex.h>
//#pragma comment(lib, "d3dx8.lib")
//extern "C" HRESULT WINAPI D3DXGetErrorStringA(HRESULT hr, LPSTR pBuffer, UINT BufferLen);
//char acErrorString[256];
//D3DXGetErrorString(hr, acErrorString, 255);
//ASSERTALWAYS(acErrorString);

extern INDEX gap_bOptimizeStateChanges;
extern INDEX gap_iOptimizeClipping;
extern INDEX gap_iDithering;
                
            
// cached states
extern BOOL GFX_bDepthTest  = FALSE;
extern BOOL GFX_bDepthWrite = FALSE;
extern BOOL GFX_bAlphaTest  = FALSE;
extern BOOL GFX_bDithering  = TRUE;
extern BOOL GFX_bBlending   = TRUE;
extern BOOL GFX_bClipping   = TRUE;
extern BOOL GFX_bClipPlane  = FALSE;
extern BOOL GFX_bColorArray = FALSE;
extern BOOL GFX_bTruform    = FALSE;
extern BOOL GFX_bFrontFace  = TRUE;
extern BOOL GFX_bViewMatrix = TRUE;
extern INDEX GFX_iActiveTexUnit = 0;
extern FLOAT GFX_fMinDepthRange = 0.0f;
extern FLOAT GFX_fMaxDepthRange = 0.0f;

extern GfxBlend GFX_eBlendSrc  = GFX_ONE;
extern GfxBlend GFX_eBlendDst  = GFX_ZERO;
extern GfxComp  GFX_eDepthFunc = GFX_LESS_EQUAL;
extern GfxFace  GFX_eCullFace  = GFX_NONE;
extern BOOL       GFX_abTexture[GFX_MAXTEXUNITS] = { FALSE, FALSE, FALSE, FALSE };
extern INDEX GFX_iTexModulation[GFX_MAXTEXUNITS] = { 0, 0, 0, 0 };

// Last ortho/frustum values (frustum has negative sign, because of orgho-frustum switching!)
extern FLOAT GFX_fLastL = 0;
extern FLOAT GFX_fLastR = 0;
extern FLOAT GFX_fLastT = 0;
extern FLOAT GFX_fLastB = 0;
extern FLOAT GFX_fLastN = 0;
extern FLOAT GFX_fLastF = 0;

// Number of vertices currently in buffer
extern INDEX GFX_ctVertices = 0;

// For D3D: mark need for clipping (when wants to be disable but cannot be because of user clip plane)
static BOOL _bWantsClipping = TRUE;

// Locking state for OGL
static BOOL _bCVAReallyLocked = FALSE;

// Clip plane and last view matrix for D3D
extern FLOAT D3D_afClipPlane[4]    = {0, 0, 0, 0};
extern FLOAT D3D_afViewMatrix[16]  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static FLOAT _afActiveClipPlane[4] = {0, 0, 0, 0};

// Truform/N-Patches
extern INDEX truform_iLevel  = -1;
extern BOOL  truform_bLinear = FALSE;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TEXTURE MANAGEMENT


#ifdef SE1_D3D8
// TODO: Add comment here
static LPDIRECT3DTEXTURE8 *_ppd3dCurrentTexture;
#endif // SE1_D3D8

// TODO: Add comment here
extern INDEX GetTexturePixRatio_OGL(GLuint uiBindNo);
extern INDEX GetFormatPixRatio_OGL(GLenum eFormat);
extern void  MimicTexParams_OGL(CTexParams &tpLocal);
extern void  UploadTexture_OGL(ULONG *pulTexture, PIX pixSizeU, PIX pixSizeV,
                                GLenum eInternalFormat, BOOL bUseSubImage);

#ifdef SE1_D3D8
// TODO: Add comment here
extern INDEX GetTexturePixRatio_D3D(LPDIRECT3DTEXTURE8 pd3dTexture);
extern INDEX GetFormatPixRatio_D3D(D3DFORMAT d3dFormat);
extern void  MimicTexParams_D3D(CTexParams &tpLocal);
extern void  UploadTexture_D3D(LPDIRECT3DTEXTURE8 *ppd3dTexture, ULONG *pulTexture,
                                PIX pixSizeU, PIX pixSizeV, D3DFORMAT eInternalFormat, BOOL bDiscard);
#endif // SE1_D3D8


extern FLOAT _fCurrentLODBias = 0;  // LOD bias adjuster

// Update texture LOD bias
extern void UpdateLODBias(const FLOAT fLODBias)
{ 
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Only if supported and needed
  if (_fCurrentLODBias == fLODBias && _pGfx->gl_fMaxTextureLODBias == 0) {
    return;
  }
  _fCurrentLODBias = fLODBias;

  _sfStats.StartTimer(CStatForm::STI_GFXAPI);

  // OpenGL
  if (eAPI == GAT_OGL) 
  { 
    // If no multitexturing
    if (_pGfx->gl_ctTextureUnits < 2) { 
      pglTexEnvf(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, fLODBias);
      OGL_CHECKERROR;
      
    // If multitexturing is active
    } else {
      // Loop thru units
      for (INDEX iUnit = 0; iUnit < _pGfx->gl_ctTextureUnits; iUnit++)
      { 
        pglActiveTexture(iUnit);  // select the unit
        pglTexEnvf(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, fLODBias);
        OGL_CHECKERROR;
      } 
      
      // Reselect the original unit
      pglActiveTexture(GFX_iActiveTexUnit);
      OGL_CHECKERROR;
    }
  }
  
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8)
  { 
    // Just set it
    HRESULT hr;
    
    // Loop thru tex units
    for (INDEX iUnit = 0; iUnit < _pGfx->gl_ctTextureUnits; iUnit++) 
    { 
      hr = _pGfx->gl_pd3dDevice->SetTextureStageState(iUnit, D3DTSS_MIPMAPLODBIAS, *((DWORD*)&fLODBias));
      D3D_CHECKERROR(hr);
    }
  }
#endif // SE1_D3D8
  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
}

// Get current texture filtering mode
void IGfxInterface::GetTextureFiltering(INDEX &iFilterType, INDEX &iAnisotropyDegree)
{
  iFilterType = _tpGlobal[0].tp_iFilter;
  iAnisotropyDegree = _tpGlobal[0].tp_iAnisotropy;
}
 
// Set texture filtering mode
void IGfxInterface::SetTextureFiltering(INDEX &iFilterType, INDEX &iAnisotropyDegree)
{              
  // Clamp vars
  INDEX iMagTex = iFilterType / 100;     iMagTex = Clamp(iMagTex, 0L, 2L);  // 0=same as iMinTex, 1=nearest, 2=linear
  INDEX iMinTex = iFilterType / 10 %10;  iMinTex = Clamp(iMinTex, 1L, 2L);  // 1=nearest, 2=linear
  INDEX iMinMip = iFilterType % 10;      iMinMip = Clamp(iMinMip, 0L, 2L);  // 0=no mipmapping, 1=nearest, 2=linear
  
  iFilterType   = iMagTex * 100 + iMinTex * 10 + iMinMip;
  iAnisotropyDegree = Clamp(iAnisotropyDegree, 1L, _pGfx->gl_iMaxTextureAnisotropy);

  // Skip if not changed
  if (_tpGlobal[0].tp_iFilter == iFilterType && _tpGlobal[0].tp_iAnisotropy == iAnisotropyDegree) {
    return;
  }
  
  _tpGlobal[0].tp_iFilter = iFilterType;
  _tpGlobal[0].tp_iAnisotropy = iAnisotropyDegree;

  // For OpenGL, that's about it
#ifdef SE1_D3D8
  if (_pGfx->GetCurrentAPI() != GAT_D3D8) {
    return;
  }
  
  _sfStats.StartTimer(CStatForm::STI_GFXAPI);

  // For D3D, it's a stage state (not texture state), so change it!
  HRESULT hr;
  _D3DTEXTUREFILTERTYPE eMagFilter, eMinFilter, eMipFilter;
  const LPDIRECT3DDEVICE8 pd3dDev = _pGfx->gl_pd3dDevice; 
  extern void UnpackFilter_D3D(INDEX iFilter, _D3DTEXTUREFILTERTYPE &eMagFilter,
                               _D3DTEXTUREFILTERTYPE &eMinFilter, _D3DTEXTUREFILTERTYPE &eMipFilter);
  UnpackFilter_D3D(iFilterType, eMagFilter, eMinFilter, eMipFilter);
  if (iAnisotropyDegree > 1) 
  { 
    // Adjust filter for anisotropy
    eMagFilter = D3DTEXF_ANISOTROPIC;
    eMinFilter = D3DTEXF_ANISOTROPIC;
  }
  
  // Set filtering and anisotropy degree
  for (INDEX iUnit = 0; iUnit < _pGfx->gl_ctTextureUnits; iUnit++) 
  { 
    // Must loop thru all usable texture units
    hr = pd3dDev->SetTextureStageState(iUnit, D3DTSS_MAXANISOTROPY, iAnisotropyDegree);  D3D_CHECKERROR(hr);
    hr = pd3dDev->SetTextureStageState(iUnit, D3DTSS_MAGFILTER, eMagFilter);  D3D_CHECKERROR(hr);
    hr = pd3dDev->SetTextureStageState(iUnit, D3DTSS_MINFILTER, eMinFilter);  D3D_CHECKERROR(hr);
    hr = pd3dDev->SetTextureStageState(iUnit, D3DTSS_MIPFILTER, eMipFilter);  D3D_CHECKERROR(hr);
  }
  
  // Done
  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
#endif // SE1_D3D8
}

// Set new texture LOD biasing
void IGfxInterface::SetTextureBiasing(FLOAT &fLODBias)
{
  // Adjust LOD biasing if needed
  fLODBias = Clamp(fLODBias, -_pGfx->gl_fMaxTextureLODBias, +_pGfx->gl_fMaxTextureLODBias); 
  if (_pGfx->gl_fTextureLODBias != fLODBias) {
    _pGfx->gl_fTextureLODBias = fLODBias;
    UpdateLODBias(fLODBias);
  }
}

// Set texture unit as active
void IGfxInterface::SetTextureUnit(INDEX iUnit)
{
  // Check API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  ASSERT(iUnit >= 0 && iUnit < 4); // supports 4 layers (for now)

  // Check consistency
#ifndef NDEBUG
  if (eAPI == GAT_OGL) {
    GLint gliRet;
    pglGetIntegerv(GL_ACTIVE_TEXTURE_ARB, &gliRet);
    ASSERT(GFX_iActiveTexUnit == (gliRet - GL_TEXTURE0_ARB));
    pglGetIntegerv(GL_CLIENT_ACTIVE_TEXTURE_ARB, &gliRet);
    ASSERT(GFX_iActiveTexUnit == (gliRet - GL_TEXTURE0_ARB));
  }
#endif

  // Cached?
  if (GFX_iActiveTexUnit == iUnit) {
    return;
  }
  GFX_iActiveTexUnit = iUnit;

  // Really set only for OpenGL
  if (eAPI != GAT_OGL) {
    return;
  }
  
  _sfStats.StartTimer(CStatForm::STI_GFXAPI);
  pglActiveTexture(iUnit);
  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
}

// Set texture as current
void IGfxInterface::SetTexture(ULONG &ulTexObject, CTexParams &tpLocal)
{
  // Clamp texture filtering if needed
  static INDEX _iLastTextureFiltering = 0;
  if (_iLastTextureFiltering != _tpGlobal[0].tp_iFilter)
  {
    INDEX iMagTex = _tpGlobal[0].tp_iFilter / 100;     iMagTex = Clamp(iMagTex, 0L, 2L);  // 0=same as iMinTex, 1=nearest, 2=linear
    INDEX iMinTex = _tpGlobal[0].tp_iFilter / 10 % 10;  iMinTex = Clamp(iMinTex, 1L, 2L);  // 1=nearest, 2=linear
    INDEX iMinMip = _tpGlobal[0].tp_iFilter % 10;      iMinMip = Clamp(iMinMip, 0L, 2L);  // 0=no mipmapping, 1=nearest, 2=linear
    
    _tpGlobal[0].tp_iFilter = iMagTex * 100 + iMinTex * 10 + iMinMip;
    _iLastTextureFiltering  = _tpGlobal[0].tp_iFilter;
  }

  // Determine API and enable texturing
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  gfxEnableTexture();

  _sfStats.StartTimer(CStatForm::STI_BINDTEXTURE);
  _sfStats.StartTimer(CStatForm::STI_GFXAPI);
  _pfGfxProfile->StartTimer(CGfxProfile::PTI_SETCURRENTTEXTURE);
  _pfGfxProfile->IncrementTimerAveragingCounter(CGfxProfile::PTI_SETCURRENTTEXTURE);

  if (eAPI == GAT_OGL) { // OpenGL
    pglBindTexture(GL_TEXTURE_2D, ulTexObject);
    MimicTexParams_OGL(tpLocal);
  } 
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) { // Direct3D
    _ppd3dCurrentTexture = (LPDIRECT3DTEXTURE8*)&ulTexObject;
    HRESULT hr = _pGfx->gl_pd3dDevice->SetTexture(GFX_iActiveTexUnit, *_ppd3dCurrentTexture);
    D3D_CHECKERROR(hr);
    MimicTexParams_D3D(tpLocal);
  }
#endif // SE1_D3D8
  
  // Done
  _pfGfxProfile->StopTimer(CGfxProfile::PTI_SETCURRENTTEXTURE);
  _sfStats.StopTimer(CStatForm::STI_BINDTEXTURE);
  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
}

// Upload texture
void IGfxInterface::UploadTexture(ULONG *pulTexture, PIX pixWidth, PIX pixHeight, ULONG ulFormat, BOOL bNoDiscard)
{
  // determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  _sfStats.StartTimer(CStatForm::STI_GFXAPI);

  if (eAPI == GAT_OGL) { // OpenGL
    UploadTexture_OGL(pulTexture, pixWidth, pixHeight, (GLenum)ulFormat, bNoDiscard);
  }
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) { // Direct3D
    const LPDIRECT3DTEXTURE8 _pd3dLastTexture = *_ppd3dCurrentTexture;
    UploadTexture_D3D(_ppd3dCurrentTexture, pulTexture, pixWidth, pixHeight, (D3DFORMAT)ulFormat, !bNoDiscard);
    
    // In case that texture has been changed, must re-set it as current
    if (_pd3dLastTexture != *_ppd3dCurrentTexture) {
      HRESULT hr = _pGfx->gl_pd3dDevice->SetTexture(GFX_iActiveTexUnit, *_ppd3dCurrentTexture);
      D3D_CHECKERROR(hr);
    }
  } 
#endif // SE1_D3D8
  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
}


// Returns size of uploaded texture
SLONG IGfxInterface::GetTextureSize(ULONG ulTexObject, BOOL bHasMipmaps/*=TRUE*/)
{
  // Nothing used if nothing uploaded
  if (ulTexObject == NULL) {
    return 0;
  }
  
  // Determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  SLONG slMipSize;

  _sfStats.StartTimer(CStatForm::STI_GFXAPI);

  // OpenGL
  if (eAPI == GAT_OGL)
  {
    // Was texture compressed?
    pglBindTexture(GL_TEXTURE_2D, ulTexObject); 
    BOOL bCompressed = FALSE;
    if (_pGfx->gl_ulFlags & GLF_EXTC_ARB) {
      pglGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED_ARB, (BOOL*)&bCompressed);
      OGL_CHECKERROR;
    }
    
    // For compressed textures, determine size directly
    if (bCompressed) {
      pglGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE_ARB, (GLint*)&slMipSize);
      OGL_CHECKERROR;
       
    // Non-compressed textures goes thru determination of internal format
    } else {
      PIX pixWidth, pixHeight;
      pglGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH,  (GLint*)&pixWidth);
      pglGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, (GLint*)&pixHeight);
      OGL_CHECKERROR;
      slMipSize = pixWidth*pixHeight * gfxGetTexturePixRatio(ulTexObject);
    }
  }
  // Direct3D
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8)
  {
    // We can determine exact size from texture surface (i.e. mipmap)
    D3DSURFACE_DESC d3dSurfDesc;
    HRESULT hr = ((LPDIRECT3DTEXTURE8)ulTexObject)->GetLevelDesc(0, &d3dSurfDesc);
    D3D_CHECKERROR(hr);
    slMipSize = d3dSurfDesc.Size;
  }
#endif // SE1_D3D8

  // Eventually count in all the mipmaps (takes extra 33% of texture size)
  extern INDEX gap_bAllowSingleMipmap;
  const SLONG slUploadSize = (bHasMipmaps || !gap_bAllowSingleMipmap) ? slMipSize * 4 / 3 : slMipSize;

  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
  return slUploadSize;
}

// Returns bytes/pixels ratio for uploaded texture
INDEX IGfxInterface::GetTexturePixRatio(ULONG ulTextureObject)
{
  // Determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  if (eAPI == GAT_OGL) {
    return GetTexturePixRatio_OGL((GLuint)ulTextureObject);
  } 
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    return GetTexturePixRatio_D3D((LPDIRECT3DTEXTURE8)ulTextureObject);
  }
#endif // SE1_D3D8
  else {
    return 0;
  }
}

// Returns bytes/pixels ratio for uploaded texture
INDEX IGfxInterface::GetFormatPixRatio(ULONG ulTextureFormat)
{
  // Determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  if (eAPI == GAT_OGL) {
    return GetFormatPixRatio_OGL((GLenum)ulTextureFormat);
  } 
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8) {
    return GetFormatPixRatio_D3D((D3DFORMAT)ulTextureFormat);
  }
#endif // SE1_D3D8
  else {
    return 0;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PATTERN TEXTURE FOR LINES

CTexParams _tpPattern;
extern ULONG _ulPatternTexture = NONE;
extern ULONG _ulLastUploadedPattern = 0;

// Upload pattern to accelerator memory
extern void gfxSetPattern(ULONG ulPattern)
{
  // Set pattern to be current texture
  _tpPattern.tp_bSingleMipmap = TRUE;
  gfxSetTextureWrapping(GFX_REPEAT, GFX_REPEAT);
  gfxSetTexture(_ulPatternTexture, _tpPattern);

  // If this pattern is currently uploaded, do nothing
  if (_ulLastUploadedPattern == ulPattern) {
    return;
  }
  
  // Convert bits to ULONGs
  ULONG aulPattern[32];
  for (INDEX iBit = 0; iBit < 32; iBit++)
  {
    if ((0x80000000 >> iBit) & ulPattern) {
      aulPattern[iBit] = 0xFFFFFFFF;
    } else {
      aulPattern[iBit] = 0x00000000;
    }
  }
  
  // Remember new pattern and upload
  _ulLastUploadedPattern = ulPattern;
  gfxUploadTexture(&aulPattern[0], 32, 1, TS.ts_tfRGBA8, FALSE);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VERTEX ARRAYS


// For D3D - (type 0=vtx, 1=nor, 2=col, 3=tex)
extern void SetVertexArray_D3D(INDEX iType, ULONG *pulVtx);

// TODO: Add comment here
void IGfxInterface::UnlockArrays(void)
{
  // Only if locked (OpenGL can lock 'em)
  if (!_bCVAReallyLocked) {
    return;
  }
  
  ASSERT(_pGfx->GetCurrentAPI() == GAT_OGL);

#ifndef NDEBUG
  INDEX glctRet;
  pglGetIntegerv(GL_ARRAY_ELEMENT_LOCK_COUNT_EXT, (GLint*)&glctRet);
  ASSERT(glctRet == GFX_ctVertices);
#endif
  pglUnlockArraysEXT();
  OGL_CHECKERROR;
 _bCVAReallyLocked = FALSE;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// OpenGL workarounds

// Initialization of commond quad elements array
extern void AddQuadElements(const INDEX ctQuads)
{
  const INDEX iStart = _aiCommonQuads.Count() / 6 * 4;
  INDEX *piQuads = _aiCommonQuads.Push(ctQuads * 6); 
  for (INDEX i = 0; i < ctQuads; i++)
  {
    piQuads[i * 6 + 0] = iStart + i * 4 + 0;
    piQuads[i * 6 + 1] = iStart + i * 4 + 1;
    piQuads[i * 6 + 2] = iStart + i * 4 + 2;
    piQuads[i * 6 + 3] = iStart + i * 4 + 2;
    piQuads[i * 6 + 4] = iStart + i * 4 + 3;
    piQuads[i * 6 + 5] = iStart + i * 4 + 0;
  }
}

// Helper function for flushers
static void FlushArrays(INDEX *piElements, INDEX ctElements)
{
  // Check
  const INDEX ctVertices = _avtxCommon.Count();
  ASSERT(_atexCommon.Count() == ctVertices);
  ASSERT(_acolCommon.Count() == ctVertices);
  extern BOOL CVA_b2D;
  gfxSetVertexArray(&_avtxCommon[0], ctVertices, FALSE);
  if (CVA_b2D) gfxLockArrays();
  gfxSetTexCoordArray(&_atexCommon[0], FALSE);
  gfxSetColorArray(&_acolCommon[0]);
  gfxDrawElements(ctElements, piElements);
  gfxUnlockArrays();
}

// Render quad elements to screen buffer
void IGfxInterface::FlushQuads(void)
{
  // If there is something to draw
  const INDEX ctElements = _avtxCommon.Count() * 6 / 4;
  if (ctElements <= 0) {
    return;
  }
  
  // Draw thru arrays (for OGL only) or elements?
  extern INDEX ogl_bAllowQuadArrays;

  if (_pGfx->GetCurrentAPI() == GAT_OGL && ogl_bAllowQuadArrays) {
    FlushArrays(NULL, _avtxCommon.Count());

  } else {
    // Make sure that enough quad elements has been initialized
    const INDEX ctQuads = _aiCommonQuads.Count();

    if (ctElements > ctQuads) {
      AddQuadElements(ctElements - ctQuads); // yes, 4 times more!
    }

    FlushArrays(&_aiCommonQuads[0], ctElements);
  }
}
 
// Render elements to screen buffer
void IGfxInterface::FlushElements(void)
{
  const INDEX ctElements = _aiCommonElements.Count();
  if (ctElements > 0) {
    FlushArrays(&_aiCommonElements[0], ctElements);
  }
}

#ifdef SE1_TRUFORM
// Set truform parameters
void IGfxInterface::SetTruform(INDEX iLevel, BOOL bLinearNormals)
{
  // Skip if Truform isn't supported
  if (_pGfx->gl_iMaxTessellationLevel < 1) {
    truform_iLevel  = 0;
    truform_bLinear = FALSE;
    return;
  }
  
  // Skip if same as last time
  iLevel = Clamp(iLevel, 0L, _pGfx->gl_iMaxTessellationLevel);
  if (truform_iLevel == iLevel && !truform_bLinear == !bLinearNormals) {
    return;
  }

  // Determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  _sfStats.StartTimer(CStatForm::STI_GFXAPI);

  // OpenGL needs truform set here
  if (eAPI == GAT_OGL) {
    GLenum eTriMode = bLinearNormals? GL_PN_TRIANGLES_NORMAL_MODE_LINEAR_ATI : GL_PN_TRIANGLES_NORMAL_MODE_QUADRATIC_ATI;
    pglPNTrianglesiATI(GL_PN_TRIANGLES_TESSELATION_LEVEL_ATI, iLevel);
    pglPNTrianglesiATI(GL_PN_TRIANGLES_NORMAL_MODE_ATI, eTriMode);
    OGL_CHECKERROR;
  }
  
  // If disabled, Direct3D will set tessellation level at "enable" call
#ifdef SE1_D3D8
  else if (eAPI == GAT_D3D8 && GFX_bTruform) { 
    FLOAT fSegments = iLevel + 1;
    HRESULT hr = _pGfx->gl_pd3dDevice->SetRenderState(D3DRS_PATCHSEGMENTS, *((DWORD*)&fSegments));
    D3D_CHECKERROR(hr);
  }
#endif // SE1_D3D8

  // Keep current truform params
  truform_iLevel  = iLevel;
  truform_bLinear = bLinearNormals;

  _sfStats.StopTimer(CStatForm::STI_GFXAPI);
}
#endif // SE1_TRUFORM

// Define specific API methods
#include <Engine/Graphics/GFX_wrapper_OpenGL.cpp>
#include <Engine/Graphics/GFX_wrapper_Direct3D.cpp>
