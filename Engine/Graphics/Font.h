/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FONT_H
#define SE_INCL_FONT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/FileName.h>
#include <Core/IO/Resource.h>


// Some default fonts
ENGINE_API extern CFontData *_pfdDisplayFont;
ENGINE_API extern CFontData *_pfdConsoleFont;

//! Font letter description
class ENGINE_API CFontCharData
{
  public:
    PIX fcd_pixXOffset, fcd_pixYOffset; // offset of letter inside tex file (in pixels)
    PIX fcd_pixStart, fcd_pixEnd;       // position and size adjustment for current letter
    
  public:
    //! Constructor
    CFontCharData(void);
    
    //! Read from stream.
    void Read_t(CTStream *inFile);

    //! Write to stream.
    void Write_t(CTStream *outFile);
};


/*
 * Font description
 */
class ENGINE_API CFontData : public CResource
{
  // Implementation
  public:
    PIX  fd_pixCharSpacing, fd_pixLineSpacing;  // intra character and intra line spacing
    PIX  fd_pixCharWidth, fd_pixCharHeight;     // maximum character width and height
    BOOL fd_bFixedWidth;   // treated as of fixed width

    CTFileName fd_fnTexture;
    class CFontCharData fd_fcdFontCharData[256];
    class CTextureData *fd_ptdTextureData;

  // Interface
  public:
    //! Default constructor.
    CFontData();

    //! Destructor.
    ~CFontData();
    
    //! Returns maximum character width. 
    inline PIX GetWidth() const 
    { 
      return fd_pixCharWidth;   
    };
    
    //! Returns maximum character height. 
    inline PIX GetHeight() const 
    { 
      return fd_pixCharHeight;
    };
    
    //! Returns intra character spacing.
    inline PIX GetCharSpacing() const
    { 
      return fd_pixCharSpacing;
    };

    //! Returns intra line spacing.
    inline PIX GetLineSpacing(void) const
    {
      return fd_pixLineSpacing;
    };
    
    //! Returns true if characters have fixed width.
    inline BOOL IsFixedWidth(void) const 
    {
      return fd_bFixedWidth;
    };
    
    //! Sets intra character spacing.
    inline void SetCharSpacing(PIX pixSpacing) 
    {
      fd_pixCharSpacing = pixSpacing;
    };

    //! Sets intra line spacing.
    inline void SetLineSpacing(PIX pixSpacing) 
    {
      fd_pixLineSpacing = pixSpacing;
    };
    
    //! Enables fixed character spacing.
    inline void SetFixedWidth(void)    
    {
      fd_bFixedWidth = TRUE;
    };
    
    //! Disables fixed character spacing.
    inline void SetVariableWidth(void)
    {
      fd_bFixedWidth = FALSE;
    };
    
    //! Sets space width ratio.
    inline void SetSpaceWidth(FLOAT fWidthRatio)
    { 
      // relative to char cell width (1/2 is default)
      fd_fcdFontCharData[' '].fcd_pixEnd = (PIX)(fd_pixCharWidth * fWidthRatio);
    }

    //! Read from stream.
    void Read_t( CTStream *inFile); // throw char *

    //! Write to stream.
    void Write_t(CTStream *outFile); // throw char *

    //! Make font using texture and settings.
    void Make_t(const CTFileName &fnTexture, PIX pixCharWidth, PIX pixCharHeight,
                CTFileName &fnOrderFile, BOOL bUseAlpha);

    //! Clear the data.
    void Clear();
};


#endif  /* include-once check. */