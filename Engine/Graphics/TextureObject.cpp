/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/Texture.h>

#include <Core/IO/Stream.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation of CTextureObject routines
 
//! Constructor
CTextureObject::CTextureObject(void)
{
}

// Copy from another object of same class
void CTextureObject::Copy(CTextureObject &toOther)
{
  CAnimObject::Copy(toOther);
}

//! Read from stream.
void CTextureObject::Read_t(CTStream *istrFile) 
{
  CAnimObject::Read_t(istrFile);
}

//! Write to stream.
void CTextureObject::Write_t(CTStream *ostrFile) 
{
  CAnimObject::Write_t(ostrFile);
}

//! Returns width in mexels.
MEX CTextureObject::GetWidth(void) const
{ 
  return ((CTextureData*)ao_AnimData)->GetWidth();
};

//! Returns height in mexels.
MEX CTextureObject::GetHeight(void) const
{
  return ((CTextureData*)ao_AnimData)->GetHeight();
};

//! Returns texture flags.
ULONG CTextureObject::GetFlags(void) const
{
  return ((CTextureData*)ao_AnimData)->GetFlags();
};

// Get filename of texture or empty string if no texture
const CTFileName &CTextureObject::GetName(void)
{
  static const CTFileName strDummy(CTString(""));
  
  // If there is some texture
  if (ao_AnimData != NULL) {
    // Get texture filename
    return ao_AnimData->GetName();
    
  // If there is no texture
  } else {
    // Get empty string
    return strDummy;
  }
}