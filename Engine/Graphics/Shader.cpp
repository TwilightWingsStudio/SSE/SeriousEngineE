/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/IO/Stream.h>
#include <Core/Math/Projection.h>
#include <Engine/Meshes/SM_Render.h>
#include <Engine/Graphics/Shader.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Graphics/Fog_internal.h>

static INDEX _ctVertices = -1;
static INDEX _ctIndices  = -1;
static INDEX _ctTextures = -1;
static INDEX _ctUVMaps   = -1;
static INDEX _ctColors   = -1;
static INDEX _ctFloats   = -1;
static INDEX _ctLights   = -1;

static CAnyProjection3D *_paprProjection;  // current projection
static Matrix12 *_pmObjToView = NULL;
static Matrix12 *_pmObjToViewStr = NULL;
static Matrix12 *_pmObjToAbs  = NULL;

static CShaderClass     *_pShader       = NULL;   // current shader
static GFXTexCoord *_pCurrentUVMap = NULL; // current UVMap

static GFXVertex  *_paVertices  = NULL;   // array of vertices
static GFXNormal   *_paNormals   = NULL;   // array of normals
static GFXTexCoord **_paUVMaps   = NULL;   // array of uvmaps to chose from

static GFXTexCoord *_paFogUVMap   = NULL;   // UVMap for fog pass
static GFXTexCoord *_paHazeUVMap  = NULL;   // UVMap for haze pass
static GFXColor    *_pacolVtxHaze = NULL;  // array of vertex colors for haze

static CTextureObject **_paTextures = NULL;// array of textures to chose from
static INDEX       *_paIndices   = NULL;   // current array of triangle indices

static GFXColor    _colAmbient = 0x000000FF;     // Ambient color
static COLOR       _colModel   = 0x000000FF;     // Model color
static GFXColor    _colLight   = 0x000000FF;     // Light color
static FLOAT3D     _vLightDir  = FLOAT3D(0,0,0); // Light direction

static COLOR _colConstant = NULL; // current set color
static COLOR *_paColors   = NULL; // array of colors to chose from
static FLOAT *_paFloats   = NULL; // array of floats to chose from
static ULONG _ulFlags     = 0;    // Shading flags (full bright, double sided, etc)
static ULONG _ulRenFlags  = 0;    // Rendering flags (fog, haze, etc)

// Vertex colors
static TStaticStackArray<GFXColor> _acolVtxColors;        // array of color values for each vertex
static TStaticStackArray<GFXColor> _acolVtxModifyColors;  // array of color modified values for each vertex
GFXColor *_pcolVtxColors = NULL;    // pointer to vertex color array (points to current array of vertex colors)

// Vertex array that is returned if shader request vertices for modify
static TStaticStackArray<GFXVertex>  _vModifyVertices;
static TStaticStackArray<GFXTexCoord> _uvUVMapForModify;

// Viewer absolute and object space projection
static FLOAT3D _vViewer;
static FLOAT3D _vViewerObj;
static FLOAT3D _vFViewerObj;
static FLOAT3D _vHDirObj;
static FLOAT3D _vLightObj;
static FLOAT3D _vObjPos;

// Returns haze/fog value in vertex 
static FLOAT   _fFogAddZ, _fFogAddH;
static FLOAT   _fHazeAdd;
static TStaticStackArray<struct GFXTexCoord> _aTexMipFogy;
static TStaticStackArray<struct GFXTexCoord> _aTexMipHazey;

// Texture wrapping set by the shader
static GfxWrap _aShaderTexWrap[2] = { GFX_REPEAT, GFX_REPEAT };

// Begin shader using
void shaBegin(CAnyProjection3D &aprProjection,CShaderClass *pShader)
{
  // Chech that last shading ended with shaEnd
  ASSERT(_pShader == NULL);
  
  // Chech if shader exists
  ASSERT(pShader != NULL);
  
  // Set current projection
  _paprProjection = &aprProjection;
  
  // Set pointer to shader
  _pShader = pShader;
}

// End shader using
void shaEnd(void)
{
  // Chech if shader exists
  ASSERT(_pShader != NULL);
  
  // Call shader function
  _pShader->ShaderFunc();
  
  // Clean used values
  shaClean();

  _pShader = NULL;
}

// Render given model
void shaRender(void)
{
  ASSERT(_ctVertices > 0);
  ASSERT(_ctIndices > 0);
  ASSERT(_paVertices != NULL);
  ASSERT(_paIndices != NULL);

  // Set vertices
  gfxSetVertexArray(_paVertices,_ctVertices, FALSE);
  gfxLockArrays();

  // If there is valid UVMap
  if (_pCurrentUVMap != NULL) {
    gfxSetTexCoordArray(_pCurrentUVMap, FALSE);
  }

  // If there is valid vertex color array
  if (_pcolVtxColors != NULL) {
    gfxSetColorArray(_pcolVtxColors);
  }

  // Draw model with set params
  gfxDrawElements(_ctIndices, _paIndices);
  gfxUnlockArrays();
}

// Check vertex against fog
static void GetFogMapInVertex(GFXVertex &vtx, GFXTexCoord &tex)
{
  const FLOAT fD = vtx.x * _vFViewerObj(1) + vtx.y * _vFViewerObj(2) + vtx.z * _vFViewerObj(3);
  const FLOAT fH = vtx.x * _vHDirObj(1) + vtx.y * _vHDirObj(2) + vtx.z * _vHDirObj(3);
  tex.s = (fD + _fFogAddZ) * _fog_fMulZ;
//  tex.s = (vtx.z) * _fog_fMulZ;
  tex.t = (fH + _fFogAddH) * _fog_fMulH;
}

// Check vertex against haze
static void GetHazeMapInVertex(GFXVertex &vtx, FLOAT &tx1)
{
  const FLOAT fD = vtx.x * _vViewerObj(1) + vtx.y * _vViewerObj(2) + vtx.z * _vViewerObj(3);
  tx1 = (fD + _fHazeAdd) * _haze_fMul;
}

// check model's bounding box against fog
BOOL shaHasFog(const FLOATaabbox3D &bbox)
{
  const FLOAT3D &vMin = bbox.minvect;
  const FLOAT3D &vMax = bbox.maxvect;
  
  GFXTexCoord tex;
  GFXVertex  vtx;
  vtx.x = vMin(1); vtx.y = vMin(2); vtx.z = vMin(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMin(1); vtx.y = vMin(2); vtx.z = vMax(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMin(1); vtx.y = vMax(2); vtx.z = vMin(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMin(1); vtx.y = vMax(2); vtx.z = vMax(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMin(2); vtx.z = vMin(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMin(2); vtx.z = vMax(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMax(2); vtx.z = vMin(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMax(2); vtx.z = vMax(3); GetFogMapInVertex(vtx,tex); if (InFog(tex.t)) { return TRUE; }
  return FALSE;
}

// Check model's bounding box against haze
BOOL shaHasHaze(const FLOATaabbox3D &bbox)
{
  const FLOAT3D &vMin = bbox.minvect;
  const FLOAT3D &vMax = bbox.maxvect;
  
  FLOAT fS;
  GFXVertex vtx;
  vtx.x = vMin(1); vtx.y = vMin(2); vtx.z = vMin(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMin(1); vtx.y = vMin(2); vtx.z = vMax(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMin(1); vtx.y = vMax(2); vtx.z = vMin(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMin(1); vtx.y = vMax(2); vtx.z = vMax(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMin(2); vtx.z = vMin(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMin(2); vtx.z = vMax(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMax(2); vtx.z = vMin(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  vtx.x = vMax(1); vtx.y = vMax(2); vtx.z = vMax(3); GetHazeMapInVertex(vtx,fS); if (InHaze(fS)) { return TRUE; }
  return FALSE;
}

void shaInitSharedFogAndHazeParams(BOOL bFog, BOOL bHaze, CAnyProjection3D &apr, Matrix12 &mObjToView, Matrix12 &mObjToAbs)
{
  ASSERT(_paFogUVMap == NULL);
  ASSERT(_paHazeUVMap == NULL);

  FLOAT3D vObjToView;
  Matrix12 mInvObjToAbs;
  FLOATmatrix3D mObjToViewRot;

  MatrixTranspose(mInvObjToAbs, mObjToAbs);
  Matrix12ToMatrixVector(mObjToViewRot, vObjToView, mObjToView);

  _vObjPos = FLOAT3D(mObjToAbs[3], mObjToAbs[7], mObjToAbs[11]);

  // Calculate projection of viewer in absolute space.
  FLOATmatrix3D &mViewer = apr->pr_ViewerRotationMatrix;
  _vViewer(1) = -mViewer(3,1);
  _vViewer(2) = -mViewer(3,2);
  _vViewer(3) = -mViewer(3,3);

  // Calculate projection of viewer in object space.
  _vViewerObj = _vViewer;
  RotateVector(_vViewerObj.vector, mInvObjToAbs);
  
  if (bHaze) {
    // Prepare haze.
    _fHazeAdd  = -_haze_hp.hp_fNear;
    _fHazeAdd += _vViewer(1) * (_vObjPos(1) - apr->pr_vViewerPosition(1));
    _fHazeAdd += _vViewer(2) * (_vObjPos(2) - apr->pr_vViewerPosition(2));
    _fHazeAdd += _vViewer(3) * (_vObjPos(3) - apr->pr_vViewerPosition(3));
  }

  if (bFog) {
    // Get viewer -z in object space.
    _vFViewerObj = FLOAT3D(0, 0, -1) * !mObjToViewRot;

    // Get fog direction in object space.
    _vHDirObj = _fog_vHDirAbs * !(!mViewer * mObjToViewRot);

    // Get viewer offset.
    _fFogAddZ  = _vViewer(1) * (_vObjPos(1) - apr->pr_vViewerPosition(1));
    _fFogAddZ += _vViewer(2) * (_vObjPos(2) - apr->pr_vViewerPosition(2));
    _fFogAddZ += _vViewer(3) * (_vObjPos(3) - apr->pr_vViewerPosition(3));

    // Get fog offset.
    _fFogAddH = (_fog_vHDirAbs % _vObjPos) + _fog_fp.fp_fH3;
  }
}

// Update model for fog and haze
void shaPrepareFogAndHaze(BOOL bOpaqueSurface)
{
  const BOOL bRenderFog = _ulRenFlags & SHA_RMF_FOG;
  const BOOL bRenderHaze = _ulRenFlags & SHA_RMF_HAZE;
  
  // Get current surface vertex array
  GFXVertex *paVertices;
  GFXColor *paColors;
  GFXColor *paHazeColors;
  INDEX ctVertices = shaGetVertexCount();
  
  paVertices = shaGetVertexArray();
  paColors = shaGetColorArray();
  paHazeColors = shaGetNewColorArray();

  if (bRenderFog) {
    if (bOpaqueSurface) {
      _aTexMipFogy.PopAll();
      _aTexMipFogy.Push(ctVertices);
      
      // Setup tex coords only
      for (INDEX ivtx = 0; ivtx < ctVertices; ivtx++)
      {
        GetFogMapInVertex(paVertices[ivtx], _aTexMipFogy[ivtx]);
      }

      shaSetFogUVMap(&_aTexMipFogy[0]);
    } else {
      GFXTexCoord tex;
      for (INDEX ivtx = 0; ivtx < ctVertices; ivtx++)
      {
        GetFogMapInVertex(paVertices[ivtx], tex);
        UBYTE ub = GetFogAlpha(tex) ^ 255;
        paColors[ivtx].AttenuateA(ub);
      }
    }
  }

  if (bRenderHaze) {
    if (bOpaqueSurface) {
      _aTexMipHazey.PopAll();
      _aTexMipHazey.Push(ctVertices);
      const COLOR colH = AdjustColor(_haze_hp.hp_colColor, _slTexHueShift, _slTexSaturation);
      GFXColor colHaze(colH);

      // setup haze tex coords and color
      for (INDEX ivtx = 0; ivtx < ctVertices; ivtx++)
      {
        GetHazeMapInVertex(paVertices[ivtx], _aTexMipHazey[ivtx].s);
        _aTexMipHazey[ivtx].t = 0.0f;
        paHazeColors[ivtx] = colHaze;
      }

      shaSetHazeUVMap(&_aTexMipHazey[0]);
      shaSetHazeColorArray(&paHazeColors[0]);
    } else {
      FLOAT tx1;
      for (INDEX ivtx = 0; ivtx < ctVertices; ivtx++)
      {
        GetHazeMapInVertex(paVertices[ivtx], tx1);
        FLOAT ub = GetHazeAlpha(tx1) ^ 255;
        paHazeColors[ivtx] = paColors[ivtx];
        paHazeColors[ivtx].AttenuateA(ub);
      }

      shaSetHazeColorArray(&paHazeColors[0]);
    }
  }
}

// Render aditional pass for fog and haze
void shaDoFogAndHazePass(void)
{
  // If full bright then no fog pass 
  if (shaGetFlags() & BASE_FULL_BRIGHT) {
    return;
  }

  ASSERT(_paFogUVMap == NULL);
  ASSERT(_paHazeUVMap == NULL);

  // Calculate fog and haze uvmap for this opaque surface
  shaPrepareFogAndHaze(TRUE);
  
  // If fog uvmap has been given
  if (_paFogUVMap != NULL) {
    // Setup texture/color arrays and rendering mode
    gfxSetTextureWrapping(GFX_CLAMP, GFX_CLAMP);
    gfxSetTexture(_fog_ulTexture, _fog_tpLocal);
    gfxSetTexCoordArray(_paFogUVMap, FALSE);
    gfxSetConstantColor(_fog_fp.fp_colColor);
    gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
    gfxEnableBlend();
    
    // Render fog pass
    gfxDrawElements(_ctIndices, _paIndices);
  }
  
  // If haze uvmap has been given
  if (_paHazeUVMap != NULL) {
    gfxSetTextureWrapping(GFX_CLAMP, GFX_CLAMP);
    gfxSetTexture(_haze_ulTexture, _haze_tpLocal);
    gfxSetTexCoordArray(_paHazeUVMap, TRUE);
    gfxBlendFunc(GFX_SRC_ALPHA, GFX_INV_SRC_ALPHA);
    gfxEnableBlend();
    
    // Set vertex color array for haze
    if (_pacolVtxHaze != NULL ) {
      gfxSetColorArray(_pacolVtxHaze);
    }

    // Render fog pass
    gfxDrawElements(_ctIndices, _paIndices);
  }

  // Restore texture wrapping set by the shader
  gfxSetTextureWrapping(_aShaderTexWrap[0], _aShaderTexWrap[1]);
}

// Modify color for fog
void shaModifyColorForFog(void)
{
  // If full bright then no fog colors
  if (shaGetFlags() & BASE_FULL_BRIGHT) {
    return;
  }

  // Update this surface color array if fog or haze exists
  shaPrepareFogAndHaze(FALSE);
}

// Calculate lightning for given model
void shaCalculateLight(void)
{
  // If full bright
  if (shaGetFlags()&BASE_FULL_BRIGHT) {
    GFXColor colLight = _colConstant;
    GFXColor colAmbient;
    GFXColor colConstant;
    
    // Is over brightning enabled
    if (shaOverBrightningEnabled()) {
      colAmbient = 0x7F7F7FFF;
    } else {
      colAmbient = 0xFFFFFFFF;
    }
    colConstant.MultiplyRGBA(colLight,colAmbient);
    shaSetConstantColor(ByteSwap32(colConstant.abgr));
    
    // No vertex colors
    return;
  }

  ASSERT(_paNormals != NULL);
  _acolVtxColors.PopAll();
  _acolVtxColors.Push(_ctVertices);

  GFXColor colModel     = (GFXColor)_colModel;   // Model color
  GFXColor &colAmbient  = (GFXColor)_colAmbient; // Ambient color
  GFXColor &colLight    = (GFXColor)_colLight;   // Light color
  GFXColor &colSurface  = (GFXColor)_colConstant; // shader color

  colModel.MultiplyRGBA(colModel,colSurface);

  UBYTE ubColShift = 8;
  SLONG slar = colAmbient.r;
  SLONG slag = colAmbient.g;
  SLONG slab = colAmbient.b;

  if (shaOverBrightningEnabled()) {
    slar = ClampUp(slar,127L);
    slag = ClampUp(slag,127L);
    slab = ClampUp(slab,127L);
    ubColShift = 8;
  } else {
    slar *= 2;
    slag *= 2;
    slab *= 2;
    ubColShift = 7;
  }

  // For each vertex color
  for (INDEX ivx = 0; ivx <_ctVertices; ivx++)
  {
    // Calculate vertex light
    FLOAT3D &vNorm = FLOAT3D(_paNormals[ivx].nx,_paNormals[ivx].ny,_paNormals[ivx].nz);
    FLOAT fDot = vNorm % _vLightDir;
    fDot = Clamp(fDot, 0.0f, 1.0f);
    SLONG slDot = NormFloatToByte(fDot);

    _acolVtxColors[ivx].r = ClampUp(colModel.r * (slar + ((colLight.r * slDot)>>ubColShift)) >> 8, 255L);
    _acolVtxColors[ivx].g = ClampUp(colModel.g * (slag + ((colLight.g * slDot)>>ubColShift)) >> 8, 255L);
    _acolVtxColors[ivx].b = ClampUp(colModel.b * (slab + ((colLight.b * slDot)>>ubColShift)) >> 8, 255L);
    _acolVtxColors[ivx].a = colModel.a;//slDot;
  }
  
  // Set current vertex color array 
  _pcolVtxColors = &_acolVtxColors[0];
}

// Calculate lightning for given model
void shaCalculateLightForSpecular(void)
{
  ASSERT(_paNormals != NULL);
  _acolVtxColors.PopAll();
  _acolVtxColors.Push(_ctVertices);

  GFXColor colModel   = (GFXColor)_colModel;   // Model color
  GFXColor &colAmbient = (GFXColor)_colAmbient; // Ambient color
  GFXColor &colLight   = (GFXColor)_colLight;   // Light color
  GFXColor &colSurface = (GFXColor)_colConstant; // shader color

  // colModel = MulColors(colModel.r,colSurface.abgr);
  colModel.MultiplyRGBA(colModel,colSurface);

  UBYTE ubColShift = 8;
  SLONG slar = colAmbient.r;
  SLONG slag = colAmbient.g;
  SLONG slab = colAmbient.b;

  if (shaOverBrightningEnabled())
  {
    slar = ClampUp(slar,127L);
    slag = ClampUp(slag,127L);
    slab = ClampUp(slab,127L);
    ubColShift = 8;
  } else {
    slar *= 2;
    slag *= 2;
    slab *= 2;
    ubColShift = 7;
  }

  // For each vertex color
  for (INDEX ivx = 0;ivx < _ctVertices; ivx++)
  {
    // Calculate vertex light
    FLOAT3D &vNorm = FLOAT3D(_paNormals[ivx].nx,_paNormals[ivx].ny,_paNormals[ivx].nz);
    FLOAT fDot = vNorm % _vLightDir;
    fDot = Clamp(fDot, 0.0f, 1.0f);
    SLONG slDot = NormFloatToByte(fDot);

    _acolVtxColors[ivx].r = ClampUp(colModel.r * (slar + ((colLight.r * slDot)>>ubColShift)) >> 8, 255L);
    _acolVtxColors[ivx].g = ClampUp(colModel.g * (slag + ((colLight.g * slDot)>>ubColShift)) >> 8, 255L);
    _acolVtxColors[ivx].b = ClampUp(colModel.b * (slab + ((colLight.b * slDot)>>ubColShift)) >> 8, 255L);
    _acolVtxColors[ivx].a = slDot;//colModel.a;//slDot;
  }
  
  // Set current wertex array 
  _pcolVtxColors = &_acolVtxColors[0];
}

// Clean all values
void shaClean(void)
{
  _ctVertices   = -1;
  _ctIndices    = -1;
  _ctColors     = -1;
  _ctTextures   = -1;
  _ctUVMaps     = -1;
  _ctLights     = -1;

  _colConstant   = 0;
  _ulFlags       = 0;
  _ulRenFlags    = 0;

  _pShader        = NULL;
  _paVertices     = NULL;
  _paNormals      = NULL;
  _paIndices      = NULL;
  _paUVMaps       = NULL;
  _paTextures     = NULL;
  _paColors       = NULL;
  _paFloats       = NULL;
  _pCurrentUVMap  = NULL;
  _pcolVtxColors  = NULL;

  _paFogUVMap     = NULL;
  _paHazeUVMap    = NULL;
  _pacolVtxHaze   = NULL;
  _pmObjToView    = NULL;
  _pmObjToViewStr = NULL;
  _pmObjToAbs     = NULL;
  _paprProjection = NULL;

  _acolVtxColors.PopAll();
  _acolVtxModifyColors.PopAll();
  _vModifyVertices.PopAll();
  _uvUVMapForModify.PopAll();
  shaCullFace(GFX_BACK);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Shader value setting

// Set array of vertices
void shaSetVertexArray(GFXVertex *paVertices,INDEX ctVertices)
{
  ASSERT(paVertices != NULL);
  ASSERT(ctVertices > 0);
  
  // Set pointer to new vertex array
  _paVertices = paVertices;
  _ctVertices = ctVertices;
}

// Set array of normals
void shaSetNormalArray(GFXNormal *paNormals)
{
  ASSERT(paNormals != NULL);

  _paNormals = paNormals;
}

// Set array of indices
void shaSetIndices(INDEX *paIndices,INDEX ctIndices)
{
  ASSERT(paIndices != NULL);
  ASSERT(ctIndices > 0);

  _paIndices = paIndices;
  _ctIndices = ctIndices;
}

// Set array of texture objects for shader
void shaSetTextureArray(CTextureObject **paTextureObject, INDEX ctTextures)
{
  _paTextures = paTextureObject;
  _ctTextures = ctTextures;
}

// Set array of uv maps
void shaSetUVMapsArray(GFXTexCoord **paUVMaps, INDEX ctUVMaps)
{
  ASSERT(paUVMaps != NULL);
  ASSERT(ctUVMaps > 0);

  _paUVMaps = paUVMaps;
  _ctUVMaps = ctUVMaps;
}

// Set array of shader colors
void shaSetColorArray(COLOR *paColors, INDEX ctColors)
{
  ASSERT(paColors!=NULL);
  ASSERT(ctColors>0);

  _paColors = paColors;
  _ctColors = ctColors;
}

// Set array of floats for shader
void shaSetFloatArray(FLOAT *paFloats, INDEX ctFloats)
{
  ASSERT(paFloats != NULL);
  _paFloats = paFloats;
  _ctFloats = ctFloats;
}

// Set shading flags
void shaSetFlags(ULONG ulFlags)
{
  _ulFlags = ulFlags;
}

// Set base color of model 
void shaSetModelColor(COLOR &colModel)
{
  _colModel = colModel;
}

// Set light direction
void shaSetLightDirection(const FLOAT3D &vLightDir)
{
  _vLightDir = vLightDir;
}

// Set light color
void shaSetLightColor(COLOR colAmbient, COLOR colLight)
{
  _colAmbient = colAmbient;
  _colLight = colLight;
}

// Set object to view matrix
void shaSetObjToViewMatrix(Matrix12 &mat)
{
  _pmObjToView = &mat;
}

// Set object to view matrix
void shaSetObjToViewStrMatrix(Matrix12 &mat)
{
  _pmObjToViewStr = &mat;
}

// Set object to abs matrix
void shaSetObjToAbsMatrix(Matrix12 &mat)
{
  _pmObjToAbs = &mat;
}

// Set current texture index
void shaSetTexture(INDEX iTextureIndex)
{
  if (_paTextures == NULL || iTextureIndex < 0 || iTextureIndex>=_ctTextures || _paTextures[iTextureIndex] == NULL) {
    gfxDisableTexture();
    return;
  }
  ASSERT(iTextureIndex < _ctTextures);

  CTextureObject *pto = _paTextures[iTextureIndex];
  ASSERT(pto != NULL);

  CTextureData *pTextureData = (CTextureData*)pto->GetData();
  const INDEX iFrameNo = pto->GetFrame();
  pTextureData->SetAsCurrent(iFrameNo);
}

// Set current uvmap index
void shaSetUVMap(INDEX iUVMapIndex)
{
  ASSERT(iUVMapIndex >= 0);
  if (iUVMapIndex <= _ctUVMaps) {
    _pCurrentUVMap = _paUVMaps[iUVMapIndex];
  }
}

// Set current color index
void shaSetColor(INDEX icolIndex)
{
  ASSERT(icolIndex >= 0);

  if (icolIndex >= _ctColors) {
    _colConstant = C_WHITE | CT_OPAQUE;
  } else {
    _colConstant = _paColors[icolIndex];
  }
  
  // Set this color as constant color
  gfxSetConstantColor(_colConstant);
}

// Set array of texcoords index
void shaSetTexCoords(GFXTexCoord *uvNewMap)
{
  _pCurrentUVMap = uvNewMap;
}

// Set array of vertex colors
void shaSetVertexColors(GFXColor *paColors)
{
  _pcolVtxColors = paColors;
}

// Set constant color
void shaSetConstantColor(const COLOR colConstant)
{
  gfxSetConstantColor(colConstant);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Shader value getting

// Get vertex count
INDEX shaGetVertexCount(void)
{
  return _ctVertices;
}

// Get index count
INDEX shaGetIndexCount(void)
{
  return _ctIndices;
}

// Get float from array of floats
FLOAT shaGetFloat(INDEX iFloatIndex)
{
  ASSERT(iFloatIndex >= 0);
  ASSERT(iFloatIndex < _ctFloats);
  return _paFloats[iFloatIndex];
}

// Get texture from array of textures
CTextureObject *shaGetTexture(INDEX iTextureIndex)
{
  ASSERT(iTextureIndex >= 0);
  if (_paTextures == NULL || iTextureIndex >= _ctTextures || _paTextures[iTextureIndex] == NULL) {
    return NULL;
  } else {
    return _paTextures[iTextureIndex];
  }
}

// Get color from color array
COLOR &shaGetColor(INDEX iColorIndex)
{
  ASSERT(iColorIndex < _ctColors);
  return _paColors[iColorIndex];
}

// Get shading flags
ULONG &shaGetFlags()
{
  return _ulFlags;
}

// Get base color of model
COLOR &shaGetModelColor(void)
{
  return _colModel;
}

// Get light direction
FLOAT3D &shaGetLightDirection(void)
{
  return _vLightDir;
}

// Get current light color
COLOR &shaGetLightColor(void)
{
  return _colLight.abgr;
}

// Get current ambient volor
COLOR &shaGetAmbientColor(void)
{
  return _colAmbient.abgr;
}

// Get current set color
COLOR &shaGetCurrentColor(void)
{
  return _colConstant;
}

// Get vertex array
GFXVertex *shaGetVertexArray(void)
{
  return _paVertices;
}

// Get index array
INDEX *shaGetIndexArray(void)
{
  return _paIndices;
}

// Get normal array
GFXNormal *shaGetNormalArray(void)
{
  return _paNormals;
}

// Get uvmap array from array of uvmaps
GFXTexCoord *shaGetUVMap(INDEX iUVMapIndex)
{
  ASSERT(iUVMapIndex >= 0);
  if (iUVMapIndex >= _ctUVMaps) {
    return NULL;
  } else {
    return _paUVMaps[iUVMapIndex];
  }
}

// Get color array
GFXColor *shaGetColorArray(void)
{
  return &_acolVtxColors[0];
}

// Get empty color array for modifying
GFXColor *shaGetNewColorArray(void)
{
  ASSERT(_ctVertices != 0);
  _acolVtxModifyColors.PopAll();
  _acolVtxModifyColors.Push(_ctVertices);
  return &_acolVtxModifyColors[0];
}

// Get empty texcoords array for modifying
GFXTexCoord *shaGetNewTexCoordArray(void)
{
  ASSERT(_ctVertices != 0);
  _uvUVMapForModify.PopAll();
  _uvUVMapForModify.Push(_ctVertices);
  return &_uvUVMapForModify[0];
}

// Get empty vertex array for modifying
GFXVertex *shaGetNewVertexArray(void)
{
  ASSERT(_ctVertices!=0);
  _vModifyVertices.PopAll();
  _vModifyVertices.Push(_ctVertices);
  return &_vModifyVertices[0];
}

// Get current projection
CAnyProjection3D *shaGetProjection()
{
  return _paprProjection;
}

// Get object to view matrix
Matrix12 *shaGetObjToViewMatrix(void)
{
  ASSERT(_pmObjToView != NULL);
  return _pmObjToView;
}

// Get object to view stretch matrix
Matrix12 *shaGetObjToViewStrMatrix(void)
{
  ASSERT(_pmObjToViewStr != NULL);
  return _pmObjToViewStr;
}

// Get object to abs matrix
Matrix12 *shaGetObjToAbsMatrix(void)
{
  ASSERT(_pmObjToAbs != NULL);
  return _pmObjToAbs;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Shader states

// Set face culling
void shaCullFace(GfxFace eFace)
{
  if (_paprProjection != NULL && (*_paprProjection)->pr_bMirror) {
    gfxFrontFace(GFX_CW);
  } else {
    gfxFrontFace(GFX_CCW);
  }
  gfxCullFace(eFace);
}

// Set blending operations
void shaBlendFunc(GfxBlend eSrc, GfxBlend eDst)
{
  gfxBlendFunc(eSrc,eDst);
}

// Set texture modulation mode
void shaSetTextureModulation(INDEX iScale) 
{
  gfxSetTextureModulation(iScale);
}

// Enable blening 
void shaEnableBlend(void)
{
  gfxEnableBlend();
}

// Disable blening 
void shaDisableBlend(void)
{
  gfxDisableBlend();
}

// Enable alpha test
void shaEnableAlphaTest(void)
{
  gfxEnableAlphaTest();
}

// Disable alpha test
void shaDisableAlphaTest(void)
{
  gfxDisableAlphaTest();
}

// Enable depth test
void shaEnableDepthTest(void)
{
  gfxEnableDepthTest();
}

// Disable depth test
void shaDisableDepthTest(void)
{
  gfxDisableDepthTest();
}

// Enable depth write
void shaEnableDepthWrite(void)
{
  gfxEnableDepthWrite();
}

// Disable depth write
void shaDisableDepthWrite(void)
{
  gfxDisableDepthWrite();
}

// Set depth buffer compare mode
void shaDepthFunc(GfxComp eComp)
{
  gfxDepthFunc(eComp);
}

// Set texture wrapping 
void shaSetTextureWrapping(enum GfxWrap eWrapU, enum GfxWrap eWrapV)
{
  // Remember shader's texture wrapping
  _aShaderTexWrap[0] = eWrapU;
  _aShaderTexWrap[1] = eWrapV;

  gfxSetTextureWrapping(eWrapU, eWrapV);
}


// Set object rendering flags (fog,haze,etc)
void shaSetRenFlags(ULONG ulRenFlags)
{
  _ulRenFlags = ulRenFlags;
}

// Set uvmap for fog
void shaSetFogUVMap(GFXTexCoord *paFogUVMap)
{
  _paFogUVMap = paFogUVMap;
}

// Set uvmap for haze
void shaSetHazeUVMap(GFXTexCoord *paHazeUVMap)
{
  _paHazeUVMap = paHazeUVMap;
}

// Set array of vertex colors used in haze
void shaSetHazeColorArray(GFXColor *paHazeColors)
{
  _pacolVtxHaze = paHazeColors;
}

BOOL shaOverBrightningEnabled(void)
{
  // Determine multitexturing capability for overbrighting purposes
  extern INDEX mdl_bAllowOverbright;
  return mdl_bAllowOverbright && _pGfx->gl_ctTextureUnits > 1;
}
