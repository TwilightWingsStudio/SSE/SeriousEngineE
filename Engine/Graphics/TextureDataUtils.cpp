/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/Texture.h>

#include <Core/IO/Stream.h>
#include <Core/Base/Timer.h>
#include <Core/Base/Console.h>
#include <Core/Math/Functions.h>
#include <Core/Graphics/Image.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/ImageInfo.h>
#include <Engine/Graphics/TextureEffects.h>

#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Resources/ResourceManager.h>

#include <Core/Base/Statistics_Internal.h>

extern BOOL _bTextureExportMode;

// Routine that creates one-frame-texture from loaded picture thru image info structure
void CTextureData::Create_t(const CImageInfo *pII, MEX mexWanted, INDEX ctFineMips, BOOL bForce32bit)
{
  // Check for supported image format
  _bTextureExportMode = FALSE;
  ASSERT(pII->ii_BitsPerPixel == 24 || pII->ii_BitsPerPixel == 32);
  
  if (pII->ii_BitsPerPixel != 24 && pII->ii_BitsPerPixel != 32) {
    throw(TRANS("Only 24-bit and 32-bit pictures can be processed."));
  }

  // Get picture data
  PIX pixSizeU = pII->ii_Width;
  PIX pixSizeV = pII->ii_Height;

  // Check maximum supported texture dimension
  if (pixSizeU>MAX_MEX || pixSizeV>MAX_MEX) {
    throw(TRANS("At least one of texture dimensions is too large."));
  }

  // Determine physical (maximum) number of mip-levels
  INDEX iSizeULog2 = FastLog2(pixSizeU);
  INDEX iSizeVLog2 = FastLog2(pixSizeV);
  ASSERT((1UL << iSizeULog2) == pixSizeU && (1UL << iSizeVLog2) == pixSizeV);

  // Dimension in mexels must not be smaller than the one in pixels
  ASSERT(pixSizeU <= mexWanted);

  // Determine mip index from mex size
  td_iFirstMipLevel = FastLog2(mexWanted / pixSizeU);

  // Initiate proper flags
  td_ulFlags = NONE;
  if (pII->ii_BitsPerPixel == 32) {
    td_ulFlags |= TEX_ALPHACHANNEL;
  }
  
  if (bForce32bit) {
    td_ulFlags |= TEX_32BIT;
  }

  // Initialize general TextureData members
  td_ctFrames  = 0;
  td_mexWidth  = pixSizeU << td_iFirstMipLevel;
  td_mexHeight = pixSizeV << td_iFirstMipLevel;

  // Create all mip levels (either bilinear or downsampled)
  INDEX ctMipLevels  = GetNoOfMipmaps(pixSizeU, pixSizeV);
  td_ctFineMipLevels = Min(ctFineMips, ctMipLevels);
  
  // Get frame size (includes only one mip-map)
  td_slFrameSize = GetMipmapOffset(15, pixSizeU, pixSizeV) * BYTES_PER_TEXEL;

  // Allocate small ammount of memory just for Realloc sake
  td_pulFrames = (ULONG*)AllocMemory(16);
  AddFrame_t(pII);
}

// Routine that adds one-frame to texture from one picture
void CTextureData::AddFrame_t(const CImageInfo *pII)
{
  // Check for supported image format
  ASSERT(pII->ii_BitsPerPixel == 24 || pII->ii_BitsPerPixel == 32);
  
  if (pII->ii_BitsPerPixel != 24 && pII->ii_BitsPerPixel != 32) {
    throw(TRANS("Only 24-bit and 32-bit pictures can be processed."));
  }
  PIX pixWidth  = pII->ii_Width;
  PIX pixHeight = pII->ii_Height;

  // Frame that is about to be added must have the same dimensions as the texture
  ASSERT(pixWidth  == GetPixWidth()  );
  ASSERT(pixHeight == GetPixHeight() );
  if (pixWidth != GetPixWidth()) {
    throw(TRANS("Incompatible frame width."));
  }
  
  if (pixHeight != GetPixHeight()) {
    throw(TRANS("Incompatible frame height."));
  }

  // Add memory for new frame
  SLONG slFramesSize = td_slFrameSize * td_ctFrames;
  GrowMemory((void**)&td_pulFrames, slFramesSize + td_slFrameSize);

  // Add new frame to the end of the previous texture frames
  PIX    pixMipmapSize   = pixWidth*pixHeight;
  ULONG *pulCurrentFrame = td_pulFrames + slFramesSize/BYTES_PER_TEXEL;

  if (td_ulFlags & TEX_ALPHACHANNEL) {
    // Has alpha channel - do simple copying
    memcpy(pulCurrentFrame, pII->ii_Picture, pixMipmapSize * 4);
  } else {
    // Hasn't got alpha channel - do conversion from 24-bit bitmap to 32-bit format
    memcpy(pulCurrentFrame, pII->ii_Picture, pixMipmapSize * 3);
    AddAlphaChannel((UBYTE*)pulCurrentFrame, (ULONG*)pulCurrentFrame, pixMipmapSize);
  }
  
  // Make mipmaps (in place!)
  MakeMipmaps(td_ctFineMipLevels, pulCurrentFrame, pixWidth,pixHeight);

  // Increase number of frames
  td_ctFrames++;
}

// Export finest mipmap of one texture's frame to imageinfo
void CTextureData::ExportFrame_t(class CImageInfo &iiExportedImage, INDEX iFrame)
{
  // Check for right frame number and non-effect texture type
  ASSERT(iFrame < td_ctFrames && td_ptegEffect == NULL);
  if (iFrame >= td_ctFrames) {
    throw(TRANS("Texture frame that is to be exported doesn't exist."));
  }

  // Reload without modifications
  _bTextureExportMode = TRUE;
  Reload();
  ASSERT(td_pulFrames != NULL);

  // Prepare miplevel and mipmap offset
  PIX pixWidth  = GetPixWidth();
  PIX pixHeight = GetPixHeight();
  
  // Export header to image info structure
  iiExportedImage.Clear();
  iiExportedImage.ii_Width  = pixWidth;
  iiExportedImage.ii_Height = pixHeight;
  iiExportedImage.ii_BitsPerPixel = (td_ulFlags & TEX_ALPHACHANNEL) ? 32 : 24;

  // Prepare the texture for exporting (with or without alpha channel)
  ULONG *pulFrame = td_pulFrames + td_slFrameSize * iFrame / BYTES_PER_TEXEL;
  PIX  pixMipSize = pixWidth * pixHeight;
  SLONG slMipSize = pixMipSize * iiExportedImage.ii_BitsPerPixel / 8;
  iiExportedImage.ii_Picture = (UBYTE*)AllocMemory(slMipSize);
  
  // Export frame
  if (td_ulFlags & TEX_ALPHACHANNEL) {
    memcpy(iiExportedImage.ii_Picture, pulFrame, slMipSize);
  } else {
    RemoveAlphaChannel(pulFrame, iiExportedImage.ii_Picture, pixMipSize);
  }

  // Reload as it was
  _bTextureExportMode = FALSE;
  Reload();
}

// Export finest mipmap of one texture's frame to imageinfo
void CTextureData::ExportFrame_t(class CImage &iExportedImage, INDEX iFrame)
{
  /*
  // Reload without modifications
  _bTextureExportMode = TRUE;
  Reload();
  ASSERT(td_pulFrames != NULL);
  
  // Prepare miplevel and mipmap offset
  const BOOL bHasAlpha = td_ulFlags & TEX_ALPHACHANNEL;
  const EColorFormat eFormat = bHasAlpha ? ECF_RGBA : ECF_RGB;
  const PIX pixWidth  = GetPixWidth();
  const PIX pixHeight = GetPixHeight();
  const SLONG slMipSize = bHasAlpha ? (pixWidth * pixHeight * 4) : (pixWidth * pixHeight * 3);
  iExportedImage.Clear();

  ULONG *pulFrame = td_pulFrames + td_slFrameSize * iFrame / BYTES_PER_TEXEL;
  UBYTE *pBuffer = (UBYTE *)AllocMemory(slMipSize);

  // Export frame
  if (td_ulFlags & TEX_ALPHACHANNEL) {
    memcpy(pBuffer, pulFrame, slMipSize);
  } else {
    RemoveAlphaChannel(pulFrame, pBuffer, pixWidth * pixHeight);
  }

  iExportedImage.Attach(pBuffer, pixWidth, pixHeight, eFormat);

  // Reload as it was
  _bTextureExportMode = FALSE;
  Reload();
  */
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation of independent routines

#define EQUAL_SUB_STR(str) (strnicmp(ld_line, str, strlen(str))==0)

//! Process texture creation script.
void ProcessScript_t(const CTFileName &inFileName)
{
  CTFileStream File;
	char ld_line[128];
	char err_str[256];
  FLOAT fTextureWidthMeters = 2.0f;
  INDEX TexMipmaps = MAX_MEX_LOG2;
  CTextureData tex;
	CListHead FrameNamesList;
  INDEX NoOfDataFound = 0;
  BOOL bForce32bit = FALSE;

	File.Open_t(inFileName, CTStream::OM_READ);    // open script file for text reading

  FOREVER
	{
		do {
      File.GetLine_t(ld_line, 128);
    }	while ((strlen(ld_line) == 0) || (ld_line[0] == ';'));

		_strupr(ld_line);

    // Specified width of texture
    if (EQUAL_SUB_STR("TEXTURE_WIDTH")) {
      sscanf(ld_line, "TEXTURE_WIDTH %g", &fTextureWidthMeters);
      NoOfDataFound ++;
    
    // How many mip-map levels will texture have
    } else if (EQUAL_SUB_STR("TEXTURE_MIPMAPS")) {
      sscanf(ld_line, "TEXTURE_MIPMAPS %d", &TexMipmaps);
    
    // Should texture be forced to keep 32-bit quality even that 16-bit textures are set
    } else if (EQUAL_SUB_STR("TEXTURE_32BIT")) {
      bForce32bit = TRUE;
    
    // Key-word "ANIM_START" starts loading of Animation Data object
    } else if (EQUAL_SUB_STR("ANIM_START")) {
      tex.LoadFromScript_t(&File, &FrameNamesList);
      NoOfDataFound ++;
      
    // Key-word "END" ends infinite loop and script loading is over
		} else if (EQUAL_SUB_STR("END")) {
      break;
    
    // If none of known key-words isn't recognised, throw error
    } else {
      sprintf(err_str,
        TRANS("Unidentified key-word found (line: \"%s\") or unexpected end of file reached."), ld_line);
      throw(err_str);
		}
  }
  
  if (NoOfDataFound != 2) {
    throw(TRANS("Required key-word(s) has not been specified in script file:\nTEXTURE_WIDTH and/or ANIM_START"));
  }

  // Now we will create texture file form read script data
	CImageInfo   inPic;
  CTFileName   outFileName;
  CTFileStream outFile;

  // Load first picture
  CFileNameNode *pFirstFNN = LIST_HEAD(FrameNamesList, CFileNameNode, cfnn_Node);
  inPic.LoadAnyGfxFormat_t(CTString(pFirstFNN->cfnn_FileName));

  // Create texture with one frame
  tex.Create_t(&inPic, MEX_METERS(fTextureWidthMeters), TexMipmaps, bForce32bit);
  inPic.Clear();

  // Process rest of the frames in animation (if any)
  INDEX i = 0;
  FOREACHINLIST(CFileNameNode, cfnn_Node, FrameNamesList, it1)
  {
    if (i != 0) {   // we have to skip first picture since it has already been done
      inPic.LoadAnyGfxFormat_t(CTString(it1->cfnn_FileName));
      
      // Add picture as next frame in texture
      tex.AddFrame_t(&inPic);
      inPic.Clear();
    }
    i++;
  }
  
  // Save texture
  outFileName = inFileName.FileDir() + inFileName.FileName() + ".TEX";
  tex.Save_t(outFileName);

  // Clear list
  FORDELETELIST(CFileNameNode, cfnn_Node, FrameNamesList, itDel) 
  {
    delete &itDel.Current();
  }
}

//! Create texture from file and save it in specified location.
void CreateTexture_t(const CTFileName &inFileName, const CTFileName &outFileName, MEX inMex, INDEX inMipmaps,
                     BOOL bForce32bit)
{
  if (inFileName.FileExt() == ".SCR") {
    // Input is a script file
    ProcessScript_t(inFileName);
  } else {
    
    // Input is a picture file (PCX or TGA)
    CAnimData    anim;
    CTextureData tex;
    CImageInfo   inPic;

    // Mex must be specified and valid
    if ((inMex <= 0)) {
      throw(TRANS("Invalid or unspecified mexel units."));
    }

    // Load picture
    inPic.LoadAnyGfxFormat_t(inFileName);

    // Create texture
    tex.Create_t(&inPic, inMex, inMipmaps, bForce32bit);

    // No more need for picture - get out!
    inPic.Clear();

    // Save texture to file
    tex.Save_t(outFileName);
  }
}

//! Create texture from file and save it in the same location.
void CreateTexture_t(const CTFileName &inFileName, MEX inMex, INDEX inMipmaps, BOOL bForce32bit)
{
  CTFileName outFileName = inFileName.FileDir() + inFileName.FileName() + ".TEX";
  CreateTexture_t(inFileName, outFileName, inMex, inMipmaps, bForce32bit);
}
