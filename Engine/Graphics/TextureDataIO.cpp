/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Graphics/Texture.h>

#include <Core/IO/Stream.h>
#include <Core/Base/Timer.h>
#include <Core/Base/Console.h>
#include <Core/Math/Functions.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/ImageInfo.h>
#include <Engine/Graphics/TextureEffects.h>

#include <Core/Templates/DynamicArray.h>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Resources/ChaosCipher.h>
#include <Engine/Resources/ResourceManager.h>

#include <Core/Base/Statistics_Internal.h>
#include <Core/Graphics/S3TC.h>

extern INDEX tex_iDithering;
extern INDEX tex_iFiltering;       

extern INDEX gap_bAllowSingleMipmap;

extern BOOL _bTextureExportMode;

// Remove mipmaps from texture that are not needed (exceeds maximum supported dimension)
static void RemoveOversizedMipmaps(CTextureData *pTD)
{
  // If this is an effect texture, leave as it is
  if (pTD->td_ptegEffect != NULL) {
    return;
  }

  pTD->td_ulFlags &= ~TEX_DISPOSED;

  // Determine and clamp to max allowed texture dimension and size
  PIX pixClampAreaSize = (pTD->td_ctFrames > 1) ? TS.ts_pixAnimSize : TS.ts_pixNormSize;
  
  // Constant textures doesn't need clamping to area, but still must be clamped to max HW dimension!
  if (pTD->td_ulFlags & TEX_CONSTANT) {
    pixClampAreaSize = 4096 * 4096; 
  }

  // Determine dimensions of finest mip-map
  PIX pixSizeU = pTD->GetPixWidth();
  PIX pixSizeV = pTD->GetPixHeight();
  
  // Determine number of mip-maps to skip
  INDEX ctSkipMips = ClampTextureSize(pixClampAreaSize, _pGfx->gl_pixMaxTextureDimension, pixSizeU, pixSizeV);
  
  // Return if no need to remove mip-maps
  if (ctSkipMips == 0) {
    return;
  }
  
  // Check for mip overhead
  INDEX ctMips = GetNoOfMipmaps(pixSizeU, pixSizeV);
  while (ctMips <= ctSkipMips)
  {
    ctSkipMips--;
  }
  
  // Determine memory size and allocate memory for rest mip-maps
  SLONG slRemovedMipsSize, slOldFrameSize, slNewFrameSize;
  UBYTE *pubNewFrames, *pubOldFrame, *pubNewFrame;

  if (pTD->td_ulFlags & TEX_COMPRESSED) 
  {
    // cannot remove mipmaps if one dimension ends up less 4 pixels
    ASSERT((pixSizeU >> ctSkipMips) >= 4 && (pixSizeV >> ctSkipMips) >= 4);
    if ((pixSizeU >> ctSkipMips) < 4 || (pixSizeV >> ctSkipMips) < 4) {
      return;
    }

    pubOldFrame = (UBYTE*)pTD->td_pulFrames;

    for (INDEX iMip = 0; iMip < ctSkipMips; iMip++) 
    {
      const SLONG slMipSize = *(ULONG*)pubOldFrame;
      pubOldFrame += slMipSize + 4;
    }

    slOldFrameSize = pTD->td_slFrameSize;   
    size_t newFrameSize = slOldFrameSize - ((size_t)pubOldFrame - (size_t)pTD->td_pulFrames); // deal with pointers carefully!
    slNewFrameSize = newFrameSize;
    pubNewFrames = (UBYTE*)AllocMemory( slNewFrameSize * pTD->td_ctFrames);
    pubNewFrame  = pubNewFrames;

  // non-compressed textures
  } else {
    slRemovedMipsSize = GetMipmapOffset(ctSkipMips, pixSizeU, pixSizeV) * BYTES_PER_TEXEL;
    slOldFrameSize = pTD->td_slFrameSize;   
    slNewFrameSize = slOldFrameSize - slRemovedMipsSize;
    pubNewFrames = (UBYTE*)AllocMemory( slNewFrameSize * pTD->td_ctFrames);
    pubOldFrame  = ((UBYTE*)pTD->td_pulFrames) + slRemovedMipsSize;
    pubNewFrame  = pubNewFrames;
  }

  // Copy only needed mip-maps from each frame
  for (INDEX iFr = 0; iFr < pTD->td_ctFrames; iFr++)
  {
    memcpy(pubNewFrame, pubOldFrame, slNewFrameSize);
    pubNewFrame += slNewFrameSize;
    pubOldFrame += slOldFrameSize;
  }

  // Free old frames memory
  FreeMemory(pTD->td_pulFrames);
  
  // Adjust texture parameters
  pTD->td_pulFrames       = (ULONG *)pubNewFrames;
  pTD->td_slFrameSize     = slNewFrameSize;
  pTD->td_iFirstMipLevel += ctSkipMips;
  pTD->td_ctFineMipLevels = ClampDn((INDEX)(pTD->td_ctFineMipLevels-ctSkipMips), (INDEX)1);

  // Mark that this texture had some mip maps disposed
  pTD->td_ulFlags |= TEX_DISPOSED;
}

//! Returns format in what texture will be uploaded (regarding console vars)
static ULONG DetermineInternalFormat(CTextureData *pTD)
{
  ASSERT(!(pTD->td_ulFlags & TEX_COMPRESSED));
  if( pTD->td_ulFlags & TEX_COMPRESSED) return NONE;
  
  // Cache some vars
  extern INDEX gap_bAllowGrayTextures;
  BOOL bGrayTexture  = gap_bAllowGrayTextures && (pTD->td_ulFlags&TEX_GRAY);
  BOOL bAlphaChannel = pTD->td_ulFlags & TEX_ALPHACHANNEL;
  PIX  pixTexSize    = pTD->GetPixWidth() * pTD->GetPixHeight();

  // choose internal texture format for alpha textures
  INDEX iQuality;
  ULONG ulInternalFormat;
  if (bAlphaChannel)
  {
    iQuality = pTD->td_ctFrames > 1 ? TS.ts_iAnimQualityA : TS.ts_iNormQualityA;
    ulInternalFormat = TS.ts_tfRGBA4;
    switch (iQuality)
    {
      // Uploaded optimally
      case 0: {
        if (pTD->td_ulFlags & TEX_32BIT) {
          ulInternalFormat = TS.ts_tfRGBA8;  
        }
      } break;
 
      // Uploaded as 16 bit (default)        
      case 1:  
        break;
         
      // Uploaded as 32 bit or compressed
      case 2:
      case 3: { 
        ulInternalFormat = TS.ts_tfRGBA8;
      } break;
    
      default: {
        ASSERTALWAYS("Unexpected texture type found."); 
      } break;
    }
    
    // Adjust quality by size
    if (pixTexSize <= 32 * 32 && ulInternalFormat == TS.ts_tfRGBA4) {
      ulInternalFormat = TS.ts_tfRGBA8;
    }
    
    // Do eventual adjustment of internal format for grayscale textures
    if (bGrayTexture) {
      ulInternalFormat = TS.ts_tfLA8;
    }
    
    // Handle case of forced internal format (for texture cration process only!)
    if (_iTexForcedQuality == 16) {
      ulInternalFormat = TS.ts_tfRGBA4;
    }
    
    if (_iTexForcedQuality == 32) {
      ulInternalFormat = TS.ts_tfRGBA8;
    }
    
    // Do eventual adjustment of transparent textures
    if ((pTD->td_ulFlags & TEX_TRANSPARENT) && ulInternalFormat == TS.ts_tfRGBA4) {
      ulInternalFormat = TS.ts_tfRGB5A1;
    }
    
  // Choose internal texture format for opaque textures
  } else {
    iQuality = pTD->td_ctFrames > 1 ? TS.ts_iAnimQualityO : TS.ts_iNormQualityO;
    ulInternalFormat = TS.ts_tfRGB5;
    switch (iQuality)
    {
      // Uploaded optimally
      case 0: {
        if (pTD->td_ulFlags & TEX_32BIT) {
          ulInternalFormat = TS.ts_tfRGB8;
        }
      } break;  
      
      // Uploaded as 16 bit (default)
      case 1: 
        break; 
      // Uploaded as 32 bit or compressed
      case 2:  
      case 3: {
        ulInternalFormat = TS.ts_tfRGB8;
      } break;
      
      default: {
        ASSERTALWAYS("Unexpected texture type found.");  
      } break;
    }
    
    // Adjust quality by size
    if (pixTexSize <= 32 * 32 && ulInternalFormat == TS.ts_tfRGB5) {
      ulInternalFormat = TS.ts_tfRGB8;
    }
    
    // Do eventual adjustment of internal format for grayscale textures
    if (bGrayTexture) {
      ulInternalFormat = TS.ts_tfL8;
    }
    
    // Handle case of forced internal format (for texture cration process only!)
    if (_iTexForcedQuality == 16) {
      ulInternalFormat = TS.ts_tfRGB5;
    }
    
    if (_iTexForcedQuality == 32) {
      ulInternalFormat = TS.ts_tfRGB8;
    }
  }

  // Adjust format to compressed if needed and allowed
  if (iQuality == 3 && pixTexSize >= 64 * 64)
  {
    if (ulInternalFormat == TS.ts_tfRGB8 || ulInternalFormat == TS.ts_tfRGB5 || ulInternalFormat == TS.ts_tfRGB5A1) {
      ulInternalFormat = TS.ts_tfCRGB;
    }
    
    if (ulInternalFormat==TS.ts_tfRGBA8 || ulInternalFormat==TS.ts_tfRGBA4) {
      ulInternalFormat = TS.ts_tfCRGBA;
    }
  }
  
  // All done
  return ulInternalFormat;
}

// Routine that performs texture conversion to current texture format (version 4)
static void Convert(CTextureData *pTD)
{
  // Skip effect textures
  if (pTD->td_ptegEffect != NULL) {
    return;
  }

  // Determine dimensions 
  PIX pixWidth     = pTD->GetPixWidth();
  PIX pixHeight    = pTD->GetPixHeight();
  PIX pixMipSize   = pixWidth * pixHeight;
  PIX pixFrameSize = GetMipmapOffset(15, pixWidth, pixHeight);
  
  // Allocate memory for new texture
  ULONG *pulFramesNew = (ULONG*)AllocMemory(pixFrameSize * pTD->td_ctFrames * BYTES_PER_TEXEL);
  UWORD *puwFramesOld = (UWORD*)pTD->td_pulFrames;
  ASSERT(puwFramesOld != NULL);

  // Determine alpha channel presence
  BOOL bHasAlphaChannel = pTD->td_ulFlags & TEX_ALPHACHANNEL;

  // Unpack texture from 16-bit RGBA4444 or RGBA5551 format to RGBA8888 32-bit format
  UBYTE r,g,b,a;
  
  // For each frame
  for (INDEX iFr = 0; iFr < pTD->td_ctFrames; iFr++)
  {
    // Get addresses of current frames (new and old)
    PIX pixFrameOffset = iFr * pixFrameSize;
    
    // For each pixel
    for (INDEX iPix=0; iPix<pixMipSize; iPix++)
    {
      // Read 16-bit pixel
      UWORD uwPix = puwFramesOld[pixFrameOffset+iPix];
      // Unpack it
      if (bHasAlphaChannel)
      {
        // With alpha channel
        r = (uwPix & 0xF000) >> 8;
        g = (uwPix & 0x0F00) >> 4;
        b = (uwPix & 0x00F0) >> 0;
        a = (uwPix & 0x000F) << 4;
        
        // Adjust strength
        r |= r >> 4;
        g |= g >> 4;
        b |= b >> 4;
        a |= a >> 4;
      } else {
        // Without alpha channel
        r = (uwPix & 0xF800) >> 8;
        g = (uwPix & 0x07C0) >> 3;
        b = (uwPix & 0x003E) << 2;
        a = 0xFF;
        
        // Adjust strength
        r |= r >> 5;
        g |= g >> 5;
        b |= b >> 5;
      }

      // Pack it back to 32-bit
      ULONG ulPix = RGBAToColor(r,g,b,a);
      
      // Store 32-bit pixel
      pulFramesNew[pixFrameOffset+iPix] = ByteSwap32(ulPix);
    }
  }

  // Free old memory
  FreeMemory(pTD->td_pulFrames);
  
  // Remember new texture parameters
  pTD->td_pulFrames   = pulFramesNew;
  pTD->td_slFrameSize = pixFrameSize * BYTES_PER_TEXEL;
}

// Internal routines for texture::read routine

// Test mipmap if it can be transparent
#define TRANS_TRESHOLD 7
static BOOL IsTransparent(ULONG *pulMipmap, INDEX pixMipSize)
{
  COLOR col;
  ULONG ulA;
  
  // Determine transparency
  for (INDEX iPix = 0; iPix < pixMipSize; iPix++)
  {
    col = ByteSwap32(pulMipmap[iPix]);
    ulA = (col & CT_AMASK) >> CT_ASHIFT;
    if (ulA > TRANS_TRESHOLD && ulA < (255 - TRANS_TRESHOLD)) {
      return FALSE;
    }
  }
  
  // Transparent!
  return TRUE;
}

// Test mipmap whether it is grayscaled
static BOOL IsGray(ULONG *pulMipmap, INDEX pixMipSize)
{
  // Loop thru texels
  for (INDEX iPix = 0; iPix < pixMipSize; iPix++)
  {
    COLOR col = ByteSwap32(pulMipmap[iPix]);
    if (!IsGray(col)) {
      return FALSE; // colored
    }
  }
  // Grayscaled
  return TRUE;
}

// Test mipmap if it can be equilized
#define EQUALIZER_TRESHOLD 3
static BOOL IsEqualized(ULONG *pulMipmap, INDEX pixMipSize)
{
  // Determine components and calc averages
  COLOR col;
  ULONG ulR = 0, ulG = 0, ulB = 0;
  for (INDEX iPix = 0; iPix < pixMipSize; iPix++)
  {
    col  = ByteSwap32(pulMipmap[iPix]);
    ulR += (col & CT_RMASK) >> CT_RSHIFT;
    ulG += (col & CT_GMASK) >> CT_GSHIFT;
    ulB += (col & CT_BMASK) >> CT_BSHIFT;
  }
  ulR /= pixMipSize;
  ulG /= pixMipSize;
  ulB /= pixMipSize;
  const ULONG ulLoEdge = 127 - EQUALIZER_TRESHOLD;
  const ULONG ulHiEdge = 128 + EQUALIZER_TRESHOLD;
  BOOL bEqulized = FALSE;
  if (ulR>ulLoEdge && ulR < ulHiEdge && ulG > ulLoEdge && ulG < ulHiEdge && ulB > ulLoEdge && ulB < ulHiEdge) {
    bEqulized = TRUE;
  }
  return bEqulized;
}

// Reads 32/24-bit texture from file and eventually converts it to 8-bit pixel format
void CTextureData::Read_t(CTStream *inFile)
{
  //ASSERT(inFile->GetDescription() != "Textures\\Test\\BetterQuality\\FloorWS08.tex");

  // Reset texture (blank all except some flags)
  Clear();

  // Determine API
  const GfxAPIType eAPI = _pGfx->GetCurrentAPI();
  _pGfx->CheckAPI();

  // Determine driver context presence (must have at least 1 texture unit!)
  const BOOL bHasContext = (_pGfx->gl_ctRealTextureUnits > 0);

  // Read version
  INDEX iVersion;
  inFile->ExpectID_t("TVER");
  *inFile >> iVersion;
  
  // [SEE]
  INDEX iNewVer = (iVersion & 0xFFFF0000) >> 16;
  iVersion &= 0x0000FFFF;
  CChaosTexCipher cipher(iNewVer);

  // Check currently supported versions
  if (iVersion != 4 && iVersion != 3) {
    throw(TRANS("Invalid texture format version."));
  }
  
  // Mark if this texture was loaded form the old format
  if (iVersion == 3) {
    td_ulFlags |= TEX_WASOLD;
  }
  
  BOOL bResetEffectBuffers = FALSE;
  BOOL bFramesLoaded = FALSE;
  BOOL bAlphaChannel = FALSE;
  BOOL bCompressed = FALSE;
  SLONG slCompressedFrameSize = 0;
  
  // Loop trough file and react according to chunk ID
  do
  {
    // Obtain chunk id
    CChunkID idChunk = inFile->GetID_t();
    if (idChunk == CChunkID("    ")) {
      // We should stop reading when an invalid chunk has been encountered
      break;
    }

    // If this is chunk containing texture data
    if (idChunk == CChunkID("TDAT"))
    {
      ULONG ulFlags = 0;
      INDEX ctMipLevels;

      // [SEE] LastChaos Texture
      if (iNewVer == 1 || iNewVer == 5) {
        (*inFile) >> td_mexWidth;
        (*inFile) >> td_iFirstMipLevel;
        (*inFile) >> td_mexHeight;
        (*inFile) >> td_ctFineMipLevels;
        (*inFile) >> ulFlags;
        (*inFile) >> td_ctFrames;
        
        // Decode it.
        td_mexWidth = cipher.Decode(td_mexWidth);
        td_iFirstMipLevel = cipher.Decode(td_iFirstMipLevel);
        td_mexHeight = cipher.Decode(td_mexHeight);
        td_ctFineMipLevels = cipher.Decode(td_ctFineMipLevels);
        ulFlags = cipher.Decode(ulFlags);
        td_ctFrames = cipher.Decode(td_ctFrames);
        
        // Combine flags
        td_ulFlags |= ulFlags;
        bAlphaChannel = td_ulFlags & TEX_ALPHACHANNEL;
        bCompressed   = td_ulFlags & TEX_COMPRESSED; // [SEE] Compressed Tex
        
        td_slFrameSize = GetMipmapOffset(15, GetPixWidth(), GetPixHeight()) * BYTES_PER_TEXEL;
        continue;
      }
      
      // Read data describing texture
      *inFile >> ulFlags;
      *inFile >> td_mexWidth;
      *inFile >> td_mexHeight;
      *inFile >> td_ctFineMipLevels;
      if (iVersion != 4) {
        *inFile >> ctMipLevels;
      }
      *inFile >> td_iFirstMipLevel;
      if (iVersion != 4) {
        *inFile >> td_slFrameSize;
      }
      *inFile >> td_ctFrames;
      
      // Combine flags
      td_ulFlags |= ulFlags;
      bAlphaChannel = td_ulFlags & TEX_ALPHACHANNEL;
      bCompressed   = td_ulFlags & TEX_COMPRESSED; // [SEE] Compressed Tex
      
      // Determine frame size
      if (iVersion == 4) {
        td_slFrameSize = GetMipmapOffset(15, GetPixWidth(), GetPixHeight()) * BYTES_PER_TEXEL;
      }
      
    // If this is chunk containing raw frames
    } else if (idChunk == CChunkID("FRMS"))  { 
      ASSERT(!bCompressed);
    
      // If no driver is present and texture is not static
      if (!(bHasContext || td_ulFlags & TEX_STATIC))
      { 
        // Determine frames' size
        SLONG slSkipSize = td_slFrameSize;
        if (iVersion == 4)
        {
          slSkipSize = GetPixWidth() * GetPixHeight();
          if (bAlphaChannel) {
            slSkipSize *= 4;
          } else {
            slSkipSize *= 3;
          }
        } 
        
        // Just seek over frames (skip it)
        inFile->Seek_t(slSkipSize * td_ctFrames, CTStream::SD_CUR);
        continue;
      }
      
      // Calculate texture size for corresponding texture format and allocate memory
      SLONG slTexSize = td_slFrameSize * td_ctFrames;
      td_pulFrames = (ULONG*)AllocMemory(slTexSize);
      
      // If older version
      if (iVersion == 3) {
        // Alloc memory block and read mip-maps
        inFile->Read_t(td_pulFrames, slTexSize);
      
      // If current version
      } else {
        PIX pixFrameSizeOnDisk = GetPixWidth() * GetPixHeight();
        for (INDEX iFr = 0; iFr < td_ctFrames; iFr++)
        { 
          // Loop thru frames
          ULONG *pulCurrentFrame = td_pulFrames + (iFr * td_slFrameSize / BYTES_PER_TEXEL);
          if (bAlphaChannel) {
            // Read texture with alpha channel from file
            inFile->Read_t(pulCurrentFrame, pixFrameSizeOnDisk * 4);
          } else {
            // Read texture without alpha channel from file
            inFile->Read_t(pulCurrentFrame, pixFrameSizeOnDisk * 3);
            
            // Add opaque alpha channel
            AddAlphaChannel((UBYTE*)pulCurrentFrame, pulCurrentFrame, pixFrameSizeOnDisk);
          }
        }
      }

      bFramesLoaded = TRUE;

    // [SEE] Compressed Tex
    } else if (idChunk == CChunkID("FRMC")) {
      ASSERT(bCompressed);
      
      *inFile >> slCompressedFrameSize;
      const SLONG slCompressedTexSize = td_ctFrames * slCompressedFrameSize;
      
      // if no driver is present
      if (!bHasContext) {
        inFile->Seek_t(slCompressedTexSize, CTStream::SD_CUR);// Skip data.
      } else {
        td_pulFrames = (ULONG*)AllocMemory(slCompressedTexSize);
        inFile->Read_t(td_pulFrames, slCompressedTexSize); // load frames!
        bFramesLoaded = TRUE;
      }

      td_slFrameSize = slCompressedFrameSize;

    // If this is chunk containing texture animation data
    } else if (idChunk == CChunkID("ANIM")) {
      // Read corresponding animation(s)
      CAnimData::Read_t(inFile);
    
    // If this is chunk containing base texture name
    } else if (idChunk == CChunkID("BAST")) {
      CTFileName fnBaseTexture;
      
      // Read file name of base texture
      *inFile >> fnBaseTexture;
      
      // If there is base texture, obtain it
      if (fnBaseTexture != "")
      {
        // Must not be the same as base texture
        CTFileName fnTex = inFile->GetDescription();
        if (fnTex == fnBaseTexture) {
          // Generate exception
          ThrowF_t(TRANS("Texture \"%s\" has same name as its base texture."), (CTString&)fnTex);
        } else {
          // Obtain base texture
          CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_TEXTURE, fnBaseTexture);
          td_ptdBaseTexture = static_cast<CTextureData *>(pser);
        }
      }
      // Force base to be static by default
      td_ptdBaseTexture->Force(TEX_STATIC);
      
    // If this is chunk containing saved effect buffers
    } else if (idChunk == CChunkID("FXBF")) {
      // Skip chunk in old versions
      bResetEffectBuffers = TRUE;
      if (iVersion != 4) {
        inFile->Seek_t(2 * GetPixWidth() * GetPixHeight() * sizeof(SWORD), CTStream::SD_CUR);
      } else {
        ASSERT(td_pixBufferWidth > 0 && td_pixBufferHeight > 0);
        ULONG ulSize = AllocEffectBuffers();
        if (td_ptegEffect->IsWater()) {
          ulSize *= 2;
        }
        inFile->Seek_t(2 * ulSize, CTStream::SD_CUR);
      }
    } else if (idChunk == CChunkID("FXB2")) { 
      // Read effect buffers
      ASSERT(td_pixBufferWidth > 0 && td_pixBufferHeight > 0);
      ULONG ulSize = AllocEffectBuffers();
      inFile->Read_t(td_pubBuffer1, ulSize);
      inFile->Read_t(td_pubBuffer2, ulSize);
      
    // If this is chunk containing effect data
    } else if (idChunk == CChunkID("FXDT")) {
      // Read effect class
      ULONG ulGlobalEffect;
      *inFile >> ulGlobalEffect;
      
      // Read effect buffer dimensions
      if (iVersion == 4) {
        *inFile >> td_pixBufferWidth;
      }
      
      if (iVersion == 4) {
        *inFile >> td_pixBufferHeight;
      }
      
      // Allocate memory for texture effect struct
      td_ptegEffect = new CTextureEffectGlobal(this, ulGlobalEffect);
      
      // Skip global properties for old format effect texture
      if (iVersion != 4) {
        inFile->Seek_t(64 * sizeof(char), CTStream::SD_CUR);
      }
      
      // Read count of effect sources
      INDEX ctEffectSources;
      *inFile >> ctEffectSources;
      
      // Add requested number of members to effect source array
      CTextureEffectSource *pEffectSources = td_ptegEffect->teg_atesEffectSources.New(ctEffectSources);

      // Read whole dynamic array of effect sources
      FOREACHINDYNAMICARRAY(td_ptegEffect->teg_atesEffectSources, CTextureEffectSource, itEffectSource)
      {
        // Read type of effect source
        *inFile >> (ULONG) itEffectSource->tes_ulEffectSourceType;
        
        // Read structure holding effect source properties
        inFile->Read_t(&itEffectSource->tes_tespEffectSourceProperties, sizeof(struct TextureEffectSourceProperties));
        
        // Remember pointer to global effect
        itEffectSource->tes_ptegGlobalEffect = td_ptegEffect;
        
        // Read count of effect pixels
        INDEX ctEffectSourcePixels;
        *inFile >> ctEffectSourcePixels;
        
        // If there are any effect pixels
        if (ctEffectSourcePixels > 0) {
          // Alocate needed ammount of members
          itEffectSource->tes_atepPixels.New(ctEffectSourcePixels);
          
          // Read all effect pixels in one block
          inFile->Read_t(&itEffectSource->tes_atepPixels[0], sizeof(struct TextureEffectPixel) * ctEffectSourcePixels);
        }
      }
      
      // Allocate memory for effect frame buffer
      SLONG slFrameSize = GetMipmapOffset(15, GetPixWidth(), GetPixHeight()) * BYTES_PER_TEXEL;
      td_pulFrames = (ULONG*)AllocMemory(slFrameSize);
      
      // Remember once again new frame size just for the sake of old effect textures
      td_slFrameSize = slFrameSize;
      
      // Mark that effect texture needs to be static
      td_ulFlags |= TEX_STATIC;
     
    // If this is chunk containing data about detail texture
    } else if (idChunk == CChunkID("DTLT")) {
      // Skip chunk (this is here only for compatibility reasons)
      CTFileName fnTmp;
      *inFile >> fnTmp;
    } else {
      ThrowF_t(TRANS("Unrecognisable chunk ID (\"%s\") found while reading texture \"%s\"."), (char*)idChunk,
               inFile->GetDescription().ConstData());
    }
  } while (!inFile->AtEOF()); // Until we didn't reach end of file

  // Reset effect buffers if needed
  if (bResetEffectBuffers) {
    InitEffectBufferDimensions();
    AllocEffectBuffers();
  }

  // Were done if frames weren't loaded or effect texture has been read
  if (!bFramesLoaded || td_ptegEffect != NULL) {
    return;
  }
  
  // [SEE] Compressed Tex
  if (bCompressed) {
    RemoveOversizedMipmaps(this);
    const PIX pixWidth  = GetPixWidth();
    const PIX pixHeight = GetPixHeight();
    const BOOL bStatic  = td_ulFlags & TEX_STATIC;

    ULONG *pulExpanded = NULL;
    SLONG slExpandSize = 0;

    const BOOL bAlphaCh = (td_ulFlags & TEX_ALPHACHANNEL);
    const BOOL bComprAl = (td_ulFlags & TEX_COMPRESSEDALPHA) && bAlphaCh;
    const PIX pixExpandSize = GetMipmapOffset( 15, pixWidth,pixHeight);
    slExpandSize = pixExpandSize * BYTES_PER_TEXEL;
    pulExpanded  = (ULONG*)AllocMemory(slExpandSize*td_ctFrames);

    INDEX slExpectedSize = pixWidth * pixHeight * 4; // RGBA
    if (!bAlphaCh) {
      slExpectedSize /= 8; // DXT1
    } else {
      slExpectedSize /= 4;
    }

    ASSERT(td_slFrameSize >= slExpectedSize); // Say about shit in Debug!

    // loop thru frames
    for (INDEX iFr = 0; iFr < td_ctFrames; iFr++)
    { // determine frame offset and upload texture frame
      UBYTE *pubFrameCo = (UBYTE*)(td_pulFrames+1) + (iFr*td_slFrameSize); // +1 to skip length of mipmap!
      ULONG *pulFrameEx = pulExpanded + (iFr*pixExpandSize);

      // If source data is not enough big for S3TC decompression.
      if (td_slFrameSize < slExpectedSize) {
        for (INDEX i = 0; i < pixWidth * pixHeight; i++)
        {
          pulFrameEx[i] = 0;
        }

        continue; // Sometimes this really happen!
      }

      Cs3tcCompressor::CompressionType eType = Cs3tcCompressor::TYPE_DXT1;
      
      if (bComprAl) {
        eType = Cs3tcCompressor::TYPE_DXT5;
      } else if (bAlphaCh) {
        eType = Cs3tcCompressor::TYPE_DXT3;
      }

      Cs3tcCompressor::Decode(pubFrameCo, (UBYTE *)pulFrameEx, pixWidth, pixHeight, eType);

      // mipmaps must be created in memory
      MakeMipmaps(td_ctFineMipLevels, pulFrameEx, pixWidth, pixHeight);
    }
    
    ASSERT(slExpandSize > 0 && pulExpanded != NULL);
    td_slFrameSize = slExpandSize;
    td_pulFrames = pulExpanded;
    td_ulFlags &= ~TEX_COMPRESSED;
  }

  // If texture is in old format, convert it to current format
  if (iVersion == 3) {
    Convert(this);
  }
  PIX pixWidth  = GetPixWidth();
  PIX pixHeight = GetPixHeight();
  PIX pixTexSize = pixWidth * pixHeight;
  PIX pixFrameSize = td_slFrameSize >> 2; // /BYTES_PER_TEXEL;
  INDEX iFrame;

  // Test first mipmap for transparency (i.e. is one bit of alpha channel enough?)
  // (must test it before filtering and/or mipmap reduction gets to this texture)
  if (bAlphaChannel)
  {
    td_ulFlags |= TEX_TRANSPARENT; 
    for (iFrame = 0; iFrame < td_ctFrames; iFrame++)
    {
      ULONG *pulCurrentFrame = td_pulFrames + iFrame * pixFrameSize;
      if (!IsTransparent(pulCurrentFrame, pixTexSize))
      {
        // No need to test other frames if one found that isn't gray
        td_ulFlags &= ~TEX_TRANSPARENT; 
        break; 
      }
    }
  }
  
  // Generate texture mip-maps for each frame (in version 4 they're no longer kept in file)
  // and eventually adjust texture saturation, do filtering and/or dithering
  tex_iFiltering = Clamp(tex_iFiltering, -6L, +6L);
  INDEX iTexFilter = tex_iFiltering;
  if (_bTextureExportMode || (td_ulFlags & TEX_CONSTANT)) {
    iTexFilter = 0; // don't filter constants and textures for exporting
  }
  
  if (iTexFilter) {
    td_ulFlags |= TEX_FILTERED;
  }

  // Eventually saturate texture
  if (!_bTextureExportMode && !(td_ulFlags & TEX_KEEPCOLOR) && (_slTexSaturation != 256 || _slTexHueShift != 0))
  {
    td_ulFlags |= TEX_SATURATED;
    for (iFrame = 0; iFrame < td_ctFrames; iFrame++)
    {
      ULONG *pulCurrentFrame = td_pulFrames + iFrame * pixFrameSize;
      AdjustBitmapColor(pulCurrentFrame, pulCurrentFrame, pixWidth, pixHeight, _slTexHueShift, _slTexSaturation);
    }
  }
  
  // Make mipmaps
  for (iFrame = 0; iFrame < td_ctFrames; iFrame++)
  { 
    ULONG *pulCurrentFrame = td_pulFrames + iFrame * pixFrameSize;
    MakeMipmaps(td_ctFineMipLevels, pulCurrentFrame, pixWidth,pixHeight, iTexFilter);
  }

  // Remove mipmaps from texture that are not needed and update texture size
  if (!_bTextureExportMode) {
    RemoveOversizedMipmaps(this);
  }

  // Do some additional mipmap adjustments if needed
  pixWidth  = GetPixWidth();
  pixHeight = GetPixHeight();
  pixTexSize = pixWidth * pixHeight;
  pixFrameSize = td_slFrameSize>>2; // /BYTES_PER_TEXEL;

  // Eventually colorize mipmaps
  extern INDEX tex_bColorizeMipmaps;
  if (!_bTextureExportMode && tex_bColorizeMipmaps && !(td_ulFlags & TEX_CONSTANT))
  {
    td_ulFlags |= TEX_COLORIZED;
    for (iFrame = 0; iFrame < td_ctFrames; iFrame++)
    {
      ULONG *pulCurrentFrame = td_pulFrames + iFrame * pixFrameSize;
      ColorizeMipmaps(1, pulCurrentFrame, pixWidth, pixHeight);
    }
    
  // If not colorized, test if texture is gray
  } else {
    td_ulFlags |= TEX_GRAY; 
    for (iFrame = 0; iFrame < td_ctFrames; iFrame++)
    {
      ULONG *pulCurrentFrame = td_pulFrames + iFrame * pixFrameSize;
      if (!IsGray(pulCurrentFrame, pixTexSize)) {
        // No need to test other frames if one found that isn't gray
        td_ulFlags &= ~TEX_GRAY; 
        break; 
      }
    }
  }
  // Test texture for equality (i.e. determine if texture could be discardable in shade mode when in lowest mipmap)
  if (td_ctFrames < 2 && (!gap_bAllowSingleMipmap || td_ctFineMipLevels > 1))
  { 
    // Get last mipmap pointer
    INDEX ctLastPixels   = Max(pixWidth,pixHeight) / Min(pixWidth,pixHeight);
    ULONG *pulLastMipMap = td_pulFrames + td_slFrameSize / BYTES_PER_TEXEL - ctLastPixels;
    if (IsEqualized(pulLastMipMap, ctLastPixels)) {
      td_ulFlags |= TEX_EQUALIZED;
    }
  }

  // Prepare dithering type
  td_ulInternalFormat = DetermineInternalFormat(this);
  tex_iDithering = Clamp(tex_iDithering, 0L, 10L); 
  INDEX iDitherType = 0;
  
  // Only non-static-constant textures can be dithered
  if (!(td_ulFlags & TEX_STATIC) || !(td_ulFlags & TEX_CONSTANT)) 
  { 
    extern INDEX AdjustDitheringType_OGL(   GLenum eFormat, INDEX iDitheringType);
    if (eAPI == GAT_OGL) {
      iDitherType = AdjustDitheringType_OGL(   (GLenum)td_ulInternalFormat, tex_iDithering);
    }
#ifdef SE1_D3D8
    extern INDEX AdjustDitheringType_D3D(D3DFORMAT eFormat, INDEX iDitheringType);
    if (eAPI == GAT_D3D8) {
      iDitherType = AdjustDitheringType_D3D((D3DFORMAT)td_ulInternalFormat, tex_iDithering);
    }
#endif // SE1_D3D8
  }
  
  // Eventually dither texture
  if (!_bTextureExportMode && iDitherType != 0)
  {
    td_ulFlags |= TEX_DITHERED;
    for (iFrame = 0; iFrame < td_ctFrames; iFrame++)
    {
      ULONG *pulCurrentFrame = td_pulFrames + iFrame * pixFrameSize;
      DitherMipmaps(iDitherType, pulCurrentFrame, pulCurrentFrame, pixWidth, pixHeight);
    }
  }
  
  // Upload texture if not static and API is active
  // (or, in the other hand, better not - this could cause reloading due to force() after obtain())
  if (!_bTextureExportMode && bHasContext && !(td_ulFlags & TEX_STATIC)) {
    SetAsCurrent();
  }
}

// Writes texutre to file
void CTextureData::Write_t(CTStream *outFile)  
{
  // Cannot write textures that have been mangled somehow
  _bTextureExportMode = FALSE;
  if (td_ptegEffect == NULL && IsModified()) {
    throw(TRANS("Cannot write texture that has modified frames."));
  }

  // Must not have base texture with same name
  if (td_ptdBaseTexture != NULL) {
    CTFileName fnTex = outFile->GetDescription();
    if (fnTex == td_ptdBaseTexture->GetName()) {
      ThrowF_t(TRANS("Texture \"%s\" has same name as its base texture."), (CTString&)fnTex);
    }
  }

  // Write version
  INDEX iVersion = 4;
  outFile->WriteID_t("TVER");
  *outFile << iVersion;

  // Isolate required flags
  ULONG ulFlags = td_ulFlags & (TEX_ALPHACHANNEL | TEX_32BIT);
  BOOL bAlphaChannel = td_ulFlags & TEX_ALPHACHANNEL;

  // Write chunk containing texture data
  outFile->WriteID_t(CChunkID("TDAT"));
  
  // Write data describing texture
  *outFile << ulFlags;
  *outFile << td_mexWidth;
  *outFile << td_mexHeight;
  *outFile << td_ctFineMipLevels;
  *outFile << td_iFirstMipLevel;
  *outFile << td_ctFrames;

  // If global effect struct exists in texture, don't save frames
  if (td_ptegEffect == NULL)
  { 
    // Write chunk containing raw frames
    ASSERT(td_ctFrames > 0);
    ASSERT(td_pulFrames != NULL);
    outFile->WriteID_t(CChunkID("FRMS"));
    PIX pixFrSize = GetPixWidth() * GetPixHeight();
    
    // Eventually prepare temp buffer in case of frames without alpha channel
    UBYTE *pubTmp = NULL;
    if (!bAlphaChannel) {
      pubTmp = (UBYTE*)AllocMemory(pixFrSize * 3);
    }
    
    // Write frames without mip-maps (just write the largest one)
    for (INDEX iFr = 0; iFr < td_ctFrames; iFr++ )
    { 
      // Determine write params
      ULONG *pulCurrentFrame = td_pulFrames + (iFr * td_slFrameSize / BYTES_PER_TEXEL);
      if (bAlphaChannel) { 
        // Write frame with alpha channel
        outFile->Write_t(pulCurrentFrame, pixFrSize * 4);
      } else { 
        // Write frame without alpha channel
        RemoveAlphaChannel(pulCurrentFrame, pubTmp, pixFrSize);
        outFile->Write_t(pubTmp, pixFrSize *3);
      }
    }
    // No need for temp buffer anymore
    if (pubTmp != NULL) {
      FreeMemory(pubTmp);
    }
    
  // If exists global effect struct in texture
  } else  {
    // Write chunk containing effect data
    outFile->WriteID_t(CChunkID("FXDT"));
    
    // Write effect class
    *outFile << td_ptegEffect->teg_ulEffectType;
    
    // Write effect buffer dimensions
    *outFile << td_pixBufferWidth;
    *outFile << td_pixBufferHeight;
    
    // Write count of effect sources
    *outFile << td_ptegEffect->teg_atesEffectSources.Count();

    // Write whole dynamic array of effect sources
    FOREACHINDYNAMICARRAY(td_ptegEffect->teg_atesEffectSources, CTextureEffectSource, itEffectSource)
    { 
      // Write type of effect source
      *outFile << itEffectSource->tes_ulEffectSourceType;
      
      // Write structure holding effect source properties
      outFile->Write_t(&itEffectSource->tes_tespEffectSourceProperties, sizeof(struct TextureEffectSourceProperties));
      INDEX ctEffectSourcePixels = itEffectSource->tes_atepPixels.Count();
      
      // Write count of effect pixels
      *outFile << ctEffectSourcePixels;
      
      // If there are any effect pixels
      if (ctEffectSourcePixels > 0) {
        // Write all effect pixels in one block
        outFile->Write_t(&itEffectSource->tes_atepPixels[0], sizeof(struct TextureEffectPixel)*ctEffectSourcePixels);
      }
    }
    
    // If effect buffers are valid
    if (td_pubBuffer1 != NULL && td_pubBuffer2 != NULL)
    { 
      // Write chunk containing effect buffers
      outFile->WriteID_t(CChunkID("FXB2"));
      ULONG ulSize = GetEffectBufferSize();
      
      // Write effect buffers
      outFile->Write_t(td_pubBuffer1, ulSize);
      outFile->Write_t(td_pubBuffer2, ulSize);
    }
  }
  
  // Write chunk containing texture animation data
  outFile->WriteID_t(CChunkID("ANIM"));
  
  // Write corresponding animation(s)
  CAnimData::Write_t(outFile);

  // If this texture has base texture
  if (td_ptdBaseTexture != NULL)
  {
    // Write chunk containing base texture file name
    outFile->WriteID_t(CChunkID("BAST"));
    
    // Write file name of base texture
    *outFile << td_ptdBaseTexture->GetName();
  }
}

// Creates new effect texture with one frame
void CTextureData::CreateEffectTexture(PIX pixWidth, PIX pixHeight, MEX mexWidth,
                                       CTextureData *ptdBaseTexture, ULONG ulGlobalEffect)
{
  ptdBaseTexture->Reference();
  Clear();
  CAnimData::DefaultAnimation();

  // Determine mip index from mex size
  td_iFirstMipLevel = FastLog2(mexWidth / pixWidth);
  
  // Fill some of TextureData members
  td_ulFlags   = TEX_STATIC;
  td_mexWidth  = pixWidth << td_iFirstMipLevel;
  td_mexHeight = pixHeight << td_iFirstMipLevel;
  td_ctFrames  = 1;
  td_pulFrames = NULL;
  
  // Remember base texture
  td_ptdBaseTexture = ptdBaseTexture;

  // Allocate texture effect global
  td_ptegEffect = new CTextureEffectGlobal(this, ulGlobalEffect);

  // Allocate and reset effect buffers
  InitEffectBufferDimensions();
  AllocEffectBuffers();
}