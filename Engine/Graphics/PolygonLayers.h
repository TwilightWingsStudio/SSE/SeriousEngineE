/* Copyright (c) 2021-2022 by Dreamy Cecil & ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_POLYGONLAYERS_H
#define SE_INCL_POLYGONLAYERS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

// [Cecil] Layers: Define amount of brush polygon layers

#define BRUSH_LAYERS 5 // Texture layers for brush polygons

#define DETAIL_LAYER 2 // 3rd out of 3 layers in TSE
#define SHADOW_LAYER BRUSH_LAYERS // Shadow layer index (one after the last brush layer)

#define POLYGON_LAYERS (BRUSH_LAYERS + 1) // Brush layers + shadow layer

// [Cecil] Layers: For DrawPort_RenderScene.cpp

typedef ULONG CGroupFlags; // group flags mask type

// Single-texturing group flags (ST)
#define GF_TX(layer) (1L << layer)
#define GF_SHD GF_TX(SHADOW_LAYER)

#define ST_FLAGS_COUNT POLYGON_LAYERS // offset after ST flags
#define GF_ST_MASK (1L << ST_FLAGS_COUNT)-1 // mask of ST group flags

// After-shadow group flags (AS)
#define GF_FLAT (1L << ST_FLAGS_COUNT) // flat fill instead of texture 1
#define GF_TA(layer) (1L << (ST_FLAGS_COUNT + layer)) // texture N after shade

#define AS_FLAGS_COUNT (ST_FLAGS_COUNT + BRUSH_LAYERS) // offset after AT flags
#define GF_AS_MASK (1L << AS_FLAGS_COUNT)-1 // mask of ST + AS group flags

// Special group flags (SP)
#define GF_FOG  (1L << (AS_FLAGS_COUNT + 0))
#define GF_HAZE (1L << (AS_FLAGS_COUNT + 1))
#define GF_SEL  (1L << (AS_FLAGS_COUNT + 2))
#define GF_KEY  (1L << (AS_FLAGS_COUNT + 3)) // first layer requires alpha-keying

#define SP_FLAGS_COUNT (AS_FLAGS_COUNT + 4) // offset after SP flags
#define GF_SP_MASK (1L << SP_FLAGS_COUNT)-1 // mask of all group flags

// Texture combinations for max 4 texture units (excluding fog, haze and selection)
#define GF_TX0_TX1         (1L << (SP_FLAGS_COUNT + 0))
#define GF_TX0_TX2         (1L << (SP_FLAGS_COUNT + 1))
#define GF_TX0_SHD         (1L << (SP_FLAGS_COUNT + 2))
#define GF_TX2_SHD         (1L << (SP_FLAGS_COUNT + 3)) // second pass
#define GF_TX0_TX1_TX2     (1L << (SP_FLAGS_COUNT + 4))
#define GF_TX0_TX1_SHD     (1L << (SP_FLAGS_COUNT + 5))
#define GF_TX0_TX2_SHD     (1L << (SP_FLAGS_COUNT + 6))
#define GF_TX0_TX1_TX2_SHD (1L << (SP_FLAGS_COUNT + 7))

// Total number of groups
#define GROUPS_MINCOUNT GF_ST_MASK   // min group (mask of ST group flags)
#define GROUPS_MAXCOUNT GF_SP_MASK+1 // max group (not a mask, but fits ST, AS and SP flags)

#endif  /* include-once check. */
