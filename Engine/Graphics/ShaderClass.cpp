/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/IO/Stream.h>
#include <Core/Modules/Module.h>

#include <Engine/Graphics/Shader.h>
#include <Engine/Resources/ResourceManager.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Shader handling

// Constructor
CShaderClass::CShaderClass()
{
  _pModule = NULL;
  ShaderFunc = NULL;
  GetShaderDesc = NULL;
}

// Destructor
CShaderClass::~CShaderClass()
{
  // Release shader dll
  Clear();
}

// Clear shader 
void CShaderClass::Clear(void)
{
  ShaderFunc = NULL;
  GetShaderDesc = NULL;
}

// Count used memory
SLONG CShaderClass::GetUsedMemory(void)
{
  return sizeof(CShaderClass);
}

// Write to stream
void CShaderClass::Write_t(CTStream *ostrFile)
{
}

// Read from stream
void CShaderClass::Read_t(CTStream *istrFile)
{
  // Read the dll filename and class name from the stream
  CTFileName fnmModule;
  CTString strShaderFunc;
  CTString strShaderInfo;

  fnmModule.ReadFromText_t(*istrFile, "Package: ");
  strShaderFunc.ReadFromText_t(*istrFile, "Name: ");
  strShaderInfo.ReadFromText_t(*istrFile, "Info: ");

  _pModule = (FModule *)_pResourceMgr->Obtain_t(CResource::TYPE_MODULE, fnmModule);
  
  // Get pointer to shader render function
  ShaderFunc = (void(*)(void))GetProcAddress(_pModule->GetModuleHandle(), strShaderFunc.ConstData());
  
  // If error accured
  if (ShaderFunc == NULL)
  {
    // Report error
    istrFile->Throw_t("GetProcAddress 'ShaderFunc' Error");
  }
  
  // Get pointer to shader info function
  GetShaderDesc = (void(*)(ShaderDesc&))GetProcAddress(_pModule->GetModuleHandle(), strShaderInfo.ConstData());
  
  // If error accured
  if (GetShaderDesc == NULL) {
    // Report error
    istrFile->Throw_t("GetProcAddress 'ShaderDesc' Error");
  }
}

UBYTE CShaderClass::GetType() const
{
  return TYPE_SHADER;
}