/* Copyright (c) 2022 by Dreamy Cecil & ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_GFX_NULL_H
#define SE_INCL_GFX_NULL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/Graphics/GfxInterface.h>

class IGfxNull : public IGfxInterface
{
  public:
    //! Unique graphical API type.
    GfxAPIType GetType(void) const override {
      return GAT_NONE;
    };

  //! Graphical API methods.
  public:

    //! Enable operations.
    void EnableDepthWrite(void) override { NOTHING; };
    void EnableDepthBias(void) override { NOTHING; };
    void EnableDepthTest(void) override { NOTHING; };
    void EnableAlphaTest(void) override { NOTHING; };
    void EnableBlend(void) override { NOTHING; };
    void EnableDither(void) override { NOTHING; };
    void EnableTexture(void) override { NOTHING; };
    void EnableClipping(void) override { NOTHING; };
    void EnableClipPlane(void) override { NOTHING; };

    //! Disable operations.
    void DisableDepthWrite(void) override { NOTHING; };
    void DisableDepthBias(void) override { NOTHING; };
    void DisableDepthTest(void) override { NOTHING; };
    void DisableAlphaTest(void) override { NOTHING; };
    void DisableBlend(void) override { NOTHING; };
    void DisableDither(void) override { NOTHING; };
    void DisableTexture(void) override { NOTHING; };
    void DisableClipping(void) override { NOTHING; };
    void DisableClipPlane(void) override { NOTHING; };

    //! Set blending operations.
    void BlendFunc(GfxBlend eSrc, GfxBlend eDst) override { NOTHING; };

    //! Set depth buffer compare mode.
    void DepthFunc(GfxComp eFunc) override { NOTHING; };

    //! Set depth buffer range.
    void DepthRange(FLOAT fMin, FLOAT fMax) override { NOTHING; };

    //! Color mask control (use CT_RMASK, CT_GMASK, CT_BMASK, CT_AMASK to enable specific channels).
    void SetColorMask(ULONG ulColorMask) override { NOTHING; };

  //! Projections.
  public:

    //! Set face culling.
    void CullFace(GfxFace eFace) override { NOTHING; };
    void FrontFace(GfxFace eFace) override { NOTHING; };

    //! Set custom clip plane (if NULL, disable it).
    void ClipPlane(const DOUBLE *pdPlane) override { NOTHING; };

    //! Set orthographic matrix.
    void SetOrtho(const FLOAT fLeft, const FLOAT fRight, const FLOAT fTop,  const FLOAT fBottom, const FLOAT fNear, const FLOAT fFar, const BOOL bSubPixelAdjust) override { NOTHING; };

    //! Set frustrum matrix.
    void SetFrustum(const FLOAT fLeft, const FLOAT fRight, const FLOAT fTop,  const FLOAT fBottom, const FLOAT fNear, const FLOAT fFar) override { NOTHING; };

    //! Set view matrix.
    void SetViewMatrix(const FLOAT *pfMatrix) override { NOTHING; };

    //! Set texture matrix.
    void SetTextureMatrix(const FLOAT *pfMatrix) override { NOTHING; };

    //! Polygon mode (point, line or fill).
    void PolygonMode(GfxPolyMode ePolyMode) override { NOTHING; };

  //! Textures.
  public:

    //! Set texture wrapping mode.
    void SetTextureWrapping(enum GfxWrap eWrapU, enum GfxWrap eWrapV) override { NOTHING; };

    //! Set texture modulation mode (1X or 2X).
    void SetTextureModulation(INDEX iScale) override { NOTHING; };

    //! Generate texture for API.
    void GenerateTexture(ULONG &ulTexObject) override { NOTHING; };

    //! Unbind texture from API.
    void DeleteTexture(ULONG &ulTexObject) override { NOTHING; };

  //! Vertex arrays.
  public:

    //! Prepare vertex array for API.
    void SetVertexArray(void *pvtx, INDEX ctVtx, BOOL bWithShade) override { NOTHING; };

  #ifdef SE1_TRUFORM
    //! Prepare normal array for API.
    void SetNormalArray(GFXNormal *pnor) override { NOTHING; };
  #endif

    //! Prepare UV array for API.
    void SetTexCoordArray(GFXTexCoord *ptex, BOOL b4) override { NOTHING; }; // b4 = projective mapping (4 FLOATs)

    //! Prepare color array for API.
    void SetColorArray(GFXColor *pcol) override { NOTHING; };

    //! Draw prepared arrays.
    void DrawElements(INDEX ctElem, INDEX *pidx) override { NOTHING; };

    //! Set constant color for subsequent rendering (until first SetColorArray() call!).
    void SetConstantColor(COLOR col) override { NOTHING; };

    //! Color array usage control.
    void EnableColorArray(void) override { NOTHING; };
    void DisableColorArray(void) override { NOTHING; };

  //! Miscellaneous.
  public:

    //! Force finish of rendering queue.
    void Finish(void) override { NOTHING; };

    //! Compiled vertex array control.
    void LockArrays(void) override { NOTHING; };

  #ifdef SE1_TRUFORM
    //! Toggle truform.
    void EnableTruform(void) override { NOTHING; };
    void DisableTruform(void) override { NOTHING; };
  #endif
};

#endif  /* include-once check. */
