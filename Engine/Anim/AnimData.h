/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ANIMDATA_H
#define SE_INCL_ANIMDATA_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Animation data for a class of animateable objects.
class ENGINE_API CAnimData : public CResource
{
  public:
    INDEX ad_NumberOfAnims;
    class COneAnim *ad_Anims;	    // array of animations

  public:
    //! Fill member variables with invalid data.
    CAnimData();

    //! Free allocated data (ad_Anims array), check invalid data.
    ~CAnimData();

    //! Clears animation data object.
    void Clear();

    //! Get amount of memory used by this object.
    SLONG GetUsedMemory(void);

  public:
    //! Creates given number of default animations (1 frame, given name and apeed).
    void CreateAnimations(INDEX ctAnimations, CTString strName="None",
                          INDEX iDefaultFrame=0,TIME tmSpeed=0.02f);

    //! Replaces frames array with given one.
    void SetFrames(INDEX iAnimation, INDEX ctFrames, INDEX *pNewFrames);

    //! Replaces requested animation's name with given one.
    void SetName(INDEX iAnimation, CTString strNewName);

    //! Replaces requested animation's speed with given one.
    void SetSpeed(INDEX iAnimation, TIME tmSpeed);

    //! Obtains frame index for given place in array representing given animation.
    INDEX GetFrame(INDEX iAnimation, INDEX iFramePlace);

    //! Sets frame index for given place in array representing given animation.
    void SetFrame(INDEX iAnimation, INDEX iFramePlace, INDEX iNewFrame);

    //! Fill animation data object vith valid data containing one animation, one frame.
    void DefaultAnimation();

    //! Get animation's info.
    void GetAnimInfo(INDEX iAnimNo, CAnimInfo &aiInfo) const;

    //! Add animation.
    void AddAnimation(void);

    //! Delete animation.
    void DeleteAnimation(INDEX iAnim);

    //! Get number of animations.
    INDEX GetAnimsCt(void) const;
    
    //! Get type of this resource.
    virtual UBYTE GetType() const;

  // reference counting functions
  public:
    virtual void RemReference_internal(void);

    //! Check if this kind of objects is auto-freed.
    virtual BOOL IsAutoFreed(void);

    //! Add one reference to this resource.
    void AddReference(void);
    
    //! Remove one reference to this resource.
    void RemReference(void);

  // IO
  public:
    //! Read from stream.
    void Read_t(CTStream *istrFile); // throw char *

    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char *
    
    //! Load list of frames from script file.
    void LoadFromScript_t(CTStream *File, CListHead *FrameFileList); // throw char *

    //! Print #define <animation name> lines for all animations into given file.
    void ExportAnimationNames_t(CTStream *ostrFile, CTString strAnimationPrefix);  // throw char *
};

#endif /* include-once check. */