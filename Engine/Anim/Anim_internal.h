//! One animation of an animateable object.
class COneAnim
{
  public:
    //! Constructor.
    COneAnim();

    //! Destructor.
    ~COneAnim();

    //! Copy constructor.
    COneAnim &operator=(const COneAnim &oaAnim);

  public:
    NAME oa_Name;
    TIME oa_SecsPerFrame;	    // speed of this animation
    INDEX oa_NumberOfFrames;
    INDEX *oa_FrameIndices;   // array of frame indices
};

/*
 * Node used for linking ptrs to COneAnim objects while loading
 * script file before turning them into an array
 * Class is used only for loading script files
 */
class COneAnimNode
{
  public:
    ~COneAnimNode();
    COneAnimNode(COneAnim *AnimToInsert, CListHead *LH);
  public:
    CListNode coan_Node;
    COneAnim *coan_OneAnim;
};