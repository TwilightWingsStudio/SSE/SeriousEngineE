/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */


#ifndef SE_INCL_ANIMATED_H
#define SE_INCL_ANIMATED_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Some object that can play animations.
class ENGINE_API IAnimated
{
  public:
    //! Loop anims forward.
    virtual void NextAnim(void) = 0;

    //! Loop anims backward.
    virtual void PrevAnim(void) = 0;

    //! Get animation's info.
    virtual void GetAnimInfo(INDEX iAnimNo, CAnimInfo &aiInfo) const = 0;

    //! Get animation's length.
    virtual FLOAT GetCurrentAnimLength(void) const = 0;
    
    //! Get animation's length.
    virtual FLOAT GetAnimLength(INDEX iAnim) const = 0;

    //! Get number of animations in current anim data.
    virtual INDEX GetAnimsCt() const = 0;

    //! If animation has finished.
    virtual BOOL IsAnimFinished(void) const = 0;

    //! Get time when current anim was started.
    virtual TIME GetStartTime(void) const = 0;

    //! Get passed time from start of animation.
    virtual TIME GetPassedTime(void) const = 0;

    //! Start new animation -- obsolete.
    virtual void StartAnim(INDEX iNew) = 0;

    //! Start playing an animation.
    virtual void PlayAnim(INDEX iNew, ULONG ulFlags) = 0;

    //! Seamlessly continue playing another animation from same point.
    //! NOTE: Never called.
    virtual void SwitchToAnim(INDEX iNew) = 0;

    //! Set new animation but doesn't starts it.
    virtual void SetAnim(INDEX iNew) = 0;

    //! Reset anim (restart).
    virtual void ResetAnim() = 0;

    //! Pauses current animation.
    virtual void PauseAnim() = 0;

    //! Continues paused animation.
    virtual void ContinueAnim() = 0;

    //! Offsets the animation phase.
    //! NOTE: Never called.
    virtual void OffsetPhase(TIME tm) = 0;

    //! Retrieves paused flag.
    virtual BOOL IsPaused(void) = 0;

    //! Gets the number of current animation.
    virtual INDEX GetAnim(void) const = 0;

    //! Gets the number of last animation.
    virtual INDEX GetLastAnim(void) const = 0;

  public:
    //! Loop frames forward.
    virtual void NextFrame(void) = 0;

    //! Loop frames backward.
    virtual void PrevFrame(void) = 0;

    //! Select first frame.
    virtual void FirstFrame(void) = 0;

    //! Select last frame.
    virtual void LastFrame(void) = 0;

    //! Gets the number of current frame.
    virtual INDEX GetFrame(void) const = 0;

    //! Gets number of frames in current anim.
    //! NOTE: Never called.
    virtual INDEX GetFramesInCurrentAnim(void) const = 0;

    //! Get information for linear interpolation beetween frames.
    virtual void GetFrame(INDEX &iFrame0, INDEX &iFrame1, FLOAT &fRatio) const = 0;
};

#endif /* include-once check. */