/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ANIM_H
#define SE_INCL_ANIM_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/Resource.h>

#include <Core/Base/Lists.h>

#define NAME_SIZE 32
typedef char NAME[NAME_SIZE];
#define PATH_MAX 260
typedef char FILE_NAME[PATH_MAX];

//! An object used for obtaining animation's information
class CAnimInfo
{
  public:
    NAME ai_AnimName;
    TIME ai_SecsPerFrame;
    INDEX ai_NumberOfFrames;
};


//! Node used for linking file names representing frames.
/*!
  Nodes of this kind are returned result of LoadFromScript function.
*/
class ENGINE_API CFileNameNode
{
  public:
    FILE_NAME cfnn_FileName;
    CListNode cfnn_Node;
    CFileNameNode(const char *NewFileName, CListHead *LH);
};

#include <Engine/Anim/AnimData.h>
#include <Engine/Anim/AnimObject.h>

#endif /* include-once check. */
