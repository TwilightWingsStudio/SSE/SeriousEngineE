/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Anim/Anim.h>

#include <Core/Base/Memory.h>
#include <Core/IO/Stream.h>
#include <Core/Base/Timer.h>
#include <Core/Math/Functions.h>

#include <Engine/Resources/ResourceManager.h>

#include <Core/Base/ListIterator.inl>
#include <Core/Templates/DynamicArray.cpp>
#include <Engine/Anim/Anim_internal.h>

/*
 * Default constructor.
 */
CAnimObject::CAnimObject(void)
{
  // set invalid data for validation check
  ao_AnimData = NULL;
  ao_tmAnimStart = 0.0f;
  ao_iCurrentAnim = -1;
  ao_iLastAnim = -1;
  ao_ulFlags = AOF_PAUSED;
};

/* Destructor. */
CAnimObject::~CAnimObject(void)
{
  ao_AnimData->RemReference();
};

// copy from another object of same class
ENGINE_API void CAnimObject::Copy(CAnimObject &aoOther)
{
  SetData(aoOther.GetData());
  ao_tmAnimStart  = aoOther.ao_tmAnimStart;
  ao_iCurrentAnim = aoOther.ao_iCurrentAnim;
  ao_iLastAnim    = aoOther.ao_iLastAnim;
  ao_ulFlags      = aoOther.ao_ulFlags;
}

// synchronize with another animation object (set same anim and frames)
ENGINE_API void CAnimObject::Synchronize(CAnimObject &aoOther)
{
  // copy animations, time and flags
  INDEX ctAnims = GetAnimsCt();
  ao_tmAnimStart  = aoOther.ao_tmAnimStart;
  ao_iCurrentAnim = ClampUp(aoOther.ao_iCurrentAnim, ctAnims - 1L);
  ao_iLastAnim    = ClampUp(aoOther.ao_iLastAnim, ctAnims - 1L);
  ao_ulFlags      = aoOther.ao_ulFlags;
}

/*
 * Get animation's lenght.
 */
FLOAT CAnimObject::GetAnimLength(INDEX iAnim) const
{
  if (ao_AnimData == NULL) return 1.0f;
  ASSERT(ao_AnimData != NULL);

  if (iAnim >= ao_AnimData->ad_NumberOfAnims) {
    iAnim = 0;
  }

  ASSERT((iAnim >= 0) && (iAnim < ao_AnimData->ad_NumberOfAnims));
  COneAnim *pCOA = &ao_AnimData->ad_Anims[iAnim];
  return pCOA->oa_NumberOfFrames*pCOA->oa_SecsPerFrame;
};

FLOAT CAnimObject::GetCurrentAnimLength(void) const
{
  return GetAnimLength(ao_iCurrentAnim);
}

/*
 * Calculate frame that coresponds to given time.
 */
INDEX CAnimObject::FrameInTime(TIME time) const
{
  ASSERT(ao_AnimData != NULL);
  ASSERT((ao_iCurrentAnim >= 0) && (ao_iCurrentAnim < ao_AnimData->ad_NumberOfAnims));
  INDEX iFrameInAnim;

  COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];

  if (ao_ulFlags & AOF_PAUSED) {
    // return index of paused frame inside global frame array
    iFrameInAnim = ClipFrame(pCOA->oa_NumberOfFrames + ClipFrame(FloatToInt(ao_tmAnimStart / pCOA->oa_SecsPerFrame)));

  } else {
    // return index of frame inside global frame array of frames in given moment
    iFrameInAnim = ClipFrame(FloatToInt((time - ao_tmAnimStart) / pCOA->oa_SecsPerFrame));
  }

  return pCOA->oa_FrameIndices[iFrameInAnim];
}

/*
 * Pauses animation
 */
void CAnimObject::PauseAnim(void)
{
  if (ao_ulFlags & AOF_PAUSED) return;                          // dont pause twice
  ao_ulFlags |= AOF_PAUSED;
  ao_tmAnimStart = _pTimer->CurrentTick() - ao_tmAnimStart;  // set difference from current time as start time,
  MarkChanged();                                  // so get frame will get correct current frame
}

/*
 * Continues paused animation
 */
void CAnimObject::ContinueAnim(void)
{
  if (!(ao_ulFlags & AOF_PAUSED)) return;

  // calculate freezed frame index inside current animation (not in global list of frames!)
  COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];
  if (pCOA->oa_NumberOfFrames <= 0) {
    return;
  }

  INDEX iStoppedFrame = (pCOA->oa_NumberOfFrames + (SLONG)(ao_tmAnimStart / pCOA->oa_SecsPerFrame)
                         % pCOA->oa_NumberOfFrames) % pCOA->oa_NumberOfFrames;

  // using current frame index calculate time so animation continues from same frame
  ao_tmAnimStart = _pTimer->CurrentTick() - pCOA->oa_SecsPerFrame * iStoppedFrame;
  ao_ulFlags &= ~AOF_PAUSED;
  MarkChanged();
}

/*
 * Offsets the animation phase
 */
void CAnimObject::OffsetPhase(TIME tm)
{
  ao_tmAnimStart += tm;
}

/*
 * Loop anims forward
 */
void CAnimObject::NextAnim()
{
  ASSERT(ao_iCurrentAnim != -1);
  ASSERT(ao_AnimData != NULL);

  ao_iCurrentAnim = (ao_iCurrentAnim + 1) % ao_AnimData->ad_NumberOfAnims;
  ao_iLastAnim = ao_iCurrentAnim;
  ao_tmAnimStart = _pTimer->CurrentTick();
  MarkChanged();
};

/*
 * Loop anims backward
 */
void CAnimObject::PrevAnim()
{
  ASSERT(ao_iCurrentAnim != -1);
  ASSERT(ao_AnimData != NULL);

  ao_iCurrentAnim = (ao_AnimData->ad_NumberOfAnims + ao_iCurrentAnim - 1) % ao_AnimData->ad_NumberOfAnims;
  ao_iLastAnim = ao_iCurrentAnim;
  ao_tmAnimStart = _pTimer->CurrentTick();
  MarkChanged();
};

/*
 * Selects frame for given time offset from animation start (0)
 */
void CAnimObject::SelectFrameInTime(TIME tmOffset)
{
  ao_tmAnimStart = tmOffset;  // set fixed start time
  MarkChanged();
}

void CAnimObject::FirstFrame(void)
{
  SelectFrameInTime(0.0f);
}

void CAnimObject::LastFrame(void)
{
  class COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim]; 
  SelectFrameInTime(GetAnimLength(ao_iCurrentAnim)-pCOA->oa_SecsPerFrame);
}

/*
 * Loop frames forward
 */
void CAnimObject::NextFrame()
{
  ASSERT(ao_iCurrentAnim != -1);
  ASSERT(ao_AnimData != NULL);
  ASSERT(ao_ulFlags & AOF_PAUSED);

  ao_tmAnimStart += ao_AnimData->ad_Anims[ao_iCurrentAnim].oa_SecsPerFrame;
  MarkChanged();
};

/*
 * Loop frames backward
 */
void CAnimObject::PrevFrame()
{
  ASSERT(ao_iCurrentAnim != -1);
  ASSERT(ao_AnimData != NULL);
  ASSERT(ao_ulFlags & AOF_PAUSED);

  ao_tmAnimStart -= ao_AnimData->ad_Anims[ao_iCurrentAnim].oa_SecsPerFrame;
  MarkChanged();
};

/*
 * Retrieves paused flag
 */
BOOL CAnimObject::IsPaused()
{
  return ao_ulFlags&AOF_PAUSED;
};

/*
 * Test if some updateable object is up to date with this anim object.
 */
BOOL CAnimObject::IsUpToDate(const CUpdateable &ud) const
{
  // if the object itself has changed, or its data has changed
  if (!CChangeable::IsUpToDate(ud) || !ao_AnimData->IsUpToDate(ud)) {
    // something has changed
    return FALSE;
  }

  // otherwise, nothing has changed
  return TRUE;
}

/*
 * Attach data to this object.
 */
void CAnimObject::SetData(CAnimData *pAD)
{
  // mark new data as referenced once more
  pAD->AddReference();
  // mark old data as referenced once less
  ao_AnimData->RemReference();
  // remember new data
  ao_AnimData = pAD;

  if (pAD != NULL) StartAnim(0);
  // mark that something has changed
  MarkChanged();
}

// obtain anim and set it for this object
void CAnimObject::SetData_t(const CTFileName &fnmAnim)
{
  // if the filename is empty
  if (fnmAnim == "") {
    // release current anim
    SetData(NULL);

  // if the filename is not empty
  } else {
    // obtain it (adds one reference)
    CResource *pad = _pResourceMgr->Obtain_t(CResource::TYPE_ANIMATION, fnmAnim);
    // set it as data (adds one more reference, and remove old reference)
    SetData(static_cast<CAnimData *>(pad));
    // release it (removes one reference)
    _pResourceMgr->Release(pad);
    // total reference count +1+1-1 = +1 for new data -1 for old data
  }
}

/*
 * Sets new animation (but doesn't starts it).
 */
void CAnimObject::SetAnim(INDEX iNew)
{
  if (ao_AnimData == NULL) return;

  // clamp animation
  if (iNew >= GetAnimsCt() )
  {
    iNew = 0;
  }

  // if new animation then remember starting time
  if (ao_iCurrentAnim != iNew) { 
    ao_tmAnimStart = _pTimer->CurrentTick();
  }

  // set new animation number
  ao_iCurrentAnim=iNew;
  ao_iLastAnim=iNew;
  // mark that something has changed
  MarkChanged();
};

/*
 * Start new animation.
 */
void CAnimObject::StartAnim(INDEX iNew)
{
  if (ao_AnimData == NULL) return;

  // set new animation
  SetAnim(iNew);
  // set pause off, looping on
  ao_ulFlags = AOF_LOOPING;
};

/* Start playing an animation. */
void CAnimObject::PlayAnim(INDEX iNew, ULONG ulFlags)
{
  if (ao_AnimData == NULL) return;

  // clamp animation
  if (iNew >= GetAnimsCt() ) {
    iNew = 0;
  }

  // if anim needs to be reset at start
  if (!(ulFlags & AOF_NORESTART) || ao_iCurrentAnim != iNew) {
    // if smooth transition
    if (ulFlags & AOF_SMOOTHCHANGE) {
      // calculate time to end of the current anim
      class COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];
      TIME tmNow = _pTimer->CurrentTick();
      TIME tmLength = GetCurrentAnimLength();
      FLOAT fFrame = ((_pTimer->CurrentTick() - ao_tmAnimStart) / pCOA->oa_SecsPerFrame);
      INDEX iFrame = INDEX(fFrame);
      FLOAT fFract = fFrame - iFrame;
      iFrame = ClipFrame(iFrame);
      TIME tmPassed = (iFrame + fFract) * pCOA->oa_SecsPerFrame;
      TIME tmLeft = tmLength - tmPassed;
      // set time ahead to end of the current animation
      ao_tmAnimStart = _pTimer->CurrentTick() + tmLeft;
      // remember last animation
      ao_iLastAnim = ao_iCurrentAnim;
      // set new animation number
      ao_iCurrentAnim = iNew;

    // if normal transition
    } else {
      ao_iLastAnim    = iNew;
      ao_iCurrentAnim = iNew;
      // remember starting time
      ao_tmAnimStart = _pTimer->CurrentTick();
    }

  // if anim doesn't need be reset at start then do nothing
  } else {
    NOTHING;
  }

  // set pause off, looping flag from flags
  ao_ulFlags = ulFlags & (AOF_LOOPING|AOF_PAUSED);

  // mark that something has changed
  MarkChanged();
};

/* Seamlessly continue playing another animation from same point. */
void CAnimObject::SwitchToAnim(INDEX iNew)
{
  if (ao_AnimData == NULL) return;

  // clamp animation
  if (iNew >= GetAnimsCt() )
  {
    iNew = 0;
  }

  // set new animation number
  ao_iCurrentAnim=iNew;
  ao_iLastAnim = ao_iCurrentAnim;
}

/*
 * Reset anim (restart)
 */
void CAnimObject::ResetAnim()
{
  if (ao_AnimData == NULL) return;
  // remember starting time
  ao_tmAnimStart = _pTimer->CurrentTick();
  // mark that something has changed
  MarkChanged();
};

// Get info about some animation
void CAnimObject::GetAnimInfo(INDEX iAnimNo, CAnimInfo &aiInfo) const
{
  if (iAnimNo >= ao_AnimData->ad_NumberOfAnims) {
    iAnimNo = 0;
  }

  ASSERT(iAnimNo < ao_AnimData->ad_NumberOfAnims);
  strcpy(aiInfo.ai_AnimName, ao_AnimData->ad_Anims[iAnimNo].oa_Name);
  aiInfo.ai_SecsPerFrame = ao_AnimData->ad_Anims[iAnimNo].oa_SecsPerFrame;
  aiInfo.ai_NumberOfFrames = ao_AnimData->ad_Anims[iAnimNo].oa_NumberOfFrames;
}

// clip frame index to be inside valid range (wrap around for looping anims)
INDEX CAnimObject::ClipFrame(INDEX iFrame) const
{
  if (ao_AnimData->ad_NumberOfAnims == 0) {
    return 0;
  }

  class COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];

  // if looping
  if (ao_ulFlags & AOF_LOOPING) {
    // wrap-around
    if (pCOA->oa_NumberOfFrames <= 0) {
      return 0;
    }

    return ULONG(iFrame)%pCOA->oa_NumberOfFrames;

  // if not looping
  } else {

    // clamp
    if (iFrame < 0) {
      return 0;
    } else if (iFrame>=pCOA->oa_NumberOfFrames) {
      return pCOA->oa_NumberOfFrames - 1;
    } else {
      return iFrame;
    }
  }
}

TIME CAnimObject::GetStartTime(void) const
{
  return ao_tmAnimStart;
}

// Get info about time passed until now in current animation
TIME CAnimObject::GetPassedTime(void) const
{
  if (ao_AnimData == NULL) return 0.0f;

  INDEX iStoppedFrame;
  class COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];

  if (!(ao_ulFlags & AOF_PAUSED)) {
    iStoppedFrame = ClipFrame((INDEX)((_pTimer->CurrentTick() - ao_tmAnimStart) / pCOA->oa_SecsPerFrame));
  } else {
    iStoppedFrame = ClipFrame((INDEX)(ao_tmAnimStart / pCOA->oa_SecsPerFrame));
  }

  return(iStoppedFrame * pCOA->oa_SecsPerFrame);
}

/*
 * If animation is finished
 */
BOOL CAnimObject::IsAnimFinished(void) const
{
  if (ao_AnimData == NULL) return FALSE;
  if (ao_ulFlags & AOF_LOOPING) return FALSE;

  INDEX iStoppedFrame;
  class COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];

  if (!(ao_ulFlags & AOF_PAUSED)) {
    iStoppedFrame = ClipFrame((INDEX)((_pTimer->CurrentTick() - ao_tmAnimStart) / pCOA->oa_SecsPerFrame));
  } else {
    iStoppedFrame = ClipFrame((INDEX)(ao_tmAnimStart / pCOA->oa_SecsPerFrame));
  }
  
  return(iStoppedFrame == pCOA->oa_NumberOfFrames-1);
}

// get number of animations in curent anim data
INDEX CAnimObject::GetAnimsCt(void) const
{
  if (ao_AnimData == NULL) return 1;
  ASSERT(ao_AnimData != NULL);

  return(ao_AnimData->ad_NumberOfAnims);
};

// get index of current animation
INDEX CAnimObject::GetAnim(void) const
{
  return(ao_iCurrentAnim);
};

INDEX CAnimObject::GetLastAnim(void) const
{
  return ao_iLastAnim;
}

/*
 * Gets the number of current frame.
 */
INDEX CAnimObject::GetFrame(void) const
{
  return FrameInTime(_pTimer->CurrentTick());  // return frame index that coresponds to current moment
}

/* Gets number of frames in current anim. */
INDEX CAnimObject::GetFramesInCurrentAnim(void) const
{
  ASSERT(ao_AnimData != NULL);

  return ao_AnimData->ad_Anims[ao_iCurrentAnim].oa_NumberOfFrames;
};

/*
 * Get  information for linear interpolation beetween frames.
 */
void CAnimObject::GetFrame(INDEX &iFrame0, INDEX &iFrame1, FLOAT &fRatio) const
{
  if (ao_AnimData == NULL || ao_AnimData->ad_NumberOfAnims <= 0 ||
    ao_AnimData->ad_Anims[ao_iCurrentAnim].oa_NumberOfFrames <= 0)
  {
    iFrame0 = 0;
    iFrame1 = 0;
    fRatio = 0.0f;
    return;
  }

  ASSERT(ao_AnimData != NULL);
  ASSERT((ao_iCurrentAnim >= 0) && (ao_iCurrentAnim < ao_AnimData->ad_NumberOfAnims));
  TIME tmNow = _pTimer->CurrentTick() + _pTimer->GetLerpFactor()*_pTimer->TickQuantum;

  if (ao_ulFlags & AOF_PAUSED)
  {
    // return index of paused frame inside global frame array
    class COneAnim *pCOA = &ao_AnimData->ad_Anims[ao_iCurrentAnim];
    INDEX iStoppedFrame = ClipFrame((SLONG)(ao_tmAnimStart / pCOA->oa_SecsPerFrame));
    iFrame0 = iFrame1 = pCOA->oa_FrameIndices[iStoppedFrame];
    fRatio = 0.0f;
  }
  else
  {
    // return index of frame inside global frame array of frames in given moment
    TIME tmCurrentRelative = tmNow - ao_tmAnimStart;

    if (tmCurrentRelative >= 0) {
      class COneAnim *pOA0 = &ao_AnimData->ad_Anims[ao_iCurrentAnim];
      float fFrameNow = (tmCurrentRelative) / pOA0->oa_SecsPerFrame;
      iFrame0 = pOA0->oa_FrameIndices[ClipFrame(ULONG(fFrameNow))];
      iFrame1 = pOA0->oa_FrameIndices[ClipFrame(ULONG(fFrameNow + 1))];
      fRatio = fFrameNow - (float)floor(fFrameNow);

    } else {
      class COneAnim *pOA0 = &ao_AnimData->ad_Anims[ao_iLastAnim];
      class COneAnim *pOA1 = &ao_AnimData->ad_Anims[ao_iCurrentAnim];
      INDEX iAnim = ao_iCurrentAnim;
      ((CAnimObject*)this)->ao_iCurrentAnim = ao_iLastAnim;
      float fFrameNow = tmCurrentRelative / pOA0->oa_SecsPerFrame + pOA0->oa_NumberOfFrames;
      iFrame0 = pOA0->oa_FrameIndices[Clamp(SLONG(fFrameNow),  0L, pOA0->oa_NumberOfFrames - 1L)];
      INDEX iFrameNext = SLONG(fFrameNow + 1);

      if (iFrameNext >= pOA0->oa_NumberOfFrames) {
        iFrame1 = pOA1->oa_FrameIndices[0];
      } else {
        iFrame1 = pOA0->oa_FrameIndices[Clamp(iFrameNext,  0L, pOA0->oa_NumberOfFrames - 1L)];
      }

      ((CAnimObject*)this)->ao_iCurrentAnim = iAnim;
      fRatio = fFrameNow - (float)floor(fFrameNow);
    }
  }
}

void CAnimObject::Write_t(CTStream *pstr)
{
  (*pstr).WriteID_t("ANOB");
  (*pstr).WriteRawChunk_t(&ao_tmAnimStart, sizeof(TIME));
  (*pstr).WriteRawChunk_t(&ao_iCurrentAnim, sizeof(INDEX));
  (*pstr).WriteRawChunk_t(&ao_iLastAnim, sizeof(INDEX));
  (*pstr).WriteRawChunk_t(&ao_ulFlags, sizeof(INDEX));
};

void CAnimObject::Read_t(CTStream *pstr)
{
  if ((*pstr).PeekID_t() == CChunkID("ANOB")) {
    (*pstr).ExpectID_t("ANOB");
    (*pstr).ReadRawChunk_t(&ao_tmAnimStart, sizeof(TIME));
    (*pstr).ReadRawChunk_t(&ao_iCurrentAnim, sizeof(INDEX));
    (*pstr).ReadRawChunk_t(&ao_iLastAnim, sizeof(INDEX));
    (*pstr).ReadRawChunk_t(&ao_ulFlags, sizeof(INDEX));

  // legacy
  } else {
    (*pstr).ReadRawChunk_t(&ao_tmAnimStart, sizeof(TIME));
    (*pstr).ReadRawChunk_t(&ao_iCurrentAnim, sizeof(INDEX));
    ao_iLastAnim = ao_iCurrentAnim;
    ao_ulFlags = 0;
  }

  // clamp animation
  if (ao_AnimData == NULL || ao_iCurrentAnim >= GetAnimsCt() )
  {
    ao_iCurrentAnim = 0;
  }

  // clamp animation
  if (ao_AnimData == NULL || ao_iLastAnim >= GetAnimsCt() )
  {
    ao_iLastAnim = 0;
  }
};
