/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ANIMOBJECT_H
#define SE_INCL_ANIMOBJECT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Objects/GameObject.h>

#include <Engine/Anim/Animated.h>

//! Animation object flags.
enum AnimObjectFlags
{
  //! Current animation is paused.
  AOF_PAUSED       = (1L << 0), 

  //! Anim object is playing a looping animation.
  AOF_LOOPING      = (1L << 1), 

  //! Don't restart anim (used for PlayAnim()).
  AOF_NORESTART    = (1L << 2),

  //! Smoothly change between anims.
  AOF_SMOOTHCHANGE = (1L << 3),
};

//! An instance of animateable object.
class ENGINE_API CAnimObject : public CChangeable, public CGameObject, public IAnimated
{
  public:
    TIME ao_tmAnimStart;      // time when current anim was started
    INDEX ao_iCurrentAnim;    // index of active animation
    ULONG ao_ulFlags;         // flags
    INDEX ao_iLastAnim;       // index of last animation (for smooth transition)
    CAnimData *ao_AnimData;

  public:
    //! Default constructor.
    /*!
      Some of usual smart pointer functions are implemented, because AnimObjects
      behave as smart pointers to AnimData objects
    */
    CAnimObject(void);

    //! Destructor.
    ~CAnimObject(void);

    //! Copy from another object of same class.
    void Copy(CAnimObject &aoOther);

    //! Synchronize with another animation object (set same anim and frames).
    void Synchronize(CAnimObject &aoOther);

    //! Copying of AnimObjects is not allowed.
    inline CAnimObject(const CAnimObject &aoOther)
    {
      ASSERT(FALSE);
    };

    inline const CAnimObject &operator=(const CAnimObject &aoOther)
    {
      ASSERT(FALSE);
      return *this;
    };

    //! Clip frame index to be inside valid range (wrap around for looping anims).
    INDEX ClipFrame(INDEX iFrame) const;

    //! Loop anims forward.
    void NextAnim(void);

    //! Loop anims backward.
    void PrevAnim(void);

    //! Loop frames forward.
    void NextFrame(void);

    //! Loop frames backward.
    void PrevFrame(void);

    //! Select frame in given time offset.
    void SelectFrameInTime(TIME tmOffset);

    //! Select first frame.
    void FirstFrame(void);

    //! Select last frame.
    void LastFrame(void);

    //! Test if some updateable object is up to date with this anim object.
    BOOL IsUpToDate(const CUpdateable &ud) const;

    //! Get animation's info.
    void GetAnimInfo(INDEX iAnimNo, CAnimInfo &aiInfo) const;

    //! Attach data to this object.
    void SetData(CAnimData *pAD);

    //! Get current anim data ptr.
    __forceinline CAnimData *GetData() { return ao_AnimData; };

    //! Get animation's length.
    FLOAT GetCurrentAnimLength(void) const;
    
    //! Get animation's length.
    FLOAT GetAnimLength(INDEX iAnim) const;

    //! Get number of animations in current anim data.
    INDEX GetAnimsCt() const;

    //! If animation has finished.
    BOOL IsAnimFinished(void) const;

    //! Get time when current anim was started.
    TIME GetStartTime(void) const;

    //! Get passed time from start of animation.
    TIME GetPassedTime(void) const;

    //! Start new animation -- obsolete.
    void StartAnim(INDEX iNew);

    //! Start playing an animation.
    void PlayAnim(INDEX iNew, ULONG ulFlags);

    //! Seamlessly continue playing another animation from same point.
    //! NOTE: Never called.
    void SwitchToAnim(INDEX iNew);

    //! Set new animation but doesn't starts it.
    void SetAnim(INDEX iNew);

    //! Reset anim (restart).
    void ResetAnim();

    //! Pauses current animation.
    void PauseAnim();

    //! Continues paused animation.
    void ContinueAnim();

    //! Offsets the animation phase.
    void OffsetPhase(TIME tm);

    //! Retrieves paused flag.
    BOOL IsPaused(void);

    //! Gets the number of current animation.
    INDEX GetAnim(void) const;

    //! Gets the number of last animation.
    INDEX GetLastAnim(void) const;

    //! Gets the number of current frame.
    INDEX GetFrame(void) const;

    //! Gets number of frames in current anim.
    INDEX GetFramesInCurrentAnim(void) const;

    //! Get information for linear interpolation beetween frames.
    void GetFrame(INDEX &iFrame0, INDEX &iFrame1, FLOAT &fRatio) const;
    
    //! Calculate frame that coresponds to given time.
    INDEX FrameInTime(TIME time) const;

  // IO
  public:
    //! Read from stream.
    void Read_t(CTStream *istrFile); // throw char *

    //! Write to stream.
    void Write_t(CTStream *ostrFile); // throw char *

    //! Obtain animation and set it for this object.
    void SetData_t(const CTFileName &fnmAnim); // throw char *
};

#endif /* include-once check. */