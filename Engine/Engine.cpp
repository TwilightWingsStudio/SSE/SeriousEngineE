/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Build.h>
#include <Core/Core.h>
#include <Core/Base/Profiling.h>
#include <Engine/Input/Input.h>
#include <Core/Base/Console.h>
#include <Core/Base/Console_internal.h>
#include <Core/Base/Statistics_Internal.h>
#include <Core/Base/Shell.h>
#include <Core/Base/CRC.h>
#include <Core/Base/CRCTable.h>
#include <Engine/Base/ProgressHook.h>
#include <Engine/Sound/SoundListener.h>
#include <Engine/Sound/SoundLibrary.h>
#include <Engine/Graphics/GfxLibrary.h>
#include <Engine/Graphics/Font.h>
#include <Engine/Network/Network.h>
#include <Core/Templates/DynamicContainer.cpp>
#include <Engine/Resources/ResourceManager.h>
#include <Core/Templates/StaticArray.cpp>
#include <Engine/Input/IFeel.h>

// this version string can be referenced from outside the engine
ENGINE_API CTString _strEngineBuild  = "";
ENGINE_API ULONG _ulEngineBuildMajor = _SE_BUILD_MAJOR;
ENGINE_API ULONG _ulEngineBuildMinor = _SE_BUILD_MINOR;

// critical section for access to zlib functions
CSyncMutex zip_csLock; 

// to keep system gamma table
static UWORD auwSystemGamma[256 * 3];

// OS info
static CTString sys_strOS = "";
static INDEX sys_iOSMajor = 0;
static INDEX sys_iOSMinor = 0;
static INDEX sys_iOSBuild = 0;
static CTString sys_strOSMisc = "";

// CPU info
static CTString sys_strCPUVendor = "";
static INDEX sys_iCPUType = 0;
static INDEX sys_iCPUFamily = 0;
static INDEX sys_iCPUModel = 0;
static INDEX sys_iCPUStepping = 0;
static BOOL  sys_bCPUHasMMX = 0;
static BOOL  sys_bCPUHasCMOV = 0;
static INDEX sys_iCPUMHz = 0;

// RAM info
static INDEX sys_iRAMPhys = 0;
static INDEX sys_iRAMSwap = 0;

// HDD info
static INDEX sys_iHDDSize = 0;
static INDEX sys_iHDDFree = 0;
static INDEX sys_iHDDMisc = 0;

// MOD info
static CTString sys_strModName = "";
static CTString sys_strModExt  = "";

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
  switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
      break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;

    default:
      ASSERT(FALSE);
  }

  return TRUE;
}

#include <Core/Base/Processor.h>
#include <thread>

static void DetectCPU(void)
{
  char strVendor[12 + 1];
  strVendor[12] = 0;
  ULONG ulTFMS;
  ULONG ulFeatures;

  CProcessorId procId0(0, 0); // Vendor Name
  CProcessorId procId1(1, 0); // Features

  memcpy(&strVendor[0], &procId0.EBX(), 4);
  memcpy(&strVendor[4], &procId0.EDX(), 4);
  memcpy(&strVendor[8], &procId0.ECX(), 4);

  ulTFMS = procId1.EAX();
  ulFeatures = procId1.EDX();

  INDEX iType     = (ulTFMS >> 12) & 0x3;
  INDEX iFamily   = (ulTFMS >>  8) & 0xF;
  INDEX iModel    = (ulTFMS >>  4) & 0xF;
  INDEX iStepping = (ulTFMS >>  0) & 0xF;

  const int ctCore = std::thread::hardware_concurrency();

  CInfoF("  Vendor: %s\n", strVendor);
  CInfoF("  Type: %d, Family: %d, Model: %d, Stepping: %d\n", iType, iFamily, iModel, iStepping);
  CInfoF("  Cores: %d\n", ctCore);
  CInfoF("  Clock: %.0fMHz\n", _pTimer->tm_llCPUSpeedHZ / 1E6);

  BOOL bMMX  = ulFeatures & (1 << 23);
  BOOL bCMOV = ulFeatures & (1 << 15);

  CTString strYes = "Yes";
  CTString strNo = "No";

  CInfoF("  MMX : %s\n", bMMX  ? strYes : strNo);
  CInfoF("  CMOV: %s\n", bCMOV ? strYes : strNo);

  sys_strCPUVendor = strVendor;
  sys_iCPUType = iType;
  sys_iCPUFamily =  iFamily;
  sys_iCPUModel = iModel;
  sys_iCPUStepping = iStepping;
  sys_bCPUHasMMX = bMMX != 0;
  sys_bCPUHasCMOV = bCMOV != 0;
  sys_iCPUMHz = INDEX(_pTimer->tm_llCPUSpeedHZ / 1E6);

  if (!bMMX) FatalError("MMX support required but not present!");
}

static void DetectCPUWrapper(void)
{
  __try {
    DetectCPU();
  } __except(EXCEPTION_EXECUTE_HANDLER) {
    CWarningF( TRANS("Cannot detect CPU: exception raised.\n"));
  }
}

#include <Engine/Graphics/GfxProfile.h>
#include <Engine/Models/ModelProfile.h>
#include <Engine/Sound/SoundProfile.h>
#include <Engine/Network/NetworkProfile.h>
#include <Engine/Rendering/RenderProfile.h>
#include <Engine/World/WorldEditingProfile.h>
#include <Engine/World/PhysicsProfile.h>

// startup engine 
ENGINE_API void SE_InitEngine(CTString strGameID)
{
  _pCallProgressHook_t = &CallProgressHook_t;

  SE_InitCore();

  _pfGfxProfile = new CGfxProfile;
  _pfModelProfile = new CModelProfile;
  _pfSoundProfile = new CSoundProfile;
  _pfNetworkProfile = new CNetworkProfile;
  _pfRenderProfile = new CRenderProfile;
  _pfWorldEditingProfile = new CWorldEditingProfile;
  _pfPhysicsProfile = new CPhysicsProfile;

  CInfoF(TRANS("--- Engine Startup ---\n"));
  _strEngineBuild.PrintF( TRANS("Engine Build: %d.%d"), _SE_BUILD_MAJOR, _SE_BUILD_MINOR);
  CInfoF("  %s\n", _strEngineBuild);

  _pResourceMgr = new CResourceManager;
  _pGfx   = new CGfxLibrary;
  _pSound = new CSoundLibrary;
  _pInput = new CInput;
  _pNetwork = new CNetworkLibrary;

  CRCT_Init();


  // print basic engine info

  // print info on the started application
  //CInfoF(TRANS("Executable: %s\n"), strExePath);
  //CInfoF(TRANS("Assumed engine directory: %s\n"), _fnmApplicationPath);

  CInfoF("\n");

  // report os info
  CInfoF(TRANS("Examining underlying OS...\n"));
  OSVERSIONINFOA osv;
  memset(&osv, 0, sizeof(osv));
  osv.dwOSVersionInfoSize = sizeof(osv);

  if (GetVersionExA(&osv)) {

    switch (osv.dwPlatformId)
    {
      case VER_PLATFORM_WIN32s:         sys_strOS = "Win32s";  break;
      case VER_PLATFORM_WIN32_WINDOWS:  sys_strOS = "Win9x"; break;
      case VER_PLATFORM_WIN32_NT:       sys_strOS = "WinNT"; break;
      default: sys_strOS = "Unknown\n"; break;
    }

    sys_iOSMajor = osv.dwMajorVersion;
    sys_iOSMinor = osv.dwMinorVersion;
    sys_iOSBuild = osv.dwBuildNumber & 0xFFFF;
    sys_strOSMisc = osv.szCSDVersion;

    CInfoF(TRANS("  Type: %s\n"), sys_strOS.ConstData());
    CInfoF(TRANS("  Version: %d.%d, build %d\n"), 
      osv.dwMajorVersion, osv.dwMinorVersion, osv.dwBuildNumber & 0xFFFF);
    CInfoF(TRANS("  Misc: %s\n"), osv.szCSDVersion);

  } else {
    CErrorF(TRANS("Error getting OS info: %s\n"), GetWindowsError(GetLastError()) );
  }

  CInfoF("\n");

  // report CPU
  CInfoF(TRANS("Detecting CPU...\n"));
  DetectCPUWrapper();
  CInfoF("\n");

  // report memory info
  extern void ReportGlobalMemoryStatus(void);
  ReportGlobalMemoryStatus();

  MEMORYSTATUS ms;
  GlobalMemoryStatus(&ms);

  #define MB (1024 * 1024)
  sys_iRAMPhys = ms.dwTotalPhys     / MB;
  sys_iRAMSwap = ms.dwTotalPageFile / MB;

  // initialize zip semaphore
  zip_csLock.cs_iIndex = -1;  // not checked for locking order

  // get info on the first disk in system
  /*
  DWORD dwSerial;
  DWORD dwFreeClusters;
  DWORD dwClusters;
  DWORD dwSectors;
  DWORD dwBytes;

  char strDrive[] = "C:\\";
  strDrive[0] = strExePath[0];

  GetVolumeInformationA(strDrive, NULL, 0, &dwSerial, NULL, NULL, NULL, 0);
  GetDiskFreeSpaceA(strDrive, &dwSectors, &dwBytes, &dwFreeClusters, &dwClusters);
  sys_iHDDSize = __int64(dwSectors) * dwBytes * dwClusters / MB;
  sys_iHDDFree = __int64(dwSectors) * dwBytes * dwFreeClusters / MB;
  sys_iHDDMisc = dwSerial;
  */
 
  // add console variables

  // OS info
  _pShell->DeclareSymbol("user const CTString sys_strOS    ;", &sys_strOS);
  _pShell->DeclareSymbol("user const INDEX sys_iOSMajor    ;", &sys_iOSMajor);
  _pShell->DeclareSymbol("user const INDEX sys_iOSMinor    ;", &sys_iOSMinor);
  _pShell->DeclareSymbol("user const INDEX sys_iOSBuild    ;", &sys_iOSBuild);
  _pShell->DeclareSymbol("user const CTString sys_strOSMisc;", &sys_strOSMisc);

  // CPU info
  _pShell->DeclareSymbol("user const CTString sys_strCPUVendor;", &sys_strCPUVendor);
  _pShell->DeclareSymbol("user const INDEX sys_iCPUType       ;", &sys_iCPUType    );
  _pShell->DeclareSymbol("user const INDEX sys_iCPUFamily     ;", &sys_iCPUFamily  );
  _pShell->DeclareSymbol("user const INDEX sys_iCPUModel      ;", &sys_iCPUModel   );
  _pShell->DeclareSymbol("user const INDEX sys_iCPUStepping   ;", &sys_iCPUStepping);
  _pShell->DeclareSymbol("user const INDEX sys_bCPUHasMMX     ;", &sys_bCPUHasMMX  );
  _pShell->DeclareSymbol("user const INDEX sys_bCPUHasCMOV    ;", &sys_bCPUHasCMOV );
  _pShell->DeclareSymbol("user const INDEX sys_iCPUMHz        ;", &sys_iCPUMHz     );

  // RAM info
  _pShell->DeclareSymbol("user const INDEX sys_iRAMPhys;", &sys_iRAMPhys);
  _pShell->DeclareSymbol("user const INDEX sys_iRAMSwap;", &sys_iRAMSwap);
  _pShell->DeclareSymbol("user const INDEX sys_iHDDSize;", &sys_iHDDSize);
  _pShell->DeclareSymbol("user const INDEX sys_iHDDFree;", &sys_iHDDFree);
  _pShell->DeclareSymbol("     const INDEX sys_iHDDMisc;", &sys_iHDDMisc);

  // MOD info
  _pShell->DeclareSymbol("user const CTString sys_strModName;", &sys_strModName);
  _pShell->DeclareSymbol("user const CTString sys_strModExt;",  &sys_strModExt);

  // Stock clearing
  _pShell->DeclareSymbol("user void FreeUnusedStock(void);", &CResourceManager::FreeUnusedStocks);
  
  // Timer tick quantum
  _pShell->DeclareSymbol("user const FLOAT fTickQuantum;", (FLOAT*)&_pTimer->TickQuantum);

  // keep mod name in sys cvar
  sys_strModName = _strModName;
  sys_strModExt  = _strModExt;

// checking of crc
#if 0
  ULONG ulCRCActual = -2;
  SLONG ulCRCExpected = -1;

  try {
    // get the checksum of engine
    #ifndef NDEBUG
      #define SELFFILE "Bin\\Debug\\EngineD.dll"
      #define SELFCRCFILE "Bin\\Debug\\EngineD.crc"
    #else
      #define SELFFILE "Bin\\Engine.dll"
      #define SELFCRCFILE "Bin\\Engine.crc"
    #endif

    ulCRCActual = GetFileCRC32_t(CTString(SELFFILE));
    // load expected checksum from the file on disk
    ulCRCExpected = 0;
    LoadIntVar(CTString(SELFCRCFILE), ulCRCExpected);
  } catch (char *strError) {
    CErrorF("%s\n", strError);
  }

  // if not same
  if (ulCRCActual != ulCRCExpected) {
    // don't run
    //FatalError(TRANS("Engine CRC is invalid.\nExpected %08x, but found %08x.\n"), ulCRCExpected, ulCRCActual);
  }
#endif

  if (!_bDedicatedServer) {
    _pInput->Initialize();
  }

  _pGfx->Init();
  _pSound->Init();

  if (strGameID != "") {
    _pNetwork->Init(strGameID);

    // just make classes declare their shell variables
    try {
      CResource* pec = _pResourceMgr->Obtain_t(CResource::TYPE_ENTITYCLASS, CTString("Classes\\PlayerController.ecl"));
      ASSERT(pec != NULL);
      _pResourceMgr->Release(pec);  // this must not be a dependency!
    // if couldn't find player class
    } catch (char *strError) {
      FatalError(TRANS("Cannot initialize classes:\n%s"), strError);
    }

  } else {
    _pNetwork = NULL;
  }

  // mark that default fonts aren't loaded (yet)
  _pfdDisplayFont = NULL;
  _pfdConsoleFont = NULL;

  // readout system gamma table
  HDC  hdc = GetDC(NULL);
  BOOL bOK = GetDeviceGammaRamp( hdc, &auwSystemGamma[0]);
  _pGfx->gl_ulFlags |= GLF_ADJUSTABLEGAMMA;

  if (!bOK) {
    _pGfx->gl_ulFlags &= ~GLF_ADJUSTABLEGAMMA;
    CWarningF( TRANS("\nWARNING: Gamma, brightness and contrast are not adjustable!\n\n"));
  } // done

  ReleaseDC(NULL, hdc);
  
  // init IFeel
  #ifdef SE1_IFEEL
  HWND hwnd = NULL;//GetDesktopWindow();
  HINSTANCE hInstance = GetModuleHandle(NULL);

  if (IFeel_InitDevice(hInstance,hwnd))
  {
    CTString strDefaultProject = "Data\\Default.ifr";

    // get project file name for this device
    CTString strIFeel = IFeel_GetProjectFileName();

    // if no file name is returned use default file
    if (strIFeel.Length() == 0) {
      strIFeel = strDefaultProject;
    }

    if (!IFeel_LoadFile(strIFeel))
    {
      if (strIFeel != strDefaultProject)
      {
        IFeel_LoadFile(strDefaultProject);
      }
    }

    CInfoF("\n");
  }
  #endif // SE1_IFEEL
}


// shutdown entire engine
ENGINE_API void SE_EndEngine(void)
{
  // restore system gamma table (if needed)
  if (_pGfx->gl_ulFlags & GLF_ADJUSTABLEGAMMA) {
    HDC  hdc = GetDC(NULL);
    BOOL bOK = SetDeviceGammaRamp( hdc, &auwSystemGamma[0]);
    //ASSERT(bOK);
    ReleaseDC(NULL, hdc);
  }

  // free all memory used by the crc cache
  CRCT_Clear();

  // shutdown
  if (_pNetwork != NULL) {
    delete _pNetwork; 
    _pNetwork = NULL;
  }

  delete _pResourceMgr; _pResourceMgr = NULL;
  delete _pInput;    _pInput   = NULL;  
  delete _pSound;    _pSound   = NULL;  
  delete _pGfx;      _pGfx     = NULL;    

  // shutdown profilers
  _sfStats.Clear();
  _pfGfxProfile->pf_apcCounters.Clear();
  _pfGfxProfile->pf_aptTimers.Clear();
  _pfModelProfile->pf_apcCounters.Clear();
  _pfModelProfile->pf_aptTimers.Clear();
  _pfSoundProfile->pf_apcCounters.Clear();
  _pfSoundProfile->pf_aptTimers.Clear();
  _pfNetworkProfile->pf_apcCounters.Clear();
  _pfNetworkProfile->pf_aptTimers.Clear();
  _pfRenderProfile->pf_apcCounters.Clear();
  _pfRenderProfile->pf_aptTimers.Clear();
  _pfWorldEditingProfile->pf_apcCounters.Clear();
  _pfWorldEditingProfile->pf_aptTimers.Clear();
  _pfPhysicsProfile->pf_apcCounters.Clear();
  _pfPhysicsProfile->pf_aptTimers.Clear();

  // remove default fonts if needed
  if (_pfdDisplayFont != NULL) {
    delete _pfdDisplayFont;  _pfdDisplayFont = NULL;
  }

  if (_pfdConsoleFont != NULL) {
    delete _pfdConsoleFont;  _pfdConsoleFont = NULL;
  } 

  // deinit IFeel
  #ifdef SE1_IFEEL
  IFeel_DeleteDevice();
  #endif // SE1_IFEEL

  SE_EndCore();
}


// prepare and load some default fonts
ENGINE_API void SE_LoadDefaultFonts(void)
{
  _pfdDisplayFont = new CFontData;
  _pfdConsoleFont = new CFontData;

  // try to load default fonts
  try {
    _pfdDisplayFont->Load_t( CTFILENAME( "Fonts\\Display3-narrow.fnt"));
    _pfdConsoleFont->Load_t( CTFILENAME( "Fonts\\Console1.fnt"));

  } catch (char *strError) {
    FatalError( TRANS("Error loading font: %s."), strError);
  }

  // change fonts' default spacing a bit
  _pfdDisplayFont->SetCharSpacing( 0);
  _pfdDisplayFont->SetLineSpacing(+1);
  _pfdConsoleFont->SetCharSpacing(-1);
  _pfdConsoleFont->SetLineSpacing(+1);
  _pfdConsoleFont->SetFixedWidth();
}


// updates main windows' handles for windowed mode and fullscreen
ENGINE_API void SE_UpdateWindowHandle( HWND hwndMain)
{
  ASSERT(hwndMain != NULL);

  _hwndMain = hwndMain;
  _bFullScreen = _pGfx != NULL && (_pGfx->gl_ulFlags & GLF_FULLSCREEN);
}


static BOOL TouchBlock(UBYTE *pubMemoryBlock, INDEX ctBlockSize)
{
#if 0
  // cannot pretouch block that are smaller than 64KB :(
  ctBlockSize -= 16 * 0x1000;
  if (ctBlockSize < 4) return FALSE; 

  __try {
    // 4 times should be just enough
    for (INDEX i = 0; i < 4; i++)
    {
      // must do it in asm - don't know what VC will try to optimize
      __asm {
        // The 16-page skip is to keep Win 95 from thinking we're trying to page ourselves in
        // (we are doing that, of course, but there's no reason we shouldn't) - THANX JOHN! :)
        mov   esi,dword ptr [pubMemoryBlock]
        mov   ecx,dword ptr [ctBlockSize]
        shr   ecx,2
touchLoop:
        mov   eax,dword ptr [esi]
        mov   ebx,dword ptr [esi+16*0x1000]
        add   eax,ebx     // BLA, BLA, TROOCH, TRUCH
        add   esi,4
        dec   ecx
        jnz   touchLoop
      }
    }
  }
  __except(EXCEPTION_EXECUTE_HANDLER) { 
    return FALSE;
  }
#endif

  return TRUE;
}


// pretouch all memory commited by process
extern BOOL _bNeedPretouch = FALSE;
ENGINE_API extern void SE_PretouchIfNeeded(void)
{
  #if 0
  // only if pretouching is needed?
  extern INDEX gam_bPretouch;

  if (!_bNeedPretouch || !gam_bPretouch) {
    return;
  }
  
  _bNeedPretouch = FALSE;

  // set progress bar
  SetProgressDescription( TRANS("pretouching"));
  CallProgressHook_t(0.0f);

  // need to do this two times - 1st for numerations, and 2nd for real (progress bar and that shit)
  BOOL bPretouched = TRUE;
  INDEX ctFails, ctBytes, ctBlocks;
  INDEX ctPassBytes, ctTotalBlocks;

  for (INDEX iPass = 1; iPass <= 2; iPass++)
  { 
    // flush variables
    ctFails = 0; ctBytes = 0; ctBlocks = 0; ctTotalBlocks = 0;
    void *pvNextBlock = NULL;
    MEMORY_BASIC_INFORMATION mbi;

    // lets walk thru memory blocks
    while (VirtualQuery(pvNextBlock, &mbi, sizeof(mbi)))
    { 
      // don't mess with kernel's memory and zero-sized blocks    
      if (((ULONG)pvNextBlock) > 0x7FFF0000UL || mbi.RegionSize < 1) {
        break;
      }

      // if this region of memory belongs to our process
      BOOL bCanAccess = (mbi.Protect == PAGE_READWRITE); // || (mbi.Protect==PAGE_EXECUTE_READWRITE);

      if (mbi.State == MEM_COMMIT && bCanAccess && mbi.Type == MEM_PRIVATE) // && !IsBadReadPtr( mbi.BaseAddress, 1)
      { 
        // increase counters
        ctBlocks++;
        ctBytes += mbi.RegionSize;

        // in first pass we only count
        if (iPass == 1) {
          goto nextRegion;
        }

        // update progress bar
        CallProgressHook_t((FLOAT)ctBytes / ctPassBytes);

        // pretouch
        ASSERT( mbi.RegionSize>0);
        BOOL bOK = TouchBlock((UBYTE *)mbi.BaseAddress, mbi.RegionSize);
        if (!bOK) { 
          // whoops!
          ctFails++;
        }
        // for easier debugging (didn't help much, though)
        //Sleep(5);  
      }

nextRegion:
      // advance to next region
      pvNextBlock = ((UBYTE*)mbi.BaseAddress) + mbi.RegionSize;
      ctTotalBlocks++;
    }

    // done with one pass
    ctPassBytes = ctBytes;

    if ((ctPassBytes / 1024 / 1024) > sys_iRAMPhys) {
      // not enough RAM, sorry :(
      bPretouched = FALSE;
      break;
    }
  }

  // report
  if (bPretouched) {
    // success
    CInfoF(TRANS("Pretouched %d KB of memory in %d blocks.\n"), ctBytes/1024, ctBlocks); //, ctTotalBlocks);
  } else {
    // fail
    CErrorF(TRANS("Cannot pretouch due to lack of physical memory (%d KB of overflow).\n"), ctPassBytes / 1024 - sys_iRAMPhys * 1024);
  }

  // some blocks failed?
  if (ctFails > 1) {
    CWarningF(TRANS("(%d blocks were skipped)\n"), ctFails);
  }

  //_pShell->Execute("StockDump();");
  #endif

  _bNeedPretouch = FALSE;
}


#if 0

      // printout block info
      CDebugF("--------\n");
      CTString strTmp1, strTmp2;
      CDebugF("Base/Alloc Address: 0x%8X / 0x%8X - Size: %d KB\n", mbi.BaseAddress, mbi.AllocationBase, mbi.RegionSize / 1024);

      switch (mbi.Protect)
      {
        case PAGE_READONLY:          strTmp1 = "PAGE_READONLY";          break;
        case PAGE_READWRITE:         strTmp1 = "PAGE_READWRITE";         break;
        case PAGE_WRITECOPY:         strTmp1 = "PAGE_WRITECOPY";         break;
        case PAGE_EXECUTE:           strTmp1 = "PAGE_EXECUTE";           break;
        case PAGE_EXECUTE_READ:      strTmp1 = "PAGE_EXECUTE_READ";      break;
        case PAGE_EXECUTE_READWRITE: strTmp1 = "PAGE_EXECUTE_READWRITE"; break;
        case PAGE_GUARD:             strTmp1 = "PAGE_GUARD";             break;
        case PAGE_NOACCESS:          strTmp1 = "PAGE_NOACCESS";          break;
        case PAGE_NOCACHE:           strTmp1 = "PAGE_NOCACHE";           break;
      }

      switch (mbi.AllocationProtect)
      {
        case PAGE_READONLY:          strTmp2 = "PAGE_READONLY";          break;
        case PAGE_READWRITE:         strTmp2 = "PAGE_READWRITE";         break;
        case PAGE_WRITECOPY:         strTmp2 = "PAGE_WRITECOPY";         break;
        case PAGE_EXECUTE:           strTmp2 = "PAGE_EXECUTE";           break;
        case PAGE_EXECUTE_READ:      strTmp2 = "PAGE_EXECUTE_READ";      break;
        case PAGE_EXECUTE_READWRITE: strTmp2 = "PAGE_EXECUTE_READWRITE"; break;
        case PAGE_GUARD:             strTmp2 = "PAGE_GUARD";             break;
        case PAGE_NOACCESS:          strTmp2 = "PAGE_NOACCESS";          break;
        case PAGE_NOCACHE:           strTmp2 = "PAGE_NOCACHE";           break;
      }

      CDebugF("Current/Alloc protect: %s / %s\n", strTmp1, strTmp2);

      switch (mbi.State)
      {
        case MEM_COMMIT:  strTmp1 = "MEM_COMMIT";  break;
        case MEM_FREE:    strTmp1 = "MEM_FREE";    break;
        case MEM_RESERVE: strTmp1 = "MEM_RESERVE"; break;
      }

      switch (mbi.Type)
      {
        case MEM_IMAGE:   strTmp2 = "MEM_IMAGE";   break;
        case MEM_MAPPED:  strTmp2 = "MEM_MAPPED";  break;
        case MEM_PRIVATE: strTmp2 = "MEM_PRIVATE"; break;
      }

      CDebugF("State/Type: %s / %s\n", strTmp1, strTmp2);

#endif
