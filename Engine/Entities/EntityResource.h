/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENTITYRESOURCE_H
#define SE_INCL_ENTITYRESOURCE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum EntityResourceFlags
{
  ERF_EDITOR = (1UL << 1),
};

//! Game resource of some entity.
class ENGINE_API CEntityResource
{
  public:
    enum CResource::Type er_eType;     // type of resource
    ULONG er_ulId;                     // resource ID in this class
    ULONG er_ulFlags;                  // resource flags
    CTFileName er_fnmResource;         // resource file name

    // This one is auto-filled by SCape on DLL initialization
    union                               // pointer to resource
    { 
      CResource *er_pInStock;
      CTextureData *er_ptdTexture;      // for textures
      CModelData   *er_pmdModel;        // for models
      CSoundData   *er_psdSound;        // for sounds
      CEntityClass *er_pecEntityClass;  // for entity classes
      CModelConfiguration *er_pModelConfiguration; // [SEE]
      void *er_pvPointer;   // for comparison needs
    };

  public:
    //! Constructor for a specific resource.
    /*!
      NOTE: This uses special EFNM initialization for CTFileName class!
    */
    CEntityResource(CResource::Type eType, ULONG ulId, ULONG ulFlags, char *strEFNMResource)
      : er_eType(eType)
      , er_ulId(ulId)
      , er_ulFlags(ulFlags)
      , er_fnmResource(strEFNMResource, 4)
    { 
      er_pvPointer = NULL; 
    };

    //! Default constructor.
    CEntityResource(void) 
    { 
      er_ulId = (ULONG) -1; 
      er_ulFlags = 0;
      er_pvPointer = NULL; 
    };

    //! Obtain the resource.
    void Obtain_t(void);  // throw char *
    
    //! Obtain the resource with immediate error reports.
    void ObtainWithCheck(void);
    
    //! Add resource to crc table
    void AddToCRCTable(void);
    
    //! Release the resource.
    void Release(void);

    //! Returns resource type.
    inline enum CResource::Type GetType() const
    {
      return er_eType;
    }

    //! Returns resource id.
    inline ULONG GetId() const
    {
      return er_ulId;
    }
    
    //! Returns resource flags.
    inline ULONG GetFlags() const
    {
      return er_ulFlags;
    }
    
    //! Returns resource filename.
    const CTFileName &GetFileName() const
    {
      return er_fnmResource;
    }

    //! Returns resource pointer.
    CModelConfiguration *GetModelConfig()
    {
      return er_pModelConfiguration;
    }
};

#endif /* include-once check. */