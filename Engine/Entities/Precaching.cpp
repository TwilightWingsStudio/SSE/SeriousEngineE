/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/Precaching.h>

#include <Engine/Resources/ResourceManager.h>

#include <Engine/Models/ModelData.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Sound/SoundData.h>

// TODO: Move CAutoPrecacheSound into a separate file
//! Default constructor.
CAutoPrecacheSound::CAutoPrecacheSound()
{
  apc_psd = NULL;
}

//! Destructor.
CAutoPrecacheSound::~CAutoPrecacheSound()
{
  if (apc_psd != NULL) {
    _pResourceMgr->Release(apc_psd);
  }
}

//! Precache sound from a file.
void CAutoPrecacheSound::Precache(const CTFileName &fnm)
{
  if (apc_psd != NULL) {
    _pResourceMgr->Release(apc_psd);
  }
  
  try {
    if (fnm != "") {
      CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_SOUND, fnm);
      apc_psd = static_cast<CSoundData *>(pser);
    }
  } catch (char *strError) {
    CErrorF("%s\n", strError);
  }
}

// TODO: Move CAutoPrecacheModel into a separate file
//! Default constructor.
CAutoPrecacheModel::CAutoPrecacheModel()
{
  apc_pmd = NULL;
}

//! Destructor.
CAutoPrecacheModel::~CAutoPrecacheModel()
{
  if (apc_pmd != NULL) {
    _pResourceMgr->Release(apc_pmd);
  }
}

//! Precache model from a file.
void CAutoPrecacheModel::Precache(const CTFileName &fnm)
{
  if (apc_pmd != NULL) {
    _pResourceMgr->Release(apc_pmd);
  }
  
  try {
    if (fnm != "") {
      CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_MODEL, fnm);
      apc_pmd = static_cast<CModelData *>(pser);
    }
  } catch (char *strError) {
    CErrorF("%s\n", strError);
  }
}

// TODO: Move CAutoPrecacheTexture into a separate file
//! Default constructor.
CAutoPrecacheTexture::CAutoPrecacheTexture()
{
  apc_ptd = NULL;
}

//! Destructor.
CAutoPrecacheTexture::~CAutoPrecacheTexture()
{
  if (apc_ptd != NULL) {
    _pResourceMgr->Release(apc_ptd);
  }
}

//! Precache texture from a file.
void CAutoPrecacheTexture::Precache(const CTFileName &fnm)
{
  if (apc_ptd != NULL) {
    _pResourceMgr->Release(apc_ptd);
  }
  
  try {
    if (fnm != "") {
      CResource *pser = _pResourceMgr->Obtain_t(CResource::TYPE_TEXTURE, fnm);
      apc_ptd = static_cast<CTextureData *>(pser);
    }
  } catch (char *strError) {
    CErrorF("%s\n", strError);
  }
}