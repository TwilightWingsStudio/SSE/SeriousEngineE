/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATESTACK_H
#define SE_INCL_STATESTACK_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Interface that represents entity state stack.
class ENGINE_API IStateStack
{
  public:
    //! Jump to a new state.
    virtual void Jump(SLONG slThisState, SLONG slTargetState, BOOL bOverride, const CEntityEvent &eeInput) {};
    
    //! Call a subautomaton.
    virtual void Call(SLONG slThisState, SLONG slTargetState, BOOL bOverride, const CEntityEvent &eeInput) {};
    
    //! Return from a subautomaton.
    virtual void Return(SLONG slThisState, const CEntityEvent &eeReturn) {};

    //! Print stack to debug output
    virtual const char *PrintStackDebug(void)
    {
      return "<not implemented>";
    }

    //! Set next timer event to occur at given moment time.
    virtual void SetTimerAt(TIME timeAbsolute) {};

    //! Set next timer event to occur after given time has elapsed.
    virtual void SetTimerAfter(TIME timeDelta) {};

    //! Cancel eventual pending timer.
    virtual void UnsetTimer(void) {};
};

#endif  /* include-once check. */