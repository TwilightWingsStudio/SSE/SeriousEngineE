/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_INTERNALCLASSES_H
#define SE_INCL_INTERNALCLASSES_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityProperties.h>
#include <Core/Base/CTString.h>
#include <Core/IO/FileName.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Matrix.h>
#include <Core/Math/AABBox.h>
#include <Core/Math/Placement.h>
#include <Engine/Entities/PlayerCharacter.h>

#define DECL_DLL ENGINE_API
#include <Engine/Classes/MovableEntity.h>
#include <Engine/Classes/MovableModelEntity.h>
#include <Engine/Classes/MovableBrushEntity.h>
#include <Engine/Classes/PawnEntity.h>
#include <Engine/Classes/PawnControllerEntity.h>
#include <Engine/Classes/PlayerControllerEntity.h>
#undef DECL_DLL


#endif  /* include-once check. */

