/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityProperties.h>
#include <Core/Base/ErrorReporting.h>
#include <Engine/Entities/Precaching.h>
#include <Core/Base/Translation.h>
#include <Core/Base/CRCTable.h>

#include <Engine/Resources/ResourceManager.h>

//! Get pointer to entity event from its code
class CLibEntityEvent *CLibEntityClass::EventForId(SLONG slEvent) const
{
  for (int iEvent = 0; iEvent < dec_ctEvents; iEvent++)
  {
    CLibEntityEvent *pEvent = dec_adeeEvents[iEvent];

    if (pEvent->dee_slEvent == slEvent) {
      return pEvent;
    }
  }
  
  return dec_pdecBase == NULL ? NULL : dec_pdecBase->EventForId(slEvent);
}

// Get pointer to entity property from its name.
class CEntityProperty *CLibEntityClass::PropertyForName(const CTString &strPropertyName) const
{
  // For each property
  for (INDEX iProperty = 0; iProperty < dec_ctProperties; iProperty++)
  {
    CEntityProperty &ep = dec_aepProperties[iProperty];

    // If it has that name then return it
    if (ep.GetName() == strPropertyName) {
      return &ep;
    }
  }
  
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    return dec_pdecBase->PropertyForName(strPropertyName);
  // Otherwise, none found
  } else {
    return NULL;
  }
}

// Get pointer to entity property from its packed identifier.
class CEntityProperty *CLibEntityClass::PropertyForId(ULONG idEntityProperty, CEntityProperty::Type eType) const
{
  // For each property
  for (INDEX iProperty = 0; iProperty < dec_ctProperties; iProperty++)
  {
    CEntityProperty &ep = dec_aepProperties[iProperty];

    // If it has that same identifier
    if (ep.GetId() == idEntityProperty)
    {
      const BOOL bIgnoreType = eType == CEntityProperty::EPT_INVALID;

      // If it also has same type then return it
      if (bIgnoreType || ep.GetType() == eType) {
        return &ep;

      // If it has different type
      } else {
        // Return that it was not found, this makes the whole thing much safer
        return NULL;
      }
    }
  }
  
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    return dec_pdecBase->PropertyForId(idEntityProperty, eType);
  // Otherwise, none found
  } else {
    return NULL;
  }
};

// Get pointer to resource from its identifier.
class CEntityResource *CLibEntityClass::ResourceForId(ULONG idEntityResource, CResource::Type eType)
{
  // For each resource
  for (INDEX iResource = 0; iResource < dec_ctResources; iResource++)
  {
    CEntityResource &er = dec_aerResources[iResource];

    // If it has that same identifier
    if (er.GetId() == idEntityResource)
    {
      const BOOL bIgnoreType = eType == CResource::TYPE_INVALID;

      if (bIgnoreType || (er.GetType() == eType)) {
        // Obtain it
        er.ObtainWithCheck();
        
        // Return it
        return &er;

      // If it has different type
      } else {
        // Return that it was not found, this makes the whole thing much safer
        return NULL;
      }
    }
  }
  
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    return dec_pdecBase->ResourceForId(idEntityResource, eType);
  // Otherwise, none found
  } else {
    return NULL;
  }
}

// Get pointer to resource from the resource.
class CEntityResource *CLibEntityClass::ResourceForPointer(void *pv)
{
  // For each resource
  for (INDEX iResource = 0; iResource < dec_ctResources; iResource++)
  {
    CEntityResource &er = dec_aerResources[iResource];
    
    // If it has that same pointer
    if (er.er_pvPointer == pv) {
      // Obtain it
      er.ObtainWithCheck();
      
      // Return it
      return &er;
    }
  }
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    // 
    return dec_pdecBase->ResourceForPointer(pv);
  // Otherwise, none found
  } else {
    return NULL;
  }
}

// Precache given resource
void CLibEntityClass::Precache(SLONG idEntityResource, CResource::Type eType, INDEX iUser /* = -1 */)
{
  CTmpPrecachingNow tpn;

  CEntityResource *pEntityResource = ResourceForId(idEntityResource, eType);
  ASSERT(pEntityResource != NULL);
  pEntityResource->ObtainWithCheck();

  // Deal with CLibEntityClass hooks.
  if (pEntityResource->GetType() == CResource::TYPE_ENTITYCLASS) {
    CLibEntityClass *pClass = pEntityResource->er_pecEntityClass->ec_pdecDLLClass;
    pClass->dec_OnPrecache(pClass, iUser);
  }
}

// Precache given resource
void CLibEntityClass::PrecacheModel(SLONG idEntityResource)
{
  Precache(idEntityResource, CResource::TYPE_MODEL);
}

// Precache 'modelcfg' resource.
void CLibEntityClass::PrecacheModelConfig(SLONG idEntityResource)
{
  Precache(idEntityResource, CResource::TYPE_MODELCONFIGURATION);
}

// Precache 'texture' resource.
void CLibEntityClass::PrecacheTexture(SLONG idEntityResource)
{
  Precache(idEntityResource, CResource::TYPE_TEXTURE);
}

// Precache 'sound' resource.
void CLibEntityClass::PrecacheSound(SLONG idEntityResource)
{
  Precache(idEntityResource, CResource::TYPE_SOUND);
}

// Precache 'class' resource.
void CLibEntityClass::PrecacheClass(SLONG idEntityResource, INDEX iUser /* = -1 */)
{
  Precache(idEntityResource, CResource::TYPE_ENTITYCLASS, iUser);
}

// Get event handler given state and event code.
CEntity::pEventHandler CLibEntityClass::HandlerForStateAndEvent(SLONG slState, SLONG slEvent) const
{
  // We ignore the event code here
  (void) slEvent;

  // For each handler
  for (INDEX iHandler = 0; iHandler < dec_ctHandlers; iHandler++)
  {
    const CEventHandlerEntry &ehe = dec_aeheHandlers[iHandler];
    
    // If it has that same state then return it
    if (ehe.GetStateId() == slState) {
      return ehe.GetFunction();
    }
  }
  
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    return dec_pdecBase->HandlerForStateAndEvent(slState, slEvent);
  // Otherwise, none found
  } else {
    return NULL;
  }
}

// Get event handler name for given state.
const char *CLibEntityClass::HandlerNameForState(SLONG slState) const
{
  // For each handler
  for (INDEX iHandler = 0; iHandler < dec_ctHandlers; iHandler++)
  {
    const CEventHandlerEntry &ehe = dec_aeheHandlers[iHandler];
    
    // If it has that same state then return its name
    if (ehe.GetStateId() == slState) {
      return ehe.GetName();
    }
  }
  
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    return dec_pdecBase->HandlerNameForState(slState);
  // Otherwise, none found
  } else {
    return "no handler!?";
  }
}

// Get derived class override for given state.
SLONG CLibEntityClass::GetOverridenState(SLONG slState) const
{
  // For each handler
  for (INDEX iHandler = 0; iHandler < dec_ctHandlers; iHandler++)
  {
    const CEventHandlerEntry &ehe = dec_aeheHandlers[iHandler];
    
    // If it has that same base state
    if (ehe.GetBaseStateId() >= 0 && ehe.GetBaseStateId() == slState) {
      // Return overriden state with possible recursive overriding
      return GetOverridenState(ehe.GetStateId());
    }
  }
  
  // If base class exists then look in the base class
  if (dec_pdecBase != NULL) {
    return dec_pdecBase->GetOverridenState(slState);
  // Otherwise, none found
  } else {
    return slState;
  }
}