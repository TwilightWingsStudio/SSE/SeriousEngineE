/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_LIVEENTITY_H
#define SE_INCL_LIVEENTITY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Engine/Entities/Entity.h>

#define HEALTH_VALUE_MULTIPLIER 100

//! Entity that is alive (has health).
class ENGINE_API CLiveEntity : public CEntity 
{
  public:
    FLOAT en_fHealth;            // health of the entity

  public:
    //! Constructor.
    CLiveEntity(void);

    //! Copy entity from another entity of same class.
    virtual void Copy(CEntity &enOther, ULONG ulFlags);

    //! Read from stream.
    virtual void Read_t(CTStream *istr);  // throw char *

    //! Write to stream.
    virtual void Write_t(CTStream *ostr); // throw char *

    //! Apply some damage to the entity (see event EDamage for more info)
    virtual void ReceiveDamage(CEntity *penInflictor, INDEX iDamageType, INDEX iDamageAmmount,
                               const FLOAT3D &vHitPoint, const FLOAT3D &vDirection);

    //! Returns bytes of memory used by this object
    inline SLONG GetUsedMemory(void) 
    {
      return(sizeof(CLiveEntity) - sizeof(CEntity) + CEntity::GetUsedMemory());
    };
};

#endif /* include-once check. */