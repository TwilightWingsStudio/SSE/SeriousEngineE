/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityProperties.h>
#include <Engine/Entities/LastPositions.h>
#include <Engine/Entities/EntityCollision.h>
#include <Engine/Entities/ShadingInfo.h>
#include <Engine/Light/LightSource.h>

#include <Core/Math/Geometry.inl>
#include <Core/Math/Clipping.inl>
#include <Core/Math/Float.h>
#include <Core/Math/OBBox.h>
#include <Core/Math/Functions.h>

#include <Core/Base/CRC.h>
#include <Core/Base/Console.h>
#include <Core/Base/Statistics_Internal.h>
#include <Engine/Network/Network.h>
#include <Engine/Network/PlayerTarget.h>
#include <Engine/Network/SessionState.h>
#include <Engine/Brushes/Brush.h>
#include <Engine/Brushes/BrushTransformed.h>
#include <Engine/Brushes/BrushArchive.h>
#include <Engine/World/World.h>
#include <Engine/World/WorldRayCasting.h>
#include <Engine/World/PhysicsProfile.h>
#include <Engine/Resources/ReplaceFile.h>
#include <Engine/Entities/InternalClasses.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Sound/SoundData.h>
#include <Engine/Sound/SoundObject.h>
#include <Engine/Graphics/Texture.h>
#include <Engine/Meshes/SM_Render.h>
#include <Engine/Meshes/ModelConfiguration.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/TerrainArchive.h>
#include <Engine/Terrain/TerrainRayCasting.h>
#include <Engine/Terrain/TerrainMisc.h>

#include <Core/Base/ListIterator.inl>

#include <Core/Templates/BSP.h>

// FIXME: CPP inclusion
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/StaticStackArray.cpp>

#include <Engine/Resources/ResourceManager.h>

// A reference to a void event for use as default parameter
const EVoid _evVoid;
const CEntityEvent &_eeVoid = _evVoid;

extern INDEX _ctEntities;
extern INDEX _ctPredictorEntities;

/////////////////////////////////////////////////////////////////////
// CEntity

// Default constructor.
CEntity::CEntity(void)
{
  en_pbrBrush = NULL;
  en_psiShadingInfo = NULL;
  en_pciCollisionInfo = NULL;
  en_pecClass = NULL;
  en_ulFlags = 0;
  en_ulSpawnFlags = 0xFFFFFFFFL;    // active always
  en_ulPhysicsFlags = 0;
  en_ulCollisionFlags = 0;
  en_ulID = 0;
  en_RenderType = RT_NONE;
  en_fSpatialClassificationRadius = -1.0f;
  n_pParent = NULL;
  en_plpLastPositions = NULL;
  _ctEntities++;
}

// Destructor.
CEntity::~CEntity(void)
{
  ASSERT(GetReferenceCount() == 0);
  ASSERT(en_ulID != 0);
  ASSERT(en_RenderType == RT_NONE);
  
  // Remove it from container in its world
  ASSERT(!en_pwoWorld->wo_cenEntities.IsMember(this));
  ASSERT(!en_pwoWorld->wo_cenPawnEntities.IsMember(this)); // [SSE]
  en_pwoWorld->wo_cenAllEntities.Remove(this);

  // Unset spatial clasification
  en_rdSectors.Clear();

  /*
  Models are always destructed on End(), but brushes and terrains are not, so
  if the pointer is not NULL, then it must be a brush or terrain.
  Both of them are derived from CBrushBase so GetBrushType() will return its real type 
  */

  // If it is brush of terrain
  if (en_pbrBrush != NULL) 
  {
    INDEX btType = en_pbrBrush->GetBrushType();
    
    // If this is brush3d then free the brush
    if (btType == CBrushBase::BT_BRUSH3D) {
      en_pwoWorld->wo_baBrushes.ba_abrBrushes.Delete(en_pbrBrush);
      en_pbrBrush = NULL;
      
    // If this is terrain then free the brush
    } else if (btType == CBrushBase::BT_TERRAIN) { 
      en_pwoWorld->wo_taTerrains.ta_atrTerrains.Delete(en_ptrTerrain);
      en_pbrBrush = NULL;
    
    // Unknown type
    } else {
      ASSERTALWAYS("Unsupported brush type");
    }
  }
  
  // Clear entity type
  en_RenderType = RT_NONE;
  en_pecClass->RemReference();
  en_pecClass = NULL;

  en_fSpatialClassificationRadius = -1.0f;
  _ctEntities--;

  if (IsPredictable()) 
  {
    if (en_pwoWorld->wo_cenPredictable.IsMember(this)) {
      en_pwoWorld->wo_cenPredictable.Remove(this);
    }
  }
  
  if (en_ulFlags & ENF_WILLBEPREDICTED) 
  {
    if (en_pwoWorld->wo_cenWillBePredicted.IsMember(this)) {
      en_pwoWorld->wo_cenWillBePredicted.Remove(this);
    }
  }
  
  if (IsPredictor()) 
  {
    if (en_pwoWorld->wo_cenPredictor.IsMember(this)) {
      en_pwoWorld->wo_cenPredictor.Remove(this);
      _ctPredictorEntities--;
    }
  }
}

/////////////////////////////////////////////////////////////////////
// Access functions

// Test if the entity is an empty brush.
BOOL CEntity::IsEmptyBrush(void) const
{
  // if it is not brush then it is not empty brush
  if (en_RenderType != CEntity::RT_BRUSH && en_RenderType != RT_FIELDBRUSH) {
    return FALSE;
  // if it is brush
  } else {
    // get its brush
    CBrush3D &brBrush = *en_pbrBrush;
    
    // get the first mip of the brush
    CBrushMip *pbmMip = brBrush.GetFirstMip();
    
    // it is empty if it has zero sectors
    return pbmMip->bm_abscSectors.Count() == 0;
  }
}

// Return max Game Players
INDEX CEntity::GetMaxPlayers(void) 
{
  return NET_MAXGAMEPLAYERS;
};

void CEntity::GetPlayerControllers(TDynamicContainer<CEntity> &cenOutput)
{
  const INDEX ctMaxPlayers = CEntity::GetMaxPlayers();
  CSessionState &ses = _pNetwork->ga_sesSessionState;

  for (INDEX iPlayer = 0; iPlayer < ctMaxPlayers; iPlayer++)
  {
    CPlayerTarget &plt = ses.ses_apltPlayers[iPlayer];

    if (plt.IsActive()) {
      cenOutput.Add(plt.GetEntity());
    }
  }
}

void CEntity::GetPlayerEntities(TDynamicContainer<CEntity> &cenOutput)
{
  const INDEX ctMaxPlayers = CEntity::GetMaxPlayers();
  CSessionState &ses = _pNetwork->ga_sesSessionState;

  for (INDEX iPlayer = 0; iPlayer < ctMaxPlayers; iPlayer++)
  {
    CPlayerTarget &plt = ses.ses_apltPlayers[iPlayer];

    if (!plt.IsActive()) {
      continue;
    }

    CEntity *pen = plt.GetEntity();

    // If we have controller then return connected pawn.
    if (IsDerivedFromClass(pen, &CPlayerControllerEntity_DLLClass)) {
      CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(pen);
      CEntity *penPawn = penController->GetPawn();
      
      if (penPawn) {
        cenOutput.Add(penPawn);
      }

      continue;
    } else {
      cenOutput.Add(pen);
    }
  }
}

// Return Player Entity
CEntity *CEntity::GetPlayerController(INDEX iPlayer)
{
  ASSERT(iPlayer >= 0 && iPlayer < GetMaxPlayers());

  CSessionState &ses = _pNetwork->ga_sesSessionState;
  CPlayerTarget &plt = ses.ses_apltPlayers[iPlayer];

  if (plt.IsActive()) {
    return plt.GetEntity();
  } else {
    return NULL;
  }
}

// Return Player Entity
CEntity *CEntity::GetPlayerEntity(INDEX iPlayer)
{
  ASSERT(iPlayer >= 0 && iPlayer < GetMaxPlayers());

  CEntity *pen = GetPlayerController(iPlayer);
  
  if (IsDerivedFromClass(pen, &CPlayerControllerEntity_DLLClass)) {
    CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(pen);
    return penController->GetPawn();
  }
  
  return pen;
}

// Get bounding box of this entity - for AI purposes only.
void CEntity::GetBoundingBox(FLOATaabbox3D &box)
{
  if (en_pciCollisionInfo != NULL) {
    box = en_pciCollisionInfo->ci_boxCurrent;
  } else {
    GetSize(box);
    box += GetPlacement().pl_PositionVector;
  }
}

// Get size of this entity - for UI purposes only.
void CEntity::GetSize(FLOATaabbox3D &box)
{
  if (en_RenderType == CEntity::RT_MODEL || en_RenderType == CEntity::RT_EDITORMODEL) {
    en_pmoModelObject->GetCurrentFrameBBox(box);
    box.StretchByVector(en_pmoModelObject->mo_Stretch);
  
  } else if (en_RenderType == CEntity::RT_SKAMODEL || en_RenderType == CEntity::RT_SKAEDITORMODEL) {
    GetModelInstance()->GetCurrentCollisionBox(box);
    box.StretchByVector(GetModelInstance()->mi_vStretch);
  
  } else if (en_RenderType == CEntity::RT_TERRAIN) {
    GetTerrain()->GetAllTerrainBBox(box);
  
  } else if (en_RenderType == CEntity::RT_BRUSH || en_RenderType == CEntity::RT_FIELDBRUSH) {
    CBrushMip *pbm = en_pbrBrush->GetFirstMip();
    if (pbm == NULL) {
      box = FLOATaabbox3D(FLOAT3D(0, 0, 0), FLOAT3D(0, 0, 0));
    } else {
      box = pbm->bm_boxBoundingBox;
      box += -GetPlacement().pl_PositionVector;
    }
    
  } else {
    box = FLOATaabbox3D(FLOAT3D(0, 0, 0), FLOAT3D(0, 0, 0));
  }
}

const CTString &CEntity::GetDescription(void) const // name + some more verbose data
{
  static const CTString strDummyDescription("");
  return strDummyDescription;
}

// Get first target of this entity.
CEntity *CEntity::GetTarget(void) const
{
  return NULL;
}

BOOL CEntity::GetTargetChain(TDynamicContainer<CEntity> &cenOutput) const 
{
  return GetTargetChain(cenOutput, (CEntity *)this);
}

// Check if entity can be used as a target.
BOOL CEntity::IsTargetable(void) const
{
  // cannot be targeted unless this function is overridden
  return FALSE;
}

// Check if entity is marker
BOOL CEntity::IsMarker(void) const{
  // cannot be marker unless this function is overridden
  return FALSE;
}

BOOL CEntity::IsPawn(void) const
{
  return FALSE;
}

// Check if entity is important
BOOL CEntity::IsImportant(void) const{
  // cannot be important unless this function is overridden
  return FALSE;
}

// Check if entity is moved on a route set up by its targets.
BOOL CEntity::MovesByTargetedRoute(CTString &strTargetProperty) const
{
  return FALSE;
}

// Check if entity can drop marker for making linked route.
BOOL CEntity::DropsMarker(CTFileName &fnmMarkerClass, CTString &strTargetProperty) const
{
  return FALSE;
}

// Get light source information - return NULL if not a light source.
CLightSource *CEntity::GetLightSource(void)
{
  return NULL;
}

BOOL CEntity::IsTargetValid(SLONG slPropertyOffset, CEntity *penTarget)
{
  return TRUE;
}

// Get anim data for given animation property - return NULL for none.
CAnimData *CEntity::GetAnimData(SLONG slPropertyOffset)
{
  return NULL;
}

// Get force type name, return empty string if not used.
const CTString &CEntity::GetForceName(INDEX iForce)
{
  static const CTString strDummyName("");
  return strDummyName;
}

// Get forces in given point.
void CEntity::GetForce(INDEX iForce, const FLOAT3D &vPoint, CForceStrength &fsGravity, CForceStrength &fsField)
{
  // Default gravity
  fsGravity.fs_vDirection = FLOAT3D(0,-1,0);
  fsGravity.fs_fAcceleration = 9.81f;
  fsGravity.fs_fVelocity = 0;

  // No force field
  fsField.fs_fAcceleration = 0;
}
// Get entity that controls the force, used for change notification checking.
CEntity *CEntity::GetForceController(INDEX iForce)
{
  return NULL;
}

// Adjust model shading parameters if needed - return TRUE if needs model shadows.
BOOL CEntity::AdjustShadingParameters(FLOAT3D &vLightDirection, COLOR &colLight, COLOR &colAmbient)
{
  return TRUE;
}

// Adjust model mip factor if needed.
void CEntity::AdjustMipFactor(FLOAT &fMipFactor)
{
  (void)fMipFactor;
  NOTHING;
}

// Get a different model object for rendering - so entity can change its appearance dynamically
// NOTE: base model is always used for other things (physics, etc).
CModelObject *CEntity::GetModelForRendering(void)
{
  return en_pmoModelObject;
}

// Get a different model instance for rendering - so entity can change its appearance dynamically
// NOTE: base model is always used for other things (physics, etc).
CModelInstance *CEntity::GetModelInstanceForRendering(void)
{
  return en_pmiModelInstance;
}

// Get fog type name, return empty string if not used.
const CTString &CEntity::GetFogName(INDEX iFog)
{
  static const CTString strDummyName("");
  return strDummyName;
}

// Get fog, return FALSE for none.
BOOL CEntity::GetFog(INDEX iFog, class CFogParameters &fpFog)
{
  return FALSE;
}

// Get haze type name, return empty string if not used.
const CTString &CEntity::GetHazeName(INDEX iHaze)
{
  static const CTString strDummyName("");
  return strDummyName;
}

// Get haze, return FALSE for none.
BOOL CEntity::GetHaze(INDEX iHaze, class CHazeParameters &hpHaze, FLOAT3D &vViewDir)
{
  return FALSE;
}

// Get mirror type name, return empty string if not used.
const CTString &CEntity::GetMirrorName(INDEX iMirror)
{
  static const CTString strDummyName("");
  return strDummyName;
}

// Get mirror, return FALSE for none.
BOOL CEntity::GetMirror(INDEX iMirror, class CMirrorParameters &mpMirror)
{
  return FALSE;
}

// Get gradient type name, return empty string if not used.
const CTString &CEntity::GetGradientName(INDEX iGradient)
{
  static const CTString strDummyName("");
  return strDummyName;
}

// Get gradient, return FALSE for none.
BOOL CEntity::GetGradient(INDEX iGradient, class CGradientParameters &gpGradient)
{
  return FALSE;
}

// Get classification box stretching vector.
FLOAT3D CEntity::GetClassificationBoxStretch(void)
{
  return FLOAT3D(1.0f, 1.0f, 1.0f);
}

// Get field information - return NULL if not a field.
CFieldSettings *CEntity::GetFieldSettings(void)
{
  return NULL;
}

// Render particles made by this entity.
void CEntity::RenderParticles(void)
{
  NOTHING;
}

// Get current collision box index for this entity.
INDEX CEntity::GetCollisionBoxIndex(void)
{
  return 0; // by default, use only box 0
}

// Get current collision box - override for custom collision boxes.
void CEntity::GetCollisionBoxParameters(INDEX iBox, FLOATaabbox3D &box, INDEX &iEquality)
{
  // If this is ska model
  if (en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    box.minvect = GetModelInstance()->GetCollisionBoxMin(iBox);
    box.maxvect = GetModelInstance()->GetCollisionBoxMax(iBox);
    FLOATaabbox3D boxNS = box;
    box.StretchByVector(GetModelInstance()->mi_vStretch);
    iEquality = GetModelInstance()->GetCollisionBoxDimensionEquality(iBox);
  } else {
    box.minvect = en_pmoModelObject->GetCollisionBoxMin(iBox);
    box.maxvect = en_pmoModelObject->GetCollisionBoxMax(iBox);
    box.StretchByVector(en_pmoModelObject->mo_Stretch);
    iEquality = en_pmoModelObject->GetCollisionBoxDimensionEquality(iBox);
  }
}

// Render game view
void CEntity::RenderGameView(CDrawPort *pdp, void *pvUserData)
{
  NOTHING;
}

// Apply mirror and stretch to the entity if supported
void CEntity::MirrorAndStretch(FLOAT fStretch, BOOL bMirrorX)
{
  NOTHING;
}

// Get offset for depth-sorting of alpha models (in meters, positive is nearer)
FLOAT CEntity::GetDepthSortOffset(void)
{
  return 0.0f;
}

// Get visibility tweaking bits
ULONG CEntity::GetVisTweaks(void)
{
  return 0;
}

// Get max tessellation level
FLOAT CEntity::GetMaxTessellationLevel(void)
{
  return 0.0f;
}

// Get pointer to your predictor/predicted
CEntity *CEntity::GetPredictionPair(void)
{
  // This should never be called, it must be overriden if prediction is used!
  ASSERT(FALSE);
  return this;  // This is safest to return if in release version
}

// Set pointer to your predictor/predicted.
void CEntity::SetPredictionPair(CEntity *penPair)
{
  // This should never be called, it must be overriden if prediction is used!
  ASSERT(FALSE);
}

// Add to prediction any entities that this entity depends on
void CEntity::AddDependentsToPrediction(void)
{
}

// Called by other entities to set time prediction parameter
void CEntity::SetPredictionTime(TIME tmAdvance)   // give time interval in advance to set
{
  NOTHING; // by default, don't use time prediction
}

// Called by engine to get the upper time limit 
TIME CEntity::GetPredictionTime(void)   // return moment in time up to which to predict this entity
{
  return -1.0f; // by default, don't use time prediction
}

// Get maximum allowed range for predicting this entity
FLOAT CEntity::GetPredictionRange(void)
{
  return UpperLimit(0.0f);    // by default, cli_fPredictEntitiesRange is the limit
}

// Copy for prediction
void CEntity::CopyForPrediction(CEntity &enOrg)
{
  // This should never be called, it must be overriden if prediction is used!
  ASSERT(FALSE);
}

CEntity *CEntity::GetPredictor(void)
{
  ASSERT(IsPredicted());
  CEntity *pen = GetPredictionPair();
  ASSERT(pen->IsPredictor());
  return pen;
}

CEntity *CEntity::GetPredicted(void)
{
  ASSERT(IsPredictor());
  CEntity *pen = GetPredictionPair();
  ASSERT(pen != NULL);
  ASSERT(pen->IsPredicted());
  return pen;
}

// Become predictable/unpredictable
void CEntity::SetPredictable(BOOL bON)
{
  // If predictor then do nothing
  if (IsPredictor()) {
    return;
  }

  // If already set then check that valid
  if (IsPredictable()) {
    ASSERT(en_pwoWorld->wo_cenPredictable.IsMember(this));
    
    // If turning on then do nothing
    if (bON) {
      return;
    }
    
    // Mark as not predictable
    en_ulFlags&=~ENF_PREDICTABLE;
    
    // Remove from container
    en_pwoWorld->wo_cenPredictable.Remove(this);

  // If not set then check that valid
  } else {
    ASSERT(!en_pwoWorld->wo_cenPredictable.IsMember(this));
    ASSERT(!IsPredictor());
    ASSERT(!IsPredicted());
    
    // If turning off then do nothing
    if (!bON) {
      return;
    }
    
    // Mark as predictable
    en_ulFlags|=ENF_PREDICTABLE;
    
    // Add to container
    en_pwoWorld->wo_cenPredictable.Add(this);
  }
}

// Check if this instance is head of prediction chain
BOOL CEntity::IsPredictionHead(void)
{
  // If predicted, or will be predicted, or is a temporary predictor 
  // then it cannot be head of the chain
  if (en_ulFlags & (ENF_PREDICTED | ENF_WILLBEPREDICTED | ENF_TEMPPREDICTOR)) {
    return FALSE;
  }
  
  // If predictor, but not currently in the last step of prediction then it cannot be head of the chain
  if ((en_ulFlags & ENF_PREDICTOR) 
      && 
      _pTimer->CurrentTick() <= _pNetwork->ga_sesSessionState.ses_tmPredictionHeadTick) 
  {
    return FALSE;
  }
  
  // Otherwise it is head of the chain
  return TRUE;
}

// Get the prediction original (predicted), or self if not predicting
CEntity *CEntity::GetPredictionTail(void)
{
  // If this is a predictor then it must be head of the prediction chain
  if (IsPredictor()) {
    //ASSERT(IsPredictionHead());
    // Get its predicted
    return GetPredicted();
  }
  
  // In other cases, return self
  return this;
}

// Check if active for prediction now
BOOL CEntity::IsAllowedForPrediction(void) const
{
  return !_pNetwork->IsPredicting() || IsPredictor();
}

// Check an event for prediction, returns true if already predicted
BOOL CEntity::CheckEventPrediction(ULONG ulTypeID, ULONG ulEventID)
{
  return _pNetwork->ga_sesSessionState.CheckEventPrediction(this, ulTypeID, ulEventID);
}

// Called after creating and setting its properties.
void CEntity::OnInitialize(const CEntityEvent &eeInput)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Try to find a handler in start state
  pEventHandler pehHandler = HandlerForStateAndEvent(1, eeInput.ee_slEvent);
  
  // If there is a handler then call the function
  if (pehHandler!=NULL) {
    (this->*pehHandler)(eeInput);
  // If there is no handler
  } else {
    ASSERTALWAYS("All entities must have Main procedure!");
  }
}
// Called before releasing entity.
void CEntity::OnEnd(void)
{
}

// Prepare entity (call after setting properties).
void CEntity::Initialize(const CEntityEvent &eeInput)
{
  CSetFPUPrecision FPUPrecision(FPT_24BIT);

  // Make sure we are not deleted during intialization
  CEntityPointer penThis = this;

  Initialize_internal(eeInput);
  
  // Set spatial clasification
  FindSectorsAroundEntity();
  
  // Precache all other things
  Precache();
}

// Internal function for entity reinitialization (do not discard shadows etc.).
void CEntity::Initialize_internal(const CEntityEvent &eeInput)
{
  #ifndef NDEBUG
    // Clear settings for debugging
    en_RenderType = RT_ILLEGAL;
  #endif

  // Remember brush zoning flag
  BOOL bWasZoning = en_ulFlags&ENF_ZONING;

  // Let derived class initialize according to the properties
  OnInitialize(eeInput);
  
  // Derived class must set all properties
//  ASSERT(en_RenderType != RT_ILLEGAL);

  // If this is a brush then test if zoning
  if (en_RenderType == RT_BRUSH || en_RenderType == RT_FIELDBRUSH) {
    BOOL bZoning = en_ulFlags & ENF_ZONING;
    
    // If switching from zoning to non-zoning then switch from zoning to non-zoning
    if (bWasZoning && !bZoning) {
      en_pbrBrush->SwitchToNonZoning();
      en_rdSectors.Clear();
    // If switching from non-zoning to zoning then switch from non-zoning to zoning
    } else if (!bWasZoning && bZoning) {
      en_pbrBrush->SwitchToZoning();
      en_rdSectors.Clear();
    }
  }

  // If it is a field brush
  CFieldSettings *pfs = GetFieldSettings();
  if (pfs != NULL) {
    // Remember its field settings
    ASSERT(en_RenderType == RT_FIELDBRUSH);
    en_pbrBrush->br_pfsFieldSettings = pfs;
  }
}

// Clean-up entity.
void CEntity::End(void)
{
  CSetFPUPrecision FPUPrecision(FPT_24BIT);
  /* NOTE: Must not remove from thinker/mover list here, or CServer::ProcessGameTick()
   * might crash!
   */
  End_internal();
}

// Clean-up entity internally.
void CEntity::End_internal(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Let derived class clean-up after itself
  OnEnd();

  // Clear last positions
  if (en_plpLastPositions != NULL) {
    delete en_plpLastPositions;
    en_plpLastPositions = NULL;
  }

  // Clear spatial classification
  en_fSpatialClassificationRadius = -1.0f;
  en_boxSpatialClassification = FLOATaabbox3D();

  // Depending on entity type
  switch (en_RenderType)
  {
    // If it is brush
    case RT_BRUSH:
      DiscardCollisionInfo();
      break;
      
    // If it is field brush
    case RT_FIELDBRUSH:
      DiscardCollisionInfo();
      break;

    // If it is model then free its model object
    case RT_MODEL:
    case RT_EDITORMODEL:
      delete en_pmoModelObject;
      delete en_psiShadingInfo;
      DiscardCollisionInfo();
      en_pmoModelObject = NULL;
      en_psiShadingInfo = NULL;
      break;
      
    // If it is ska model
    case RT_SKAMODEL:
    case RT_SKAEDITORMODEL:
      en_pmiModelInstance->Clear();
      delete en_pmiModelInstance;
      delete en_psiShadingInfo;
      DiscardCollisionInfo();
      en_pmiModelInstance = NULL;
      en_psiShadingInfo = NULL;
      break;
    case RT_TERRAIN:
      DiscardCollisionInfo();
      break;

    // If it is nothing then do nothing
    case RT_NONE:
    case RT_VOID:
      NOTHING;
      break;
      
    // If it is any other type
    default:
      ASSERTALWAYS("Unsupported entity type");
  }
  
  // Clear entity type
  en_RenderType = RT_NONE;
}

// Reinitialize the entity.
void CEntity::Reinitialize(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);
  End_internal();
  Initialize_internal(_eeVoid);
}

// Teleport this entity to a new location -- takes care of telefrag damage
void CEntity::Teleport(const CPlacement3D &plNew, BOOL bTelefrag /*=TRUE*/)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);
  ASSERT(en_fSpatialClassificationRadius>0);

  // If telefragging is on and the entity has collision box
  // then create the box of the entity at its new placement
  if (bTelefrag && en_pciCollisionInfo != NULL) {
    FLOATmatrix3D mRot;
    MakeRotationMatrixFast(mRot, plNew.pl_OrientationAngle);
    
    FLOAT3D vPos = plNew.pl_PositionVector;
    CMovingSphere &ms0 = en_pciCollisionInfo->ci_absSpheres[0];
    CMovingSphere &ms1 = en_pciCollisionInfo->ci_absSpheres[en_pciCollisionInfo->ci_absSpheres.Count() - 1];
    
    FLOATaabbox3D box;
    box  = FLOATaabbox3D(vPos+ms0.ms_vCenter * mRot, ms0.ms_fR);
    box |= FLOATaabbox3D(vPos+ms1.ms_vCenter * mRot, ms1.ms_fR);

    // First inflict huge damage there in the entities box
    InflictBoxDamage(this, DMT_TELEPORT, 100000.0f, box);
  }

  // Remember original orientation matrix
  FLOATmatrix3D mOld = en_mRotation;

  // Now put the entity there
  SetPlacement(plNew);
  
  // Movable entity
  if (en_ulPhysicsFlags & EPF_MOVABLE) {
    ((CMovableEntity*)this)->ClearTemporaryData();
    ((CMovableEntity*)this)->en_plLastPlacement = en_plPlacement;
    
    // Transform speeds
    FLOATmatrix3D mRel = en_mRotation * !mOld;
    ((CMovableEntity*)this)->en_vCurrentTranslationAbsolute *= mRel;
    
    if (_pNetwork->ga_ulDemoMinorVersion >= 7) {
      // Clear reference
      ((CMovableEntity*)this)->en_penReference = NULL;
      ((CMovableEntity*)this)->en_pbpoStandOn = NULL;
    }

    // Notify that it was teleported
    SendEvent(ETeleport());
    ((CMovableEntity*)this)->AddToMovers();

    extern INDEX ent_bReportSpawnInWall;
    if (ent_bReportSpawnInWall) {
      // If movable model then check if it was teleported inside a wall
      if (en_RenderType == RT_MODEL     || en_RenderType == RT_EDITORMODEL || 
          en_RenderType == RT_SKAMODEL  || en_RenderType == RT_SKAEDITORMODEL) 
      {
        CMovableModelEntity *pmme = (CMovableModelEntity*)this;
        CEntity *ppenObstacleDummy;
        if (pmme->CheckForCollisionNow(pmme->en_iCollisionBox, &ppenObstacleDummy)) {
          CDebugF("Entity '%s' was teleported inside a wall at (%g,%g,%g)!\n", 
            GetName(), 
            en_plPlacement.pl_PositionVector(1),
            en_plPlacement.pl_PositionVector(2),
            en_plPlacement.pl_PositionVector(3));
        }
      }
    }
  }
}

// Set placement of this entity. (use only in WEd!)
void CEntity::SetPlacement(const CPlacement3D &plNew)
{
  CSetFPUPrecision FPUPrecision(FPT_24BIT);
  
  // Check if orientation is changed
  BOOL bSameOrientation = (plNew.pl_OrientationAngle == en_plPlacement.pl_OrientationAngle);

  // If the orientation has not changed then set the placement and do all needed recalculation
  if (bSameOrientation) {
    SetPlacement_internal(plNew, en_mRotation, FALSE /* doesn't have to be near. */);
  
  // If the orientation has changed then calculate new rotation matrix
  } else {
    FLOATmatrix3D mRotation;
    MakeRotationMatrixFast(mRotation, plNew.pl_OrientationAngle);
    
    // Set the placement and do all needed recalculation
    SetPlacement_internal(plNew, mRotation, FALSE /* doesn't have to be near. */);
  }

  // If this entity has parent then adjust relative placement
  if (GetParentEntity() != NULL) {
    en_plRelativeToParent = en_plPlacement;
    en_plRelativeToParent.AbsoluteToRelativeSmooth(GetParentEntity()->en_plPlacement);
  }
}

// Fall down to floor. (use only in WEd!)
void CEntity::FallDownToFloor(void)
{
  CEntity::RenderType rt = GetRenderType();
  
  // Is this old model
  if (rt == CEntity::RT_MODEL || rt == CEntity::RT_EDITORMODEL) {
    ASSERT(en_pmoModelObject != NULL);
    
  // Is this ska model
  } else if (rt == CEntity::RT_SKAMODEL || rt == CEntity::RT_SKAEDITORMODEL) {
    ASSERT(GetModelInstance() != NULL);
  } else {
    return;
  }
  
  // if (rt != CEntity::RT_MODEL && rt != CEntity::RT_EDITORMODEL) return;
  // ASSERT(en_pmoModelObject != NULL);

  CPlacement3D plPlacement = GetPlacement();
  FLOAT3D vRay[4];
  
  // If it is movable entity
  if (en_ulPhysicsFlags & EPF_MOVABLE) {
    INDEX iEq;
    FLOATaabbox3D box;
    GetCollisionBoxParameters(GetCollisionBoxIndex(), box, iEq);
    FLOAT3D vMin = box.Min();
    FLOAT3D vMax = box.Max();
    
    // All ray casts start from same height
    vRay[0](2) = vMax(2);
    vRay[1](2) = vMax(2);
    vRay[2](2) = vMax(2);
    vRay[3](2) = vMax(2);

    vRay[0](1) = vMin(1);
    vRay[0](3) = vMin(3);
    vRay[1](1) = vMin(1);
    vRay[1](3) = vMax(3);
    vRay[2](1) = vMax(1);
    vRay[2](3) = vMin(3);
    vRay[3](1) = vMax(1);
    vRay[3](3) = vMax(3);
  } else {
    FLOATaabbox3D box;
    
    if (rt == CEntity::RT_SKAMODEL || rt == CEntity::RT_SKAEDITORMODEL) {
      GetModelInstance()->GetCurrentCollisionBox(box);
    } else {
      en_pmoModelObject->GetCurrentFrameBBox(box);
    }

    FLOAT3D vCenterUp = box.Center();
    vCenterUp(2) = box.Max()(2);
    vRay[0] = vCenterUp;
    vRay[1] = vCenterUp;
    vRay[2] = vCenterUp;
    vRay[3] = vCenterUp;
  }

  FLOAT fMaxY = -9999999.0f;
  BOOL bFloorHitted = FALSE;
  
  for (INDEX iRay = 0; iRay < 4; iRay++)
  {
    FLOAT3D vSource = plPlacement.pl_PositionVector+vRay[iRay];
    FLOAT3D vTarget = vSource;
    vTarget(2) -= 1000.0f;
    CCastRay crRay(this, vSource, vTarget);
    crRay.cr_ttHitModels = CCastRay::TT_NONE; // CCastRay::TT_FULLSEETHROUGH;
    crRay.cr_bHitTranslucentPortals = TRUE;
    crRay.cr_bPhysical = TRUE;
    GetWorld()->CastRay(crRay);
    
    if ((crRay.cr_penHit != NULL) && (crRay.cr_vHit(2) > fMaxY)) {
      fMaxY = crRay.cr_vHit(2);
      bFloorHitted = TRUE;
    }
  }
  
  if (bFloorHitted) {
    plPlacement.pl_PositionVector(2) += fMaxY-plPlacement.pl_PositionVector(2) + 0.01f;
  }
  
  SetPlacement(plPlacement);
}

extern CEntity *_penLightUpdating;
extern BOOL _bDontDiscardLinks = FALSE;

// Internal repositioning function
void CEntity::SetPlacement_internal(const CPlacement3D &plNew, const FLOATmatrix3D &mRotation, BOOL bNear)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);
  _pfPhysicsProfile->StartTimer(CPhysicsProfile::PTI_SETPLACEMENT);
  _pfPhysicsProfile->IncrementTimerAveragingCounter(CPhysicsProfile::PTI_SETPLACEMENT);

  // Invalidate eventual cached info for still models
  en_ulFlags &= ~ENF_VALIDSHADINGINFO;

  _pfPhysicsProfile->StartTimer(CPhysicsProfile::PTI_SETPLACEMENT_COORDSUPDATE);
  
  // Remembel old placement of the entity
  CPlacement3D plOld = en_plPlacement;
  
  // Set new placement of the entity
  en_plPlacement = plNew;
  en_mRotation = mRotation;
  _pfPhysicsProfile->StopTimer(CPhysicsProfile::PTI_SETPLACEMENT_COORDSUPDATE);

  // If this is a brush entity
  if (en_RenderType == RT_BRUSH || en_RenderType == RT_FIELDBRUSH) {
    _pfPhysicsProfile->StartTimer(CPhysicsProfile::PTI_SETPLACEMENT_BRUSHUPDATE);
    
    // Recalculate all bounding boxes relative to new position
    _bDontDiscardLinks = TRUE;
    en_pbrBrush->CalculateBoundingBoxes();
    _bDontDiscardLinks = FALSE;

    BOOL bHasShadows = FALSE;
    
    // For all brush mips
    FOREACHINLIST(CBrushMip, bm_lnInBrush, en_pbrBrush->br_lhBrushMips, itbm) 
    {
      // For all sectors in the mip
      FOREACHINDYNAMICARRAY(itbm->bm_abscSectors, CBrushSector, itbsc) 
      {
        // For all polygons in this sector
        FOREACHINSTATICARRAY(itbsc->bsc_abpoPolygons, CBrushPolygon, itbpo) 
        {
          // If the polygon has shadows then discard shadows
          if (!(itbpo->bpo_ulFlags & BPOF_FULLBRIGHT)) {
            itbpo->DiscardShadows();
            bHasShadows = TRUE;
          }
        }
      }
    }
    
    // Find possible shadow layers near affected area
    if (bHasShadows) {
      if (en_ulFlags & ENF_DYNAMICSHADOWS) {
        _penLightUpdating = NULL;
      } else {
        _penLightUpdating = this;
      }
      en_pwoWorld->FindShadowLayers(en_pbrBrush->GetFirstMip()->bm_boxBoundingBox,
        FALSE, FALSE /* no directional */);
      _penLightUpdating = NULL;
    }

    // If it is zoning
    if (en_ulFlags & ENF_ZONING) 
    {
      // FPU must be in 53-bit mode
      CSetFPUPrecision FPUPrecision(FPT_53BIT);

      // For all brush mips
      FOREACHINLIST(CBrushMip, bm_lnInBrush, en_pbrBrush->br_lhBrushMips, itbm) 
      {
        // For all sectors in the mip
        FOREACHINDYNAMICARRAY(itbm->bm_abscSectors, CBrushSector, itbsc) 
        {
          // Find entities in sector
          itbsc->FindEntitiesInSector();
        }
      }
    }
    
    _pfPhysicsProfile->StopTimer(CPhysicsProfile::PTI_SETPLACEMENT_BRUSHUPDATE);
    
  } else if (en_RenderType == RT_TERRAIN) {
    // Update terrain shadow map
    CTerrain *ptrTerrain = GetTerrain();
    ASSERT(ptrTerrain != NULL);
    ptrTerrain->UpdateShadowMap();
  }

  // Set spatial clasification
  _pfPhysicsProfile->StartTimer(CPhysicsProfile::PTI_SETPLACEMENT_SPATIALUPDATE);
  if (bNear) {
    FindSectorsAroundEntityNear();
  } else {
    FindSectorsAroundEntity();
  }
  _pfPhysicsProfile->StopTimer(CPhysicsProfile::PTI_SETPLACEMENT_SPATIALUPDATE);

  _pfPhysicsProfile->StartTimer(CPhysicsProfile::PTI_SETPLACEMENT_LIGHTUPDATE);
  
  // If it is a light source
  {
    CLightSource *pls = GetLightSource();
    if (pls != NULL) {
      // Find all shadow maps that should have layers from this light source
      pls->FindShadowLayers(FALSE);
      
      // Update shadow map on all terrains in world
      pls->UpdateTerrains(plOld,en_plPlacement);
    }
  }

  _pfPhysicsProfile->StopTimer(CPhysicsProfile::PTI_SETPLACEMENT_LIGHTUPDATE);

  // Move the entity to new position in collision grid
  if (en_pciCollisionInfo != NULL) 
  {
    _pfPhysicsProfile->StartTimer(CPhysicsProfile::PTI_SETPLACEMENT_COLLISIONUPDATE);
    FLOATaabbox3D boxNew;
    en_pciCollisionInfo->MakeBoxAtPlacement(en_plPlacement.pl_PositionVector, en_mRotation, boxNew);
    
    if (en_RenderType != RT_BRUSH && en_RenderType != RT_FIELDBRUSH) {
      en_pwoWorld->MoveEntityInCollisionGrid(this, en_pciCollisionInfo->ci_boxCurrent, boxNew);
    }
    
    en_pciCollisionInfo->ci_boxCurrent = boxNew;
    _pfPhysicsProfile->StopTimer(CPhysicsProfile::PTI_SETPLACEMENT_COLLISIONUPDATE);
  }

  _pfPhysicsProfile->StopTimer(CPhysicsProfile::PTI_SETPLACEMENT);

  // NOTE: this is outside profile because it uses recursion

  // For each child of this entity
  FOREACHINLIST(CEntity, n_lnInParent, GetChildren(), itenChild) 
  {
    CPlacement3D plNew = itenChild->en_plRelativeToParent;
    plNew.RelativeToAbsoluteSmooth(en_plPlacement);
    itenChild->SetPlacement(plNew);
  }
}

// Used in rendering - gets lerped placement between ticks
CPlacement3D CEntity::GetLerpedPlacement(void) const
{
  const CEntity *penParent = static_cast<const CEntity *>(n_pParent);

  // If it has no parent then no lerping
  if (penParent == NULL) {
    return en_plPlacement;
    
  // If it has parent then get lerped placement relative to parent
  } else {
    CPlacement3D plParentLerped = penParent->GetLerpedPlacement();
    CPlacement3D plLerped = en_plRelativeToParent;
    plLerped.RelativeToAbsoluteSmooth(plParentLerped);
    return plLerped;
  }
}

// Sets common entity flags.
void CEntity::SetFlags(ULONG ulFlags)
{
  en_ulFlags = ulFlags;
}

// Sets entity physics flags.
void CEntity::SetPhysicsFlags(ULONG ulFlags)
{
  // Remember the new flags
  en_ulPhysicsFlags = ulFlags;

  // Cache eventual collision info
  FindCollisionInfo();
}

// Sets entity collision flags.
void CEntity::SetCollisionFlags(ULONG ulFlags)
{
  // Remember the new flags
  en_ulCollisionFlags = ulFlags;

  // Cache eventual collision info
  FindCollisionInfo();
}

// Parent this entity to another entity.
void CEntity::SetParent(CNode *pNewParent)
{
  CNode::SetParent(pNewParent);

  if (pNewParent == NULL) {
    return;
  }

  // Calculate relative placement
  en_plRelativeToParent = en_plPlacement;
  en_plRelativeToParent.AbsoluteToRelativeSmooth(GetParentEntity()->en_plPlacement);
}


// Find first child of given class
CEntity *CEntity::GetChildOfClass(const char *strClass)
{
  // For each child of this entity
  FOREACHINLIST(CEntity, n_lnInParent, GetChildren(), itenChild) {
    // If it is of given class
    if (IsOfClass(itenChild, strClass)) {
      return itenChild;
    }
  }
  
  // Not found
  return NULL;
}  

// Destroy this entity (entity must not be targetable).
void CEntity::Destroy(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If it is already destroyed then do nothing
  if (en_ulFlags & ENF_DELETED) {
    return;
  }
  
  {
    CLightSource *pls = GetLightSource();
    
    // If it is a light source then destroy all of its shadow layers
    if (pls != NULL) {
      pls->DiscardShadowLayers();
    }
  }
  
  // Clean up the entity
  End();
  SetDefaultProperties(); // This effectively clears all entity pointers!

  // Unlink parent-child links
  if (GetParent() != NULL) {
    SetParent(NULL);
  }
  
  FORDELETELIST(CEntity, n_lnInParent, GetChildren(), itenChild) 
  {
    itenChild->SetParent(NULL);
  }

  // Set its flags to mark that it doesn't not exist anymore
  en_ulFlags |= ENF_DELETED;
  
  // Make sure that no deleted entity can be alive
  en_ulFlags &= ~ENF_ALIVE;
  
  // Remove from all sectors
  en_rdSectors.Clear();
  
  // Remove from active entities in the world
  en_pwoWorld->wo_cenEntities.Remove(this);
  
  // [SSE]
  if (IsPawn()) {
    en_pwoWorld->wo_cenPawnEntities.Remove(this);
  }
  
  // Remove the reference made by the entity itself (this can delete it!)
  RemReference();
}

FLOAT3D _vHandle;
CBrushPolygon *_pbpoNear;
CTerrain *_ptrTerrainNear;
FLOAT _fNearDistance;
FLOAT3D _vNearPoint;

static void CheckPolygonForShadingInfo(CBrushPolygon &bpo)
{
  // If it is not a wall then skip it
  if (bpo.bpo_ulFlags & (BPOF_INVISIBLE | BPOF_PORTAL)) {
    return;
  }
  
  // If the polygon or it's entity are invisible then skip it
  if (bpo.bpo_pbscSector->bsc_pbmBrushMip->bm_pbrBrush->br_penEntity->en_ulFlags & ENF_HIDDEN) {
    return;
  }

  const FLOATplane3D &plPolygon = bpo.bpo_pbplPlane->bpl_plAbsolute;
  
  // Find distance of the polygon plane from the handle
  FLOAT fDistance = plPolygon.PointDistance(_vHandle);
  
  // If it is behind the plane or further than nearest found then skip it
  if (fDistance < 0.0f || fDistance > _fNearDistance) {
    return;
  }
  
  // Find projection of handle to the polygon plane
  FLOAT3D vOnPlane = plPolygon.ProjectPoint(_vHandle);
  
  // If it is not in the bounding box of polygon
  const FLOATaabbox3D &boxPolygon = bpo.bpo_boxBoundingBox;
  const FLOAT EPSILON = 0.01f;
  if (
    (boxPolygon.Min()(1) - EPSILON > vOnPlane(1)) || 
    (boxPolygon.Max()(1) + EPSILON < vOnPlane(1)) || 
    (boxPolygon.Min()(2) - EPSILON > vOnPlane(2)) ||
    (boxPolygon.Max()(2) + EPSILON < vOnPlane(2)) || 
    (boxPolygon.Min()(3) - EPSILON > vOnPlane(3)) || 
    (boxPolygon.Max()(3) + EPSILON < vOnPlane(3))) 
  {
    // skip it
    return;
  }

  // Find major axes of the polygon plane
  INDEX iMajorAxis1, iMajorAxis2;
  GetMajorAxesForPlane(plPolygon, iMajorAxis1, iMajorAxis2);

  // Create an intersector
  CIntersector isIntersector(_vHandle(iMajorAxis1), _vHandle(iMajorAxis2));
  
  // For all edges in the polygon
  FOREACHINSTATICARRAY(bpo.bpo_abpePolygonEdges, CBrushPolygonEdge, itbpePolygonEdge) 
  {
    // Get edge vertices (edge direction is irrelevant here!)
    const FLOAT3D &vVertex0 = itbpePolygonEdge->bpe_pbedEdge->bed_pbvxVertex0->bvx_vAbsolute;
    const FLOAT3D &vVertex1 = itbpePolygonEdge->bpe_pbedEdge->bed_pbvxVertex1->bvx_vAbsolute;
    
    // Pass the edge to the intersector
    isIntersector.AddEdge(
      vVertex0(iMajorAxis1), vVertex0(iMajorAxis2),
      vVertex1(iMajorAxis1), vVertex1(iMajorAxis2));
  }

  // If the point is not inside polygon then skip it
  if (!isIntersector.IsIntersecting()) {
    return;
  }

  // Remember the polygon
  _pbpoNear = &bpo;
  _fNearDistance = fDistance;
  _vNearPoint = vOnPlane;
}

//! Find shading info for some terrain.
static void CheckTerrainForShadingInfo(CTerrain *ptrTerrain)
{
  ASSERT(ptrTerrain != NULL);
  ASSERT(ptrTerrain->GetEntity() != NULL);
  CEntity *pen = ptrTerrain->GetEntity();

  FLOAT3D vTerrainNormal;
  FLOAT3D vHitPoint;
  FLOATplane3D plHitPlane;
  vTerrainNormal = FLOAT3D(0,-1,0) * pen->en_mRotation;
  FLOAT fDistance = TestRayCastHit(ptrTerrain,pen->en_mRotation,pen->en_plPlacement.pl_PositionVector,
                                   _vHandle,_vHandle+vTerrainNormal,_fNearDistance,FALSE,plHitPlane,vHitPoint);
  if (fDistance < _fNearDistance) {
    _vNearPoint = vHitPoint;
    _fNearDistance = fDistance;
    _ptrTerrainNear = ptrTerrain;
  }
}

// Find and remember shading info for this entity if invalid.
void CEntity::FindShadingInfo(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If this entity can't even have shading info then do nothing
  if (en_psiShadingInfo == NULL) {
    return;
  }

  // If info is valid then do nothing
  if (en_ulFlags & ENF_VALIDSHADINGINFO) {
    // !!! check if the polygon is still there !
    return;
  }
  en_ulFlags |= ENF_VALIDSHADINGINFO;

  en_psiShadingInfo->si_penEntity = this;

  // Clear shading info
  en_psiShadingInfo->si_pbpoPolygon = NULL;
  en_psiShadingInfo->si_ptrTerrain  = NULL;
  
  if (en_psiShadingInfo->si_lnInPolygon.IsLinked()) {
    en_psiShadingInfo->si_lnInPolygon.Remove();
  }

  // Take reference point at handle of the model entity
  _vHandle = en_plPlacement.pl_PositionVector;
  
  // Start infinitely far away
  _pbpoNear = NULL;
  _ptrTerrainNear = NULL;
  _fNearDistance = UpperLimit(1.0f);
  
  // If this is movable entity
  if (en_ulPhysicsFlags&EPF_MOVABLE) {
    // For each cached near polygon
    TStaticStackArray<CBrushPolygon*> &apbpo = ((CMovableEntity*)this)->en_apbpoNearPolygons;
    for (INDEX iPolygon = 0; iPolygon < apbpo.Count(); iPolygon++) {
      CheckPolygonForShadingInfo(*apbpo[iPolygon]);
    }
  }

  // For each sector that this entity is in
  {FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
	  // for each brush or terrain in this sector
  {
	  FOREACHDSTOFSRC(pbsc->bsc_rsEntities, CEntity, en_rdSectors, pen)
	  if (pen->en_RenderType == CEntity::RT_TERRAIN) {
		  CheckTerrainForShadingInfo(pen->GetTerrain());
	  }
	  else if (pen->en_RenderType != CEntity::RT_BRUSH && pen->en_RenderType != CEntity::RT_FIELDBRUSH) {
		  break;
	  }
  }}
  ENDFOR} // DON'T TOUCH IT ^_^
	


  // If this is non-movable entity, or no polygon or terrain found so far
  if (_pbpoNear == NULL && _ptrTerrainNear == NULL) {
      // For each sector that this entity is in
      FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
      {
        // For each polygon in the sector
        FOREACHINSTATICARRAY(pbsc->bsc_abpoPolygons, CBrushPolygon, itbpo) 
        {
          CBrushPolygon &bpo = *itbpo;
          CheckPolygonForShadingInfo(bpo);
        }
      }
      ENDFOR
  }

  // If there is some polygon found then remember shading info
  if (_pbpoNear != NULL) {
    en_psiShadingInfo->si_pbpoPolygon = _pbpoNear;
    _pbpoNear->bpo_lhShadingInfos.AddTail(en_psiShadingInfo->si_lnInPolygon);
    en_psiShadingInfo->si_vNearPoint = _vNearPoint;

    CEntity *penWithPolygon = _pbpoNear->bpo_pbscSector->bsc_pbmBrushMip->bm_pbrBrush->br_penEntity;
    ASSERT(penWithPolygon != NULL);
    const FLOATmatrix3D &mPolygonRotation = penWithPolygon->en_mRotation;
    const FLOAT3D &vPolygonTranslation = penWithPolygon->GetPlacement().pl_PositionVector;

    _vNearPoint = (_vNearPoint-vPolygonTranslation)*!mPolygonRotation;

    MEX2D vmexShadow;
    _pbpoNear->bpo_mdShadow.GetTextureCoordinates(
      _pbpoNear->bpo_pbplPlane->bpl_pwplWorking->wpl_mvRelative,
      _vNearPoint, vmexShadow);
      
    CBrushShadowMap &bsm = _pbpoNear->bpo_smShadowMap;
    INDEX iMipLevel = bsm.sm_iFirstMipLevel;
    
    FLOAT fpixU = FLOAT(vmexShadow(1) + bsm.sm_mexOffsetX) * (1.0f / (1 << iMipLevel));
    FLOAT fpixV = FLOAT(vmexShadow(2) + bsm.sm_mexOffsetY) * (1.0f / (1 << iMipLevel));
    en_psiShadingInfo->si_pixShadowU = floor(fpixU);
    en_psiShadingInfo->si_pixShadowV = floor(fpixV);
    en_psiShadingInfo->si_fUDRatio = fpixU-en_psiShadingInfo->si_pixShadowU;
    en_psiShadingInfo->si_fLRRatio = fpixV-en_psiShadingInfo->si_pixShadowV;

  // else if there is some terrain found then remember shading info
  } else if (_ptrTerrainNear != NULL) {
    en_psiShadingInfo->si_ptrTerrain = _ptrTerrainNear;
    en_psiShadingInfo->si_vNearPoint = _vNearPoint;
    
    FLOAT2D vTc = CalculateShadingTexCoords(_ptrTerrainNear,_vNearPoint);
    en_psiShadingInfo->si_pixShadowU = floor(vTc(1));
    en_psiShadingInfo->si_pixShadowV = floor(vTc(2));
    en_psiShadingInfo->si_fLRRatio   = vTc(1) - en_psiShadingInfo->si_pixShadowU;
    en_psiShadingInfo->si_fUDRatio   = vTc(2) - en_psiShadingInfo->si_pixShadowV;

    _ptrTerrainNear->tr_lhShadingInfos.AddTail(en_psiShadingInfo->si_lnInPolygon);
  }
}

// Find first sector that entity is in.
CBrushSector *CEntity::GetFirstSector(void)
{
  {FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
    return pbsc;
  ENDFOR};
  return NULL;
}

CBrushSector *CEntity::GetFirstSectorWithName(void)
{
  CBrushSector *pbscResult = NULL;
  {
    FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
      if (pbsc->bsc_strName != "") {
        pbscResult = pbsc;
        break;
      }
    ENDFOR
  }
  return pbscResult;
}

// Find and remember collision info for this entity.
void CEntity::FindCollisionInfo(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Discard eventual collision info
  DiscardCollisionInfo();

  // If the entity is colliding
  if (en_ulCollisionFlags & ECF_TESTMASK)
  {
    // If it is a model then cache its new collision info
    if ((en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL) && (en_pmoModelObject->GetData() != NULL)) 
    {
      en_pciCollisionInfo = new CCollisionInfo;
      en_pciCollisionInfo->FromModel(this, GetCollisionBoxIndex());
    } else if ((en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) &&(GetModelInstance() != NULL)) {
      // Cache its new collision info
      en_pciCollisionInfo = new CCollisionInfo;
      en_pciCollisionInfo->FromModel(this, GetCollisionBoxIndex());
      
    // If it is a brush
    } else if (en_RenderType == RT_BRUSH) {
      // If it is zoning brush and non movable then do nothing
      if ((en_ulFlags & ENF_ZONING) && !(en_ulPhysicsFlags & EPF_MOVABLE)) {
        return;
      }
      
      // Cache its new collision info
      en_pciCollisionInfo = new CCollisionInfo;
      en_pciCollisionInfo->FromBrush(en_pbrBrush);
      
    // If it is a field brush then cache its new collision info
    } else if (en_RenderType == RT_FIELDBRUSH) {
      en_pciCollisionInfo = new CCollisionInfo;
      en_pciCollisionInfo->FromBrush(en_pbrBrush);
      return;
    } else if (en_RenderType == RT_TERRAIN) {
      return;
    } else {
      return;
    }
    
    // Add entity to collision grid
    FLOATaabbox3D boxNew;
    en_pciCollisionInfo->MakeBoxAtPlacement(en_plPlacement.pl_PositionVector, en_mRotation, boxNew);
    if (en_RenderType != RT_BRUSH && en_RenderType != RT_FIELDBRUSH) {
      en_pwoWorld->AddEntityToCollisionGrid(this, boxNew);
    }
    en_pciCollisionInfo->ci_boxCurrent = boxNew;
  }
}

// Discard collision info for this entity
void CEntity::DiscardCollisionInfo(void)
{
  // If there was any collision info then remove entity from collision grid
  if (en_pciCollisionInfo!=NULL) 
  {
    if (en_RenderType != RT_BRUSH && en_RenderType != RT_FIELDBRUSH) {
      en_pwoWorld->RemoveEntityFromCollisionGrid(this, en_pciCollisionInfo->ci_boxCurrent);
    }
    
    // Free it
    delete en_pciCollisionInfo;
    en_pciCollisionInfo = NULL;
  }
  
  // If movable entity
  if (en_ulPhysicsFlags & EPF_MOVABLE) {
    ((CMovableEntity*)this)->ClearTemporaryData();
  }
}

// Copy collision info from some other entity
void CEntity::CopyCollisionInfo(CEntity &enOrg)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If there is no collision info then do nothing
  if (enOrg.en_pciCollisionInfo == NULL) {
    en_pciCollisionInfo = NULL;
    return;
  }
  
  // Create info and copy it
  en_pciCollisionInfo = new CCollisionInfo(*enOrg.en_pciCollisionInfo);
  
  // Add entity to collision grid
  FLOATaabbox3D boxNew;
  en_pciCollisionInfo->MakeBoxAtPlacement(en_plPlacement.pl_PositionVector, en_mRotation, boxNew);
  
  if (en_RenderType != RT_BRUSH && en_RenderType != RT_FIELDBRUSH) {
    en_pwoWorld->AddEntityToCollisionGrid(this, boxNew);
  }
  
  en_pciCollisionInfo->ci_boxCurrent = boxNew;
}

// Get box and sphere for spatial clasification.
void CEntity::UpdateSpatialRange(void)
{
  CSetFPUPrecision FPUPrecision(FPT_24BIT);

  en_fSpatialClassificationRadius = -1.0f;

  // If zoning then do nothing
  if (en_ulFlags & ENF_ZONING) {
    return;
  }

  FLOATaabbox3D box;
  FLOATaabbox3D boxStretched;
  
  // Get bounding box of the entity
  // Is this old model
  if (en_RenderType == CEntity::RT_MODEL || en_RenderType == CEntity::RT_EDITORMODEL) {
    en_pmoModelObject->GetAllFramesBBox(box);
    box.StretchByVector(en_pmoModelObject->mo_Stretch);
    FLOAT3D fClassificationStretch = GetClassificationBoxStretch();
    boxStretched = box;
    boxStretched .StretchByVector(fClassificationStretch);
    en_boxSpatialClassification = boxStretched;
    
  // Is this ska model
  } else if (en_RenderType == CEntity::RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    GetModelInstance()->GetAllFramesBBox(box);
    box.StretchByVector(GetModelInstance()->mi_vStretch);
    FLOAT3D fClassificationStretch = GetClassificationBoxStretch();
    boxStretched = box;
    boxStretched.StretchByVector(fClassificationStretch);
    en_boxSpatialClassification = boxStretched;
    
  // Is this brush
  } else if (en_RenderType == CEntity::RT_BRUSH || en_RenderType == RT_FIELDBRUSH) {
    box = en_pbrBrush->GetFirstMip()->bm_boxRelative;
    boxStretched = box;
    en_boxSpatialClassification = box;
  // Is this terrain
  } else if (en_RenderType == CEntity::RT_TERRAIN) {
    GetTerrain()->GetAllTerrainBBox(box);
    boxStretched = box;
    en_boxSpatialClassification = box;
  } else {
    return; // Sound entities are not related to sectors !!!!
  }
  en_fSpatialClassificationRadius = Max(box.Min().Length(), box.Max().Length() );
  ASSERT(IsValidFloat(en_fSpatialClassificationRadius));
}

// Find and remember all sectors that this entity is in.
void CEntity::FindSectorsAroundEntity(void)
{
  CSetFPUPrecision sfp(FPT_53BIT);

  // If not in spatial clasification then do nothing
  if (en_fSpatialClassificationRadius < 0) {
    return;
  }
  
  // Get bounding sphere and box of entity
  FLOAT fSphereRadius = en_fSpatialClassificationRadius;
  const FLOAT3D &vSphereCenter = en_plPlacement.pl_PositionVector;
  
  // Make oriented bounding box of the entity
  FLOATobbox3D boxEntity = FLOATobbox3D(en_boxSpatialClassification, 
    en_plPlacement.pl_PositionVector, en_mRotation);
  DOUBLEobbox3D boxdEntity = FLOATtoDOUBLE(boxEntity);

  // Unset spatial clasification
  en_rdSectors.Clear();

  // For each brush in the world
  FOREACHINDYNAMICARRAY(en_pwoWorld->wo_baBrushes.ba_abrBrushes, CBrush3D, itbr) 
  {
    CBrush3D &br = *itbr;
    
    // If the brush entity is not zoning then skip it
    if (itbr->br_penEntity == NULL || !(itbr->br_penEntity->en_ulFlags & ENF_ZONING)) {
      continue;
    }
    
    // For each mip in the brush
    FOREACHINLIST(CBrushMip, bm_lnInBrush, itbr->br_lhBrushMips, itbm)
    {
      // For each sector in the brush mip
      FOREACHINDYNAMICARRAY(itbm->bm_abscSectors, CBrushSector, itbsc)
      {
        // If the sector's bounding box has contact with the sphere 
        if (itbsc->bsc_boxBoundingBox.TouchesSphere(vSphereCenter, fSphereRadius)
            // and with the box
            && boxEntity.HasContactWith(FLOATobbox3D(itbsc->bsc_boxBoundingBox))) 
        {
          // If the sphere is inside the sector
          if (itbsc->bsc_bspBSPTree.TestSphere(FLOATtoDOUBLE(vSphereCenter), FLOATtoDOUBLE(fSphereRadius)) >= 0) 
          {
            // If the box is inside the sector
            if (itbsc->bsc_bspBSPTree.TestBox(boxdEntity) >= 0) 
            {
              // Relate the entity to the sector
              if (en_RenderType == RT_BRUSH
                  || en_RenderType == RT_FIELDBRUSH
                  || en_RenderType == RT_TERRAIN) // brushes first
              {  
                AddRelationPairHeadHead(itbsc->bsc_rsEntities, en_rdSectors);
              } else {
                AddRelationPairTailTail(itbsc->bsc_rsEntities, en_rdSectors);
              }
            }
          }
        }
      }
    }
  }
}

// Find and remember all sectors that this entity is in.
void CEntity::FindSectorsAroundEntityNear(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If not in spatial clasification do nothing
  if (en_fSpatialClassificationRadius < 0) {
    return;
  }
  
  // This may be called only for movable entities
  ASSERT(en_ulPhysicsFlags & EPF_MOVABLE);
  CMovableEntity *pen = (CMovableEntity *)this;

  // Get bounding sphere and box of entity
  FLOAT fSphereRadius = en_fSpatialClassificationRadius;
  const FLOAT3D &vSphereCenter = en_plPlacement.pl_PositionVector;
  FLOATaabbox3D boxEntity(vSphereCenter, fSphereRadius);
  
  // Make oriented bounding box of the entity
  FLOATobbox3D oboxEntity = FLOATobbox3D(en_boxSpatialClassification, 
    en_plPlacement.pl_PositionVector, en_mRotation);
  DOUBLEobbox3D oboxdEntity = FLOATtoDOUBLE(oboxEntity);

  CListHead lhActive;
  
  // For each sector around this entity
  {FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
    // Remember its link
    pbsc->bsc_prlLink = pbsc_iter;
    
    // Add it to list of active sectors
    lhActive.AddTail(pbsc->bsc_lnInActiveSectors);
  ENDFOR}

  TStaticStackArray<CBrushPolygon*> &apbpo = pen->en_apbpoNearPolygons;
  
  // For each cached polygon
  for (INDEX iPolygon = 0; iPolygon < apbpo.Count(); iPolygon++) {
    CBrushSector *pbsc = apbpo[iPolygon]->bpo_pbscSector;
    
    // Add its sector if not already added, and has BSP (is zoning)
    if (!pbsc->bsc_lnInActiveSectors.IsLinked() && pbsc->bsc_bspBSPTree.bt_pbnRoot != NULL) {
      lhActive.AddTail(pbsc->bsc_lnInActiveSectors);
      pbsc->bsc_prlLink = NULL;
    }
  }

  // For each active sector
  FOREACHINLIST(CBrushSector, bsc_lnInActiveSectors, lhActive, itbsc) 
  {
    CBrushSector *pbsc = itbsc;
    
    // Test if entity is in sector
    BOOL bIn =
        // The sector's bounding box has contact with given bounding box,
        pbsc->bsc_boxBoundingBox.HasContactWith(boxEntity) && 
        // The sphere is inside the sector
        pbsc->bsc_bspBSPTree.TestSphere(FLOATtoDOUBLE(vSphereCenter), FLOATtoDOUBLE(fSphereRadius)) >= 0 && 
        // (use more detailed testing for moving brushes)
        (en_RenderType != RT_BRUSH 
          || 
          oboxEntity.HasContactWith(FLOATobbox3D(pbsc->bsc_boxBoundingBox)) && // oriented box touches box of sector
          (pbsc->bsc_bspBSPTree.TestBox(oboxdEntity) >= 0)); // oriented box is in bsp
    
    // If it is not
    if (!bIn) {
      // If it has link
      if (pbsc->bsc_prlLink != NULL) {
        // Remove link to that sector
        delete pbsc->bsc_prlLink;
        pbsc->bsc_prlLink = NULL;
      }
    // If it is
    } else {
      // If it doesn't have link then add the link
      if (pbsc->bsc_prlLink == NULL) {
        if (en_RenderType == RT_BRUSH ||
            en_RenderType == RT_FIELDBRUSH ||
            en_RenderType == RT_TERRAIN) {  // brushes first
          AddRelationPairHeadHead(pbsc->bsc_rsEntities, en_rdSectors);
        } else {
          AddRelationPairTailTail(pbsc->bsc_rsEntities, en_rdSectors);
        }
      }
    }
  }

  // Clear list of active sectors
  FORDELETELIST(CBrushSector, bsc_lnInActiveSectors, lhActive, itbsc) 
  {
    itbsc->bsc_prlLink = NULL;
    itbsc->bsc_lnInActiveSectors.Remove();
  }
  ASSERT(lhActive.IsEmpty());

  // If there is no link found then test with brute force algorithm
  if (en_rdSectors.IsEmpty()) {
    FindSectorsAroundEntity();
  }
}

// Uncache shadows of each polygon that has given gradient index
void CEntity::UncacheShadowsForGradient(INDEX iGradient)
{
  if (en_RenderType != CEntity::RT_BRUSH)
  {
    ASSERTALWAYS("Uncache shadows for gradient called on non-brush entity!");
    return;
  }

  // For all brush mips
  FOREACHINLIST(CBrushMip, bm_lnInBrush, en_pbrBrush->br_lhBrushMips, itbm)
  {
    // For all sectors in the mip
    FOREACHINDYNAMICARRAY(itbm->bm_abscSectors, CBrushSector, itbsc)
    {
      // For all polygons in this sector
      FOREACHINSTATICARRAY(itbsc->bsc_abpoPolygons, CBrushPolygon, itbpo)
      {
        // If the polygon has shadows then uncache shadows
        if (itbpo->bpo_bppProperties.bpp_ubGradientType == iGradient)
        {
          itbpo->bpo_smShadowMap.Uncache();
        }
      }
    }
  }
}

// Get state transition for given state and event code.
CEntity::pEventHandler CEntity::HandlerForStateAndEvent(SLONG slState, SLONG slEvent)
{
  // find translation in the translation table of the DLL class
  return en_pecClass->HandlerForStateAndEvent(slState, slEvent);
}

// Handle an event - return false if event was not handled.
BOOL CEntity::HandleEvent(const CEntityEvent &ee)
{
  /*
   By default, base entities ignore all events.
   Events are handled by classes derived from CRationalEntity using state stack.

   Anyway, it is possible to override this in some class if some other way
   of event handling is desired.
   */

  return FALSE;
}

/////////////////////////////////////////////////////////////////////
// Event posting system

// TODO: Separate to another file
class CSentEvent 
{
  public:
    CEntityPointer se_penEntity;
    CEntityEvent *se_peeEvent;
    inline void Clear(void) 
    { 
      se_penEntity = NULL; 
    }
};

static TStaticStackArray<CSentEvent> _aseSentEvents;  // delayed events

// Send an event to this entity.
void CEntity::SendEvent(const CEntityEvent &ee)
{
  if (this == NULL) {
    ASSERT(FALSE);
    return;
  }
  CSentEvent &se = _aseSentEvents.Push();
  se.se_penEntity = this;
  se.se_peeEvent = ((CEntityEvent&)ee).MakeCopy();  // discard const qualifier
}

// Find entities in a box (box must be around this entity)
void CEntity::FindEntitiesInRange(const FLOATaabbox3D &boxRange, TDynamicContainer<CEntity> &cen, BOOL bCollidingOnly)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // For each entity in the world of this entity
  FOREACHINDYNAMICCONTAINER(en_pwoWorld->wo_cenEntities, CEntity, iten) 
  {
    // If it is zoning brush entity
    if (iten->en_RenderType == CEntity::RT_BRUSH && (iten->en_ulFlags & ENF_ZONING)) 
    {
      // Get first mip in its brush
      CBrushMip *pbm = iten->en_pbrBrush->GetFirstMip();
      
      // If the mip doesn't touch the box then skip it
      if (!pbm->bm_boxBoundingBox.HasContactWith(boxRange)) {
        continue;
      }

      // For all sectors in this mip
      FOREACHINDYNAMICARRAY(pbm->bm_abscSectors, CBrushSector, itbsc)
      {
        // If the sector doesn't touch the box  then skip it
        if (!itbsc->bsc_boxBoundingBox.HasContactWith(boxRange)) {
          continue;
        }

        // For all entities in the sector
        {FOREACHDSTOFSRC(itbsc->bsc_rsEntities, CEntity, en_rdSectors, pen)
          // if the model entity touches the box
          if ((pen->en_RenderType == RT_MODEL || pen->en_RenderType == RT_EDITORMODEL)
              && boxRange.HasContactWith(
                FLOATaabbox3D(pen->GetPlacement().pl_PositionVector, pen->en_fSpatialClassificationRadius)
              )) 
          {
            // If it has collision box
            if (pen->en_pciCollisionInfo != NULL) {
              // For each sphere
              FOREACHINSTATICARRAY(pen->en_pciCollisionInfo->ci_absSpheres, CMovingSphere, itms) 
              {
                // Project it
                itms->ms_vRelativeCenter0 = itms->ms_vCenter*pen->en_mRotation+pen->en_plPlacement.pl_PositionVector;
                
                // If the sphere touches the range then add it to container
                if (boxRange.HasContactWith(FLOATaabbox3D(itms->ms_vRelativeCenter0, itms->ms_fR))) {
                  if (!cen.IsMember(pen)) {
                    cen.Add(pen);
                  }
                  goto next_entity;
                }
              }
              
            // If no collision box, but non-colliding are allowed add it to container
            } else if (!bCollidingOnly) {
              if (!cen.IsMember(pen)) {
                cen.Add(pen);
              }
            }
            
          // If the brush entity touches the box
          } else if (pen->en_RenderType == RT_BRUSH 
                      && boxRange.HasContactWith(
                        FLOATaabbox3D(pen->GetPlacement().pl_PositionVector, pen->en_fSpatialClassificationRadius)
                      )
                     ) 
          {
            // If the brush touches the box then add it to container
            if (boxRange.HasContactWith(pen->en_pbrBrush->GetFirstMip()->bm_boxBoundingBox)) {
              if (!cen.IsMember(pen)) {
                cen.Add(pen);
              }
            }
          } else if ((pen->en_RenderType == RT_SKAMODEL || pen->en_RenderType == RT_SKAEDITORMODEL)
                     && boxRange.HasContactWith(
                      FLOATaabbox3D(pen->GetPlacement().pl_PositionVector, pen->en_fSpatialClassificationRadius)
                     )) 
          {
            // If it has collision box
            if (pen->en_pciCollisionInfo != NULL) {
              // For each sphere
              FOREACHINSTATICARRAY(pen->en_pciCollisionInfo->ci_absSpheres, CMovingSphere, itms) 
              {
                // Project it
                itms->ms_vRelativeCenter0 = itms->ms_vCenter * pen->en_mRotation 
                                              + pen->en_plPlacement.pl_PositionVector;
                
                // If the sphere touches the range then add it to container
                if (boxRange.HasContactWith(FLOATaabbox3D(itms->ms_vRelativeCenter0, itms->ms_fR))) {
                  if (!cen.IsMember(pen)) {
                    cen.Add(pen);
                  }
                  goto next_entity;
                }
              }
              
            // If no collision box, but non-colliding are allowed then add it to container
            } else if (!bCollidingOnly) {
              if (!cen.IsMember(pen)) {
                cen.Add(pen);
              }
            }
          }
          next_entity:;
        ENDFOR}
      }
    }
  }
}

// Send an event to all entities in a box (box must be around this entity).
void CEntity::SendEventInRange(const CEntityEvent &ee, const FLOATaabbox3D &boxRange)
{
  // Find entities in the range
  TDynamicContainer<CEntity> cenToReceive;
  FindEntitiesInRange(boxRange, cenToReceive, FALSE);

  // For each entity in container
  FOREACHINDYNAMICCONTAINER(cenToReceive, CEntity, iten) 
  {
    // Send the event to it
    iten->SendEvent(ee);
  }
}

// Handle all sent events.
void CEntity::HandleSentEvents(void)
{
  CSetFPUPrecision FPUPrecision(FPT_24BIT);

  // While there are any unhandled events
  INDEX iFirstEvent = 0;
  while (iFirstEvent < _aseSentEvents.Count()) 
  {
    CSentEvent &se = _aseSentEvents[iFirstEvent];
    
    // If not allowed to execute now then skip it
    if (!se.se_penEntity->IsAllowedForPrediction()) {
      iFirstEvent++;
      continue;
    }
    
    // If the entity is not destroyed then handle the current event
    if (!(se.se_penEntity->en_ulFlags&ENF_DELETED)) {
      se.se_penEntity->HandleEvent(*se.se_peeEvent);
    }
    
    // Go to next event
    iFirstEvent++;
  }

  // For each event
  for (INDEX iee = 0; iee < _aseSentEvents.Count(); iee++) 
  {
    CSentEvent &se = _aseSentEvents[iee];
    
    // Release the entity and destroy the event
    se.se_penEntity = NULL;
    delete se.se_peeEvent;
    se.se_peeEvent = NULL;
  }

  // flush all events
  _aseSentEvents.PopAll();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DLL class interface

// Initialize for being virtual entity that is not rendered.
void CEntity::InitAsVoid(void)
{
  en_RenderType = RT_VOID;
  en_pbrBrush = NULL;
}
// Initialize for beeing a model object.
void CEntity::InitAsModel(void)
{
  // Set render type to model
  en_RenderType = RT_MODEL;
  
  // Create a model object
  en_pmoModelObject = new CModelObject;
  en_psiShadingInfo = new CShadingInfo;
  en_ulFlags &= ~ENF_VALIDSHADINGINFO;
}

// Initialize for beeing a ska model object.
void CEntity::InitAsSkaModel(void)
{
  en_RenderType = RT_SKAMODEL;
  en_psiShadingInfo = new CShadingInfo;
  en_ulFlags &= ~ENF_VALIDSHADINGINFO;
}

// Initialize for beeing a terrain object.
void CEntity::InitAsTerrain(void)
{
  en_RenderType = RT_TERRAIN;
  
  // If there is no existing terrain
  if (en_ptrTerrain == NULL) {
    // Create a new empty terrain in the brush archive of current world
    en_ptrTerrain = en_pwoWorld->wo_taTerrains.ta_atrTerrains.New();
    en_ptrTerrain->tr_penEntity = this;

    // Create empty terrain
    en_ptrTerrain->CreateEmptyTerrain_t(257, 257);
    en_ptrTerrain->SetTerrainSize(FLOAT3D(256, 50, 256));
    en_ptrTerrain->SetShadowMapsSize(0,0);
    en_ptrTerrain->UpdateShadowMap();
  }
  UpdateSpatialRange();
}

// Initialize for beeing a model object, for editor only.
void CEntity::InitAsEditorModel(void)
{
  // Set render type to model
  en_RenderType = RT_EDITORMODEL;
  
  // Create a model object
  en_pmoModelObject = new CModelObject;
  en_psiShadingInfo = new CShadingInfo;
  en_ulFlags &= ~ENF_VALIDSHADINGINFO;
}

// Initialize for beeing a ska model object, for editor only.
void CEntity::InitAsSkaEditorModel(void)
{
  // Set render type to model
  en_RenderType = RT_SKAEDITORMODEL;
  // Create a model object
  en_psiShadingInfo = new CShadingInfo;
  en_ulFlags &= ~ENF_VALIDSHADINGINFO;
}

// Initialize for beeing a brush object.
void CEntity::InitAsBrush(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Set render type to brush
  en_RenderType = RT_BRUSH;
  
  // If there is no existing brush
  if (en_pbrBrush == NULL) {
    // Create a new empty brush in the brush archive of current world
    en_pbrBrush = en_pwoWorld->wo_baBrushes.ba_abrBrushes.New();
    en_pbrBrush->br_penEntity = this;
    
    // Create a brush mip for it
    CBrushMip *pbmMip = new CBrushMip;
    
    // Add it to list
    en_pbrBrush->br_lhBrushMips.AddTail(pbmMip->bm_lnInBrush);
    
    // Set back-pointer to the brush
    pbmMip->bm_pbrBrush = en_pbrBrush;
    en_pbrBrush->CalculateBoundingBoxes();
  }
  UpdateSpatialRange();
}

// Initialize for beeing a field brush object.
void CEntity::InitAsFieldBrush(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Set render type to brush
  en_RenderType = RT_FIELDBRUSH;
  
  // If there is no existing brush
  if (en_pbrBrush == NULL) {
    // Create a new empty brush in the brush archive of current world
    en_pbrBrush = en_pwoWorld->wo_baBrushes.ba_abrBrushes.New();
    en_pbrBrush->br_penEntity = this;
    
    // Create a brush mip for it
    CBrushMip *pbmMip = new CBrushMip;
    
    // Add it to list
    en_pbrBrush->br_lhBrushMips.AddTail(pbmMip->bm_lnInBrush);
    
    // Set back-pointer to the brush
    pbmMip->bm_pbrBrush = en_pbrBrush;
    en_pbrBrush->CalculateBoundingBoxes();
  }
  UpdateSpatialRange();
}

// Switch to model.
void CEntity::SwitchToModel(void)
{
  // Change to editor model
  if (en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL) {
    en_RenderType = RT_MODEL;
  } else if (en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    en_RenderType = RT_SKAMODEL;
  } else {
    // It must be model (not brush)
    ASSERT(FALSE);
  }
}

// Switch to editor model.
void CEntity::SwitchToEditorModel(void)
{
  // Change to editor model
  if (en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL) {
    en_RenderType = RT_EDITORMODEL;
  } else if (en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    en_RenderType = RT_SKAEDITORMODEL;
  } else {
    // It must be model (not brush)
    ASSERT(FALSE);
  }
}

/////////////////////////////////////////////////////////////////////
// Model manipulation

// Set the model data for model entity.
void CEntity::SetModel(const CTFileName &fnmModel)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  // Try to load the new model data
  try {
    en_pmoModelObject->SetData_t(fnmModel);
  } catch(char *strError) {
    (void)strError;
    DECLARE_CTFILENAME(fnmDefault, "Models\\Editor\\Axis.mdl");
    
    // Try to load the default model data
    try {
      en_pmoModelObject->SetData_t(fnmDefault);
    } catch(char *strErrorDefault) {
      FatalError(TRANS("Cannot load default model '%s':\n%s"), (CTString&)fnmDefault, strErrorDefault);
    }
  }
  UpdateSpatialRange();
  FindCollisionInfo();
}

// Set the model data for model entity.
void CEntity::SetModel(SLONG idModelResource)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  CEntityResource *pecModel = en_pecClass->ResourceForId(idModelResource, CResource::TYPE_MODEL);
  en_pmoModelObject->SetData(pecModel->er_pmdModel);
  UpdateSpatialRange();
  FindCollisionInfo();
}

// Set collision info for a SKA model entity.
void CEntity::SetSkaCollisionInfo()
{
  ASSERT(en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL);
  // If there is no collision boxes for ska model then add one default collision box
  if (en_pmiModelInstance->mi_cbAABox.Count() == 0) {
    en_pmiModelInstance->AddCollisionBox("Default", FLOAT3D(-0.5, 0, -0.5), FLOAT3D(0.5, 2, 0.5));
  }
  UpdateSpatialRange();
  FindCollisionInfo();
}

extern void ObtainModelInstance_t(CModelInstance &mi, const CTString &fnSmcFile);

void CEntity::SetSkaModel_t(const CTString &fnmModel)
{
	ASSERT(en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL);

	try {
		// if no model instance exists for entity
		if(en_pmiModelInstance == NULL) {
			en_pmiModelInstance = CreateModelInstance("");
		}

		ASSERT(en_pmiModelInstance != NULL);
		ObtainModelInstance_t(*en_pmiModelInstance, fnmModel); // Obtain copy of requested model instance
	} catch (char *strErrorDefault) {
		throw(strErrorDefault);
	}

	SetSkaCollisionInfo();
}

// Set the model data for a SKA model entity from file.
BOOL CEntity::SetSkaModel(const CTString &fnmModel)
{
	ASSERT(en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL);

	// try to
	try {
		SetSkaModel_t(fnmModel);

  // if failed
	} catch(char *strError) {
    (void)strError;
		WarningMessage("%s\n\rLoading default model.\n", strError);
		DECLARE_CTFILENAME(fnmDefault, "Models\\Editor\\Ska\\Axis.smc");

    // Try to load the default model data
		try {
			SetSkaModel_t(fnmDefault);
		// if failed
		} catch(char *strErrorDefault) {
			FatalError(TRANS("Cannot load default model '%s':\n%s"), (CTString&)fnmDefault, strErrorDefault);
		}

		// set collision info for default model
		SetSkaCollisionInfo();
		return FALSE;
	}

  return TRUE;
}

void CEntity::SetAnyModel(CAbstractModelInstance &mi, SLONG idModelConfiguration)
{
  CEntityResource *perConfig = ResourceForId(idModelConfiguration, CResource::TYPE_MODELCONFIGURATION);
  ASSERT(perConfig != NULL);

  CAbstractModelInstance *pTemplateInstance = perConfig->er_pModelConfiguration->GetAbstractInstance();
  ASSERT(pTemplateInstance != NULL);
  ASSERT(mi.GetInstanceType() == pTemplateInstance->GetInstanceType());
  mi.Copy(*pTemplateInstance);
}

void CEntity::SetAnyModel(SLONG idModelConfiguration)
{
  const BOOL bVertexModel = en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL;
  const BOOL bSkeletalModel = en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL;

  ASSERT(bVertexModel || bSkeletalModel);

  if (bVertexModel) {
    SetAnyModel(*en_pmoModelObject, idModelConfiguration);
    UpdateSpatialRange();
    FindCollisionInfo();
  } else {
    // if entity does not have model instance
    if(en_pmiModelInstance == NULL) {
      en_pmiModelInstance = CreateModelInstance(""); // create new model instance
    }

    SetAnyModel(*en_pmiModelInstance, idModelConfiguration);

    SetSkaCollisionInfo();
    UpdateSpatialRange();
    FindCollisionInfo();
  }
}

// Set model main blend color
void CEntity::SetModelColor(const COLOR colBlend)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  en_pmoModelObject->mo_colBlendColor = colBlend;
}

// Get model main blend color
COLOR CEntity::GetModelColor(void) const
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  return en_pmoModelObject->mo_colBlendColor;
}

// Get the model data for model entity.
const CTFileName &CEntity::GetModel(void)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  return ((CAnimData*)en_pmoModelObject->GetData())->GetName();
}

// Start new animation for model entity.
void CEntity::StartModelAnim(INDEX iNewModelAnim, ULONG ulFlags)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  en_pmoModelObject->PlayAnim(iNewModelAnim, ulFlags);
}

// Set the main texture data for model entity.
void CEntity::SetModelMainTexture(const CTFileName &fnmTexture)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  
  // Try to load the texture data
  try {
    en_pmoModelObject->mo_toTexture.SetData_t(fnmTexture);
  } catch(char *strError) {
    (void)strError;
    DECLARE_CTFILENAME(fnmDefault, "Textures\\Editor\\Default.tex");
    
    // Try to load the default model data
    try {
      en_pmoModelObject->mo_toTexture.SetData_t(fnmDefault);
    } catch(char *strErrorDefault) {
      FatalError(TRANS("Cannot load default texture '%s':\n%s"), (CTString&)fnmDefault, strErrorDefault);
    }
  }
}

// Set the main texture data for model entity.
void CEntity::SetModelMainTexture(SLONG idTextureResource)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  CEntityResource *pecTexture = en_pecClass->ResourceForId(idTextureResource, CResource::TYPE_TEXTURE);
  en_pmoModelObject->mo_toTexture.SetData(pecTexture->er_ptdTexture);
}

// Get the main texture data for model entity.
const CTFileName &CEntity::GetModelMainTexture(void)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  return en_pmoModelObject->mo_toTexture.GetData()->GetName();
}

// Start new animation for main texture of model entity.
void CEntity::StartModelMainTextureAnim(INDEX iNewTextureAnim)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  en_pmoModelObject->mo_toTexture.StartAnim(iNewTextureAnim);
}

// Set the reflection texture data for model entity.
void CEntity::SetModelReflectionTexture(SLONG idTextureResource)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  CEntityResource *pecTexture = en_pecClass->ResourceForId(idTextureResource, CResource::TYPE_TEXTURE);
  en_pmoModelObject->mo_toReflection.SetData(pecTexture->er_ptdTexture);
}

// Set the specular texture data for model entity.
void CEntity::SetModelSpecularTexture(SLONG idTextureResource)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  CEntityResource *pecTexture = en_pecClass->ResourceForId(idTextureResource, CResource::TYPE_TEXTURE);
  en_pmoModelObject->mo_toSpecular.SetData(pecTexture->er_ptdTexture);
}

// Add attachment to model
void CEntity::AddAttachment(INDEX iAttachment, ULONG ulIDModel, ULONG ulIDTexture)
{
  // Add attachment
  CModelObject &mo = en_pmoModelObject->AddAttachmentModel(iAttachment)->amo_moModelObject;
  // Update model data
  CEntityResource *pecWeaponModel = ResourceForId(ulIDModel, CResource::TYPE_MODEL);
  mo.SetData(pecWeaponModel->er_pmdModel);
  
  // Update texture data if different
  CEntityResource *pecWeaponTexture = ResourceForId(ulIDTexture, CResource::TYPE_TEXTURE);
  mo.SetTextureData(pecWeaponTexture->er_ptdTexture);
}

// Add attachment to model.
void CEntity::AddAttachment(INDEX iAttachment, CTFileName fnModel, CTFileName fnTexture)
{
  if (fnModel == CTString("")) return;
  
  CModelObject *pmo = GetModelObject();
  ASSERT(pmo != NULL);
  
  if (pmo == NULL) return;

  CAttachmentModelObject *pamo = pmo->AddAttachmentModel(iAttachment);
  try {
    pamo->amo_moModelObject.SetData_t(fnModel);
  } catch(char *strError) {
    (void) strError;
    pmo->RemoveAttachmentModel(iAttachment);
    return;
  }

  try {
    pamo->amo_moModelObject.mo_toTexture.SetData_t(fnTexture);
  } catch(char *strError) {
    (void) strError;
  }
}

// Set the reflection texture data for attachment model entity.
void CEntity::SetModelAttachmentReflectionTexture(INDEX iAttachment, SLONG idTextureResource)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  CModelObject &mo = en_pmoModelObject->GetAttachmentModel(iAttachment)->amo_moModelObject;
  CEntityResource *pecTexture = en_pecClass->ResourceForId(idTextureResource, CResource::TYPE_TEXTURE);
  mo.mo_toReflection.SetData(pecTexture->er_ptdTexture);
}

// Set the specular texture data for attachment model entity.
void CEntity::SetModelAttachmentSpecularTexture(INDEX iAttachment, SLONG idTextureResource)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL);
  CModelObject &mo = en_pmoModelObject->GetAttachmentModel(iAttachment)->amo_moModelObject;
  CEntityResource *pecTexture = en_pecClass->ResourceForId(idTextureResource, CResource::TYPE_TEXTURE);
  mo.mo_toSpecular.SetData(pecTexture->er_ptdTexture);
}

// Get all vertices of model entity in absolute space
void CEntity::GetModelVerticesAbsolute(TStaticStackArray<FLOAT3D> &avVertices, FLOAT fNormalOffset, FLOAT fMipFactor)
{
  ASSERT(en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL ||
         en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL);
  
  // Get placement
  CPlacement3D plPlacement = GetLerpedPlacement();
  
  // Calculate rotation matrix
  FLOATmatrix3D mRotation;
  MakeRotationMatrixFast(mRotation, plPlacement.pl_OrientationAngle);
  if (en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL) 
  {
    en_pmoModelObject->GetModelVertices(avVertices, mRotation, plPlacement.pl_PositionVector, 
                                        fNormalOffset, fMipFactor);
  } else {
    GetModelInstance()->GetModelVertices(avVertices, mRotation, plPlacement.pl_PositionVector,
                                         fNormalOffset, fMipFactor);
  }
}

//! Adjust mesh's bone positions for a specific entity.
void EntityAdjustBonesCallback(void *pData)
{
  ((CEntity*)pData)->AdjustBones();
}

//! Adjust certain shader parameters for a specific entity.
void EntityAdjustShaderParamsCallback(void *pData,INDEX iSurfaceID,CShaderClass *pShader,ShaderParams &spParams)
{
  ((CEntity*)pData)->AdjustShaderParams(iSurfaceID, pShader, spParams);
}

// Returns true if bone exists and sets two given vectors as start and end point of specified bone
BOOL CEntity::GetBoneRelPosition(INDEX iBoneID, FLOAT3D &vStartPoint, FLOAT3D &vEndPoint)
{
  ASSERT(en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL);
  RM_SetObjectPlacement(CPlacement3D(FLOAT3D(0.0f, 0.0f, 0.0f), ANGLE3D(0.0f, 0.0f, 0.0f)));
  RM_SetBoneAdjustCallback(&EntityAdjustBonesCallback, this);
  return RM_GetBoneAbsPosition(*GetModelInstance(), iBoneID, vStartPoint, vEndPoint);
}

// Returns true if bone exists and sets two given vectors as start and end point of specified bone
BOOL CEntity::GetBoneAbsPosition(INDEX iBoneID, FLOAT3D &vStartPoint, FLOAT3D &vEndPoint)
{
  ASSERT(en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL);
  RM_SetObjectPlacement(GetLerpedPlacement());
  RM_SetBoneAdjustCallback(&EntityAdjustBonesCallback, this);
  return RM_GetBoneAbsPosition(*GetModelInstance(), iBoneID, vStartPoint, vEndPoint);
}

// Callback function for aditional bone adjustment
void CEntity::AdjustBones()
{
}

// Callback function for aditional shader params adjustment
void CEntity::AdjustShaderParams(INDEX iSurfaceID,CShaderClass *pShader,ShaderParams &spParams)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Precache given resource

// Precache 'model' resource.
void CEntity::PrecacheModel(SLONG slID)
{
  en_pecClass->ec_pdecDLLClass->PrecacheModel(slID);
}

// Precache 'modelcfg' resource.
void CEntity::PrecacheModelConfig(SLONG slID)
{
  en_pecClass->ec_pdecDLLClass->PrecacheModelConfig(slID);
}

// Precache 'texture' resource.
void CEntity::PrecacheTexture(SLONG slID)
{
  en_pecClass->ec_pdecDLLClass->PrecacheTexture(slID);
}

// Precache 'sound' resource.
void CEntity::PrecacheSound(SLONG slID)
{
  en_pecClass->ec_pdecDLLClass->PrecacheSound(slID);
}

// Precache 'class' resource.
void CEntity::PrecacheClass(SLONG slID, INDEX iUser /* = -1*/)
{
  en_pecClass->ec_pdecDLLClass->PrecacheClass(slID, iUser);
}

// Get a filename for a resource of given id.
const CTFileName &CEntity::FileNameForResource(SLONG slType, SLONG slID)
{
  // Find the resource
  CEntityResource *per = en_pecClass->ResourceForId(slID, (CResource::Type)slType);
  
  // The resource must exist
  ASSERT(per != NULL);
  
  // Get its name
  return per->GetFileName();
}

// Get data for a texture resource
CTextureData *CEntity::GetTextureDataForResource(SLONG slID)
{
  CEntityResource *pec = ResourceForId(slID, CResource::TYPE_TEXTURE);
  if (pec != NULL) {
    return pec->er_ptdTexture;
  } else {
    return NULL;
  }
}

// Get data for a model resource
CModelData *CEntity::GetModelDataForResource(SLONG slID)
{
  CEntityResource *pec = ResourceForId(slID, CResource::TYPE_MODEL);
  if (pec != NULL) {
    return pec->er_pmdModel;
  } else {
    return NULL;
  }
}

// Remove attachment from model
void CEntity::RemoveAttachment(INDEX iAttachment)
{
  // Remove attachment
  en_pmoModelObject->RemoveAttachmentModel(iAttachment);
}

// Initialize last positions structure for particles.
CLastPositions *CEntity::GetLastPositions(INDEX ctPositions)
{
  TIME tmNow = _pTimer->CurrentTick();
  if (en_plpLastPositions == NULL) {
    en_plpLastPositions = new CLastPositions;
    en_plpLastPositions->lp_avPositions.New(ctPositions);
    en_plpLastPositions->lp_ctUsed = 0;
    en_plpLastPositions->lp_iLast = 0;
    en_plpLastPositions->lp_tmLastAdded = tmNow;
    const FLOAT3D &vNow = GetPlacement().pl_PositionVector;
    for (INDEX iPos = 0; iPos<ctPositions; iPos++) 
    {
      en_plpLastPositions->lp_avPositions[iPos] = vNow;
    }
  }

  while (en_plpLastPositions->lp_tmLastAdded < tmNow) {
    en_plpLastPositions->AddPosition(en_plpLastPositions->GetPosition(0));
  }

  return en_plpLastPositions;
}


// Get absolute position of point on entity given relative to its size.
void CEntity::GetEntityPointRatio(const FLOAT3D &vRatio, FLOAT3D &vAbsPoint, BOOL bLerped)
{
  ASSERT(bLerped || GetFPUPrecision() == FPT_24BIT);

  if (en_RenderType != RT_MODEL && en_RenderType != RT_EDITORMODEL && en_RenderType != RT_SKAMODEL 
      && en_RenderType != RT_SKAEDITORMODEL && en_RenderType != RT_BRUSH)
  {
    ASSERT(FALSE);
    vAbsPoint = en_plPlacement.pl_PositionVector;
    return;
  }

  FLOAT3D vMin, vMax;

  if (en_RenderType == RT_BRUSH)
  {
    CBrushMip *pbmMip = en_pbrBrush->GetFirstMip();
    vMin = pbmMip->bm_boxBoundingBox.Min();
    vMax = pbmMip->bm_boxBoundingBox.Max();
    FLOAT3D vOff = vMax-vMin;
    vOff(1) *= vRatio(1);
    vOff(2) *= vRatio(2);
    vOff(3) *= vRatio(3);
    vAbsPoint = vMin+vOff;
  } else {
    if (_pNetwork->ga_ulDemoMinorVersion <= 2) {
      vMin = en_pmoModelObject->GetCollisionBoxMin(GetCollisionBoxIndex());
      vMax = en_pmoModelObject->GetCollisionBoxMax(GetCollisionBoxIndex());
    } else {
      INDEX iEq;
      FLOATaabbox3D box;
      GetCollisionBoxParameters(GetCollisionBoxIndex(), box, iEq);
      vMin = box.Min();
      vMax = box.Max();
    }
    
    FLOAT3D vOff = vMax-vMin;
    vOff(1) *= vRatio(1);
    vOff(2) *= vRatio(2);
    vOff(3) *= vRatio(3);
    FLOAT3D vPos = vMin + vOff;
    if (bLerped) {
      CPlacement3D plLerped = GetLerpedPlacement();
      FLOATmatrix3D mRot;
      MakeRotationMatrixFast(mRot, plLerped.pl_OrientationAngle);
      vAbsPoint = plLerped.pl_PositionVector + vPos * mRot;
    } else {
      vAbsPoint = en_plPlacement.pl_PositionVector + vPos * en_mRotation;
    }
  }
}

// Get absolute position of point on entity given in meters.
void CEntity::GetEntityPointFixed(const FLOAT3D &vFixed, FLOAT3D &vAbsPoint)
{
  vAbsPoint = en_plPlacement.pl_PositionVector+vFixed*en_mRotation;
}

// Get sector that given point is in - point must be inside this entity.
CBrushSector *CEntity::GetSectorFromPoint(const FLOAT3D &vPointAbs)
{
  // For each sector around entity
  {
    FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
    // If point is in this sector return that
    if (pbsc->bsc_bspBSPTree.TestSphere(FLOATtoDOUBLE(vPointAbs), 0.01) >= 0) {
      return pbsc;
    }
    ENDFOR;
  }
  return NULL;
}

// Model change notify
void CEntity::ModelChangeNotify(void)
{
  // If this is ska model
  if (en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    if (GetModelInstance() == NULL) {
      return;
    }

  // this is old model
  } else {
    if (en_pmoModelObject == NULL || en_pmoModelObject->GetData() == NULL) {
      return;
    }
  }

  UpdateSpatialRange();
  FindCollisionInfo();
}

// Terrain change notify.
void CEntity::TerrainChangeNotify(void)
{
//  GetTerrain()->RemoveLayer(0,FALSE);
//  GetTerrain()->AddDefaultLayer_t();
  GetTerrain()->ReBuildTerrain(TRUE);
  UpdateSpatialRange();
}

// Map world polygon to/from indices
CBrushPolygon *CEntity::GetWorldPolygonPointer(INDEX ibpo)
{
  if (ibpo == -1) {
    return NULL;
  } else {
    return en_pwoWorld->wo_baBrushes.ba_apbpo[ibpo];
  }
}

// Get index of some brush polygon in the world.
INDEX CEntity::GetWorldPolygonIndex(CBrushPolygon *pbpo)
{
  if (pbpo == NULL) {
    return -1;
  } else {
    return pbpo->bpo_iInWorld;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sound functions

// Play a given sound object.
void CEntity::PlaySound(CSoundObject &so, SLONG idSoundResource, SLONG slPlayType)
{
  CEntityResource *pecSound = en_pecClass->ResourceForId(idSoundResource, CResource::TYPE_SOUND);
  //so.Stop();
  so.Play(pecSound->er_psdSound, slPlayType);
}

// Get sound length from the resource.
double CEntity::GetSoundLength(SLONG idSoundResource)
{
  CEntityResource *pecSound = en_pecClass->ResourceForId(idSoundResource, CResource::TYPE_SOUND);
  return pecSound->er_psdSound->GetSecondsLength();
}

// Play a given sound object.
void CEntity::PlaySound(CSoundObject &so, const CTFileName &fnmSound, SLONG slPlayType)
{
  // try to load the sound data
  try {
    //so.Stop();
    so.Play_t(fnmSound, slPlayType);
  } catch(char *strError) {
    (void)strError;
    DECLARE_CTFILENAME(fnmDefault, "Sounds\\Default.wav");
    
    // try to load the default sound data
    try {
      so.Play_t(fnmDefault, slPlayType);
    } catch(char *strErrorDefault) {
      FatalError(TRANS("Cannot load default sound '%s':\n%s"),
        (CTString&)fnmDefault, strErrorDefault);
    }
  }
}

// Notify engine that gravity defined by this entity has changed
void CEntity::NotifyGravityChanged(void)
{
  if (_pNetwork->ga_ulDemoMinorVersion <= 2) {
    // For each entity in the world of this entity
    FOREACHINDYNAMICCONTAINER(en_pwoWorld->wo_cenEntities, CEntity, iten) 
    {
      CEntity *pen = &*iten;
      
      // If movable
      if (pen->en_ulPhysicsFlags&EPF_MOVABLE) {
        CMovableEntity *pmen = (CMovableEntity*)pen;
        // If the gravity has changed then add to movers
        pmen->AddToMovers();
      }
    }
  } else {
    // For each zoning brush in the world of this entity
    FOREACHINDYNAMICCONTAINER(en_pwoWorld->wo_cenEntities, CEntity, iten) 
    {
      CEntity *penBrush = &*iten;
      if (iten->en_RenderType != CEntity::RT_BRUSH || !(iten->en_ulFlags & ENF_ZONING)) {
        continue;
      }
      CBrush3D *pbr = penBrush->en_pbrBrush;
      
      // Get first brush mip
      CBrushMip *pbm = pbr->GetFirstMip();
      
      // For each sector in the brush mip
      {
        FOREACHINDYNAMICARRAY(pbm->bm_abscSectors, CBrushSector, itbsc) 
        {
          // If controlled by this entity
          if (penBrush->GetForceController(itbsc->GetForceType()) == this ) 
          {
            // For each entity in the sector
            {
              FOREACHDSTOFSRC(itbsc->bsc_rsEntities, CEntity, en_rdSectors, pen) 
              {
                // If movable then add to movers
                if (pen->en_ulPhysicsFlags&EPF_MOVABLE) {
                  CMovableEntity *pmen = (CMovableEntity*)pen;
                  pmen->AddToMovers();
                }
                ENDFOR
              }
            }
          }
        }
      }
    }
  }
}

// Notify engine that collision of this entity was changed
void CEntity::NotifyCollisionChanged(void)
{
  if (en_pciCollisionInfo == NULL) {
    return;
  }

  // Find colliding entities near this one
  static TStaticStackArray<CEntity*> apenNearEntities;
  en_pwoWorld->FindEntitiesNearBox(en_pciCollisionInfo->ci_boxCurrent, apenNearEntities);
  
  // For each of the found entities
  for (INDEX ienFound = 0; ienFound < apenNearEntities.Count(); ienFound++) {
    CEntity &enToNear = *apenNearEntities[ienFound];

    // If movable then add to movers
    if (enToNear.en_ulPhysicsFlags & EPF_MOVABLE) {
      ((CMovableEntity*)&enToNear)->AddToMovers();
    }
  }
  apenNearEntities.PopAll();
}

// Apply some damage to the entity (see event EDamage for more info)
void CEntity::ReceiveDamage(CEntity *penInflictor, INDEX iDamageType, INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
{
  CEntityPointer penThis = this;  // keep this entity alive during this function

  // just throw an event that you are damaged (base entities don't really have health)
  EDamage eDamage;
  eDamage.penInflictor = penInflictor;
  eDamage.vDirection   = vDirection;
  eDamage.vHitPoint    = vHitPoint;
  eDamage.iAmount      = iDamageAmount;
  eDamage.iDamageType  = iDamageType;
  SendEvent(eDamage);
}

// Receive item through event
BOOL CEntity::ReceiveItem(const CEntityEvent &ee)
{
  return FALSE;
}

// Get entity info 
void *CEntity::GetEntityInfo(void)
{
  return NULL;
};

// Fill in entity statistics - for AI purposes only
BOOL CEntity::FillEntityStatistics(struct EntityStats *pes)
{
  return FALSE;
}

// Precache resources that might be needed.
void CEntity::Precache(void)
{
  NOTHING;
}

// Create a checksum value for sync-check
void CEntity::ChecksumForSync(ULONG &ulCRC, INDEX iExtensiveSyncCheck)
{
  if (iExtensiveSyncCheck > 0) {
    CRC_AddLONG(ulCRC, en_ulFlags & ~(ENF_SELECTED | ENF_INRENDERING | ENF_VALIDSHADINGINFO 
                                      | ENF_FOUNDINGRIDSEARCH | ENF_WILLBEPREDICTED | ENF_PREDICTABLE));
    CRC_AddLONG(ulCRC, en_ulPhysicsFlags);
    CRC_AddLONG(ulCRC, en_ulCollisionFlags);
    CRC_AddLONG(ulCRC, GetReferenceCount());
  }
  
  CRC_AddLONG(ulCRC, en_RenderType);
  
  if (iExtensiveSyncCheck > 0) {
    CRC_AddLONG(ulCRC, en_ulID);
    CRC_AddFLOAT(ulCRC, en_fSpatialClassificationRadius);
    CRC_AddFLOAT(ulCRC, en_plPlacement.pl_PositionVector(1));
    CRC_AddFLOAT(ulCRC, en_plPlacement.pl_PositionVector(2));
    CRC_AddFLOAT(ulCRC, en_plPlacement.pl_PositionVector(3));
    CRC_AddFLOAT(ulCRC, en_plPlacement.pl_OrientationAngle(1));
    CRC_AddFLOAT(ulCRC, en_plPlacement.pl_OrientationAngle(2));
    CRC_AddFLOAT(ulCRC, en_plPlacement.pl_OrientationAngle(3));

    CRC_AddBlock(ulCRC, (UBYTE*)(void*)&en_mRotation, sizeof(en_mRotation));
  } else {
    CRC_AddLONG(ulCRC, (int)en_plPlacement.pl_PositionVector(1));
    CRC_AddLONG(ulCRC, (int)en_plPlacement.pl_PositionVector(2));
    CRC_AddLONG(ulCRC, (int)en_plPlacement.pl_PositionVector(3));
  }
}

// Dump sync data to text file.
void CEntity::DumpSync_t(CTStream &strm, INDEX iExtensiveSyncCheck)
{
  strm.FPrintF_t("\n---- #%05d ($%05d)----------------\n", en_pwoWorld->wo_cenAllEntities.Index(this),
                 en_pwoWorld->wo_cenEntities.Index(this));
  if (en_ulFlags & ENF_DELETED) {
    strm.FPrintF_t("*** DELETED ***\n");
  }
  strm.FPrintF_t("class: '%s'\n", GetClass()->ec_pdecDLLClass->dec_strName);
  strm.FPrintF_t("name: '%s'\n", GetName());
  
  if (iExtensiveSyncCheck > 0) {
    strm.FPrintF_t("en_ulFlags:          0x%08X\n", en_ulFlags & ~(ENF_SELECTED | ENF_INRENDERING
                                                                   | ENF_VALIDSHADINGINFO | ENF_FOUNDINGRIDSEARCH
                                                                   | ENF_WILLBEPREDICTED | ENF_PREDICTABLE));
    strm.FPrintF_t("en_ulPhysicsFlags:   0x%08X\n", en_ulPhysicsFlags);
    strm.FPrintF_t("en_ulCollisionFlags: 0x%08X\n", en_ulCollisionFlags);
    strm.FPrintF_t("en_ctReferences: %d\n", GetReferenceCount());
  }
  strm.FPrintF_t("en_RenderType: %d\n", en_RenderType);
  strm.FPrintF_t("en_ulID: 0x%08x\n", en_ulID);
  
  if (iExtensiveSyncCheck > 0) {
    strm.FPrintF_t("en_fSpatialClassificationRadius: %g(%08x)\n", 
      en_fSpatialClassificationRadius, (ULONG&)en_fSpatialClassificationRadius);
  }
  strm.FPrintF_t("placement: %g,%g,%g : %g,%g,%g\n",
    en_plPlacement.pl_PositionVector(1),
    en_plPlacement.pl_PositionVector(2),
    en_plPlacement.pl_PositionVector(3),
    en_plPlacement.pl_OrientationAngle(1),
    en_plPlacement.pl_OrientationAngle(2),
    en_plPlacement.pl_OrientationAngle(3));
    
  if (iExtensiveSyncCheck > 0) {
    strm.FPrintF_t("placement raw:\n %08X %08X %08X\n %08X %08X %08X\n",
      (ULONG&)en_plPlacement.pl_PositionVector(1),
      (ULONG&)en_plPlacement.pl_PositionVector(2),
      (ULONG&)en_plPlacement.pl_PositionVector(3),
      (ULONG&)en_plPlacement.pl_OrientationAngle(1),
      (ULONG&)en_plPlacement.pl_OrientationAngle(2),
      (ULONG&)en_plPlacement.pl_OrientationAngle(3));
    strm.FPrintF_t("matrix:\n %g %g %g\n %g %g %g\n %g %g %g\n",
      en_mRotation(1, 1), en_mRotation(1, 2), en_mRotation(1, 3),
      en_mRotation(2, 1), en_mRotation(2, 2), en_mRotation(2, 3),
      en_mRotation(3, 1), en_mRotation(3, 2), en_mRotation(3, 3));
    strm.FPrintF_t("matrix raw:\n %08X %08X %08X\n %08X %08X %08X\n %08X %08X %08X\n",
      (ULONG&)en_mRotation(1, 1), (ULONG&)en_mRotation(1, 2), (ULONG&)en_mRotation(1, 3),
      (ULONG&)en_mRotation(2, 1), (ULONG&)en_mRotation(2, 2), (ULONG&)en_mRotation(2, 3),
      (ULONG&)en_mRotation(3, 1), (ULONG&)en_mRotation(3, 2), (ULONG&)en_mRotation(3, 3));
      
    if (en_pciCollisionInfo == NULL) {
      strm.FPrintF_t("Collision info NULL\n");
    } else if (en_RenderType == RT_BRUSH || en_RenderType == RT_FIELDBRUSH) {
      strm.FPrintF_t("Collision info: Brush entity\n");
    } else {
      strm.FPrintF_t("Collision info:\n");
      strm.FPrintF_t("Min height, Max height: %g, %g\n",
        en_pciCollisionInfo->ci_fMinHeight, en_pciCollisionInfo->ci_fMaxHeight);
      strm.FPrintF_t("Handle Y, Handle R: %g, %g\n",
        en_pciCollisionInfo->ci_fHandleY, en_pciCollisionInfo->ci_fHandleR);

      strm.FPrintF_t("Handle Y, Handle R: %g, %g\n",
        en_pciCollisionInfo->ci_fHandleY, en_pciCollisionInfo->ci_fHandleR);
    
      DUMPVECTOR(en_pciCollisionInfo->ci_boxCurrent.Min());
      DUMPVECTOR(en_pciCollisionInfo->ci_boxCurrent.Max());
      DUMPLONG(en_pciCollisionInfo->ci_ulFlags);
    }
  }
}


// Get a pseudo-random integer number (safe for network gaming).
ULONG CEntity::IRnd(void) 
{
  return ((_pNetwork->ga_sesSessionState.Rnd() >> (31 - 16)) & 0xFFFF);
}

// Get a pseudo-random decimal number (safe for network gaming).
FLOAT CEntity::FRnd(void)
{
  return ((_pNetwork->ga_sesSessionState.Rnd() >> (31 - 24)) & 0xFFFFFF) / FLOAT(0xFFFFFF);
}

// Returns ammount of memory used by entity
SLONG CEntity::GetUsedMemory(void)
{
  // Initial size
  SLONG slUsedMemory = sizeof(CEntity);

  // Add relations
  slUsedMemory += en_rdSectors.Count() * sizeof(CRelationLnk);

  // Add allocated memory for model type (if any)
  switch (en_RenderType) 
  {
    case CEntity::RT_MODEL:
    case CEntity::RT_EDITORMODEL:
      slUsedMemory += en_pmoModelObject->GetUsedMemory();
      break;
      
    case CEntity::RT_SKAMODEL:
    case CEntity::RT_SKAEDITORMODEL:
      slUsedMemory += en_pmiModelInstance->GetUsedMemory();
      
    default:
      break;
  }

  // Add shading info (if any)
  if (en_psiShadingInfo != NULL) {
    slUsedMemory += sizeof(CShadingInfo);
  }
  // Add collision info (if any)
  if (en_pciCollisionInfo != NULL) {
    slUsedMemory += sizeof(CCollisionInfo) + (en_pciCollisionInfo->ci_absSpheres.sa_Count * sizeof(CMovingSphere));
  }
  
  // Add last positions memory (if any)
  if (en_plpLastPositions != NULL) {
    slUsedMemory += sizeof(CLastPositions) + (en_plpLastPositions->lp_avPositions.sa_Count * sizeof(FLOAT3D));
  }

  // Done
  return slUsedMemory;
}



// Get pointer to entity property from its packed identifier.
class CEntityProperty *CEntity::PropertyForId(ULONG ulId, ULONG ulType)
{
  return en_pecClass->PropertyForId(ulId, (CEntityProperty::Type)ulType);
}

// Get pointer to entity resource from its packed identifier.
class CEntityResource *CEntity::ResourceForId(ULONG ulId, ULONG ulType)
{
  return en_pecClass->ResourceForId(ulId, (CResource::Type)ulType);
}

// Get pointer to entity property from its name.
class CEntityProperty *CEntity::PropertyForName(const CTString &strPropertyName)
{
  return en_pecClass->PropertyForName(strPropertyName);
}
 
// Create a new entity of given class in this world.
CEntity *CEntity::CreateEntity(const CPlacement3D &plPlacement, SLONG idClass)
{
  CEntityResource *pecClassResource = en_pecClass->ResourceForId(idClass, CResource::TYPE_ENTITYCLASS);

  return en_pwoWorld->CreateEntity(plPlacement, pecClassResource->er_pecEntityClass);
}

// Get session properties for current game.
CGameObject *CEntity::GetSessionProperties(void)
{
  return _pNetwork->GetSessionProperties();
}