/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Brushes/Brush.h>

#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/StaticStackArray.cpp>
#include <Core/Math/Geometry.inl>
#include <Core/Math/Clipping.inl>

static CEntity *_pen;
static FLOAT3D _vHandle;
static CBrushPolygon *_pbpoNear;
static FLOAT _fNearDistance;
static FLOAT3D _vNearPoint;
static FLOATplane3D _plPlane;

//! Wrapper class with pointer to a brush sector.
class CActiveSector
{
  public:
    CBrushSector *as_pbsc;

    //! Dummy method used by a TStaticArray.
    void Clear(void) {};
};

static TStaticStackArray<CActiveSector> _aas;

// Add a sector if needed.
static void AddSector(CBrushSector *pbsc)
{
  // If not already active and in first mip of its brush
  if (pbsc->bsc_pbmBrushMip->IsFirstMip() && !(pbsc->bsc_ulFlags & BSCF_NEARTESTED))
  {
    // Add it to active sectors
    _aas.Push().as_pbsc = pbsc;
    pbsc->bsc_ulFlags|=BSCF_NEARTESTED;
  }
}

// Add all sectors of a brush.
static void AddAllSectorsOfBrush(CBrush3D *pbr)
{
  // Get first mip
  CBrushMip *pbmMip = pbr->GetFirstMip();
  
  // If it has no brush mip for that mip factor then skip it
  if (pbmMip == NULL) {
    return;
  }
  
  // For each sector in the brush mip
  FOREACHINDYNAMICARRAY(pbmMip->bm_abscSectors, CBrushSector, itbsc)
  {
    // Add the sector
    AddSector(itbsc);
  }
}

void SearchThroughSectors(void)
{
  // For each active sector (sectors are added during iteration!)
  for (INDEX ias = 0; ias < _aas.Count(); ias++)
  {
    CBrushSector *pbsc = _aas[ias].as_pbsc;
    
    // For each polygon in the sector
    {
      FOREACHINSTATICARRAY(pbsc->bsc_abpoPolygons, CBrushPolygon, itbpo)
      {
        CBrushPolygon &bpo = *itbpo;
        
        // If it is not a wall then skip it
        if (bpo.bpo_ulFlags & BPOF_PORTAL) {
          continue;
        }
        const FLOATplane3D &plPolygon = bpo.bpo_pbplPlane->bpl_plAbsolute;
        
        // Find distance of the polygon plane from the handle
        FLOAT fDistance = plPolygon.PointDistance(_vHandle);
        
        // If it is behind the plane or further than nearest found then skip it
        if (fDistance < 0.0f || fDistance > _fNearDistance) {
          continue;
        }
        
        // Find projection of handle to the polygon plane
        FLOAT3D vOnPlane = plPolygon.ProjectPoint(_vHandle);
        
        // If it is not in the bounding box of polygon
        const FLOATaabbox3D &boxPolygon = bpo.bpo_boxBoundingBox;
        const FLOAT EPSILON = 0.01f;
        if (
          (boxPolygon.Min()(1) - EPSILON > vOnPlane(1)) ||
          (boxPolygon.Max()(1) + EPSILON < vOnPlane(1)) ||
          (boxPolygon.Min()(2) - EPSILON > vOnPlane(2)) ||
          (boxPolygon.Max()(2) + EPSILON < vOnPlane(2)) ||
          (boxPolygon.Min()(3) - EPSILON > vOnPlane(3)) ||
          (boxPolygon.Max()(3) + EPSILON < vOnPlane(3)))
        {
          // skip it
          continue;
        }

        // Find major axes of the polygon plane
        INDEX iMajorAxis1, iMajorAxis2;
        GetMajorAxesForPlane(plPolygon, iMajorAxis1, iMajorAxis2);

        
        // Create an intersector
        CIntersector isIntersector(_vHandle(iMajorAxis1), _vHandle(iMajorAxis2));
        
        // For all edges in the polygon
        FOREACHINSTATICARRAY(bpo.bpo_abpePolygonEdges, CBrushPolygonEdge, itbpePolygonEdge)
        {
          // Get edge vertices (edge direction is irrelevant here!)
          const FLOAT3D &vVertex0 = itbpePolygonEdge->bpe_pbedEdge->bed_pbvxVertex0->bvx_vAbsolute;
          const FLOAT3D &vVertex1 = itbpePolygonEdge->bpe_pbedEdge->bed_pbvxVertex1->bvx_vAbsolute;
          
          // Pass the edge to the intersector
          isIntersector.AddEdge(vVertex0(iMajorAxis1), vVertex0(iMajorAxis2), 
                                vVertex1(iMajorAxis1), vVertex1(iMajorAxis2));
        }

        // If the point is not inside polygon then skip it
        if (!isIntersector.IsIntersecting()) {
          continue;
        }

        // Remember the polygon
        _pbpoNear = &bpo;
        _fNearDistance = fDistance;
        _vNearPoint = vOnPlane;
      }
    }

    // For each entity in the sector
    {
      FOREACHDSTOFSRC(pbsc->bsc_rsEntities, CEntity, en_rdSectors, pen)
        // If it is a brush
        if (pen->en_RenderType == CEntity::RT_BRUSH) {
          // Get its brush
          CBrush3D &brBrush = *pen->en_pbrBrush;
          
          // Add all sectors in the brush
          AddAllSectorsOfBrush(&brBrush);
        }
      ENDFOR
    }
  }
}

/* Get nearest position of nearest brush polygon to this entity if available. */
// use:
// ->bpo_pbscSector->bsc_pbmBrushMip->bm_pbrBrush->br_penEntity
// to get the entity
CBrushPolygon *CEntity::GetNearestPolygon(FLOAT3D &vPoint, FLOATplane3D &plPlane, FLOAT &fDistanceToEdge)
{
  _pen = this;
  // Take reference point at handle of the model entity
  _vHandle = en_plPlacement.pl_PositionVector;

  // Start infinitely far away
  _pbpoNear = NULL;
  _fNearDistance = UpperLimit(1.0f);

  // For each zoning sector that this entity is in
  {
    FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
      // Add the sector
      AddSector(pbsc);
    ENDFOR
  }

  // Start the search
  SearchThroughSectors();

  // For each active sector
  for (INDEX ias = 0; ias < _aas.Count(); ias++)
  {
    // Mark it as inactive
    _aas[ias].as_pbsc->bsc_ulFlags&=~BSCF_NEARTESTED;
  }
  _aas.PopAll();

  // If there is some polygon found then return info
  if (_pbpoNear != NULL) {
    plPlane = _pbpoNear->bpo_pbplPlane->bpl_plAbsolute;
    vPoint = _vNearPoint;
    fDistanceToEdge = _pbpoNear->GetDistanceFromEdges(_vNearPoint);
    return _pbpoNear;
    
  // If none is found then return failure
  } else {
    return NULL;
  }
}
