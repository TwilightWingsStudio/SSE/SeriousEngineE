/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENTITYCLASS_H
#define SE_INCL_ENTITYCLASS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Modules/ModuleClass.h>
#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityProperties.h> /* rcg10042001 */

//! General structure of an entity class.
class ENGINE_API CEntityClass : public FModuleClass 
{
  public:
    CTFileName ec_fnmModule;              // filename of the DLL with the class
    class CLibEntityClass *ec_pdecDLLClass; // pointer to DLL class in the DLL

  public:
    //! Default constructor.
    CEntityClass(void);
    
    //! Constructor for a fixed class.
    CEntityClass(class CLibEntityClass *pdecLibClass);
    
    //! Destructor.
    ~CEntityClass(void);
    
    // Clear the object.
    void Clear(void);

    //! Obtain all entries from resource table.
    void ObtainResources_t(void);  // throw char *
    
    //! Release all entries from resource table.
    void ReleaseResources(void);

    //! Add one reference to this class.
    void AddReference(void);
    
    //! Remove one reference to this class.
    void RemReference(void);

    //! Check that all properties have been properly declared.
    void CheckClassProperties(void);

    //! Construct a new member of the class.
    class CEntity *New(void);

    //! Get pointer to entity property from its packed identifier.
    class CEntityProperty *PropertyForId(ULONG ulId, enum CEntityProperty::Type eType = CEntityProperty::EPT_INVALID);

    //! Get pointer to entity property from its name.
    class CEntityProperty *PropertyForName(const CTString &strPropertyName);

    //! Get event handler for given state and event code.
    CEntity::pEventHandler HandlerForStateAndEvent(SLONG slState, SLONG slEvent);

    //! Get pointer to resource from its type and identifier.
    class CEntityResource *ResourceForId(ULONG ulId, enum CResource::Type eType = CResource::TYPE_INVALID);
    
    //! Get pointer to resource from the resource.
    class CEntityResource *ResourceForPointer(void *pv);

    //! Precache entity resource for its identifier and type.
    void Precache(SLONG idEntityResource, CResource::Type eType = CResource::TYPE_INVALID, INDEX iUser = -1);

    // Overrides from CResource
    //! Read from stream.
    virtual void Read_t(CTStream *istr);  // throw char *
    
    //! Write to stream.
    virtual void Write_t(CTStream *ostr); // throw char *

    //! Get amount of memory used by this object.
    SLONG GetUsedMemory(void);
    
    //! Check if this kind of objects is auto-freed.
    BOOL IsAutoFreed(void);
    
    //! Gather the CRC of the file.
    void AddToCRCTable(void);

    //! Get type of this resource.
    virtual UBYTE GetType() const;
};

#endif  /* include-once check. */