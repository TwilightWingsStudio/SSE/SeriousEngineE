/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityCollision.h>
#include <Core/Base/Console.h>
#include <Core/Base/ListIterator.inl>
#include <Core/Math/Geometry.inl>
#include <Core/Math/Clipping.inl>
#include <Core/Math/OBBox.h>
#include <Engine/Brushes/Brush.h>
#include <Core/Templates/BSP.h>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/StaticArray.cpp>

static CEntity *penField;
static CBrushSector *_pbsc;
static TStaticStackArray<CEntity*> _apenActive;

static BOOL EntityIsInside(CEntity *pen)
{
  // Get bounding sphere
  FLOAT fSphereRadius = pen->en_fSpatialClassificationRadius;
  const FLOAT3D &vSphereCenter = pen->en_plPlacement.pl_PositionVector;

  // If the entity touches sector's bounding box
  if (_pbsc->bsc_boxBoundingBox.TouchesSphere(vSphereCenter, fSphereRadius))
  {
    // Make oriented bounding box of the entity
    const FLOAT3D &v = pen->en_plPlacement.pl_PositionVector;
    const FLOATmatrix3D &m = pen->en_mRotation;
    FLOATobbox3D boxEntity = FLOATobbox3D(pen->en_boxSpatialClassification, v, m);
    DOUBLEobbox3D boxdEntity = FLOATtoDOUBLE(boxEntity);

    // If the box touches the sector's BSP
    if (boxEntity.HasContactWith(FLOATobbox3D(_pbsc->bsc_boxBoundingBox)) && _pbsc->bsc_bspBSPTree.TestBox(boxdEntity) <= 0)
    {
      // For each collision sphere
      TStaticArray<CMovingSphere> &absSpheres = pen->en_pciCollisionInfo->ci_absSpheres;
      for (INDEX iSphere = 0; iSphere < absSpheres.Count(); iSphere++)
      {
        CMovingSphere &ms = absSpheres[iSphere];
        ms.ms_vRelativeCenter0 = ms.ms_vCenter * m + v;
        
        // If the sphere is in the sector
        if (_pbsc->bsc_bspBSPTree.TestSphere(FLOATtoDOUBLE(ms.ms_vRelativeCenter0), ms.ms_fR) <= 0) {
          return TRUE;
        }
      }
    }
  }

  // Otherwise it doesn't touch
  return FALSE;
}

// Find first entity touching a field (this entity must be a field brush)
CEntity *CEntity::TouchingEntity(BOOL (*ConsiderEntity)(CEntity *), CEntity *penHintMaybeInside)
{
  // If not a field brush then error
  if (en_RenderType != RT_FIELDBRUSH) {
    ASSERT(FALSE);
    return NULL;
  }

  // Remember the entity and its first sector
  penField = this;
  CBrushMip *pbm = en_pbrBrush->GetBrushMipByDistance(0.0f);
  _pbsc = NULL;
  {
    FOREACHINDYNAMICARRAY(pbm->bm_abscSectors, CBrushSector, itbsc)
    {
      _pbsc = itbsc;
      break;
    }
  }
  
  // If illegal number of sectors then error
  if (_pbsc == NULL || pbm->bm_abscSectors.Count() > 1) {
    CWarningF("Field doesn't have exactly one sector - ignoring!\n");
    return NULL;
  }

  // If a specific entity to check is given
  if (penHintMaybeInside != NULL) {
    // if it is inside then return it
    if (EntityIsInside(penHintMaybeInside)) {
      return penHintMaybeInside;
    }
  }

  CEntity *penTouched = NULL;
  
  // No entities active initially
  _apenActive.PopAll();
  
  // For each zoning sector that this entity is in
  {
    FOREACHSRCOFDST(en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
    
    // For all movable model entities that should be considered in the sector
    {
      FOREACHDSTOFSRC(pbsc->bsc_rsEntities, CEntity, en_rdSectors, pen)
        if (!(pen->en_ulPhysicsFlags & EPF_MOVABLE) 
            || (pen->en_RenderType != RT_MODEL && pen->en_RenderType != RT_EDITORMODEL)
            || (!ConsiderEntity(pen))) 
        {
          continue;
        }
        
        // If already active then skip it
        if (pen->en_ulFlags & ENF_FOUNDINGRIDSEARCH) {
          continue;
        }
        
        // If it is inside then stop the search
        if (EntityIsInside(pen)) {
          penTouched = pen;
          break;
        }
        
        // Add it to active
        _apenActive.Push() = pen;
        pen->en_ulFlags |= ENF_FOUNDINGRIDSEARCH;
    }
  }
  ENDFOR} // WARNING: DON'T TOUCH THIS, PLS ^_^

  // Mark all as inactive
  {
    for (INDEX ien = 0; ien < _apenActive.Count(); ien++)
    {
      CEntity *pen = _apenActive[ien];
      pen->en_ulFlags &= ~ENF_FOUNDINGRIDSEARCH;
    }
  }

  _apenActive.PopAll();

  return penTouched;
}