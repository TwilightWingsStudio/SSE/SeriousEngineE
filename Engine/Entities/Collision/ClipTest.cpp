/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityCollision.h>
#include <Engine/Entities/Collision/ClipTest.h>
#include <Core/Base/ListIterator.inl>
#include <Core/Math/Geometry.inl>
#include <Core/Math/Clipping.inl>
#include <Engine/Brushes/Brush.h>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Engine/Network/Network.h>
#include <Engine/Network/SessionState.h>

// Project spheres of a collision info to given placement
void CClipTest::ProjectSpheresToPlacement(CCollisionInfo &ci, FLOAT3D &vPosition, FLOATmatrix3D &mRotation)
{
  // For each sphere
  FOREACHINSTATICARRAY(ci.ci_absSpheres, CMovingSphere, itms)
  {
    // Project it in start point
    itms->ms_vRelativeCenter0 = itms->ms_vCenter * mRotation + vPosition;
  }
}

// Test point to a sphere
BOOL CClipTest::PointTouchesSphere(const FLOAT3D &vPoint, const FLOAT3D &vSphereCenter, const FLOAT fSphereRadius)
{
  FLOAT fD = (vSphereCenter - vPoint).Length();
  return fD < fSphereRadius;
}

// Test sphere to the edge (point to the edge cylinder)
BOOL CClipTest::PointTouchesCylinder(const FLOAT3D &vPoint, const FLOAT3D &vCylinderBottomCenter,
                                     const FLOAT3D &vCylinderTopCenter, const FLOAT fCylinderRadius)
{
  const FLOAT3D vCylinderBottomToTop       = vCylinderTopCenter - vCylinderBottomCenter;
  const FLOAT   fCylinderBottomToTopLength = vCylinderBottomToTop.Length();
  const FLOAT3D vCylinderDirection         = vCylinderBottomToTop / fCylinderBottomToTopLength;
  
  FLOAT3D vBottomToPoint = vPoint - vCylinderBottomCenter;
  FLOAT fPointL = vBottomToPoint % vCylinderDirection;

  // If not between top and bottom then doesn't touch
  if (fPointL < 0 || fPointL > fCylinderBottomToTopLength) {
    return FALSE;
  }

  // Find distance from point to cylinder axis
  FLOAT fD = (vBottomToPoint - vCylinderDirection * fPointL).Length();

  return fD < fCylinderRadius;
}

// Test if a sphere touches brush polygon
BOOL CClipTest::SphereTouchesBrushPolygon(const CMovingSphere &msMoving, CBrushPolygon *pbpoPolygon)
{
  const FLOATplane3D &plPolygon = pbpoPolygon->bpo_pbplPlane->bpl_plAbsolute;
  
  // Calculate point distance from polygon plane
  FLOAT fDistance = plPolygon.PointDistance(msMoving.ms_vRelativeCenter0);

  // If is further away than sphere radius then no collision
  if (fDistance > msMoving.ms_fR || fDistance < -msMoving.ms_fR) {
    return FALSE;
  }

  // Calculate coordinate projected to the polygon plane
  FLOAT3D vPosMid = msMoving.ms_vRelativeCenter0;
  FLOAT3D vHitPoint = plPolygon.ProjectPoint(vPosMid);
  
  // Find major axes of the polygon plane
  INDEX iMajorAxis1, iMajorAxis2;
  GetMajorAxesForPlane(plPolygon, iMajorAxis1, iMajorAxis2);

  // Create an intersector
  CIntersector isIntersector(vHitPoint(iMajorAxis1), vHitPoint(iMajorAxis2));
  
  // For all edges in the polygon
  FOREACHINSTATICARRAY(pbpoPolygon->bpo_abpePolygonEdges, CBrushPolygonEdge, itbpePolygonEdge)
  {
    // Get edge vertices (edge direction is irrelevant here!)
    const FLOAT3D &vVertex0 = itbpePolygonEdge->bpe_pbedEdge->bed_pbvxVertex0->bvx_vAbsolute;
    const FLOAT3D &vVertex1 = itbpePolygonEdge->bpe_pbedEdge->bed_pbvxVertex1->bvx_vAbsolute;
    
    // Pass the edge to the intersector
    isIntersector.AddEdge(vVertex0(iMajorAxis1), vVertex0(iMajorAxis2),
                          vVertex1(iMajorAxis1), vVertex1(iMajorAxis2));
  }
  
  // If the polygon is intersected by the ray
  if (isIntersector.IsIntersecting()) {
    return TRUE;
  }

  // For each edge in polygon
  FOREACHINSTATICARRAY(pbpoPolygon->bpo_abpePolygonEdges, CBrushPolygonEdge, itbpe)
  {
    // Get edge vertices (edge direction is important here!)
    FLOAT3D vVertex0, vVertex1;
    itbpe->GetVertexCoordinatesAbsolute(vVertex0, vVertex1);

    // Test sphere to the edge (point to the edge cylinder)
    if (PointTouchesCylinder(
          msMoving.ms_vRelativeCenter0, // point,
          vVertex0,                     // cylinder bottom center,
          vVertex1,                     // cylinder top center,
          msMoving.ms_fR                // cylinder radius
        )) {
      return TRUE;
    }
    
    // Test sphere to the first vertex
    // NOTE: using point to sphere collision
    if (PointTouchesSphere(
          msMoving.ms_vRelativeCenter0,  // pount
          vVertex0,                      // sphere center
          msMoving.ms_fR                 // sphere radius
          )) {
      return TRUE;
    }
  }

  return FALSE;
}

// Test if entity touches brush polygon
BOOL CClipTest::EntityTouchesBrushPolygon(CBrushPolygon *pbpoPolygon)
{
  // For each sphere
  FOREACHINSTATICARRAY(ct_ciNew.ci_absSpheres, CMovingSphere, itms)
  {
    // If it touches
    if (SphereTouchesBrushPolygon(*itms, pbpoPolygon)) {
      return TRUE;
    }
  }
  return FALSE;
}

// Test if an entity can change to a new collision box without intersecting anything
BOOL CClipTest::CanChange(CEntity *pen, INDEX iNewCollisionBox)
{
  // Can be used only for models
  ASSERT(
    pen->en_RenderType == CEntity::RT_MODEL ||
    pen->en_RenderType == CEntity::RT_EDITORMODEL ||
    pen->en_RenderType == CEntity::RT_SKAMODEL ||
    pen->en_RenderType == CEntity::RT_SKAEDITORMODEL);

  // Safety check
  if (pen->en_pciCollisionInfo == NULL) {
    return FALSE;
  }

  // Remember parameters
  ct_penEntity = pen;
  ct_iNewCollisionBox = iNewCollisionBox;
  ct_penObstacle = NULL;

  // Create new temporary collision info
  ct_ciNew.FromModel(pen, iNewCollisionBox);
  
  // Project it to entity placement
  ProjectSpheresToPlacement(ct_ciNew, 
    pen->en_plPlacement.pl_PositionVector, pen->en_mRotation);

  // Get total bounding box encompassing both old and new collision boxes
  FLOATaabbox3D boxOld, boxNew;
  ASSERT(ct_penEntity->en_pciCollisionInfo != NULL);
  CCollisionInfo &ciOld = *ct_penEntity->en_pciCollisionInfo;
  ciOld.MakeBoxAtPlacement(ct_penEntity->en_plPlacement.pl_PositionVector, ct_penEntity->en_mRotation, boxOld);
  ct_ciNew.MakeBoxAtPlacement(ct_penEntity->en_plPlacement.pl_PositionVector, ct_penEntity->en_mRotation, boxNew);

  ct_boxTotal  = boxOld;
  ct_boxTotal |= boxNew;

  // For each zoning sector that this entity is in
  {
    FOREACHSRCOFDST(ct_penEntity->en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
      // Add it to list of active sectors
      ct_lhActiveSectors.AddTail(pbsc->bsc_lnInActiveSectors);
    ENDFOR
  }

  // For each active sector
  FOREACHINLIST(CBrushSector, bsc_lnInActiveSectors, ct_lhActiveSectors, itbsc)
  {
    // For non-zoning brush entities in the sector
    {
      FOREACHDSTOFSRC(itbsc->bsc_rsEntities, CEntity, en_rdSectors, pen)
        if (pen->en_RenderType != CEntity::RT_BRUSH &&
              (_pNetwork->ga_ulDemoMinorVersion <= 4 || pen->en_RenderType != CEntity::RT_FIELDBRUSH))
        {
          break;  // brushes are sorted first in list
        }

        // Get first mip
        CBrushMip *pbm = pen->en_pbrBrush->GetFirstMip();
        
        // If brush mip exists for that mip factor
        if (pbm!=NULL) {
          // For each sector in the mip
          {
            FOREACHINDYNAMICARRAY(pbm->bm_abscSectors, CBrushSector, itbscNonZoning) 
            {
              CBrushSector &bscNonZoning = *itbscNonZoning;
              // Add it to list of active sectors
              if (!bscNonZoning.bsc_lnInActiveSectors.IsLinked()) {
                ct_lhActiveSectors.AddTail(bscNonZoning.bsc_lnInActiveSectors);
              }
            }
          }
        }
      ENDFOR
    }

    CEntity *penSectorBrush = itbsc->bsc_pbmBrushMip->bm_pbrBrush->br_penEntity;
    // If the sector's brush doesn't have collision then skip it
    if (penSectorBrush->en_ulCollisionFlags == 0 || 
          (_pNetwork->ga_ulDemoMinorVersion > 2 && penSectorBrush->en_RenderType != CEntity::RT_BRUSH))
    {
      continue;
    }

    // For each polygon in the sector
    FOREACHINSTATICARRAY(itbsc->bsc_abpoPolygons, CBrushPolygon, itbpo)
    {
      CBrushPolygon *pbpo = itbpo;
      // If its bbox has no contact with bbox to test then skip it
      if (!pbpo->bpo_boxBoundingBox.HasContactWith(ct_boxTotal)) {
        continue;
      }

      // If it is passable
      if (pbpo->bpo_ulFlags & BPOF_PASSABLE)
      {
        // For each sector related to the portal
        {
          FOREACHDSTOFSRC(pbpo->bpo_rsOtherSideSectors, CBrushSector, bsc_rdOtherSidePortals, pbscRelated)
            // If the sector is not active then add it to active list
            if (!pbscRelated->bsc_lnInActiveSectors.IsLinked()) {
              ct_lhActiveSectors.AddTail(pbscRelated->bsc_lnInActiveSectors);
            }
          ENDFOR
        }

      // If it is not passable
      } else {
        // If entity touches it then test fails
        if (EntityTouchesBrushPolygon(pbpo)) {
          ct_penObstacle = pbpo->bpo_pbscSector->bsc_pbmBrushMip->bm_pbrBrush->br_penEntity;
          return FALSE;
        }
      }
    }
  }
  return TRUE;
}

// Destructor.
CClipTest::~CClipTest(void)
{
  // Clear list of active sectors
  {
    FORDELETELIST(CBrushSector, bsc_lnInActiveSectors, ct_lhActiveSectors, itbsc)
    {
      itbsc->bsc_lnInActiveSectors.Remove();
    }
  }
}