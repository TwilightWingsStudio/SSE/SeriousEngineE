/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Brushes/Brush.h>
#include <Engine/Models/ModelObject.h>
#include <Core/Math/Float.h>

// FIXME: CPP inclusion
#include <Core/Templates/DynamicArray.cpp>

#include <Engine/Entities/Collision/CollisionInfo.h>

// Max. distance between two spheres (as factor of radius of one sphere)
#define MIN_SPHEREDENSITY 1.0f

// Copy constructor.
CCollisionInfo::CCollisionInfo(const CCollisionInfo &ciOrg)
{
  ci_absSpheres = ciOrg.ci_absSpheres;
  ci_fMinHeight = ciOrg.ci_fMinHeight;
  ci_fMaxHeight = ciOrg.ci_fMaxHeight;
  ci_fHandleY = ciOrg.ci_fHandleY;
  ci_fHandleR = ciOrg.ci_fHandleR;
  ci_boxCurrent = ciOrg.ci_boxCurrent;
  ci_ulFlags = ciOrg.ci_ulFlags;
}

// Create collision info for a model.
void CCollisionInfo::FromModel(CEntity *penModel, INDEX iBox)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Get collision box information from the model
  FLOATaabbox3D boxModel;
  INDEX iBoxType;
  penModel->GetCollisionBoxParameters(iBox, boxModel, iBoxType);
  FLOAT3D vBoxOffset = boxModel.Center();
  FLOAT3D vBoxSize = boxModel.Size();

  //  ASSERT(iBoxType==LENGHT_EQ_WIDTH);
  ci_ulFlags = 0;

  INDEX iAxisMain; // in which direction are spheres set
  INDEX iAxis1, iAxis2; // other axis

  if (iBoxType == LENGTH_EQ_WIDTH) {
    iAxisMain = 2;
    iAxis1 = 1; iAxis2 = 3;
  }
  else if (iBoxType == HEIGHT_EQ_WIDTH) {
    iAxisMain = 3;
    iAxis1 = 2; iAxis2 = 1;
  }
  else if (iBoxType == LENGTH_EQ_HEIGHT) {
    iAxisMain = 1;
    iAxis1 = 2; iAxis2 = 3;
  }
  else {
    ASSERTALWAYS("Invalid collision box");
    iAxisMain = 2;
    iAxis1 = 1; iAxis2 = 3;
  }

  // Calculate radius of one sphere
  FLOAT fSphereRadius = vBoxSize(iAxis1) / 2.0f;

  // Calculate length along which to set spheres
  FLOAT fSphereCentersSpan = vBoxSize(iAxisMain) - fSphereRadius * 2;

  // Calculate number of spheres to use
  INDEX ctSpheres = 0;
  if (fSphereRadius > 0.0001f) {
    ctSpheres = INDEX(ceil(fSphereCentersSpan / (fSphereRadius * MIN_SPHEREDENSITY))) + 1;
  }

  if (ctSpheres == 0) {
    ctSpheres = 1;
  }

  // Calculate how far from each other to set sphere centers
  FLOAT fSphereCentersDistance;
  if (ctSpheres == 1) {
    fSphereCentersDistance = 0.0f;
  }
  else {
    fSphereCentersDistance = fSphereCentersSpan / (FLOAT)(ctSpheres - 1);
  }

  // Calculate coordinates for spreading sphere centers
  FLOAT fSphereCenterX = vBoxOffset(iAxis1);
  FLOAT fSphereCenterZ = vBoxOffset(iAxis2);
  FLOAT fSphereCenterY0 = vBoxOffset(iAxisMain) - (vBoxSize(iAxisMain) / 2.0f) + fSphereRadius;
  FLOAT fSphereCenterKY = fSphereCentersDistance;

  ci_fMinHeight = boxModel.Min()(2);
  ci_fMaxHeight = boxModel.Max()(2);
  ci_fHandleY = UpperLimit(1.0f);

  // Create needed number of spheres in the array
  ci_absSpheres.Clear();
  ci_absSpheres.New(ctSpheres);

  // For each sphere
  for (INDEX iSphere = 0; iSphere < ctSpheres; iSphere++)
  {
    CMovingSphere &ms = ci_absSpheres[iSphere];

    // Set its center and radius
    ms.ms_vCenter(iAxis1) = fSphereCenterX;
    ms.ms_vCenter(iAxis2) = fSphereCenterZ;
    ms.ms_vCenter(iAxisMain) = fSphereCenterY0 + iSphere * fSphereCenterKY;
    ms.ms_fR = fSphereRadius;
    ci_fHandleY = Min(ci_fHandleY, ms.ms_vCenter(2));
  }

  // Remember handle parameters
  if (ctSpheres == 1 || iBoxType == LENGTH_EQ_WIDTH)
  {
    ci_ulFlags |= CIF_CANSTANDONHANDLE;
    ci_fHandleR = fSphereRadius;
  }
  else {
    ci_fHandleR = 0.0f;
  }

  // Set optimization flags
  if (ctSpheres == 1 &&
    ci_absSpheres[0].ms_vCenter(1) == 0 &&
    ci_absSpheres[0].ms_vCenter(2) == 0 &&
    ci_absSpheres[0].ms_vCenter(3) == 0)
  {
    ci_ulFlags |= CIF_IGNOREROTATION;
  }

  if (iBoxType == LENGTH_EQ_WIDTH &&
    ci_absSpheres[0].ms_vCenter(1) == 0 &&
    ci_absSpheres[0].ms_vCenter(3) == 0)
  {
    ci_ulFlags |= CIF_IGNOREHEADING;
  }
}

// Create collision info for a ska model
void CCollisionInfo::FromSkaModel(CEntity *penModel, INDEX iBox)
{
}

// Create collision info for a brush.
void CCollisionInfo::FromBrush(CBrush3D *pbrBrush)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  ci_absSpheres.Clear();
  ci_absSpheres.New(1);
  ci_ulFlags = CIF_BRUSH;

  // Clear brush's relative box
  FLOATaabbox3D box;

  // Get first brush mip
  CBrushMip *pbm = pbrBrush->GetFirstMip();

  // For each sector in the brush mip
  FOREACHINDYNAMICARRAY(pbm->bm_abscSectors, CBrushSector, itbsc)
  {
    // For each vertex in the sector
    FOREACHINSTATICARRAY(itbsc->bsc_abvxVertices, CBrushVertex, itbvx)
    {
      CBrushVertex &bvx = *itbvx;

      // Add it to bounding box
      box |= DOUBLEtoFLOAT(bvx.bvx_vdPreciseRelative);
    }
  }

  // Create a sphere from the relative box
  ci_absSpheres[0].ms_vCenter = box.Center();
  ci_absSpheres[0].ms_fR = box.Size().Length() / 2;
  ci_fMinHeight = UpperLimit(1.0f);
  ci_fMaxHeight = LowerLimit(1.0f);
  ci_fHandleY = 0.0f;
  ci_fHandleR = 1.0f;
}

// Calculate current bounding box in absolute space from position.
void CCollisionInfo::MakeBoxAtPlacement(const FLOAT3D &vPosition, const FLOATmatrix3D &mRotation, FLOATaabbox3D &box)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  CMovingSphere &ms0 = ci_absSpheres[0];
  CMovingSphere &ms1 = ci_absSpheres[ci_absSpheres.Count() - 1];
  box = FLOATaabbox3D(vPosition + ms0.ms_vCenter * mRotation, ms0.ms_fR);
  box |= FLOATaabbox3D(vPosition + ms1.ms_vCenter * mRotation, ms1.ms_fR);
}

// Get maximum radius of entity in xz plane (relative to entity handle)
FLOAT CCollisionInfo::GetMaxFloorRadius(void)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Get first and last sphere
  CMovingSphere &ms0 = ci_absSpheres[0];
  CMovingSphere &ms1 = ci_absSpheres[ci_absSpheres.Count() - 1];

  // Get their positions in xz plane
  FLOAT3D vPosXZ0 = ms0.ms_vCenter;
  FLOAT3D vPosXZ1 = ms1.ms_vCenter;
  vPosXZ0(2) = 0.0f;
  vPosXZ1(2) = 0.0f;

  // Return maximum distance from the handle in xz plane
  return Max(vPosXZ0.Length() + ms0.ms_fR, vPosXZ1.Length() + ms1.ms_fR);
}