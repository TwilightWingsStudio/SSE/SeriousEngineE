/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CLIPTEST_H
#define SE_INCL_CLIPTEST_H

#include <Engine/Entities/Collision/CollisionInfo.h>

//! Class for testing collisions between two entities.
class CClipTest 
{
  public:
    CEntity *ct_penEntity;        // the entity
    CEntity *ct_penObstacle;      // obstacle entity (if cannot change)

    INDEX ct_iNewCollisionBox;    // index of new collision box to set
    CCollisionInfo ct_ciNew;      // collision info with new box
    FLOATaabbox3D ct_boxTotal;    // union box for old and new in absolute coordinates

    CListHead ct_lhActiveSectors; // sectors that may be of interest

  public:
    //! Destructor.
    ~CClipTest(void);

    //! Test if a point touches sphere.
    BOOL PointTouchesSphere(const FLOAT3D &vPoint, const FLOAT3D &vSphereCenter, const FLOAT fSphereRadius);
    
    //! Test if a point touches cylinder.
    BOOL PointTouchesCylinder(const FLOAT3D &vPoint, const FLOAT3D &vCylinderBottomCenter,
                              const FLOAT3D &vCylinderTopCenter, const FLOAT fCylinderRadius);
      
    //! Project spheres of a collision info to given placement.
    void ProjectSpheresToPlacement(CCollisionInfo &ci, FLOAT3D &vPosition, FLOATmatrix3D &mRotation);

    //! Test if a sphere touches brush polygon.
    BOOL SphereTouchesBrushPolygon(const CMovingSphere &msMoving, CBrushPolygon *pbpoPolygon);
    
    //! Test if entity touches brush polygon.
    BOOL EntityTouchesBrushPolygon(CBrushPolygon *pbpoPolygon);
    
    //! Test if an entity can change to a new collision box without intersecting anything.
    BOOL CanChange(CEntity *pen, INDEX iNewCollisionBox);

};

#endif  /* include-once check. */