/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MOVINGSPHERE_H
#define SE_INCL_MOVINGSPHERE_H

//! Bounding sphere used for movement clipping.
class ENGINE_API CMovingSphere
{
  public:
    FLOAT3D ms_vCenter;    // sphere center in space of moving entity
    FLOAT ms_fR;           // sphere radius

    FLOAT3D ms_vRelativeCenter0; // start point of sphere center in space of standing entity
    FLOAT3D ms_vRelativeCenter1; // end point of sphere center in space of standing entity
    FLOATaabbox3D ms_boxMovement; // the movement path in space of standing entity
};

#endif  /* include-once check. */