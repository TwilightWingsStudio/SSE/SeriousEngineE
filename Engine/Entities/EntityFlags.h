/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENTITYFLAGS_H
#define SE_INCL_ENTITYFLAGS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Flags determining whether some entity is active in some game type or difficulty level.
enum SpawnFlags
{
  //! Active at easy difficulty.
  SPF_EASY    = (1L << 0),

  //! Active at normal difficulty.  
  SPF_NORMAL  = (1L << 1),
  
  //! Active at hard difficulty.
  SPF_HARD    = (1L << 2),
  
  //! Active at extreme difficulty.
  SPF_EXTREME = (1L << 3),
  
  //! Active at tourist difficulty.
  SPF_TOURIST = (1L << 4),
  
  //! Mask for difficulty level flags.
  SPF_MASK_DIFFICULTY = 0x0000FFFFL,

  //! Active in single player mode.
  SPF_SINGLEPLAYER = (1L << 16),

  //! Active in deathmatch mode.
  SPF_DEATHMATCH   = (1L << 17),

  //! Active in cooperative mode.
  SPF_COOPERATIVE  = (1L << 18),

  //! Active in flyover (camera) mode.
  SPF_FLYOVER      = (1L << 19),

  //! Mask for game type flags.
  SPF_MASK_GAMEMODE = 0xFFFF0000L,
};


//! Various other entity flags.
enum EntityFlags
{
  //! Set if selected.
  ENF_SELECTED          = (1L << 0),  // set if selected
  
  //! Brush that defines spatial classification.
  ENF_ZONING            = (1L << 1),

  //! Set if the entity does not exist anymore.
  ENF_DELETED           = (1L << 2),

  //! Set if the entity is currently a living being.
  ENF_ALIVE             = (1L << 3),

  //! Set if the entity is currently active in rendering.
  ENF_INRENDERING       = (1L << 4),

  //! Set if shading info is valid.
  ENF_VALIDSHADINGINFO  = (1L << 5),

  //! Set if cast ray can see through.
  ENF_SEETHROUGH        = (1L << 6),

  //! Set if the entity is already found in grid search.
  ENF_FOUNDINGRIDSEARCH = (1L << 7),

  //! Model that has cluster shadows.
  ENF_CLUSTERSHADOWS    = (1L << 8),

  //! Brush or model that is used for background rendering.
  ENF_BACKGROUND        = (1L << 9),

  //! Set if cannot be moved in editor without special allowance.
  ENF_ANCHORED          = (1L << 10),

  //! Entity renders particles.
  ENF_HASPARTICLES      = (1L << 11),

  //! Entity is invisible (for AI purposes).
  ENF_INVISIBLE         = (1L << 12),

  //! Moving brush that causes automatic shadow recalculation.
  ENF_DYNAMICSHADOWS    = (1L << 13),

  //! Entity is notified when level is changed.
  ENF_NOTIFYLEVELCHANGE = (1L << 14),

  //! Entity must be carried when level is changed.
  ENF_CROSSESLEVELS     = (1L << 15),

  //! This entity can be predicted.
  ENF_PREDICTABLE       = (1L << 16),

  //! This entity is predictor for another entity.
  ENF_PREDICTOR         = (1L << 17),

  //! This entity has its predictor.
  ENF_PREDICTED         = (1L << 18),

  //! This entity will be predicted.
  ENF_WILLBEPREDICTED   = (1L << 19),

  //! Predictor that was spawned during prediction (doesn't have a predictor).
  ENF_TEMPPREDICTOR     = (1L << 20),

  //! Set if the entity is hidden (for editing).
  ENF_HIDDEN            = (1L << 21),

  //! The entity doesn't need FindShadingInfo(), it will set its own shading.
  ENF_NOSHADINGINFO     = (1L << 22),

  // [SEE]
  ENF_NEWHEALTH         = (1L << 30),
};

// Entity physics flags.
enum EntityPhysicsFlags
{
  //! Set if gravity influences its orientation.
  EPF_ORIENTEDBYGRAVITY   = (1UL << 0),

  //! Set if gravity can move it.
  EPF_TRANSLATEDBYGRAVITY = (1UL << 1),

  //! Set if can be pushed by other objects.
  EPF_PUSHABLE            = (1UL << 2),

  //! Entity always falls to nearest surface.
  EPF_STICKYFEET          = (1UL << 3),

  //! Set if rotation and translation are synchronized.
  EPF_RT_SYNCHRONIZED     = (1UL << 4),

  //! Set if entity is translated absolute and not relative to its position.
  EPF_ABSOLUTETRANSLATE   = (1UL << 5),

  //! Set if entity can change its speed immediately.
  EPF_NOACCELERATION      = (1UL << 6),

  //! Set if entity has lungs.
  EPF_HASLUNGS            = (1UL << 7),

  //! Set if entity has gills.
  EPF_HASGILLS            = (1UL << 8),

  //! Set if derived from CMovableEntity.
  EPF_MOVABLE             = (1UL << 9),

  //! Entities are not damaged when hitting this one.
  EPF_NOIMPACT            = (1UL << 10),

  //! This one is not damaged by impact this tick.
  EPF_NOIMPACTTHISTICK    = (1UL << 11),

  //! Desired rotation can be reduced by contents (like water).
  EPF_CANFADESPINNING     = (1UL << 12),

  //! While sliding down a steep slope (valid only if entity has reference).
  EPF_ONSTEEPSLOPE        = (1UL << 13),

  //! While beeing re-oriented by gravity.
  EPF_ORIENTINGTOGRAVITY  = (1UL << 14),

  //! While bouyancy causes floating in fluid.
  EPF_FLOATING            = (1UL << 15),

  //! Set if force-added to movers.
  EPF_FORCEADDED          = (1UL << 16),

  //! Mask that defines behavior on collision.
  EPF_ONBLOCK_MASK         = (7UL << 29),

  //! Stop moving.
  EPF_ONBLOCK_STOP         = (0UL << 29),

  //! Slide along.
  EPF_ONBLOCK_SLIDE        = (1UL << 29),

  //! Clim up a stair or slide along.
  EPF_ONBLOCK_CLIMBORSLIDE = (2UL << 29),

  //! Bounce off.
  EPF_ONBLOCK_BOUNCE       = (3UL << 29),

  //! Push the obstacle.
  EPF_ONBLOCK_PUSH         = (4UL << 29),

  //! Stop moving, but exactly at collision position.
  EPF_ONBLOCK_STOPEXACT    = (5UL << 29),
};

// entity collision flags are divided in 3 groups
#define ECB_COUNT 10    // max number of flags per group
#define ECF_MASK  ((1<<ECB_COUNT)-1)  // mask for one group
// what an entity is
#define ECB_IS      0
#define ECF_ISMASK  (ECF_MASK<<ECB_IS)
// which other entities to test with
#define ECB_TEST    10
#define ECF_TESTMASK  (ECF_MASK<<ECB_TEST)
// which tested entities to pass through
#define ECB_PASS    20
#define ECF_PASSMASK  (ECF_MASK<<ECB_PASS)

// optimizations to completely ignore some types of entities
#define ECF_IGNOREBRUSHES  (1UL<<30)
#define ECF_IGNOREMODELS   (1UL<<31)

#endif  /* include-once check. */