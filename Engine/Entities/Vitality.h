/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_VITALITY_H
#define SE_INCL_VITALITY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum VitalityType
{
  //! Health Points.
  VT_HEALTH = 0,

  //! Armor Points.
  VT_ARMOR,

  //! Shield Points.
  VT_SHIELD, 
};

class ENGINE_API IVitality
{
  public:
    //! Apply some damage.
    virtual void ReceiveDamage(CEntity *penInflictor, INDEX iDamageType, INDEX iDamageAmmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection) {};

    //! Apply specific vitality points .
    virtual BOOL ReceiveVitality(VitalityType eType, INDEX iRestoreAmount, INDEX iRestoreMax)
    {
      const INDEX iStartValue = GetVitality(eType);

      if (iStartValue < iRestoreMax) {
        SetVitality(eType, Min(iRestoreMax, iStartValue + iRestoreAmount));
        return TRUE;
      }

      return FALSE;
    }

    //! Apply health points.
    virtual BOOL ReceiveHealth(INDEX iRestoreAmount, INDEX iRestoreMax)
    {
      return ReceiveVitality(VT_HEALTH, iRestoreAmount, iRestoreMax);
    };

    //! Apply armor points.
    virtual BOOL ReceiveArmor(INDEX iRestoreAmount, INDEX iRestoreMax)
    {
      return ReceiveVitality(VT_ARMOR, iRestoreAmount, iRestoreMax);
    };

    //! Apply shield points.
    virtual BOOL ReceiveShield(INDEX iRestoreAmount, INDEX iRestoreMax)
    {
      return ReceiveVitality(VT_SHIELD, iRestoreAmount, iRestoreMax);
    };

    //! Change specified vitality points.
    virtual void SetVitality(VitalityType eType, INDEX iPoints) {};

    //! Change health points.
    virtual void SetHealth(INDEX iPoints)
    {
      SetVitality(VT_HEALTH, iPoints);
    };
    
    //! Change armor points.
    virtual void SetArmor(INDEX iPoints)
    {
      SetVitality(VT_ARMOR, iPoints);
    };

    //! Change shield points.
    virtual void SetShield(INDEX iPoints)
    {
      SetVitality(VT_SHIELD, iPoints);
    };

    // Returns selected vitality points.
    virtual INDEX GetVitality(VitalityType eType) const
    {
      return 0;
    }

    //! Returns health points.
    virtual INDEX GetHealth(void) const
    {
      return GetVitality(VT_HEALTH);
    }

    //! Returns armor points.
    virtual INDEX GetArmor(void) const
    {
      return GetVitality(VT_ARMOR);
    }

    //! Returns shield points.
    virtual INDEX GetShield(void) const
    {
      return GetVitality(VT_SHIELD);
    }

    //! Use only for initialization!
    //! NOTE: This is compatability hack between float-based and integer-based damage systems.
    virtual void SetHealthInit(INDEX iHealth) {};
};

#endif  /* include-once check. */
