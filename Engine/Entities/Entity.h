/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENTITY_H
#define SE_INCL_ENTITY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif


#include <Core/Base/Relations.h>
#include <Core/Base/Variant.h>
#include <Core/Templates/StaticStackArray.h>
#include <Core/Templates/Selection.h>
#include <Core/Math/Matrix.h>
#include <Core/Math/AABBox.h>
#include <Core/Math/Placement.h>
#include <Core/Objects/Node.h>
#include <Engine/Entities/EntityEvent.h>
#include <Engine/Entities/EntityFlags.h>
#include <Engine/Entities/StateStack.h>
#include <Engine/Entities/Vitality.h>
#include <Engine/Meshes/ModelInstance.h>


#define DUMPVECTOR(v) \
  strm.FPrintF_t(#v ":  %g,%g,%g %08x,%08x,%08x\n", \
    (v)(1), (v)(2), (v)(3), (ULONG&)(v)(1), (ULONG&)(v)(2), (ULONG&)(v)(3))
#define DUMPVECTOR2(strDes, v) \
  strm.FPrintF_t("%s:  %g,%g,%g\n", strDes, (v)(1), (v)(2), (v)(3))
#define DUMPLONG(l) \
  strm.FPrintF_t(#l ":  %08x\n", l)
#define DUMPPLACEMENT(plname, pl)        \
  strm.FPrintF_t("%s:\n", plname);       \
  DUMPVECTOR2("Position", pl.pl_PositionVector);    \
  DUMPVECTOR2("Orientation", pl.pl_OrientationAngle);

//! Force infulence at a point in space.
class CForceStrength 
{
  public:
    FLOAT3D fs_vDirection;    // direction of the force (must be normalized)
    FLOAT fs_fAcceleration;   // acceleration of the force (m/s2) (along the direction)
    FLOAT fs_fVelocity;       // max. velocity that force can give (m/s) (along the direction)
};

// Entity Pointer stub. It's included after class definition.
class CEntityPointer;

// Selections of entities
typedef TSelection<CEntity, ENF_SELECTED> CEntitySelection;


//! General structure of an entity instance.
class ENGINE_API CEntity : public CNode, public IStateStack, public IVitality
{
  public:
    // type of function pointer used as AI event handler
    typedef BOOL (CEntity::*pEventHandler)(const CEntityEvent &ee);

    enum RenderType 
    {
      RT_ILLEGAL     = 1,
      RT_NONE        = 2,     // not rendered ever -- used internally
      RT_MODEL       = 3,     // drawn as model
      RT_BRUSH       = 4,     // rendered as brush
      RT_EDITORMODEL = 5,     // rendered as model, but only in editor
      RT_VOID        = 7,     // not rendered ever
      RT_FIELDBRUSH  = 8,     // brush used for field effects (like triggers, force fields etc.)
      RT_SKAMODEL    = 9,     // render as ska model
      RT_SKAEDITORMODEL = 10, // render as ska model, but only in editor
      RT_TERRAIN        = 11, // render as terrain
    }; 

  public:
    enum RenderType en_RenderType;  // how is it rendered
    ULONG en_ulPhysicsFlags;        // ways of interacting with environment
    ULONG en_ulCollisionFlags;      // which entities to collide with

    ULONG en_ulFlags;               // various flags
    ULONG en_ulSpawnFlags;          // in what game types is this entity active
    ULONG en_ulID;                  // unique entity identifier

    CPlacement3D en_plPlacement;    // placement in world space
    FLOATmatrix3D en_mRotation;     // precalc. matrix for object rotation
    CEntityClass *en_pecClass;      // class used to construct this entity
    
    union                           // rendering object
    {   
      CBrush3D *en_pbrBrush;            // brush -- if brush entity
      CModelObject *en_pmoModelObject;  // model -- if model entity
      CModelInstance *en_pmiModelInstance; // ska model -- if ska model entity
      CTerrain *en_ptrTerrain;          // terrain -- if terrain entity
    };
    
    CShadingInfo *en_psiShadingInfo;      // shading info for model entities
    CCollisionInfo *en_pciCollisionInfo;  // collision info for movable colliding entities
    FLOAT en_fSpatialClassificationRadius;  // radius for spatial classification
    FLOATaabbox3D en_boxSpatialClassification;  // box in object space for spatial classification
    CLastPositions *en_plpLastPositions;    // last positions of entity

    class CWorld *en_pwoWorld;      // the world this entity belongs to

    CRelationDst en_rdSectors;      // relation to sectors this entity is in

    CPlacement3D en_plRelativeToParent;   // placement relative to parent placement

  public:
    // reference counting functions
    inline void AddReference(void);
    inline void RemReference(void);

    //! Get pointer to entity property from its packed identifier.
    class CEntityProperty *PropertyForId(ULONG ulId, ULONG ulType = 0);
    
    //! Helpers for writing/reading entity pointers.
    void ReadEntityPointer_t(CTStream *istr, CEntityPointer &pen);
    void WriteEntityPointer_t(CTStream *ostr, CEntityPointer pen);

    //! Get pointer to entity resource from its packed identifier.
    class CEntityResource *ResourceForId(ULONG ulId, ULONG ulType = 0);
    
    //! Get pointer to entity property from its name.
    class CEntityProperty *PropertyForName(const CTString &strPropertyName);
    
    //! Copy one entity property from property of another entity.
    void CopyOneProperty(CEntityProperty &epPropertySrc, CEntityProperty &epPropertyDest,
                          CEntity &enOther, ULONG ulFlags);

    //! Read all properties from a stream.
    void ReadProperties_t(CTStream &istrm);  // throw char *
    
    //! Write all properties to a stream.
    void WriteProperties_t(CTStream &ostrm); // throw char *
    
    //! Copy entity properties from another entity of same class.
    void CopyEntityProperties(CEntity &enOther, ULONG ulFlags);

    //! Internal function for entity reinitialization (do not discard shadows etc.).
    void Initialize_internal(const CEntityEvent &eeInput);

    //! Clean-up entity internally.
    void End_internal(void);
    
    //! Reinitialize the entity.
    void Reinitialize(void);

    //! Internal repositioning function.
    virtual void SetPlacement_internal(const CPlacement3D &plNew, const FLOATmatrix3D &mRotation,
      BOOL bNear);
    
    //! Uncache shadows of each polygon in entity that has given gradient index.
    void UncacheShadowsForGradient(INDEX iGradient);

    //! Find and remember shading info for this entity if invalid.
    void FindShadingInfo(void);
    
    //! Find and remember collision info for this entity.
    void FindCollisionInfo(void);
    
    //! Discard collision info for this entity.
    void DiscardCollisionInfo(void);
    
    //! Copy collision info from some other entity.
    void CopyCollisionInfo(CEntity &enOrg);
    
    //! Update range used for spatial clasification.
    void UpdateSpatialRange(void);
    
    //! Find and remember all sectors that this entity is in.
    void FindSectorsAroundEntity(void);

    //! Find and remember all sectors that this entity is in.
    void FindSectorsAroundEntityNear(void);

    //! Add entity to collision grid.
    void AddToCollisionGrid(void);

#define COPY_REMAP      (1UL<<0)  // remap pointers
#define COPY_REINIT     (1UL<<1)  // reinit entity
#define COPY_PREDICTOR  (1UL<<2)  // make predictor (complete raw copy with all states/variables and making predictor/predicted links)

    //! Copy entity from another entity of same class. (NOTE: this doesn't copy placement!)
    virtual void Copy(CEntity &enOther, ULONG ulFlags);
    
    virtual CEntity &operator=(CEntity &enOther) {ASSERT(FALSE); return *this;};
    
    //! Find a pointer to another entity while copying.
    static CEntity *FindRemappedEntityPointer(CEntity *penOriginal);
    
    //! Read from stream.
    virtual void Read_t(CTStream *istr);  // throw char *
    
    //! Write to stream.
    virtual void Write_t(CTStream *ostr); // throw char *
    
    //! Precache resources that might be needed.
    virtual void Precache(void);
    
    //! Create a checksum value for sync-check
    virtual void ChecksumForSync(ULONG &ulCRC, INDEX iExtensiveSyncCheck);
    
    //! Dump sync data to text file.
    virtual void DumpSync_t(CTStream &strm, INDEX iExtensiveSyncCheck);  // throw char *

    //! Handle all sent events.
    static void HandleSentEvents(void);

    //! Find entities in a box (box must be around this entity).
    void FindEntitiesInRange(const FLOATaabbox3D &boxRange, TDynamicContainer<CEntity> &cen, BOOL bCollidingOnly);
      
    //! Find first entity touching a field (this entity must be a field brush).
    CEntity *TouchingEntity(BOOL (*ConsiderEntity)(CEntity *), CEntity *penHintMaybeInside);

  public:
    // DLL class interface
    
    //! Initialize for being virtual entity that is not rendered.
    void InitAsVoid(void);
    
    //! Initialize for beeing a model object.
    void InitAsModel(void);
    void InitAsSkaModel(void);
    
    //! Initialize for beeing a terrain object.
    void InitAsTerrain(void);

    //! Initialize for beeing an editor model object.
    void InitAsEditorModel(void);

    //! Initialize for beeing an ska editor model object.
    void InitAsSkaEditorModel(void);
    
    //! Initialize for beeing a brush object.
    void InitAsBrush(void);
    
    //! Initialize for beeing a field brush object.
    void InitAsFieldBrush(void);
    
    //! Switch to model.
    void SwitchToModel(void);

    //! Switch to editor model.
    void SwitchToEditorModel(void);

    //! Set all properties to default values. - overridden by ecc.
    virtual void SetDefaultProperties(void);

    //! Get a filename for a resource of given type and id.
    const CTFileName &FileNameForResource(SLONG slType, SLONG slID);
    
    //! Get data for a texture resource.
    CTextureData *GetTextureDataForResource(SLONG slID);
    
    //! Get data for a model resource.
    CModelData *GetModelDataForResource(SLONG slID);

    // model manipulation functions -- only for RT_MODEL/RT_EDITORMODEL
    //! Set the model data for model entity.
    void SetModel(const CTFileName &fnmModel);

    //! Set the model data for model entity.
    void SetModel(SLONG idModelResource);
    BOOL SetSkaModel(const CTString &fnmModel);
    void SetSkaModel_t(const CTString &fnmModel);

    void SetAnyModel(SLONG idModelConfiguration);
    void SetAnyModel(CAbstractModelInstance &mi, SLONG idModelConfiguration);
    
    void SetSkaCollisionInfo();
    
    //! Get the model data for model entity.
    const CTFileName &GetModel(void);
    
    //! Start new animation for model entity.
    void StartModelAnim(INDEX iNewModelAnim, ULONG ulFlags);

    //! Play a given sound object.
    void PlaySound(CSoundObject &so, SLONG idSoundResource, SLONG slPlayType);

    //! Play a given sound object.
    void PlaySound(CSoundObject &so, const CTFileName &fnmSound, SLONG slPlayType);
    
    //! Get sound length from the resource.
    double GetSoundLength(SLONG idSoundResource);

    //! Set model main blend color
    COLOR GetModelColor(void) const;
    
    //! Get model main blend color.
    void  SetModelColor(const COLOR colBlend);

    //! Set the main texture data for model entity.
    void SetModelMainTexture(SLONG idTextureResource);

    //! Set the main texture data for model entity.
    void SetModelMainTexture(const CTFileName &fnmTexture);
    
    //! Get the main texture data for model entity.
    const CTFileName &GetModelMainTexture(void);
    
    //! Start new animation for main texture of model entity.
    void StartModelMainTextureAnim(INDEX iNewTextureAnim);

    //! Set the reflection texture data for model entity.
    void SetModelReflectionTexture(SLONG idTextureResource);
    
    //! Set the specular texture data for model entity.
    void SetModelSpecularTexture(SLONG idTextureResource);

    //! Add attachment to model.
    void AddAttachment(INDEX iAttachment, ULONG ulIDModel, ULONG ulIDTexture);

    //! Add attachment to model.
    void AddAttachment(INDEX iAttachment, CTFileName fnModel, CTFileName fnTexture);
    
    //! Remove attachment from model.
    void RemoveAttachment(INDEX iAttachment);
    
    //! Set the reflection texture data for model attachment entity.
    void SetModelAttachmentReflectionTexture(INDEX iAttachment, SLONG idTextureResource);
    
    //! Set the specular texture data for model attachment entity.
    void SetModelAttachmentSpecularTexture(INDEX iAttachment, SLONG idTextureResource);
    
    //! Get all vertices of model entity in absolute space.
    void GetModelVerticesAbsolute(TStaticStackArray<FLOAT3D> &avVertices, FLOAT fNormalOffset, FLOAT fMipFactor);
    
    //! Returns true if bone exists and sets two given vectors as start and end point of specified bone in abs space.
    BOOL GetBoneAbsPosition(INDEX iBoneID, FLOAT3D &vStartPoint, FLOAT3D &vEndPoint);
    
    //! Returns true if bone exists and sets two given vectors as start and end point of specified bone in relative space.
    BOOL GetBoneRelPosition(INDEX iBoneID, FLOAT3D &vStartPoint, FLOAT3D &vEndPoint);
    
    //! Callback function for aditional bone adjustment.
    virtual void AdjustBones();
    
    //! Callback function for aditional shader params adjustment.
    virtual void AdjustShaderParams(INDEX iSurfaceID,CShaderClass *pShader,ShaderParams &spParams);

    //! Precache 'model' resource.
    void PrecacheModel(SLONG slID);

    //! Precache 'modelcfg' resource.
    void PrecacheModelConfig(SLONG slID);

    //! Precache 'texture' resource.
    void PrecacheTexture(SLONG slID);

    //! Precache 'sound' resource.
    void PrecacheSound(SLONG slID);

    //! Precache 'class' resource.
    void PrecacheClass(SLONG slID, INDEX iUser = -1);

    //! Create a new entity of given class in this world.
    CEntity *CreateEntity(const CPlacement3D &plPlacement, SLONG idClassResource);

    //! Apply some damage directly to one entity.
    void InflictDirectDamage(CEntity *penToDamage, CEntity *penInflictor, INDEX iDamageType,
                             INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection);
    
    //! Apply some damage to all entities in some range (this tests for obstacles).
    void InflictRangeDamage(CEntity *penInflictor, INDEX iDamageType,
                            INDEX iDamageAmount, const FLOAT3D &vCenter, FLOAT fHotSpotRange, FLOAT fFallOffRange);
    
    //! Apply some damage to all entities in a box (this doesn't test for obstacles).
    void InflictBoxDamage(CEntity *penInflictor, INDEX dmtType,
      INDEX iDamageAmount, const FLOATaabbox3D &box);

    //! Notify engine that gravity defined by this entity has changed.
    void NotifyGravityChanged(void);
    
    //! Notify engine that collision of this entity was changed.
    void NotifyCollisionChanged(void);

    //! Get a pseudo-random integer number (safe for network gaming).
    ULONG IRnd(void);   // [0x0000 , 0xFFFF]

    //! Get a pseudo-random decimal number (safe for network gaming).
    FLOAT FRnd(void);   // [0.0f , 1.0f]

    // DLL class overridables
    //! Called after creating and setting its properties.
    virtual void OnInitialize(const CEntityEvent &eeInput);
    
    //! Called before releasing entity.
    virtual void OnEnd(void);

    //! Return opacity of the entity (1 is default).
    virtual FLOAT GetOpacity(void) { return 1.0f; };

    //! Returns ammount of memory used by entity.
    virtual SLONG GetUsedMemory(void);

  public:
    // construction/destruction
    //! Default constructor.
    CEntity(void);
    
    //! Destructor.
    virtual ~CEntity(void);
    
    //! Clear the object.
    void Clear(void) {};

    //! Entities can be selected
    IMPLEMENT_SELECTING(en_ulFlags);

    // access functions
    //! Prepare entity (call after setting properties).
    void Initialize(const CEntityEvent &eeInput = _eeVoid);
    
    //! Clean-up entity.
    void End(void);

    //! Destroy this entity (entity must not be targetable).
    void Destroy(void);

    //! Get state transition for given state and event code.
    virtual CEntity::pEventHandler HandlerForStateAndEvent(SLONG slState, SLONG slEvent);
    
    //! Handle an event, return false if the event is not handled.
    virtual BOOL HandleEvent(const CEntityEvent &ee);

    //! Set placement of this entity. (use only in WEd!)
    void SetPlacement(const CPlacement3D &plNew);
    
    //! Fall down to floor. (use only in WEd!)
    void FallDownToFloor(void);

    //! Get placement of this entity.
    inline const CPlacement3D &GetPlacement(void) const { return en_plPlacement; };

    //! Get rotation matrix of this entity.
    inline const FLOATmatrix3D &GetRotationMatrix(void) const { return en_mRotation; };
    
    //! This one is used in rendering - gets lerped placement between ticks.
    virtual CPlacement3D GetLerpedPlacement(void) const;
    
    //! Find first sector that entity is in.
    CBrushSector *GetFirstSector(void);
    
    //! Find first sector that entity is in (for UI purpuses).
    CBrushSector *GetFirstSectorWithName(void);

    //! Teleport this entity to a new location -- takes care of telefrag damage.
    void Teleport(const CPlacement3D &plNew, BOOL bTelefrag=TRUE);

    //! Sets common entity flags.
    void SetFlags(ULONG ulFlags);

    //! Returns common entity flags.
    inline ULONG GetFlags(void) const { return en_ulFlags; };

    //! Sets entity spawn flags.
    inline void SetSpawnFlags(ULONG ulFlags) { en_ulSpawnFlags = ulFlags; }

    //! Returns entity spawn flags.
    inline ULONG GetSpawnFlags(void) const { return en_ulSpawnFlags; };

    //! Sets entity physics flags.
    void SetPhysicsFlags(ULONG ulFlags);

    //! Returns entity physics flags.
    inline ULONG GetPhysicsFlags(void) const { return en_ulPhysicsFlags; };

    //! Sets entity collision flags.
    void SetCollisionFlags(ULONG ulFlags);

    //! Returns entity collision flags.
    inline ULONG GetCollisionFlags(void) const { return en_ulCollisionFlags; };
    
    //! Check if this is a predictor for an entity.
    inline BOOL IsPredictor(void) const { return en_ulFlags&ENF_PREDICTOR; };
    
    //! Check if this entity is being predicted.
    inline BOOL IsPredicted(void) const { return en_ulFlags&ENF_PREDICTED; };
    
    //! Check if this entity can be predicted.
    inline BOOL IsPredictable(void) const { return en_ulFlags&ENF_PREDICTABLE; };
    
    //! Get this entity's predictor.
    CEntity *GetPredictor(void);
    
    //! Get predicted entity of this entity.
    CEntity *GetPredicted(void);
    
    //! Become predictable/unpredictable
    void SetPredictable(BOOL bON);
    
    //! Check if this instance is head of prediction chain.
    BOOL IsPredictionHead(void);
    
    //! Get the prediction original (predicted), or self if not predicting.
    CEntity *GetPredictionTail(void);
    
    //! Check if active for prediction now.
    BOOL IsAllowedForPrediction(void) const;
    
    //! Check an event for prediction, returns true if already predicted.
    BOOL CheckEventPrediction(ULONG ulTypeID, ULONG ulEventID);

    //! Returns entity render type.
    inline enum RenderType GetRenderType(void) { return en_RenderType; };

    //! Returns entity class instance.
    inline CEntityClass *GetClass(void) { return en_pecClass; };

    //! Returns the world instance entity in.
    inline CWorld *GetWorld(void) { return en_pwoWorld; };

    //! Returns the brush instance entity standing on.
    inline CBrush3D *GetBrush(void) { return en_pbrBrush; };

    //! Returns CModelObject instance used for rendering.
    inline CModelObject *GetModelObject(void) { return en_pmoModelObject; };

    //! Returns CModelInstance instance used for rendering.
    inline CModelInstance *GetModelInstance(void) { return en_pmiModelInstance; };
    
    //! Returns CTerrain instance used for rendering.
    inline CTerrain *GetTerrain(void) { return en_ptrTerrain; };
    
    //! Get parent of this entity.
    inline CEntity *GetParentEntity()
    {
      return static_cast<CEntity *>(n_pParent);
    }
    
    //! Parent this entity to another entity.
    virtual void SetParent(CNode *pNewParent);

    //! Find first child of given class.
    CEntity *GetChildOfClass(const char *strClass);

    //! Test if the entity is an empty brush.
    BOOL IsEmptyBrush(void) const;

    //! Return max Game Players.
    static INDEX GetMaxPlayers(void);

    static void GetPlayerControllers(TDynamicContainer<CEntity> &cenOutput);
    static void GetPlayerEntities(TDynamicContainer<CEntity> &cenOutput);

    //! Return Player Entity.
    static CEntity *GetPlayerController(INDEX iPlayer);
    static CEntity *GetPlayerEntity(INDEX iPlayer);

    //! Get bounding box of this entity - for AI purposes only.
    void GetBoundingBox(FLOATaabbox3D &box);
    
    //! Get size of this entity - for UI purposes only.
    void GetSize(FLOATaabbox3D &box);
    
    //! Get last positions structure for particles.
    CLastPositions *GetLastPositions(INDEX ctPositions);
    
    //! Get nearest position of nearest brush polygon to this entity if available.
    CBrushPolygon *GetNearestPolygon(FLOAT3D &vPoint, FLOATplane3D &plPlane, FLOAT &fDistanceToEdge);
    
    //! Get absolute position of point on entity given relative to its size.
    void GetEntityPointRatio(const FLOAT3D &vRatio, FLOAT3D &vAbsPoint, BOOL bLerped=FALSE);
    
    //! Get absolute position of point on entity given in meters.
    void GetEntityPointFixed(const FLOAT3D &vFixed, FLOAT3D &vAbsPoint);
    
    //! Get sector that given point is in - point must be inside this entity.
    CBrushSector *GetSectorFromPoint(const FLOAT3D &vPointAbs);

    //! Map world polygon to/from indices
    CBrushPolygon *GetWorldPolygonPointer(INDEX ibpo);
    
    //! Get index of some brush polygon in the world.
    INDEX GetWorldPolygonIndex(CBrushPolygon *pbpo);
    
    //! Returns some more verbose data.
    virtual const CTString &GetDescription(void) const;
    
    //! Get first target of this entity.
    virtual CEntity *GetTarget(void) const;

    //! List all entities connected in chain (i.e. markers). Returns FALSE if chain is recursive.
    virtual BOOL GetTargetChain(TDynamicContainer<CEntity> &cenOutput) const;
    
    //! Check if entity can be used as a target.
    virtual BOOL IsTargetable(void) const;
    
    //! Check if entity is marker
    virtual BOOL IsMarker(void) const;
    
    //! Check if entity is pawn.
    virtual BOOL IsPawn(void) const;
    
    //! Check if entity is important
    virtual BOOL IsImportant(void) const;
    
    //! Check if entity is moved on a route set up by its targets.
    virtual BOOL MovesByTargetedRoute(CTString &strTargetProperty) const;
    
    //! Check if entity can drop marker for making linked route.
    virtual BOOL DropsMarker(CTFileName &fnmMarkerClass, CTString &strTargetProperty) const;
    
    //! Get light source information - return NULL if not a light source.
    virtual CLightSource *GetLightSource(void);
    
    //! Is target valid.
    virtual BOOL IsTargetValid(SLONG slPropertyOffset, CEntity *penTarget);

    //! Get force type name, return empty string if not used.
    virtual const CTString &GetForceName(INDEX iForce);
    
    //! Get forces in given point.
    virtual void GetForce(INDEX iForce, const FLOAT3D &vPoint, CForceStrength &fsGravity, CForceStrength &fsField);
    
    //! Get entity that controls the force, used for change notification checking.
    virtual CEntity *GetForceController(INDEX iForce);

    //! Get fog type name, return empty string if not used.
    virtual const CTString &GetFogName(INDEX iFog);
    
    //! Get fog, return FALSE for none.
    virtual BOOL GetFog(INDEX iFog, class CFogParameters &fpFog);
    
    //! Get haze type name, return empty string if not used.
    virtual const CTString &GetHazeName(INDEX iHaze);
    
    //! Get haze, return FALSE for none.
    virtual BOOL GetHaze(INDEX iHaze, class CHazeParameters &hpHaze, FLOAT3D &vViewDir);

    //! Get mirror type name, return empty string if not used.
    virtual const CTString &GetMirrorName(INDEX iMirror);
    
    //! Get mirror, return FALSE for none.
    virtual BOOL GetMirror(INDEX iMirror, class CMirrorParameters &mpMirror);
    
    //! Get gradient type name, return empty string if not used.
    virtual const CTString &GetGradientName(INDEX iGradient);
    
    //! Get gradient, return FALSE for none.
    virtual BOOL GetGradient(INDEX iGradient, class CGradientParameters &gpGradient);

    //! Get classification box stretching vector.
    virtual FLOAT3D GetClassificationBoxStretch(void);

    //! Get anim data for given animation property - return NULL for none.
    virtual CAnimData *GetAnimData(SLONG slPropertyOffset);

    //! Adjust model shading parameters if needed - return TRUE if needs model shadows.
    virtual BOOL AdjustShadingParameters(FLOAT3D &vLightDirection, COLOR &colLight, COLOR &colAmbient);
      
    //! Adjust model mip factor if needed.
    virtual void AdjustMipFactor(FLOAT &fMipFactor);
    
    //! get a different model object for rendering - so entity can change its appearance dynamically
    /*!
      NOTE: base model is always used for other things (physics, etc).
    */
    virtual CModelObject *GetModelForRendering(void);

    //! get a different model object for rendering - so entity can change its appearance dynamically
    /*!
      NOTE: base model is always used for other things (physics, etc).
    */
    virtual CModelInstance *GetModelInstanceForRendering(void);
    
    //! Get field information - return NULL if not a field.
    virtual CFieldSettings *GetFieldSettings(void);
    
    //! Render particles made by this entity.
    virtual void RenderParticles(void);
    
    //! Get current collision box index for this entity.
    virtual INDEX GetCollisionBoxIndex(void);
    
    //! Get current collision box - override for custom collision boxes.
    virtual void GetCollisionBoxParameters(INDEX iBox, FLOATaabbox3D &box, INDEX &iEquality);
    
    //! Render game view.
    virtual void RenderGameView(CDrawPort *pdp, void *pvUserData);
    
    //! Apply mirror and stretch to the entity if supported.
    virtual void MirrorAndStretch(FLOAT fStretch, BOOL bMirrorX);
    
    //! Get offset for depth-sorting of alpha models (in meters, positive is nearer).
    virtual FLOAT GetDepthSortOffset(void);
    
    //! Get visibility tweaking bits.
    virtual ULONG GetVisTweaks(void);

    //! Get max tessellation level.
    virtual FLOAT GetMaxTessellationLevel(void);

    //! Get pointer to your predictor/predicted.
    /*!
      NOTE: autogenerated by ECC feature
    */
    virtual CEntity *GetPredictionPair(void);

    //! Set pointer to your predictor/predicted.
    /*!
      NOTE: autogenerated by ECC feature
    */
    virtual void SetPredictionPair(CEntity *penPair);
    
    //! Add this entity to prediction
    void AddToPrediction(void);
    
    //! Called by other entities to set time prediction parameter.
    virtual void SetPredictionTime(TIME tmAdvance);   // give time interval in advance to set
    
    //! Called by engine to get the upper time limit.
    virtual TIME GetPredictionTime(void);   // return moment in time up to which to predict this entity
    
    //! Get maximum allowed range for predicting this entity.
    virtual FLOAT GetPredictionRange(void);
    
    //! Add to prediction entities that this entity depends on.
    virtual void AddDependentsToPrediction(void);
    
    //! Copy for prediction.
    virtual void CopyForPrediction(CEntity &enOrg);

    
    //! Send an event to this entity.
    void SendEvent(const CEntityEvent &ee);
    
    //! Send an event to all entities in a box (box must be around this entity).
    void SendEventInRange(const CEntityEvent &ee, const FLOATaabbox3D &boxRange);

    
    //! Apply some damage to the entity (see event EDamage for more info)
    virtual void ReceiveDamage(CEntity *penInflictor, INDEX iDamageType, INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection);

    //! Receive item through event - for AI purposes only.
    virtual BOOL ReceiveItem(const CEntityEvent &ee);
    
    //! Get entity info - for AI purposes only.
    virtual void *GetEntityInfo(void);
    
    //! Fill in entity statistics - for AI purposes only.
    virtual BOOL FillEntityStatistics(struct EntityStats *pes);

    //! Model change notify.
    void ModelChangeNotify(void);
    
    //! Terrain change notify.
    void TerrainChangeNotify(void);
    
    //! Use only for initialization!
    virtual void SetHealthInit(INDEX iHealth)
    {
      en_ulFlags |= ENF_NEWHEALTH;
      SetHealth(iHealth);
    }

    //! Dummy.
    virtual BOOL IsPlayerControlled()
    {
      return FALSE;
    }
    
    // [SEE]
    virtual SLONG RemapStateInStack(SLONG slStateId) const
    {
      return slStateId;
    }
    
    // [SEE]
    virtual SLONG RemapProperty(ULONG ulPropertyType, ULONG ulPropertyId) const
    {
      return ulPropertyId;
    }
    
    virtual void HandleUnknownProperty(ULONG ulPropertyType, ULONG ulPropertyId, CVariant &varData) {};
    
    // [SEE]
    virtual BOOL IsRational() const
    {
      return FALSE;
    }
    
    // [SEE]
    virtual BOOL IsMovable() const
    {
      return FALSE;
    }
    
    virtual void FillAutoAimCoords(FLOAT3D &vCenter, FLOAT &fRadius)
    {
      FLOAT3D vOrigin(0.0F, 0.0F, 0.0F);
      vCenter = vOrigin;
      fRadius = 0.0F;
    }
    
    // [SEE]
    //! Get session properties for current game and world.
    CGameObject *GetSessionProperties();

    // [SEE]
    virtual void ReadCustomData_t(CTStream &istrm, const CEntityProperty &ep) {};
    virtual void WriteCustomData_t(CTStream &ostrm, const CEntityProperty &ep) {};
    virtual void CopyCustomData(CEntity &enOther, const CEntityProperty &ep) {};

    // [SEE]
    //! Get enum type metadata - return NULL for none.
    virtual const CEntityPropertyEnumType *GetEnumType(const CEntityProperty &ep)
    {
      return NULL;
    };

    //! Check value and apply limits if needed.
    virtual void ValidateProperty(const CEntityProperty &ep);

    void ValidateProperties();

    //! Check if entity is of given class by name.
    static BOOL IsOfClass(CEntity *pen, const char *pstrClassName);

    //! Check if entity is of given class by pointer.
    static BOOL IsOfClass(CEntity *pen, class CLibEntityClass *pdec);

    //! Check if two entities are of same class.
    static BOOL IsOfSameClass(CEntity *pen1, CEntity *pen2);

    //! Check if entity is of given class or derived from.
    static BOOL IsDerivedFromClass(CEntity *pen, const char *pstrClassName);

    //! Check if entity is of given class or derived from.
    static BOOL IsDerivedFromClass(CEntity *pen, class CLibEntityClass *pdec);

    //! List all entities connected in chain (i.e. markers). Returns FALSE if chain is recursive.
    static BOOL GetTargetChain(TDynamicContainer<CEntity> &cenOutput, CEntity *penStart);
};

/////////////////////////////////////////////////////////////////////
// Reference counting functions
inline void CEntity::AddReference(void) 
{ 
  if (this != NULL) {
    ASSERT(GetReferenceCount() >= 0);
    Reference(); 
  }
};

inline void CEntity::RemReference(void) 
{ 
  if (this != NULL) 
  {
    ASSERT(GetReferenceCount() > 0);
    Unreference();
    if (GetReferenceCount() == 0) {
      delete this;
    }
  }
};


#include <Engine/Entities/EntityPointer.h>

#define DECL_DLL ENGINE_API
#include <Engine/Classes/BaseEvents.h>
#undef DECL_DLL

#include <Engine/Entities/LiveEntity.h>
#include <Engine/Entities/RationalEntity.h>

#include <Engine/Entities/EntityFunctors.h>

ENGINE_API void EntityAdjustBonesCallback(void *pData);
ENGINE_API void EntityAdjustShaderParamsCallback(void *pData,INDEX iSurfaceID,CShaderClass *pShader,ShaderParams &spParams);

extern "C" ENGINE_API class CLibEntityClass CEntity_DLLClass;
extern "C" ENGINE_API class CLibEntityClass CLiveEntity_DLLClass;
extern "C" ENGINE_API class CLibEntityClass CRationalEntity_DLLClass;

#endif  /* include-once check. */