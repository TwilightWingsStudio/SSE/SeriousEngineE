/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityFunctors.h>

BOOL Func_IsOfClass::operator()(CEntity *pen) const
{
  return CEntity::IsOfClass(pen, _plec);
}

BOOL Func_IsDerivedFromClass::operator()(CEntity *pen) const
{
  return CEntity::IsDerivedFromClass(pen, _plec);
}

BOOL Func_IsOfClassStr::operator()(CEntity *pen) const
{
  return CEntity::IsOfClass(pen, _strClassName);
}

BOOL Func_IsDerivedFromClassStr::operator()(CEntity *pen) const
{
  return CEntity::IsDerivedFromClass(pen, _strClassName);
}

BOOL Func_IsInSpot::operator()(CEntity *pen) const
{
  const FLOAT fDistance = (pen->GetPlacement().pl_PositionVector - _vPos).Length();
  return fDistance < _fRange;
}

BOOL Func_IsInBox::operator()(CEntity *pen) const
{
  return _boxArea.TouchesSphere(pen->GetPlacement().pl_PositionVector, 0.0f);
}