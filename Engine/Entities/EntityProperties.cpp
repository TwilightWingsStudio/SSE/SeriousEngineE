/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/IO/Stream.h>
#include <Core/Base/CRCTable.h>
#include <Core/Base/Console.h>

#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityProperties.h>
#include <Engine/Entities/Precaching.h>
#include <Engine/World/World.h>
#include <Engine/Resources/ReplaceFile.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Sound/SoundObject.h>
#include <Core/Math/Quaternion.h>

#include <Core/Templates/StaticArray.cpp>

#define FILTER_ALL            "All files (*.*)\0*.*\0"
#define FILTER_END            "\0"

#define PROPERTY(offset, type) ENTITYPROPERTY(this, offset, type)

/////////////////////////////////////////////////////////////////////
// Property management functions

static inline CAbstractVar::Type PropertyTypeToVarType(CEntityProperty::Type ept)
{
  static INDEX CONVERSION_TABLE[] = {
    CAbstractVar::TYPE_NIL,
    CAbstractVar::TYPE_S32,         // EPT_ENUM
    CAbstractVar::TYPE_S32,         // EPT_BOOL
    CAbstractVar::TYPE_F32,         // EPT_FLOAT
    CAbstractVar::TYPE_S32,         // EPT_COLOR
    CAbstractVar::TYPE_STRING,      // EPT_STRING
    CAbstractVar::TYPE_F32,         // EPT_RANGE
    CAbstractVar::TYPE_U32,         // EPT_ENTITYPTR
    CAbstractVar::TYPE_STRING,      // EPT_FILENAME
    CAbstractVar::TYPE_S32,         // EPT_INDEX
    CAbstractVar::TYPE_S32,         // EPT_ANIMATION
    CAbstractVar::TYPE_S32,         // EPT_ILLUMINATIONTYPE
    CAbstractVar::TYPE_BOX3F,       // EPT_FLOATAABBOX3D
    CAbstractVar::TYPE_F32,         // EPT_ANGLE
    CAbstractVar::TYPE_VEC3F,       // EPT_VEC3F
    CAbstractVar::TYPE_VEC3F,       // EPT_ANGLE3D
    CAbstractVar::TYPE_PLANE3F,     // EPT_FLOATplane3D
    CAbstractVar::TYPE_PTR,         // EPT_MODELOBJECT
    CAbstractVar::TYPE_PLACEMENT3D, // EPT_PLACEMENT3D
    CAbstractVar::TYPE_PTR,         // EPT_ANIMOBJECT
    CAbstractVar::TYPE_STRING,      // EPT_FILENAMENODEP
    CAbstractVar::TYPE_PTR,         // EPT_SOUNDOBJECT
    CAbstractVar::TYPE_STRING,      // EPT_STRINGTRANS
    CAbstractVar::TYPE_QUAT4F,      // EPT_FLOATQUAT3D
    CAbstractVar::TYPE_MAT33F,      // EPT_FLOATMATRIX3D
    CAbstractVar::TYPE_U32,         // EPT_FLAGS
    CAbstractVar::TYPE_PTR,         // EPT_MODELINSTANCE
    // // [SEE]
    CAbstractVar::TYPE_U32, // EPT_RESERVED1
    CAbstractVar::TYPE_S64, // EPT_SQUAD
    CAbstractVar::TYPE_U32, // EPT_RESERVED2
    CAbstractVar::TYPE_F64, // EPT_DOUBLE
    CAbstractVar::TYPE_BYTEARRAY, // EPT_BYTEARRAY
    CAbstractVar::TYPE_BYTEARRAY, // EPT_CUSTOMDATA
  };

  return CAbstractVar::Type(CONVERSION_TABLE[ept]);
}

static void ReadPropertyError_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  ThrowF_t("unknown entity property type!");
}

static void ReadPropertyIndex_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), INDEX);
}

static void ReadPropertyFloat_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), FLOAT);
}

static void ReadPropertyString_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), CTString);
}

static void ReadPropertyEntityPtr_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  pen->ReadEntityPointer_t(&istrm, ENTITYPROPERTY(pen, ep.GetOffset(), CEntityPointer));
}

static void ReadPropertyStringTrans_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.ExpectID_t("DTRS");
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), CTString);
}

static void ReadPropertyFilename_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  CTFileName &fnm = ENTITYPROPERTY(pen, ep.GetOffset(), CTFileName);

  istrm >> fnm;
  if (fnm == "") {
    return;
  }
  
  // Try to replace file name If it doesn't exist
  for (;;)
  {
    if (!FileExists(fnm))
    {
      // If file was not found, ask for replacing file
      CTFileName fnReplacingFile;
      if (GetReplacingFile(fnm, fnReplacingFile, FILTER_ALL FILTER_END))
      {
        // Replacing file was provided
        fnm = fnReplacingFile;
      } else {
        ThrowF_t(TRANS("File '%s' does not exist"), fnm.ConstData());
      }
    } else {
      return;
    }
  }
}

static void ReadPropertyBox3f_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.Read_t(&ENTITYPROPERTY(pen, ep.GetOffset(), FLOATaabbox3D), sizeof(FLOATaabbox3D));
}

static void ReadPropertyVec3f_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.Read_t(&ENTITYPROPERTY(pen, ep.GetOffset(), FLOAT3D), sizeof(VEC3F));
}

static void ReadPropertyPlane3f_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.Read_t(&ENTITYPROPERTY(pen, ep.GetOffset(), FLOATplane3D), sizeof(FLOATplane3D));
}

static void ReadPropertyModelObject_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  ReadModelObject_t(istrm, ENTITYPROPERTY(pen, ep.GetOffset(), CModelObject));
}

static void ReadPropertyPlacement3D_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.Read_t(&ENTITYPROPERTY(pen, ep.GetOffset(), CPlacement3D), sizeof(CPlacement3D));
}

static void ReadPropertyAnimObject_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  ReadAnimObject_t(istrm, ENTITYPROPERTY(pen, ep.GetOffset(), CAnimObject));
}

static void ReadPropertyFilenameNoDep_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), CTFileNameNoDep);
}

static void ReadPropertySoundObject_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  CSoundObject &so = ENTITYPROPERTY(pen, ep.GetOffset(), CSoundObject);
  ReadSoundObject_t(istrm, so);
  so.so_penEntity = pen;
}

static void ReadPropertyQuat4f_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.Read_t(&ENTITYPROPERTY(pen, ep.GetOffset(), FLOATquat3D), sizeof(FLOATquat3D));
}

static void ReadPropertyMat33f_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm.Read_t(&ENTITYPROPERTY(pen, ep.GetOffset(), FLOATmatrix3D), sizeof(FLOATmatrix3D));
}

static void ReadPropertyModelInstance_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  ReadModelInstance_t(istrm, ENTITYPROPERTY(pen, ep.GetOffset(), CModelInstance));
}

static void ReadPropertySquad_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), SQUAD);
}

static void ReadPropertyDouble_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), DOUBLE);
}

static void ReadPropertyByteArray_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), CByteArray);
}

static void ReadPropertyCustomData_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  CByteArray baData;
  istrm >> baData;

  if (baData.Size() > 0) {
    // TODO: CTMemoryStream makes copy of data. We need own serializer!
    CTMemoryStream strmData(baData.Data(), baData.Size(), CTStream::OM_READ);
    pen->ReadCustomData_t(strmData, ep);
  }
}

static void ReadPropertyVariant_t(CEntity *pen, const CEntityProperty &ep, CTStream &istrm)
{
  istrm >> ENTITYPROPERTY(pen, ep.GetOffset(), CVariant);
}

static void WritePropertyError_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ThrowF_t("unknown entity property type!");
}

static void WritePropertyIndex_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), INDEX);
}

static void WritePropertyFloat_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), FLOAT);
}

static void WritePropertyString_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CTString);
}

static void WritePropertyEntityPtr_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  pen->WriteEntityPointer_t(&ostrm, ENTITYPROPERTY(pen, ep.GetOffset(), CEntityPointer));
}

static void WritePropertyStringTrans_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm.WriteID_t("DTRS");
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CTString);
}

static void WritePropertyFilename_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CTFileName);
}

static void WritePropertyBox3f_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), BOX3F);
}

static void WritePropertyVec3f_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), VEC3F);
}

static void WritePropertyPlane3f_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), PLANE3F);
}

static void WritePropertyModelObject_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  WriteModelObject_t(ostrm, ENTITYPROPERTY(pen, ep.GetOffset(), CModelObject));
}

static void WritePropertyPlacement3D_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CPlacement3D);
}

static void WritePropertyAnimObject_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  WriteAnimObject_t(ostrm, ENTITYPROPERTY(pen, ep.GetOffset(), CAnimObject));
}

static void WritePropertyFilenameNoDep_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CTFileNameNoDep);
}

static void WritePropertySoundObject_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  WriteSoundObject_t(ostrm, ENTITYPROPERTY(pen, ep.GetOffset(), CSoundObject));
}

static void WritePropertyQuat4f_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm.Write_t(&ENTITYPROPERTY(pen, ep.GetOffset(), FLOATquat3D), sizeof(FLOATquat3D));
}

static void WritePropertyMat33f_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm.Write_t(&ENTITYPROPERTY(pen, ep.GetOffset(), MAT33F), sizeof(MAT33F));
}

static void WritePropertyModelInstance_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  WriteModelInstance_t(ostrm, ENTITYPROPERTY(pen, ep.GetOffset(), CModelInstance));
}

static void WritePropertySquad_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), SQUAD);
}

static void WritePropertyDouble_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), DOUBLE);
}

static void WritePropertyByteArray_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CByteArray);
}

static void WritePropertyCustomData_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  // Prepare data
  CTMemoryStream strmData;
  pen->WriteCustomData_t(strmData, ep);

  // Write data.
  const SLONG slDataSize = strmData.GetStreamSize();
  ostrm << slDataSize;
  ostrm.Write_t(strmData.mstrm_pubBuffer, slDataSize);
}

static void WritePropertyVariant_t(CEntity *pen, const CEntityProperty &ep, CTStream &ostrm)
{
  ostrm << ENTITYPROPERTY(pen, ep.GetOffset(), CVariant);
}

// Set all properties to default values.
void CEntity::SetDefaultProperties(void)
{
  // no properties to set in base class
}

// Helpers for writing/reading entity pointers.
void CEntity::ReadEntityPointer_t(CTStream *istrm, CEntityPointer &pen)
{
  CEntity *penPointed;
  
  // Read index
  INDEX iPointedEntity;
  *istrm >> iPointedEntity;
  
  // If there is no entity pointed to then set NULL pointer
  if (iPointedEntity == -1) {
    penPointed = NULL;
    
  // If there is some entity get the entity in this world with that index
  } else {
    extern BOOL _bReadEntitiesByID;
    if (_bReadEntitiesByID) {
      penPointed = en_pwoWorld->EntityFromID(iPointedEntity);
    } else {
      penPointed = en_pwoWorld->wo_cenAllEntities.Pointer(iPointedEntity);
    }
  }
  
  // Return the entity pointer
  pen = penPointed;
}

// Serialize an entity pointer.
void CEntity::WriteEntityPointer_t(CTStream *ostrm, CEntityPointer pen)
{
  // If there is no entity pointed to then write -1 index
  if (pen == NULL) {
    *ostrm << (INDEX)-1;
    
  // If there is some entity
  } else {
    // The entity must be in the same world as this one
    ASSERT(pen->en_pwoWorld == en_pwoWorld);
    
    // Write index of the entity in this world
    *ostrm << (pen->en_ulID);
  }
}

// Read all properties from a stream.
void CEntity::ReadProperties_t(CTStream &istrm)
{
  const BOOL bSimulation = istrm.GetFlags() & WSF_SIMULATION;

  static void(*FUNCTIONS[])(CEntity *pen, const CEntityProperty &ep, CTStream &istrm) = {
    &ReadPropertyError_t,         // EPT_INVALID
    &ReadPropertyIndex_t,         // EPT_ENUM
    &ReadPropertyIndex_t,         // EPT_BOOL
    &ReadPropertyFloat_t,         // EPT_FLOAT
    &ReadPropertyIndex_t,         // EPT_COLOR
    &ReadPropertyString_t,        // EPT_STRING
    &ReadPropertyFloat_t,         // EPT_RANGE
    &ReadPropertyEntityPtr_t,     // EPT_ENTITYPTR
    &ReadPropertyFilename_t,      // EPT_FILENAME
    &ReadPropertyIndex_t,         // EPT_INDEX
    &ReadPropertyIndex_t,         // EPT_ANIMATION
    &ReadPropertyIndex_t,         // EPT_ILLUMINATIONTYPE
    &ReadPropertyBox3f_t,         // EPT_FLOATAABBOX3D
    &ReadPropertyFloat_t,         // EPT_ANGLE
    &ReadPropertyVec3f_t,         // EPT_VEC3F
    &ReadPropertyVec3f_t,         // EPT_ANGLE3D
    &ReadPropertyPlane3f_t,       // EPT_FLOATplane3D
    &ReadPropertyModelObject_t,   // EPT_MODELOBJECT
    &ReadPropertyPlacement3D_t,   // EPT_PLACEMENT3D
    &ReadPropertyAnimObject_t,    // EPT_ANIMOBJECT
    &ReadPropertyFilenameNoDep_t, // EPT_FILENAMENODEP
    &ReadPropertySoundObject_t,   // EPT_SOUNDOBJECT
    &ReadPropertyStringTrans_t,   // EPT_STRINGTRANS
    &ReadPropertyQuat4f_t,        // EPT_FLOATQUAT3D
    &ReadPropertyMat33f_t,        // EPT_FLOATMATRIX3D
    &ReadPropertyIndex_t,         // EPT_FLAGS
    &ReadPropertyModelInstance_t, // EPT_MODELINSTANCE
    &ReadPropertyIndex_t,         // EPT_RESERVED1
    &ReadPropertySquad_t,         // EPT_SQUAD
    &ReadPropertyFloat_t,         // EPT_RESERVED2
    &ReadPropertyDouble_t,        // EPT_DOUBLE
    &ReadPropertyByteArray_t,     // EPT_BYTEARRAY
    &ReadPropertyCustomData_t,    // EPT_CUSTOMDATA
    &ReadPropertyVariant_t,       // EPT_VARIANT
  };

  istrm.ExpectID_t("PRPS");  // 'properties'
  const CLibEntityClass *pdecLibClass = en_pecClass->ec_pdecDLLClass;
  INDEX ctProperties;

  // Read number of properties (note that this doesn't have to be same as number
  // of properties in the class (class might have changed))
  istrm >> ctProperties;

  // For all saved properties
  for (INDEX iProperty = 0; iProperty < ctProperties; iProperty++)
  {
    pdecLibClass->dec_ctProperties;

    // Read packed identifier
    ULONG ulIDAndType;
    istrm >> ulIDAndType;

    // Unpack property ID and property type from the identifier
    ULONG ulPropertyId;
    CEntityProperty::Type eptType;
    ulPropertyId = ulIDAndType >> 8;
    eptType = (CEntityProperty::Type )(ulIDAndType & 0x000000FFUL);

    ulPropertyId = RemapProperty(eptType, ulPropertyId); // [SEE]

    // Get the property with that ID and type
    const CEntityProperty *pepProperty = PropertyForId(ulPropertyId, eptType);

    // If not found, but it is a string
    if (pepProperty == NULL && eptType == CEntityProperty::EPT_STRING) {
      // Maybe that became translatable string try that
      pepProperty = PropertyForId(ulPropertyId, CEntityProperty::EPT_STRINGTRANS);
      // NOTE: it is still loaded as string, without translation chunk,
      // we just find it in properties table as a translatable string.
    }
    
    BOOL bSkipProperty = FALSE;
    
    // [SEE]
    if (!bSimulation && pepProperty != NULL && pepProperty->GetFlags() & EPROPF_SIMULATIONONLY) {
      bSkipProperty = TRUE; // Skip it on load!
    }

    // If it was not found
    if (pepProperty == NULL || bSkipProperty) {
      CGameObject *pObjectProperty = NULL;
      CVariant varProperty;
      CAbstractVar::Type eVarPropType = PropertyTypeToVarType(eptType);

      // TODO: Convert this switch to dictionary of keys, it it's possible
      // Depending on the property type
      switch (eptType)
      {
        // If it is INDEX then skip INDEX
        case CEntityProperty::EPT_BOOL:
        case CEntityProperty::EPT_INDEX:
        case CEntityProperty::EPT_ENUM:
        case CEntityProperty::EPT_FLAGS:
        case CEntityProperty::EPT_ANIMATION:
        case CEntityProperty::EPT_ILLUMINATIONTYPE:
        case CEntityProperty::EPT_COLOR:
        case CEntityProperty::EPT_RESERVED1: // [SSE]
        case CEntityProperty::EPT_RESERVED2: // [SSE]
        {
          INDEX iDummy;
          istrm >> iDummy;
          varProperty.FromInt(iDummy);
          break;
        }

        // If it is FLOAT then skip FLOAT
        case CEntityProperty::EPT_FLOAT:
        case CEntityProperty::EPT_RANGE:
        case CEntityProperty::EPT_ANGLE:
        {
          FLOAT fDummy;
          istrm >> fDummy;
          varProperty.FromFloat(fDummy);
          break;
        }

        // If it is STRING then skip STRING
        case CEntityProperty::EPT_STRING:
        {
          CTString strDummy;
          istrm >> strDummy;
          varProperty.FromString(strDummy);
          break;
        }

        // If it is STRINGTRANS then skip STRINGTRANS
        case CEntityProperty::EPT_STRINGTRANS:
        {
          istrm.ExpectID_t("DTRS");
          CTString strDummy;
          istrm >> strDummy;
          varProperty.FromString(strDummy);
          break;
        }
        // If it is FILENAME then skip FILENAME
        case CEntityProperty::EPT_FILENAME:
        {
          CTFileName fnmDummy;
          istrm >> fnmDummy;
          varProperty.FromString(fnmDummy);
          break;
        }
        // If it is FILENAMENODEP then skip FILENAMENODEP
        case CEntityProperty::EPT_FILENAMENODEP:
        {
          CTFileNameNoDep fnmDummy;
          istrm >> fnmDummy;
          varProperty.FromString(fnmDummy);
          break;
        }
        // If it is ENTITYPTR skip index
        case CEntityProperty::EPT_ENTITYPTR:
        {
          INDEX iDummy;
          istrm >> iDummy;
          varProperty.FromInt(iDummy);
          break;
        }

        // If it is FLOATAABBOX3D then skip FLOATAABBOX3D
        case CEntityProperty::EPT_FLOATAABBOX3D:
        {
          FLOATaabbox3D boxDummy;
          istrm.Read_t(&boxDummy, sizeof(FLOATaabbox3D));
          varProperty.FromBox3f(boxDummy);
          break;
        }
        // If it is FLOATMATRIX3D then skip FLOATMATRIX3D
        case CEntityProperty::EPT_FLOATMATRIX3D:
        {
          FLOATmatrix3D boxDummy;
          istrm.Read_t(&boxDummy, sizeof(FLOATmatrix3D));
          varProperty.FromMat33f(boxDummy);
          break;
        }
        // If it is EPT_FLOATQUAT3D then skip EPT_FLOATQUAT3D
        case CEntityProperty::EPT_FLOATQUAT3D:
        {
          FLOATquat3D qDummy;
          istrm.Read_t(&qDummy, sizeof(FLOATquat3D));
          varProperty.FromQuat4f(qDummy);
        }
          break;
        // If it is FLOAT3D then skip FLOAT3D
        case CEntityProperty::EPT_VEC3F:
        {
          FLOAT3D vDummy;
          istrm >> vDummy;
          varProperty.FromVec3f(vDummy);
          break;
        }

        // If it is ANGLE3D then skip ANGLE3D
        case CEntityProperty::EPT_ANGLE3D:
        {
          ANGLE3D vDummy;
          istrm >> vDummy;
          varProperty.FromVec3f(vDummy);
          break;
        }

        // If it is FLOATplane3D then skip FLOATplane3D
        case CEntityProperty::EPT_FLOATplane3D:
        {
          FLOATplane3D plDummy;
          istrm.Read_t(&plDummy, sizeof(plDummy));
          varProperty.FromPlane3f(plDummy);
          break;
        }

        // If it is MODELOBJECT then skip CModelObject
        case CEntityProperty::EPT_MODELOBJECT:
        {
          pObjectProperty = new CModelObject();
          CModelObject *pmo = reinterpret_cast<CModelObject *>(pObjectProperty);
          ReadModelObject_t(istrm, *pmo);
          //SkipModelObject_t(istrm);
          break;
        }

        // If it is MODELINSTANCE then skip CModelInstance
        case CEntityProperty::EPT_MODELINSTANCE:
        {
          pObjectProperty = new CModelInstance();
          CModelInstance *pmi = reinterpret_cast<CModelInstance *>(pObjectProperty);
          ReadModelInstance_t(istrm, *pmi);
          //SkipModelInstance_t(istrm);
          break;
        }

        // If it is ANIMOBJECT then skip CAnimObject
        case CEntityProperty::EPT_ANIMOBJECT:
        {
          pObjectProperty = new CAnimObject();
          CAnimObject *pao = reinterpret_cast<CAnimObject *>(pObjectProperty);
          ReadAnimObject_t(istrm, *pao);
          //SkipAnimObject_t(istrm);
          break;
        }

        // If it is SOUNDOBJECT skip CSoundObject
        case CEntityProperty::EPT_SOUNDOBJECT:
        {
          pObjectProperty = new CSoundObject();
          CSoundObject *pso = reinterpret_cast<CSoundObject *>(pObjectProperty);
          ReadSoundObject_t(istrm, *pso);
          pso->so_penEntity = this;
          //SkipSoundObject_t(istrm);
          break;
        }

        // [SEE]
        case CEntityProperty::EPT_SQUAD:
          INT64 nDummy;
          istrm >> nDummy;
          varProperty.FromInt64(nDummy);
          break;

        // [SEE]
        case CEntityProperty::EPT_DOUBLE:
          DOUBLE fDummy;
          istrm >> fDummy;
          varProperty.FromDouble(fDummy);
          break;
          
        // [SEE]
        case CEntityProperty::EPT_BYTEARRAY: {
        case CEntityProperty::EPT_CUSTOMDATA:
          CByteArray baDummy;
          istrm >> baDummy;
          varProperty.FromByteArray(baDummy);
        } break;
        
        // [SEE]
        case CEntityProperty::EPT_VARIANT: {
          istrm >> varProperty;
        } break;

        default:
          ASSERTALWAYS("Unknown property type");
      }
      
      if (pObjectProperty) {
        varProperty.FromPtr(pObjectProperty);
      }
      
      // If not just skipped!
      if (!bSkipProperty) {
        HandleUnknownProperty(eptType, ulPropertyId, varProperty);
      }

      if (pObjectProperty) {
        delete pObjectProperty;
      }

    // If it was found
    } else {
      // Fixup for loading old strings as translatable strings
      CEntityProperty::Type eptLoad = pepProperty->GetType();
      if (eptType == CEntityProperty::EPT_STRING && eptLoad == CEntityProperty::EPT_STRINGTRANS) {
        eptLoad = CEntityProperty::EPT_STRING;
      }

      if (eptLoad > CEntityProperty::EPT_LAST) {
        eptLoad = CEntityProperty::EPT_INVALID;
      }

      (*FUNCTIONS[eptLoad])(this, *pepProperty, istrm);
    }
  }
}

// Write all properties to a stream.
void CEntity::WriteProperties_t(CTStream &ostrm)
{
  const BOOL bSimulation = ostrm.GetFlags() & WSF_SIMULATION;

  static void(*FUNCTIONS[])(CEntity *pen, const CEntityProperty &ep, CTStream &istrm) = {
    &WritePropertyError_t,         // EPT_ERROR
    &WritePropertyIndex_t,         // EPT_ENUM
    &WritePropertyIndex_t,         // EPT_BOOL
    &WritePropertyFloat_t,         // EPT_FLOAT
    &WritePropertyIndex_t,         // EPT_COLOR
    &WritePropertyString_t,        // EPT_STRING
    &WritePropertyFloat_t,         // EPT_RANGE
    &WritePropertyEntityPtr_t,     // EPT_ENTITYPTR
    &WritePropertyFilename_t,      // EPT_FILENAME
    &WritePropertyIndex_t,         // EPT_INDEX
    &WritePropertyIndex_t,         // EPT_ANIMATION
    &WritePropertyIndex_t,         // EPT_ILLUMINATIONTYPE
    &WritePropertyBox3f_t,         // EPT_FLOATAABBOX3D
    &WritePropertyFloat_t,         // EPT_ANGLE
    &WritePropertyVec3f_t,         // EPT_VEC3F
    &WritePropertyVec3f_t,         // EPT_ANGLE3D
    &WritePropertyPlane3f_t,       // EPT_FLOATplane3D
    &WritePropertyModelObject_t,   // EPT_MODELOBJECT
    &WritePropertyPlacement3D_t,   // EPT_PLACEMENT3D
    &WritePropertyAnimObject_t,    // EPT_ANIMOBJECT
    &WritePropertyFilenameNoDep_t, // EPT_FILENAMENODEP
    &WritePropertySoundObject_t,   // EPT_SOUNDOBJECT
    &WritePropertyStringTrans_t,   // EPT_STRINGTRANS
    &WritePropertyQuat4f_t,        // EPT_FLOATQUAT3D
    &WritePropertyMat33f_t,        // EPT_FLOATMATRIX3D
    &WritePropertyIndex_t,         // EPT_FLAGS
    &WritePropertyModelInstance_t, // EPT_MODELINSTANCE
    &WritePropertyIndex_t,         // EPT_RESERVED1
    &WritePropertySquad_t,         // EPT_SQUAD
    &WritePropertyFloat_t,         // EPT_RESERVED2
    &WritePropertyDouble_t,        // EPT_DOUBLE
    &WritePropertyByteArray_t,     // EPT_BYTEARRAY
    &WritePropertyCustomData_t,    // EPT_CUSTOMDATA
    &WritePropertyVariant_t,       // EPT_VARIANT
  };

  INDEX ctProperties = 0;

  ostrm.WriteID_t("PRPS");  // 'properties'

  // Write number of properties
  const SLONG slPropertiesPos = ostrm.GetPos_t();
  ostrm << INDEX(0); // Write dummy

  // For all classes in hierarchy of this entity
  {
    for (CLibEntityClass *pdecLibClass = en_pecClass->ec_pdecDLLClass;
        pdecLibClass != NULL;
        pdecLibClass = pdecLibClass->dec_pdecBase) 
    {
      // For all properties
      for (INDEX iProperty = 0; iProperty < pdecLibClass->dec_ctProperties; iProperty++)
      {
        const CEntityProperty &epProperty = pdecLibClass->dec_aepProperties[iProperty];

        // [SEE]
        if (!bSimulation && epProperty.GetFlags() & EPROPF_SIMULATIONONLY) {
          continue;
        }

        // Pack property ID and property type together
        ULONG ulID = epProperty.GetId();
        ULONG ulType = (ULONG)epProperty.GetType();
        ULONG ulIDAndType = (ulID << 8) | ulType;

        // Write the packed identifier
        ostrm << ulIDAndType;

        (*FUNCTIONS[epProperty.GetType()])(this, epProperty, ostrm);
        
        ctProperties++;
      }
    }
  }
  
  const SLONG slAfterProperties = ostrm.GetPos_t();
  ostrm.SetPos_t(slPropertiesPos);
  ostrm << ctProperties;
  ostrm.SetPos_t(slAfterProperties);
}

// Convert value of an enum to its name
const char *CEntityPropertyEnumType::NameForValue(INDEX iValue)
{
  for (INDEX i = 0; i < epet_ctValues; i++)
  {
    if (epet_aepevValues[i].epev_iValue == iValue) {
      return epet_aepevValues[i].epev_strName;
    }
  }
  return "";
}

void CEntity::ValidateProperty(const CEntityProperty &ep)
{
  const CVariant *pLowerLimit = ep.GetLowerLimit();
  const CVariant *pUpperLimit = ep.GetUpperLimit();

  if (pLowerLimit != NULL) {
    // Lower limit - integer.
    if (ep.IsInteger() && pLowerLimit->IsIntegral()) {
      if (ep.GetType() == CEntityProperty::EPT_INDEX) {
        INDEX &iValue = PROPERTY(ep.GetOffset(), INDEX);
        const INDEX iLimitValue = pLowerLimit->ToInt();
        iValue = ClampDn(iValue, iLimitValue);
      } else if (ep.GetType() == CEntityProperty::EPT_SQUAD) {
        SQUAD &sqValue = PROPERTY(ep.GetOffset(), SQUAD);
        const SQUAD sqLimitValue = pLowerLimit->ToInt64();
        sqValue = ClampDn(sqValue, sqValue);
      }

    // Lower limit - floating point.
    } else if (ep.IsFloating() && pLowerLimit->IsFloating()) {
      if (ep.GetType() == CEntityProperty::EPT_DOUBLE) {
        DOUBLE &fValue = PROPERTY(ep.GetOffset(), DOUBLE);
        DOUBLE fLimitValue = pLowerLimit->ToDouble();
        fValue = ClampDn(fValue, fLimitValue);
      } else {
        FLOAT &fValue = PROPERTY(ep.GetOffset(), FLOAT);
        const FLOAT fLimitValue = pLowerLimit->ToFloat();
        fValue = ClampDn(fValue, fLimitValue);
      }
    }
  }

  if (pUpperLimit != NULL) {
    // Upper limit - integer.
    if (ep.IsInteger() && pUpperLimit->IsIntegral()) {
      if (ep.GetType() == CEntityProperty::EPT_INDEX) {
        INDEX &iValue = PROPERTY(ep.GetOffset(), INDEX);
        const INDEX iLimitValue = pUpperLimit->ToInt();
        iValue = ClampUp(iValue, iLimitValue);
      } else if (ep.GetType() == CEntityProperty::EPT_SQUAD) {
        SQUAD &sqValue = PROPERTY(ep.GetOffset(), SQUAD);
        const SQUAD sqLimitValue = pUpperLimit->ToInt64();
        sqValue = ClampUp(sqValue, sqValue);
      }

    // Upper limit - floating point.
    } else if (ep.IsFloating() && pUpperLimit->IsFloating()) {
      if (ep.GetType() == CEntityProperty::EPT_DOUBLE) {
        DOUBLE &fValue = PROPERTY(ep.GetOffset(), DOUBLE);
        const DOUBLE fLimitValue = pUpperLimit->ToDouble();
        fValue = ClampUp(fValue, fLimitValue);
      } else {
        FLOAT &fValue = PROPERTY(ep.GetOffset(), FLOAT);
        const FLOAT fLimitValue = pUpperLimit->ToFloat();
        fValue = ClampUp(fValue, fLimitValue);
      }
    }
  }
}

void CEntity::ValidateProperties()
{
  for (CLibEntityClass *pdecLibClass = en_pecClass->ec_pdecDLLClass;
      pdecLibClass != NULL;
      pdecLibClass = pdecLibClass->dec_pdecBase) 
  {
    // For all properties
    for (INDEX iProperty = 0; iProperty < pdecLibClass->dec_ctProperties; iProperty++)
    {
      const CEntityProperty &epProperty = pdecLibClass->dec_aepProperties[iProperty];
      ValidateProperty(epProperty);
    }
  }
}

// These entity classes are bases, here stop all recursive searches
ENTITY_CLASSDEFINITION_BASE(CEntity, 32000);
ENTITY_CLASSDEFINITION_BASE(CLiveEntity, 32001);
ENTITY_CLASSDEFINITION_BASE(CRationalEntity, 32002);

extern void ClearToDefault(CByteArray &ba)
{
  ba.Clear();
};

extern void ClearToDefault(CVariant &var)
{
  var.Clear();
};