/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Engine/Resources/ReplaceFile.h>

#include <Engine/Brushes/Brush.h>
#include <Engine/Brushes/BrushArchive.h>
#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityProperties.h>
#include <Engine/Entities/ShadingInfo.h>
#include <Engine/Light/LightSource.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/TerrainArchive.h>

#include <Core/Math/Float.h>

#include <Engine/World/World.h>

// Read from stream.
void CEntity::Read_t(CTStream *istr)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // [SEE] ABZ / EuroCops Loader
  if (istr->PeekID_t() == CChunkID("PEHA")) {
    istr->ExpectID_t("PEHA");
  }

  // [SEE] LastChaos Loader
  if (istr->PeekID_t() == CChunkID("ENT5")) {
    istr->ExpectID_t("ENT5");
    ULONG ulID;
    SLONG slSize;
    ULONG ulDummyFlags1, ulDummyFlags2;
    (*istr) >> ulID >> slSize;    // skip id and size
    (*istr) >> (ULONG &)en_RenderType
           >> en_ulPhysicsFlags
           >> en_ulCollisionFlags
           >> en_ulSpawnFlags
           >> en_ulFlags
           >> ulDummyFlags1
           >> ulDummyFlags2;
    (*istr).Read_t(&en_mRotation, sizeof(en_mRotation));
  
  } else {
    // Read base class data from stream
    istr->ExpectID_t("ENT4");
    ULONG ulID;
    SLONG slSize;
    (*istr) >> ulID >> slSize;    // skip id and size
    (*istr) >> (ULONG &)en_RenderType
           >> en_ulPhysicsFlags
           >> en_ulCollisionFlags
           >> en_ulSpawnFlags
           >> en_ulFlags;
    (*istr).Read_t(&en_mRotation, sizeof(en_mRotation));
  }    

  // Clear flags for selection and caching info
  en_ulFlags &= ~(ENF_SELECTED|ENF_INRENDERING|ENF_VALIDSHADINGINFO);
  en_psiShadingInfo = NULL;
  en_pciCollisionInfo = NULL;

  // If this is a brush then read brush index in world's brush archive
  if (en_RenderType == RT_BRUSH || en_RenderType == RT_FIELDBRUSH) 
  {
    INDEX iBrush;
    (*istr) >> iBrush;
    en_pbrBrush = &en_pwoWorld->wo_baBrushes.ba_abrBrushes[iBrush];
    en_pbrBrush->br_penEntity = this;
  
  // If this is a terrain then read terrain index in world's terrain archive
  } else if (en_RenderType == RT_TERRAIN) {
    INDEX iTerrain;
    (*istr)>>iTerrain;
    en_ptrTerrain = &en_pwoWorld->wo_taTerrains.ta_atrTerrains[iTerrain];
    en_ptrTerrain->tr_penEntity = this;
    // Force terrain regeneration (regenerate tiles on next render)
    en_ptrTerrain->ReBuildTerrain(TRUE);

  // If this is a model then create a new model object
  } else if (en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL) {
    en_pmoModelObject = new CModelObject;
    en_psiShadingInfo = new CShadingInfo;
    en_ulFlags &= ~ENF_VALIDSHADINGINFO;
    // Read model
    ReadModelObject_t(*istr, *en_pmoModelObject);
  
  // If this is a ska model
  } else if (en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    en_pmiModelInstance = CreateModelInstance("Temp");
    en_psiShadingInfo = new CShadingInfo;
    en_ulFlags &= ~ENF_VALIDSHADINGINFO;

    ReadModelInstance_t(*istr, *GetModelInstance());
    
  // If this is a void
  } else if (en_RenderType == RT_VOID) {
    en_pbrBrush = NULL;
  }

  // If the entity has a parent then read the parent pointer and relative offset
  if (istr->PeekID_t() == CChunkID("PART")) // parent
  { 
    istr->ExpectID_t("PART");
    INDEX iParent;
    *istr >> iParent;
    extern BOOL _bReadEntitiesByID;
    if (_bReadEntitiesByID) {
      n_pParent = en_pwoWorld->EntityFromID(iParent);
    } else {
      n_pParent = en_pwoWorld->wo_cenAllEntities.Pointer(iParent);
    }
    *istr >> en_plRelativeToParent;
    
    // Link to parent
    GetParent()->GetChildren().AddTail(n_lnInParent);
  }

  // Read the derived class properties from stream
  ReadProperties_t(*istr);

  // If it is a light source
  {
    CLightSource *pls = GetLightSource();
    if (pls != NULL) {
      // Read the light source layer list
      pls->ls_penEntity = this;
      pls->Read_t(istr);
    }
  }

  // If it is a field brush
  CFieldSettings *pfs = GetFieldSettings();
  if (pfs != NULL) {
    // Remember its field settings
    ASSERT(en_RenderType == RT_FIELDBRUSH);
    en_pbrBrush->br_pfsFieldSettings = pfs;
  }

  // If entity was predictable then restore that condition
  if (en_ulFlags & ENF_PREDICTABLE) {
    en_ulFlags &= ~ENF_PREDICTABLE; // have to clear it to be able to set it back
    SetPredictable(TRUE);
  }
}

// Write to stream.
void CEntity::Write_t(CTStream *ostr)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // Write base class data to stream
  ostr->WriteID_t("ENT4");
  SLONG slSize = 0;
  (*ostr) << en_ulID << slSize;    // save id and keep space for size
  (*ostr) << (ULONG&)en_RenderType
         << en_ulPhysicsFlags
         << en_ulCollisionFlags
         << en_ulSpawnFlags
         << en_ulFlags;
  (*ostr).Write_t(&en_mRotation, sizeof(en_mRotation));
  
  // If this is a brush then write brush index in world's brush archive
  if (en_RenderType == RT_BRUSH || en_RenderType == RT_FIELDBRUSH) {
    (*ostr) << en_pwoWorld->wo_baBrushes.ba_abrBrushes.Index(en_pbrBrush);
  
  // If this is a terrain then write brush index in world's brush archive
  } else if (en_RenderType == RT_TERRAIN) {
    (*ostr) << en_pwoWorld->wo_taTerrains.ta_atrTerrains.Index(en_ptrTerrain);
  
  // If this is a model then write model
  } else if (en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL) {
    WriteModelObject_t(*ostr, *en_pmoModelObject);
    
  // If this is ska model then write ska model
  } else if (en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
    WriteModelInstance_t(*ostr, *GetModelInstance());
  
  // If this is a void then do nothing
  } else if (en_RenderType == RT_VOID) {
    NOTHING;
  }

  // If the entity has a parent then write the parent pointer and relative offset
  if (GetParentEntity() != NULL) {
    ostr->WriteID_t("PART"); // parent
    INDEX iParent = GetParentEntity()->en_ulID;
    *ostr << iParent;
    *ostr << en_plRelativeToParent;
  }

  // Write the derived class properties to stream
  WriteProperties_t(*ostr);
  
  // If it is a light source
  {
    CLightSource *pls = GetLightSource();
    if (pls != NULL) {
      // Read the light source layer list
      pls->Write_t(ostr);
    }
  }
}
