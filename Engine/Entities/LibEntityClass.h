/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_DLLENTITYCLASS_H
#define SE_INCL_DLLENTITYCLASS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! One entry in function table
class CEventHandlerEntry
{
  public:
    inline SLONG GetStateId() const
    {
      return ehe_slState;
    }
    
    inline SLONG GetBaseStateId() const
    {
      return ehe_slBaseState;
    }
    
    inline const char *GetName() const
    {
      return ehe_strName;
    }
    
    inline const CEntity::pEventHandler GetFunction() const
    {
      return ehe_pEventHandler;
    }
  
  public:
    SLONG ehe_slState;                    // code of the automaton state
    SLONG ehe_slBaseState;                // code of the base state if overridden
    CEntity::pEventHandler ehe_pEventHandler;  // handler function
    const char *ehe_strName;              // symbolic name of handler for debugging
};

//! The class defining entity event at DLL.
class ENGINE_API CLibEntityEvent 
{
  public:
    SLONG dee_slEvent;                // event code
    CEntityEvent *(*dee_New)(void);   // pointer to function that makes intance of this event
};

//! The class defining an entity class DLL itself
class ENGINE_API CLibEntityClass
{
  public:
    CLibEntityEvent **dec_adeeEvents; // array of events
    INDEX dec_ctEvents;               // number of events

    CEntityProperty *dec_aepProperties; // array of properties
    INDEX dec_ctProperties;             // number of properties

    CEventHandlerEntry *dec_aeheHandlers;  // array of event handlers
    INDEX dec_ctHandlers;                  // number of handlers

    CEntityResource *dec_aerResources; // array of resources
    INDEX dec_ctResources;             // number of resources

    char *dec_strName;                  // descriptive name of the class
    char *dec_strIconFileName;          // filename of texture or thumbnail
    INDEX dec_iID;                      // class ID

    CLibEntityClass *dec_pdecBase;      // pointer to the base class

    CEntity *(*dec_New)(void);          // pointer to function that creates a member of class
    void (*dec_OnInitClass)(void);      // pointer to function for connecting DLL
    void (*dec_OnEndClass)(void);       // pointer to function for disconnecting DLL
    void (*dec_OnPrecache)(CLibEntityClass *pdec, INDEX iUser);       // function for precaching data
    void (*dec_OnWorldInit)(CWorld *pwoWorld);    // function called on world initialization
    void (*dec_OnWorldTick)(CWorld *pwoWorld);    // function called for each tick
    void (*dec_OnWorldRender)(CWorld *pwoWorld);  // function called for each rendering
    void (*dec_OnWorldEnd)(CWorld *pwoWorld);     // function called on world cleanup

  public:
    //! Get event from array.
    inline const CLibEntityEvent* GetEvent(INDEX iEvent) const
    {
      if (iEvent > 0 && iEvent < dec_ctEvents) {
        return dec_adeeEvents[iEvent];
      }

      return NULL;
    }

    //! Get number of events.
    inline INDEX GetEventCount() const
    {
      return dec_ctEvents;
    }

    //! Get property from array.
    inline const CEntityProperty &GetProperty(INDEX iProperty) const
    {
      ASSERT(iProperty > 0 && iProperty < dec_ctProperties);
      return dec_aepProperties[iProperty];
    }

    //! Get number of properties.
    inline INDEX GetPropertyCount() const
    {
      return dec_ctProperties;
    }

    //! Get handler from array.
    inline const CEventHandlerEntry &GetHandler(INDEX iHandler) const
    {
      ASSERT(iHandler > 0 && iHandler < dec_ctHandlers);
      return dec_aeheHandlers[iHandler];
    }

    //! Get number of handlers.
    inline INDEX GetHandlerCount() const
    {
      return dec_ctHandlers;
    }

    //! Get resource from array.
    inline CEntityResource &GetResource(INDEX iResource) const
    {
      ASSERT(iResource > 0 && iResource < dec_ctResources);
      return dec_aerResources[iResource];
    }

    //! Get number of resources.
    inline INDEX GetResourceCount() const
    {
      return dec_ctResources;
    }

    //! Get descriptive name of the class.
    inline const char *GetName() const
    {
      return dec_strName;
    }

    //! Get the thumbnail texture.
    inline const char *GetIconFilename() const
    {
      return dec_strIconFileName;
    }

    //! Get class id.
    inline INDEX GetId () const
    {
      return dec_iID;
    }

    //! Get pointer to entity class we based on.
    inline const CLibEntityClass *GetBaseClass() const
    {
      return dec_pdecBase;
    }

    //! Get pointer to entity event from its code
    class CLibEntityEvent *EventForId(SLONG slEvent) const;

    //! Get pointer to entity property from its packed identifier.
    class CEntityProperty *PropertyForId(ULONG idEntityProperty, CEntityProperty::Type eType = CEntityProperty::EPT_INVALID) const;

    //! Get pointer to entity property from its name.
    class CEntityProperty *PropertyForName(const CTString &strPropertyName) const;

    //! Get event handler given state and event code.
    CEntity::pEventHandler HandlerForStateAndEvent(SLONG slState, SLONG slEvent) const;
    
    //! Get event handler name for given state.
    const char *HandlerNameForState(SLONG slState) const;
    
    //! Get derived class override for given state.
    SLONG GetOverridenState(SLONG slState) const;

    //! Get pointer to resource from its identifier and type.
    class CEntityResource *ResourceForId(ULONG idEntityResource, enum CResource::Type eType = CResource::TYPE_INVALID);

    //! Get pointer to resource from the resources.
    class CEntityResource *ResourceForPointer(void *pv);

    //! Universal precache method.
    void Precache(SLONG idEntityResource, CResource::Type eType = CResource::TYPE_INVALID, INDEX iUser = -1);
    
    //! Precache 'model' resource.
    void PrecacheModel(SLONG idEntityResource);
    
    //! Precache 'modelcfg' resource.
    void PrecacheModelConfig(SLONG idEntityResource);
    
    //! Precache 'texture' resource.
    void PrecacheTexture(SLONG idEntityResource);
    
    //! Precache 'sound' resource.
    void PrecacheSound(SLONG idEntityResource);
    
    //! Precache 'class' resource.
    void PrecacheClass(SLONG idEntityResource, INDEX iUser = -1);
};

/* rcg10062001 */
#if (defined _MSC_VER)
 #define DECLSPEC_DLLEXPORT _declspec (dllexport)
#else
 #define DECLSPEC_DLLEXPORT
#endif

// Macro for defining entity class DLL structures
#define ENTITY_CLASSDEFINITION(classname, basename, descriptivename, iconfilename, id)\
  extern "C" DECLSPEC_DLLEXPORT CLibEntityClass classname##_DLLClass; \
  CLibEntityClass classname##_DLLClass = {                            \
    classname##_events,                                               \
    classname##_eventsct,                                             \
    classname##_properties,                                           \
    classname##_propertiesct,                                         \
    classname##_handlers,                                             \
    classname##_handlersct,                                           \
    classname##_resources,                                            \
    classname##_resourcesct,                                          \
    descriptivename,                                                  \
    iconfilename,                                                     \
    id,                                                               \
    &basename##_DLLClass,                                             \
    &classname##_New,                                                 \
    &classname##_OnInitClass,                                         \
    &classname##_OnEndClass,                                          \
    &classname##_OnPrecache,                                          \
    &classname##_OnWorldInit,                                         \
    &classname##_OnWorldTick,                                         \
    &classname##_OnWorldRender,                                       \
    &classname##_OnWorldEnd                                           \
  };\
  SYMBOLLOCATOR(classname##_DLLClass)

#define ENTITY_CLASSDEFINITION_BASE(classname, id)                    \
  extern "C" DECLSPEC_DLLEXPORT CLibEntityClass classname##_DLLClass; \
  CLibEntityClass classname##_DLLClass = {                            \
    NULL,0, NULL,0, NULL,0, NULL, 0, "", "", id,                      \
    NULL, NULL,NULL,NULL,NULL, NULL,NULL,NULL,NULL                    \
  }
  
#endif /* include-once check. */