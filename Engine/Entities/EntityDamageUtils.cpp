/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityCollision.h>

#include <Core/Math/Float.h>

#include <Engine/World/World.h>
#include <Engine/World/WorldRayCasting.h>

#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/StaticStackArray.cpp>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Methods for inflicting damage to entities directly or around certain areas.

// Apply some damage directly to one entity.
void CEntity::InflictDirectDamage(CEntity *penToDamage, CEntity *penInflictor, INDEX iDamageType,
                                  INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If any of the entities are not allowed to execute now
  if (!IsAllowedForPrediction()
      || !penToDamage->IsAllowedForPrediction()
      || !penInflictor->IsAllowedForPrediction())
  {
    // do nothing
    return;
  }

  // If significant damage
  if (iDamageAmount > 0) {
    penToDamage->ReceiveDamage(penInflictor, iDamageType, iDamageAmount, vHitPoint, vDirection);
  }
}

// Find intensity of current entity at given distance
static inline INDEX IntensityAtDistance(INDEX iDamageAmount, FLOAT fHotSpotRange, 
                                        FLOAT fFallOffRange, FLOAT fDistance)
{
  // If further than fall-off range then intensity is zero
  if (fDistance > fFallOffRange) {
    return 0;
    
  // If closer than hot-spot range then intensity is maximum
  } else if (fDistance < fHotSpotRange) {
    return iDamageAmount;
    
  // If between fall-off and hot-spot range then interpolate
  } else {
    return iDamageAmount * (fFallOffRange - fDistance) / (fFallOffRange - fHotSpotRange);
  }
}

// Check if a range damage can hit given model entity
static BOOL CheckModelRangeDamage(CEntity &en, const FLOAT3D &vCenter, FLOAT &fMinD, FLOAT3D &vHitPos)
{
  CCollisionInfo *pci = en.en_pciCollisionInfo;
  if (pci == NULL) {
    return FALSE;
  }

  // Create 3 points along entity
  const FLOATmatrix3D &mR = en.en_mRotation;
  const FLOAT3D &vO = en.en_plPlacement.pl_PositionVector;
  FLOAT3D avPoints[3];
  INDEX ctSpheres = pci->ci_absSpheres.Count();
  
  if (ctSpheres < 1) {
    return FALSE;
  }
  
  avPoints[1] = pci->ci_absSpheres[ctSpheres-1].ms_vCenter * mR + vO;
  avPoints[2] = pci->ci_absSpheres[0].ms_vCenter * mR + vO;
  avPoints[0] = (avPoints[1] + avPoints[2]) * 0.5f;

  // Check if any point can be hit
  BOOL bCanHit = FALSE;
  for (INDEX i = 0; i < 3; i++) {
    CCastRay crRay(&en, avPoints[i], vCenter);
    crRay.cr_ttHitModels = CCastRay::TT_NONE;     // Only brushes block the damage
    crRay.cr_bHitTranslucentPortals = FALSE;
    crRay.cr_bPhysical = TRUE;
    en.en_pwoWorld->CastRay(crRay);
    if (crRay.cr_penHit == NULL) {
      bCanHit = TRUE;
      break;
    }
  }

  // If none can be hit then skip this entity
  if (!bCanHit) {
    return FALSE;
  }

  // Find minimum distance
  fMinD = UpperLimit(0.0f);
  vHitPos = vO;
  
  // For each sphere
  FOREACHINSTATICARRAY(pci->ci_absSpheres, CMovingSphere, itms)
  {
    // Project it
    itms->ms_vRelativeCenter0 = itms->ms_vCenter * en.en_mRotation+vO;
    FLOAT fD = (itms->ms_vRelativeCenter0-vCenter).Length() - itms->ms_fR;
    if (fD < fMinD) {
      fMinD = Min(fD, fMinD);
      vHitPos = itms->ms_vRelativeCenter0;
    }
  }
  
  if (fMinD < 0) {
    fMinD = 0;
  }
  return TRUE;
}

// Check if a range damage can hit given brush entity
static BOOL CheckBrushRangeDamage(CEntity &en, const FLOAT3D &vCenter, FLOAT &fMinD, FLOAT3D &vHitPos)
{
  // Don't actually check for brushes, doesn't have to be too exact
  const FLOAT3D &vO = en.en_plPlacement.pl_PositionVector;
  fMinD = (vO-vCenter).Length();
  vHitPos = vO;
  return TRUE;
}

// Apply some damage to all entities in some range.
void CEntity::InflictRangeDamage(CEntity *penInflictor, INDEX iDamageType, INDEX iDamageAmount, 
                                 const FLOAT3D &vCenter, FLOAT fHotSpotRange, FLOAT fFallOffRange)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If any of the entities are not allowed to execute now then do nothing
  if (!IsAllowedForPrediction() || !penInflictor->IsAllowedForPrediction()) {
    return;
  }

  // Find entities in the range
  TDynamicContainer<CEntity> cenInRange;
  FindEntitiesInRange(FLOATaabbox3D(vCenter, fFallOffRange), cenInRange, TRUE);

  // For each entity in container
  FOREACHINDYNAMICCONTAINER(cenInRange, CEntity, iten) 
  {
    CEntity &en = *iten;
    // If entity is not allowed to execute now then do nothing
    if (!en.IsAllowedForPrediction()) {
      continue;
    }

    // If can be hit
    FLOAT3D vHitPos;
    FLOAT fMinD;
    if ((en.en_RenderType == RT_MODEL || en.en_RenderType == RT_EDITORMODEL || 
          en.en_RenderType == RT_SKAMODEL || en.en_RenderType == RT_SKAEDITORMODEL)
        && CheckModelRangeDamage(en, vCenter, fMinD, vHitPos)
        || (en.en_RenderType == RT_BRUSH) && CheckBrushRangeDamage(en, vCenter, fMinD, vHitPos))
    {

      // Find damage ammount
      FLOAT fAmount = IntensityAtDistance(iDamageAmount, fHotSpotRange, fFallOffRange, fMinD);

      // If significant then inflict damage to it
      if (fAmount > 0) {
        en.ReceiveDamage(penInflictor, iDamageType, fAmount, vHitPos, (vHitPos-vCenter).Normalize());
      }
    }
  }
}

// Apply some damage to all entities in a box (this doesn't test for obstacles).
void CEntity::InflictBoxDamage(CEntity *penInflictor, INDEX iDamageType,
                               INDEX iDamageAmount, const FLOATaabbox3D &box)
{
  ASSERT(GetFPUPrecision() == FPT_24BIT);

  // If any of the entities are not allowed to execute now then do nothing
  if (!IsAllowedForPrediction() || !penInflictor->IsAllowedForPrediction()) {
    return;
  }

  // Find entities in the range
  TDynamicContainer<CEntity> cenInRange;
  FindEntitiesInRange(box, cenInRange, TRUE);

  // For each entity in container
  FOREACHINDYNAMICCONTAINER(cenInRange, CEntity, iten)
  {
    CEntity &en = *iten;
    //ASSERT(en.en_pciCollisionInfo!=NULL);  // assured by FindEntitiesInRange()
    if (en.en_pciCollisionInfo == NULL) {
      continue;
    }
    CCollisionInfo *pci = en.en_pciCollisionInfo;
    // If entity is not allowed to execute now then do nothing
    if (!en.IsAllowedForPrediction()) {
      continue;
    }

    // If significant damage then inflict damage to it
    // TODO: Fix it! Move this check!
    if (iDamageAmount > 0) {
      en.ReceiveDamage(penInflictor, iDamageType, iDamageAmount, box.Center(), 
                       (box.Center() - en.GetPlacement().pl_PositionVector).Normalize());
    }
  }
}
