/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityProperties.h>
#include <Core/Base/ErrorReporting.h>
#include <Engine/Entities/Precaching.h>
#include <Core/Base/Translation.h>
#include <Core/Base/CRCTable.h>
#include <Core/Modules/Module.h>

#include <Engine/Resources/ResourceManager.h>

// Default constructor.
CEntityClass::CEntityClass(void)
{
  ec_fnmModule.Clear();
  _pModule = NULL;
  ec_pdecDLLClass = NULL;
}

// Constructor for a fixed class.
CEntityClass::CEntityClass(class CLibEntityClass *pdecLibClass)
{
  ec_pdecDLLClass = pdecLibClass;
  _pModule = NULL;
  ec_fnmModule.Clear();
}

// Destructor.
CEntityClass::~CEntityClass(void)
{
  Clear();
}

/////////////////////////////////////////////////////////////////////
// Reference counting functions
void CEntityClass::AddReference(void)
{
  if (this != NULL) {
    Reference();
  }
};

void CEntityClass::RemReference(void)
{
  if (this != NULL) {
    _pResourceMgr->Release(this);
  }
};

// Clear the object.
void CEntityClass::Clear(void)
{
  // If the DLL is loaded
  if (_pModule != NULL && ec_pdecDLLClass != NULL) {
    // Detach the DLL
    ec_pdecDLLClass->dec_OnEndClass();

    // Release all resources needed by the DLL
    ReleaseResources();
  }

  ec_pdecDLLClass = NULL;
  _pModule = NULL;
  ec_fnmModule.Clear();
}

// Check that all properties have been properly declared.
void CEntityClass::CheckClassProperties(void)
{
// do nothing in release version
#ifndef NDEBUG
  // For all classes in hierarchy of this entity
  for (CLibEntityClass *pdecLibClass1 = ec_pdecDLLClass; 
      pdecLibClass1 != NULL; 
      pdecLibClass1 = pdecLibClass1->dec_pdecBase)
  {
    // For all properties
    for (INDEX iProperty1 = 0; iProperty1 < pdecLibClass1->dec_ctProperties; iProperty1++) 
    {
      const CEntityProperty &epProperty1 = pdecLibClass1->dec_aepProperties[iProperty1];

      // For all classes in hierarchy of this entity
      for (CLibEntityClass *pdecLibClass2 = ec_pdecDLLClass;
          pdecLibClass2 != NULL;
          pdecLibClass2 = pdecLibClass2->dec_pdecBase) 
      {
        // For all properties
        for (INDEX iProperty2 = 0; iProperty2 < pdecLibClass2->dec_ctProperties; iProperty2++)
        {
          const CEntityProperty &epProperty2 = pdecLibClass2->dec_aepProperties[iProperty2];

          // The two properties must not have same id unless they are same property
          ASSERTMSG(&epProperty1 == &epProperty2 || epProperty1.GetId() != epProperty2.GetId(), 
                    "No two properties may have same id!");
        }
      }
    }
  }

  // For all classes in hierarchy of this entity
  for (CLibEntityClass *pdecLibClass1 = ec_pdecDLLClass;
      pdecLibClass1 != NULL;
      pdecLibClass1 = pdecLibClass1->dec_pdecBase) 
  {
    // For all resources
    for (INDEX iResource1 = 0; iResource1 < pdecLibClass1->dec_ctResources; iResource1++) 
    {
      CEntityResource &ecResource1 = pdecLibClass1->dec_aerResources[iResource1];

      // For all classes in hierarchy of this entity
      for (CLibEntityClass *pdecLibClass2 = ec_pdecDLLClass;
          pdecLibClass2!=NULL;
          pdecLibClass2 = pdecLibClass2->dec_pdecBase)
      {
        // For all resources
        for (INDEX iResource2 = 0; iResource2 < pdecLibClass2->dec_ctResources; iResource2++)
        {
          CEntityResource &ecResource2 = pdecLibClass2->dec_aerResources[iResource2];
          // The two resources must not have same id unless they are same resources
          ASSERTMSG(&ecResource1 == &ecResource2 || ecResource1.GetId() != ecResource2.GetId(),
                    "No two entity resources may have same id!");
        }
      }
    }
  }
#endif
}

// Construct a new member of the class.
CEntity *CEntityClass::New(void)
{
  // The DLL must be loaded
  ASSERT(ec_pdecDLLClass != NULL);
  
  // Ask the DLL class to call the 'operator new' in the scope where the class is declared
  CEntity *penNew = ec_pdecDLLClass->dec_New();
  
  // Remember this class as class of the entity
  AddReference();
  penNew->en_pecClass = this;
  
  // Set all properties to default
  penNew->SetDefaultProperties();

  // Return it
  return penNew;
}

// Obtain all entries from resource table.
void CEntityClass::ObtainResources_t(void)
{
  // For each resource
  for (INDEX iResource = 0; iResource < ec_pdecDLLClass->dec_ctResources; iResource++)
  {
    // If not precaching all
    if (gam_iPrecachePolicy < PRECACHE_ALL) {
      CEntityResource &ec = ec_pdecDLLClass->dec_aerResources[iResource];
      // If resource is not class then skip it
      if (ec.GetType() != CResource::TYPE_ENTITYCLASS) {
        continue;
      }
    }

    // Try to obtain the resource
    try {
      ec_pdecDLLClass->dec_aerResources[iResource].Obtain_t();
    } catch (char *) {
      // If in paranoia mode then fail
      if (gam_iPrecachePolicy==PRECACHE_PARANOIA) {
        throw;
        
      // If not in paranoia mode then ignore all errors
      } else {
        NOTHING;
      }
    }
  }
}

// Release all entries from resource table.
void CEntityClass::ReleaseResources(void)
{
  // For each resource
  for (INDEX iResource = 0; iResource < ec_pdecDLLClass->dec_ctResources; iResource++)
  {
    // Release the resource
    ec_pdecDLLClass->dec_aerResources[iResource].Release();
  }
}

// Read from stream.
void CEntityClass::Read_t(CTStream *istr)
{
  // Read the dll filename and class name from the stream
  CTFileName fnmModule;
  fnmModule.ReadFromText_t(*istr, "Package: ");
  CTString strClassName;
  strClassName.ReadFromText_t(*istr, "Class: ");

  if (fnmModule.FileExt() != ".ml") {
    ThrowF_t(TRANS("Cannot open '%s': Expected path to a Module Link file (.ml) under 'Package:'"), GetName());
  }

  _pModule = (FModule *)_pResourceMgr->Obtain_t(CResource::TYPE_MODULE, fnmModule);

  ec_fnmModule = fnmModule;

  // Get the pointer to the DLL class structure
  ec_pdecDLLClass = (CLibEntityClass *)GetProcAddress(_pModule->GetModuleHandle(), strClassName + "_DLLClass");
  
  // If class structure is not found
  if (ec_pdecDLLClass == NULL) {
    ec_fnmModule.Clear();
    
    // Report error
    ThrowF_t(TRANS("Class '%s' not found in entity class package file '%s'"), strClassName, fnmModule);
  }

  // Obtain all resources needed by the DLL
  {
    CTmpPrecachingNow tpn;
    ObtainResources_t();
  }

  // Attach the DLL
  ec_pdecDLLClass->dec_OnInitClass();

  // Check that the class properties have been properly declared
  CheckClassProperties();
}

// Write to stream.
void CEntityClass::Write_t(CTStream *ostr)
{
  ASSERTALWAYS("Do not write CEntityClass objects!");
}

// Get amount of memory used by this object
SLONG CEntityClass::GetUsedMemory(void)
{
  // We don't know exact memory used, but we want to enumerate them
  return 0;
}

// Check if this kind of objects is auto-freed
BOOL CEntityClass::IsAutoFreed(void) 
{ 
  return FALSE;
};

// Gather the CRC of the file
void CEntityClass::AddToCRCTable(void)
{
  const CTFileName &fnm = GetName();
  // If already added then do nothing
  if (CRCT_IsFileAdded(fnm)) {
    return;
  }

  // Add the file itself
  CRCT_AddFile_t(fnm);
  
  // Add its DLL
  CRCT_AddFile_t(ec_fnmModule);
}

// Get pointer to entity property from its name.
class CEntityProperty *CEntityClass::PropertyForName(const CTString &strPropertyName) 
{
  return ec_pdecDLLClass->PropertyForName(strPropertyName);
}

// Get pointer to entity property from its packed identifier.
class CEntityProperty *CEntityClass::PropertyForId(ULONG ulId, CEntityProperty::Type eType) 
{
  return ec_pdecDLLClass->PropertyForId(ulId, eType);
}

// Get event handler for given state and event code.
CEntity::pEventHandler CEntityClass::HandlerForStateAndEvent(SLONG slState, SLONG slEvent)
{
  return ec_pdecDLLClass->HandlerForStateAndEvent(slState, slEvent);
}

// Get pointer to resource from its identifier.
class CEntityResource *CEntityClass::ResourceForId(ULONG ulId, CResource::Type eType)
{
  return ec_pdecDLLClass->ResourceForId(ulId, eType);
}

// Get pointer to resource from the resource.
class CEntityResource *CEntityClass::ResourceForPointer(void *pv)
{
  return ec_pdecDLLClass->ResourceForPointer(pv);
}

// Universal precache method.
void CEntityClass::Precache(SLONG idEntityResource, CResource::Type eType, INDEX iUser)
{
  return ec_pdecDLLClass->Precache(idEntityResource, eType, iUser);
}

UBYTE CEntityClass::GetType() const
{
  return TYPE_ENTITYCLASS;
}