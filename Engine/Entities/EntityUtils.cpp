/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityClass.h>

// Check if entity is of given class
BOOL CEntity::IsOfClass(CEntity *pen, const char *pstrClassName)
{
  if (pen == NULL || pstrClassName == NULL) {
    return FALSE;
  }
  if (strcmp(pen->GetClass()->ec_pdecDLLClass->dec_strName, pstrClassName) == 0) {
    return TRUE;
  } else {
    return FALSE;
  }
}

// Check if entity is of given class by pointer.
BOOL CEntity::IsOfClass(CEntity *pen, class CLibEntityClass *pdec)
{
  return pen != NULL && pen->en_pecClass->ec_pdecDLLClass == pdec;
};

// Check if entities is of same class
BOOL CEntity::IsOfSameClass(CEntity *pen1, CEntity *pen2)
{
  if (pen1 == NULL || pen2 == NULL) {
    return FALSE;
  }
  
  if (pen1->GetClass()->ec_pdecDLLClass == pen2->GetClass()->ec_pdecDLLClass) {
    return TRUE;
  } else {
    return FALSE;
  }
}

// Check if entity is of given class or derived from
BOOL CEntity::IsDerivedFromClass(CEntity *pen, const char *pstrClassName)
{
  if (pen == NULL || pstrClassName == NULL) {
    return FALSE;
  }
  
  // For all classes in hierarchy of the entity
  for (CLibEntityClass *pdecLibClass = pen->GetClass()->ec_pdecDLLClass;
      pdecLibClass != NULL;
      pdecLibClass = pdecLibClass->dec_pdecBase) 
  {
    // If it is the wanted class then it is derived
    if (strcmp(pdecLibClass->dec_strName, pstrClassName) == 0) {
      return TRUE;
    }
  }
  
  // Otherwise, it is not derived
  return FALSE;
}

// Check if entity is of given class or derived from
BOOL CEntity::IsDerivedFromClass( CEntity *pen, class CLibEntityClass *pdec)
{
  if (pen == NULL) {
    return FALSE;
  }

  // For all classes in hierarchy of the entity
  for (CLibEntityClass *pdecLibClass = pen->GetClass()->ec_pdecDLLClass;
    pdecLibClass != NULL;
    pdecLibClass = pdecLibClass->dec_pdecBase)
  {
    // if it is the wanted class then it is derived
    if (pdecLibClass == pdec) {
      return TRUE;
    }
  }

  // Otherwise, it is not derived
  return FALSE;
}

BOOL CEntity::GetTargetChain(TDynamicContainer<CEntity> &cenOutput, CEntity *penStart)
{
  ASSERT(penStart != NULL);
  ASSERT(cenOutput.Count() == 0);

  for (CEntity *it = penStart; it != NULL; it = it->GetTarget())
  {
    // If recursive chain...
    if (cenOutput.IsMember(it)) {
      return FALSE;
    }

    cenOutput.Add(it); // Add the current one.
  }

  return TRUE;
}