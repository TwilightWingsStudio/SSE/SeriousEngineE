/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/Entity.h>
#include <Engine/Entities/LastPositions.h>
#include <Engine/Entities/EntityProperties.h>
#include <Engine/Entities/EntityClass.h>
#include <Core/Base/Translation.h>
#include <Core/Base/Timer.h>
#include <Core/Base/Console.h>

#include <Core/Base/ListIterator.inl>
#include <Core/Base/ErrorReporting.h>
#include <Engine/World/World.h>
#include <Core/Math/Float.h>
#include <Core/Math/Quaternion.h>

#include <Engine/Brushes/BrushArchive.h>
#include <Engine/Light/LightSource.h>
#include <Engine/Entities/ShadingInfo.h>
#include <Engine/Models/ModelObject.h>
#include <Engine/Sound/SoundObject.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/Selection.cpp>


// TODO: Separate CPointerRemapping to its own files

//! Class for remapping pointers to some entity.
class CPointerRemapping
{
  public:
    CEntity *pr_penOriginal;
    CEntity *pr_penCopy;

    //! Dummy method used by a TStaticArray.
    inline void Clear(void) {};
};

static TStaticArray<CPointerRemapping> _aprRemaps;
static BOOL _bRemapPointersToNULLs = TRUE;
extern BOOL _bReinitEntitiesWhileCopying = TRUE;
static BOOL _bMirrorAndStretch = FALSE;
static FLOAT _fStretch = 1.0f;
static enum WorldMirrorType _wmtMirror = WMT_NONE;
extern INDEX _ctPredictorEntities;

// Mirror a placement of one entity
static void MirrorAndStretchPlacement(CPlacement3D &pl)
{
  ASSERT(_wmtMirror == WMT_X || _wmtMirror == WMT_Y || _wmtMirror == WMT_Z || _wmtMirror == WMT_NONE);

  // If there should be mirror
  if (_wmtMirror != WMT_NONE)
  {
    // Make rotation matrix for the placement
    FLOATmatrix3D m;
    MakeRotationMatrix(m, pl.pl_OrientationAngle);
    
    // Get row vectors, with object x flipped
    FLOAT3D vX(-m(1, 1), m(1, 2), m(1, 3));
    FLOAT3D vY(-m(2, 1), m(2, 2), m(2, 3));
    FLOAT3D vZ(-m(3, 1), m(3, 2), m(3, 3));

    // Flip needed axis
    switch (_wmtMirror)
    {
      case WMT_X: 
        pl.pl_PositionVector(1) = -pl.pl_PositionVector(1);
        vX = -vX; 
        break;
        
      case WMT_Y: 
        pl.pl_PositionVector(2) = -pl.pl_PositionVector(2);
        vY = -vY; 
        break;
        
      case WMT_Z: 
        pl.pl_PositionVector(3) = -pl.pl_PositionVector(3);
        vZ = -vZ; 
        break;
        
      default: ASSERT(FALSE);
    }

    // Compose matrix back from the vectors
    m(1, 1) = vX(1);  m(2, 1) = vY(1);  m(3, 1) = vZ(1);
    m(1, 2) = vX(2);  m(2, 2) = vY(2);  m(3, 2) = vZ(2);
    m(1, 3) = vX(3);  m(2, 3) = vY(3);  m(3, 3) = vZ(3);
    
    // Decompose matrix into angles
    DecomposeRotationMatrix(pl.pl_OrientationAngle, m);
  }
  pl.pl_PositionVector *= _fStretch;
}

CEntity *CEntity::FindRemappedEntityPointer(CEntity *penOriginal)
{
  // If original is null then copy is null
  if (penOriginal == NULL) {
    return NULL;
  }

  // Try to find valid remap
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      if (itpr->pr_penOriginal == penOriginal) {
        return itpr->pr_penCopy;
      }
    }
  }
  
  // If none found, copy is either null or original
  return _bRemapPointersToNULLs? NULL : penOriginal;
}

/*
 * Copy entity from another entity of same class.
 * NOTES:
 *  - Doesn't copy placement, it must be done on creation.
 *  - Entity must be initialized afterwards.
 */
void CEntity::Copy(CEntity &enOther, ULONG ulFlags)
{
  BOOL bRemapPointers = ulFlags & COPY_REMAP;
  BOOL bMakePredictor = ulFlags & COPY_PREDICTOR;

  // Copy base class data
  en_RenderType     = enOther.en_RenderType;
  en_ulPhysicsFlags = enOther.en_ulPhysicsFlags;
  en_ulCollisionFlags = enOther.en_ulCollisionFlags;
  en_ulFlags        = enOther.en_ulFlags &
    ~(ENF_SELECTED|ENF_FOUNDINGRIDSEARCH|ENF_VALIDSHADINGINFO|ENF_INRENDERING);
  en_ulSpawnFlags   = enOther.en_ulSpawnFlags;

  // If prediction then set flags
  if (bMakePredictor) {
    en_ulFlags = (en_ulFlags & ~(ENF_PREDICTED | ENF_PREDICTABLE)) | ENF_PREDICTOR;
    enOther.en_ulFlags = (enOther.en_ulFlags & ~ENF_PREDICTOR) | ENF_PREDICTED;
  } else {
    en_ulFlags = (en_ulFlags & ~(ENF_PREDICTED | ENF_PREDICTABLE | ENF_PREDICTOR));
  }

  // If this is a brush
  if (enOther.en_RenderType == RT_BRUSH || en_RenderType == RT_FIELDBRUSH)
  {
    // There must be no existing brush
    ASSERT(en_pbrBrush == NULL);
    
    // Create a new empty brush in the brush archive of current world
    en_pbrBrush = en_pwoWorld->wo_baBrushes.ba_abrBrushes.New();
    en_pbrBrush->br_penEntity = this;
    
    // Copy the brush
    if (_bMirrorAndStretch) {
      en_pbrBrush->Copy(*enOther.en_pbrBrush, _fStretch, _wmtMirror != WMT_NONE);
    } else {
      en_pbrBrush->Copy(*enOther.en_pbrBrush, 1.0f, FALSE);
    }
  
  // If this is a terrain
  } else if (enOther.en_RenderType == RT_TERRAIN) {
    #pragma message(">> CEntity::Copy")
    ASSERT(FALSE);
    
  
  }

  // If this is a model
  if (enOther.en_RenderType == RT_MODEL || en_RenderType == RT_EDITORMODEL)
  {
    // If will not initialize
    if (!(ulFlags&COPY_REINIT)) {
      // Create a new model object
      en_pmoModelObject = new CModelObject;
      en_psiShadingInfo = new CShadingInfo;
      en_ulFlags &= ~ENF_VALIDSHADINGINFO;
      
      // Copy it
      en_pmoModelObject->Copy(*enOther.en_pmoModelObject);
    }
    
  // If this is ska model
  } else if (enOther.en_RenderType == RT_SKAMODEL || en_RenderType == RT_SKAEDITORMODEL) {
       // Create a new model instance
      en_psiShadingInfo = new CShadingInfo;
      en_ulFlags &= ~ENF_VALIDSHADINGINFO;
      en_pmiModelInstance = CreateModelInstance("Temp");
      
      // Copy it
      GetModelInstance()->Copy(*enOther.GetModelInstance());
  }

  // Copy the parent pointer
  if (bRemapPointers) {
    n_pParent = FindRemappedEntityPointer(enOther.GetParentEntity());
  } else {
    n_pParent = enOther.GetParentEntity();
  }
  
  // If the entity has a parent
  if (GetParentEntity() != NULL) {
    // Create relative offset
    en_plRelativeToParent = en_plPlacement;
    en_plRelativeToParent.AbsoluteToRelativeSmooth(GetParentEntity()->en_plPlacement);
    
    // Link to parent
    GetParent()->GetChildren().AddTail(n_lnInParent);
  }

  // Copy the derived class properties
  CopyEntityProperties(enOther, ulFlags);

  // If prediction
  if (bMakePredictor) {
    // Create cross-links and add to containers
    SetPredictionPair(&enOther);
    enOther.SetPredictionPair(this);
    enOther.en_pwoWorld->wo_cenPredicted.Add(&enOther);
    enOther.en_pwoWorld->wo_cenPredictor.Add(this);
    
    // Copy last positions
    if (enOther.en_plpLastPositions != NULL) {
      en_plpLastPositions = new CLastPositions(*enOther.en_plpLastPositions);
    }
  }
}

#include <Core/Base/ByteArray.h>

// Copy one entity property from property of another entity.
void CEntity::CopyOneProperty(CEntityProperty &epPropertySrc, CEntityProperty &epPropertyDest,
                              CEntity &enOther, ULONG ulFlags)
{

// A helper macro
#define COPYPROPERTY(type)                                            \
  (type &)ENTITYPROPERTY(this, epPropertyDest.GetOffset(), type)      \
    = (type &)ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), type)

  // TODO: Convert this switch to static dictionary of keys, if it's possible
  // Depending on the property type
  switch (epPropertySrc.GetType())
  {
    // If it is BOOL then copy BOOL
    case CEntityProperty::EPT_BOOL:
      COPYPROPERTY(INDEX); // WARNING: But there index, wtf?
      break;
      
    // If it is INDEX
    case CEntityProperty::EPT_INDEX:
    case CEntityProperty::EPT_ENUM:
    case CEntityProperty::EPT_FLAGS:
    case CEntityProperty::EPT_ANIMATION:
    case CEntityProperty::EPT_ILLUMINATIONTYPE:
    case CEntityProperty::EPT_COLOR:
    case CEntityProperty::EPT_ANGLE:
      // copy INDEX
      COPYPROPERTY(INDEX);
      break;
     
    // If it is FLOAT then copy FLOAT
    case CEntityProperty::EPT_FLOAT:
    case CEntityProperty::EPT_RANGE:
      COPYPROPERTY(FLOAT);
      break;
      
    // If it is STRING then copy STRING
    case CEntityProperty::EPT_STRING:
    case CEntityProperty::EPT_STRINGTRANS:
      COPYPROPERTY(CTString);
      break;
      
    // If it is FILENAME then copy FILENAME
    case CEntityProperty::EPT_FILENAME:
      COPYPROPERTY(CTFileName);
      break;
      
    // If it is FILENAMENODEP then copy FILENAMENODEP
    case CEntityProperty::EPT_FILENAMENODEP:
      COPYPROPERTY(CTFileNameNoDep);
      break;
      
    // If it is FLOATAABBOX3D then copy FLOATAABBOX3D
    case CEntityProperty::EPT_FLOATAABBOX3D:
      COPYPROPERTY(FLOATaabbox3D);
      break;
      
    // If it is FLOATMATRIX3D then copy FLOATMATRIX3D
    case CEntityProperty::EPT_FLOATMATRIX3D:
      COPYPROPERTY(FLOATmatrix3D);
      break;
      
    // If it is FLOAT3D then copy FLOAT3D
    case CEntityProperty::EPT_VEC3F:
      COPYPROPERTY(FLOAT3D);
      break;
      
    // If it is ANGLE3D then copy ANGLE3D
    case CEntityProperty::EPT_ANGLE3D:
      COPYPROPERTY(ANGLE3D);
      break;
      
    // If it is QUATERNION3D then copy ANGLE3D
    case CEntityProperty::EPT_FLOATQUAT3D:
      COPYPROPERTY(FLOATquat3D);
      break;
      
    // If it is FLOATplane3D then copy FLOATplane3D
    case CEntityProperty::EPT_FLOATplane3D:
      COPYPROPERTY(FLOATplane3D);
      break;
      
    // If it is ENTITYPTR
    case CEntityProperty::EPT_ENTITYPTR:
      // Remap and copy the pointer
      if (ulFlags & COPY_REMAP)
      {
        ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CEntityPointer) =
                        FindRemappedEntityPointer(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CEntityPointer));

      // Copy CEntityPointer
      } else {
        COPYPROPERTY(CEntityPointer);
      }
      break;
      
    // If it is MODELOBJECT then copy CModelObject
    case CEntityProperty::EPT_MODELOBJECT:
      ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CModelObject).
                            Copy(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CModelObject));
      
      // Model objects are not copied, but should be initialized in Main()
      break;
    
    // If it is MODELINSTANCE
    case CEntityProperty::EPT_MODELINSTANCE:
      // Copy CModelInstance
      ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CModelInstance).
                            Copy(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CModelInstance));
      
      // Model objects are not copied, but should be initialized in Main()
      break;
    
    // If it is ANIMOBJECT then copy CAnimObject
    case CEntityProperty::EPT_ANIMOBJECT:
      ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CAnimObject).
                            Copy(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CAnimObject));
      break;
    
    // If it is SOUNDOBJECT then copy CSoundObject
    case CEntityProperty::EPT_SOUNDOBJECT:
      {
        if (!(ulFlags & COPY_PREDICTOR)){
          CSoundObject &so = ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CSoundObject);
          so.Copy(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CSoundObject));
          so.so_penEntity = this;
        }
      }
      break;
      
    // If it is CPlacement3D then copy CPlacement3D
    case CEntityProperty::EPT_PLACEMENT3D:
      COPYPROPERTY(CPlacement3D);
      break;
      
    // [SEE]
    case CEntityProperty::EPT_SQUAD: {
      COPYPROPERTY(SQUAD);
    } break;
      
    // [SEE]
    case CEntityProperty::EPT_DOUBLE: {
      COPYPROPERTY(DOUBLE);
    } break; 
      
    // [SEE]
    case CEntityProperty::EPT_BYTEARRAY: {
      CByteArray &ba = ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CByteArray);
      ba.Copy(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CByteArray));
    } break;
      
    // [SEE]
    case CEntityProperty::EPT_CUSTOMDATA: {
      CopyCustomData(enOther, epPropertyDest);
    } break;

    // [SEE]
    case CEntityProperty::EPT_VARIANT: {
      CVariant &var = ENTITYPROPERTY(this, epPropertyDest.GetOffset(), CVariant);
      var.Copy(ENTITYPROPERTY(&enOther, epPropertySrc.GetOffset(), CVariant));
    } break;

    default:
      ASSERTALWAYS("Unknown property type");
  }
}

// Copy entity properties from another entity of same class.
void CEntity::CopyEntityProperties(CEntity &enOther, ULONG ulFlags)
{
  // Other entity must have same class
  ASSERT(enOther.en_pecClass == en_pecClass);

  // For all classes in hierarchy of this entity
  for (CLibEntityClass *pdecLibClass = en_pecClass->ec_pdecDLLClass;
      pdecLibClass != NULL;
      pdecLibClass = pdecLibClass->dec_pdecBase)
  {
    // For all properties
    for (INDEX iProperty = 0; iProperty < pdecLibClass->dec_ctProperties; iProperty++)
    {
      CEntityProperty &epProperty = pdecLibClass->dec_aepProperties[iProperty];
      CopyOneProperty(epProperty, epProperty, enOther, ulFlags);
    }
  }
}

// Copy container of entities from another world to this one and select them.
void CWorld::CopyEntities(CWorld &woOther, TDynamicContainer<CEntity> &cenToCopy, 
                          CEntitySelection &senCopied, const CPlacement3D &plOtherSystem)
{
  INDEX ctEntities = cenToCopy.Count();
  if (ctEntities <= 0) {
    return;
  }

  CSetFPUPrecision FPUPrecision(FPT_24BIT);

  ULONG ulCopyFlags = COPY_REMAP;
  if (_bReinitEntitiesWhileCopying) {
    ulCopyFlags |= COPY_REINIT;
  };

  // Create array of pointer remaps
  _aprRemaps.Clear();
  _aprRemaps.New(ctEntities);


  // PASS 1: create entities

  // For each entity to copy
  INDEX iRemap = 0;
  {
    FOREACHINDYNAMICCONTAINER(cenToCopy, CEntity, itenToCopy)
    {
      CEntity &enToCopy = *itenToCopy;

      CEntity *penNew;
      CPlacement3D plEntity;
      
      // Transform the entity placement from the system of other world
      plEntity = enToCopy.en_plPlacement;
      plEntity.RelativeToAbsolute(plOtherSystem);

      // Mirror and stretch placement if needed
      if (_bMirrorAndStretch) {
        MirrorAndStretchPlacement(plEntity);
      }

      /*
       * NOTE: We must use CreateEntity_t() overload with class name instead with class pointer
       * because the entity class must be obtained by the target world too!
       */
       
      // Try to
      try {
        // Create an entity of same class as the one to copy
        penNew = CreateEntity_t(plEntity, enToCopy.en_pecClass->GetName());
      // If not successfull
      } catch (char *strError) {
        (void)strError;
        ASSERT(FALSE);    // This should not happen
        FatalError(TRANS("Cannot CopyEntity():\n%s"), strError);
      }

      // Remember its remap pointer
      _aprRemaps[iRemap].pr_penOriginal = &enToCopy;
      _aprRemaps[iRemap].pr_penCopy = penNew;
      iRemap++;
    }
  }

  // PASS 2: copy properties

  // For each of the created entities
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      CEntity *penOriginal = itpr->pr_penOriginal;
      CEntity *penCopy = itpr->pr_penCopy;

      // Copy the entity from its original
      penCopy->Copy(*penOriginal, ulCopyFlags);
      
      // If this is a brush then update the bounding boxes of the brush
      if (penOriginal->en_RenderType == CEntity::RT_BRUSH || penOriginal->en_RenderType == CEntity::RT_FIELDBRUSH) {
        penCopy->en_pbrBrush->CalculateBoundingBoxes();
      }
      
      if (_bMirrorAndStretch) {
        penCopy->MirrorAndStretch(_fStretch, _wmtMirror != WMT_NONE);
      }
    }
  }

  // PASS 3: initialize

  // For each of the created entities
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      CEntity *penOriginal = itpr->pr_penOriginal;
      CEntity *penCopy = itpr->pr_penCopy;
      if (_bReinitEntitiesWhileCopying) {
        // Init the new copy
        penCopy->Initialize();
      } else {
        penCopy->UpdateSpatialRange();
        penCopy->FindCollisionInfo();
        
        // Set spatial clasification
        penCopy->FindSectorsAroundEntity();
      }
    }
  }

  // PASS 4: find shadows

  // For each of the created entities
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      CEntity *penOriginal = itpr->pr_penOriginal;
      CEntity *penCopy = itpr->pr_penCopy;

      // if this is a brush then find possible shadow layers near affected area
      if (penCopy->en_RenderType == CEntity::RT_BRUSH || penCopy->en_RenderType == CEntity::RT_FIELDBRUSH) {
        FindShadowLayers(penCopy->en_pbrBrush->GetFirstMip()->bm_boxBoundingBox);
      }

      // If this is a light source
      {
        CLightSource *pls = penCopy->GetLightSource();
        if (pls != NULL) {
          // Find all shadow maps that should have layers from this light source
          pls->FindShadowLayers(FALSE);
          
          // Update shadow map on terrains
          pls->UpdateTerrains();
        }
      }

      // Select it
      senCopied.Select(*penCopy);
    }
  }

  // Make sure someone doesn't reuse the remap array accidentially
  _aprRemaps.Clear();
}

// Copy one entity from another world into this one.
CEntity *CWorld::CopyOneEntity(CEntity &enToCopy, const CPlacement3D &plOtherSystem)
{
  // Prepare container for copying
  TDynamicContainer<CEntity> cenToCopy;
  cenToCopy.Add(&enToCopy);
  
  // Copy the entities in container
  CEntitySelection senCopied;
  CopyEntities(*enToCopy.en_pwoWorld, cenToCopy, senCopied, plOtherSystem);

  {
    FOREACHINDYNAMICCONTAINER(senCopied, CEntity, itenCopied)
    {
      return itenCopied;
    }
  }
  
  ASSERT(FALSE);
  return NULL;
}

// Copy all entities except one from another world to this one.
void CWorld::CopyAllEntitiesExceptOne(CWorld &woOther, CEntity &enExcepted, const CPlacement3D &plOtherSystem)
{
  // Prepare container for copying, without excepted entity
  TDynamicContainer<CEntity> cenToCopy;
  cenToCopy = woOther.wo_cenEntities;
  cenToCopy.Remove(&enExcepted);
  
  // Copy the entities in container and ignore the selection (we don't need it)
  CEntitySelection senCopied;
  CopyEntities(woOther, cenToCopy, senCopied, plOtherSystem);
  senCopied.Clear();
}


// Copy entity in world.
CEntity *CWorld::CopyEntityInWorld(CEntity &enOriginal, const CPlacement3D &plOtherEntity, 
                                    BOOL bWithDescendants /*= TRUE*/)
{
  // New entity
  CEntity *penNew;

  /*
   * NOTE: We must use CreateEntity_t() overload with class name instead with class pointer
   * because the entity class must be obtained by the target world too!
   */
   
  // Try to create an entity of same class as the one to copy
  try {
    penNew = CreateEntity_t(plOtherEntity, enOriginal.en_pecClass->GetName());
  // If not successfull
  } catch (char *strError) {
    (void)strError;
    ASSERT(FALSE);    // This should not happen
    FatalError(TRANS("Cannot CopyEntity():\n%s"), strError);
  }

  // Copy the entity from its original
  penNew->Copy(enOriginal, COPY_REINIT);
  
  // If this is a brush then update the bounding boxes of the brush
  if (enOriginal.en_RenderType == CEntity::RT_BRUSH || enOriginal.en_RenderType == CEntity::RT_FIELDBRUSH) {
    penNew->en_pbrBrush->CalculateBoundingBoxes();
  }
  
  // Init the new copy
  penNew->Initialize();

  // If this is a brush then find possible shadow layers near affected area
  if (penNew->en_RenderType == CEntity::RT_BRUSH || penNew->en_RenderType == CEntity::RT_FIELDBRUSH) {
    FindShadowLayers(penNew->en_pbrBrush->GetFirstMip()->bm_boxBoundingBox);
  }

  // If this is a light source then find all shadow maps that should have layers from this light source
  {
    CLightSource *pls = penNew->GetLightSource();
    if (pls != NULL) {
      pls->FindShadowLayers(FALSE);
    }
  }

  // If descendants should be copied too
  if (bWithDescendants)
  {
    // For each child of this entity
    {
      FOREACHINLIST(CEntity, n_lnInParent, enOriginal.GetChildren(), itenChild)
      {
        // Copy it relatively to the new entity
        CPlacement3D plChild = itenChild->en_plRelativeToParent;
        plChild.RelativeToAbsoluteSmooth(penNew->en_plPlacement);
        CEntity *penNewChild = CopyEntityInWorld(*itenChild, plChild, TRUE);
        
        // Add new child to its new parent
        penNewChild->SetParent(penNew);
      }
    }
  }

  return penNew;
}

// Mirror and stretch another world into this one
void CWorld::MirrorAndStretch(CWorld &woOriginal, FLOAT fStretch, enum WorldMirrorType wmt)
{
  _bMirrorAndStretch = TRUE;
  _fStretch = fStretch;
  _wmtMirror = wmt;

  // Clear this world
  Clear();

  // Make container for copying
  TDynamicContainer<CEntity> cenToCopy;
  cenToCopy = woOriginal.wo_cenEntities;
  
  // Dummy selection for copied entities, we don't need that info
  CEntitySelection senCopied;
  
  // Make mirroring placement
  CPlacement3D plOtherSystem;
  plOtherSystem = CPlacement3D(FLOAT3D(0, 0, 0), ANGLE3D(0, 0, 0));
  
  // Copy entities with mirror and stretch
  CopyEntities(woOriginal, cenToCopy, senCopied, plOtherSystem);

  // Update all links
  {
    CSetFPUPrecision FPUPrecision(FPT_53BIT);
    wo_baBrushes.LinkPortalsAndSectors();
  }

  _bMirrorAndStretch = FALSE;
}

// Copy entities for prediction.
void CWorld::CopyEntitiesToPredictors(TDynamicContainer<CEntity> &cenToCopy)
{
  INDEX ctEntities = cenToCopy.Count();
  if (ctEntities <= 0) {
    return;
  }

  extern INDEX cli_bReportPredicted;
  if (cli_bReportPredicted)
  {
    CDebugF(TRANS("Predicting %d entities:\n"), ctEntities);
    {
      FOREACHINDYNAMICCONTAINER(cenToCopy, CEntity, itenToCopy)
      {
        CEntity &enToCopy = *itenToCopy;
        CDebugF("  %s:%s\n", enToCopy.GetClass()->ec_pdecDLLClass->dec_strName, enToCopy.GetName().ConstData());
      }
    }
  }

  // Clear current tick to prevent timer setting from assertions
  TIME tmCurrentTickOld = _pTimer->CurrentTick();
  _pTimer->SetCurrentTick(0.0f);

  ULONG ulCopyFlags = COPY_REMAP|COPY_PREDICTOR;

  // Create array of pointer remaps
  _aprRemaps.Clear();
  _aprRemaps.New(ctEntities);

  
  // PASS 1: create entities

  // For each entity to copy
  INDEX iRemap = 0;
  {
    FOREACHINDYNAMICCONTAINER(cenToCopy, CEntity, itenToCopy)
    {
      CEntity &enToCopy = *itenToCopy;

      CEntity *penNew;

      // Create an entity of same class as the one to copy
      penNew = CreateEntity(enToCopy.en_plPlacement, enToCopy.en_pecClass);

      // Remember its remap pointer
      _aprRemaps[iRemap].pr_penOriginal = &enToCopy;
      _aprRemaps[iRemap].pr_penCopy = penNew;
      iRemap++;
      _ctPredictorEntities++;
    }
  }
  
  // Unfound pointers must be kept unremapped
  _bRemapPointersToNULLs = FALSE;

  
  // PASS 2: copy properties

  // For each of the created entities
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      CEntity *penOriginal = itpr->pr_penOriginal;
      CEntity *penCopy = itpr->pr_penCopy;

      // Copy the entity from its original
      penCopy->Copy(*penOriginal, ulCopyFlags);
      
      // If this is a brush
      if (penOriginal->en_RenderType == CEntity::RT_BRUSH || penOriginal->en_RenderType == CEntity::RT_FIELDBRUSH) {
        ASSERT(FALSE);  // should we allow prediction of brushes?
        // update the bounding boxes of the brush
        //penCopy->en_pbrBrush->CalculateBoundingBoxes();
      }
    }
  }

  
  // PASS 3: initialize

  // For each of the created entities
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      CEntity *penOriginal = itpr->pr_penOriginal;
      CEntity *penCopy = itpr->pr_penCopy;

      // Copy spatial classification
      penCopy->en_fSpatialClassificationRadius = penOriginal->en_fSpatialClassificationRadius;
      penCopy->en_boxSpatialClassification     = penOriginal->en_boxSpatialClassification;
      
      // Copy collision info
      penCopy->CopyCollisionInfo(*penOriginal);
      
      // For each sector around the original
      {
        FOREACHSRCOFDST(penOriginal->en_rdSectors, CBrushSector, bsc_rsEntities, pbsc)
          // Copy the link
          if (penOriginal->en_RenderType == CEntity::RT_BRUSH
                || penOriginal->en_RenderType == CEntity::RT_FIELDBRUSH
                || penOriginal->en_RenderType == CEntity::RT_TERRAIN)
          {  // brushes first
            AddRelationPairHeadHead(pbsc->bsc_rsEntities, penCopy->en_rdSectors);
          } else {
            AddRelationPairTailTail(pbsc->bsc_rsEntities, penCopy->en_rdSectors);
          }
        ENDFOR
      }
    }
  }

  
  // PASS 4: find shadows

  // For each of the created entities
  {
    FOREACHINSTATICARRAY(_aprRemaps, CPointerRemapping, itpr)
    {
      CEntity *penOriginal = itpr->pr_penOriginal;
      CEntity *penCopy = itpr->pr_penCopy;

      // If this is a brush
      if (penCopy->en_RenderType == CEntity::RT_BRUSH || penCopy->en_RenderType == CEntity::RT_FIELDBRUSH) {
        ASSERT(FALSE);  // should we allow prediction of brushes?
        // Find possible shadow layers near affected area
        //FindShadowLayers(penCopy->en_pbrBrush->GetFirstMip()->bm_boxBoundingBox);
      }

      // If this is a light source
      {
        CLightSource *pls = penCopy->GetLightSource();
        if (pls != NULL) {
          // Find all shadow maps that should have layers from this light source
          pls->FindShadowLayers(FALSE);
        }
      }
    }
  }

  // Make sure someone doesn't reuse the remap array accidentially
  _aprRemaps.Clear();

  _bRemapPointersToNULLs = TRUE;

  // Return current tick
  _pTimer->SetCurrentTick(tmCurrentTickOld);
}
