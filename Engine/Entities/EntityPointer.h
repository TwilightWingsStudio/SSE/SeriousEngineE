/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

// WARN: Affects the order of inclusion
#include <Engine/Entities/Entity.h>

#ifndef SE_INCL_ENTITYPOINTER_H
#define SE_INCL_ENTITYPOINTER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Smart pointer to entity objects, does the book-keeping for reference counting.
class CEntityPointer
{
  public:
    CEntity *ep_pen;  // the pointer itself

  public:
    // All standard smart pointer functions are defined as inlines in Entity.h
    // (due to strange order of inclusion needed for events and enums)
    
    //! Dummy constructor.
    inline CEntityPointer(void)
    {
      ep_pen = NULL;
    }
    
    //! Destructor.
    inline ~CEntityPointer(void)
    {
      ep_pen->RemReference();
    };
    
    //! Copy constructor.
    inline CEntityPointer(const CEntityPointer &penOther)
      : ep_pen(penOther.ep_pen)
    {
      ep_pen->AddReference();
    };

    //! Constructor that makes it pointing to entity.
    inline CEntityPointer(CEntity *pen) : ep_pen(pen)
    {
      ep_pen->AddReference();
    };
    
    //! Assigning CEntity * as value.
    inline const CEntityPointer &operator=(CEntity *pen)
    {
      pen->AddReference();    // must first add, then remove!
      ep_pen->RemReference();
      ep_pen = pen;
      return *this;
    }
    
    //! Assigning value of another CEntityPointer.
    inline const CEntityPointer &operator=(const CEntityPointer &penOther)
    {
      penOther.ep_pen->AddReference();    // must first add, then remove!
      ep_pen->RemReference();
      ep_pen = penOther.ep_pen;
      return *this;
    }
    
    //! Gettings members with arrow.
    inline CEntity* operator->(void) const
    {
      return ep_pen;
    }
    
    //! Get raw pointer to the entity.
    inline operator CEntity*(void) const
    {
      return ep_pen;
    }
    
    //! Get reference to the entity.
    inline CEntity& operator*(void) const
    {
      return *ep_pen; 
    }
};

#endif /* include-once check. */