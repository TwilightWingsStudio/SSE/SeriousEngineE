/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/PlayerCharacter.h>
#include <Core/Base/Timer.h>
#include <Core/IO/Stream.h>
#include <Engine/Network/NetworkMessage.h>

typedef HRESULT __stdcall CoCreateGuid_t(UBYTE *pguid);

// Get a GUID from system
static void GetGUID(UBYTE aub[16])
{
  HINSTANCE hOle32Lib = NULL;
  CoCreateGuid_t *pCoCreateGuid = NULL;

  try {
    // Load ole32
    hOle32Lib = ::LoadLibraryA("ole32.dll");
    if (hOle32Lib == NULL) {
      ThrowF_t(TRANS("Cannot load ole32.dll."));
    }

    // Find GUID function
    pCoCreateGuid = (CoCreateGuid_t*)GetProcAddress(hOle32Lib, "CoCreateGuid");
    if (pCoCreateGuid == NULL) {
      ThrowF_t(TRANS("Cannot find CoCreateGuid()."));
    }

    // Create the guid
    HRESULT hres = pCoCreateGuid(&aub[0]);

    // Check for success
    if (hres != S_OK) {
      ThrowF_t(TRANS("CoCreateGuid(): Error 0x%08x"), hres);
    }

    // Free the ole32 library
    FreeLibrary(hOle32Lib);

  } catch(char *strError) {
    FatalError(TRANS("Cannot make GUID for a player:\n%s"), strError);
  }
}

// Default constructor -- no character.
CPlayerCharacter::CPlayerCharacter(void)
  : pc_strName("<invalid player>"), pc_strTeam("")
{
  memset(pc_aubAppearance, 0, MAX_PLAYERAPPEARANCE);
}

// Create a new character with its name.
CPlayerCharacter::CPlayerCharacter(const CTString &strName)
  : pc_strName(strName), pc_strTeam("")
{
  // If the name passed to constructor is empty string then make this an unnamed player
  if (strName == "") {
    pc_strName = "<unnamed player>";
  }
  
  // Create the guid
  GetGUID(pc_UUID._bytes);
  memset(pc_aubAppearance, 0, MAX_PLAYERAPPEARANCE);
}

// Load character from a file.
void CPlayerCharacter::Load_t(const CTFileName &fnFile)
{
  CTFileStream strm;
  strm.Open_t(fnFile);
  Read_t(&strm);
  strm.Close();
}

// Save character to a file.
void CPlayerCharacter::Save_t(const CTFileName &fnFile)
{
  CTFileStream strm;
  strm.Create_t(fnFile);
  Write_t(&strm);
  strm.Close();
}

// Read character from a stream.
void CPlayerCharacter::Read_t(CTStream *pstr)
{
  pstr->ExpectID_t("PLC4");
  (*pstr) >> pc_strName>>pc_strTeam;
  pstr->Read_t(pc_UUID.Data(), 16);
  pstr->Read_t(pc_aubAppearance, sizeof(pc_aubAppearance));
}

// Write character into a stream.
void CPlayerCharacter::Write_t(CTStream *pstr)
{
  pstr->WriteID_t("PLC4");
  (*pstr) << pc_strName<<pc_strTeam;
  pstr->Write_t(pc_UUID.Data(), 16);
  pstr->Write_t(pc_aubAppearance, sizeof(pc_aubAppearance));
}

// Get character name.
const CTString &CPlayerCharacter::GetName(void) const 
{
  return pc_strName; 
};

// Get character name for text output.
const CTString CPlayerCharacter::GetNameForPrinting(void) const
{
  CTString strName(pc_strName);
  // get rid of newlines in the name
  strName.ReplaceSubstr("\n", "");
  strName.ReplaceSubstr("\r", "");
  return "^o" + pc_strName + "^r";
}

// Set character name.
void CPlayerCharacter::SetName(CTString strName) 
{ 
  // Limit string length to 20 characters not including decorated text control codes
  // strName.TrimRightNaked(20); // TODO: !!!! needs checking
  pc_strName = strName; 
};

// Get character team.
const CTString &CPlayerCharacter::GetTeam(void) const
{
  return pc_strTeam; 
}

const CTString CPlayerCharacter::GetTeamForPrinting(void) const
{
  return "^o" + pc_strTeam + "^r"; 
}

// Set character team.
void CPlayerCharacter::SetTeam(CTString strTeam)
{
  // Limit string length to 20 characters not including decorated text control codes
  // strTeam.TrimRightNaked(20); // TODO: !!!! needs checking
  pc_strTeam = strTeam; 
}

// Assignment operator.
CPlayerCharacter &CPlayerCharacter::operator=(const CPlayerCharacter &pcOther)
{
  ASSERT(this != NULL && &pcOther != NULL);
  pc_strName = pcOther.pc_strName;
  pc_strTeam = pcOther.pc_strTeam;
  pc_UUID = pcOther.pc_UUID;
  memcpy(pc_aubAppearance, pcOther.pc_aubAppearance, MAX_PLAYERAPPEARANCE);
  return *this;
};

// Comparison operator.
BOOL CPlayerCharacter::operator==(const CPlayerCharacter &pcOther) const
{
  return pc_UUID == pcOther.pc_UUID;
};

// Write character into a stream.
CTStream &operator<<(CTStream &strm, CPlayerCharacter &pc)
{
  pc.Write_t(&strm);
  return strm;
};

// Read character from a stream.
CTStream &operator>>(CTStream &strm, CPlayerCharacter &pc)
{
  pc.Read_t(&strm);
  return strm;
};

//////////////////////////////////////////////////////////////////////
// Message operations

// Write character into a network message.
CNetworkMessage &operator<<(CNetworkMessage &nm, CPlayerCharacter &pc)
{
  nm << pc.pc_strName << pc.pc_strTeam;
  nm.Write(pc.pc_UUID.Data(), 16);
  nm.Write(pc.pc_aubAppearance, MAX_PLAYERAPPEARANCE);
  return nm;
};

// Read character from a network message.
CNetworkMessage &operator>>(CNetworkMessage &nm, CPlayerCharacter &pc)
{
  nm >> pc.pc_strName >> pc.pc_strTeam;
  nm.Read(pc.pc_UUID.Data(), 16);
  nm.Read(pc.pc_aubAppearance, MAX_PLAYERAPPEARANCE);
  return nm;
};
