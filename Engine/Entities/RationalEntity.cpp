/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/CRC.h>
#include <Core/IO/Stream.h>
#include <Engine/Entities/Entity.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Entities/EntityProperties.h>

#include <Engine/World/World.h>

// Allocation step for state stack.
#define STATESTACK_ALLOCATIONSTEP 5

// Constructor.
CRationalEntity::CRationalEntity(void)
{
}

// Calculate physics for moving.

//! Clear entity's movement properties.
void CRationalEntity::ClearMovingTemp(void)
{
}

//! Logic before entity movement.
void CRationalEntity::PreMoving(void)
{
}

//! Logic during entity movement.
void CRationalEntity::DoMoving(void)
{
}

//! Logic after entity movement.
void CRationalEntity::PostMoving(void)
{
}

// Create a checksum value for sync-check
void CRationalEntity::ChecksumForSync(ULONG &ulCRC, INDEX iExtensiveSyncCheck)
{
  CEntity::ChecksumForSync(ulCRC, iExtensiveSyncCheck);

  if (iExtensiveSyncCheck > 0) {
    CRC_AddFLOAT(ulCRC, en_timeTimer);
    CRC_AddLONG(ulCRC, en_stslStateStack.Count());
  }
  
  if (iExtensiveSyncCheck > 0) {
  }
}

// Dump sync data to text file
void CRationalEntity::DumpSync_t(CTStream &strm, INDEX iExtensiveSyncCheck)
{
  CEntity::DumpSync_t(strm, iExtensiveSyncCheck);
  if (iExtensiveSyncCheck > 0) {
    strm.FPrintF_t("en_timeTimer:  %g(%08x)\n", en_timeTimer, (ULONG&)en_timeTimer);
    strm.FPrintF_t("en_stslStateStack.Count(): %d\n", en_stslStateStack.Count());
  }
  strm.FPrintF_t("en_fHealth:    %g(%08x)\n", en_fHealth, (ULONG&)en_fHealth);
}

// Copy entity from another entity of same class.
void CRationalEntity::Copy(CEntity &enOther, ULONG ulFlags)
{
  CLiveEntity::Copy(enOther, ulFlags);
  if (!(ulFlags & COPY_REINIT)) {
    CRationalEntity *prenOther = (CRationalEntity *)(&enOther);
    en_timeTimer = prenOther->en_timeTimer;
    en_stslStateStack = prenOther->en_stslStateStack;
    if (prenOther->en_lnInTimers.IsLinked()) {
      en_pwoWorld->AddTimer(this);
    }
  }
}

// Read from stream.
void CRationalEntity::Read_t(CTStream *istr)
{
  CLiveEntity::Read_t(istr);
  (*istr) >> en_timeTimer;
  
  // If waiting for thinking then add to list of thinkers
  if (en_timeTimer != THINKTIME_NEVER) {
    en_pwoWorld->AddTimer(this);
  }

  // Read the state stack
  en_stslStateStack.Clear();
  en_stslStateStack.SetAllocationStep(STATESTACK_ALLOCATIONSTEP);
  INDEX ctStates;
  (*istr) >> ctStates;
  for (INDEX iState = 0; iState < ctStates; iState++)
  {
    SLONG slStateId;
    (*istr) >> slStateId;
    en_stslStateStack.Push() = RemapStateInStack(slStateId);
  }
}

// Write to stream.
void CRationalEntity::Write_t(CTStream *ostr)
{
  CLiveEntity::Write_t(ostr);
  
  // If not currently waiting for thinking
  if (!en_lnInTimers.IsLinked()) {
    // Set dummy thinking time as a flag for later loading
    en_timeTimer = THINKTIME_NEVER;
  }
  
  (*ostr) << en_timeTimer;
  
  // Write the state stack
  (*ostr)<<en_stslStateStack.Count();
  for (INDEX iState = 0; iState < en_stslStateStack.Count(); iState++) 
  {
    (*ostr)<<en_stslStateStack[iState];
  }
}

// Set next timer event to occur at given moment time.
void CRationalEntity::SetTimerAt(TIME timeAbsolute)
{
  // Must never set think back in time, except for special 'never' time
  ASSERTMSG(timeAbsolute>_pTimer->CurrentTick() || timeAbsolute==THINKTIME_NEVER, "Do not SetThink() back in time!");
  
  // Set the timer
  en_timeTimer = timeAbsolute;

  // add to world's list of timers if neccessary
  if (en_timeTimer != THINKTIME_NEVER) {
    en_pwoWorld->AddTimer(this);
  } else {
    if (en_lnInTimers.IsLinked()) {
      en_lnInTimers.Remove();
    }
  }
}

// Set next timer event to occur after given time has elapsed.
void CRationalEntity::SetTimerAfter(TIME timeDelta)
{
  // Set the execution for the moment that is that much ahead of the current tick
  SetTimerAt(_pTimer->CurrentTick() + timeDelta);
}

// Cancel eventual pending timer.
void CRationalEntity::UnsetTimer(void)
{
  en_timeTimer = THINKTIME_NEVER;
  if (en_lnInTimers.IsLinked()) {
    en_lnInTimers.Remove();
  }
}

// Unwind stack to a given state.
void CRationalEntity::UnwindStack(SLONG slThisState)
{
  // For each state on the stack (from top to bottom)
  for (INDEX iStateInStack = en_stslStateStack.Count() - 1; iStateInStack >= 0; iStateInStack--) {
    // If it is the state then unwind to it
    if (en_stslStateStack[iStateInStack] == slThisState) {
      en_stslStateStack.PopUntil(iStateInStack);
      return;
    }
  }
  
  // The state must be on the stack
  ASSERTALWAYS("Unwinding to unexisting state!");
}

// Jump to a new state.
void CRationalEntity::Jump(SLONG slThisState, SLONG slTargetState, BOOL bOverride, const CEntityEvent &eeInput)
{
  // Unwind the stack to this state
  UnwindStack(slThisState);
  
  // Set the new topmost state
  if (bOverride) {
    slTargetState = en_pecClass->ec_pdecDLLClass->GetOverridenState(slTargetState);
  }
  en_stslStateStack[en_stslStateStack.Count() - 1] = slTargetState;
  
  // Handle the given event in the new state
  HandleEvent(eeInput);
};

//Call a subautomaton.
void CRationalEntity::Call(SLONG slThisState, SLONG slTargetState, BOOL bOverride, const CEntityEvent &eeInput)
{
  // Unwind the stack to this state
  UnwindStack(slThisState);
  
  // Push the new state to stack
  if (bOverride) {
    slTargetState = en_pecClass->ec_pdecDLLClass->GetOverridenState(slTargetState);
  }
  en_stslStateStack.Push() = slTargetState;
  
  // Handle the given event in the new state
  HandleEvent(eeInput);
};

// Return from a subautomaton.
void CRationalEntity::Return(SLONG slThisState, const CEntityEvent &eeReturn)
{
  // Unwind the stack to this state
  UnwindStack(slThisState);
  
  // Pop one state from the stack
  en_stslStateStack.PopUntil(en_stslStateStack.Count() - 2);
  
  // Handle the given event in the new topmost state
  HandleEvent(eeReturn);
};

// Print stack to debug output
const char *CRationalEntity::PrintStackDebug(void)
{
  _RPT2(_CRT_WARN, "-- stack of '%s'@%gs\n", GetName(), _pTimer->CurrentTick());

  INDEX ctStates = en_stslStateStack.Count();
  for (INDEX iState = ctStates - 1; iState >= 0; iState--) {
    SLONG slState = en_stslStateStack[iState];
    _RPT2(_CRT_WARN, "0x%08x %s\n", slState, en_pecClass->ec_pdecDLLClass->HandlerNameForState(slState));
  }
  _RPT0(_CRT_WARN, "----\n");
  return "ok";
}

// Handle an event - return false if event was not handled.
BOOL CRationalEntity::HandleEvent(const CEntityEvent &ee)
{
  // For each state on the stack (from top to bottom)
  for (INDEX iStateInStack=en_stslStateStack.Count() - 1; iStateInStack >= 0; iStateInStack--) {
    // Try to find a handler in that state
    pEventHandler pehHandler = HandlerForStateAndEvent(en_stslStateStack[iStateInStack], ee.ee_slEvent);
    
    // if there is a handler then call the function
    if (pehHandler != NULL) {
      BOOL bHandled = (this->*pehHandler)(ee);
      // If the event was successfully handled then return that it was handled
      if (bHandled) {
        return TRUE;
      }
    }
  }

  // If no transition was found, the event was not handled
  return FALSE;
}

// Called after creating and setting its properties.
void CRationalEntity::OnInitialize(const CEntityEvent &eeInput)
{
  // Make sure entity doesn't destroy itself during intialization
  CEntityPointer penThis = this;

  // Do not think
  en_timeTimer = THINKTIME_NEVER;
  if (en_lnInTimers.IsLinked()) {
    en_lnInTimers.Remove();
  }

  // Initialize state stack
  en_stslStateStack.Clear();
  en_stslStateStack.SetAllocationStep(STATESTACK_ALLOCATIONSTEP);

  en_stslStateStack.Push() = 1;   // start state is always state with number 1

  // Call the main function of the entity
  HandleEvent(eeInput);
}

// Called before releasing entity.
void CRationalEntity::OnEnd(void)
{
  // Cancel eventual pending timer
  UnsetTimer();
}