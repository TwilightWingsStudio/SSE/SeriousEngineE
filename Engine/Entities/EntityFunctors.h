/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENTITYFUNCTORS_H
#define SE_INCL_ENTITYFUNCTORS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

template<typename ReturnType>
struct TEntityFunctor : public TObjectFunctor<ReturnType, CEntity>
{
  virtual ReturnType operator()(CEntity *pen) const = 0;
};

struct ENGINE_API Func_IsOfClass : public TEntityFunctor<BOOL>
{
  CLibEntityClass *_plec;

  //! Constructor.
  Func_IsOfClass(CLibEntityClass *plec)
  {
    _plec = plec;
  }

  //! Logic.
  BOOL operator()(CEntity *pen) const override;
};

struct ENGINE_API Func_IsDerivedFromClass : public TEntityFunctor<BOOL>
{
  CLibEntityClass *_plec;

  //! Constructor.
  Func_IsDerivedFromClass(CLibEntityClass *plec)
  {
    _plec = plec;
  }

  //! Logic.
  BOOL operator()(CEntity *pen) const override;
};

struct ENGINE_API Func_IsOfClassStr : public TEntityFunctor<BOOL>
{
  CTString _strClassName;

  //! Constructor.
  Func_IsOfClassStr(const CTString &strClassName)
  {
    _strClassName = strClassName;
  }

  //! Logic.
  BOOL operator()(CEntity *pen) const override;
};

struct ENGINE_API Func_IsDerivedFromClassStr : public TEntityFunctor<BOOL>
{
  CTString _strClassName;

  //! Constructor.
  Func_IsDerivedFromClassStr(const CTString &strClassName)
  {
    _strClassName = strClassName;
  }

  //! Logic.
  BOOL operator()(CEntity *pen) const override;
};

struct ENGINE_API Func_IsInSpot : public TEntityFunctor<BOOL>
{
  FLOAT3D _vPos;
  FLOAT _fRange;

  //! Constructor.
  Func_IsInSpot(FLOAT3D vPos, FLOAT fRange)
  {
    _vPos = vPos;
    _fRange = fRange;
  }

  //! Logic.
  BOOL operator()(CEntity *pen) const override;
};

struct ENGINE_API Func_IsInBox : public TEntityFunctor<BOOL>
{
  FLOATaabbox3D _boxArea;

  Func_IsInBox(FLOATaabbox3D boxArea)
  {
    _boxArea = boxArea;
  }

  Func_IsInBox(FLOAT3D vPos, FLOATaabbox3D boxArea)
  {
    _boxArea = boxArea;
    _boxArea += vPos;
  }

  //! Logic.
  BOOL operator()(CEntity *pen) const override;
};

#endif