/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Engine/Entities/EntityProperties.h>
#include <Engine/Entities/Precaching.h>
#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>

#include <Engine/Meshes/ModelConfiguration.h>
#include <Core/Templates/StaticArray.cpp>

#include <Engine/Resources/ResourceManager.h>
#include <Engine/Entities/EntityClass.h>
#include <Engine/Models/ModelData.h>
#include <Engine/Models/Model_internal.h>
#include <Engine/Sound/SoundData.h>
#include <Engine/Graphics/Texture.h>

// Obtain the resource.
void CEntityResource::Obtain_t(void)
{
  // If obtained then Just add to CRC
  if (er_pvPointer != NULL) {
    AddToCRCTable();
    return; // Do not obtain again
  }
  
  er_pInStock = _pResourceMgr->Obtain_t(er_eType, er_fnmResource);
  
  // Specific check.
  if (er_eType == CResource::TYPE_MODELCONFIGURATION) {
    ASSERT(er_pModelConfiguration != NULL);
    ASSERT(er_pModelConfiguration->GetModelInstance() != NULL);
  }

  // If not already loaded and should not be precaching now then report warning
  if (er_pInStock->GetReferenceCount() <= 1 && !_precache_bNowPrecaching) {
    CWarningF(TRANS("Not precached: (0x%08X)'%s'\n"), this->er_ulId, (CTString&)er_fnmResource);
  }
  
  //CDebugF(TRANS("Precaching NOW: (0x%08X)'%s'\n"), this->er_ulId, er_fnmResource);

  // Add to CRC
  AddToCRCTable();
}

// Obtain the resource with immediate error reports.
void CEntityResource::ObtainWithCheck(void)
{
  try {
    Obtain_t();
  } catch(char *strError) {
    FatalError("%s", strError);
  }
}

// Add resource to crc table
void CEntityResource::AddToCRCTable(void)
{
  // If not obtained then do nothing
  if (er_pvPointer == NULL) {
    return;
  }

  // Add it
  switch (er_eType)
  {
    case CResource::TYPE_TEXTURE: er_ptdTexture->AddToCRCTable();     break;
    case CResource::TYPE_MODEL:   er_pmdModel->AddToCRCTable();       break;
    case CResource::TYPE_SOUND:   er_psdSound->AddToCRCTable();       break;
    case CResource::TYPE_ENTITYCLASS:   er_pecEntityClass->AddToCRCTable(); break;
  }
}

// Release the resource.
void CEntityResource::Release(void)
{
  // If the component is not obtained then don't release it
  if (er_pvPointer == NULL) {
    return;
  }

  _pResourceMgr->Release(er_pInStock);

  // Released
  er_pvPointer = NULL;
}