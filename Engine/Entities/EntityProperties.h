/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENTITYPROPERTIES_H
#define SE_INCL_ENTITYPROPERTIES_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/FileName.h>
#include <Engine/Entities/Entity.h>


/////////////////////////////////////////////////////////////////////
// Classes and macros for defining enums for entity properties

//! Enum value with a name of some enum entity property.
class CEntityPropertyEnumValue
{
  public:
    INDEX epev_iValue;      // value
    char *epev_strName;     // descriptive name of the enum value (for editor)
};

//! List of named enum values for an entity property.
class CEntityPropertyEnumType
{
  public:
    CEntityPropertyEnumValue *epet_aepevValues;     // array of values
    INDEX epet_ctValues;                            // number of values
    
  public:
    //! Convert value of an enum to its name
    ENGINE_API const char *NameForValue(INDEX iValue);
};

#define EP_ENUMBEG(typename) extern _declspec (dllexport) CEntityPropertyEnumValue typename##_values[] = {
#define EP_ENUMVALUE(value, name) {value, name}
#define EP_ENUMEND(typename) }; CEntityPropertyEnumType typename##_enum = { \
  typename##_values, sizeof(typename##_values)/sizeof(CEntityPropertyEnumValue ) }

// types that are used only for properties.
// range, edited visually as sphere around entity
typedef FLOAT RANGE;

// type of illumination (for lights that illuminate through polygons)
typedef INDEX ILLUMINATIONTYPE;

// type of light animation
typedef INDEX ANIMATION;

// CTString browsed like a file - doesn't cause dependencies
typedef CTString CTFileNameNoDep;

// CTString that gets translated to different languages
typedef CTString CTStringTrans;


/////////////////////////////////////////////////////////////////////
// Classes and macros for defining entity properties

enum EntityPropertyFlags
{
  //! Not visualized in perspective view (for ranges).
  EPROPF_HIDEINPERSPECTIVE = (1UL << 0),

  //! Will be serialized only during simulation.
  EPROPF_SIMULATIONONLY    = (1UL << 1),
};

//! Class for defining entity property.
class ENGINE_API CEntityProperty
{
  public:
    enum Type
    {
      /* IMPORTANT: Take care not to renumber the property types, since it would ruin loading
       * of previously saved data like levels etc.! Always give a new type new number, and
       * never change existing number, nor reuse old ones!
       * Update 'next free number' comment when adding new type, but not when removing some!
       */
      EPT_PARENT = 9998,          // used internaly in WED for setting entity's parents
      EPT_SPAWNFLAGS = 9999,      // used internaly in WED for spawn flags property
      EPT_INVALID = 0,            // Error!
      EPT_ENUM = 1,               // enum xxx, INDEX with descriptions
      EPT_BOOL = 2,               // BOOL
      EPT_FLOAT = 3,              // FLOAT
      EPT_COLOR = 4,              // COLOR
      EPT_STRING = 5,             // CTString
      EPT_RANGE = 6,              // RANGE FLOAT edited as a sphere around the entity
      EPT_ENTITYPTR = 7,          // CEntityPointer
      EPT_FILENAME = 8,           // CTFileName
      EPT_INDEX = 9,              // INDEX
      EPT_ANIMATION = 10,         // ANIMATION, INDEX edited by animation names
      EPT_ILLUMINATIONTYPE = 11,  // ILLUMINATIONTYPE, INDEX edited by illumination names
      EPT_FLOATAABBOX3D = 12,     // FLOATaabbox3D
      EPT_ANGLE = 13,             // ANGLE, INDEX edited in degrees
      EPT_VEC3F = 14,             // FLOAT3D
      EPT_ANGLE3D = 15,           // ANGLE3D
      EPT_FLOATplane3D = 16,      // FLOATplane3D
      EPT_MODELOBJECT = 17,       // CModelObject
      EPT_PLACEMENT3D = 18,       // PLACEMENT3D
      EPT_ANIMOBJECT = 19,        // CAnimObject
      EPT_FILENAMENODEP = 20,     // CTString browsed like a file - doesn't cause dependencies
      EPT_SOUNDOBJECT = 21,       // CSoundObject
      EPT_STRINGTRANS = 22,       // CTString that gets translated to different languages
      EPT_FLOATQUAT3D = 23,       // FLOATquat3D
      EPT_FLOATMATRIX3D = 24,     // FLOATmatrix3D
      EPT_FLAGS = 25,             // flags - ULONG bitfield with names for each field
      EPT_MODELINSTANCE = 26,
      // [SEE]
      EPT_RESERVED1 = 27,         // LC (EPT_ZONEFLAGS i.e. ULONG)
      EPT_SQUAD = 28,             // Rev and LC (EPT_ZONEFLAGS_EX i.e. UQUAD)
      EPT_RESERVED2 = 29,         // Rev
      EPT_DOUBLE = 30,            // DOUBLE
      EPT_BYTEARRAY = 31,
      EPT_CUSTOMDATA = 32,
      EPT_VARIANT = 33,
      EPT_LAST = EPT_VARIANT,
      // next free number: 34
    } ep_eptType;                 // type of property
    
    CEntityPropertyEnumType *ep_pepetEnumType;   // if the type is EPT_ENUM or EPT_FLAGS

    ULONG ep_ulID;         // property ID for this class
    SLONG ep_slOffset;     // offset of the property in the class
    char *ep_strName;      // descriptive name of the property (for editor)
    ULONG ep_ulFlags;      // additional flags for the property
    char  ep_chShortcut;   // shortcut key for selecting the property in editor (0 for none)
    COLOR ep_colColor;     // property color, for various wed purposes (like target arrows)
    CVariant *ep_pLowerLimit;
    CVariant *ep_pUpperLimit;

  public:
    //! Constructor.
    CEntityProperty(Type eptType, CEntityPropertyEnumType *pepetEnumType, ULONG ulID, SLONG slOffset, 
                    char *strName, char chShortcut, COLOR colColor, ULONG ulFlags, CVariant *pLowerLimit, CVariant *pUpperLimit)
      : ep_eptType         (eptType       )
      , ep_pepetEnumType   (pepetEnumType )
      , ep_ulID            (ulID          )
      , ep_slOffset        (slOffset      )
      , ep_strName         (strName       )
      , ep_chShortcut      (chShortcut    )
      , ep_colColor        (colColor      )
      , ep_ulFlags         (ulFlags)
      , ep_pLowerLimit     (pLowerLimit)
      , ep_pUpperLimit     (pUpperLimit)
    {};
    
    //! Default constructor.
    CEntityProperty(void) {};
    
    inline Type GetType() const
    {
      return ep_eptType;
    }
    
    inline CEntityPropertyEnumType *GetEnumType() const
    {
      return ep_pepetEnumType;
    }
    
    inline ULONG GetId() const
    {
      return ep_ulID;
    }
    
    inline SLONG GetOffset() const
    {
      return ep_slOffset;
    }
    
    inline const char *GetName() const
    {
      return ep_strName;
    }
    
    inline ULONG GetFlags() const
    {
      return ep_ulFlags;
    }
    
    inline char GetShortcut() const
    {
      return ep_chShortcut;
    }
    
    inline COLOR GetColor() const
    {
      return ep_colColor;
    }

    inline const BOOL IsInteger() const
    {
      return GetType() == EPT_INDEX || GetType() == EPT_SQUAD;
    }

    inline const BOOL IsFloating() const
    {
      return GetType() == EPT_FLOAT || GetType() == EPT_RANGE || GetType() == EPT_ANGLE || GetType() == EPT_DOUBLE;
    }

    inline const CVariant *GetLowerLimit() const
    {
      return ep_pLowerLimit;
    }

    inline const CVariant *GetUpperLimit() const
    {
      return ep_pUpperLimit;
    }
};

// macro for accessing property inside an entity
#define ENTITYPROPERTY(entityptr, offset, type) (*((type *)(((UBYTE *)entityptr)+offset)))

#include <Engine/Entities/EntityResource.h>
#include <Engine/Entities/LibEntityClass.h>

inline ENGINE_API void ClearToDefault(FLOAT &f) { f = 0.0f; };
inline ENGINE_API void ClearToDefault(DOUBLE &f) { f = 0.0; };
inline ENGINE_API void ClearToDefault(INDEX &i) { i = 0; };
inline ENGINE_API void ClearToDefault(SQUAD &i) { i = 0; };
inline ENGINE_API void ClearToDefault(BOOL &b) { b = FALSE; };
inline ENGINE_API void ClearToDefault(CEntityPointer &pen) { pen = NULL; };
inline ENGINE_API void ClearToDefault(CTString &str) { str = ""; };
inline ENGINE_API void ClearToDefault(FLOATplane3D &pl) { pl = FLOATplane3D(FLOAT3D(0,1,0), 0); };
inline ENGINE_API void ClearToDefault(FLOAT3D &v) { v = FLOAT3D(0,1,0); };
inline ENGINE_API void ClearToDefault(COLOR &c) { c = 0xFFFFFFFF; };
inline ENGINE_API void ClearToDefault(CModelData *&pmd) { pmd = NULL; };
inline ENGINE_API void ClearToDefault(CTextureData *&pmt) { pmt = NULL; };
ENGINE_API void ClearToDefault(CByteArray& ba);
ENGINE_API void ClearToDefault(CVariant& var);

#endif /* include-once check. */