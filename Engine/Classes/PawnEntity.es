/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

/*
 * Entity that can be controlled by player or AI.
 */
4
%{
  #include "StdH.h"

  #include <Core/IO/Stream.h>
  #include <Core/Base/CRC.h>

  #include <Engine/Entities/InternalClasses.h>
%}

class export CPawnEntity : CMovableModelEntity {
name      "PawnEntity";
thumbnail "";
features "AbstractBaseClass";
properties:
  1 CEntityPointer en_penController,
  
 10 INDEX m_iHealth = 0,
 
 20 ANGLE3D m_aLocalRotation = FLOAT3D(0, 0, 0),
 21 ANGLE3D m_aLocalViewRotation = FLOAT3D(0, 0, 0),
 22 FLOAT3D m_vLocalTranslation = FLOAT3D(0, 0, 0),

{
  CPlacement3D en_plViewpoint;       // placement of view point relative to the entity
  CPlacement3D en_plLastViewpoint;   // last view point (used for lerping)
}

resources:
functions:

  BOOL IsPawn(void) const
  {
    return TRUE;
  }

  class CPawnControllerEntity *GetController(void)
  {
    return reinterpret_cast<CPawnControllerEntity *>(&*en_penController);
  }

  //! Copy entity from another entity of same class.
  export void Copy(CEntity &enOther, ULONG ulFlags)
  {
    CMovableModelEntity::Copy(enOther, ulFlags);

    // Copy fields.
    CPawnEntity *pOtherPawn = (CPawnEntity *)(&enOther);
    en_plViewpoint = pOtherPawn->en_plViewpoint;
    en_plLastViewpoint = pOtherPawn->en_plLastViewpoint;
  }

  export BOOL IsControlled()
  {
    return GetController() != NULL;
  }

  export BOOL IsPlayerControlled()
  {
    return IsControlled() && GetController()->IsPlayerController();
  }

  virtual INDEX GetVitality(VitalityType eType) const
  {
    return eType == VT_HEALTH ? m_iHealth : 0;
  }

  void SetVitality(VitalityType eType, INDEX iPoints)
  {
    if (eType == VT_HEALTH) {
      m_iHealth = iPoints;
    }
  }

  // Used to color the crosshair.
  export virtual FLOAT GetHealthFactor() const
  {
    return 0.0F;
  }

  export virtual void OnControllerConnected(CPawnControllerEntity *pNewController)
  {
    en_penController = pNewController;
  }

  export virtual void OnControllerDisconnected()
  {
    en_penController = NULL;
  }

procedures:
  // must have at least one procedure per class
  Dummy() {};
};
