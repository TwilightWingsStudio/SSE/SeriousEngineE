/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

/*
 * Entity that controls some pawn entity.
 */
5
%{
  #include "StdH.h"

  #include <Core/IO/Stream.h>
  #include <Core/Base/CRC.h>

  #include <Engine/Entities/InternalClasses.h>
%}

class export CPawnControllerEntity : CRationalEntity {
name      "PawnController";
thumbnail "";
features "AbstractBaseClass";
properties:
  1 CEntityPointer en_penPawn,

{
}

resources:
functions:

  export class CPawnEntity *GetPawn(void)
  {
    return reinterpret_cast<CPawnEntity *>(&*en_penPawn);
  }

  export virtual FLOAT GetPing() const
  {
    return 0.0F;
  }
  
  //! Copy entity from another entity of same class.
  export void Copy(CEntity &enOther, ULONG ulFlags)
  {
    CRationalEntity::Copy(enOther, ulFlags);

    CPawnControllerEntity *pOtherController = (CPawnControllerEntity *)(&enOther);
    // TODO:
  }
  
  export virtual BOOL IsPlayerController() const
  {
    return FALSE;
  }

  export void DisconnectFromPawn()
  {
    CPawnEntity *pCurrentPawn = GetPawn();
    
    // If we don't have pawn then we don't need to do anything.
    if (pCurrentPawn == NULL) {
      return;
    }
    
    pCurrentPawn->OnControllerDisconnected();
    pCurrentPawn = NULL;
  }

  export void ConnectToPawn(CEntity *pNewPawn)
  {
    CPawnEntity *pCurrentPawn = GetPawn();

    // Break link with old pawn if it is present.
    if (pCurrentPawn != pNewPawn && pCurrentPawn != NULL) {
      pCurrentPawn->OnControllerDisconnected();
    }
    
    en_penPawn = pNewPawn;

    // 
    if (pNewPawn == NULL) {
      return;
    }
    
    ((CPawnEntity* )pNewPawn)->OnControllerConnected(this);
  }

procedures:
  // must have at least one procedure per class
  Dummy() {};
};
