/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

/*
 * Entity represents human player that controls pawn.
 */
6
%{
  #include "StdH.h"

  #include <Core/IO/Stream.h>
  #include <Core/Base/CRC.h>

  #include <Engine/Entities/InternalClasses.h>
%}

class export CPlayerControllerEntity : CPawnControllerEntity {
name      "PlayerController";
thumbnail "";
features "AbstractBaseClass";

properties:
  1 FLOAT en_tmPing = 0.0f,    // ping value in seconds (determined by derived class and distributed by the engine)
  2 CUSTOMDATA en_cdCharacter,

{
  CPlayerCharacter en_pcCharacter;   // character of the player
}

resources:
functions:

  export virtual FLOAT GetPing() const
  {
    return en_tmPing;
  }

  export virtual BOOL IsPlayerController() const
  {
    return TRUE;
  }

  virtual void ReadCustomData_t(CTStream &istrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    
    if (slPropertyOffset == offsetof(CPlayerControllerEntity, en_cdCharacter)) {
      istrm >> en_pcCharacter;
    }
  }
  
  virtual void WriteCustomData_t(CTStream &ostrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();

    if (slPropertyOffset == offsetof(CPlayerControllerEntity, en_cdCharacter)) {
      ostrm << en_pcCharacter;
    }
  }
  
  virtual void CopyCustomData(CEntity &enOther, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    CPlayerControllerEntity *penOther = static_cast<CPlayerControllerEntity *>(&enOther);
    
    if (slPropertyOffset == offsetof(CPlayerControllerEntity, en_cdCharacter)) {
      en_pcCharacter = penOther->en_pcCharacter;
    }
  }

  //! Apply the action packet to the entity movement.
  export virtual void ApplyAction(const CPlayerAction &pa, FLOAT tmLatency) {};
  
  //! Called when player is disconnected.
  export virtual void Disconnect(void) {};
  
  //! Called when player character is changed.
  export virtual void CharacterChanged(const CPlayerCharacter &pcNew) { en_pcCharacter = pcNew; };
  
  //! Provide info for GameAgent enumeration.
  export virtual void GetGameAgentPlayerInfo(INDEX iPlayer, CTString &strOut) { };

  //! Provide info for MSLegacy enumeration.
  export virtual void GetMSLegacyPlayerInf(INDEX iPlayer, CTString &strOut) { };
  
  //! Get name of this player.
  export CTString GetPlayerName(void)
  {
    return en_pcCharacter.GetNameForPrinting();
  }

  export const CTString &GetName(void) const
  {
    return en_pcCharacter.GetName();
  }

  //! Create a checksum value for sync-check.
  export void ChecksumForSync(ULONG &ulCRC, INDEX iExtensiveSyncCheck)
  {
    CPawnControllerEntity::ChecksumForSync(ulCRC, iExtensiveSyncCheck);
    CRC_AddBlock(ulCRC, en_pcCharacter.pc_UUID.Data(), 16);
    CRC_AddBlock(ulCRC, en_pcCharacter.pc_aubAppearance, sizeof(en_pcCharacter.pc_aubAppearance));
  }
  
  //! Dump sync data to text file.
  export void DumpSync_t(CTStream &strm, INDEX iExtensiveSyncCheck)  // throw char *
  {
    CPawnControllerEntity::DumpSync_t(strm, iExtensiveSyncCheck);
    strm.FPrintF_t("player: %s\n", en_pcCharacter.GetName());
    strm.FPrintF_t("UUID: ");

    {
      for (INDEX i = 0; i < 16; i++)
      {
        strm.FPrintF_t("%02X", en_pcCharacter.pc_UUID.Data()[i]);
      }
    }

    strm.FPrintF_t("\n");
    strm.FPrintF_t("appearance: ");

    {
      for (INDEX i = 0; i < MAX_PLAYERAPPEARANCE; i++)
      {
        strm.FPrintF_t("%02X", en_pcCharacter.pc_aubAppearance[i]);
      }
    }

    strm.FPrintF_t("\n");
  }

procedures:
  // must have at least one procedure per class
  Dummy() {};
};
