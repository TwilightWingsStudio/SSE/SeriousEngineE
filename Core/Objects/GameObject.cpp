/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/String.h>
#include <Core/Objects/GameObject.h>
#include <Core/Objects/ObjectFactory.h>

CGameObject::CGameObject()
{
}

CGameObject::~CGameObject()
{
}

CGameObject *CGameObject::Clone() const
{
  ASSERTALWAYS(FALSE);
  return new CGameObject(*this);
}

BOOL CGameObject::GetById(ULONG idProperty, FVariant &val, CAbstractVar::Type type)
{
  return FALSE;
}

FVariant CGameObject::GetById(ULONG idProperty)
{
  FVariant varResult;
  GetById(idProperty, varResult);
  return varResult;
}

BOOL CGameObject::SetById(ULONG idProperty, const FVariant &val)
{
  return FALSE;
}

BOOL CGameObject::Get(const FString &strIdentifier, FVariant &val, CAbstractVar::Type type)
{
  return GetById(strIdentifier.GetHash(), val, type);
}

FVariant CGameObject::Get(const FString &strIdentifier)
{
  FVariant varResult;
  Get(strIdentifier, varResult);
  return varResult;
}

BOOL CGameObject::Set(const FString &strIdentifier, const FVariant &val)
{
  return SetById(strIdentifier.GetHash(), val);
}