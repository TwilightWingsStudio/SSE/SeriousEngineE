/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_GAMEOBJECT_H
#define SE_INCL_GAMEOBJECT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Variant.h>

//! Class that represents any object.
class CORE_API CGameObject 
{
  public:
    //! Constructor.
    CGameObject();

    //! Destructor.
    virtual ~CGameObject();
    
    //! Get exact copy of the object.
    virtual CGameObject *Clone() const;

    //! Get property value by its ID into variant.
    virtual BOOL GetById(ULONG idProperty, FVariant &val, CAbstractVar::Type type = CAbstractVar::TYPE_NIL);

    //! Get property value by its ID into variant.
    FVariant GetById(ULONG idProperty);

    //! Get property value by its unique string identifier.
    virtual BOOL Get(const FString &strIdentifier, FVariant &val, CAbstractVar::Type type = CAbstractVar::TYPE_NIL);

    //! Get property value by its unique string identifier.
    FVariant Get(const FString &strIdentifier);

    //! Set property value by its ID from variant. Returns false if property is dynamic.
    virtual BOOL SetById(ULONG idProperty, const FVariant &val);

    //! Set property value by its unique string identifier. Returns false if property is dynamic.
    virtual BOOL Set(const FString &strIdentifier, const FVariant &val);
};

#endif  /* include-once check. */