/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Objects/ReferenceCounted.h>

//! Constructor.
CRefCounted::CRefCounted()
{
  m_ctReferenced = 0;
}

//! Increment reference counter by one.
void CRefCounted::Reference()
{
  ASSERT(m_ctReferenced >= 0);
  m_ctReferenced++;
}

//! Decrement reference counter.
void CRefCounted::Unreference()
{
  m_ctReferenced--;
  ASSERT(m_ctReferenced >= 0);
}

bool CRefCounted::IsReferenced()
{
  ASSERT(m_ctReferenced >= 0);
  return m_ctReferenced > 0;
}