#include "StdH.h"

#include <Core/Core.h>
#include <Core/CoreVersion.h>
#include <Core/Base/Console.h>
#include <Core/IO/Stream.h>
#include <Core/Base/Shell.h>
#include <Core/Base/Console_internal.h>
#include <Core/Base/Timer.h>
#include <Core/Threading/Synchronization.h>

#include <Core/Compression/CompressionManager.h>
#include <Core/IO/ArchiveManager.h>
#include <Core/Graphics/ImageManager.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

#include <Core/Modules/ModuleClass.h> // To implement it!

#ifdef NDEBUG
  #define SE_CORE_CONFIG "Release"
#else
  #define SE_CORE_CONFIG "Debug"
#endif

// critical section for access to zlib functions
CSyncMutex zip_csLock;

extern void (*_pCallProgressHook_t)(FLOAT fProgress) = nullptr;
extern HWND _hwndMain = NULL;
extern BOOL _bFullScreen = FALSE;
extern BOOL _bAllocationArrayParanoiaCheck = FALSE;

extern INDEX sys_iCPUMisc = 0;

BOOL _bConsoleApp = FALSE;
BOOL _bDedicatedServer = FALSE;
BOOL _bWorldEditorApp = FALSE;
FString _strLogFile = "";

extern FString _strCoreBuild = "";

// reverses string
void StrRev(char *str)
{
  char ctmp;
  char *pch0 = str;
  char *pch1 = str + strlen(str) - 1;

  while (pch1 > pch0)
  {
    ctmp = *pch0;
    *pch0 = *pch1;
    *pch1 = ctmp;
    pch0++;
    pch1--;
  }
}

static char strExePath[MAX_PATH] = "";
static char strDirPath[MAX_PATH] = "";

static void AnalyzeApplicationPath(void)
{
  strcpy(strDirPath, "D:\\");
  strcpy(strExePath, "D:\\TestExe.xbe");
  char strTmpPath[MAX_PATH] = "";

  // get full path to the exe
  GetModuleFileNameA(NULL, strExePath, sizeof(strExePath)-1);
  // copy that to the path
  strncpy(strTmpPath, strExePath, sizeof(strTmpPath)-1);
  strDirPath[sizeof(strTmpPath)-1] = 0;

  // remove name from application path
  StrRev(strTmpPath);

  // find last backslash
  char *pstr = strchr(strTmpPath, '\\');

  if (pstr == NULL) {
    // not found - path is just "\"
    strcpy(strTmpPath, "\\");
    pstr = strTmpPath;
  }

  // remove 'debug' from app path if needed
  if (strnicmp(pstr, "\\gubed", 6) == 0) {
    pstr += 6;
  }

  if (pstr[0] == '\\') {
    pstr++;
  }

  char *pstrFin = strchr(pstr, '\\');

  if (pstrFin == NULL) {
    strcpy(pstr, "\\");
    pstrFin = pstr;
  }

  // copy that to the path
  StrRev(pstrFin);
  strncpy(strDirPath, pstrFin, sizeof(strDirPath)-1);
  strDirPath[sizeof(strDirPath)-1] = 0;
}

extern FLOAT mth_fCSGEpsilon = 1.0f;

void SE_InitCore()
{
  // [SEE] Manually setup an unhandled exception handler
  extern void SE_SetupExceptionHandler();
  SE_SetupExceptionHandler();

  AnalyzeApplicationPath();
  _fnmApplicationPath = FString(strDirPath);
  _fnmApplicationExe = FString(strExePath);

  try {
    _fnmApplicationExe.RemoveApplicationPath_t();
  }
  catch (char *strError) {
    (void)strError;
    ASSERT(FALSE);
  }

  _pConsole = new CConsole;

  if (_strLogFile == "") {
    _strLogFile = FPath(FString(strExePath)).FileName();
  }
  
  // [SEE]
  FPath fnmLogsDir = _fnmApplicationPath + "Logs\\";
  
  // TODO: Use cross-platform implementation.
  DWORD dwAttrib = GetFileAttributesA(fnmLogsDir);
  if (dwAttrib == -1)
  {
    CreateDirectoryA(fnmLogsDir, NULL);
  }

  _pConsole->Initialize(fnmLogsDir + _strLogFile + ".log", 90, 512);

  _strCoreBuild.PrintF(TRANS("$Version: SEE; %s; %s; %s; %s; %s"), SE_CORE_COMMIT_HASH, SE_CORE_BRANCH, SE_CORE_CONFIG, __DATE__, __TIME__);

  CInfoF("Core version: %s\n", _strCoreBuild);

  CInfoF("Initializing timer.\n");
  _pTimer = new CTimer;

  CInfoF("Binary name: %s\n", _fnmApplicationExe);
  CInfoF("Application directory: %s\n", _fnmApplicationPath);

  // Initialize shelll.
  _pShell = new CShell;
  _pShell->Initialize();

  extern INDEX fil_bPreferZips;
  extern INDEX wld_bFastObjectOptimization;

  _pShell->DeclareSymbol("user INDEX wld_bFastObjectOptimization;", &wld_bFastObjectOptimization);
  _pShell->DeclareSymbol("user FLOAT mth_fCSGEpsilon;", &mth_fCSGEpsilon);
  _pShell->DeclareSymbol("user INDEX con_bNoWarnings;", &con_bNoWarnings);
  _pShell->DeclareSymbol("persistent user INDEX fil_bPreferZips;", &fil_bPreferZips);
  _pShell->DeclareSymbol("     const INDEX sys_iCPUMisc       ;", &sys_iCPUMisc    );
  
  _pCompressionMgr = new FCompressionManager();
  _pArchiveMgr = new FArchiveManager();
  _pImageMgr = new FImageManager();

  // init MODs and stuff ...
  extern void InitStreams(void);
  InitStreams();

  // initialize zip semaphore
  zip_csLock.cs_iIndex = -1;  // not checked for locking order
}

void SE_EndCore()
{
  CInfoF("Core is shutting down");

  delete _pCompressionMgr; _pCompressionMgr = NULL;
  delete _pArchiveMgr;   _pArchiveMgr = NULL;
  delete _pTimer;    _pTimer = NULL;
  delete _pShell;    _pShell = NULL;
  delete _pConsole;  _pConsole = NULL;

  extern void EndStreams(void);
  EndStreams();
}