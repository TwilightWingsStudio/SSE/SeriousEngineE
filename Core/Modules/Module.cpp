/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/Modules/Module.h>

#ifndef _WIN32
  #include <dlfcn.h>
#endif

class FModulePrivate
{
  public:
    FPath fnmPackage;

    union {
      void *pHandle;
#ifdef _WIN32
      HINSTANCE hiLibrary;
#endif
    };

    //! Pointer to information retrieving routines.
    void (*pGetInfoFunc)(FModuleInfo *);

    //! Pointer to initialization routines, called after LoadLibrary.
    void (*pOnStartupFunc)(void);

    //! Pointer to deinit routines, called before FreeLibrary.
    void (*pOnShutdownFunc)(void);

  public:
    FModulePrivate()
    {
      Clear();
    }

    void Clear()
    {
      if (fnmPackage.Length() > 0) {
        fnmPackage.Clear();
      }

      hiLibrary = nullptr;
      pGetInfoFunc = nullptr;
      pOnStartupFunc = nullptr;
      pOnShutdownFunc = nullptr;
    }
};

//! Constructor.
FModule::FModule()
{
  _pData = new FModulePrivate;
}

//! Destructor
FModule::~FModule()
{
  Clear();

  if (_pData) {
    _pData->Clear();
    delete _pData;
  }
}

// Count used memory
SLONG FModule::GetUsedMemory(void)
{
  return sizeof(FModule);
}

#ifdef _WIN32
HINSTANCE FModule::GetModuleHandle()
{
  return _pData->hiLibrary;
}
#else
void *FModule::GetModuleHandle()
{
  return _pData->pHandle;
}
#endif

// Write to stream
void FModule::Write_t(CTStream *ostrFile)
{
}

// Load a Dynamic Link Library.
#ifdef _WIN32
HINSTANCE LoadLibrary_t(const char *strFileName)
{
  HINSTANCE hiDLL = ::LoadLibraryA(strFileName);

  // If the DLL can not be loaded
  if (hiDLL == NULL) {
    // Get the error code
    DWORD dwMessageId = GetLastError();
    
    // Format the windows error message
    LPVOID lpMsgBuf;
    DWORD dwSuccess = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        dwMessageId,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // default language
        (LPTSTR) &lpMsgBuf,
        0,
        NULL
    );
    
    FString strWinError;
    // If formatting succeeds
    if (dwSuccess != 0) {
      // Copy the result
      strWinError = ((char *)lpMsgBuf);
      
      // Free the windows message buffer
      LocalFree(lpMsgBuf );
    } else {
      // Set our message about the failure
      FString strError;
      strError.PrintF(
        TRANS("Cannot format error message!\n"
        "Original error code: %d,\n"
        "Formatting error code: %d.\n"),
        dwMessageId, GetLastError());
      strWinError = strError;
    }

    // Report error
    ThrowF_t(TRANS("Cannot load module '%s':\n%s"), strFileName, strWinError);
  }

  return hiDLL;
}

#else
void *LoadLibrary_t(const char *strFileName)
{
  _pHandle = dlopen(LIBM_SO, strFileName, RTLD_LAZY);
}
#endif

// Read from stream
void FModule::Read_t(CTStream *istrFile)
{
  // Read the dll filename and class name from the stream
  FPath fnmDLL;
  FString strSymbolPrefix;
  FString strGetInfoFunc;
  FString strOnStartupFunc;
  FString strOnShutdownFunc;

  fnmDLL.ReadFromText_t(*istrFile, "Package: ");
  strSymbolPrefix.ReadFromText_t(*istrFile, "SymbolPrefix: ");

  // create name of dll
  #ifndef NDEBUG
    fnmDLL = _fnmApplicationExe.FileDir() + fnmDLL.FileName() + /*_strModExt+*/"D" + fnmDLL.FileExt();
  #else
    fnmDLL = _fnmApplicationExe.FileDir() + fnmDLL.FileName() + /*_strModExt+*/fnmDLL.FileExt();
  #endif

  FPath fnmExpanded;
  ExpandFilePath(EFP_READ | EFP_NOZIPS, fnmDLL, fnmExpanded);

  _pData->fnmPackage = fnmExpanded;

  // Load dll
  _pData->hiLibrary = LoadLibrary_t(fnmExpanded);

  if (strSymbolPrefix != "") {
    strGetInfoFunc.PrintF("%sInfo", strSymbolPrefix);
    strOnStartupFunc.PrintF("%sStartup", strSymbolPrefix);
    strOnShutdownFunc.PrintF("%sShutdown", strSymbolPrefix);

    _pData->pGetInfoFunc = (void(*)(FModuleInfo *))GetSymbol(strGetInfoFunc.ConstData());
    _pData->pOnStartupFunc = (void(*)(void))GetSymbol(strOnStartupFunc.ConstData());
    _pData->pOnShutdownFunc = (void(*)(void))GetSymbol(strOnShutdownFunc.ConstData());

    OnStartup();
  }
}

// Clear modyle 
void FModule::Clear(void)
{
  // Release dll
  if (_pData->hiLibrary != NULL) {
    OnShutdown();

  #ifdef _WIN32
    FreeLibrary(_pData->hiLibrary);
  #else
    dlclose(_pData->_pHandle);
  #endif
  }

  _pData->Clear();
}

void *FModule::GetSymbol(const char *pSymName)
{
#ifdef _WIN32
  return GetProcAddress(GetModuleHandle(), pSymName);
#else
  return dlsym(_pHandle, pSymName);
#endif
}

void FModule::GetInfo(FModuleInfo &info)
{
  if (_pData->pGetInfoFunc) {
    _pData->pGetInfoFunc(&info);
  }
}

void FModule::OnStartup()
{
  if (_pData->pOnStartupFunc) {
    _pData->pOnStartupFunc();
  }
}

void FModule::OnShutdown()
{
  if (_pData->pOnShutdownFunc) {
    _pData->pOnShutdownFunc();
  }
}

// Get the description of this object.
FString FModule::GetDescription(void)
{
  return _pData->fnmPackage;
}

UBYTE FModule::GetType() const
{
  return TYPE_MODULE;
}