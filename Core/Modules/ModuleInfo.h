/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MODULEINFO_H
#define SE_INCL_MODULEINFO_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Module description.
class CORE_API FModuleInfo
{
  public:
    FString Name;
    FString CommitHash;
    FString BranchName;
    ULONG Version;

  public:
    //! Constructor.
    inline FModuleInfo()
    {
      Clear();
    }

    //! Returns module name.
    inline const FString &GetName(void) const
    {
      return Name;
    };

    //! Returns commit hash.
    inline const FString &GetCommitHash(void) const
    {
      return CommitHash;
    };

    //! Returns commit hash.
    inline const FString &GetBranchName(void) const
    {
      return BranchName;
    };

    //! Returns numeric version.
    inline ULONG GetVersion() const
    {
      return Version;
    }

    //! Reset memory.
    inline void Clear()
    {
      Name.Clear();
      CommitHash.Clear();
      BranchName.Clear();

      Version = 0;
    }
};

#endif  /* include-once check. */