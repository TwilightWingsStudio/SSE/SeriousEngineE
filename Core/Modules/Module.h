/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MODULE_H
#define SE_INCL_MODULE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/Resource.h>
#include <Core/Modules/ModuleInfo.h>

class FModulePrivate;

//! Abstract class that represents loaded module.
class CORE_API FModule : public CResource 
{
  public:
    FModulePrivate *_pData;

  public:
    //! Constructor.
    FModule();

    //! Destructor
    ~FModule() override;

    //! Clear module. 
    void Clear(void) override;

    //! Read from stream.
    void Read_t(CTStream *istrFile) override; // throw char *
    
    //! Write to stream.
    void Write_t(CTStream *ostrFile) override; // throw char *
    
    //! Returns amount of used memory in bytes.
    SLONG GetUsedMemory(void) override;

    //! Returns loaded library handle.
  #ifdef _WIN32
    HINSTANCE GetModuleHandle();
  #else
    void *GetModuleHandle();
  #endif

    void *GetSymbol(const char *pSymName);

    //! Retrive info from module.
    void GetInfo(FModuleInfo &info);

    //! Invoke Startup with safe.
    void OnStartup();

    //! Invoke Shutdown with safe.
    void OnShutdown();

    //! Get the description of this object.
    FString GetDescription(void) override;

    //! Check if this kind of objects is auto-freed.
    BOOL IsAutoFreed(void) override
    {
      return FALSE;
    };
    
    //! TODO: Add comment.
    UBYTE GetType() const override;
};

#endif  /* include-once check. */