@Echo Off
Set "file=%~dp0CoreVersion.h"
Set "cmd=git rev-parse HEAD"
Set "cmd2=git symbolic-ref -q --short HEAD"
Set "git_sha1=#define SE_CORE_COMMIT_HASH"
Set "git_branch=#define SE_CORE_BRANCH"

For /F "Delims=" %%A In ('%cmd%')Do Set "git_sha1_result=%%A"
For /F "Delims=" %%A In ('%cmd2%')Do set "git_branch_result=%%A"

IF "%git_sha1_result%"=="" set "git_sha1_result=0000000000000000000000000000000000000000"
IF "%git_branch_result%"=="" set "git_branch_result=master"

(Echo #ifndef SE_INCL_CORE_VERSION
    Echo #define SE_INCL_CORE_VERSION
    Echo %git_sha1% "%git_sha1_result%"
    Echo %git_branch% "%git_branch_result%"
    Echo #endif)>"%file%"
