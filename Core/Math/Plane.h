/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PLANE_H
#define SE_INCL_PLANE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Assert.h>
#include <Core/Math/Functions.h>

//! Template class for plane in space of arbitrary dimensions and arbitrary type of coordinates
template<class Type, int iDimensions>
class TPlane : public TVector<Type, iDimensions>
{ // normal vector
  public:
    Type pl_distance;      // distance from point 0 along the normal vector

  public:
    //! Default constructor.
    inline TPlane(void);

    //! Constructor from normal vector and distance.
    inline TPlane(const TVector<Type, iDimensions> &normal, const Type distance);

    //! Constructor from normal vector and a point on plane.
    inline TPlane(const TVector<Type, iDimensions> &normal, const TVector<Type, iDimensions> &point);

    //! Constructor from 3 points on plane, counter clockwise order.
    inline TPlane(const TVector<Type, iDimensions> &point0, const TVector<Type, iDimensions> &point1, const TVector<Type, iDimensions> &point2);

    //! Reference distance.
    inline Type &Distance(void);

    //! Reference distance.
    inline const Type &Distance(void) const;

    //! Get a reference point on the plane.
    inline TVector<Type, iDimensions> ReferencePoint(void) const;

    //! Get a reference point on the plane, if origin is at given vector.
    inline TVector<Type, iDimensions> ReferencePoint(const TVector<Type, iDimensions> &vOrigin) const;

    //! Get distance of point from plane. 
    inline const Type PointDistance(const TVector<Type, iDimensions> &point) const;

    //! Get missing coordinate value.
    inline void GetCoordinate(const int iIndex, TVector<Type, iDimensions> &point) const;

    //! Get distance of plane from plane.
    inline const Type PlaneDistance(const TPlane<Type, iDimensions> &plOther) const;

    //! Project a point to the plane.
    inline TVector<Type, iDimensions> ProjectPoint(const TVector<Type, iDimensions> &point) const;

    //! Project a direction vector to the plane.
    inline TVector<Type, iDimensions> ProjectDirection(const TVector<Type, iDimensions> &direction) const;

    //! Get index of the greatest coordinate of normal.
    inline INDEX GetMaxNormal(void) const;

    //! Deproject a point to the plane.
    inline TVector<Type, iDimensions> DeprojectPoint(const TPlane<Type, iDimensions> &plOther, const TVector<Type, iDimensions> &point) const;

    //! Deproject a direction vector to the plane.
    inline TVector<Type, iDimensions> DeprojectDirection(const TPlane<Type, iDimensions> &plOther, const TVector<Type, iDimensions> &point) const;

    //! Offset the plane forward for a given distance.
    inline void Offset(const Type offset);

    /* Mathematical operators. */
    //! unary minus (fliping of the plane)
    inline TPlane<Type, iDimensions> operator-(void) const;

    //! Addition of vector (translation of the plane by the vector).
    inline TPlane<Type, iDimensions> operator+(const TVector<Type, iDimensions> &vector2) const;

    //! Addition of vector (translation of the plane by the vector).
    inline TPlane<Type, iDimensions> &operator+=(const TVector<Type, iDimensions> &vector2);
    
    //! Substraction.
    inline TPlane<Type, iDimensions> operator-(const TVector<Type, iDimensions> &vector2) const;

    //! Substraction.
    inline TPlane<Type, iDimensions> &operator-=(const TVector<Type, iDimensions> &vector2);

    //! Multiplication by a square matrix (sides swapped -- see implementation for notes).
    inline TPlane<Type, iDimensions> &operator*=(const TMatrix<Type, iDimensions, iDimensions> &matrix2);

    //! Multiplication by a square matrix (sides swapped -- see implementation for notes).
    TPlane<Type, iDimensions> operator*(const TMatrix<Type, iDimensions, iDimensions> &matrix2) const;
};

// inline functions implementation
/*
 * Default constructor.
 */
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions>::TPlane(void) {}

/* 
 * Constructor from normal vector and distance.
 */
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions>::TPlane(const TVector<Type, iDimensions> &normal, const Type distance)
  : TVector<Type, iDimensions>(normal),
    pl_distance(distance)
{
  // normalize normal vector
  ((TVector<Type, iDimensions>)*this).Normalize();
}

/*
 * Constructor from normal vector and a point on plane.
 */
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions>::TPlane(const TVector<Type, iDimensions> &normal, const TVector<Type, iDimensions> &point)
  : TVector<Type, iDimensions>(normal)
{
  // normalize normal vector
  this->Normalize();
  pl_distance = (*this)%point;   // distance = normalized_normal * point (dot product)
}

/*
 * Constructor from 3 points on plane, counter clockwise order.
 */
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions>::TPlane(const TVector<Type, iDimensions> &point0, const TVector<Type, iDimensions> &point1, const TVector<Type, iDimensions> &point2)
{
  // create normal vector of plane
  TVector<Type, iDimensions> normal = (point2-point1)*(point0-point1);  // cross product

  // construct plane with normal and one point
  *this = TPlane<Type, iDimensions>(normal, point0);
}

/*
 * Reference distance.
 */
template<class Type, int iDimensions>
inline Type &TPlane<Type, iDimensions>::Distance(void) {
  return pl_distance;
}

template<class Type, int iDimensions>
inline const Type &TPlane<Type, iDimensions>::Distance(void) const {
  return pl_distance;
}

/*
 * Get distance of point from plane.
 */
template<class Type, int iDimensions>
inline const Type TPlane<Type, iDimensions>::PointDistance(const TVector<Type, iDimensions> &point) const
{
  /* Distance of the point from (0,0,0) along the normal vector of the plane
   * minus distance of the plane from (0,0,0) along the normal vector of the plane.
   */
  return (*this)%point-pl_distance;
}

/*
 * Get missing coordinate
 */
template<class Type, int iDimensions>
inline void TPlane<Type, iDimensions>::GetCoordinate(const int iIndex, TVector<Type, iDimensions> &point) const
{
  Type sum = pl_distance;

  for (INDEX i = 1; i <= iDimensions; i++)
  {
    if (i != iIndex)
      sum -= (*this)(i) * point(i);
  }

  point( iIndex) = sum / (*this)(iIndex);
}

/*
 * Get distance of plane from plane.
 */
template<class Type, int iDimensions>
inline const Type TPlane<Type, iDimensions>::PlaneDistance(const TPlane<Type, iDimensions> &plOther) const
{
  /* Distance of the reference point of ther other plane
   */
  return PointDistance(plOther.ReferencePoint());
}

/*
 * Get a reference point on the plane.
 */
template<class Type, int iDimensions>
inline TVector<Type, iDimensions> TPlane<Type, iDimensions>::ReferencePoint(void) const
{
  // let the reference point be from (0,0,0) along the normal vector
  return ((TVector<Type, iDimensions>)*this) * pl_distance;
}

/*
 * Get a reference point on the plane, if origin is at given vector.
 */
template<class Type, int iDimensions>
inline TVector<Type, iDimensions> TPlane<Type, iDimensions>::ReferencePoint(const TVector<Type, iDimensions> &vOrigin) const
{
  // let the reference point be from the origin along the normal vector,
  // as far as the origin is away from the plane
  return vOrigin - ((TVector<Type, iDimensions>)*this) * (PointDistance(vOrigin));
}

/*
 * Project a point to the plane.
 */
template<class Type, int iDimensions>
inline TVector<Type, iDimensions> TPlane<Type, iDimensions>::ProjectPoint(const TVector<Type, iDimensions> &point) const
{
  return ReferencePoint(point);
}

/*
 * Project a direction vector on the plane.
 */
template<class Type, int iDimensions>
inline TVector<Type, iDimensions> TPlane<Type, iDimensions>::ProjectDirection(const TVector<Type, iDimensions> &direction) const
{
  // projected direction is vector between projected endpoint and projected origin
  return ProjectPoint(direction) - ReferencePoint();
}

template<class Type, int iDimensions>
/* Deproject a point to the plane. */
inline TVector<Type, iDimensions> TPlane<Type, iDimensions>::DeprojectPoint(const TPlane<Type, iDimensions> &plOther, const TVector<Type, iDimensions> &point) const
{
  TVector<Type, iDimensions> &vNormal      = (TVector<Type, iDimensions> &) *this;
  TVector<Type, iDimensions> &vNormalOther = (TVector<Type, iDimensions> &) plOther;

  return point - vNormalOther*( PointDistance(point)/(vNormal%vNormalOther) );
}

template<class Type, int iDimensions>
/* Deproject a direction vector to the plane. */
inline TVector<Type, iDimensions> TPlane<Type, iDimensions>::DeprojectDirection(const TPlane<Type, iDimensions> &plOther, const TVector<Type, iDimensions> &point) const
{
  TVector<Type, iDimensions> &vNormal      = (TVector<Type, iDimensions> &) *this;
  TVector<Type, iDimensions> &vNormalOther = (TVector<Type, iDimensions> &) plOther;

  return point - vNormalOther*( (point%vNormal)/(vNormal%vNormalOther) );
}

/*
 * Get index of the greatest coordinate of normal.
 */
template<class Type, int iDimensions>
inline INDEX TPlane<Type, iDimensions>::GetMaxNormal(void) const
{
  INDEX iMax = 1;
  Type tMax = Abs((*this)(1));

  for (INDEX i = 2; i <= iDimensions; i++)
  {
    if (Abs((*this)(i)) > tMax) {
      tMax = Abs((*this)(i));
      iMax = i;
    }
  }

  return iMax;
}

/* Offset the plane forward for a given distance. */
template<class Type, int iDimensions>
inline void TPlane<Type, iDimensions>::Offset(const Type offset)
{
  pl_distance += offset;
}

// unary minus (fliping of the plane)
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> TPlane<Type, iDimensions>::operator-(void) const
{
  return TPlane<Type, iDimensions>(-(TVector<Type, iDimensions>)*this, -pl_distance);
}

// addition of vector (translation along the vector)
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> &TPlane<Type, iDimensions>::operator+=(const TVector<Type, iDimensions> &vector2)
{
  /* Calculate the length of the projection of the translation vector on the
    normal vector of the plane and add it to the plane distance.
  */
  pl_distance += (*static_cast<TVector<Type, iDimensions> *>(this))%vector2;

  return *this;
}

template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> TPlane<Type, iDimensions>::operator+(const TVector<Type, iDimensions> &vector2) const
{
  return TPlane<Type, iDimensions>(*this)+=vector2;
}

template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> &TPlane<Type, iDimensions>::operator-=(const TVector<Type, iDimensions> &vector2)
{
  /* Calculate the length of the projection of the translation vector on the
    normal vector of the plane and add it to the plane distance.
  */
  pl_distance -= (*static_cast<TVector<Type, iDimensions> *>(this)) % vector2;

  return *this;
}

template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> TPlane<Type, iDimensions>::operator-(const TVector<Type, iDimensions> &vector2) const
{
  return TPlane<Type, iDimensions>(*this) -= vector2;
}

/*
 * Multiplication of a plane by a square matrix.
 */
// NOTE: The matrix should have been on the left side of the vector, but the template syntax 
// wouldn't allow that.
template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> &TPlane<Type, iDimensions>::operator*=(const TMatrix<Type, iDimensions, iDimensions> &matrix2)
{
  (*static_cast<TVector<Type, iDimensions> *>(this)) *= matrix2;

  return *this;
}

template<class Type, int iDimensions>
inline TPlane<Type, iDimensions> TPlane<Type, iDimensions>::operator*(const TMatrix<Type, iDimensions, iDimensions> &matrix2) const
{
  return TPlane<Type, iDimensions>(*this) *= matrix2;
}

// helper functions for converting between FLOAT and DOUBLE planes
inline DOUBLEplane3D FLOATtoDOUBLE(const FLOATplane3D &plf)
{
  return DOUBLEplane3D(FLOATtoDOUBLE((FLOAT3D&)plf), FLOATtoDOUBLE(plf.Distance()));
}

inline FLOATplane3D DOUBLEtoFLOAT(const DOUBLEplane3D &pld)
{
  return FLOATplane3D(DOUBLEtoFLOAT((DOUBLE3D&)pld), DOUBLEtoFLOAT(pld.Distance()));
}

#endif  /* include-once check. */
