/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FIXINT_H
#define SE_INCL_FIXINT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

/*
 * iInt    - number of bits in integer part
 * iFrac   - number of bits in fractional part
 * NOTE: Since 32 bit integer holds this fix int, iInt+iFrac must be 32.
 */

//! Template class for fixed integer with arbitrary sizes of integer and fractional parts
template<int iInt, int iFrac>
class TFixInt
{
  public:
    SLONG slHolder;   // the representation is stored here

  public:
    //! Default constructor.
    inline TFixInt<iInt, iFrac>(void) {};

    //! Copy constructor.
    inline TFixInt<iInt, iFrac>(const TFixInt<iInt, iFrac> &x) { slHolder = x.slHolder; };

    //! Constructor from 32-bit integer.
    inline TFixInt<iInt, iFrac>(SLONG sl) : slHolder(sl << iFrac) {};

    //! Constructor from 32-bit integer.
    inline TFixInt<iInt, iFrac>(ULONG ul) : slHolder((SLONG)(ul << iFrac)) {};

    //! Constructor from 16-bit integer.
    inline TFixInt<iInt, iFrac>(SWORD sw) : slHolder((SLONG)(sw << iFrac)) {};

    //! Constructor from 16-bit integer.
    inline TFixInt<iInt, iFrac>(UWORD uw) : slHolder((SLONG)(uw << iFrac)) {};

    //! Constructor from 8-bit integer.
    inline TFixInt<iInt, iFrac>(SBYTE sb) : slHolder((SLONG)(sb << iFrac)) {};

    //! Constructor from 8-bit integer.
    inline TFixInt<iInt, iFrac>(UBYTE ub) : slHolder((SLONG)(ub << iFrac)) {};
    
    //! Constructor from 32-bit integer.
    inline TFixInt<iInt, iFrac>(signed int si)   : slHolder((SLONG)(si << iFrac)) {};
    
    //! Constructor from 32-bit integer.
    inline TFixInt<iInt, iFrac>(unsigned int ui) : slHolder((SLONG)(ui << iFrac)) {};

    //! Constructor from 32-bit float.
    inline TFixInt<iInt, iFrac>(float f)  : slHolder((SLONG)(f * (1L << iFrac))) {};

    //! Constructor from 64-bit float.
    inline TFixInt<iInt, iFrac>(double f) : slHolder((SLONG)(f * (1L << iFrac))) {};

    //! Set holder constructor.
    inline TFixInt<iInt, iFrac>(SLONG slNewHolder, int iDummy) : slHolder(slNewHolder) {};

    //! Conversion to integer (truncatenation).
    inline operator SLONG(void) const
    {
      return slHolder >> iFrac;
    };

    //! Conversion to 32-bit float (exact).
    inline operator float(void) const
    {
      return slHolder / (float) (1L << iFrac);
    };

    //! Conversion to 64-bit float (exact).
    inline operator double(void) const
    {
      return slHolder / (double) (1L << iFrac);
    };

    //! Negation.
    inline TFixInt<iInt, iFrac> operator-(void) const
    {
      return TFixInt<iInt, iFrac>(-slHolder, 1);
    };

    //! Addition.
    inline TFixInt<iInt, iFrac> &operator+=(TFixInt<iInt, iFrac> x)
    {
      slHolder += x.slHolder; return *this;
    };

    //! Addition.
    inline TFixInt<iInt, iFrac> operator+(TFixInt<iInt, iFrac> x) const
    {
      return TFixInt<iInt, iFrac>(slHolder + x.slHolder, 1);
    };

    //! Substraction.
    inline TFixInt<iInt, iFrac> &operator-=(TFixInt<iInt, iFrac> x)
    {
      slHolder -= x.slHolder;
      return *this;
    };

    //! Substraction.
    inline TFixInt<iInt, iFrac> operator-(TFixInt<iInt, iFrac> x) const
    {
      return TFixInt<iInt, iFrac>(slHolder - x.slHolder, 1);
    };

    //! Multiplication.
    inline TFixInt<iInt, iFrac> operator*(TFixInt<iInt, iFrac> x) const
    {
      return TFixInt<iInt, iFrac>( (SLONG)((__int64(slHolder) * x.slHolder) >> iFrac), 1);
    };

    //! Multiplication.
    inline TFixInt<iInt, iFrac> operator*(SLONG sl) const
    {
      return TFixInt<iInt, iFrac>(slHolder * sl, 1);
    };

    //! Multiplication.
    friend inline TFixInt<iInt, iFrac> operator*(SLONG sl, TFixInt<iInt, iFrac> x)
    {
      return TFixInt<iInt, iFrac>(x.slHolder * sl, 1);
    };
    
    //! Multiplication.
    inline TFixInt<iInt, iFrac> &operator*=(TFixInt<iInt, iFrac> x)
    {
      return *this = *this*x;
    };

    //! Division.
    inline TFixInt<iInt, iFrac> operator/(TFixInt<iInt, iFrac> x) const
    {
      return TFixInt<iInt, iFrac>( (SLONG) ( (__int64(slHolder) << iFrac) / x.slHolder ) , 1);
    };

    //! Division.
    inline TFixInt<iInt, iFrac> &operator/=(TFixInt<iInt, iFrac> x)
    {
      return *this = *this / x;
    };

    //! Lower.
    inline BOOL operator< (TFixInt<iInt, iFrac> x) const
    {
      return slHolder <  x.slHolder;
    };

    //! Lower or equal.
    inline BOOL operator<=(TFixInt<iInt, iFrac> x) const
    {
      return slHolder <= x.slHolder;
    };

    //! Bigger.
    inline BOOL operator> (TFixInt<iInt, iFrac> x) const
    {
      return slHolder >  x.slHolder;
    };

    //! Bigger or equal.
    inline BOOL operator>=(TFixInt<iInt, iFrac> x) const
    {
      return slHolder >= x.slHolder;
    };

    //! Equal.
    inline BOOL operator==(TFixInt<iInt, iFrac> x) const
    {
      return slHolder == x.slHolder;
    };

    //! Non equal.
    inline BOOL operator!=(TFixInt<iInt, iFrac> x) const
    {
      return slHolder != x.slHolder;
    };

    //! Rounding and truncatenation functions.
    friend inline SLONG Floor(TFixInt<iInt, iFrac> x)
    {
      return (x.slHolder) >> iFrac;
    };

    //! Rounding and truncatenation functions.
    friend inline SLONG Ceil(TFixInt<iInt, iFrac> x)
    {
      return (x.slHolder + (0xFFFFFFFFL >> iInt)) >> iFrac;
    };

    //! Truncatenate to fixed number of decimal bits.
    friend inline TFixInt<iInt, iFrac> Trunc(TFixInt<iInt, iFrac> x, int iDecimalPart)
    {
      return TFixInt<iInt, iFrac>(x.slHolder & (0xFFFFFFFF * ((1 << iFrac) / iDecimalPart)), 1);
    };
};

#endif  /* include-once check. */
