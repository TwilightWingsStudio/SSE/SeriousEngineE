/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_NAMETABLE_CPP
#define SE_INCL_NAMETABLE_CPP

#include <Core/Templates/NameTable.h>
#include <Core/Templates/StaticArray.cpp>

#if NAMETABLE_CASESENSITIVE==1
  #define COMPARENAMES(a, b) (strcmp(a, b)==0)
#elif NAMETABLE_CASESENSITIVE==0
  #define COMPARENAMES(a, b) (a==b)
#else
  #error "NAMETABLE_CASESENSITIVE not defined"
#endif


// Default constructor
template<class Type>
TNameTable<Type>::TNameTable(void)
{
  nt_ctCompartments = 0;
  nt_ctSlotsPerComp = 0;
  nt_ctSlotsPerCompStep = 0;
}

// Destructor -- frees all memory
template<class Type>
TNameTable<Type>::~TNameTable(void) {}

// Remove all slots, and reset the nametable to initial (empty) state
template<class Type>
void TNameTable<Type>::Clear(void)
{
  nt_ctCompartments = 0;
  nt_ctSlotsPerComp = 0;
  nt_ctSlotsPerCompStep = 0;
  nt_antsSlots.Clear();
}

// Internal finding
template<class Type>
TNameTableSlot<Type> *TNameTable<Type>::FindSlot(ULONG ulKey, const FString &strName)
{
  ASSERT(nt_ctCompartments > 0 && nt_ctSlotsPerComp > 0);

  // Find compartment number
  INDEX iComp = ulKey%nt_ctCompartments;
  
  // For each slot in the compartment
  INDEX iSlot = iComp * nt_ctSlotsPerComp;
  for (INDEX iSlotInComp = 0; iSlotInComp < nt_ctSlotsPerComp; iSlotInComp++, iSlot++)
  {
    TNameTableSlot<Type> *pnts = &nt_antsSlots[iSlot];
    // If empty
    if (pnts->nts_ptElement == NULL) {
      continue; // skip it
    }
    
    // If it has same key
    if (pnts->nts_ulKey == ulKey)
    {
      // If it is same element
      if (COMPARENAMES(pnts->nts_ptElement->GetName(), strName)) {
        return pnts;  // return it
      }
    }
  }

  // Not found
  return NULL;
}

// Set allocation parameters.
template<class Type>
void TNameTable<Type>::SetAllocationParameters(INDEX ctCompartments, INDEX ctSlotsPerComp, INDEX ctSlotsPerCompStep)
{
  ASSERT(nt_ctCompartments == 0 && nt_ctSlotsPerComp == 0 && nt_ctSlotsPerCompStep == 0);
  ASSERT(ctCompartments > 0     && ctSlotsPerComp > 0     && ctSlotsPerCompStep > 0    );

  nt_ctCompartments = ctCompartments;
  nt_ctSlotsPerComp = ctSlotsPerComp;
  nt_ctSlotsPerCompStep = ctSlotsPerCompStep;

  nt_antsSlots.New(nt_ctCompartments * nt_ctSlotsPerComp);
}

// Find an object by name
template<class Type>
Type *TNameTable<Type>::Find(const FString &strName)
{
  ASSERT(nt_ctCompartments > 0 && nt_ctSlotsPerComp > 0);

  TNameTableSlot<Type> *pnts = FindSlot(strName.GetHash(), strName);
  if (pnts == NULL) {
    return NULL;
  }
  return pnts->nts_ptElement;
}

// Expand the name table to next step
template<class Type>
void TNameTable<Type>::Expand(void)
{
  ASSERT(nt_ctCompartments > 0 && nt_ctSlotsPerComp > 0);

  // If we are here -> the compartment has overflowed
  ASSERT(nt_ctSlotsPerCompStep > 0);

  // Move the array of slots
  TStaticArray<TSlotType> antsSlotsOld;
  antsSlotsOld.MoveArray(nt_antsSlots);

  // Allocate new bigger array
  INDEX ctOldSlotsPerComp = nt_ctSlotsPerComp;
  nt_ctSlotsPerComp += nt_ctSlotsPerCompStep;
  nt_antsSlots.New(nt_ctSlotsPerComp * nt_ctCompartments);

  // for each compartment
  for (INDEX iComp = 0; iComp < nt_ctCompartments; iComp++)
  {
    // For each old slot in compartment
    for (INDEX iSlotInComp = 0; iSlotInComp < ctOldSlotsPerComp; iSlotInComp++)
    {
      TNameTableSlot<Type> &ntsOld = antsSlotsOld[iSlotInComp + iComp * ctOldSlotsPerComp];
      TNameTableSlot<Type> &ntsNew = nt_antsSlots[iSlotInComp + iComp * nt_ctSlotsPerComp];
      
      // If it is used
      if (ntsOld.nts_ptElement != NULL) {
        // Copy it to new array
        ntsNew.nts_ptElement = ntsOld.nts_ptElement;
        ntsNew.nts_ulKey     = ntsOld.nts_ulKey;
      }
    }
  }
}

static BOOL _bExpanding = FALSE;  // check to prevend recursive expanding

// Add a new object
template<class Type>
void TNameTable<Type>::Add(Type *ptNew)
{
  ASSERT(nt_ctCompartments > 0 && nt_ctSlotsPerComp > 0);

  ULONG ulKey = ptNew->GetName().GetHash();

  // Find compartment number
  INDEX iComp = ulKey % nt_ctCompartments;
  
  // For each slot in the compartment
  INDEX iSlot = iComp * nt_ctSlotsPerComp;
  for (INDEX iSlotInComp = 0; iSlotInComp < nt_ctSlotsPerComp; iSlotInComp++, iSlot++)
  {
    TNameTableSlot<Type> *pnts = &nt_antsSlots[iSlot];
    
    // If it is empty
    if (pnts->nts_ptElement == NULL) {
      // Put it here
      pnts->nts_ulKey = ulKey;
      pnts->nts_ptElement = ptNew;
      return;
    }
    
    // Must not already exist
    //ASSERT(pnts->nts_ptElement->GetName()!=ptNew->GetName());
  }

  // if we are here -> the compartment has overflowed

  // Expand the name table to next step
  ASSERT(!_bExpanding);
  _bExpanding = TRUE;
  Expand();
  
  // Add the new element
  Add(ptNew);
  _bExpanding = FALSE;
}

// Remove an object
template<class Type>
void TNameTable<Type>::Remove(Type *ptOld)
{
  ASSERT(nt_ctCompartments > 0 && nt_ctSlotsPerComp > 0);
  
  // Find its slot
  const FString &strName = ptOld->GetName();
  TNameTableSlot<Type> *pnts = FindSlot(strName.GetHash(), strName);
  if (pnts != NULL) {
    // Mark slot as unused
    ASSERT(pnts->nts_ptElement == ptOld);
    pnts->nts_ptElement = NULL;
  }
}

// Remove all objects but keep slots
template<class Type>
void TNameTable<Type>::Reset(void)
{
  for (INDEX iSlot = 0; iSlot < nt_antsSlots.Count(); iSlot++)
  {
    nt_antsSlots[iSlot].Clear();
  }
}

#undef NAMETABLE_CASESENSITIVE

#endif