/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_DYNAMICSTACKARRAY_CPP
#define SE_INCL_DYNAMICSTACKARRAY_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicStackArray.h>
#include <Core/Templates/DynamicArray.cpp>

// Default constructor.
template<class Type>
inline TDynamicStackArray<Type>::TDynamicStackArray(void) : TDynamicArray<Type>()
{
  da_ctUsed = 0;
  da_ctAllocationStep = 256;
  // lock the array on construction
  TDynamicArray<Type>::Lock();
}

// Destructor.
template<class Type>
inline TDynamicStackArray<Type>::~TDynamicStackArray(void)
{
  // Lock the array on destruction
  TDynamicArray<Type>::Unlock();
};

// Destroy all objects, and reset the array to initial (empty) state.
template<class Type>
inline void TDynamicStackArray<Type>::Clear(void)
{
  TDynamicArray<Type>::Clear(); da_ctUsed = 0; 
}

// Set how many elements to allocate when stack overflows.
template<class Type>
inline void TDynamicStackArray<Type>::SetAllocationStep(ULONG ctStep) 
{
  ASSERT(ctStep > 0);
  da_ctAllocationStep = ctStep;
};

// Add new object(s) on top of stack.
template<class Type>
inline Type &TDynamicStackArray<Type>::Push(void)
{
  // If there are no free elements in the array
  if (TDynamicArray<Type>::Count() - da_ctUsed < 1) {
    // alocate a new block
    TDynamicArray<Type>::New(da_ctAllocationStep);
  }
  
  // Get the new element
  da_ctUsed++;
  ASSERT(da_ctUsed <= TDynamicArray<Type>::Count());
  return TDynamicArray<Type>::operator[](da_ctUsed - 1);
}
template<class Type>
inline Type *TDynamicStackArray<Type>::Push(ULONG ct)
{
  // If there are no free elements in the array
  while (TDynamicArray<Type>::Count() - da_ctUsed < ct)
  {
    // Alocate a new block
    TDynamicArray<Type>::New(da_ctAllocationStep);
  }
  
  // Get new elements
  da_ctUsed+=ct;
  ASSERT(da_ctUsed <= TDynamicArray<Type>::Count());
  return &TDynamicArray<Type>::operator[](da_ctUsed - ct);
}

// Remove all objects from stack, but keep stack space.
template<class Type>
inline void TDynamicStackArray<Type>::PopAll(void) {
  // If there is only one block allocated
  if (this->da_BlocksList.IsEmpty() || &this->da_BlocksList.Head() == &this->da_BlocksList.Tail()) {
    // Just clear the counter
    da_ctUsed = 0;

  // If there is more than one block allocated
  } else {
    // Remember how much was allocated, rounded up to allocation step
    ULONG ctUsedBefore = TDynamicArray<Type>::Count();
    
    // Free all memory
    TDynamicArray<Type>::Clear();
    
    // Allocate one big block
    TDynamicArray<Type>::New(ctUsedBefore);
    da_ctUsed = 0;
  }
}

// Random access operator.
template<class Type>
inline Type &TDynamicStackArray<Type>::operator[](ULONG i)
{
  ASSERT(this != NULL);
  ASSERT(i < da_ctUsed);     // check bounds
  return TDynamicArray<Type>::operator[](i);
}

template<class Type>
inline const Type &TDynamicStackArray<Type>::operator[](ULONG i) const
{
  ASSERT(this != NULL);
  ASSERT(i < da_ctUsed);     // check bounds
  return TDynamicArray<Type>::operator[](i);
}

// Get number of elements in array.
template<class Type>
ULONG TDynamicStackArray<Type>::Count(void) const
{
  ASSERT(this != NULL);
  return da_ctUsed;
}

// Get index of a member from it's pointer
template<class Type>
ULONG TDynamicStackArray<Type>::Index(Type *ptMember)
{
  ASSERT(this != NULL);
  ULONG i = TDynamicArray<Type>::Index(ptMember);
  ASSERTMSG(i<da_ctUsed, "TDynamicStackArray<>::Index(): Not a member of this array!");
  return i;
}

// Get array of pointers to elements (used for sorting elements by sorting pointers).
template<class Type>
Type **TDynamicStackArray<Type>::GetArrayOfPointers(void)
{
  return this->da_Pointers;
}

// Assignment operator.
template<class Type>
TDynamicStackArray<Type> &TDynamicStackArray<Type>::operator=(TDynamicStackArray<Type> &arOriginal)
{
  ASSERT(this != NULL);
  ASSERT(&arOriginal != NULL);
  ASSERT(this != &arOriginal);

  // Copy stack arrays
  TDynamicArray<Type>::operator=(arOriginal);
  
  // Copy used count
  da_ctUsed = arOriginal.da_ctUsed;

  return *this;
}

#endif  /* include-once check. */