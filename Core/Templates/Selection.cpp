/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SELECTION_CPP
#define SE_INCL_SELECTION_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/Selection.h>
#include <Core/Templates/DynamicContainer.cpp>

// Select one object.
template<class cType, unsigned long ulFlag>
void TSelection<cType, ulFlag>::Select(cType &tToSelect)
{
  // If the object is not yet selected
  if (!tToSelect.IsSelected(ulFlag)) {
    // Select it
    tToSelect.Select(ulFlag);
    // Add it to this container
    Add(&tToSelect);

  // If the object is already selected
  } else {
    ASSERTALWAYS("Object already selected!");
  }
}

// Deselect one object.
template<class cType, unsigned long ulFlag>
void TSelection<cType, ulFlag>::Deselect(cType &tToSelect)
{
  // If the object is selected
  if (tToSelect.IsSelected(ulFlag)) {
    // Deselect it
    tToSelect.Deselect(ulFlag);
    
    // Remove it from this container
    Remove(&tToSelect);

  // If the object is not selected
  } else {
    ASSERTALWAYS("Object is not selected!");
  }
}

// Test if one object is selected.
template<class cType, unsigned long ulFlag>
BOOL TSelection<cType, ulFlag>::IsSelected(cType &tToSelect)
{
  // Test if the object is selected
  return tToSelect.IsSelected(ulFlag);
}

// Deselect all objects.
template<class cType, unsigned long ulFlag>
void TSelection<cType, ulFlag>::Clear(void)
{
  // For all objects in the container
  FOREACHINDYNAMICCONTAINER(*this, cType, itObject)
  {
    // Object must be allocated and valid
    ASSERT(_CrtIsValidPointer(&*itObject, sizeof(cType), TRUE));
    //ASSERT(_CrtIsValidHeapPointer(&*itObject));
    //ASSERT(_CrtIsMemoryBlock(&*itObject, sizeof(cType), NULL, NULL, NULL ));

    // Ddeselect it
    itObject->Deselect(ulFlag);
  }
  
  // Clear the entire container at once
  TDynamicContainer<cType>::Clear();
}

// TODO: Add comment here
template<class cType, unsigned long ulFlag>
cType *TSelection<cType, ulFlag>::GetFirstInSelection(void)
{
  if (Count() == 0) {
    return NULL;
  }
  return (cType *) &TDynamicContainer<cType>::GetFirst();
}

#endif  /* include-once check. */