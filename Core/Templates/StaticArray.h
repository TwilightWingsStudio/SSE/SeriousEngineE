/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATICARRAY_H
#define SE_INCL_STATICARRAY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

/*
 * Template class for array with static allocation of objects.
 */
template<class Type>
class TStaticArray
{
  public:
    ULONG sa_Count;      // number of objects in array
    Type *sa_Array;      // objects
  public:
    //! Default constructor. 
    inline TStaticArray(void);
    
    //! Destructor. 
    inline ~TStaticArray(void);
    void operator=(const TStaticArray<Type> &arOriginal);

    //! Create a given number of objects. 
    inline void New(ULONG iCount);
    
    //! Expand stack size but keep old objects. 
    inline void Expand(ULONG iNewCount);
    
    //! Destroy all objects. 
    inline void Delete(void);
    
    //! Destroy all objects, and reset the array to initial (empty) state. 
    inline void Clear(void);

    //! Random access operator. 
    inline Type &operator[](ULONG iObject);
    inline const Type &operator[](ULONG iObject) const;
    
    //! Get number of objects in array. 
    virtual ULONG Count(void) const;
    
    //! Get index of a object from it's pointer. 
    ULONG Index(Type *ptObject);

    //! Copy all elements of another array into this one. 
    void CopyArray(const TStaticArray<Type> &arOriginal);
    
    //! Move all elements of another array into this one. 
    void MoveArray(TStaticArray<Type> &arOther);

    //! Sort elements.
    void Sort(int (*pCompare)(const void *p0, const void *p1));
};

#endif  /* include-once check. */