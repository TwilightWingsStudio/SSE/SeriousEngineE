/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */


#ifndef SE_INCL_STATICSTACKARRAY_CPP
#define SE_INCL_STATICSTACKARRAY_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticStackArray.h>
#include <Core/Templates/StaticArray.cpp>

// Default constructor.
template<class Type>
inline TStaticStackArray<Type>::TStaticStackArray(void) : TStaticArray<Type>()
{
  sa_UsedCount = 0;
  sa_ctAllocationStep = 256;
}

// Destructor.
template<class Type>
inline TStaticStackArray<Type>::~TStaticStackArray(void) {};

// Destroy all objects, and reset the array to initial (empty) state.
template<class Type>
inline void TStaticStackArray<Type>::Clear(void)
{
  if (TStaticArray<Type>::Count() != 0) {
    Delete(); 
  }
}

// Set how many elements to allocate when stack overflows.
template<class Type>
inline void TStaticStackArray<Type>::SetAllocationStep(ULONG ctStep) 
{
  ASSERT(ctStep > 0);
  sa_ctAllocationStep = ctStep;
};

// Create a given number of objects.
template<class Type>
inline void TStaticStackArray<Type>::New(ULONG iCount)
{
  TStaticArray<Type>::New(iCount);
  sa_UsedCount = 0;
};

// Destroy all objects.
template<class Type>
inline void TStaticStackArray<Type>::Delete(void)
{
  TStaticArray<Type>::Delete();
  sa_UsedCount = 0;
}

// Add new object(s) on top of stack.
template<class Type>
inline Type &TStaticStackArray<Type>::Push(void)
{
  sa_UsedCount++;
  if (sa_UsedCount > TStaticArray<Type>::Count()) {
    this->Expand(TStaticArray<Type>::Count() + sa_ctAllocationStep);
  }

  ASSERT(sa_UsedCount <= TStaticArray<Type>::Count());
  return TStaticArray<Type>::operator[](sa_UsedCount - 1);
}

// Add new object(s) on top of stack.
template<class Type>
inline Type *TStaticStackArray<Type>::Push(ULONG ct)
{
  sa_UsedCount += ct;
  while (sa_UsedCount > TStaticArray<Type>::Count())
  {
    this->Expand(TStaticArray<Type>::Count() + sa_ctAllocationStep);
  }
  ASSERT(sa_UsedCount <= TStaticArray<Type>::Count());
  return &TStaticArray<Type>::operator[](sa_UsedCount - ct);
}

// Remove one object from top of stack and return it.
template<class Type>
inline Type &TStaticStackArray<Type>::Pop(void)
{
  ASSERT(sa_UsedCount > 0);
  sa_UsedCount--;
  return TStaticArray<Type>::operator[](sa_UsedCount);
}

// Remove objects higher than the given index from stack, but keep stack space.
// Not a ULONG because negative numbers may be passed. -1 means make no one element used.
template<class Type>
inline void TStaticStackArray<Type>::PopUntil(INDEX iNewTop)
{
  ASSERT(iNewTop < INDEX(sa_UsedCount));
  sa_UsedCount = iNewTop + 1;
}

// Remove all objects from stack, but keep stack space.
template<class Type>
inline void TStaticStackArray<Type>::PopAll(void)
{
  sa_UsedCount = 0;
}

// Random access operator.
template<class Type>
inline Type &TStaticStackArray<Type>::operator[](ULONG i)
{
  ASSERT(this != NULL);
  ASSERT(i < sa_UsedCount);     // check bounds
  return TStaticArray<Type>::operator[](i);
}

// Random access operator.
template<class Type>
inline const Type &TStaticStackArray<Type>::operator[](ULONG i) const
{
  ASSERT(this != NULL);
  ASSERT(i < sa_UsedCount);     // check bounds
  return TStaticArray<Type>::operator[](i);
}

// Get number of elements in array.
template<class Type>
ULONG TStaticStackArray<Type>::Count(void) const
{
  ASSERT(this != NULL);
  return sa_UsedCount;
}

// Get index of a member from it's pointer
template<class Type>
ULONG TStaticStackArray<Type>::Index(Type *ptMember)
{
  ASSERT(this != NULL);
  ULONG i = TStaticArray<Type>::Index(ptMember);
  ASSERTMSG(i < sa_UsedCount, "TStaticStackArray<>::Index(): Not a member of this array!");
  return i;
}

// Assignment operator.
template<class Type>
TStaticStackArray<Type> &TStaticStackArray<Type>::operator=(const TStaticStackArray<Type> &arOriginal)
{
  ASSERT(this != NULL);
  ASSERT(&arOriginal != NULL);
  ASSERT(this != &arOriginal);

  // Copy stack arrays
  TStaticArray<Type>::operator=(arOriginal);
  
  // Copy used count
  sa_UsedCount = arOriginal.sa_UsedCount;

  return *this;
}


// Move all elements of another array into this one.
template<class Type>
void TStaticStackArray<Type>::MoveArray(TStaticStackArray<Type> &arOther)
{
  ASSERT(this != NULL);
  ASSERT(&arOther != NULL);
  ASSERT(this != &arOther);

  // Clear previous contents
  Clear();
  
  // If the other array has no elements
  if (arOther.Count() == 0) {
    return; // no assignment
  }
  
  // Move data from the other array into this one and clear the other one
  TStaticArray<Type>::MoveArray(arOther);
  sa_UsedCount        = arOther.sa_UsedCount;
  sa_ctAllocationStep = arOther.sa_ctAllocationStep;
  arOther.sa_UsedCount        = 0;
}

#endif  /* include-once check. */