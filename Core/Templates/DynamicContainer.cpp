/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_DYNAMICCONTAINER_CPP
#define SE_INCL_DYNAMICCONTAINER_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>
#include <Core/Base/Memory.h>
#include <Core/Templates/StaticStackArray.cpp>

// Default constructor.
template<class Type>
TDynamicContainer<Type>::TDynamicContainer(void)
{
#if CHECKARRAYLOCKING
  // not locked
  dc_LockCt = 0;
#endif
}

// Copy constructor.
template<class Type>
TDynamicContainer<Type>::TDynamicContainer(TDynamicContainer<Type> &dcOriginal)
{
#if CHECKARRAYLOCKING
  // not locked
  dc_LockCt = 0;
#endif

  // Call assignment operator
  (*this) = dcOriginal;
}

// Destructor -- removes all objects.
template<class Type>
TDynamicContainer<Type>::~TDynamicContainer(void)
{
  Clear();
}

// Remove all objects, and reset the array to initial (empty) state.
template<class Type>
void TDynamicContainer<Type>::Clear(void)
{
  ASSERT(this != NULL);
  TStaticStackArray<Type*>::Clear();
}

// Add a given object to container.
template<class Type>
void TDynamicContainer<Type>::Add(Type *ptNewObject)
{
  // Set the new pointer
  this->Push() = ptNewObject;
}

// Insert a given object to container at specified index.
template<class Type>
void TDynamicContainer<Type>::Insert(Type *ptNewObject, const ULONG iPos/*=0*/)
{
  // Get number of member that need moving and add new one
  const ULONG ctMovees = TStaticStackArray<Type*>::Count() - iPos;
  TStaticStackArray<Type*>::Push();
  
  // Move all members after insert position one place up
  Type **pptInsertAt = this->sa_Array+iPos;
  Type **pptMoveTo   = pptInsertAt + 1;
  memmove(pptMoveTo, pptInsertAt, sizeof(Type*) * ctMovees);
  
  // Store pointer to newly inserted member at specified position
  *pptInsertAt = ptNewObject;
}

// Remove a given object from container.
template<class Type>
void TDynamicContainer<Type>::Remove(Type *ptOldObject)
{
  ASSERT(this != NULL);
#if CHECKARRAYLOCKING
  // Check that not locked for indices
  ASSERT(dc_LockCt == 0);
#endif

  // Find its index
  ULONG iMember = GetIndex(ptOldObject);
  
  // Move last pointer here
  this->sa_Array[iMember] = this->sa_Array[this->Count() - 1];
  this->Pop();
}

// Test if a given object is in the container.
template<class Type>
BOOL TDynamicContainer<Type>::IsMember(Type *ptOldObject)
{
  ASSERT(this != NULL);
  // slow !!!!
  
  // Check all members
  for (ULONG iMember = 0; iMember < this->Count(); iMember++)
  {
    if (this->sa_Array[iMember] == ptOldObject) {
      return TRUE;
    }
  }
  return FALSE;
}

// Get pointer to a member from it's index.
template<class Type>
Type *TDynamicContainer<Type>::Pointer(ULONG iMember)
{
  ASSERT(this != NULL);
  
  // Check that index is currently valid
  ASSERT(iMember < Count());

#if CHECKARRAYLOCKING
  // Check that locked for indices
  ASSERT(dc_LockCt > 0);
#endif
  return this->sa_Array[iMember];
}

template<class Type>
const Type *TDynamicContainer<Type>::Pointer(ULONG iMember) const
{
  ASSERT(this != NULL);
  
  // Check that index is currently valid
  ASSERT(iMember >= 0 && iMember < Count());
#if CHECKARRAYLOCKING
  // Check that locked for indices
  ASSERT(dc_LockCt > 0);
#endif
  return this->sa_Array[iMember];
}

// Lock for getting indices.
template<class Type>
void TDynamicContainer<Type>::Lock(void)
{
  ASSERT(this != NULL);
#if CHECKARRAYLOCKING
  ASSERT(dc_LockCt >= 0);
  
  // Increment lock counter
  dc_LockCt++;
#endif
}

// Unlock after getting indices.
template<class Type>
void TDynamicContainer<Type>::Unlock(void)
{
  ASSERT(this!=NULL);
#if CHECKARRAYLOCKING
  dc_LockCt--;
  ASSERT(dc_LockCt >= 0);
#endif
}

// Get index of a member from it's pointer.
template<class Type>
ULONG TDynamicContainer<Type>::Index(Type *ptMember)
{
  ASSERT(this != NULL);
  // Check that locked for indices
#if CHECKARRAYLOCKING
  ASSERT(dc_LockCt > 0);
#endif
  return GetIndex(ptMember);
}

// Get index of a member from it's pointer without locking.
template<class Type>
ULONG TDynamicContainer<Type>::GetIndex(Type *ptMember)
{
  ASSERT(this != NULL);
  // slow !!!!
  
  // Check all members
  for (ULONG iMember = 0; iMember < this->Count(); iMember++)
  {
    if (this->sa_Array[iMember] == ptMember) {
      return iMember;
    }
  }
  ASSERTALWAYS("TDynamicContainer<Type><>::Index(): Not a member of this container!");
  return 0;
}

// Get first object in container (there must be at least one when calling this).
template<class Type>
Type &TDynamicContainer<Type>::GetFirst(void)
{
  ASSERT(Count() >= 1);
  return *this->sa_Array[0];
}

// Assignment operator.
template<class Type>
TDynamicContainer<Type> &TDynamicContainer<Type>::operator=(TDynamicContainer<Type> &coOriginal)
{
  TStaticStackArray<Type *>::operator=(coOriginal);
  return *this;
}

// Move all elements of another array into this one.
template<class Type>
void TDynamicContainer<Type>::MoveContainer(TDynamicContainer<Type> &coOther)
{
  ASSERT(this != NULL && &coOther != NULL);
  // check that not locked for indices
#if CHECKARRAYLOCKING
  ASSERT(dc_LockCt == 0 && coOther.dc_LockCt == 0);
#endif
  TStaticStackArray<Type*>::MoveArray(coOther);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TDynamicContainerIterator<Type>

// Template class for iterating dynamic array.
template<class Type>
class TDynamicContainerIterator
{
  private:
    ULONG dci_Index;               // index of current element
    TDynamicContainer<Type> &dci_Array;   // reference to array
  public:
    // Constructor for given array. 
    inline TDynamicContainerIterator(TDynamicContainer<Type> &da);
    
    // Destructor. 
    inline ~TDynamicContainerIterator(void);

    // Move to next object. 
    inline void MoveToNext(void);
    
    // Check if finished. 
    inline BOOL IsPastEnd(void);
    
    // Get current element. 
    Type &Current(void)
    {
      return *dci_Array.Pointer(dci_Index);
    }
    
    Type &operator*(void)
    {
      return *dci_Array.Pointer(dci_Index);
    }
    
    operator Type *(void)
    {
      return dci_Array.Pointer(dci_Index);
    }
    
    Type *operator->(void)
    {
      return dci_Array.Pointer(dci_Index);
    }
};

// Constructor for given array.
template<class Type>
inline TDynamicContainerIterator<Type>::TDynamicContainerIterator(TDynamicContainer<Type> &da) : dci_Array(da)
{
  // Lock indices
  dci_Array.Lock();
  dci_Index = 0;
}

// Destructor.
template<class Type>
inline TDynamicContainerIterator<Type>::~TDynamicContainerIterator(void)
{
  // Unlock indices
  dci_Array.Unlock();
  dci_Index = -1;
}

// Move to next object.
template<class Type>
inline void TDynamicContainerIterator<Type>::MoveToNext(void)
{
  ASSERT(this != NULL);
  dci_Index++;
}

// Check if finished.
template<class Type>
inline BOOL TDynamicContainerIterator<Type>::IsPastEnd(void)
{
  ASSERT(this != NULL);
  return dci_Index >= dci_Array.Count();
}

// Iterate whole dynamic container
// NOTE: The iterator defined by this macro must be destroyed before adding/removing
// elements in the container. To do so, embed the for loop in additional curly braces.
#define FOREACHINDYNAMICCONTAINER(container, type, iter) \
  for (TDynamicContainerIterator<type> iter(container); !iter.IsPastEnd(); iter.MoveToNext() )

template<class Type>
void TDynamicContainer<Type>::Find(TDynamicContainer<Type> &coOutput, BOOL (*pCompare)(const Type *p))
{
  ASSERT(coOutput.Count() == 0);

  FOREACHINDYNAMICCONTAINER(*this, Type, itob) 
  {
    Type *ptObject = &itob.Current();
    
    if ((*pCompare)(ptObject)) {
      coOutput.Add(ptObject);
    }
  }
}

template<class Type>
void TDynamicContainer<Type>::Find(TDynamicContainer<Type> &coOutput, const TObjectFunctor<BOOL, Type> &func)
{
  ASSERT(coOutput.Count() == 0);

  FOREACHINDYNAMICCONTAINER(*this, Type, itob) 
  {
    Type *ptObject = &itob.Current();
    
    if (func(ptObject)) {
      coOutput.Add(ptObject);
    }
  }
}

#endif  /* include-once check. */