/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ALLOCATIONARRAY_H
#define SE_INCL_ALLOCATIONARRAY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticStackArray.h>

/*
 * Template class for stack-like array with static allocation of objects.
 */
template<class Type>
class TAllocationArray : public TStaticArray<Type>
{
  public:
    TStaticStackArray<ULONG> aa_aiFreeElements; // array of indices of free elements
    ULONG aa_ctAllocationStep;  // how many elements to allocate when pool overflows

  public:
    //! Default constructor.
    inline TAllocationArray(void);
    
    //! Destructor.
    inline ~TAllocationArray(void);

    //! Set how many elements to allocate when pool overflows.
    inline void SetAllocationStep(ULONG ctStep);
    
    //! Create a given number of objects - do not use.
    inline void New(ULONG iCount);
    
    //! Destroy all objects - do not use.
    inline void Delete(void);
    
    //! Destroy all objects, and reset the array to initial (empty) state.
    inline void Clear(void);

    //! Alocate a new object.
    inline ULONG Allocate(void);
    
    //! Free object with given index.
    inline void Free(ULONG iToFree);
    
    //! Free all objects, but keep pool space.
    inline void FreeAll(void);

    //! check if an index is allocated (slow!)
    inline BOOL IsAllocated(ULONG i);

    //! Random access operator.
    inline Type &operator[](ULONG iObject);

    //! Read only random access operator.
    inline const Type &operator[](ULONG iObject) const;
    
    //! Get number of allocated objects in array.
    ULONG Count(void) const;
    
    //! Get index of a object from it's pointer.
    ULONG Index(Type *ptObject);

    //! Assignment operator.
    TAllocationArray<Type> &operator=(const TAllocationArray<Type> &aaOriginal);
};

#endif  /* include-once check. */