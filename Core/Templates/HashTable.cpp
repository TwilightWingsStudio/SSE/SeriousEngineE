/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_HASHTABLE_CPP
#define SE_INCL_HASHTABLE_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core\Base\Console.h>
#include <math.h>
#include <Core\Base\Translation.h>

// default constructor
template<class Type, class ValueType>
THashTable<Type, ValueType>::THashTable()
{
  ht_ctCompartments = 0;
  ht_ctSlotsPerComp = 0;
  ht_ctSlotsPerCompStep = 0;
  ht_GetItemKey = NULL;
  ht_GetItemValue = NULL;
}

// Destructor -- frees all memory
template<class Type, class ValueType>
THashTable<Type, ValueType>::~THashTable(void) {}

// Remove all slots, and reset the nametable to initial (empty) state, keeps the callback functions
template<class Type, class ValueType>
void THashTable<Type, ValueType>::Clear(void)
{
  ht_ctCompartments = 0;
  ht_ctSlotsPerComp = 0;
  ht_ctSlotsPerCompStep = 0;
  ht_ahtsSlots.Clear();
}

// Internal finding, returns pointer to the the item
template<class Type, class ValueType>
THashTableSlot<ValueType> *THashTable<Type, ValueType>::FindSlot(ULONG ulKey, ValueType &Value) 
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);

  // Find compartment number
  INDEX iComp = ulKey % ht_ctCompartments;
  
  // For each slot in the compartment
  INDEX iSlot = iComp * ht_ctSlotsPerComp;
  for (INDEX iSlotInComp = 0; iSlotInComp < ht_ctSlotsPerComp; iSlotInComp++, iSlot++)
  {
    THashTableSlot *phts = &ht_ahtsSlots[iSlot];
    // If empty
    if (phts->hts_ptElement == NULL) {
      continue; // skip it
    }
    
    // If it has same key
    if (phts->hts_ulKey == ulKey)
    {
      // If it is same element
      if (ht_GetItemValue(phts->hts_ptElement) == Value) {
        return phts;
      }
    }
  }

  // not found
  return NULL;
}

// Internal finding, returns the index of the item in the nametable
template<class Type, class ValueType>
INDEX THashTable<Type, ValueType>::FindSlotIndex(ULONG ulKey, ValueType &Value)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);

  // Find compartment number
  INDEX iComp = ulKey%ht_ctCompartments;
  
  // For each slot in the compartment
  INDEX iSlot = iComp * ht_ctSlotsPerComp;
  for (INDEX iSlotInComp = 0; iSlotInComp < ht_ctSlotsPerComp; iSlotInComp++, iSlot++)
  {
    THashTableSlot<ValueType> *phts = &ht_ahtsSlots[iSlot];
    // If empty
    if (phts->hts_ptElement == NULL) {
      continue; // skip it
    }
    
    // If it has same key
    if (phts->hts_ulKey == ulKey)
    {
      // If it is same element
      if (ht_GetItemValue(phts->hts_ptElement) == Value) {
        return iSlot; // return it
      }
    }
  }

  // not found
  return -1;
}

template<class Type, class ValueType>
Type* THashTable<Type, ValueType>::GetItemFromIndex(INDEX iIndex)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);
  ASSERT(iIndex >= 0 && iIndex<ht_ctCompartments * ht_ctSlotsPerComp);

  return ht_ahtsSlots[iIndex].hts_ptElement;
}

template<class Type, class ValueType>
ValueType THashTable<Type, ValueType>::GetValueFromIndex(INDEX iIndex)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);
  ASSERT(iIndex >= 0 && iIndex < ht_ctCompartments * ht_ctSlotsPerComp);
 
  return ht_GetItemValue(ht_ahtsSlots[iIndex].hts_ptElement);
}

// Set allocation parameters.
template<class Type, class ValueType>
void THashTable<Type, ValueType>::SetAllocationParameters(INDEX ctCompartments, INDEX ctSlotsPerComp, INDEX ctSlotsPerCompStep)
{
  ASSERT(ht_ctCompartments == 0 && ht_ctSlotsPerComp == 0 && ht_ctSlotsPerCompStep == 0);
  ASSERT(ctCompartments > 0     && ctSlotsPerComp > 0     && ctSlotsPerCompStep > 0    );

  ht_ctCompartments = ctCompartments;
  ht_ctSlotsPerComp = ctSlotsPerComp;
  ht_ctSlotsPerCompStep = ctSlotsPerCompStep;

  ht_ahtsSlots.New(ht_ctCompartments * ht_ctSlotsPerComp);
}

// TODO: Add comment here
template<class Type, class ValueType>
void THashTable<Type, ValueType>::SetCallbacks(ULONG (*GetItemKey)(ValueType &Item), ValueType (*GetItemValue)(Type* Item))
{
  ASSERT(GetItemKey != NULL);
  ASSERT(GetItemValue != NULL);

  ht_GetItemKey = GetItemKey;
  ht_GetItemValue = GetItemValue;
}

// Find an object by name
template<class Type, class ValueType>
Type *THashTable<Type, ValueType>::Find(ValueType &Value)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);

  THashTableSlot *phts = FindSlot(ht_GetItemKey(Value), Value);
  if (phts == NULL) {
    return NULL;
  }
  return phts->hts_ptElement;
}

// Find an object by name, return it's index
template<class Type, class ValueType>
INDEX THashTable<Type, ValueType>::FindIndex(ValueType &Value)
{
  ASSERT(ht_ctCompartments>0 && ht_ctSlotsPerComp>0);

  return FindSlotIndex(ht_GetItemKey(Value), Value);
}

// Expand the name table to next step
template<class Type, class ValueType>
void THashTable<Type, ValueType>::Expand(void)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);

  // if we are here -> the compartment has overflowed
  ASSERT(ht_ctSlotsPerCompStep > 0);

  // Move the array of slots
  TStaticArray<THashTableSlot > ahtsSlotsOld;
  ahtsSlotsOld.MoveArray(ht_ahtsSlots);

  // Allocate new bigger array
  INDEX ctOldSlotsPerComp = ht_ctSlotsPerComp;
  ht_ctSlotsPerComp+=ht_ctSlotsPerCompStep;
  ht_ahtsSlots.New(ht_ctSlotsPerComp*ht_ctCompartments);

  // For each compartment
  for (INDEX iComp = 0; iComp < ht_ctCompartments; iComp++)
  {
    // For each old slot in compartment
    for (INDEX iSlotInComp = 0; iSlotInComp < ctOldSlotsPerComp; iSlotInComp++)
    {
      THashTableSlot &htsOld = ahtsSlotsOld[iSlotInComp + iComp * ctOldSlotsPerComp];
      THashTableSlot &htsNew = ht_ahtsSlots[iSlotInComp + iComp * ht_ctSlotsPerComp];
      // If it is used
      if (htsOld.hts_ptElement != NULL) {
        // Copy it to new array
        htsNew.hts_ptElement = htsOld.hts_ptElement;
        htsNew.hts_ulKey     = htsOld.hts_ulKey;
      }
    }
  }
}

static BOOL _bExpanding = FALSE;  // check to prevent recursive expanding

// Add a new object
template<class Type, class ValueType>
void THashTable<Type, ValueType>::Add(Type *ptNew)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);

  ValueType Value = ht_GetItemValue(ptNew);
  ULONG ulKey = ht_GetItemKey(Value);

  // Find compartment number
  INDEX iComp = ulKey%ht_ctCompartments;
  
  // For each slot in the compartment
  INDEX iSlot = iComp*ht_ctSlotsPerComp;
  for (INDEX iSlotInComp = 0; iSlotInComp < ht_ctSlotsPerComp; iSlotInComp++, iSlot++)
  {
    THashTableSlot *phts = &ht_ahtsSlots[iSlot];
    // If it is empty
    if (phts->hts_ptElement == NULL) {
      // Put it here
      phts->hts_ulKey = ulKey;
      phts->hts_ptElement = ptNew;
      return;
    }
    // Must not already exist
    //ASSERT(phts->hts_ptElement->GetName()!=ptNew->GetName());
  }

  // if we are here -> the compartment has overflowed

  // Expand the name table to next step
  ASSERT(!_bExpanding);
  _bExpanding = TRUE;
  Expand();
  
  // Add the new element
  Add(ptNew);
  _bExpanding = FALSE;
}

// Remove an object
template<class Type, class ValueType>
void THashTable<Type, ValueType>::Remove(Type *ptOld)
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);
  // find its slot
  ValueType Value = ht_GetItemValue(ptOld);
  THashTableSlot *phts = FindSlot(ht_GetItemKey(Value), Value);
  if (phts != NULL) {
    // Mark slot as unused
    ASSERT(phts->hts_ptElement == ptOld);
    phts->hts_ptElement = NULL;
  }
}

// Remove an object
template<class Type, class ValueType>
void THashTable<Type, ValueType>::RemoveAll()
{
  ASSERT(ht_ctCompartments > 0 && ht_ctSlotsPerComp > 0);

  for (INDEX iComp = 0; iComp < ht_ctCompartments; iComp++)
  {
    // For each slot in the compartment
    INDEX iSlot = iComp * ht_ctSlotsPerComp;

    for (INDEX iSlotInComp = 0; iSlotInComp < ht_ctSlotsPerComp; iSlotInComp++, iSlot++)
    {
      // If it is not empty
      THashTableSlot &hts = ht_ahtsSlots[iSlot];
      if (ht_ahtsSlots[iSlot].hts_ptElement != NULL) {
        ht_ahtsSlots[iSlot].hts_ptElement = NULL;
      }
    }
  }
  return;
}

// Remove all objects but keep slots
template<class Type, class ValueType>
void THashTable<Type, ValueType>::Reset(void)
{
  for (INDEX iSlot = 0; iSlot < ht_ahtsSlots.Count(); iSlot++)
  {
    ht_ahtsSlots[iSlot].Clear();
  }
}

// TODO: Add comment here
template<class Type, class ValueType>
void THashTable<Type, ValueType>::ReportEfficiency()
{
  DOUBLE dSum  = 0;
  DOUBLE dSum2 = 0;
  ULONG  ulCount = 0;

  for (INDEX iComp = 0; iComp < ht_ctCompartments; iComp++)
  {
    INDEX iCount = 0;
    for (INDEX iSlot = iComp * ht_ctSlotsPerComp; iSlot < (iComp + 1) * ht_ctSlotsPerComp; iSlot++)
    {
      if (ht_ahtsSlots[iSlot].hts_ptElement != NULL) {
        iCount++;
        ulCount++;
      }
    }
    dSum += iCount;
    dSum2 += iCount * iCount;
  }

  DOUBLE dFullPercent,dAvg,dStDev;
  dFullPercent = (double) ulCount / (ht_ctCompartments * ht_ctSlotsPerComp); // percentage of full slots in the hash table
  dAvg = dSum / ht_ctCompartments; // average number of full slots per compartement
  dStDev = sqrt((dSum2 - 2 * dSum * dAvg + ulCount * dAvg * dAvg) / (ulCount - 1));

  CInfoF(TRANS("Hash table efficiency report:\n"));
  CInfoF(TRANS("  Compartements: %ld,  Slots per compartement: %ld,  Full slots: %ld\n"),
          ht_ctCompartments, ht_ctSlotsPerComp, ulCount);
  CInfoF(TRANS("  Percentage of full slots: %5.2f%%,  Average full slots per compartement: %5.2f \n"),
          dFullPercent * 100, dAvg);
  CInfoF(TRANS("  Standard deviation is: %5.2f\n"), dStDev);
}

#endif  /* include-once check. */