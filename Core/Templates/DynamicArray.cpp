/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_DYNAMICARRAY_CPP
#define SE_INCL_DYNAMICARRAY_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Memory.h>
#include <Core/Base/ListIterator.inl>

#include <Core/Templates/DynamicArray.h>

// iterate whole dynamic array
/* NOTE: The iterator defined by this macro must be destroyed before adding/removing
 * elements in the array. To do so, embed the for loop in additional curly braces.
 */
#define FOREACHINDYNAMICARRAY(array, type, iter) \
  for (TDynamicArrayIterator<type> iter(array); !iter.IsPastEnd(); iter.MoveToNext() )

/*
 * TODO: Add comment here
 */
class CDABlockInfo
{
  public:
    CListNode bi_ListNode;
    void *bi_Memory;
};

// Default constructor.
template<class Type>
TDynamicArray<Type>::TDynamicArray(void) {
#if CHECKARRAYLOCKING
  // Not locked
  da_LockCt = 0;
#endif
  // Set to empty array of pointers
  da_Pointers = NULL;
  da_Count = 0;
}

// Copy constructor.
template<class Type>
TDynamicArray<Type>::TDynamicArray(TDynamicArray<Type> &daOriginal)
{
#if CHECKARRAYLOCKING
  // Not locked
  da_LockCt = 0;
#endif
  // Set to empty array of pointers
  da_Pointers = NULL;
  da_Count = 0;

  // call assignment operator
  (*this) = daOriginal;
}

// Destructor -- frees all memory.
template<class Type>
TDynamicArray<Type>::~TDynamicArray(void)
{
  Clear();
}

// Destroy all objects, and reset the array to initial (empty) state.
template<class Type>
void TDynamicArray<Type>::Clear(void)
{
  ASSERT(this != NULL);
  
  // If any pointers are allocated
  if (da_Count != 0)
  {
    // NOTE: We must explicitly clear objects here, because array deleting does not call object destructors!
    // for all pointers
    for (ULONG iPointer = 0; iPointer < da_Count; iPointer++)
    {
      // Destroy the object that it points to
      ::Clear(*da_Pointers[iPointer]);
    }

    // Free the pointers
    FreeMemory(da_Pointers);
    
    // Mark as freed
    da_Pointers = NULL;
    da_Count = 0;
  } else {
    // Check that the pointers are really not allocated
    ASSERT(da_Pointers == NULL);
    // Nothing to free
  }
  
  // For all memory blocks
  FORDELETELIST(CDABlockInfo, bi_ListNode, da_BlocksList, itBlock)
  {
    // Free memory used by block (this doesn't call destructors - see note above!)
    delete[] (Type *)itBlock->bi_Memory;
    
    // Free memory used by block info
    delete &itBlock.Current();
  }
}

// Random access operator.
template<class Type>
Type &TDynamicArray<Type>::operator[](ULONG iObject)
{
  return *Pointer(iObject); 
};

template<class Type>
const Type &TDynamicArray<Type>::operator[](ULONG iObject) const
{
  return *Pointer(iObject); 
};

// Grow pointer array by a given number of members.
template<class Type>
void TDynamicArray<Type>::GrowPointers(ULONG iCount)
{
  ASSERT(this != NULL && iCount > 0);
  
  // If not yet allocated
  if (da_Count == 0) {
    // Check that the pointers are really not allocated
    ASSERT(da_Pointers == NULL);
    
    // Allocate
    da_Count = iCount;
    da_Pointers = (Type **)AllocMemory(da_Count*sizeof(Type*));
    
  // If allocated
  } else {
    // Grow to new size
    da_Count += iCount;
    GrowMemory((void **)&da_Pointers, da_Count*sizeof(Type*));
  }
}

// Shrink pointer array by a given number of members.
template<class Type>
void TDynamicArray<Type>::ShrinkPointers(ULONG iCount)
{
  ASSERT(this != NULL && iCount > 0);
  
  // Check that the pointers are allocated
  ASSERT(da_Pointers != NULL);

  // Decrement count
  da_Count -= iCount;
  
  // Checked that it has not dropped below zero
  ASSERT(da_Count >= 0);
  
  // If all pointers are freed by this
  if (da_Count == 0) {
    // Dree the array
    FreeMemory(da_Pointers);
    da_Pointers = NULL;
    
  // If some remain
  } else {
    // Shrink to new size
    ShrinkMemory((void **)&da_Pointers, da_Count*sizeof(Type*));
  }
}

// Allocate a new memory block.
template<class Type>
Type *TDynamicArray<Type>::AllocBlock(ULONG iCount)
{
  ASSERT(this != NULL && iCount > 0);
  Type *ptBlock;
  CDABlockInfo *pbi;

  // Allocate the memory and call constructors for all members (+1 for cache-prefetch opt)
  ptBlock = new Type[iCount + 1];     // call vector constructor, for better performance
  
  // Allocate the block info
  pbi = new CDABlockInfo;
  
  // Add the block to list
  da_BlocksList.AddTail(pbi->bi_ListNode);
  
  // Remember block memory
  pbi->bi_Memory = ptBlock;
  return ptBlock;
}

// Create a given number of new members.
template<class Type>
Type *TDynamicArray<Type>::New(ULONG iCount /*= 1*/) {
  ASSERT(this != NULL);
  
  // If no new members are needed in fact
  if (iCount == 0) {
    return NULL; // do nothing
  }
  Type *ptBlock;
  ULONG iOldCount = da_Count;

  // Grow the pointer table
  GrowPointers(iCount);
  
  // Allocate the memory block
  ptBlock = AllocBlock(iCount);
  
  // Set pointers
  for (ULONG iNewMember = 0; iNewMember < iCount; iNewMember++)
  {
    da_Pointers[iOldCount+iNewMember] = ptBlock + iNewMember;
  }
  return ptBlock;
}

// Delete a given member.
template<class Type>
void TDynamicArray<Type>::Delete(Type *ptMember)
{
  ASSERT(this != NULL);
#if CHECKARRAYLOCKING
  // Check that not locked for indices
  ASSERT(da_LockCt == 0);
#endif

  // Clear the object
  ::Clear(*ptMember);

  ULONG iMember=GetIndex(ptMember);
  
  // Move last pointer here
  da_Pointers[iMember]=da_Pointers[da_Count - 1];
  
  // Shrink pointers by one
  ShrinkPointers(1);
  // Do nothing to free memory
  //!!!!
}

// Get pointer to a member from it's index.
template<class Type>
Type *TDynamicArray<Type>::Pointer(ULONG iMember)
{
  ASSERT(this != NULL);
  
  // Check that index is currently valid
  ASSERT(iMember >= 0 && iMember < da_Count);
#if CHECKARRAYLOCKING

  // Check that locked for indices
  ASSERT(da_LockCt > 0);
#endif
  return da_Pointers[iMember];
}

template<class Type>
const Type *TDynamicArray<Type>::Pointer(ULONG iMember) const
{
  ASSERT(this != NULL);
  
  // Check that index is currently valid
  ASSERT(iMember >= 0 && iMember < da_Count);
#if CHECKARRAYLOCKING

  // Check that locked for indices
  ASSERT(da_LockCt > 0);
#endif
  return da_Pointers[iMember];
}

// Lock for getting indices.
template<class Type>
void TDynamicArray<Type>::Lock(void)
{
  ASSERT(this != NULL);
#if CHECKARRAYLOCKING
  ASSERT(da_LockCt >= 0);
  
  // Increment lock counter
  da_LockCt++;
#endif
}

// Unlock after getting indices.
template<class Type>
void TDynamicArray<Type>::Unlock(void)
{
  ASSERT(this != NULL);
#if CHECKARRAYLOCKING
  da_LockCt--;
  ASSERT(da_LockCt >= 0);
#endif
}

// Get index of a member from it's pointer.
template<class Type>
ULONG TDynamicArray<Type>::Index(Type *ptMember)
{
  ASSERT(this != NULL);
#if CHECKARRAYLOCKING

  // Check that locked for indices
  ASSERT(da_LockCt > 0);
#endif
  return GetIndex(ptMember);
}

// Get index of a member from it's pointer without locking.
template<class Type>
ULONG TDynamicArray<Type>::GetIndex(Type *ptMember)
{
  ASSERT(this != NULL);
  
  // slow !!!!
  
  // Check all members
  for (ULONG iMember = 0; iMember < da_Count; iMember++)
  {
    if (da_Pointers[iMember] == ptMember) {
      return iMember;
    }
  }
  ASSERTALWAYS("TDynamicArray<>::Index(): Not a member of this array!");
  return 0;
}

// Get number of elements in array.
template<class Type>
ULONG TDynamicArray<Type>::Count(void) const
{
  ASSERT(this != NULL);
  return da_Count;
}

// Assignment operator.
template<class Type>
TDynamicArray<Type> &TDynamicArray<Type>::operator=(TDynamicArray<Type> &arOriginal)
{
  ASSERT(this != NULL);
  ASSERT(&arOriginal != NULL);
  ASSERT(this != &arOriginal);
  
  // Clear previous contents
  Clear();
  
  // Get count of elements in original array
  ULONG ctOriginal = arOriginal.Count();
  
  // If the other array has no elements
  if (ctOriginal == 0) {
    // no assignment
    return*this;
  }
  
  // Create that much elements
  Type *atNew = New(ctOriginal);

  // Copy them all
  arOriginal.Lock();
  for (ULONG iNew = 0; iNew < ctOriginal; iNew++)
  {
    atNew[iNew] = arOriginal[iNew];
  }
  arOriginal.Unlock();

  return *this;
}

// Move all elements of another array into this one.
template<class Type>
void TDynamicArray<Type>::MoveArray(TDynamicArray<Type> &arOther)
{
  ASSERT(this != NULL && &arOther != NULL);
#if CHECKARRAYLOCKING
  // Check that not locked for indices
  ASSERT(da_LockCt == 0 && arOther.da_LockCt == 0);
#endif

  // If the other array has no elements
  if (arOther.da_Count == 0) {
    return; // no moving
  }

  // Remember number of elements
  ULONG iOldCount = da_Count;
  
  // Grow pointer array to add the pointers to elements of other array
  GrowPointers(arOther.da_Count);
  
  // For each pointer in other array
  for (ULONG iOtherPointer = 0; iOtherPointer < arOther.da_Count; iOtherPointer++)
  {
    // Copy it at the end of this array
    da_Pointers[iOldCount + iOtherPointer] = arOther.da_Pointers[iOtherPointer];
  }
  
  // Remove array of pointers in other array
  arOther.ShrinkPointers(arOther.da_Count);
  
  // Move list of allocated blocks from the other array to the end of this one
  da_BlocksList.MoveList(arOther.da_BlocksList);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TDynamicArrayIterator

/*
 * Template class for iterating dynamic array.
 */
template<class Type>
class TDynamicArrayIterator
{
  private:
    ULONG dai_Index;           // index of current element
    TDynamicArray<Type> &dai_Array;   // reference to array
  public:
    // Constructor for given array. 
    inline TDynamicArrayIterator(TDynamicArray<Type> &da);
    
    // Destructor. 
    inline ~TDynamicArrayIterator(void);

    // Move to next object. 
    inline void MoveToNext(void);
    
    // Check if finished. 
    inline BOOL IsPastEnd(void);
    
    // Get current element. 
    Type &Current(void)
    {
      return *dai_Array.Pointer(dai_Index);
    }
    
    Type &operator*(void)
    {
      return *dai_Array.Pointer(dai_Index);
    }
    
    operator Type *(void)
    {
      return dai_Array.Pointer(dai_Index);
    }
    
    Type *operator->(void)
    {
      return dai_Array.Pointer(dai_Index);
    }
};

// Constructor for given array.
template<class Type>
inline TDynamicArrayIterator<Type>::TDynamicArrayIterator(TDynamicArray<Type> &da) : dai_Array(da) {
  // Lock indices
  dai_Array.Lock();
  dai_Index = 0;
}

// Destructor.
template<class Type>
inline TDynamicArrayIterator<Type>::~TDynamicArrayIterator(void)
{
  // Unlock indices
  dai_Array.Unlock();
  dai_Index = -1;
}

// Move to next object.
template<class Type>
inline void TDynamicArrayIterator<Type>::MoveToNext(void)
{
  ASSERT(this != NULL);
  dai_Index++;
}

// Check if finished.
template<class Type>
inline BOOL TDynamicArrayIterator<Type>::IsPastEnd(void)
{
  ASSERT(this != NULL);
  return dai_Index >= dai_Array.Count();
}

#endif  /* include-once check. */