/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_NAMETABLE_H
#define SE_INCL_NAMETABLE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticArray.h>

//! Template class for a slot, containing an element under a certain hashing key.
template<class Type>
class TNameTableSlot
{
  public:
    ULONG nts_ulKey;      // hashing key
    Type *nts_ptElement;  // the element inhere
    TNameTableSlot(void)
    {
      nts_ptElement = NULL;
    };
    void Clear(void)
    {
      nts_ptElement = NULL;
    };
};

/*
 * Template class for storing pointers to objects for fast access by name.
 */
template<class Type>
class TNameTable
{
  // implementation:
  public:
    typedef TNameTableSlot<Type> TSlotType; // vc60 fallback

    INDEX nt_ctCompartments;    // number of compartments in table
    INDEX nt_ctSlotsPerComp;    // number of slots in one compartment
    INDEX nt_ctSlotsPerCompStep;    // allocation step for number of slots in one compartment
    TStaticArray<TSlotType> nt_antsSlots;  // all slots are here

    //! Internal finding
    TNameTableSlot<Type> *FindSlot(ULONG ulKey, const FString &strName);
    
    //! Expand the name table to next step
    void Expand(void);

  // interface:
  public:
    //! Default constructorю
    TNameTable(void);

    //! Destructor -- frees all memoryю
    ~TNameTable(void);

    //! Remove all slots, and reset the nametable to initial (empty) stateю
    void Clear(void);

    //! Set allocation parameters.
    void SetAllocationParameters(INDEX ctCompartments, INDEX ctSlotsPerComp, INDEX ctSlotsPerCompStep);

    //! Find an object by nameю
    Type *Find(const FString &strName);
    
    //! Add a new objectю
    void Add(Type *ptNew);
    
    //! Remove an objectю
    void Remove(Type *ptOld);

    //! Remove all objects but keep slotsю
    void Reset(void);

    //! Get estimated efficiency of the nametableю
    FString GetEfficiency(void);
};

#endif  /* include-once check. */