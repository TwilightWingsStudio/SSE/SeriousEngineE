/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_HASHTABLE_H
#define SE_INCL_HASHTABLE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticArray.h>

/*
 * Add comment here
 */
template<class Type>
class THashTableSlot
{
  public:
    ULONG hts_ulKey;      // hashing key
    Type *hts_ptElement;  // the element inhere

    THashTableSlot(void) 
    {
      hts_ptElement = NULL;
    };
    void Clear(void)
    {
      hts_ptElement = NULL;
    };
};

/*
 * Template class for storing pointers to objects for fast access by name.
 */
template<class Type, class ValueType>
class THashTable
{
  // implementation:
  public:
    typedef THashTableSlot<ValueType> TSlotType; // vc60 fallback

    INDEX ht_ctCompartments;    // number of compartments in table
    INDEX ht_ctSlotsPerComp;    // number of slots in one compartment
    INDEX ht_ctSlotsPerCompStep;    // allocation step for number of slots in one compartment
    TStaticArray<TSlotType> ht_ahtsSlots;  // all slots are here

    ULONG (*ht_GetItemKey)(ValueType &Value);
    ValueType (*ht_GetItemValue)(Type* Item);

    //! Internal finding, returns pointer to the the slot 
    THashTableSlot<ValueType> *FindSlot(ULONG ulKey, ValueType &Value);
    
    //! Internal finding, returns the index of the item in the nametable
    INDEX FindSlotIndex(ULONG ulKey, ValueType &Value);
    
    //! Get the item stored in the hashtable by it's index
    Type* GetItemFromIndex(INDEX iIndex);
    
    //! Get the value of the item stored in the hashtable by it's index
    ValueType GetValueFromIndex(INDEX iIndex);
    
    //! Expand the hash table to next step
    void Expand(void);

  // interface:
  public:
    //! Default constructor
    THashTable(void);
    
    //! Destructor -- frees all memory
    ~THashTable(void);
    
    //! Remove all slots, and reset the nametable to initial (empty) state
    void Clear(void);

    //! Set allocation parameters.
    void SetAllocationParameters(INDEX ctCompartments, INDEX ctSlotsPerComp, INDEX ctSlotsPerCompStep);
    
    //! Set callbacks.
    void SetCallbacks(ULONG (*GetItemKey)(ValueType &Item), ValueType (*GetItemValue)(Type* Item));

    //! Find an object by value, return a pointer to it.
    Type* Find(ValueType &Value);
    
    //! Find an object by value, return it's index.
    INDEX FindIndex(ValueType &Value);
    
    //! Add a new object.
    void Add(Type *ptNew);
    
    //! Remove an object.
    void Remove(Type *ptOld);
    
    //! Remove an object.
    void RemoveAll();

    //! Remove all objects but keep slots.
    void Reset(void);

    //! Get estimated efficiency of the hashtable.
    void ReportEfficiency(void);
};

#endif  /* include-once check. */