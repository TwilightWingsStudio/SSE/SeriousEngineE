/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Core.h>
#include <Core/Templates/BSP.h>
#include <Core/Templates/BSP_internal.h>

#include <Core/IO/Stream.h>
#include <Core/Base/CRC.h>
#include <Core/Math/Vector.h>
#include <Core/Math/Plane.h>
#include <Core/Math/OBBox.h>
#include <Core/Math/Functions.h>

#include <Core/Templates/StaticStackArray.cpp>
#include <Core/Templates/DynamicArray.cpp>


// epsilon value used for BSP cutting
//#define BSP_EPSILON ((Type) 0.015625)       // 1/2^6 ~= 1.5 cm
#define BSP_EPSILON Type((1.0/65536.0)*4*mth_fCSGEpsilon) // 1/2^16
//#define BSP_EPSILON Type(0.00390625) // 1/2^8
//#define EPSILON (1.0f/8388608.0f) // 1/2^23
//#define EPSILON 0.0009765625f // 1/2^10
//#define EPSILON 0.03125f    // 1/2^5
//#define EPSILON 0.00390625f // 1/2^8

template <class Type>
inline BOOL EpsilonEq(const Type &a, const Type &b)
{
  return Abs(a - b) <= BSP_EPSILON;
};

template <class Type>
inline BOOL EpsilonNe(const Type &a, const Type &b)
{
  return Abs(a - b) > BSP_EPSILON;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP vertex

// Assignment operator with coordinates only.
template<class Type, int iDimensions>
TBspVertex<Type, iDimensions> &TBspVertex<Type, iDimensions>::operator=(const TVector<Type, iDimensions> &vCoordinates)
{
  *(TVector<Type, iDimensions> *)this = vCoordinates;
  return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP vertex container

// Default constructor.
template<class Type, int iDimensions>
TBspVertexContainer<Type, iDimensions>::TBspVertexContainer(void) {}

template<class Type, int iDimensions>
void TBspVertexContainer<Type, iDimensions>::AddVertex(const TVector<Type, iDimensions> &vPoint)
{
  bvc_aVertices.Push() = vPoint;
}

// Initialize for a direction.
template<class Type, int iDimensions>
void TBspVertexContainer<Type, iDimensions>::Initialize(const TVector<Type, iDimensions> &vDirection)
{
  bvc_vDirection = vDirection;

  // Init array of vertices
  bvc_aVertices.SetAllocationStep(32);

  // Find largest axis of direction vector
  INDEX iMaxAxis = 0;
  Type tMaxAxis = (Type)0;//vDirection(1);
  for (INDEX iAxis = 1; iAxis <= iDimensions; iAxis++)
  {
    if (Abs(vDirection(iAxis)) > Abs(tMaxAxis) ) {
      tMaxAxis = vDirection(iAxis);
      iMaxAxis = iAxis;
    }
  }

  // This assert would seem natural here, but it is not possible because of parallel planes!
  // must be greater or equal than minimal max axis of any normalized vector in that space
  //ASSERT(Abs(tMaxAxis) > (1.0/sqrt(double(iDimensions))-0.01) );

  // Remember that axis index and sign for sorting
  bvc_iMaxAxis = iMaxAxis;
  bvc_tMaxAxisSign = Sgn(tMaxAxis);
}

// Unnitialize.
template<class Type, int iDimensions>
void TBspVertexContainer<Type, iDimensions>::Uninitialize(void)
{
  // Delete array of vertices
  bvc_aVertices.Delete();
  
  // Destroy axis index and sign
  bvc_iMaxAxis = -1;
  bvc_tMaxAxisSign = (Type)0;
}

static INDEX qsort_iCompareAxis;

//! Compare vertices of a certain type.
template<class Type, int iDimensions>
class CVertexComparator
{
  public:
    // Compare two vertices.
    static inline int CompareVertices(const TVector<Type, iDimensions> &vx0, const TVector<Type, iDimensions> &vx1, INDEX iAxis)
    {
      if (vx0(iAxis) < vx1(iAxis)) {
        return -1;
      } else if (vx0(iAxis) > vx1(iAxis)) {
        return 1;
      } else {
        return 0;
      }
    }

    // Compare two vertices for quick-sort.
    static int qsort_CompareVertices_plus(const void *pvVertex0, const void *pvVertex1)
    {
      TBspVertex<Type, iDimensions> &vx0 = *(TBspVertex<Type, iDimensions> *)pvVertex0;
      TBspVertex<Type, iDimensions> &vx1 = *(TBspVertex<Type, iDimensions> *)pvVertex1;
      return +CompareVertices(vx0, vx1, qsort_iCompareAxis);
    }
    
    static int qsort_CompareVertices_minus(const void *pvVertex0, const void *pvVertex1)
    {
      TBspVertex<Type, iDimensions> &vx0 = *(TBspVertex<Type, iDimensions> *)pvVertex0;
      TBspVertex<Type, iDimensions> &vx1 = *(TBspVertex<Type, iDimensions> *)pvVertex1;
      return -CompareVertices(vx0, vx1, qsort_iCompareAxis);
    }
};

// Sort vertices in this container along the largest axis of container direction.
template<class Type, int iDimensions>
void TBspVertexContainer<Type, iDimensions>::Sort(void)
{
  // If there are no vertices, or the container is not line
  if (bvc_aVertices.Count() == 0 || IsPlannar()) {
    return; // do not attempt to sort
  }

  // Sort by max. axis
  qsort_iCompareAxis = bvc_iMaxAxis;

  // If the sign of axis is positive
  if (bvc_tMaxAxisSign > 0)
  {
    // sort them normally
    if (bvc_aVertices.Count() > 0) {
      qsort(&bvc_aVertices[0], bvc_aVertices.Count(), sizeof(TBspVertex<Type, iDimensions>),
            CVertexComparator<Type, iDimensions>::qsort_CompareVertices_plus);
    }
    
  // If it is negative
  } else {
    // Sort them inversely
    if (bvc_aVertices.Count() > 0) {
      qsort(&bvc_aVertices[0], bvc_aVertices.Count(), sizeof(TBspVertex<Type, iDimensions>),
            CVertexComparator<Type, iDimensions>::qsort_CompareVertices_minus);
    }
  }
}

// Elliminate paired vertices.
template<class Type, int iDimensions>
void TBspVertexContainer<Type, iDimensions>::ElliminatePairedVertices(void)
{
  // If there are no vertices, or the container is not line
  if (bvc_aVertices.Count() == 0 || IsPlannar()) {
    // do not attempt to sort
    return;
  }

  // Initially, last vertices are far away
  Type tLastInside;  tLastInside  = (Type)32000;
  TBspVertex<Type, iDimensions> *pbvxLastInside  = NULL;

  // For all vertices in container
  for (INDEX iVertex = 0; iVertex < bvc_aVertices.Count(); iVertex++)
  {
    TBspVertex<Type, iDimensions> &bvx = bvc_aVertices[iVertex];    // reference to this vertex
    Type t = bvx(bvc_iMaxAxis);                 // coordinate along max. axis

    // If last inside vertex is next to this one
    if (EpsilonEq(t, tLastInside)) {
      // Last vertex is far away
      tLastInside  = (Type)32000;
      IFDEBUG(pbvxLastInside = NULL);

    // Otherwise
    } else {
      // Make this last inside vertex
      tLastInside = t;
      pbvxLastInside = &bvx;
    }
  }
}

// Create edges from vertices in one container -- must be sorted before.
template<class Type, int iDimensions>
void TBspVertexContainer<Type, iDimensions>::CreateEdges(TDynamicArray<TBspEdge<Type, iDimensions> > &abed, size_t ulEdgeTag)
{
  // If there are no vertices, or the container is not line
  if (bvc_aVertices.Count() == 0 || IsPlannar()) {
    return; // do not attempt to sort
  }

  // Initially, edge is inactive
  BOOL bActive = FALSE;
  TBspEdge<Type, iDimensions> *pbed = NULL;

  // For all vertices in container
  for (INDEX iVertex = 0; iVertex < bvc_aVertices.Count(); iVertex++)
  {
    TBspVertex<Type, iDimensions> &bvx = bvc_aVertices[iVertex];    // reference to this vertex

    // If edge is inactive
    if (!bActive) {
      // Create new edge
      pbed = abed.New();
      pbed->bed_ulEdgeTag = ulEdgeTag;
      
      // Set start vertex
      pbed->bed_vVertex0 = bvx;
    } else {
      // Set end vertex
      pbed->bed_vVertex1 = bvx;
      
      // Trash edge pointer
      IFDEBUG(pbed = NULL);
    }
    
    // Toggle edge
    bActive = !bActive;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP edge

// Constructor with two vectors.
template<class Type, int iDimensions>
TBspEdge<Type, iDimensions>::TBspEdge(const TVector<Type, iDimensions> &vVertex0, const TVector<Type, iDimensions> &vVertex1, size_t ulTag)
  : bed_vVertex0(vVertex0)
  , bed_vVertex1(vVertex1)
  , bed_ulEdgeTag(ulTag)
{}

// Remove all edges marked for removal
template<class Type, int iDimensions>
void TBspEdge<Type, iDimensions>::RemoveMarkedBSPEdges(TDynamicArray<TBspEdge<Type, iDimensions> > &abed)
{
  typedef TBspEdge<Type, iDimensions> edge_t; // local declaration, to fix macro expansion in FOREACHINDYNAMICARRAY
  
  // Conut edges left
  INDEX ctEdgesLeft = 0;
  {
    FOREACHINDYNAMICARRAY(abed, edge_t, itbed)
    {
      if (itbed->bed_ulEdgeTag != 0) {
        ctEdgesLeft++;
      }
    }
  }
  
  // Make a copy of array without removed edges
  TDynamicArray<TBspEdge<Type, iDimensions> > abed2;
  abed2.New(ctEdgesLeft);
  abed2.Lock();
  INDEX iedNew = 0;
  {
    FOREACHINDYNAMICARRAY(abed, edge_t, itbed)
    {
      edge_t &bed = *itbed;
      if (bed.bed_ulEdgeTag != 0) {
        abed2[iedNew] = bed;
        iedNew++;
      }
    }
  }
  abed2.Unlock();
  
  // Use that copy instead the original array
  abed.Clear();
  abed.MoveArray(abed2);
}

// Optimize a polygon made out of BSP edges using tag information
template<class Type, int iDimensions>
void TBspEdge<Type, iDimensions>::OptimizeBSPEdges(TDynamicArray<TBspEdge<Type, iDimensions> > &abed)
{
  typedef TBspEdge<Type, iDimensions> edge_t; // local declaration, to fix macro expansion in FOREACHINDYNAMICARRAY

  // If there are no edges
  if (abed.Count() == 0) {
    // do nothing
    return;
  }
  BOOL bSomeJoined;
  
  // Repeat
  do {
    bSomeJoined = FALSE;
    
    // For each edge
    {
      FOREACHINDYNAMICARRAY(abed, edge_t, itbed1)
      {
        edge_t &bed1 = *itbed1;
        
        // If it is already marked
        if (bed1.bed_ulEdgeTag == 0) {
          continue; // skip it
        }
        
        // If it is dummy edge
        if (bed1.bed_vVertex0 == bed1.bed_vVertex1) {
          // Mark it for removal
          bSomeJoined = TRUE;
          bed1.bed_ulEdgeTag = 0;
          
          // Skip it
          continue;
        }

        // For each other edge
        {
          FOREACHINDYNAMICARRAY(abed, edge_t, itbed2)
          {
            edge_t &bed2 = *itbed2;
            if (&bed1 == &bed2) {
              continue;
            }
            // If it is already marked
            if (bed2.bed_ulEdgeTag == 0) {
              continue; // skip it
            }
            
            // If they originate from same edge (plane)
            if (bed1.bed_ulEdgeTag == bed2.bed_ulEdgeTag)
            {
              // If they are complemented
              if (bed1.bed_vVertex0 == bed2.bed_vVertex1 && bed1.bed_vVertex1 == bed2.bed_vVertex0) {
                // Marked them both
                bSomeJoined = TRUE;
                bed1.bed_ulEdgeTag = 0;
                bed2.bed_ulEdgeTag = 0;
                
                // Skip them both
                break;
              }
              
              // If second one continues after first one
              if (bed1.bed_vVertex1 == bed2.bed_vVertex0) {
                // Extend end of first edge to the end of second one
                bed1.bed_vVertex1=bed2.bed_vVertex1;
                bSomeJoined = TRUE;
                
                // Marked second edge
                bed2.bed_ulEdgeTag = 0;
                
              // If second one continues before first one
              } else if (bed1.bed_vVertex0 == bed2.bed_vVertex1) {
                // Extend start of first edge to the start of second one
                bed1.bed_vVertex0 = bed2.bed_vVertex0;
                bSomeJoined = TRUE;
                
                // Marked second edge
                bed2.bed_ulEdgeTag = 0;
              }
            }
          }
        }
      }
    }
    
  // While some edges can be joined
  } while (bSomeJoined);

  // remove all marked edges
  RemoveMarkedBSPEdges(abed);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP polygon

// Add an edge to the polygon.
template<class Type, int iDimensions>
inline void TBspPolygon<Type, iDimensions>::AddEdge(const TVector<Type, iDimensions> &vPoint0, const TVector<Type, iDimensions> &vPoint1, size_t ulTag)
{
  *bpo_abedPolygonEdges.New() = TBspEdge<Type, iDimensions>(vPoint0, vPoint1, ulTag);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP node

// Recursive destructor.
template<class Type, int iDimensions>
void TBspNode<Type, iDimensions>::DeleteBSPNodeRecursively(void)
{
  // Delete sub-trees first, before deleting this node
  if (bn_pbnFront != NULL) {
    bn_pbnFront->DeleteBSPNodeRecursively();
  }
  
  if (bn_pbnBack != NULL) {
    bn_pbnBack->DeleteBSPNodeRecursively();
  }
  delete this;
}

// Constructor for a leaf node.
template<class Type, int iDimensions>
TBspNode<Type, iDimensions>::TBspNode(enum BSPNodeLocation bnl)
  : bn_bnlLocation(bnl)
  , bn_pbnFront(NULL)
  , bn_pbnBack(NULL)
{
  ASSERT(bnl == BNL_INSIDE || bnl == BNL_OUTSIDE);
}

// Constructor for a branch node.
template<class Type, int iDimensions>
TBspNode<Type, iDimensions>::TBspNode(const TPlane<Type, iDimensions> &plSplitPlane, size_t ulPlaneTag,
                 TBspNode<Type, iDimensions> &bnFront, TBspNode<Type, iDimensions> &bnBack)
  : TPlane<Type, iDimensions>(plSplitPlane)
  , bn_pbnFront(&bnFront)
  , bn_pbnBack(&bnBack)
  , bn_bnlLocation(BNL_BRANCH)
  , bn_ulPlaneTag(ulPlaneTag)
{
}

// Constructor for cloning a bsp (sub)tree.
template<class Type, int iDimensions>
TBspNode<Type, iDimensions>::TBspNode(TBspNode<Type, iDimensions> &bnRoot)
  : TPlane<Type, iDimensions>(bnRoot)          // copy the plane
  , bn_bnlLocation(bnRoot.bn_bnlLocation)     // copy the location
  , bn_ulPlaneTag(bnRoot.bn_ulPlaneTag)       // copy the plane tag
{
  // If this has a front child
  if (bnRoot.bn_pbnFront != NULL) {
    // Clone front sub tree
    bn_pbnFront = new TBspNode<Type, iDimensions>(*bnRoot.bn_pbnFront);
  } else {
    // No front sub tree
    bn_pbnFront = NULL;
  }

  // If this has a back child
  if (bnRoot.bn_pbnBack != NULL) {
    // Clone back sub tree
    bn_pbnBack  = new TBspNode<Type, iDimensions>(*bnRoot.bn_pbnBack);
  } else {
    // No back sub tree
    bn_pbnBack  = NULL;
  }
}

// Test if a sphere is inside, outside, or intersecting. (Just a trivial rejection test)
template<class Type, int iDimensions>
FLOAT TBspNode<Type, iDimensions>::TestSphere(const TVector<Type, iDimensions> &vSphereCenter, Type tSphereRadius) const
{
  // If this is an inside node
  if (bn_bnlLocation == BNL_INSIDE) {
    return 1;   // it is inside
    
  // If this is an outside node
  } else if (bn_bnlLocation == BNL_OUTSIDE) {
    return -1;  // it is outside
    
  // If this is a branch
  } else {
    ASSERT(bn_bnlLocation == BNL_BRANCH);
    
    // Test the sphere against the split plane
    Type tCenterDistance = PointDistance(vSphereCenter);
    
    // If the sphere is in front of the plane
    if (tCenterDistance > +tSphereRadius) {
      // Recurse down the front node
      return bn_pbnFront->TestSphere(vSphereCenter, tSphereRadius);
      
    // If the sphere is behind the plane
    } else if (tCenterDistance < -tSphereRadius) {
      // Recurse down the back node
      return bn_pbnBack->TestSphere(vSphereCenter, tSphereRadius);
      
    // If the sphere is split by the plane
    } else {
      // If front node touches
      FLOAT fFront = bn_pbnFront->TestSphere(vSphereCenter, tSphereRadius);
      if (fFront == 0) {
        return 0; // it touches
      }
      
      // If back node touches
      FLOAT fBack = bn_pbnBack->TestSphere(vSphereCenter, tSphereRadius);
      if (fBack == 0) {
        return 0; // it touches
      }
      
      // If front and back have same classification
      if (fFront == fBack) {
        return fFront;  // return it
        
      // If front and back have different classification
      } else {
        return 0; // it touches
      }
    }
  }
}

// Test if a box is inside, outside, or intersecting. (Just a trivial rejection test)
template<class Type, int iDimensions>
FLOAT TBspNode<Type, iDimensions>::TestBox(const TOBBox<Type> &box) const
{
  // If this is an inside node
  if (bn_bnlLocation == BNL_INSIDE) {
    
    return 1;   // it is inside

  // If this is an outside node
  } else if (bn_bnlLocation == BNL_OUTSIDE) {
    return -1;  // it is outside
    
  // If this is a branch
  } else {
    ASSERT(bn_bnlLocation == BNL_BRANCH);
    
    // Test the box against the split plane
    Type tTest = box.TestAgainstPlane(*this);
    
    // If the sphere is in front of the plane
    if (tTest > 0) {
      // Recurse down the front node
      return bn_pbnFront->TestBox(box);
      
    // If the sphere is behind the plane
    } else if (tTest < 0) {
      // Recurse down the back node
      return bn_pbnBack->TestBox(box);
      
    // If the sphere is split by the plane
    } else {
      // If front node touches
      FLOAT fFront = bn_pbnFront->TestBox(box);
      if (fFront == 0) {
        return 0; // it touches
      }
      
      // If back node touches
      FLOAT fBack = bn_pbnBack->TestBox(box);
      if (fBack == 0) {
        return 0; // it touches
      }
      // if front and back have same classification
      if (fFront == fBack) {
        return fFront;  // return it
        
      // If front and back have different classification
      } else {
        return 0; // it touches
      }
    }
  }
}

// Find minimum/maximum parameters of points on a line that are inside - recursive
template<class Type, int iDimensions>
void TBspNode<Type, iDimensions>::FindLineMinMax(TBspLine<Type, iDimensions> &bl, const TVector<Type, iDimensions> &v0,
                                                const TVector<Type, iDimensions> &v1, Type t0, Type t1)
{
  // If this is an inside node
  if (bn_bnlLocation == BNL_INSIDE)
  {
    // Just update min/max
    bl.bl_tMin = Min(bl.bl_tMin, t0);
    bl.bl_tMax = Max(bl.bl_tMax, t1);
    return;
    
  // If this is an outside node
  } else if (bn_bnlLocation == BNL_OUTSIDE) {
    return; // do nothing
    
  // If this is a branch
  } else {
    ASSERT(bn_bnlLocation == BNL_BRANCH);
    
    // Test the points against the split plane
    Type tD0 = PointDistance(v0);
    Type tD1 = PointDistance(v1);
    
    // If both are front
    if (tD0 >= 0 && tD1 >= 0) {
      // Recurse down the front node
      bn_pbnFront->FindLineMinMax(bl, v0, v1, t0, t1);
      return;
      
    // If both are back
    } else if (tD0 < 0 && tD1 < 0) {
      // Recurse down the back node
      bn_pbnBack->FindLineMinMax(bl, v0, v1, t0, t1);
      return;
      
    // If on different sides
    } else {
      // Find split point
      Type tFraction = tD0 / (tD0 - tD1);
      TVector<Type, iDimensions> vS = v0 + (v1 - v0) * tFraction;
      Type tS = t0 + (t1 - t0) * tFraction;
      
      // If first is front
      if (tD0 >= 0) {
        // Recurse first part down the front node
        bn_pbnFront->FindLineMinMax(bl, v0, vS, t0, tS);
        
        // Recurse second part down the back node
        bn_pbnBack->FindLineMinMax(bl, vS, v1, tS, t1);
        return;
        
      // If first is back
      } else {
        // Recurse first part down the back node
        bn_pbnBack->FindLineMinMax(bl, v0, vS, t0, tS);
        
        // Recurse second part down the front node
        bn_pbnFront->FindLineMinMax(bl, vS, v1, tS, t1);
        return;
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP cutter

// Constructor for splitting a polygon with a BSP tree.
template<class Type, int iDimensions>
TBspCutter<Type, iDimensions>::TBspCutter(TBspPolygon<Type, iDimensions> &bpoPolygon, TBspNode<Type, iDimensions> &bnRoot)
{
  // Cut the polygon with entire tree
  CutPolygon(bpoPolygon, bnRoot);
}

// Destructor.
template<class Type, int iDimensions>
TBspCutter<Type, iDimensions>::~TBspCutter(void) {}

// Cut a polygon with a BSP tree.
template<class Type, int iDimensions>
void TBspCutter<Type, iDimensions>::CutPolygon(TBspPolygon<Type, iDimensions> &bpoPolygon, TBspNode<Type, iDimensions> &bn)
{
  // If the polygon has no edges
  if (bpoPolygon.bpo_abedPolygonEdges.Count() == 0) {
    return; // skip cutting
  }

  // If this node is inside node
  if (bn.bn_bnlLocation == BNL_INSIDE) {
    // Add entire polygon to inside part
    bc_abedInside.MoveArray(bpoPolygon.bpo_abedPolygonEdges);

  // If this node is outside node
  } else if (bn.bn_bnlLocation == BNL_OUTSIDE) {
    // Add entire polygon to outside part
    bc_abedOutside.MoveArray(bpoPolygon.bpo_abedPolygonEdges);

  // If this node is a branch
  } else if (bn.bn_bnlLocation == BNL_BRANCH) {
    TBspPolygon<Type, iDimensions> bpoFront;   // part of polygon in front of this splitter
    TBspPolygon<Type, iDimensions> bpoBack;    // part of polygon behind this splitter

    // Split the polygon with split plane of this node
    BOOL bOnPlane = SplitPolygon(bpoPolygon, (TPlane<Type, iDimensions> &)bn, bn.bn_ulPlaneTag, bpoFront, bpoBack);

    // If the polygon is not on the split plane
    if (!bOnPlane) {
      // Recursively split front part with front part of bsp
      CutPolygon(bpoFront, *bn.bn_pbnFront);
      
      // Recursively split back part with back part of bsp
      CutPolygon(bpoBack, *bn.bn_pbnBack);

    // If the polygon is on the split plane
    } else {
      TBspNode<Type, iDimensions> *pbnFront;  // front node (relative to the polygon orientation)
      TBspNode<Type, iDimensions> *pbnBack;   // back node (relative to the polygon orientation)

      // Check the direction of the polygon with the front direction of the split plane
      Type tDirection = (TVector<Type, iDimensions> &)bpoPolygon%(TVector<Type, iDimensions> &)bn;
      
      // If the directions are same
      if (tDirection > +BSP_EPSILON) {
        // Make nodes relative to polygon same as relative to the split plane
        pbnFront = bn.bn_pbnFront;
        pbnBack  = bn.bn_pbnBack;

      // If the directions are opposite
      } else if (tDirection < -BSP_EPSILON) {
        // Make nodes relative to polygon opposite as relative to the split plane
        pbnFront = bn.bn_pbnBack;
        pbnBack  = bn.bn_pbnFront;
        
      // If the directions are indeterminate
      } else {
        // That must not be
        ASSERT(FALSE);
      }

      // Cut it with front part of bsp
      TBspCutter<Type, iDimensions> bcFront(bpoPolygon, *pbnFront);
      
      // There must be no on-border parts
      ASSERT(bcFront.bc_abedBorderInside.Count() == 0 && bcFront.bc_abedBorderOutside.Count() == 0);

      // Make a polygon from parts that are inside in front part of BSP
      TBspPolygon<Type, iDimensions> bpoInsideFront((TPlane<Type, iDimensions> &)bpoPolygon, bcFront.bc_abedInside, bpoPolygon.bpo_ulPlaneTag);
      
      // Cut them with back part of bsp
      TBspCutter<Type, iDimensions> bcBackInsideFront(bpoInsideFront, *pbnBack);

      // Make a polygon from parts that are outside in front part of BSP
      TBspPolygon<Type, iDimensions> bpoOutsideFront((TPlane<Type, iDimensions> &)bpoPolygon, bcFront.bc_abedOutside, bpoPolygon.bpo_ulPlaneTag);
      
      // Cut them with back part of bsp
      TBspCutter<Type, iDimensions> bcBackOutsideFront(bpoOutsideFront, *pbnBack);

      // Add parts that are inside both in front and back to inside part
      bc_abedInside.MoveArray(bcBackInsideFront.bc_abedInside);
      
      // Add parts that are outside both in front and back to outside part
      bc_abedOutside.MoveArray(bcBackOutsideFront.bc_abedOutside);

      // Add parts that are inside in front and outside back to on-border-inside-part
      bc_abedBorderInside.MoveArray(bcBackInsideFront.bc_abedOutside);
      
      // Add parts that are outside in front and inside back to on-border-outside-part
      bc_abedBorderOutside.MoveArray(bcBackOutsideFront.bc_abedInside);
    }
  } else {
    ASSERTALWAYS("Bad node type");
  }
}

// Split a polygon with a plane. -- returns FALSE if polygon is laying on the plane
template<class Type, int iDimensions>
BOOL TBspCutter<Type, iDimensions>::SplitPolygon(TBspPolygon<Type, iDimensions> &bpoPolygon,
                                                const TPlane<Type, iDimensions> &plSplitPlane,
                                                size_t ulPlaneTag,
                                                TBspPolygon<Type, iDimensions> &bpoFront,
                                                TBspPolygon<Type, iDimensions> &bpoBack)
{
  (TPlane<Type, iDimensions> &)bpoFront = (TPlane<Type, iDimensions> &)bpoPolygon;
  bpoFront.bpo_ulPlaneTag = bpoPolygon.bpo_ulPlaneTag;
  (TPlane<Type, iDimensions> &)bpoBack = (TPlane<Type, iDimensions> &)bpoPolygon;
  bpoBack.bpo_ulPlaneTag = bpoPolygon.bpo_ulPlaneTag;

  // Calculate the direction of split line
  TVector<Type, iDimensions> vSplitDirection = ((TVector<Type, iDimensions> &)plSplitPlane) * (TVector<Type, iDimensions> &)bpoPolygon;

  // If the polygon is parallel with the split plane
  if (vSplitDirection.Length() < +BSP_EPSILON)
  {
    // Calculate the distance of the polygon from the split plane
    Type fDistance = plSplitPlane.PlaneDistance(bpoPolygon);

    // If the polygon is in front of plane
    if (fDistance > +BSP_EPSILON) {
      // Move all edges to front array
      bpoFront.bpo_abedPolygonEdges.MoveArray(bpoPolygon.bpo_abedPolygonEdges);
      
      // The polygon is not on the plane
      return FALSE;

    // If the polygon is behind the plane
    } else if (fDistance < -BSP_EPSILON) {
      // Move all edges to back array
      bpoBack.bpo_abedPolygonEdges.MoveArray(bpoPolygon.bpo_abedPolygonEdges);
      
      // The polygon is not on the plane
      return FALSE;

    // If the polygon is on the plane
    } else {
      // Just return so
      return TRUE;
    }

  // If the polygon is not parallel with the split plane
  } else {
    // Initialize front and back vertex containers
    TBspVertexContainer<Type, iDimensions> bvcFront, bvcBack;
    bvcFront.Initialize(vSplitDirection);
    bvcBack.Initialize(-vSplitDirection);

    typedef TBspEdge<Type, iDimensions> edge_t; // local declaration, to fix macro expansion in FOREACHINDYNAMICARRAY
    // For each edge in polygon
    {
      FOREACHINDYNAMICARRAY(bpoPolygon.bpo_abedPolygonEdges, edge_t, itbed)
      {
        // Split the edge
        SplitEdge(itbed->bed_vVertex0, itbed->bed_vVertex1, itbed->bed_ulEdgeTag, plSplitPlane, bpoFront, bpoBack, bvcFront, bvcBack);
      }
    }

    // Sort vertex containers
    bvcFront.Sort();
    bvcBack.Sort();
    
    // Elliminate paired vertices
    bvcFront.ElliminatePairedVertices();
    bvcBack.ElliminatePairedVertices();
    
    // Create more front polygon edges from front vertex container
    bvcFront.CreateEdges(bpoFront.bpo_abedPolygonEdges, ulPlaneTag);
    
    // Create more back polygon edges from back vertex container
    bvcBack.CreateEdges(bpoBack.bpo_abedPolygonEdges, ulPlaneTag);

    // The polygon is not on the plane
    return FALSE;
  }
}

// Split an edge with a plane.
template<class Type, int iDimensions>
void TBspCutter<Type, iDimensions>::SplitEdge(const TVector<Type, iDimensions> &vPoint0,
                                             const TVector<Type, iDimensions> &vPoint1,
                                             size_t ulEdgeTag,
                                             const TPlane<Type, iDimensions> &plSplitPlane,
                                             TBspPolygon<Type, iDimensions> &bpoFront,
                                             TBspPolygon<Type, iDimensions> &bpoBack,
                                             TBspVertexContainer<Type, iDimensions> &bvcFront,
                                             TBspVertexContainer<Type, iDimensions> &bvcBack)
{
  // Calculate point distances from clip plane
  Type tDistance0 = plSplitPlane.PointDistance(vPoint0);
  Type tDistance1 = plSplitPlane.PointDistance(vPoint1);

  /* ---- first point behind plane ---- */
  if (tDistance0 < -BSP_EPSILON)
  {
    // If both are back
    if (tDistance1 < -BSP_EPSILON) {
      // Add the whole edge to back node
      bpoBack.AddEdge(vPoint0, vPoint1, ulEdgeTag);
      // No split points

    // If first is back, second front
    } else if (tDistance1 > +BSP_EPSILON) {
      // Calculate intersection coordinates
      TVector<Type, iDimensions> vPointMid = vPoint0 - (vPoint0 - vPoint1) * tDistance0 / (tDistance0 - tDistance1);
      
      // Add front part to front node
      bpoFront.AddEdge(vPointMid, vPoint1, ulEdgeTag);
      
      // Add back part to back node
      bpoBack.AddEdge(vPoint0, vPointMid, ulEdgeTag);
      
      // Add split point to front _and_ back part of splitter
      bvcFront.AddVertex(vPointMid);
      bvcBack.AddVertex(vPointMid);

    // If first is back, second on the plane
    } else {
      // Add the whole edge to back node
      bpoBack.AddEdge(vPoint0, vPoint1, ulEdgeTag);
      
      // Add second point to back part of splitter
      bvcBack.AddVertex(vPoint1);
    }

  /* ---- first point in front of plane ---- */
  } else if (tDistance0 > +BSP_EPSILON) {
    // If first is front, second back
    if (tDistance1 < -BSP_EPSILON) 
    {
      // Calculate intersection coordinates
      TVector<Type, iDimensions> vPointMid = vPoint1 - (vPoint1 - vPoint0) * tDistance1 / (tDistance1 - tDistance0);
      
      // Add front part to front node
      bpoFront.AddEdge(vPoint0, vPointMid, ulEdgeTag);
      
      // Add back part to back node
      bpoBack.AddEdge(vPointMid, vPoint1, ulEdgeTag);
      
      // Add split point to front _and_ back part of splitter
      bvcFront.AddVertex(vPointMid);
      bvcBack.AddVertex(vPointMid);


    // if both are front
    } else if (tDistance1 > +BSP_EPSILON) {
      // Add the whole edge to front node
      bpoFront.AddEdge(vPoint0, vPoint1, ulEdgeTag);
      // No split points

    // If first is front, second on the plane
    } else {
      // Add the whole edge to front node
      bpoFront.AddEdge(vPoint0, vPoint1, ulEdgeTag);
      
      // Add second point to front part of splitter
      bvcFront.AddVertex(vPoint1);
    }

  /* ---- first point on the plane ---- */
  } else {
    // If first is on the plane, second back
    if (tDistance1 < -BSP_EPSILON) {
      // Add the whole edge to back node
      bpoBack.AddEdge(vPoint0, vPoint1, ulEdgeTag);
      
      // Add first point to back part of splitter
      bvcBack.AddVertex(vPoint0);

    // If first is on the plane, second in front of the plane
    } else if (tDistance1 > +BSP_EPSILON) {
      // Add the whole edge to front node
      bpoFront.AddEdge(vPoint0, vPoint1, ulEdgeTag);
      
      // Add first point to front part of splitter
      bvcFront.AddVertex(vPoint0);

    // If both are on the plane
    } else {
      // Check the direction of the edge with the front direction of the splitter
      Type tDirection = (vPoint1 - vPoint0)%bvcFront.bvc_vDirection;
      
      // If the directions are same
      if (tDirection > +BSP_EPSILON) {
        // Add the whole edge to front node
        bpoFront.AddEdge(vPoint0, vPoint1, ulEdgeTag);
        
        // Add both points to front part of the splitter
        bvcFront.AddVertex(vPoint0);
        bvcFront.AddVertex(vPoint1);

      // If the directions are opposite
      } else if (tDirection < -BSP_EPSILON) {
        // Add the whole edge to back node
        bpoBack.AddEdge(vPoint0, vPoint1, ulEdgeTag);
        
        // Add both points to back part of the splitter
        bvcBack.AddVertex(vPoint0);
        bvcBack.AddVertex(vPoint1);

      // If the directions are indeterminate
      } else {
        // That must mean that there is no edge in fact
        //ASSERT(Type(vPoint1-vPoint0) < 2*BSP_EPSILON); //!!!!
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSP tree

// Default constructor.
template<class Type, int iDimensions>
TBspTree<Type, iDimensions>::TBspTree(void)
{
  bt_pbnRoot = NULL;
}

// Destructor.
template<class Type, int iDimensions>
TBspTree<Type, iDimensions>::~TBspTree(void)
{
  Destroy();
}

// Constructor with array of polygons oriented inwards.
template<class Type, int iDimensions>
TBspTree<Type, iDimensions>::TBspTree(TDynamicArray<TBspPolygon<Type, iDimensions> > &abpoPolygons)
{
  bt_pbnRoot = NULL;
  Create(abpoPolygons);
}

// Create bsp-subtree from array of polygons oriented inwards.
template<class Type, int iDimensions>
TBspNode<Type, iDimensions> *TBspTree<Type, iDimensions>::CreateSubTree(TDynamicArray<TBspPolygon<Type, iDimensions> > &abpoPolygons)
{
  // Local declarations, to fix macro expansion in FOREACHINDYNAMICARRAY
  typedef TBspEdge<Type, iDimensions> edge_t;
  typedef TBspPolygon<Type, iDimensions> polygon_t;
  ASSERT(abpoPolygons.Count() >= 1);

  // Use first polygon as splitter
  abpoPolygons.Lock();
  TBspPolygon<Type, iDimensions> bpoSplitter = abpoPolygons[0];
  abpoPolygons.Unlock();
  
  // Tags must be valid
  ASSERT(bpoSplitter.bpo_ulPlaneTag != -1);

  // Create two new polygon arrays - back and front
  TDynamicArray<TBspPolygon<Type, iDimensions> > abpoFront, abpoBack;

  // For each polygon in this array
  {
    FOREACHINDYNAMICARRAY(abpoPolygons, polygon_t, itbpo)
    {
      TBspPolygon<Type, iDimensions> bpoFront, bpoBack;

      // Tags must be valid
      ASSERT(itbpo->bpo_ulPlaneTag != -1);
      
      // If the polygon has plane tag same as the tag of the splitter
      if (itbpo->bpo_ulPlaneTag == bpoSplitter.bpo_ulPlaneTag) {
        // they are assumed coplanar, so skip it
        continue;
      }

      // Split it by the plane of splitter polygon
      BOOL bOnPlane = TBspCutter<Type, iDimensions>::SplitPolygon(itbpo.Current(), bpoSplitter,
                                                                 bpoSplitter.bpo_ulPlaneTag, bpoFront, bpoBack);
      // If the polygon is not coplanar with the splitter
      if (!bOnPlane)
      {
        // If there are some parts that are front
        if (bpoFront.bpo_abedPolygonEdges.Count() > 0) {
          // Create a polygon in front array and add all inside parts to it
          TBspPolygon<Type, iDimensions> *pbpo = abpoFront.New(1);
          pbpo->bpo_abedPolygonEdges.MoveArray(bpoFront.bpo_abedPolygonEdges);
          *(TPlane<Type, iDimensions> *)pbpo = itbpo.Current();
          pbpo->bpo_ulPlaneTag = itbpo->bpo_ulPlaneTag;
        }
        
        // If there are some parts that are back
        if (bpoBack.bpo_abedPolygonEdges.Count() > 0) {
          // Create a polygon in back array and add all outside parts to it
          TBspPolygon<Type, iDimensions> *pbpo = abpoBack.New(1);
          pbpo->bpo_abedPolygonEdges.MoveArray(bpoBack.bpo_abedPolygonEdges);
          *(TPlane<Type, iDimensions> *)pbpo = itbpo.Current();
          pbpo->bpo_ulPlaneTag = itbpo->bpo_ulPlaneTag;
        }
      }
    }
  }

  // Free this array (to not consume too much memory)
  abpoPolygons.Clear();

  TBspNode<Type, iDimensions> *pbnFront, *pbnBack;
  // If there is some polygon in front array
  if (abpoFront.Count() > 0) {
    // Create front subtree using front array
    pbnFront = CreateSubTree(abpoFront);
  } else {
    // Make front node an inside leaf node
    pbnFront = new TBspNode<Type, iDimensions>(BNL_INSIDE);
  }

  // If there is some polygon in back array
  if (abpoBack.Count() > 0) {
    // Create back subtree using back array
    pbnBack = CreateSubTree(abpoBack);
  } else {
    // Make back node an outside leaf node
    pbnBack = new TBspNode<Type, iDimensions>(BNL_OUTSIDE);
  }

  // Make a splitter node with the front and back nodes
  return new TBspNode<Type, iDimensions>(bpoSplitter, bpoSplitter.bpo_ulPlaneTag, *pbnFront, *pbnBack);
}

// Create bsp-tree from array of polygons oriented inwards.
template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::Create(TDynamicArray<TBspPolygon<Type, iDimensions> > &abpoPolygons)
{
  typedef TBspPolygon<Type, iDimensions> polygon_t; // local declaration, to fix macro expansion in FOREACHINDYNAMICARRAY

  // Free eventual existing tree
  Destroy();

  // Create the tree using the recursive function
  bt_pbnRoot = CreateSubTree(abpoPolygons);
  
  // Move the tree to array
  MoveNodesToArray();
}

// Destroy bsp-tree.
template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::Destroy(void)
{
  // If tree is in array
  if (bt_abnNodes.Count() > 0) {
    // Clear array
    bt_abnNodes.Clear();
    bt_pbnRoot = NULL;
    
  // If there is some free tree
  } else if (bt_pbnRoot != NULL) {
    // Delete it
    bt_pbnRoot->DeleteBSPNodeRecursively();
    bt_pbnRoot = NULL;
  }
}

// Test if a sphere could touch any of inside nodes. (Just a trivial rejection test)
template<class Type, int iDimensions>
FLOAT TBspTree<Type, iDimensions>::TestSphere(const TVector<Type, iDimensions> &vSphereCenter, Type tSphereRadius) const
{
  if (bt_pbnRoot == NULL) {
    return FALSE;
  }
  
  // Just start recursive testing at root node
  return bt_pbnRoot->TestSphere(vSphereCenter, tSphereRadius);
}

// Test if a box is inside, outside, or intersecting. (Just a trivial rejection test)
template<class Type, int iDimensions>
FLOAT TBspTree<Type, iDimensions>::TestBox(const OBBox<Type> &box) const
{
  if (bt_pbnRoot == NULL) {
    return FALSE;
  }
  
  // Just start recursive testing at root node
  return bt_pbnRoot->TestBox(box);
}

// Find minimum/maximum parameters of points on a line that are inside
template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::FindLineMinMax(const TVector<Type, iDimensions> &v0,
                                                const TVector<Type, iDimensions> &v1,
                                                Type &tMin,
                                                Type &tMax) const
{
  // Init line
  TBspLine<Type, iDimensions> bl;
  bl.bl_tMin = UpperLimit(Type(0));
  bl.bl_tMax = LowerLimit(Type(0));

  // Recursively split it
  bt_pbnRoot->FindLineMinMax(bl, v0, v1, Type(0), Type(1));

  // Return the min/max
  tMin = bl.bl_tMin;
  tMax = bl.bl_tMax;
}

static INDEX _ctNextIndex;

// Move one subtree to array.
template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::MoveSubTreeToArray(TBspNode<Type, iDimensions> *pbnSubtree)
{
  // If this is no node
  if (pbnSubtree == NULL) {
    return; // do nothing
  }
  
  // First move all subnodes
  MoveSubTreeToArray(pbnSubtree->bn_pbnFront);
  MoveSubTreeToArray(pbnSubtree->bn_pbnBack);

  // Get the node in array
  TBspNode<Type, iDimensions> &bnInArray = bt_abnNodes[_ctNextIndex];
  _ctNextIndex--;

  // Copy properties to the array node
  (TPlane<Type, iDimensions>&)bnInArray = (TPlane<Type, iDimensions>&)*pbnSubtree;
  bnInArray.bn_bnlLocation = pbnSubtree->bn_bnlLocation;
  bnInArray.bn_ulPlaneTag = pbnSubtree->bn_ulPlaneTag;
  
  // Let plane tag hold pointer to node in array
  pbnSubtree->bn_ulPlaneTag = (size_t)&bnInArray;

  // Remap pointers to subnodes
  if (pbnSubtree->bn_pbnFront == NULL) {
    bnInArray.bn_pbnFront = NULL;
  } else {
    bnInArray.bn_pbnFront = (TBspNode<Type, iDimensions>*)pbnSubtree->bn_pbnFront->bn_ulPlaneTag;
  }
  if (pbnSubtree->bn_pbnBack == NULL) {
    bnInArray.bn_pbnBack = NULL;
  } else {
    bnInArray.bn_pbnBack = (TBspNode<Type, iDimensions>*)pbnSubtree->bn_pbnBack->bn_ulPlaneTag;
  }
}

// Count nodes in subtree.
template<class Type, int iDimensions>
INDEX TBspTree<Type, iDimensions>::CountNodes(TBspNode<Type, iDimensions> *pbnSubtree)
{
  if (pbnSubtree == NULL) {
    return 0;
  } else {
    return 1+ CountNodes(pbnSubtree->bn_pbnFront) +  CountNodes(pbnSubtree->bn_pbnBack);
  }
}

// Move all nodes to array.
template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::MoveNodesToArray(void)
{
  // If there is no tree
  if (bt_pbnRoot == NULL) {
    return;  // do nothing
  }

  // Count nodes
  INDEX ctNodes = CountNodes(bt_pbnRoot);
  
  // Allocate large enough array
  bt_abnNodes.New(ctNodes);
  
  // Start at the end of array
  _ctNextIndex = ctNodes - 1;
  
  // Recusively remap all nodes
  MoveSubTreeToArray(bt_pbnRoot);

  // Delete the old nodes
  bt_pbnRoot->DeleteBSPNodeRecursively();

  // First node is always at start of array
  bt_pbnRoot = &bt_abnNodes[0];
}

// Read/write entire bsp tree to disk.
template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::Read_t(CTStream &strm) // throw char *
{
  // Free eventual existing tree
  Destroy();

  // Read current version and size
  INDEX iVersion;
  SLONG slSize;
  strm >> iVersion >> slSize;
  ASSERT(iVersion == 1);

  // Read count of nodes and create array
  INDEX ctNodes;
  strm >> ctNodes;

  // We can't do sizeof(TBspNode<Type, iDimensions>) because it uses pointers and their size may be 8 bytes on x64.
  const int sizeOfBSPNode =
    sizeof(enum BSPNodeLocation) +
    sizeof(INDEX) + // front index
    sizeof(INDEX) + // back index
    sizeof(ULONG) + // plane tag
    sizeof(TPlane<Type, iDimensions>);

  ASSERT(slSize == (SLONG)(sizeof(INDEX) + ctNodes * sizeOfBSPNode));

  bt_abnNodes.New(ctNodes);
  
  // For each node
  for (INDEX iNode = 0; iNode < ctNodes; iNode++)
  {
    TBspNode<Type, iDimensions> &bn = bt_abnNodes[iNode];
    
    // Read it from disk
    strm.Read_t(&(TPlane<Type, iDimensions>&)bn, sizeof(TPlane<Type, iDimensions>));
    strm >> (INDEX&)bn.bn_bnlLocation;

    INDEX iFront;
    strm>>iFront;
    if (iFront == -1) {
      bn.bn_pbnFront = NULL;
    } else {
      bn.bn_pbnFront = &bt_abnNodes[iFront];
    }

    INDEX iBack;
    strm >> iBack;
    if (iBack == -1) {
      bn.bn_pbnBack = NULL;
    } else {
      bn.bn_pbnBack = &bt_abnNodes[iBack];
    }

    ULONG ulPlaneTag; // saved 4 bytes
    strm >> ulPlaneTag;
    bn.bn_ulPlaneTag = ulPlaneTag;
  }

  // Check end id
  strm.ExpectID_t("BSPE");  // bsp end

  // First node is always at start of array
  if (bt_abnNodes.Count() > 0) {
    bt_pbnRoot = &bt_abnNodes[0];
  } else {
    bt_pbnRoot = NULL;
  }
}

template<class Type, int iDimensions>
void TBspTree<Type, iDimensions>::Write_t(CTStream &strm) // throw char *
{
  INDEX ctNodes = bt_abnNodes.Count();

  // We can't do sizeof(TBspNode<Type, iDimensions>) because it uses pointers and their size may be 8 bytes on x64.
  const int sizeOfBSPNode =
    sizeof(enum BSPNodeLocation) +
    sizeof(INDEX) + // front index
    sizeof(INDEX) + // back index
    sizeof(ULONG) + // plane tag
    sizeof(TPlane<Type, iDimensions>);

  // Calculate size of chunk to write
  SLONG slSize = sizeof(INDEX) + ctNodes * sizeOfBSPNode;
  
  // Write current version and size
  strm << INDEX(1) << slSize;

  // Write count of nodes
  strm << ctNodes;
  
  // For each node
  for (INDEX iNode = 0; iNode < ctNodes; iNode++)
  {
    TBspNode<Type, iDimensions> &bn = bt_abnNodes[iNode];
    
    // Write it to disk
    strm.Write_t(&(TPlane<Type, iDimensions>&)bn, sizeof(TPlane<Type, iDimensions>));
    strm<<(INDEX&)bn.bn_bnlLocation;

    INDEX iFront;
    if (bn.bn_pbnFront == NULL) {
      iFront = -1;
    } else {
      iFront = bt_abnNodes.Index(bn.bn_pbnFront);
    }
    strm << iFront;

    INDEX iBack;
    if (bn.bn_pbnBack == NULL) {
      iBack = -1;
    } else {
      iBack = bt_abnNodes.Index(bn.bn_pbnBack);
    }
    strm << iBack;
    strm << IntPtrToID(bn.bn_ulPlaneTag);
  }
  
  // Write end id for checking
  strm.WriteID_t("BSPE");  // bsp end
}

// Instantiate template classes implemented here for needed types
#pragma warning (disable: 4660) // if already instantiated by some class

// Remove templates
template DOUBLEbspvertex3D;
template DOUBLEbspvertexcontainer3D;
template DOUBLEbspedge3D;
template DOUBLEbspnode3D;
template DOUBLEbsppolygon3D;
template DOUBLEbsptree3D;
template DOUBLEbspcutter3D;

template FLOATbspvertex3D;
template FLOATbspvertexcontainer3D;
template FLOATbspedge3D;
template FLOATbspnode3D;
template FLOATbsppolygon3D;
template FLOATbsptree3D;
template FLOATbspcutter3D;

#pragma warning (default: 4660)