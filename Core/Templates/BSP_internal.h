/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_BSP_INTERNAL_H
#define SE_INCL_BSP_INTERNAL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

/*
 * Type used to identify BSP-node locations
 */
enum BSPNodeLocation
{
  BNL_ILLEGAL = 0,    // for illegal value
  BNL_INSIDE,       // inside leaf node
  BNL_OUTSIDE,      // outside leaf node
  BNL_BRANCH,       // branch node, unspecified location
};

/*
 * Template class for BSP vertex
 */
template<class Type, int iDimensions>
class TBspVertex : public TVector<Type, iDimensions>
{
  public:
    /* Default constructor. */
    inline TBspVertex(void) {};

    /* Assignment operator with coordinates only. */
    inline TBspVertex<Type, iDimensions> &operator=(const TVector<Type, iDimensions> &vCoordinates);
};

/*
 * Template class for BSP vertex container
 */
template<class Type, int iDimensions>
class TBspVertexContainer
{
  // vc60 fallback
  public:
    typedef TBspVertex<Type, iDimensions> TVertexType; 
    typedef TBspEdge<Type, iDimensions> TEdgeType;

  public:
    INDEX bvc_iMaxAxis;                     // index of largest axis of direction
    Type  bvc_tMaxAxisSign;                 // sign of largest axis of direction

    TStaticStackArray<TVertexType> bvc_aVertices;  // array of vertices
  public:
    TVector<Type, iDimensions> bvc_vDirection;                  // direction of the split line

    // Default constructor.
    TBspVertexContainer(void);

    // Initialize for a direction.
    void Initialize(const TVector<Type, iDimensions> &vDirection);
    
    // Uninitialize.
    void Uninitialize(void);

    // Check if this container is in an unusable state (polygon coplanar with the splitter).
    inline BOOL IsPlannar(void)
    {
      return bvc_iMaxAxis == 0;
    };

    // Add a new vertex.
    inline void AddVertex(const TVector<Type, iDimensions> &vPoint);

    // Sort vertices in this container along the largest axis of container direction.
    void Sort(void);
    
    // Elliminate paired vertices.
    void ElliminatePairedVertices(void);
    
    // Create edges from vertices in one container -- must be sorted before.
    void CreateEdges(TDynamicArray<TEdgeType> &abedAll, size_t ulEdgeTag);
};

/*
 * Template class for BSP edge
 */
template<class Type, int iDimensions>
class TBspEdge
{
  public:
    TVector<Type, iDimensions> bed_vVertex0;  // edge vertices
    TVector<Type, iDimensions> bed_vVertex1;
    size_t bed_ulEdgeTag;   // tags for BSPs with tagged edges/planes

    // Default constructor. 
    inline TBspEdge(void) : bed_ulEdgeTag(-1) {};
    
    // Constructor with two vectors. 
    inline TBspEdge(const TVector<Type, iDimensions> &vVertex0, const TVector<Type, iDimensions> &vVertex1, size_t ulTag);
    
    // Clear the object. 
    inline void Clear(void) {};
    
    // Remove all edges marked for removal
    static void RemoveMarkedBSPEdges(TDynamicArray<TBspEdge<Type, iDimensions> > &abed);
    
    // Optimize a polygon made out of BSP edges using tag information
    static void OptimizeBSPEdges(TDynamicArray<TBspEdge<Type, iDimensions> > &abed);
};

/*
 * Template class for polygons used in creating BSP-trees
 */
template<class Type, int iDimensions>
class TBspPolygon : public TPlane<Type, iDimensions>
{
  public:
    TDynamicArray<TBspEdge<Type, iDimensions> > bpo_abedPolygonEdges;  // array of edges in the polygon
    size_t bpo_ulPlaneTag;         // tags for BSPs with tagged planes (-1 for no tag)

    // Add an edge to the polygon. 
    inline void AddEdge(const TVector<Type, iDimensions> &vPoint0, const TVector<Type, iDimensions> &vPoint1, size_t ulTag);

    // Default constructor. 
    inline TBspPolygon(void) : bpo_ulPlaneTag(-1) {};
    
    // Constructor with array of edges and plane. 
    inline TBspPolygon(
      TPlane<Type, iDimensions> &plPlane, TDynamicArray<TBspEdge<Type, iDimensions> > abedPolygonEdges, size_t ulPlaneTag)
        : TPlane<Type, iDimensions>(plPlane)
        , bpo_abedPolygonEdges(abedPolygonEdges)
        , bpo_ulPlaneTag(ulPlaneTag)
      {};

    // Clear the object. 
    inline void Clear(void) {bpo_abedPolygonEdges.Clear();};
};

//! Template class for BSP line segment.
template<class Type, int iDimensions>
class TBspLine
{
  public:
    Type bl_tMin;
    Type bl_tMax;
};

/*
 * Template class for BSP-tree node of arbitrary dimensions and arbitrary type of members
 */
template<class Type, int iDimensions>
class TBspNode : public TPlane<Type, iDimensions>  // split plane
{
  public:
    enum BSPNodeLocation bn_bnlLocation;    // location of bsp node

    TBspNode<Type, iDimensions> *bn_pbnFront;        // pointer to child node in front of split plane
    TBspNode<Type, iDimensions> *bn_pbnBack;         // pointer to child node behind split plane
    size_t bn_ulPlaneTag;         // tags for BSPs with tagged planes (-1 for no tag)

  public:
    // Default constructor (for arrays only). 
    inline TBspNode(void) : bn_ulPlaneTag(-1), bn_pbnBack(nullptr), bn_pbnFront(nullptr), bn_bnlLocation(BNL_ILLEGAL) {};
    
    // Constructor for a leaf node. 
    inline TBspNode(enum BSPNodeLocation bnl);
    
    // Constructor for a branch node. 
    inline TBspNode(const TPlane<Type, iDimensions> &plSplitPlane, size_t ulPlaneTag, TBspNode<Type, iDimensions> &bnFront,
                   TBspNode<Type, iDimensions> &bnBack);
      
    // Constructor for cloning a bsp (sub)tree. 
    TBspNode(TBspNode<Type, iDimensions> &bnRoot);
    
    // Recursive destructor. 
    void DeleteBSPNodeRecursively(void);

    // Find minimum/maximum parameters of points on a line that are inside - recursive
    void FindLineMinMax(TBspLine<Type, iDimensions> &bl, const TVector<Type, iDimensions> &v0,
                        const TVector<Type, iDimensions> &v1, Type t0, Type t1);

    // Test if a sphere is inside, outside, or intersecting. (Just a trivial rejection test) 
    FLOAT TestSphere(const TVector<Type, iDimensions> &vSphereCenter, Type tSphereRadius) const;
    
    // Test if a box is inside, outside, or intersecting. (Just a trivial rejection test) 
    FLOAT TestBox(const OBBox<Type> &box) const;
};

/*
 * Template class that performs polygon cuts using BSP-tree
 */
template<class Type, int iDimensions>
class TBspCutter
{
  public:
    // Split an edge with a plane.
    static inline void SplitEdge(const TVector<Type, iDimensions> &vPoint0, const TVector<Type, iDimensions> &vPoint1, size_t ulEdgeTag,
      const TPlane<Type, iDimensions> &plSplitPlane,
      TBspPolygon<Type, iDimensions> &abedFront, TBspPolygon<Type, iDimensions> &abedBack,
      TBspVertexContainer<Type, iDimensions> &bvcFront, TBspVertexContainer<Type, iDimensions> &bvcBack);

    // Cut a polygon with a BSP tree.
    void CutPolygon(TBspPolygon<Type, iDimensions> &bpoPolygon, TBspNode<Type, iDimensions> &bn);

  public:
    TDynamicArray<TBspEdge<Type, iDimensions> > bc_abedInside;       // edges of inside part of polygon
    TDynamicArray<TBspEdge<Type, iDimensions> > bc_abedOutside;      // edges of outside part of polygon
    TDynamicArray<TBspEdge<Type, iDimensions> > bc_abedBorderInside; // edges of border part of polygon facing inwards
    TDynamicArray<TBspEdge<Type, iDimensions> > bc_abedBorderOutside;// edges of border part of polygon facing outwards

    // Split a polygon with a plane.
    static inline BOOL SplitPolygon(TBspPolygon<Type, iDimensions> &bpoPolygon, const TPlane<Type, iDimensions> &plPlane, size_t ulPlaneTag,
      TBspPolygon<Type, iDimensions> &bpoFront, TBspPolygon<Type, iDimensions> &bpoBack);

    // Constructor for splitting a polygon with a BSP tree.
    TBspCutter(TBspPolygon<Type, iDimensions> &bpoPolygon, TBspNode<Type, iDimensions> &bnRoot);
    
    // Destructor.
    ~TBspCutter(void);
};

#endif  /* include-once check. */