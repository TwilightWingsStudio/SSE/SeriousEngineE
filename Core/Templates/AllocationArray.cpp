/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */


#ifndef SE_INCL_ALLOCATIONARRAY_CPP
#define SE_INCL_ALLOCATIONARRAY_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/StaticStackArray.h>
#include <Core/Templates/StaticStackArray.cpp>
#include <Core/Templates/StaticArray.cpp>

extern CORE_API BOOL _bAllocationArrayParanoiaCheck;

// Default constructor.
template<class Type>
inline TAllocationArray<Type>::TAllocationArray(void) : TStaticArray<Type>(), aa_aiFreeElements()
{
  aa_ctAllocationStep = 256;
}

// Destructor.
template<class Type>
inline TAllocationArray<Type>::~TAllocationArray(void) {};

// Destroy all objects, and reset the array to initial (empty) state.
template<class Type>
inline void TAllocationArray<Type>::Clear(void)
{
  // Delete the objects themselves
  TStaticArray<Type>::Clear();
  
  // Clear array of free indices
  aa_aiFreeElements.Clear();
}

// Set how many elements to allocate when stack overflows.
template<class Type>
inline void TAllocationArray<Type>::SetAllocationStep(ULONG ctStep) 
{
  ASSERT(ctStep>0);
  aa_ctAllocationStep = ctStep;
};

// Create a given number of objects.
template<class Type>
inline void TAllocationArray<Type>::New(ULONG iCount)
{
  // never call this!
  ASSERT(FALSE);
};

// Destroy all objects.
template<class Type>
inline void TAllocationArray<Type>::Delete(void)
{
  // never call this!
  ASSERT(FALSE);
}

// Alocate a new object.
template<class Type>
inline ULONG TAllocationArray<Type>::Allocate(void)
{
  // If there are no more free indices
  if (aa_aiFreeElements.Count() == 0)
  {
    // Remember old size
    ULONG ctOldSize = TStaticArray<Type>::Count();
    
    // Expand the array by the allocation step
    this->Expand(ctOldSize+aa_ctAllocationStep);
    
    // Create new free indices
    ULONG *piNewFree = aa_aiFreeElements.Push(aa_ctAllocationStep);
    
    // Fill them up
    for (ULONG iNew = 0; iNew < aa_ctAllocationStep; iNew++)
    {
      piNewFree[iNew] = ctOldSize + iNew;
    }
  }
  
  // Pop one free index from the top of stack, and use that one
  return aa_aiFreeElements.Pop();
}

// Free object with given index.
template<class Type>
inline void TAllocationArray<Type>::Free(ULONG iToFree)
{
#ifndef NDEBUG
  // Must be within pool limits
  ASSERT(iToFree >= 0 && iToFree<TStaticArray<Type>::Count());
  
  // Must not be free
  if (_bAllocationArrayParanoiaCheck) {
    ASSERT(IsAllocated(iToFree));
  }
#endif
  // push its index on top of the free stack
  aa_aiFreeElements.Push() = iToFree;
}

// Free all objects, but keep pool space.
template<class Type>
inline void TAllocationArray<Type>::FreeAll(void)
{
  // Clear the free array
  aa_aiFreeElements.PopAll();
  
  // Push as much free elements as there is pool space
  ULONG ctSize = TStaticArray<Type>::Count();
  ULONG *piNewFree = aa_aiFreeElements.Push(ctSize);
  
  // Fill them up
  for (ULONG iNew = 0; iNew < ctSize; iNew++)
  {
    piNewFree[iNew] = iNew;
  }
}

// Check if an index is allocated (slow!)
template<class Type>
inline BOOL TAllocationArray<Type>::IsAllocated(ULONG i)
{
  // Must be within pool limits
  ASSERT(i >= 0 && i < TStaticArray<Type>::Count());
  
  // For each free index
  ULONG ctFree = aa_aiFreeElements.Count();
  for (ULONG iFree = 0; iFree < ctFree; iFree++)
  {
    // If it is that one
    if (aa_aiFreeElements[iFree] == i) {
      return FALSE; // it is not allocated
    }
  }
  
  // If not found as free, it is allocated
  return TRUE;
}

// Random access operator.
template<class Type>
inline Type &TAllocationArray<Type>::operator[](ULONG iObject)
{
#ifndef NDEBUG
  ASSERT(this!=NULL);
  // Must be within pool limits
  ASSERT(iObject >= 0 && iObject<TStaticArray<Type>::Count());
  
  // Must not be free
  if (_bAllocationArrayParanoiaCheck) {
    ASSERT(IsAllocated(iObject));
  }
#endif
  return TStaticArray<Type>::operator[](iObject);
}

template<class Type>
inline const Type &TAllocationArray<Type>::operator[](ULONG iObject) const
{
#ifndef NDEBUG
  ASSERT(this != NULL);
  
  // Must be within pool limits
  ASSERT(iObject < TStaticArray<Type>::Count());
  
  // Must not be free
  if (_bAllocationArrayParanoiaCheck) {
    ASSERT(IsAllocated(iObject));
  }
#endif
  return TStaticArray<Type>::operator[](iObject);
}

// Get number of allocated objects in array.
template<class Type>
ULONG TAllocationArray<Type>::Count(void) const
{
  ASSERT(this != NULL);
  
  // It is pool size without the count of free elements
  return TStaticArray<Type>::Count() - aa_aiFreeElements.Count();
}

// Get index of a object from it's pointer.
template<class Type>
ULONG TAllocationArray<Type>::Index(Type *ptObject)
{
  ASSERT(this != NULL);
  ULONG i = TStaticArray<Type>::Index(ptObject);
  ASSERT(IsAllocated(i));
  return i;
}

// Assignment operator.
template<class Type>
TAllocationArray<Type> &TAllocationArray<Type>::operator=(const TAllocationArray<Type> &aaOriginal)
{
  ASSERT(this != NULL);
  (TStaticArray<Type>&)(*this) = (TStaticArray<Type>&)aaOriginal;
  aa_aiFreeElements = aaOriginal.aa_aiFreeElements;
  aa_ctAllocationStep = aaOriginal.aa_ctAllocationStep;
}

#endif  /* include-once check. */