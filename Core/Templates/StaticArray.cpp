/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATICARRAY_CPP
#define SE_INCL_STATICARRAY_CPP

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#define FOREACHINSTATICARRAY(array, type, iter) \
  for (TStaticArrayIterator<type> iter(array); !iter.IsPastEnd(); iter.MoveToNext() )

#include <Core/Base/Console.h>
#include <Core/Templates/StaticArray.h>

// Default constructor.
template<class Type>
inline TStaticArray<Type>::TStaticArray(void)
{
  sa_Count = 0;
  sa_Array = NULL;
}

/*
 * Destructor.
 */
template<class Type>
inline TStaticArray<Type>::~TStaticArray(void)
{
  // If some objects were allocated
  if (sa_Count != 0) {
    Delete(); // destroy them
  }
};

// Random access operator.
template<class Type>
inline void TStaticArray<Type>::operator=(const TStaticArray<Type> &arOriginal)
{
  CopyArray(arOriginal);
}

template<class Type>
// Destroy all objects, and reset the array to initial (empty) state.
inline void TStaticArray<Type>::Clear(void)
{
  if (sa_Count != 0) {
    Delete(); 
  }
}

// Create a given number of objects.
template<class Type>
inline void TStaticArray<Type>::New(ULONG iCount)
{
  ASSERT(this != NULL);

  // If no new members are needed in fact
  if (iCount == 0) {
    return; // do nothing
  }

  //ASSERT(sa_Count==0 && sa_Array==NULL);
#ifndef NDEBUG
  if (!(sa_Count == 0 && sa_Array == NULL))
  {
    if (sa_Array == NULL) {
      CDebugF("TStaticArray array not set!\n");
    } else {
      CDebugF("TStaticArray new(%d) called while already holding %d elements!\n", iCount, sa_Count);
    }
  }
#endif
  sa_Count = iCount;
  sa_Array = new Type[iCount + 1]; //(+1 for cache-prefetch opt)
};

// Expand stack size but keep old objects.
template<class Type>
inline void TStaticArray<Type>::Expand(ULONG iNewCount)
{
  ASSERT(this != NULL && iNewCount > sa_Count);

  // If not already allocated
  if (sa_Count == 0) {
    // Just allocate
    New(iNewCount);
    return;
    
  // If already allocated
  } else {
    ASSERT(sa_Count != 0 && sa_Array != NULL);
    
    // Allocate new array with more space
    Type *ptNewArray = new Type[iNewCount + 1]; //(+1 for cache-prefetch opt)
    
    // Copy old objects
    for (ULONG iOld = 0; iOld < sa_Count; iOld++)
    {
      ptNewArray[iOld] = sa_Array[iOld];
    }
    
    // Free old array
    delete[] sa_Array;
    
    // Remember the new array
    sa_Count = iNewCount;
    sa_Array = ptNewArray;
  }
}

// Destroy all objects.
template<class Type>
inline void TStaticArray<Type>::Delete(void)
{
  ASSERT(this != NULL);
  ASSERT(sa_Count != 0 && sa_Array != NULL);
  delete[] sa_Array;
  sa_Count = 0;
  sa_Array = NULL;
}

// Random access operator.
template<class Type>
inline Type &TStaticArray<Type>::operator[](ULONG i)
{
  ASSERT(this != NULL);
  ASSERT(i < sa_Count);     // check bounds
  return sa_Array[i];
}

// TODO: Add comment here
template<class Type>
inline const Type &TStaticArray<Type>::operator[](ULONG i) const
{
  ASSERT(this !=  NULL);
  ASSERT(i < sa_Count);     // check bounds
  return sa_Array[i];
}

// Get number of elements in array.
template<class Type>
ULONG TStaticArray<Type>::Count(void) const
{
  ASSERT(this != NULL);
  return sa_Count;
}

// Get index of a member from it's pointer
template<class Type>
ULONG TStaticArray<Type>::Index(Type *ptMember)
{
  ASSERT(this != NULL);
  ASSERT(sa_Count > 0);
  ASSERT(uintptr_t(ptMember) >= uintptr_t(sa_Array)); // Error if behind the array pointer.

  // Find element!
  for (ULONG i = 0; i < sa_Count; i++)
  {
    if (ptMember == &sa_Array[i]) {
      return i;
    }
  }

  ASSERTALWAYS("TStaticArray<>::Index(): Not a member of this array!");
  return 0;
}

// Assignment operator.
template<class Type>

// Copy all elements of another array into this one.
void TStaticArray<Type>::CopyArray(const TStaticArray<Type> &arOriginal)
{
  ASSERT(this != NULL);
  ASSERT(&arOriginal != NULL);
  ASSERT(this != &arOriginal);

  // Clear previous contents
  Clear();
  
  // Get count of elements in original array
  ULONG ctOriginal = arOriginal.Count();
  
  // If the other array has no elements
  if (ctOriginal == 0) {
    return;
  }
  
  // Create that much elements
  New(ctOriginal);
  
  // Copy them all
  for (ULONG iNew = 0; iNew < ctOriginal; iNew++)
  {
    sa_Array[iNew] = arOriginal[iNew];
  }
}

// Move all elements of another array into this one. 
template<class Type>
void TStaticArray<Type>::MoveArray(TStaticArray<Type> &arOther)
{
  ASSERT(this != NULL);
  ASSERT(&arOther != NULL);
  ASSERT(this != &arOther);

  // Clear previous contents
  Clear();
  
  // If the other array has no elements
  if (arOther.Count() == 0) {
    return; // no assignment
  }
  
  // Move data from the other array into this one and clear the other one
  sa_Count = arOther.sa_Count;
  sa_Array = arOther.sa_Array;
  arOther.sa_Count = 0;
  arOther.sa_Array = NULL;
}

//! Sort elements.
template<class Type>
void TStaticArray<Type>::Sort(int (*pCompare)(const void *p0, const void *p1))
{
  if (Count() < 2) {
    return;
  }

  qsort(sa_Array, Count(), sizeof(Type), *pCompare);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TStaticArrayIterator

// Template class for iterating static array.
template<class Type>
class TStaticArrayIterator
{
  private:
    ULONG sai_Index;          // index of current element
    TStaticArray<Type> &sai_Array;   // reference to array
  public:
    // Constructor for given array. 
    inline TStaticArrayIterator(TStaticArray<Type> &sa);
    
    // Destructor. 
    inline ~TStaticArrayIterator(void);

    // Move to next object. 
    inline void MoveToNext(void);
    
    // Check if finished. 
    inline BOOL IsPastEnd(void);
    
    // Get current element. 
    Type &Current(void)
    {
      return sai_Array[sai_Index];
    }
    
    Type &operator*(void)
    {
      return sai_Array[sai_Index];
    }
    
    operator Type *(void)
    {
      return &sai_Array[sai_Index];
    }
    
    Type *operator->(void)
    {
      return &sai_Array[sai_Index];
    }
};

// Constructor for given array.
template<class Type>
inline TStaticArrayIterator<Type>::TStaticArrayIterator(TStaticArray<Type> &sa) : sai_Array(sa)
{
  sai_Index = 0;
}

// Destructor.
template<class Type>
inline TStaticArrayIterator<Type>::~TStaticArrayIterator(void)
{
  sai_Index = -1;
}

// Move to next object.
template<class Type>
inline void TStaticArrayIterator<Type>::MoveToNext(void)
{
  ASSERT(this != NULL);
  sai_Index++;
}

// Check if finished.
template<class Type>
inline BOOL TStaticArrayIterator<Type>::IsPastEnd(void)
{
  ASSERT(this != NULL);
  return sai_Index>=sai_Array.sa_Count;
}

#endif  /* include-once check. */