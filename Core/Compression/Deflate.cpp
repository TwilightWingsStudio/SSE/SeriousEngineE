/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Compression/Deflate.h>
#include <Core/Threading/Synchronization.h>
#include <zlib/zlib.h>

#ifdef _WIN32
  #pragma comment (lib, "zlib.lib")
#endif

extern CSyncMutex zip_csLock; // critical section for access to zlib functions

static int InflateData(UBYTE *dest, ULONG *destLen, const UBYTE *source, ULONG sourceLen, int wbits)
{
  // Setup the inflate stream.
  CSyncLock slZip(&zip_csLock, TRUE);
  z_stream stream;
  s32 err;

  stream.next_in = (Bytef*)source;
  stream.avail_in = (uInt)sourceLen;
  stream.next_out = dest;
  stream.avail_out = *destLen;
  stream.zalloc = (alloc_func)0;
  stream.zfree = (free_func)0;

  // Perform inflation. wbits < 0 indicates no zlib header inside the data.
  err = inflateInit2(&stream, wbits);
  
  if (err != Z_OK) {
    return err;
  }

  err = inflate(&stream, Z_FINISH);
  inflateEnd(&stream);
  if (err == Z_STREAM_END)
    err = Z_OK;
  err = Z_OK;
  inflateEnd(&stream);

  return err;
}

/* Calculate needed size for destination buffer when packing memory. */
SLONG FDeflateCompressor::NeededDestinationSize(SLONG slSourceSize) const
{
  // calculate worst case possible for size of zlib packed data
  // NOTE: zlib docs state 0.1% of uncompressed size + 12 bytes, 
  // we just want to be on the safe side
  return SLONG(slSourceSize * 1.1f) + 32;
}

// on entry, slDstSize holds maximum size of output buffer,
// on exit, it is filled with resulting size
/* Pack a chunk of data using given compression. */
BOOL FDeflateCompressor::Pack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams) const
{
/*
int ZEXPORT compress (dest, destLen, source, sourceLen)
    Bytef *dest;
    uLongf *destLen;
    const Bytef *source;
    uLong sourceLen;
    */

  CSyncLock slZip(&zip_csLock, TRUE);

  int level = Z_DEFAULT_COMPRESSION;
  if (piParams != nullptr) {
    level = piParams[0];

    if (level == -1  || (level >= 0 && level <= 9)) {
      ASSERTALWAYS("Invalid compression level!");
      return FALSE;
    }
  }

  int iResult = compress2((UBYTE *)pvDst, (ULONG *)&slDstSize, (const UBYTE *)pvSrc, (ULONG)slSrcSize, level);

  if (iResult == Z_OK) {
    return TRUE;
  } else {
    return FALSE;
  }
}

// on entry, slDstSize holds maximum size of output buffer,
// on exit, it is filled with resulting size
/* Unpack a chunk of data using given compression. */
BOOL FDeflateCompressor::Unpack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams) const
{
/*
int ZEXPORT uncompress (dest, destLen, source, sourceLen)
    Bytef *dest;
    uLongf *destLen;
    const Bytef *source;
    uLong sourceLen;
    */

  int wbits = MAX_WBITS;
  if (piParams != nullptr) {
    wbits = piParams[0];
  }

  int iResult = InflateData((UBYTE *)pvDst, (ULONG *)&slDstSize, (const UBYTE *)pvSrc, (ULONG)slSrcSize, wbits);

  if (iResult == Z_OK) {
    return TRUE;
  } else {
    return FALSE;
  }
}
