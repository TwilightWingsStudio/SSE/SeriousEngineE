/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Compression/RLEBB.h>

/////////////////////////////////////////////////////////////////////
// RLE compressor

/*
RLE packed format(s):
NOTE:
  In order to decompress packed data, size of original data must be known, it is not
  written in the packed data by the algorithms. Depending on the usage, data size may be
  included in the header by the called function. This generally saves even more space
  if smaller chunks of known size are compressed (e.g. small networks packets).

Basic implemented packing format here is BYTE-BYTE. It packes data by bytes and uses bytes
for packing codes. Other sized RLE formats can be implemented as needed (WORD-WORD,
LONG-LONG, BYTE-WORD, WORD-BYTE etc.).

Packed data is divided in sections, each section has a leading byte as code and compressed
data following it. The data is interpreted differently, depending on the code.

  1) CODE<0 => REPLICATION
  If the code is negative, following data is one byte that is replicated given number of
  times:
  CODE DATA                 -> DATA DATA DATA ... DATA ((-CODE)+1 times)
  (note that it is not possible to encode just one byte this way, since one byte can be coded
   well with copying)
  (count = -code+1  =>  code = -count+1)

  2) CODE>=0 => COPYING
  If the code is positive, following data is given number of bytes that are just copied:
  CODE DAT0 DAT1 ... DATn   -> DAT0 DAT1 ... DATn (n=CODE)
  (note that CODE=0 means copy next one byte)
  (count = code+1  =>  code = count-1)

Packing algorithm used here relies on the destination buffer being big enough to
hold packed data. Generally, for BYTE-BYTE packing, maximum buffer is 129/128 of
original size (degenerate case with no data replication).
*/

/*
 * Calculate needed size for destination buffer when packing memory.
 */
SLONG FRLEBBCompressor::NeededDestinationSize(SLONG slSourceSize) const
{
  // calculate worst case possible for size of RLEBB packed data
  // *129/128+1 would be enough, but we add some more to ensure that we don't
  // overwrite the temporary buffer
   return slSourceSize * 129 / 128 + 5;
}

// on entry, slDstSize holds maximum size of output buffer,
// on exit, it is filled with resulting size
/* Pack a chunk of data using given compression. */
BOOL FRLEBBCompressor::Pack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams) const
{
  // cannot pack zero bytes
  ASSERT(slSrcSize >= 1);

  // calculate limits for source and destination buffers
  const SBYTE *pbSourceFirst = (const SBYTE *)pvSrc;            // start marker
  const SBYTE *pbSourceLimit = (const SBYTE *)pvSrc + slSrcSize;   // end marker

//  SLONG slDestinationSize=NeededDestinationSize(slSrcSize);
  SBYTE *pbDestinationFirst = (SBYTE *)pvDst;            // start marker
  SBYTE *pbDestinationLimit = (SBYTE *)pvDst + slDstSize;  // end marker

  UBYTE *pbCountFirst = (UBYTE *)pbDestinationLimit - slSrcSize; // start marker
  UBYTE *pbCountLimit = (UBYTE *)pbDestinationLimit;           // end marker

  {
    /* PASS 1: Use destination buffer to cache number of forward-same bytes. */

    // set the count of the last byte to one
    UBYTE *pbCount = pbCountLimit - 1;
    *pbCount-- = 1;

    // for all bytes from one before last to the first one
    for (const SBYTE *pbSource = pbSourceLimit - 2; pbSource >= pbSourceFirst; pbSource--, pbCount--)
    {
      // if the byte is same as its successor, and the count will fit in code
      if (pbSource[0] == pbSource[1] && (SLONG)pbCount[1] + 1 <= -(SLONG)MIN_SBYTE) {
        // set its count to the count of its successor plus one
        pbCount[0] = pbCount[1] + 1;

      // if the byte is different than its successor
      } else {
        // set its count to one
        pbCount[0] = 1;
      }
    }
  }

  /* PASS 2: Pack bytes from source to the destination buffer. */

  // start at the beginning of the buffers
  const SBYTE *pbSource      = pbSourceFirst;
  const UBYTE *pbCount       = pbCountFirst;
  SBYTE       *pbDestination = pbDestinationFirst;

  // while there is some data to pack
  while (pbSource < pbSourceLimit)
  {
    ASSERT(pbCount < pbCountLimit);

    // if current byte is replicated
    if (*pbCount > 1) {
      // write the replicate-packed data
      INDEX ctSameBytes = (INDEX)*pbCount;
      SLONG slCode = -ctSameBytes + 1;
      ASSERT((SLONG)MIN_SBYTE <= slCode && slCode < 0);
      *pbDestination++ = (SBYTE)slCode;
      *pbDestination++ = pbSource[0];
      pbSource += ctSameBytes;
      pbCount  += ctSameBytes;

    // if current byte is not replicated
    } else {
      // count bytes to copy before encountering byte replicated more than 3 times
      INDEX ctDiffBytes = 1;

      while ((ctDiffBytes < (SLONG)MAX_SBYTE + 1) && (&pbSource[ctDiffBytes] < pbSourceLimit) )
      {
        if ((SLONG)pbCount[ctDiffBytes - 1] <= 3) {
          ctDiffBytes++;
        } else {
          break;
        }
      }

      // write the copy-packed data
      SLONG slCode = ctDiffBytes - 1;
      ASSERT(0 <= slCode && slCode <= (SLONG)MAX_SBYTE);
      *pbDestination++ = (SBYTE)slCode;
      memcpy(pbDestination, pbSource, ctDiffBytes);
      pbSource      += ctDiffBytes;
      pbCount       += ctDiffBytes;
      pbDestination += ctDiffBytes;
    }
  }

  // packing must exactly be finished now
  ASSERT(pbSource == pbSourceLimit);
  ASSERT(pbCount  == pbCountLimit);

  // calculate size of packed data
  slDstSize = pbDestination - pbDestinationFirst;
  return TRUE;
}

// on entry, slDstSize holds maximum size of output buffer,
// on exit, it is filled with resulting size
/* Unpack a chunk of data using given compression. */
BOOL FRLEBBCompressor::Unpack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams) const
{
  const SBYTE *pbSource      = (const SBYTE *)pvSrc;            // current pointer
  const SBYTE *pbSourceLimit = (const SBYTE *)pvSrc + slSrcSize;  // end marker

  SBYTE *pbDestination      = (SBYTE *)pvDst;  // current pointer
  SBYTE *pbDestinationFirst = (SBYTE *)pvDst;  // start marker

  // repeat
  do {
    // get code
    SLONG slCode = *pbSource++;

    // if it is replication
    if (slCode < 0) {
      // get next byte and replicate it given number of times
      INDEX ctSameBytes = -slCode + 1;
      memset(pbDestination, *pbSource++, ctSameBytes);
      pbDestination += ctSameBytes;

    // if it is copying
    } else {
      // copy given number of next bytes
      INDEX ctCopyBytes = slCode + 1;
      memcpy(pbDestination, pbSource, ctCopyBytes);
      pbSource      += ctCopyBytes;
      pbDestination += ctCopyBytes;
    }
  // until all data is unpacked
  } while (pbSource < pbSourceLimit);

  // data must be unpacked correctly
  ASSERT(pbSource == pbSourceLimit);

  // calculate size of data that was unpacked
  slDstSize = pbDestination - pbDestinationFirst;
  return TRUE;
}