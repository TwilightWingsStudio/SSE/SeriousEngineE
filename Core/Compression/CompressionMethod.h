/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_COMPRESSIONMETHOD_H
#define SE_INCL_COMPRESSIONMETHOD_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Lossless data compression methods.
enum CompressionMethod
{
  //! Erroring placeholder.
  COMPRESSION_METHOD_INVALID = 0,

  //! Data as is.
  COMPRESSION_METHOD_STORE,

  // Run-length encoding.
  COMPRESSION_METHOD_RLE,

  //! LZ77 + Huffman encoding
  COMPRESSION_METHOD_DEFLATE,

  //! Lempel-Ziv-Storer-Szymanski.
  COMPRESSION_METHOD_LZSS,

  //! Lempel-Ziv-Markov chain-Algorithm.
  COMPRESSION_METHOD_LZMA,

  //! Lempel–Ziv–Oberhumer
  COMPRESSION_METHOD_LZO,

  //! LZ4.
  COMPRESSION_METHOD_LZ4,

  //! Lempel-Ziv Ross Williams variant 1.
  COMPRESSION_METHOD_LZRW1,

  //! Lempel–Ziv–Welch
  COMPRESSION_METHOD_LZW,

  //! Prediction by partial matching.
  COMPRESSION_METHOD_PPMd,
};

#endif /* include-once check. */