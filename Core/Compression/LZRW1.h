/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_LZRW1_H
#define SE_INCL_LZRW1_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Compression/Compressor.h>

//! Data compressor that uses LZRW1 (Lempel–Ziv Ross Williams Variant 1) algorithm.
class CORE_API FLZCompressor : public FCompressor
{
  public:
    //! Calculate needed size for destination buffer when packing memory.
    SLONG NeededDestinationSize(SLONG slSourceSize) const override;

    // on entry, slDstSize holds maximum size of output buffer,
    // on exit, it is filled with resulting size
    //! Pack a chunk of data using given compression.
    BOOL Pack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams = nullptr) const override;

    //! Unpack a chunk of data using given compression.
    BOOL Unpack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams = nullptr) const override;

    //! Returns compression method type.
    CompressionMethod GetType() const override
    {
      return COMPRESSION_METHOD_LZRW1;
    }
};

#endif /* include-once check. */