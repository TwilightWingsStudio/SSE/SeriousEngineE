/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_COMPRESSIONMANAGER_H
#define SE_INCL_COMPRESSIONMANAGER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>
#include <Core/Compression/Compressor.h>
#include <Core/Threading/Synchronization.h>

//! Unified subsystem used to compress/decompress data.
class CORE_API FCompressionManager
{
  public:
    //! Constructor.
    FCompressionManager();

    //! Make compressor available to be used.
    void Register(FCompressor *pCompressor);

    //! Pack a chunk of data using given compression.
    BOOL Pack(void *pvDst, SLONG &slDstSize, const void *pvSrc, SLONG slSrcSize, CompressionMethod eCompressionMethod, INDEX *piParams = nullptr);

    //! Pack a byte array using given compression.
    BOOL Pack(void *pvDst, SLONG &slDstSize, const FByteArray &src, CompressionMethod eCompressionMethod, INDEX *piParams = nullptr);

    //! Unpack a chunk of data using given compression.
    BOOL Unpack(void *pvDst, SLONG &slDstSize, const void *pvSrc, SLONG slSrcSize, CompressionMethod eCompressionMethod, INDEX *piParams = nullptr);

    //! Unpack a byte array using given compression.
    BOOL Unpack(void *pvDst, SLONG &slDstSize, const FByteArray &src, CompressionMethod eCompressionMethod, INDEX *piParams = nullptr);

    //! Unpack from stream.
    static void UnpackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, FCompressor *pCompressor, INDEX *piParams = nullptr);

    //! Pack to stream.
    static void PackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, FCompressor *pCompressor, INDEX *piParams = nullptr);

    //! Unpack from stream.
    void UnpackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, CompressionMethod eCompressionMethod, INDEX *piParams = nullptr);

    //! Pack to stream.
    void PackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, CompressionMethod eCompressionMethod, INDEX *piParams = nullptr);

    //! Get reference to registered compressors.
    inline TDynamicContainer<FCompressor> &GetCompressors()
    {
      return _cCompressors;
    }

  public:
    CSyncMutex _csManager; // make it thread safe
    TDynamicContainer<FCompressor> _cCompressors;
};

CORE_API extern FCompressionManager *_pCompressionMgr;

#endif /* include-once check. */