/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_COMPRESSOR_H
#define SE_INCL_COMPRESSOR_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Compression/CompressionMethod.h>

//! Abstract base class for objects that can compress memory blocks.
class CORE_API FCompressor
{
  public:
    //! Calculate needed size for destination buffer when packing memory with given compression.
    virtual SLONG NeededDestinationSize(SLONG slSourceSize) const = 0;

    // on entry, slDstSize holds maximum size of output buffer,
    // on exit, it is filled with resulting size
    //! Pack a chunk of data using given compression.
    virtual BOOL Pack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams = nullptr) const = 0;

    //! Unpack a chunk of data using given compression.
    virtual BOOL Unpack(const void *pvSrc, SLONG slSrcSize, void *pvDst, SLONG &slDstSize, INDEX *piParams = nullptr) const = 0;

    //! Returns compression method type.
    virtual CompressionMethod GetType() const
    {
      return COMPRESSION_METHOD_INVALID;
    }
};

#endif /* include-once check. */