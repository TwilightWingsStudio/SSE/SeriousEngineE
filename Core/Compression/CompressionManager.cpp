/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Compression/CompressionManager.h>
#include <Core/Compression/Deflate.h>
#include <Core/Compression/LZRW1.h>
#include <Core/Compression/RLEBB.h>
#include <Core/IO/Stream.h>

#include <Core/Templates/DynamicContainer.cpp>

FCompressionManager *_pCompressionMgr = NULL;

struct Func_CheckComprTypeId : TObjectFunctor<BOOL, FCompressor>
{
  UBYTE _idObjectType;

  Func_CheckComprTypeId(UBYTE idObjectType)
  {
    _idObjectType = idObjectType;
  }

  UBYTE GetTypeId() const
  {
    return _idObjectType;
  }

  BOOL operator()(FCompressor *ptObject) const override
  {
    return GetTypeId() == ULONG(-1) || GetTypeId() == ptObject->GetType();
  }
};

FCompressionManager::FCompressionManager()
{
  _csManager.cs_iIndex = -1;
  Register(new FDeflateCompressor);
  Register(new FLZCompressor);
  Register(new FRLEBBCompressor);
}

void FCompressionManager::Register(FCompressor *pCompressor)
{
  CSyncLock slManager(&_csManager, TRUE);
  _cCompressors.Push() = pCompressor;
}

BOOL FCompressionManager::Pack(void *pvDst, SLONG &slDstSize, const void *pvSrc, SLONG slSrcSize, CompressionMethod eCompressionMethod, INDEX *piParams)
{
  CSyncLock slManager(&_csManager, TRUE);
  Func_CheckComprTypeId func(eCompressionMethod);

  TDynamicContainer<FCompressor> cFound;
  GetCompressors().Find(cFound, func);

  if (cFound.Count() == 0) {
    ASSERTALWAYS("Unsupported compression method!");
    return FALSE;
  }

  return cFound.Pointer(0)->Pack(pvSrc, slSrcSize, pvDst, slDstSize, piParams);
}

BOOL FCompressionManager::Pack(void *pvDst, SLONG &slDstSize, const FByteArray &src, CompressionMethod eCompressionMethod, INDEX *piParams)
{
  return Pack(pvDst, slDstSize, src.ConstData(), src.Size(), eCompressionMethod, piParams);
}

//! Unpack a chunk of data using given compression.
BOOL FCompressionManager::Unpack(void *pvDst, SLONG &slDstSize, const void *pvSrc, SLONG slSrcSize, CompressionMethod eCompressionMethod, INDEX *piParams)
{
  CSyncLock slManager(&_csManager, TRUE);
  Func_CheckComprTypeId func(eCompressionMethod);

  TDynamicContainer<FCompressor> cFound;
  GetCompressors().Find(cFound, func);

  if (cFound.Count() == 0) {
    ASSERTALWAYS("Unsupported compression method!");
    return FALSE;
  }

  return cFound.Pointer(0)->Unpack(pvSrc, slSrcSize, pvDst, slDstSize, piParams);
}

BOOL FCompressionManager::Unpack(void *pvDst, SLONG &slDstSize, const FByteArray &src, CompressionMethod eCompressionMethod, INDEX *piParams)
{
  return Unpack(pvDst, slDstSize, src.ConstData(), src.Size(), eCompressionMethod, piParams);
}

//! Unpack from stream to stream.
void FCompressionManager::UnpackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, FCompressor *pCompressor, INDEX *piParams) // throw char *
{
  // read the header
  SLONG slSizeDst, slSizeSrc;
  strmSrc >> slSizeDst;
  strmSrc >> slSizeSrc;

  // get the buffer of source stream
  UBYTE *pubSrc = strmSrc.mstrm_pubBuffer + strmSrc.mstrm_slLocation;
  // allocate buffer for decompression
  UBYTE *pubDst = (UBYTE*)AllocMemory(slSizeDst);
  // compress there
  BOOL bOk = pCompressor->Unpack(pubSrc, slSizeSrc, pubDst, slSizeDst, piParams);

  // if failed
  if (!bOk) {
    // report error
    FreeMemory(pubDst);
    ThrowF_t(TRANS("Error while unpacking a stream."));
  }

  // write the uncompressed data to destination
  strmDst.Write_t(pubDst, slSizeDst);
  strmDst.SetPos_t(0);
  FreeMemory(pubDst);
}

//! Pack from stream to stream.
void FCompressionManager::PackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, FCompressor *pCompressor, INDEX *piParams) // throw char *
{
  // get the buffer of source stream
  UBYTE *pubSrc = strmSrc.mstrm_pubBuffer + strmSrc.mstrm_slLocation;
  SLONG slSizeSrc = strmSrc.GetStreamSize();

  // allocate buffer for compression
  SLONG slSizeDst = pCompressor->NeededDestinationSize(slSizeSrc);
  UBYTE *pubDst = (UBYTE*)AllocMemory(slSizeDst);

  // compress there
  BOOL bOk = pCompressor->Pack(pubSrc, slSizeSrc, pubDst, slSizeDst, piParams);

  // if failed
  if (!bOk) {
    // report error
    FreeMemory(pubDst);
    ThrowF_t(TRANS("Error while packing a stream."));
  }

  // write the header to destination
  strmDst << slSizeSrc;
  strmDst << slSizeDst;

  // write the compressed data to destination
  strmDst.Write_t(pubDst, slSizeDst);
  FreeMemory(pubDst);
}

//! Unpack from stream.
void FCompressionManager::UnpackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, CompressionMethod eCompressionMethod, INDEX *piParams)
{
  CSyncLock slManager(&_csManager, TRUE);
  Func_CheckComprTypeId func(eCompressionMethod);

  TDynamicContainer<FCompressor> cFound;
  GetCompressors().Find(cFound, func);

  ASSERT(cFound.Count() > 0);
  UnpackStream_t(strmDst, strmSrc, cFound.Pointer(0), piParams);
}

//! Pack to stream.
void FCompressionManager::PackStream_t(CTStream &strmDst, CTMemoryStream &strmSrc, CompressionMethod eCompressionMethod, INDEX *piParams)
{
  CSyncLock slManager(&_csManager, TRUE);
  Func_CheckComprTypeId func(eCompressionMethod);

  TDynamicContainer<FCompressor> cFound;
  GetCompressors().Find(cFound, func);

  ASSERT(cFound.Count() > 0);
  PackStream_t(strmDst, strmSrc, cFound.Pointer(0), piParams);
}