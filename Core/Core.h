#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <conio.h>
#include <windows.h>
#endif

#include <Core/CoreApi.h>
#include <Core/Base/Types.h>
#include <Core/IO/Stream.h>

extern CORE_API void (*_pCallProgressHook_t)(FLOAT fProgress);
extern CORE_API HWND _hwndMain;
extern CORE_API BOOL _bFullScreen;
extern CORE_API BOOL _bAllocationArrayParanoiaCheck;

//
extern CORE_API BOOL _bConsoleApp;
extern CORE_API BOOL _bDedicatedServer;
extern CORE_API BOOL _bWorldEditorApp; // is this world edtior app
extern CORE_API FString _strLogFile;

extern CORE_API FString _strCoreBuild;

extern CORE_API FLOAT mth_fCSGEpsilon;

// some global stuff
CORE_API void SE_InitCore();
CORE_API void SE_EndCore();