/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/IO/Stream.h>
#include <Core/IO/FileName.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/Archive.h>
#include <Core/IO/ArchiveManager.h>

#ifdef _WIN32
#include <io.h>
#endif

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/DynamicStackArray.cpp>
#include <Core/Templates/NameTable.cpp>

extern TDynamicStackArray<FPath> _afnmBaseBrowseInc;
extern TDynamicStackArray<FPath> _afnmBaseBrowseExc;

class CDirToRead
{
  public:
    CListNode dr_lnNode;
    FString dr_strDir;
};

int qsort_CompareFPath(const void *elem1, const void *elem2 )
{
  const FPath &fnm1 = **(FPath **)elem1;
  const FPath &fnm2 = **(FPath **)elem2;
  return strcmp(fnm1, fnm2);
}

extern BOOL FileMatchesList(TDynamicStackArray<FPath> &afnm, const FPath &fnm);

void FillDirList_internal(const FPath &fnmBasePath,
  TDynamicStackArray<FPath> &afnm, const FPath &fnmDir, const FString &strPattern, BOOL bRecursive,
  TDynamicStackArray<FPath> *pafnmInclude, TDynamicStackArray<FPath> *pafnmExclude)
{
  // add the directory to list of directories to search
  CListHead lhDirs;
  CDirToRead *pdrFirst = new CDirToRead;
  pdrFirst->dr_strDir = fnmDir;
  lhDirs.AddTail(pdrFirst->dr_lnNode);

  // while the list of directories is not empty
  while (!lhDirs.IsEmpty())
  {
    // take the first one
    CDirToRead *pdr = LIST_HEAD(lhDirs, CDirToRead, dr_lnNode);
    FPath fnmDir = pdr->dr_strDir;
    delete pdr;

    // if the dir is not allowed
    if (pafnmInclude != NULL &&
      (!FileMatchesList(*pafnmInclude, fnmDir) || FileMatchesList(*pafnmExclude, fnmDir)) ) {
      // skip it
      continue;
    }
    
    // start listing the directory
    struct _finddata_t c_file; intptr_t hFile;
    hFile = _findfirst((fnmBasePath + fnmDir + "*").ConstData(), &c_file );
    
    // for each file in the directory
    for (
      BOOL bFileExists = hFile != -1; 
      bFileExists; 
      bFileExists = _findnext( hFile, &c_file ) == 0)
    {

      // if dummy dir (this dir, parent dir, or any dir starting with '.') then skip it
      if (c_file.name[0] == '.') {
        continue;
      }

      // get the file's filepath
      FPath fnm = fnmDir + c_file.name;

      // if it is a directory
      if (c_file.attrib & _A_SUBDIR) {
        // if recursive reading
        if (bRecursive) {
          // add it to the list of directories to search
          CDirToRead *pdrNew = new CDirToRead;
          pdrNew->dr_strDir = fnm + "\\";
          lhDirs.AddTail(pdrNew->dr_lnNode);
        }
      // if it matches the pattern then add that file
      } else if (strPattern == "" || fnm.Matches(strPattern)) {
        afnm.Push() = fnm;
      }
    }
  }
}


// make a list of all files in a directory
CORE_API void MakeDirList(
  TDynamicStackArray<FPath> &afnmDir, const FPath &fnmDir, const FString &strPattern, ULONG ulFlags)
{
  afnmDir.PopAll();
  BOOL bRecursive = ulFlags & DLI_RECURSIVE;
  BOOL bSearchCD  = ulFlags & DLI_SEARCHCD;

  // make one temporary array
  TDynamicStackArray<FPath> afnm;

  if (_fnmMod != "") {
    FillDirList_internal(_fnmApplicationPath, afnm, fnmDir, strPattern, bRecursive,
      &_afnmBaseBrowseInc, &_afnmBaseBrowseExc);

    if (bSearchCD) {
      FillDirList_internal(_fnmSharedPath, afnm, fnmDir, strPattern, bRecursive,
      &_afnmBaseBrowseInc, &_afnmBaseBrowseExc);
    }

    FillDirList_internal(_fnmApplicationPath+_fnmMod, afnm, fnmDir, strPattern, bRecursive, NULL, NULL);
  } else {
    FillDirList_internal(_fnmApplicationPath, afnm, fnmDir, strPattern, bRecursive, NULL, NULL);
    if (bSearchCD) {
      FillDirList_internal(_fnmSharedPath, afnm, fnmDir, strPattern, bRecursive, NULL, NULL);
    }
  }

  // for each file in zip archives
  FString strDirPattern = fnmDir;

  FOREACHINDYNAMICCONTAINER(_pArchiveMgr->_ctObjects, FArchive, itar)
  {
    FArchive *pArchive = &*itar;

    FOREACHINDYNAMICCONTAINER(pArchive->_ctFileInfos, FArchiveEntry, itze)
    {
      FArchiveEntry *pEntry = &*itze;

      const FPath &fnm = pEntry->GetName();
      
      // if not in this dir, skip it
      if (bRecursive) {
        if (!fnm.HasPrefix(strDirPattern)) {
          continue;
        }
      } else {
        if (fnm.FileDir() != fnmDir) {
          continue;
        }
      }

      // if doesn't match pattern then skip it
      if (strPattern != "" && !fnm.Matches(strPattern)) {
        continue;
      }

      // if mod is active, and the file is not in mod
      if (_fnmMod != "" && !pArchive->IsMod()) {
        // if it doesn't match base browse path then skip it
        if (!FileMatchesList(_afnmBaseBrowseInc, fnm) || FileMatchesList(_afnmBaseBrowseExc, fnm) ) {
          continue;
        }
      }

      // add that file
      afnm.Push() = fnm;
    }
  }

  // if no files then don't check for duplicates
  if (afnm.Count() == 0) {
    return;
  }

  // resort the array
  qsort(afnm.da_Pointers, afnm.Count(), sizeof(void*), qsort_CompareFPath);

  // for each file
  INDEX ctFiles = afnm.Count();

  for (INDEX iFile = 0; iFile<ctFiles; iFile++)
  {
    // if not same as last one then copy over to final array
    if (iFile == 0 || afnm[iFile] != afnm[iFile - 1]) {
      afnmDir.Push() = afnm[iFile];
    }
  }
}
