/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _WIN32
#include <io.h>
#endif

#include <Core/Base/Console.h>
#include <Core/Base/Shell.h>
#include <Core/IO/Stream.h>
#include <Core/IO/ArchiveEntry.H>
#include <Core/IO/Archive.h>
#include <Core/IO/ArchiveManager.H>
#include <Core/IO/FileIndexNode.h>

#include <Core/Templates/StaticStackArray.cpp>
#include <Core/Templates/DynamicStackArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

// default size of page used for stream IO operations (4Kb)
ULONG _ulPageSize = 0;

// include/exclude lists for base dir writing/browsing
TDynamicStackArray<FPath> _afnmBaseWriteInc;
TDynamicStackArray<FPath> _afnmBaseWriteExc;
TDynamicStackArray<FPath> _afnmBaseBrowseInc;
TDynamicStackArray<FPath> _afnmBaseBrowseExc;

// list of paths or patterns that are not included when making CRCs for network connection
// this is used to enable connection between different localized versions
TDynamicStackArray<FPath> _afnmNoCRC;

static FPath _fnmApp;

extern INDEX fil_bPreferZips = FALSE;

// load a filelist
static BOOL LoadFileList(TDynamicStackArray<FPath> &afnm, const FPath &fnmList)
{
  afnm.PopAll();

  try {
    CTFileStream strm;
    strm.Open_t(fnmList);

    while (!strm.AtEOF())
    {
      FString strLine;
      strm.GetLine_t(strLine);
      strLine.TrimSpacesLeft();
      strLine.TrimSpacesRight();

      if (strLine != "") {
        afnm.Push() = strLine;
      }
    }

    return TRUE;
  } catch(char *strError) {
    CWarningF("%s\n", strError);
    return FALSE;
  }
}

extern BOOL FileMatchesList(TDynamicStackArray<FPath> &afnm, const FPath &fnm)
{
  for (INDEX i = 0; i < afnm.Count(); i++)
  {
    if (fnm.Matches(afnm[i]) || fnm.HasPrefix(afnm[i])) {
      return TRUE;
    }
  }

  return FALSE;
}

void InitStreams(void)
{
  // obtain information about system
  SYSTEM_INFO siSystemInfo;
  GetSystemInfo(&siSystemInfo);
  // and remember page size
  _ulPageSize = siSystemInfo.dwPageSize*16;   // cca. 64kB on WinNT/Win95

  // keep a copy of path for setting purposes
  _fnmApp = _fnmApplicationPath;

  // if no mod defined yet
  if (_fnmMod == "") {
    // check for 'default mod' file
    LoadStringVar(FString("DefaultMod.txt"), _fnmMod);
  }

  CInfoF(TRANS("Current mod: %s\n"), _fnmMod == "" ? TRANS("<none>") : (FString&)_fnmMod);

  // if there is a mod active
  if (_fnmMod != "")
  {
    // load mod's include/exclude lists
    CInfoF(TRANS("Loading mod include/exclude lists...\n"));
    BOOL bOK = FALSE;
    bOK |= LoadFileList(_afnmBaseWriteInc , FString("BaseWriteInclude.lst"));
    bOK |= LoadFileList(_afnmBaseWriteExc , FString("BaseWriteExclude.lst"));
    bOK |= LoadFileList(_afnmBaseBrowseInc, FString("BaseBrowseInclude.lst"));
    bOK |= LoadFileList(_afnmBaseBrowseExc, FString("BaseBrowseExclude.lst"));

    // if none found
    if (!bOK) {
      // the mod is not valid
      _fnmMod = FString("");
      CErrorF(TRANS("Error: MOD not found!\n"));
    // if mod is ok
    } else {
      // remember mod name (the parameter that is passed on cmdline)
      _strModName = _fnmMod;
      _strModName.DeleteChar(_strModName.Length() - 1);
      _strModName = FPath(_strModName).FileName();
    }
  }

  // find eventual extension for the mod's dlls
  _strModExt = "";
  LoadStringVar(FString("ModExt.txt"), _strModExt);

  // For each group file in base directory...
  _pArchiveMgr->RegisterDirectory(_fnmApplicationPath +  "Content\\", FFileIndexNode::FF_ROOT);

  // If there is a mod active...
  if (_fnmMod != "") {
    _pArchiveMgr->RegisterDirectory(_fnmApplicationPath + _fnmMod, FFileIndexNode::FF_MOD);
    _pArchiveMgr->RegisterDirectory(_fnmApplicationPath + _fnmMod + "Content\\", FFileIndexNode::FF_MOD);
  }

  // If there is a shared path...
  if (_fnmSharedPath != "") {
    _pArchiveMgr->RegisterDirectory(_fnmSharedPath + "Content\\", FFileIndexNode::FF_ROOT|FFileIndexNode::FF_SHARED);

    // if there is a mod active
    if (_fnmMod != "") {
      _pArchiveMgr->RegisterDirectory(_fnmSharedPath + _fnmMod + "Content\\", FFileIndexNode::FF_ROOT|FFileIndexNode::FF_SHARED);
    }
  }

  _pArchiveMgr->SearchArchives();

  CInfoF("\n");

  LoadFileList(_afnmNoCRC, CTFILENAME("Data\\NoCRC.lst"));

  _pShell->SetINDEX("sys_iCPUMisc", 1);
}

void EndStreams(void)
{
}

void UseApplicationPath(void) 
{
  _fnmApplicationPath = _fnmApp;
}

void IgnoreApplicationPath(void)
{
  _fnmApplicationPath = FString("");
}

// Test if a file exists.
BOOL FileExists(const FPath &fnmFile)
{
  // if no file then it doesn't exist
  if (fnmFile == "") {
    return FALSE;
  }

  // try to open the file for reading
  try {
    CTFileStream strmFile;
    strmFile.Open_t(fnmFile);

    // if successful, it means that it exists,
    return TRUE;

  // if failed, it means that it doesn't exist
  } catch (char *strError) {
    (void) strError;
    return FALSE;
  }
}

// Test if a file exists for writing. 
// (this is can be diferent than normal FileExists() if a mod uses basewriteexclude.lst
BOOL FileExistsForWriting(const FPath &fnmFile)
{
  // if no file
  if (fnmFile == "") {
    // it doesn't exist
    return FALSE;
  }

  // expand the filename to full path for writing
  FPath fnmFullFileName;
  INDEX iFile = ExpandFilePath(EFP_WRITE, fnmFile, fnmFullFileName);

  // check if it exists
  FILE *f = fopen(fnmFullFileName, "rb");

  if (f != NULL) { 
    fclose(f);
    return TRUE;
  } else {
    return FALSE;
  }
}

// Get file timestamp
SLONG GetFileTimeStamp_t(const FPath &fnm)
{
  // expand the filename to full path
  FPath fnmExpanded;
  INDEX iFile = ExpandFilePath(EFP_READ, fnm, fnmExpanded);

  if (iFile != EFP_FILE) {
    return FALSE;
  }

  int file_handle;

  // try to open file for reading
  file_handle = _open(fnmExpanded, _O_RDONLY | _O_BINARY);
  if (file_handle == -1) {
    ThrowF_t(TRANS("Cannot open file '%s' for reading"), FString(fnm));
    return -1;
  }

  struct stat statFileStatus;

  // get file status
  fstat(file_handle, &statFileStatus);
  _close(file_handle);

  ASSERT(statFileStatus.st_mtime <= time(NULL));
  return statFileStatus.st_mtime;
}

// Get CRC32 of a file
ULONG GetFileCRC32_t(const FPath &fnmFile) // throw char *
{
  // open the file
  CTFileStream fstrm;
  fstrm.Open_t(fnmFile);

  // return the checksum
  return fstrm.GetStreamCRC32_t();
}

// Test if a file is read only (also returns FALSE if file does not exist)
BOOL IsFileReadOnly(const FPath &fnm)
{
  // expand the filename to full path
  FPath fnmExpanded;
  INDEX iFile = ExpandFilePath(EFP_READ, fnm, fnmExpanded);

  if (iFile != EFP_FILE) {
    return FALSE;
  }

  int file_handle;

  // try to open file for reading
  file_handle = _open(fnmExpanded, _O_RDONLY | _O_BINARY);

  if (file_handle == -1) {
    return FALSE;
  }

  struct stat statFileStatus;

  // get file status
  fstat(file_handle, &statFileStatus);
  _close(file_handle);
  ASSERT(statFileStatus.st_mtime <= time(NULL));

  return !(statFileStatus.st_mode & _S_IWRITE);
}

// Delete a file
BOOL RemoveFile(const FPath &fnmFile)
{
  // expand the filename to full path
  FPath fnmExpanded;
  INDEX iFile = ExpandFilePath(EFP_WRITE, fnmFile, fnmExpanded);

  if (iFile == EFP_FILE) {
    int ires = remove(fnmExpanded);
    return ires == 0;
  } else {
    return FALSE;
  }
}

static BOOL IsFileReadable_internal(FPath &fnmFullFileName)
{
  FILE *pFile = fopen(fnmFullFileName, "rb");

  if (pFile != NULL) {
    fclose(pFile);
    return TRUE;
  } else {
    return FALSE;
  }
}

// check for some file extensions that can be substituted
static BOOL SubstExt_internal(FPath &fnmFullFileName)
{
  if (fnmFullFileName.FileExt() == ".mp3") {
    fnmFullFileName = fnmFullFileName.NoExt() + ".ogg";
    return TRUE;

  } else if (fnmFullFileName.FileExt() == ".ogg") {
    fnmFullFileName = fnmFullFileName.NoExt() + ".mp3";
    return TRUE;
    
  } else if (fnmFullFileName.FileExt() == ".smc") {
    fnmFullFileName = fnmFullFileName.NoExt() + ".bmc";
    return TRUE;
    
  } else if (fnmFullFileName.FileExt() == ".bmc") {
    fnmFullFileName = fnmFullFileName.NoExt() + ".smc";
    return TRUE;
    
  } else {
    return TRUE;
  }
}

static INDEX ExpandFilePath_read(ULONG ulType, const FPath &fnmFile, FPath &fnmExpanded)
{
  TDynamicContainer<FArchiveEntry> arentries;
  const INDEX ctFound = _pArchiveMgr->QueryFiles(fnmFile, arentries, TRUE, 1); 

  // if a mod is active
  if (_fnmMod != "") {
    // first try in the mod's dir
    if (!fil_bPreferZips) {
      fnmExpanded = _fnmApplicationPath + _fnmMod + fnmFile;
      if (IsFileReadable_internal(fnmExpanded)) {
        return EFP_FILE;
      }
    }

    // if not disallowing zips
    if (!(ulType & EFP_NOZIPS)) {
      // if exists in mod's zip then use that one
      if (ctFound > 0 && arentries.Pointer(0)) {
        fnmExpanded = fnmFile;
        return EFP_MODZIP;
      }
    }

    // try in the mod's dir after
    if (fil_bPreferZips) {
      fnmExpanded = _fnmApplicationPath + _fnmMod + fnmFile;

      if (IsFileReadable_internal(fnmExpanded)) {
        return EFP_FILE;
      }
    }
  }

  // try in the app's base dir
  if (!fil_bPreferZips) {

    FPath fnmAppPath = _fnmApplicationPath;
    fnmAppPath.SetAbsolutePath();

    if (fnmFile.HasPrefix(fnmAppPath)) {
      fnmExpanded = fnmFile;
    } else {
      fnmExpanded = _fnmApplicationPath + fnmFile;
    }

    if (IsFileReadable_internal(fnmExpanded)) {
      return EFP_FILE;
    }
  }

  // if not disallowing zips
  if (!(ulType & EFP_NOZIPS))
  {
    // if exists in any zip then use that one
    if (ctFound > 0) {
      fnmExpanded = fnmFile;
      return EFP_BASEZIP;
    }
  }

  // try in the app's base dir
  if (fil_bPreferZips) {
    fnmExpanded = _fnmApplicationPath + fnmFile;

    if (IsFileReadable_internal(fnmExpanded)) {
      return EFP_FILE;
    }
  }

  // finally, try in the CD path
  if (_fnmSharedPath != "")
  {
    // if a mod is active then first try in the mod's dir
    if (_fnmMod != "")
    {
      fnmExpanded = _fnmSharedPath + _fnmMod + fnmFile;

      if (IsFileReadable_internal(fnmExpanded)) {
        return EFP_FILE;
      }
    }

    fnmExpanded = _fnmSharedPath+fnmFile;

    if (IsFileReadable_internal(fnmExpanded)) {
      return EFP_FILE;
    }
  }

  return EFP_NONE;
}

// Expand a file's filename to full path
INDEX ExpandFilePath(ULONG ulType, const FPath &fnmFile, FPath &fnmExpanded)
{
  FPath fnmFileAbsolute = fnmFile;
  fnmFileAbsolute.SetAbsolutePath();

  // if writing
  if (ulType & EFP_WRITE) {
    // if should write to mod dir then do that
    if (_fnmMod != "" && (!FileMatchesList(_afnmBaseWriteInc, fnmFileAbsolute) || FileMatchesList(_afnmBaseWriteExc, fnmFileAbsolute))) {

      fnmExpanded = _fnmApplicationPath+_fnmMod+fnmFileAbsolute;
      fnmExpanded.SetAbsolutePath();

      return EFP_FILE;

    // if should not write to mod dir then write to base dir
    } else {
      fnmExpanded = _fnmApplicationPath+fnmFileAbsolute;
      fnmExpanded.SetAbsolutePath();

      return EFP_FILE;
    }

  // if reading
  } else if (ulType & EFP_READ) {

    // check for expansions for reading
    INDEX iRes = ExpandFilePath_read(ulType, fnmFileAbsolute, fnmExpanded);

    // if not found then check for some file extensions that can be substituted
    if (iRes == EFP_NONE) {
      FPath fnmSubst = fnmFileAbsolute;

      if (SubstExt_internal(fnmSubst)) {
        iRes = ExpandFilePath_read(ulType, fnmSubst, fnmExpanded);
      }
    }

    fnmExpanded.SetAbsolutePath();

    if (iRes != EFP_NONE) {
      return iRes;
    }

  // in other cases
  } else  {
    ASSERT(FALSE);
    fnmExpanded = _fnmApplicationPath+fnmFileAbsolute;
    fnmExpanded.SetAbsolutePath();
    return EFP_FILE;
  }

  fnmExpanded = _fnmApplicationPath+fnmFileAbsolute;
  fnmExpanded.SetAbsolutePath();

  return EFP_NONE;
}