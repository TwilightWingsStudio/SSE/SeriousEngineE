/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/IO/FileName.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/FileDevice.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/ZipArchive.h>
#include <Core/IO/ZipArchive_internal.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Console.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

// convert slashes to backslashes in a file path
static void ConvertSlashes(char *p)
{
  while (*p != 0)
  {
    if (*p == '/') {
      *p = '\\';
    }

    p++;
  }
}

static CompressionMethod ConvertZipComressionMethod(int iMethod)
{
  switch (iMethod)
  {
    case ZIP_COMPRESSION_STORE: return COMPRESSION_METHOD_STORE;
    case ZIP_COMPRESSION_DEFLATE: return COMPRESSION_METHOD_DEFLATE;
    case ZIP_COMPRESSION_LZMA: return COMPRESSION_METHOD_LZMA;
  }

  return COMPRESSION_METHOD_INVALID;
}

static void ReadEndOfDir_t(SZipEndOfDir &eod, FDataStream &strm)
{
  //strm.Read(&eod, sizeof(SZipEndOfDir));

  strm >> eod.DiskNo >> eod.DirStartDiskNo >> eod.EntriesInDirOnThisDisk;
  strm >> eod.EntriesInDir >> eod.SizeOfDir >> eod.DirOffsetInFile;
  strm >> eod.CommentLenght;
}

static void ReadFileHeader_t(SZipFileHeader &header, FDataStream &strm)
{
  strm >> header.VersionMadeBy >> header.VersionToExtract >> header.GPBFlag >> header.CompressionMethod;
  strm >> header.ModFileTime >> header.ModFileDate >> header.CRC32 >> header.CompressedSize >> header.UncompressedSize;
  strm >> header.FileNameLen >> header.ExtraFieldLen >> header.FileCommentLen >> header.DiskNoStart;
  strm >> header.InternalFileAttributes >> header.ExternalFileAttributes >> header.LocalHeaderOffset;
}

static void ReadLocalFileHeader_t(SZipLocalFileHeader &lfh, FDataStream &strm)
{
  //strm.Read(&lfh, sizeof(SZipLocalFileHeader));

  strm >> lfh.VersionToExtract >> lfh.GPBFlag >> lfh.CompressionMethod;
  strm >> lfh.ModFileTime >> lfh.ModFileDate;
  strm >> lfh.CRC32 >> lfh.CompressedSize >> lfh.UncompressedSize;
  strm >> lfh.FileNameLen >> lfh.ExtraFieldLen;
}

static void RegisterEntries(FZipLoaderContext &ctx)
{
  // Finally register entries.
  for (int i = 0; i < ctx.Files.Count(); i++)
  {
    const SZipFileHeader &header = ctx.Files[i];

    FArchiveEntry *pNew = new FArchiveEntry(ctx.pArchive, header.FileName);
    pNew->SetOffset (ctx.Offsets[i]);
    pNew->SetChecksum(header.CRC32);
    pNew->SetCompressedSize(header.CompressedSize);
    pNew->SetUncompressedSize(header.UncompressedSize);
    pNew->SetCompressionMethod(ConvertZipComressionMethod(header.CompressionMethod));

    ctx.pArchive->RegisterEntry(pNew);
  }
}

void FZipArchiveLoader::Load_t(FArchive *pArchive, FDataStream &strm) const
{
  FReadWriteDevice *pDevice = strm.Device(); 

  FZipLoaderContext ctx(pArchive);

  strm.SetByteOrder(FDataStream::BO_LITTLEENDIAN);

  // start at the end of file, minus expected minimum overhead
  int iPos = pDevice->Size() - sizeof(long) - sizeof(SZipEndOfDir) + 2;

  // do not search more than 128k (should be around 65k at most)
  int iMinPos = iPos - 128 * 1024;
  if (iMinPos < 0) {
    iMinPos = 0;
  }

  SZipEndOfDir eod;
  BOOL bEODFound = FALSE;

  // while not at beginning
  for (; iPos > iMinPos; iPos--)
  {
    FByteArray baEodTag('\0', 4);

    // read signature
    strm.Device()->Seek(iPos);
    strm.Read(baEodTag.Data(), 4);

    // if this is the sig
    if (baEodTag == "PK\x05\x06") // 0x06054b50
    {
      // read directory end
      FByteArray baDirEnd('\x00', 18);
      strm.Read(baDirEnd.Data(), 18);

      FDataStream strmDirEnd(baDirEnd);
      strmDirEnd.SetByteOrder(strm.GetByteOrder());
      ReadEndOfDir_t(eod, strmDirEnd);

      // if multi-volume zip then fail
      if (eod.DiskNo != 0 || eod.DirStartDiskNo != 0
        || eod.EntriesInDirOnThisDisk != eod.EntriesInDir) {
        ThrowF_t(TRANS("%s : Multi-volume zips are not supported"), pArchive->GetName().ConstData());
      }                          

      // check against empty zips then fail
      if (eod.EntriesInDir <= 0) {
        ThrowF_t(TRANS("%s : Empty zip"), pArchive->GetName().ConstData());
      }                                                     

      // all ok
      bEODFound = TRUE;
      break;
    }
  }

  // if eod not found then fail
  if (!bEODFound) {
    ThrowF_t(TRANS("%s : Cannot find 'end of central directory'"), pArchive->GetName().ConstData());
  }

  // go to the beginning of the central dir
  ctx.Headers.New(eod.EntriesInDir);

  strm.Device()->Seek(eod.DirOffsetInFile);

  // for each file
  for (int i = 0; i < ctx.Headers.Count(); i++)
  {
    SZipFileHeader &header = ctx.Headers[i];

    FByteArray baEntryTag('\0', 4);
    strm.Read(baEntryTag.Data(), 4);

    // if this is not the expected sig then fail
    if (baEntryTag != "PK\x01\x02") { // 0x02014b50
      ThrowF_t(TRANS("%s : Wrong signature for 'file header' number %d'"), pArchive->GetName().ConstData(), i);
    }

    FByteArray baHeader('\x00', 42); // 42, here is answer!
    strm.Read(baHeader.Data(), 42);

    FDataStream strmHeader(baHeader);
    strmHeader.SetByteOrder(strm.GetByteOrder());
    ReadFileHeader_t(header, strmHeader);

    // read the filename
    FByteArray baFileName('\0', 512);

    if (header.FileNameLen > baFileName.Size()) {
      ThrowF_t(TRANS("%s : Too long filepath in zip"), pArchive->GetName().ConstData());
    }

    if (header.FileNameLen <= 0) {
      ThrowF_t(TRANS("%s : Invalid filepath length in zip"), pArchive->GetName().ConstData());
    }

    strm.Read(baFileName.Data(), header.FileNameLen);
    header.FileName.PrintF("%s", baFileName.ConstData());

    // skip eventual comment and extra fields
    if (header.FileCommentLen + header.ExtraFieldLen > 0) {
      strm.Skip(header.FileCommentLen + header.ExtraFieldLen);
    }
  }

  // Find files and firectories.
  for (int j = 0; j < ctx.Headers.Count(); j++)
  {
    SZipFileHeader &header = ctx.Headers[j];

    // if the file is directory
    if (header.FileName.Data()[header.FileName.Length() - 1] == '/') {
      // check size
      if (header.UncompressedSize != 0 || header.CompressedSize != 0) {
        ThrowF_t(TRANS("%s/%s : Invalid directory"), pArchive->GetName().ConstData(), header.FileName.ConstData());
      }

    // if the file is real file
    } else {
      // convert filename
      ConvertSlashes(header.FileName.Data());
      ctx.Files.Add(&header);
    }
  }

  ctx.LocalHeaders.New(ctx.Files.Count());
  ctx.Offsets.New(ctx.Files.Count());

  // Read local file headers.
  for (int k = 0; k < ctx.LocalHeaders.Count(); k++)
  {
    SZipFileHeader &header = ctx.Files[k];
    SZipLocalFileHeader &lfh = ctx.LocalHeaders[k];

    // seek to the local header of the entry
    pDevice->Seek(header.LocalHeaderOffset);
    
    // read the sig
    FByteArray baFileHeaderTag('\0', 4);
    pDevice->Read((char *)baFileHeaderTag.Data(), 4);

    // if this is not the expected sig then fail
    if (baFileHeaderTag != "PK\x03\x04") { // 0x04034b50
      ThrowF_t(TRANS("%s/%s : Wrong signature for 'local file header'"), pArchive->GetName().ConstData(), header.FileName.ConstData());
    }

    FByteArray baHeader('\x00', 26);
    strm.Read(baHeader.Data(), 26);

    FDataStream strmHeader(baHeader);
    strmHeader.SetByteOrder(strm.GetByteOrder());
    ReadLocalFileHeader_t(lfh, strmHeader);

    // determine exact compressed data position
    ctx.Offsets[k] = pDevice->Pos() + lfh.FileNameLen + lfh.ExtraFieldLen;
  }

  RegisterEntries(ctx);
}

bool FZipArchiveLoader::IsCompressionSupported(enum CompressionMethod eMethod) const
{
  switch (eMethod)
  {
    case COMPRESSION_METHOD_STORE:
    case COMPRESSION_METHOD_DEFLATE:
    case COMPRESSION_METHOD_LZMA:
      return true;
  }

  return false;
}

void FZipArchiveLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".zip";
  astr.Push() = ".gro";
  astr.Push() = ".pk3";
}