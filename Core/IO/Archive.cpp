/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>

#include <Core/Math/Functions.h>

#include <Core/IO/FileName.h>
#include <Core/IO/FileDevice.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/Archive.h>

#include <Core/Compression/CompressionManager.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

extern ULONG CalcArchiveEntryChecksum_t(FArchiveEntry *pEntry, FReadWriteDevice *pDevice)
{
  const SLONG slBlockSize = 4096;
  FByteArray baBlock('\0', slBlockSize);

  ULONG ulCRC;
  CRC_Start(ulCRC);

  if (!pDevice->Seek(pEntry->GetOffset())) {
    ThrowF_t(TRANS("Unable to seek archive entry!"));
  }

  // for each block in file
  for (SLONG slPos = 0; slPos < pEntry->GetUncompressedSize(); slPos += slBlockSize)
  {
    SLONG slThisBlockSize = Min(pEntry->GetUncompressedSize() - slPos, slBlockSize);

    pDevice->Read((char *)baBlock.Data(), slThisBlockSize); // read the block
    CRC_AddBlock(ulCRC, baBlock.Data(), slThisBlockSize); // checksum it
  }

  CRC_Finish(ulCRC);
  
  return ulCRC;
}

void FArchive::CalculateChecksums_t(FReadWriteDevice *pDevice)
{
  FOREACHINDYNAMICCONTAINER(_ctFileInfos, FArchiveEntry, itae)
  {
    FArchiveEntry *pEntry = &itae.Current();
    pEntry->SetChecksum(CalcArchiveEntryChecksum_t(pEntry, pDevice));
  }
}

void FArchive::Initialize()
{
  _pDevice = new FFileDevice;
  _idArchive = MAX_SLONG;
  _ntFileInfos.SetAllocationParameters(50, 2, 2);
  _ulFlags = 0;
}

FArchive::~FArchive()
{
  if (_pDevice) {
    ASSERT(!_pDevice->IsOpen());
    delete _pDevice;
  }
}

ULONG FArchive::GetFileCount() const
{
  return _ctFileInfos.Count();
}

void FArchive::RegisterEntry(FArchiveEntry *pEntry)
{
  _ctFileInfos.Add(pEntry);
  _ntFileInfos.Add(pEntry);
}

FArchiveEntry* FArchive::FindEntry(const FPath &fnm)
{
  return _ntFileInfos.Find(fnm);
}

void FArchive::Close()
{
  ASSERT(_pDevice->IsOpen());
  _pDevice->Close();
}

BOOL FArchive::IsOpen() const
{
  return _pDevice->IsOpen(); //_pArchiveFile != nullptr;
}

void FArchive::SetName(const FPath &fnm)
{
  _fnmName = fnm;

  if (_pDevice->GetType() == FReadWriteDevice::TYPE_FILE) {
    FFileDevice *pFileDevice = static_cast<FFileDevice *>(_pDevice);
    pFileDevice->SetFilename(fnm);
  }
}

void FArchive::Prepare_t()
{
  if (!_pDevice->IsOpen()) {
    // if failed to open it
    if (!_pDevice->Open(FReadWriteDevice::OM_READONLY)) {
      ThrowF_t(TRANS("%s : Cannot open: %s"), GetName().ConstData(), strerror(errno));
    }
  }
}

//! Read file from archive.
void FArchive::ReadFile_t(const FArchiveEntry &ae, UBYTE *pBuffer)
{
  ASSERT(_pDevice->IsOpen());

  const CompressionMethod eMethod = ae.GetCompressionMethod();
  INDEX aiParams[] = { -15 };

  if (!IsCompressionSupported(eMethod)) {
    ThrowF_t(TRANS("%s/%s : Compression (%d) is unsupported by archive loader!"), GetName().ConstData(), ae.GetName().ConstData(), eMethod);
  }

  _pDevice->Seek(ae.GetOffset());

  if (eMethod == COMPRESSION_METHOD_STORE) {
    _pDevice->Read((char *)pBuffer, ae.GetUncompressedSize());
    return;
  }

  FByteArray baCompressed('\x00', ae.GetCompressedSize());
  _pDevice->Read((char *)baCompressed.Data(), ae.GetCompressedSize());

  SLONG slDstSize = ae.GetUncompressedSize();
  _pCompressionMgr->Unpack(pBuffer, slDstSize, baCompressed.Data(), baCompressed.Size(), eMethod, &aiParams[0]);
}

void FArchive::ReadFile_t(const FPath &fnm, UBYTE *pBuffer)
{
  FArchiveEntry *pEntry = FindEntry(fnm);
  ReadFile_t(*pEntry, pBuffer);
}