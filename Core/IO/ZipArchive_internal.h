#pragma pack(1)

// one file in central dir
#define SIGNATURE_FH 0x02014b50
struct SZipFileHeader
{
  SWORD VersionMadeBy;
  SWORD VersionToExtract;
  SWORD GPBFlag;
  SWORD CompressionMethod;
  SWORD ModFileTime;
  SWORD ModFileDate;
  SLONG CRC32;
  SLONG CompressedSize;
  SLONG UncompressedSize;
  SWORD FileNameLen;
  SWORD ExtraFieldLen;
  SWORD FileCommentLen;
  SWORD DiskNoStart;
  SWORD InternalFileAttributes;
  SLONG ExternalFileAttributes;
  SLONG LocalHeaderOffset;

// follows:
  FPath FileName;
//  filename (variable size)
//  extra field (variable size)
//  file comment (variable size)
};

// at the end of entire zip file
#define SIGNATURE_EOD 0x06054b50
struct SZipEndOfDir
{
  SWORD DiskNo;
  SWORD DirStartDiskNo;
  SWORD EntriesInDirOnThisDisk;
  SWORD EntriesInDir;
  SLONG SizeOfDir;
  SLONG DirOffsetInFile;
  SWORD CommentLenght;
// follows: 
//  zipfile comment (variable size)
};

// before each file in the zip
#define SIGNATURE_LFH 0x04034b50
struct SZipLocalFileHeader
{
  SWORD VersionToExtract;
  SWORD GPBFlag;
  SWORD CompressionMethod;
  SWORD ModFileTime;
  SWORD ModFileDate;
  SLONG CRC32;
  SLONG CompressedSize;
  SLONG UncompressedSize;
  SWORD FileNameLen;
  SWORD ExtraFieldLen;

// follows:
//	filename (variable size)
//  extra field (variable size)
};

class FZipLoaderContext : public FArchiveLoaderContext
{
  public:
    //! Cental directory headers.
    TStaticArray<SZipFileHeader> Headers;
    TStaticArray<SZipLocalFileHeader> LocalHeaders;
    TStaticArray<ULONG> Offsets;
    TDynamicContainer<SZipFileHeader> Files;

    //! Constructor.
    FZipLoaderContext(FArchive *p)
    {
      pArchive = p;
    }
};

enum ZipCompressionMethod
{
  //! Stored as is without compression.
  ZIP_COMPRESSION_STORE = 0,

  //! Shrink/UnShrink (Legacy).
  ZIP_COMPRESSION_METHOD_SHRINK = 1,

  //! Legacy!
  ZIP_COMPRESSION_METHOD_EXPAND_0 = 2,

  //! Legacy!
  ZIP_COMPRESSION_METHOD_EXPAND_1 = 3,

  //! Legacy!
  ZIP_COMPRESSION_METHOD_EXPAND_2 = 4,

  //! Legacy!
  ZIP_COMPRESSION_METHOD_EXPAND_3 = 5,

  //! Implode (Legacy).
  ZIP_COMPRESSION_METHOD_IMPLODE = 6,
  
  //! Use zlib to decompress.
  ZIP_COMPRESSION_DEFLATE = 8,

  //! Deflate64.
  ZIP_COMPRESSION_DEFLATE64 = 9,

  //! bzip2.
  ZIP_COMPRESSION_BZIP2 = 12,
  
  //! LZMA compression.
  ZIP_COMPRESSION_LZMA = 14,

  //! PPMd version I, Rev 1.
  ZIP_COMPRESSION_PPMd = 98,
  
  //! AES encryption.
  ZIP_COMPRESSION_ENCRYPTED = 99,
};

#pragma pack()