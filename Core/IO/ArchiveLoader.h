/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ARCHIVELOADER_H
#define SE_INCL_ARCHIVELOADER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/FileLoader.h>
#include <Core/IO/Archive.h>

class CORE_API FArchiveLoader : public FFileLoader
{
  public:
    //! Get file info.
    virtual void Load_t(FArchive *pArchive, FDataStream &strm) const = 0;

    //! Check if compression method supported by format.
    virtual bool IsCompressionSupported(enum CompressionMethod eMethod) const = 0;

    //! Check if part or not.
    virtual bool IsMultiVolume(const FPath &fnm) const
    {
      return false;
    }

    //! Determine archive format.
    virtual ArchiveFormat GetFormat() const
    {
      return ARCHIVE_FORMAT_INVALID;
    }
};

class CORE_API FArchiveLoaderContext
{
  public:
    FArchive *pArchive;

  public:
    inline FArchive *GetArchive()
    {
      return pArchive;
    }
};

#endif /* include-once check */