#pragma pack(1)

struct SWadHeader
{
  char tag[4];
  ULONG NumberOfLumps;
  ULONG InfoTableOffset;
};

struct SWadFileEntry
{
  ULONG	Offset;
  ULONG CompressedSize;
  ULONG UncompressedSize;
  UBYTE Type;
  UBYTE CompressionMethod;
  UBYTE Padding[2];
  char Name[16];
};

class FWadLoaderContext : public FArchiveLoaderContext
{
  public:
    TStaticArray<SWadFileEntry> Lumps;

    FWadLoaderContext(FArchive *p)
    {
      pArchive = p;
    }
};

struct SWadDoomFileEntry
{
  ULONG	Offset;
  ULONG Size;
  char Name[8];
};

//! There are entries with different types.
enum EWadQuakeEntryType
{
  //! Color palette.
  WAD_ENTRY_QUAKE_PALETTE  = 64,

  //! Pictures for status bar.
  WAD_ENTRY_QUAKE_QPIC    = 66,

  //! Mip Texture.
  WAD_ENTRY_QUAKE_MIPTEX  = 68,

  //! Console picture (font).
  WAD_ENTRY_QUAKE_FONT    = 69,
};

enum EWadGoldSrcEntryType
{
  WAD_ENTRY_GOLDSRC_QPIC = 66,
  WAD_ENTRY_GOLDSRC_MIPTEX = 67,
  WAD_ENTRY_GOLDSRC_FONT = 69,
};

//! Lumps withing WAD have namespace.
enum EWadDoomLumpNamespace
{
  //! Outside any markers.
  WAD_DOOM_NAMESPACE_GLOBAL = 0,

  //! Sprite image type.
  WAD_DOOM_NAMESPACE_SPITES,

  //! Flat image type.
  WAD_DOOM_NAMESPACE_FLATS,

  //! Optional.
  WAD_DOOM_NAMESPACE_COLORMAPS,

  //! Action Script Library.
  WAD_DOOM_NAMESPACE_ASCLIBRARY,

  //! Image in any supported format.
  WAD_DOOM_NAMESPACE_NEWTEXTURES,

  //! Strife conversation scripts.
  WAD_DOOM_NAMESPACE_STRIFEVOICES,

  //! Hi-Resolution image.
  WAD_DOOM_NAMEPSACE_HIRES,
  
  //! Voxel models.
  WAD_DOOM_NAMESPACE_VOXELS,
};

struct SWadDoomLumpInfo
{
  int Namespace;

  SWadDoomLumpInfo()
  {
    Namespace = WAD_DOOM_NAMESPACE_GLOBAL;
  }
};

struct SWadMarker
{
	bool isClosing;
	unsigned int Index;
};

class FWadDoomLoaderContext : public FArchiveLoaderContext
{
  public:
    TStaticArray<SWadDoomFileEntry> Lumps;
    TStaticArray<SWadDoomLumpInfo> LumpInfos;

    FWadDoomLoaderContext(FArchive *p)
    {
      pArchive = p;
    }
};

static const char *WadGetNamespaceName(int ns)
{
  switch (ns)
  {
    case WAD_DOOM_NAMESPACE_SPITES: return "Sprites";
    case WAD_DOOM_NAMESPACE_FLATS: return "Flats";
    case WAD_DOOM_NAMESPACE_COLORMAPS: return "Colormap";
    case WAD_DOOM_NAMESPACE_ASCLIBRARY: return "ACS";
    case WAD_DOOM_NAMESPACE_NEWTEXTURES: return "Textures";
    case WAD_DOOM_NAMESPACE_STRIFEVOICES: return "Voices";
    case WAD_DOOM_NAMEPSACE_HIRES: return "HiRes";
    case WAD_DOOM_NAMESPACE_VOXELS: return "Voxels";
  }

  return "<Global>";
}

//! Compression applied on lump.
enum EWadCompressionMethod
{
  //! Stored as is.
  WAD_COMPRESSION_STORE = 0,

  //! LZSS.
  WAD_COMPRESSION_LZSS,
};

#pragma pack()