/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */


#ifndef SE_INCL_STREAM_H
#define SE_INCL_STREAM_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Translation.h>
#include <Core/Base/Lists.h>
#include <Core/Base/String.h>
#include <Core/IO/FileName.h>
#include <Core/IO/PathDictionary.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Templates/DynamicStackArray.h>
#include <Core/IO/DataStream.h>

//! Maximum length of file that can be saved (default: 8Mb).
CORE_API extern ULONG _ulMaxLengthOfSavingFile;

#define CTSTREAM_BEGIN CTStream::EnableStreamHandling(); __try
#define CTSTREAM_END __except( CTStream::ExceptionFilter( GetExceptionCode(),\
                                                          GetExceptionInformation()) )\
  {\
     CTStream::ExceptionFatalError();\
  }; CTStream::DisableStreamHandling();

#define CID_LENGTH 4

//! Chunk ID class
class CORE_API CChunkID
{
  public:
    char cid_ID[CID_LENGTH+1];

  public:
    //! Default constructor and string constructor.
    inline CChunkID(const char *strString = "    ");

    /* Comparison operator. */
    inline int operator==(const CChunkID &cidOther) const;
    inline int operator!=(const CChunkID &cidOther) const;
    inline operator const char *(void) const { return cid_ID; }
    inline operator char *(void) { return cid_ID; }
};

// inline implementations
//! Default constructor and string constructor.
inline CChunkID::CChunkID(const char *strString /*= "    "*/)
{
  ASSERT(strlen(strString)==CID_LENGTH);
  memcpy(cid_ID, strString, CID_LENGTH+1);
};

/* Comparison operator. */
inline int CChunkID::operator==(const CChunkID &cidOther) const {
  return (*((ULONG *)&cid_ID[0]) == *((ULONG *)&cidOther.cid_ID[0]));
};

inline int CChunkID::operator!=(const CChunkID &cidOther) const {
  return (*((ULONG *)&cid_ID[0]) != *((ULONG *)&cidOther.cid_ID[0]));
};

//! CroTeam stream class - abstract base class.
class CORE_API CTStream : public IDataStream
{  
  public:
    CListNode strm_lnListNode;  // for linking into main library's list of opened streams
    ULONG strm_ulFlags; // [SEE] Multi Purpose Serialization Flags
    FString strm_strStreamDescription; // descriptive string
    
    FPathDictionary &strm_Dictionary;

    /* Throw an exception of formatted string. */
    void Throw_t(char *strFormat, ...); // throw char *

  public:
    // modes for opening streams
    enum OpenMode {
      OM_READ  = 1,
      OM_WRITE = 2,
      OM_READTEXT  = OM_READ ,
      OM_WRITETEXT = OM_WRITE,
      OM_READBINARY  = OM_READ ,
      OM_WRITEBINARY = OM_WRITE,
    };

    // direction for seeking
    enum SeekDir {
      SD_BEG = SEEK_SET,
      SD_END = SEEK_END,
      SD_CUR = SEEK_CUR,
    };

    //! Static function enable stream handling.
    static void EnableStreamHandling(void);

    //! Static function disable stream handling.
    static void DisableStreamHandling(void);

  #ifdef PLATFORM_WIN32 /* rcg10042001 !!! FIXME */
    //! Static function to filter exceptions and intercept access violation.
    static int ExceptionFilter(DWORD dwCode, _EXCEPTION_POINTERS *pExceptionInfoPtrs);
  #endif

    //! Static function to report fatal exception error.
    static void ExceptionFatalError(void);

    //! Default constructor.
    CTStream(void);

    //! Destruction.
    virtual ~CTStream(void);

    //! Returns serialization flags.
    inline ULONG GetFlags() const
    {
      return strm_ulFlags;
    }

    //! Check if the stream can be read. - used mainly for assertions.
    virtual BOOL IsReadable(void) = 0;

    //! Check if the stream can be written. - used mainly for assertions.
    virtual BOOL IsWriteable(void) = 0;

    //! Check if the stream can be seeked. - used mainly for assertions.
    virtual BOOL IsSeekable(void) = 0;

    //! Read a block of data from stream.
    virtual void Read_t(void *pvBuffer, SLONG slSize) = 0; // throw char *

    //! Write a block of data to stream.
    virtual void Write_t(const void *pvBuffer, SLONG slSize) = 0; // throw char *


    //! Seek in stream.
    virtual void Seek_t(SLONG slOffset, enum SeekDir sd) = 0; // throw char *

    //! Set absolute position in stream.
    virtual void SetPos_t(SLONG slPosition) = 0; // throw char *

    //! Get absolute position in stream.
    virtual SLONG GetPos_t(void) = 0; // throw char *

    //! Get size of stream.
    virtual SLONG GetStreamSize(void) = 0;

    //! Get CRC32 of stream.
    virtual ULONG GetStreamCRC32_t(void) = 0;

    //! Check if file position points to the EOF.
    virtual BOOL AtEOF(void) = 0;

    //! Get description of this stream (e.g. filename for a CFileStream).
    inline FString &GetDescription(void) { return strm_strStreamDescription; };

    /* Read an object from stream. */
    inline CTStream &operator>>(float  &f) { Read_t( &f, sizeof( f)); return *this; } // throw char *
    inline CTStream &operator>>(double &d) { Read_t( &d, sizeof( d)); return *this; } // throw char *
    inline CTStream &operator>>(ULONG &ul) { Read_t(&ul, sizeof(ul)); return *this; } // throw char *
    inline CTStream &operator>>(UWORD &uw) { Read_t(&uw, sizeof(uw)); return *this; } // throw char *
    inline CTStream &operator>>(UBYTE &ub) { Read_t(&ub, sizeof(ub)); return *this; } // throw char *
    inline CTStream &operator>>(SLONG &sl) { Read_t(&sl, sizeof(sl)); return *this; } // throw char *
    inline CTStream &operator>>(SWORD &sw) { Read_t(&sw, sizeof(sw)); return *this; } // throw char *
    inline CTStream &operator>>(SBYTE &sb) { Read_t(&sb, sizeof(sb)); return *this; } // throw char *
    inline CTStream &operator>>(BOOL   &b) { Read_t( &b, sizeof( b)); return *this; } // throw char *
    inline CTStream &operator>>(INT64 &sn) { Read_t(&sn, sizeof(sn)); return *this; } // throw char *
    inline CTStream &operator>>(UINT64 &un){ Read_t(&un, sizeof(un)); return *this; } // throw char *

    /* Write an object into stream. */
    inline CTStream &operator<<(const float  &f) { Write_t( &f, sizeof( f)); return *this; } // throw char *
    inline CTStream &operator<<(const double &d) { Write_t( &d, sizeof( d)); return *this; } // throw char *
    inline CTStream &operator<<(const ULONG &ul) { Write_t(&ul, sizeof(ul)); return *this; } // throw char *
    inline CTStream &operator<<(const UWORD &uw) { Write_t(&uw, sizeof(uw)); return *this; } // throw char *
    inline CTStream &operator<<(const UBYTE &ub) { Write_t(&ub, sizeof(ub)); return *this; } // throw char *
    inline CTStream &operator<<(const SLONG &sl) { Write_t(&sl, sizeof(sl)); return *this; } // throw char *
    inline CTStream &operator<<(const SWORD &sw) { Write_t(&sw, sizeof(sw)); return *this; } // throw char *
    inline CTStream &operator<<(const SBYTE &sb) { Write_t(&sb, sizeof(sb)); return *this; } // throw char *
    inline CTStream &operator<<(const BOOL   &b) { Write_t( &b, sizeof( b)); return *this; } // throw char *
    inline CTStream &operator<<(const INT64 &sn) { Write_t(&sn, sizeof(sn)); return *this; } // throw char *
    inline CTStream &operator<<(const UINT64 &un){ Write_t(&un, sizeof(un)); return *this; } // throw char *

    // FPath reading/writing
    CORE_API friend CTStream &operator>>(CTStream &strmStream, FPath &fnmFileName);
    CORE_API friend CTStream &operator<<(CTStream &strmStream, const FPath &fnmFileName);
    
    // FByteArray reading/writing
    CORE_API friend CTStream &operator>>(CTStream &strmStream, FByteArray &baData);
    CORE_API friend CTStream &operator<<(CTStream &strmStream, const FByteArray &baData);

    // FVariant reading/writing
    CORE_API friend CTStream &operator>>(CTStream &strmStream, FVariant &baData);
    CORE_API friend CTStream &operator<<(CTStream &strmStream, const FVariant &baData);

    /* Put a line of text into stream. */
    virtual void PutLine_t(const char *strBuffer); // throw char *
    virtual void PutString_t(const char *strString); // throw char *
    virtual void FPrintF_t(const char *strFormat, ...); // throw char *

    /* Get a line of text from stream. */
    virtual void GetLine_t(char *strBuffer, SLONG slBufferSize, char cDelimiter='\n'); // throw char *
    virtual void GetLine_t(FString &strLine, char cDelimiter='\n'); // throw char *

    virtual CChunkID GetID_t(void); // throw char *
    virtual CChunkID PeekID_t(void); // throw char *
    virtual void ExpectID_t(const CChunkID &cidExpected); // throw char *
    virtual void ExpectKeyword_t(const FString &strKeyword); // throw char *
    virtual SLONG GetSize_t(void); // throw char *
    virtual void ReadRawChunk_t(void *pvBuffer, SLONG slSize); // throw char *
    virtual void ReadChunk_t(void *pvBuffer, SLONG slExpectedSize); // throw char *
    virtual void ReadFullChunk_t(const CChunkID &cidExpected, void *pvBuffer, SLONG slExpectedSize); // throw char *
    virtual void *ReadChunkAlloc_t(SLONG slSize=0); // throw char *
    virtual void ReadStream_t(CTStream &strmOther); // throw char *

    virtual void WriteID_t(const CChunkID &cidSave); // throw char *
    virtual void WriteSize_t(SLONG slSize); // throw char *
    virtual void WriteRawChunk_t(void *pvBuffer, SLONG slSize); // throw char *  // doesn't write length
    virtual void WriteChunk_t(void *pvBuffer, SLONG slSize); // throw char *
    virtual void WriteFullChunk_t(const CChunkID &cidSave, void *pvBuffer, SLONG slSize); // throw char *
    virtual void WriteStream_t(CTStream &strmOther); // throw char *

    // Whether or not the given pointer is coming from this stream (mainly used for exception handling)
    virtual BOOL PointerInStream(void* pPointer);

    // filename dictionary operations
    inline FPathDictionary &GetDictionary()
    {
      return strm_Dictionary;
    }
    
    // read the dictionary from given offset in file (internal function)
    void ReadDictionary_intenal_t(SLONG slOffset);

    // copy filename dictionary from another stream
    void CopyDictionary(CTStream &strmOther);

    //! Start writing a with a dictionary.
    void DictionaryWriteBegin_t(const FPath &fnmImportFrom, SLONG slImportOffset);

    //! Stop writing a with a dictionary.
    void DictionaryWriteEnd_t(void);

    //! Start reading a with a dictionary.
    SLONG DictionaryReadBegin_t(void);  // returns offset of dictionary for cross-file importing

    //! Stop reading a with a dictionary.
    void DictionaryReadEnd_t(void);

    //! Preload all files mentioned in the dictionary.
    void DictionaryPreload_t(void);

    // [SEE]
    void ReadString_t(FString &str) override;
    void WriteString_t(const FString &str) override;
    void ReadPath_t(FPath &fnm) override;
    void WritePath_t(const FPath &fnm) override;
};

#include <Core/IO/FileStream.h>
#include <Core/IO/MemoryStream.h>
#include <Core/IO/FileUtils.h>

//! Global string with application path
CORE_API extern FPath _fnmApplicationPath;

//! Global string with current MOD path
CORE_API extern FPath _fnmMod;

//! Global string with current name (the parameter that is passed on cmdline)
CORE_API extern FString _strModName;

//! Global string with url to be shown to users that don't have the mod installed (should be set by game.dll).
CORE_API extern FString _strModURL;

//! Global string with current MOD extension (for adding to dlls).
CORE_API extern FString _strModExt;

//! Global string with CD path (for minimal installations).
CORE_API extern FPath _fnmSharedPath;

//! Global string with filename of the started application.
CORE_API extern FPath _fnmApplicationExe;

#endif  /* include-once check. */
