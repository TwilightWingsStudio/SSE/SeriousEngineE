/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/IO/Resource.h>
#include <Core/IO/ResourceStock.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>


extern CResourceStock *_pModuleStock = NULL;
extern CResourceStock *_pAnimStock = NULL;
extern CResourceStock *_pAnimSetStock = NULL;
extern CResourceStock *_pEntityClassStock = NULL;
extern CResourceStock *_pMeshStock = NULL;
extern CResourceStock *_pModelStock = NULL;
extern CResourceStock *_pModelConfigStock = NULL;
extern CResourceStock *_pShaderStock = NULL;
extern CResourceStock *_pSkeletonStock = NULL;
extern CResourceStock *_pSoundStock = NULL;
extern CResourceStock *_pTextureStock = NULL;


// Default constructor.
CResourceStock::CResourceStock(void)
{
  st_ntObjects.SetAllocationParameters(50, 2, 2);
}

// Destructor.
CResourceStock::~CResourceStock(void)
{
  FreeUnused(); // Free all unused elements of the stock
}

// Obtain an object from stock - loads if not loaded.
CResource *CResourceStock::Obtain_t(const FPath &fnmFileName)
{
  // Find stocked object with same name
  CResource *pExisting = st_ntObjects.Find(fnmFileName);
  
  // If found
  if (pExisting != NULL) {
    // Mark that it is used once again
    pExisting->Reference();
    
    // Return its pointer
    return pExisting;
  }

  // if not found,

  // Create new stock object
  CResource *ptNew = New();
  ptNew->ser_FileName = fnmFileName;
  st_ctObjects.Add(ptNew);
  st_ntObjects.Add(ptNew);

  // Load it
  try {
    ptNew->Load_t(fnmFileName);
  } catch(char *) {
    st_ctObjects.Remove(ptNew);
    st_ntObjects.Remove(ptNew);
    delete ptNew;
    throw;
  }

  // Mark that it is used for the first time
  //ASSERT(!ptNew->IsReferenced());
  ptNew->Reference();

  // Return the pointer to the new one
  return ptNew;
}

// Release an object when not needed any more.
void CResourceStock::Release(CResource *ptObject)
{
  // Mark that it is used one less time
  ptObject->Unreference();
  
  // If it is not used at all any more and should be freed automatically
  if (!ptObject->IsReferenced() && ptObject->IsAutoFreed()) {
    // Remove it from stock
    st_ctObjects.Remove(ptObject);
    st_ntObjects.Remove(ptObject);
    delete ptObject;
  }
}

// Free all unused elements of the stock
void CResourceStock::FreeUnused(void)
{
  BOOL bAnyRemoved;
  
  // Repeat
  do {
    // Create container of objects that should be freed
    TDynamicContainer<CResource> ctToFree;
    {
      FOREACHINDYNAMICCONTAINER(st_ctObjects, CResource, itt)
      {
        if (!itt->IsReferenced()) {
          ctToFree.Add(itt);
        }
      }
    }
    bAnyRemoved = ctToFree.Count() > 0;
    
    // For each object that should be freed
    {
      FOREACHINDYNAMICCONTAINER(ctToFree, CResource, itt)
      {
        st_ctObjects.Remove(itt);
        st_ntObjects.Remove(itt);
        delete (&*itt);
      }
    }

  // As long as there is something to remove
  } while (bAnyRemoved);

}

// Calculate amount of memory used by all objects in the stock
SLONG CResourceStock::CalculateUsedMemory(void)
{
  SLONG slUsedTotal = 0;
  {
    FOREACHINDYNAMICCONTAINER(st_ctObjects, CResource, itt)
    {
      SLONG slUsedByObject = itt->GetUsedMemory();
      if (slUsedByObject < 0) {
        return -1;
      }
      slUsedTotal += slUsedByObject;
    }
  }
  return slUsedTotal;
}

// Dump memory usage report to a file
void CResourceStock::DumpMemoryUsage_t(CTStream &strm) // throw char *
{
  FString strLine;
  SLONG slUsedTotal = 0;
  {
    FOREACHINDYNAMICCONTAINER(st_ctObjects, CResource, itt)
    {
      SLONG slUsedByObject = itt->GetUsedMemory();
      if (slUsedByObject < 0) {
        strm.PutLine_t("Error!");
        return;
      }
      strLine.PrintF("%7.1fk %s(%d) %s", slUsedByObject / 1024.0f, itt->GetName().ConstData(), itt->GetReferenceCount(),
                                         itt->GetDescription());
      strm.PutLine_t(strLine);
    }
  }
}

// Get number of total elements in stock
INDEX CResourceStock::GetTotalCount(void)
{
  return st_ctObjects.Count();
}

// Get number of used elements in stock
INDEX CResourceStock::GetUsedCount(void)
{
  INDEX ctUsed = 0;
  {
    FOREACHINDYNAMICCONTAINER(st_ctObjects, CResource, itt)
    {
      if (itt->IsReferenced()) {
        ctUsed++;
      }
    }
  }
  return ctUsed;
}
