/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _WIN32
#include <io.h>
#endif

#include <Core/Base/ByteArray.h>
#include <Core/Base/Console.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Translation.h>

#include <Core/IO/FileName.h>
#include <Core/IO/FileDevice.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/BsaArchive.h>
#include <Core/IO/PakArchive.h>
#include <Core/IO/TarArchive.h>
#include <Core/IO/GrpArchive.h>
#include <Core/IO/WadArchive.h>
#include <Core/IO/ZipArchive.h>
#include <Core/IO/FileIndexNode.h>
#include <Core/IO/ArchiveManager.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>
#include <Core/Templates/StaticStackArray.cpp>

FArchiveManager *_pArchiveMgr = NULL;

static int qsort_CompareArchiveId_Descending(const void *ppPEN0, const void *ppPEN1)
{
  FArchive &ar0 = **(FArchive**)ppPEN0;
  FArchive &ar1 = **(FArchive**)ppPEN1;

  ULONG id0 = ar0.GetId();
  ULONG id1 = ar1.GetId();

  if (id0 < id1) return +1;
  else if (id0 > id1) return -1;
  else              return  0;
}

int qsort_FFileIndexNode_reverse(const void *elem1, const void *elem2 )
{          
  // get the infos
  const FFileIndexNode &ainf1 = **(FFileIndexNode**)elem1;
  const FFileIndexNode &ainf2 = **(FFileIndexNode**)elem2;

  // calculate priorities based on location of gro file
  // 3 - Mod, 2 - Shared Mod, 1 - Root Folder, 0 - Shared Folder
  INDEX iPriority1 = 1; 

  if (ainf1.GetFlags() & FFileIndexNode::FF_MOD) {
    iPriority1 += 2;
  }

  if (ainf1.GetFlags() & FFileIndexNode::FF_SHARED) {
    iPriority1 -= 1; // Lower priority.
  }

  INDEX iPriority2 = 1;

  if (ainf2.GetFlags() & FFileIndexNode::FF_MOD) {
    iPriority2 += 2;
  }

  if (ainf2.GetFlags() & FFileIndexNode::FF_SHARED) {
    iPriority2 -= 1; // Lower priority.
  }

  // find sorting order
  if (iPriority1 < iPriority2) {
    return +1;
  } else if (iPriority1 > iPriority2) {
    return -1;
  } else {
    return -stricmp(ainf1.GetName(), ainf2.GetName());
  }
}

static void LoadArchivesFromDir(FFileIndexNode &dir)
{
  struct _finddata_t c_file;
  intptr_t hFile;
  
  INDEX ctArchives = 0;

  hFile = _findfirst(dir.GetName() + "*.*", &c_file); // "*.gro", &c_file);
  BOOL bOK = (hFile!=-1);

  while(bOK)
  {
    FString strFile(c_file.name);

    if (strFile.Matches("*.zip") || strFile.Matches("*.gro") || strFile.Matches("*.pk3")) {
      dir.Children.Push() = new FFileIndexNode(dir.GetName() + c_file.name, FFileIndexNode::FT_FILE, dir.GetFlags());

      ctArchives++;
    }

    bOK = _findnext(hFile, &c_file) == 0;
  }
  
  CInfoF("  %s : %d package(s)\n", dir.GetName().ConstData(), ctArchives);

  _findclose( hFile );
}

void FArchiveManager::SortByArchiveId(TDynamicContainer<FArchive> &cInput)
{
  cInput.Sort(&qsort_CompareArchiveId_Descending);
}

FArchiveManager::FArchiveManager()
{
  _csManager.cs_iIndex = -1;
  _idNextArchive = 0;
  _ntArchives.SetAllocationParameters(50, 2, 2);

  RegisterLoader(new FZipArchiveLoader);
  RegisterLoader(new FTarArchiveLoader);
  RegisterLoader(new FPakArchiveLoader);
  RegisterLoader(new FWadArchiveLoader);
  RegisterLoader(new FBsaArchiveLoader);
  RegisterLoader(new FGrpArchiveLoader);
}

void FArchiveManager::RegisterLoader(FArchiveLoader *pLoader)
{
  CSyncLock slManager(&_csManager, TRUE);
  _ctLoaders.Push() = pLoader;
}

FArchiveManager::~FArchiveManager()
{
  CloseUnused();
}

FArchive *FArchiveManager::Obtain_t(const FPath &fnm)
{
  CSyncLock slManager(&_csManager, TRUE);

  FArchive *pArchive = _ntArchives.Find(fnm);
  
  if (pArchive) {
    return pArchive;
  }

  TDynamicContainer<FArchiveLoader> _ctAssumedByExt;
  TStaticStackArray<FString> aValidExtensions;

  aValidExtensions.SetAllocationStep(4);

  FOREACHINDYNAMICCONTAINER(_ctLoaders, FArchiveLoader, itt)
  {
    FArchiveLoader &loader = itt.Current();
    loader.GetValidExtensions(aValidExtensions);

    for (int i = 0; i < aValidExtensions.Count(); i++)
    {
      if (aValidExtensions[i] == fnm.FileExt()) {
        _ctAssumedByExt.Push() = &loader;
        break;
      }
    }

    aValidExtensions.Clear();
  }

  if (_ctAssumedByExt.Count() == 0) {
    ThrowF_t(TRANS("%s : Archive has unknown extension!"), fnm.ConstData());
  }

  pArchive = new FArchive();
  pArchive->Initialize();
  pArchive->SetName(fnm);
  pArchive->SetId(_idNextArchive++);

  FFileDevice dev(fnm);

  if (!dev.Open(FReadWriteDevice::OM_READONLY)) {
    ThrowF_t(TRANS("%s: Cannot open file (%s)"), fnm.ConstData(), strerror(errno));
  }

  FDataStream strm(&dev);
  strm.SetExceptionMode(true);
  _ctAssumedByExt.Pointer(0)->Load_t(pArchive, strm);

  _ctObjects.Add(pArchive);
  _ntArchives.Add(pArchive);
  SortByArchiveId(_ctObjects); // Put newest first!
  return pArchive;
}

void FArchiveManager::Release(FArchive *pArchive)
{
  CSyncLock slManager(&_csManager, TRUE);
  _ctObjects.Remove(pArchive);
  _ntArchives.Remove(pArchive);
  SortByArchiveId(_ctObjects); // Put newest first!

  if (pArchive->IsOpen()) {
    pArchive->Close();
  }

  delete pArchive;
}

INDEX FArchiveManager::QueryFiles(const FPath &fnm, TDynamicContainer<FArchiveEntry> &entries, BOOL bModSearch, INDEX ctMaxFiles)
{
  CSyncLock slManager(&_csManager, TRUE);

  FOREACHINDYNAMICCONTAINER(_ctObjects, FArchive, itt)
  {
    FArchive &archive = itt.Current();

    // Skip mod archives if not allowed.
    if (!bModSearch && archive.IsMod()) {
      continue;
    }

    FArchiveEntry *pEntry = archive.FindEntry(fnm);

    if (pEntry) {
      entries.Add(pEntry);

      if (ctMaxFiles > 0) {
        const INDEX ctEntries = entries.Count();

        if (ctEntries >= ctMaxFiles) {
          return ctEntries;
        }
      }
    }
  }

  return entries.Count();
}

void FArchiveManager::CloseUnused()
{
  FOREACHINDYNAMICCONTAINER(_ctObjects, FArchive, itt)
  {
    FArchive &archive = itt.Current();
    
    // Skip invalid and closed archives.
    if (!archive.IsOpen()) {
      continue;
    }    
    
    //if (pArchive->GetHandleCount() == 0) {
      archive.Close();
    //}
  }
  
  // TODO: Sorting should be performed!
}

void FArchiveManager::RegisterDirectory(const FPath &fnm, ULONG ulFlags)
{
  CSyncLock slManager(&_csManager, TRUE);
  _ctArchiveDirs.Push() = new FFileIndexNode(fnm, FFileIndexNode::FT_DIRECTORY, ulFlags);
}

void FArchiveManager::SearchArchives()
{
  CSyncLock slManager(&_csManager, TRUE);
  CInfoF(TRANS("Searching for archive files...\n"));

  FOREACHINDYNAMICCONTAINER(_ctArchiveDirs, FFileIndexNode, itd)
  {
    FFileIndexNode &dir = itd.Current();
    LoadArchivesFromDir(dir);
    dir.Children.Sort(qsort_FFileIndexNode_reverse);
  }

  CInfoF(TRANS("Loading archives...\n"));

  FOREACHINDYNAMICCONTAINER(_ctArchiveDirs, FFileIndexNode, itt)
  {
    const FFileIndexNode &dir = itt.Current();
    CInfoF("  %s : %d package(s)\n", dir.GetName().ConstData(), dir.Children.Count());

    // try to
    try {
      // read the zip directories
      for (INDEX iArchive = dir.Children.Count() - 1; iArchive >= 0; --iArchive)
      {
        const FFileIndexNode &ainf = dir.Children[iArchive];
        FArchive *pArchive = _pArchiveMgr->Obtain_t(ainf.GetName());
        pArchive->SetMod(ainf.GetFlags() & FFileIndexNode::FF_MOD);

        // report that file was read
        FString strPath = pArchive->GetName();
        strPath.RemovePrefix(dir.GetName());
        CInfoF(TRANS("    %s : %d files\n"), strPath, pArchive->_ctFileInfos.Count());
      }

    // if failed then report warning
    } catch(char *strError) {
      CErrorF(TRANS("There were group file errors:\n%s"), strError);
    }
  }
}