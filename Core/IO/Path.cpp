/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Path.h>

#include <Core/Base/ErrorReporting.h>
#include <Core/IO/Stream.h>
#include <Core/Templates/DynamicStackArray.cpp>

template class TDynamicArray<FPath>;
template class TDynamicStackArray<FPath>;
#include <Core/Templates/StaticStackArray.cpp>
template class TStaticStackArray<long>;

/*
 * Get directory part of a filename.
 */
FPath FPath::FileDir() const
{
  ASSERT(IsValid());

  // make a temporary copy of string
  FPath strPath(*this);
  // find last backlash in it
  char *pPathBackSlash = strrchr(strPath.Data(), '\\');

  // if there is no backslash then return emptystring as directory
  if (pPathBackSlash == NULL) {
    return(FPath(""));
  }

  // set end of string after where the backslash was
  pPathBackSlash[1] = 0;

  // return a copy of temporary string
  return( FPath( strPath));
}

FPath &FPath::operator=(const char *strCharString)
{
  ASSERTALWAYS( "Use FPath for conversion from char *!");
  return *this;
}

/*
 * Get name part of a filename.
 */
FPath FPath::FileName() const
{
  ASSERT(IsValid());

  // make a temporary copy of string
  FPath strPath(*this);
  // find last dot in it
  char *pDot = strrchr(strPath.Data(), '.');

  // if there is a dot then set end of string there
  if (pDot != NULL) {
    pDot[0] = 0;
  }

  // find last backlash in what's left
  char *pBackSlash = strrchr(strPath.Data(), '\\');

  // if there is no backslash then return it all as filename
  if (pBackSlash == NULL) {
    return (FPath(strPath));
  }

  // return a copy of temporary string, starting after the backslash
  return (FPath(pBackSlash + 1));
}

/*
 * Get extension part of a filename.
 */
FPath FPath::FileExt() const
{
  ASSERT(IsValid());

  // find last dot in the string
  const char *pExtension = strrchr(ConstData(), '.');

  // if there is no dot then return no extension
  if (pExtension == NULL) {
    return (FPath(""));
  }

  // return a copy of the extension part, together with the dot
  return (FPath(pExtension));
}

FPath FPath::NoExt() const
{
  return FileDir()+FileName();
}

static INDEX GetSlashPosition(const char* pszString)
{
  for (INDEX iPos = 0; '\0' != *pszString; ++iPos, ++pszString)
  {
    if (('\\' == *pszString) || ('/' == *pszString)) {
      return iPos;
    }
  }

  return -1;
}

/*
 * Set path to the absolute path, taking \.. and /.. into account.
 */
void FPath::SetAbsolutePath(void)
{
  // Collect path parts
  FString strRemaining(*this);
  TStaticStackArray<FString> astrParts;
  INDEX iSlashPos = GetSlashPosition(strRemaining);

  if (0 > iSlashPos) {
    return; // Invalid path
  }

  for (;;)
  {
    FString &strBeforeSlash = astrParts.Push();
    FString strAfterSlash;
    strRemaining.Split(iSlashPos, strBeforeSlash, strAfterSlash);
    strAfterSlash.TrimLeft(strAfterSlash.Length() - 1);
    strRemaining = strAfterSlash;
    iSlashPos = GetSlashPosition(strRemaining);

    if (0 > iSlashPos) {
      astrParts.Push() = strRemaining;
      break;
    }
  }

  // Remove certain path parts
  for (INDEX iPart = 0; iPart < astrParts.Count(); ++iPart)
  {
    if (FString("..") != astrParts[iPart]) {
      continue;
    }

    if (0 == iPart) {
      return; // Invalid path
    }

    // Remove ordered
    TStaticStackArray<FString> astrShrinked;
    astrShrinked.Push(astrParts.Count() - 2);
    astrShrinked.PopAll();

    for (INDEX iCopiedPart = 0; iCopiedPart < astrParts.Count(); ++iCopiedPart)
    {
      if ((iCopiedPart != iPart - 1) && (iCopiedPart != iPart)) {
        astrShrinked.Push() = astrParts[iCopiedPart];
      }
    }

    astrParts.MoveArray(astrShrinked);
    iPart -= 2;
  }

  // Set new content
  strRemaining.Clear();

  for (INDEX iPart1 = 0; iPart1 < astrParts.Count(); ++iPart1)
  {
    strRemaining += astrParts[iPart1];

    if (iPart1 < astrParts.Count() - 1) {
#ifdef PLATFORM_WIN32
      strRemaining += FString("\\");
#else
      strRemaining += FString("/");
#endif
    }
  }

  (*this) = strRemaining;
}

/*
 * Remove application path from a file name and returns TRUE if it's a relative path.
 */
BOOL FPath::RemoveApplicationPath_t(void) // throws char *
{
  FPath fnmApp = _fnmApplicationPath;
  fnmApp.SetAbsolutePath();

  // remove the path string from beginning of the string
  BOOL bIsRelative = RemovePrefix(fnmApp);

  if (_fnmMod != "") {
    RemovePrefix(_fnmApplicationPath+_fnmMod);
  }

  return bIsRelative;
}

CTStream &operator>>(CTStream &strmStream, FPath &fnmFileName)
{
  strmStream.ReadPath_t(fnmFileName);
  return strmStream;
}

CTStream &operator<<(CTStream &strmStream, const FPath &fnmFileName)
{
  strmStream.WritePath_t(fnmFileName);
  return strmStream;
}

void FPath::ReadFromText_t(CTStream &strmStream,
                                const FString &strKeyword) // throw char *
{
  ASSERT(IsValid());

  char strTag[] = "_FNM "; strTag[0] = 'T';  // must create tag at run-time!
  // keyword must be present
  strmStream.ExpectKeyword_t(strKeyword);
  // after the user keyword, dependency keyword must be present
  strmStream.ExpectKeyword_t(strTag);

  // read the string from the file
  char str[1024];
  strmStream.GetLine_t(str, sizeof(str));

  // copy it here
  (*this) = FString((const char *)str);
}

#include <Core/IO/DataStream.h>

FDataStream &operator<<(FDataStream &strmStream, const FPath &fnm)
{
  strmStream.WritePath_t(fnm);
  return strmStream;
}

FDataStream &operator>>(FDataStream &strmStream, FPath &fnm)
{
  strmStream.ReadPath_t(fnm);
  return strmStream;
}
