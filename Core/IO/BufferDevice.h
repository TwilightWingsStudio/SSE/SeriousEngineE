/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_BUFFERDEVICE_H
#define SE_INCL_BUFFERDEVICE_H

#include <Core/Base/ByteArray.h>
#include <Core/IO/ReadWriteDevice.h>

class CORE_API FBufferDevice final : public FReadWriteDevice
{
  public:
    //! Default constructor.
    FBufferDevice();

    //! Constructor that uses pointed FByteArray as internal buffer.
    FBufferDevice(FByteArray *pByteArray);

    //! Start interacting in given mode.
    bool Open(OpenMode om) override;

    //! End interacting with.
    void Close() override;

    //! Returns carret position.
    size_t Pos() const override;

    //! Checks if carret at end.
    bool AtEnd() const override;

    //! Size of the buffer.
    size_t Size() const override;

    //! Tries to move carret to specified position.
    bool Seek(size_t pos) override;

    //! Move formard.
    size_t Skip(size_t max_size) override;

    //! Take bytes from device.
    size_t Read(char *pData, size_t max_size) override;

    //! Take bytes without moving carret formard.
    size_t Peek(char *pData, size_t max_size) override;

    //! Put bytes into device.
    size_t Write(const char *pData, size_t max_size) override;

    //! Check device type.
    virtual Type GetType() const
    {
      return TYPE_BUFFER;
    }

  // Buffer specific.
  public:
    void SetBuffer(FByteArray *pData);

  protected:
    size_t _pos;
    FByteArray *_pData;
};

#endif /* include-once check */