/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Console.h>

#include <Core/IO/FileName.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/GrpArchive.h>

#include <Core/Threading/Synchronization.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

extern ULONG CalcArchiveEntryChecksum_t(FArchiveEntry *pEntry, FReadWriteDevice *pDevice);

struct GrpEntry
{
  FPath Filename;
  ULONG Size;
  ULONG Offset; // Not serialized. But extracted.

  //! Constructor.
  GrpEntry()
  {
    Size = Offset = 0;
  }
};

void FGrpArchiveLoader::Load_t(FArchive *pArchive, FDataStream &strm) const
{
  const FByteArray baExpSignature("KenSilverman");
  strm.SetByteOrder(FDataStream::BO_LITTLEENDIAN);

  FReadWriteDevice *pDevice = strm.Device();
  FByteArray baSignature('\x00', 12);
  ULONG FileCount;
  TStaticArray<GrpEntry> Entries;

  strm.Read(baSignature.Data(), 12);

  if (baSignature != baExpSignature) {
    ThrowF_t(TRANS("%s : Wrong signature for GRP file!"), pArchive->GetName().ConstData());
  }

  strm >> FileCount;
  Entries.New(FileCount);

  for (int i = 0; i < Entries.Count(); i++)
  {
    GrpEntry &entry = Entries[i];
    FByteArray baName('\x00', 12);

    strm.Read(baName.Data(), 12);
    strm >> entry.Size;

    entry.Filename.PrintF((const char *)baName.ConstData());
  }

  for (int j = 0; j < Entries.Count(); j++)
  {
    GrpEntry &entry = Entries[j];
    entry.Offset = strm.Device()->Pos();
  }

  for (int k = 0; k < Entries.Count(); k++)
  {
    const GrpEntry &entry = Entries[k];

    FArchiveEntry *pNew = new FArchiveEntry(pArchive, entry.Filename);
    pNew->SetCompressionMethod(COMPRESSION_METHOD_STORE);
    pNew->SetUncompressedSize(entry.Size);
    pNew->SetOffset(entry.Offset);
    pArchive->RegisterEntry(pNew);
  }

  pArchive->CalculateChecksums_t(pDevice);
}

bool FGrpArchiveLoader::IsCompressionSupported(enum CompressionMethod eMethod) const
{
  return eMethod == COMPRESSION_METHOD_STORE;
}

void FGrpArchiveLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".grp";
}