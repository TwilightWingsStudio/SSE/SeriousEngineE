#pragma pack(1)

enum ETarLinkIndicator
{
  TLI_NORMAL_FILE_OLD        =  0,
  TLI_NORMAL_FILE            = '0',
  TLI_HARD_LINK              = '1',
  TLI_SYMBOLIC_LINK          = '2',
  TLI_CHAR_SPECIAL_DEVICE    = '3',
  TLI_BLOCK_SPECIAL_DEVICE   = '4',
  TLI_DIRECTORY              = '5',
  TLI_FIFO_SPECIAL_FILE      = '6',
  TLI_CONTIGUOUS_FILE        = '7',
  TLI_GLOBAL_EXTENDED_HEADER = 'g',
  TLI_EXTENDED_HEADER        = 'x',
};

struct STarHeader
{
  char FileName[100];
  char FileMode[8];
  char UserId[8];
  char GroupId[8];
  char Size[12];
  char ModifiedTime[12];
  char Checksum[8];
  char Link;
  char LinkName[100];
  char Magic[6];
  char USTARVersion[2];
  char UserName[32];
  char GroupName[32];
  char DeviceMajor[8];
  char DeviceMinor[8];
  char FileNamePrefix[155];
};

#pragma pack()