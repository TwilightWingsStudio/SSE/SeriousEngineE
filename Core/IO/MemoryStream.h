/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */


#ifndef SE_INCL_MEMORYSTREAM_H
#define SE_INCL_MEMORYSTREAM_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! CroTeam memory stream class.
class CORE_API CTMemoryStream : public CTStream
{
  public:
    BOOL mstrm_bReadable;      // set if stream is readable
    BOOL mstrm_bWriteable;     // set if stream is writeable
    INDEX mstrm_ctLocked;      // counter for buffer locking

    UBYTE* mstrm_pubBuffer;    // buffer of the stream
    UBYTE* mstrm_pubBufferEnd; // pointer to the end of the stream buffer
    SLONG mstrm_slLocation;    // location in the stream
    UBYTE* mstrm_pubBufferMax; // furthest that the stream location has ever gotten

  public:
    //! Create dynamically resizing stream for reading/writing. 
    CTMemoryStream(void);

    //! Create static stream from given buffer.
    CTMemoryStream(void *pvBuffer, SLONG slSize, CTStream::OpenMode om = CTStream::OM_READ);

    //! Destructor.
    virtual ~CTMemoryStream(void);


    //! Lock the buffer contents and it's size.
    void LockBuffer(void **ppvBuffer, SLONG *pslSize);

    //! Unlock buffer.
    void UnlockBuffer(void);


    //! Read a block of data from stream.
    void Read_t(void *pvBuffer, SLONG slSize); // throw char *

    //! Write a block of data to stream.
    void Write_t(const void *pvBuffer, SLONG slSize); // throw char *

    //! Seek in stream.
    void Seek_t(SLONG slOffset, enum SeekDir sd); // throw char *

    //! Set absolute position in stream.
    void SetPos_t(SLONG slPosition); // throw char *

    //! Get absolute position in stream.
    SLONG GetPos_t(void); // throw char *

    //! Get size of stream.
    SLONG GetSize_t(void); // throw char *

    //! Get size of stream.
    SLONG GetStreamSize(void);

    //! Get CRC32 of stream.
    ULONG GetStreamCRC32_t(void);

    //! Check if file position points to the EOF.
    BOOL AtEOF(void);


    //! whether or not the given pointer is coming from this stream (mainly used for exception handling).
    virtual BOOL PointerInStream(void* pPointer);

    // memory stream can be opened only for reading and writing
    virtual BOOL IsWriteable(void);
    virtual BOOL IsReadable(void);
    virtual BOOL IsSeekable(void);
};

#endif  /* include-once check. */
