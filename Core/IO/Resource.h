/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_RESOURCE_H
#define SE_INCL_RESOURCE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Changeable.h>
#include <Core/IO/FileName.h>
#include <Core/Objects/ReferenceCounted.h>

//! Abstract base class for objects that can be saved and loaded.
class CORE_API CResource : public CChangeable, public CRefCounted
{
  public:
    enum Type
    {
      TYPE_INVALID = 0,
      TYPE_ENTITYCLASS,
      TYPE_TEXTURE,
      TYPE_ANIMATION,
      TYPE_MODEL,
      TYPE_SOUND,
      TYPE_SKELMESH,
      TYPE_SKELETON,
      TYPE_SHADER,
      TYPE_BONEANIMSET,
      TYPE_MODELCONFIGURATION,
      TYPE_MODULE,
      TYPE_LAST,
    };

  public:
    FPath ser_FileName;  // last file name loaded

  public:
    //! Default constructor.
    CResource(void);

    //! Destructor.
    virtual ~CResource(void);

    //! Get the file name of this object.
    inline const FPath &GetName(void) { return ser_FileName; };

    //! Get the description of this object.
    virtual FString GetDescription(void);

    //! Load from file.
    void Load_t(const FPath fnFileName); // throw char *

    //! Save to file.
    void Save_t(const FPath fnFileName); // throw char *

    //! Reload from file.
    void Reload(void);

    //! Clear the object.
    virtual void Clear(void);

    //! Read from stream.
    virtual void Read_t(CTStream *istrFile) = 0; // throw char *

    //! Write to stream.
    virtual void Write_t(CTStream *ostrFile) = 0; // throw char *

    //! Check if this kind of objects is auto-freed.
    virtual BOOL IsAutoFreed(void) { return TRUE; };

    //! Get amount of memory used by this object.
    virtual SLONG GetUsedMemory(void) { return -1; };

    // Gather the CRC of the file.
    virtual void AddToCRCTable(void);

    virtual UBYTE GetType() const
    {
      return 0;
    }
};

#endif  /* include-once check. */
