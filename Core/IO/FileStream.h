/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FILESTREAM_H
#define SE_INCL_FILESTREAM_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! CroTeam file stream class.
class CORE_API CTFileStream : public CTStream
{
  private:
    FILE *fstrm_pFile;    // ptr to opened file

    INDEX fstrm_iZipHandle; // handle of zip-file entry
    INDEX fstrm_iZipLocation; // location in zip-file entry
    ULONG fstrm_ulZipChecksum; // CRC32
    UBYTE* fstrm_pubZipBuffer; // buffer for zip-file entry
    SLONG fstrm_slZipSize; // size of the zip-file entry

    BOOL fstrm_bReadOnly;  // set if file is opened in read-only mode

  public:
    //! Default constructor.
    CTFileStream(void);

    //! Destructor.
    virtual ~CTFileStream(void);

    //! Open an existing file.
    void Open_t(const FPath &fnFileName, enum CTStream::OpenMode om=CTStream::OM_READ); // throw char *

    //! Create a new file or overwrite existing.
    void Create_t(const FPath &fnFileName); // throw char *

    //! Close an open file.
    void Close(void);

    //! Get CRC32 of stream
    ULONG GetStreamCRC32_t(void);

    //! Read a block of data from stream.
    void Read_t(void *pvBuffer, SLONG slSize); // throw char *

    //! Write a block of data to stream.
    void Write_t(const void *pvBuffer, SLONG slSize); // throw char *

    //! Seek in stream.
    void Seek_t(SLONG slOffset, enum SeekDir sd); // throw char *

    //! Set absolute position in stream.
    void SetPos_t(SLONG slPosition); // throw char *

    //! Get absolute position in stream.
    SLONG GetPos_t(void); // throw char *

    //! Get size of stream.
    SLONG GetStreamSize(void);

    //! Check if file position points to the EOF.
    BOOL AtEOF(void);

    //! Whether or not the given pointer is coming from this stream (mainly used for exception handling).
    virtual BOOL PointerInStream(void* pPointer);

    // from CTStream
    inline virtual BOOL IsWriteable(void) { return !fstrm_bReadOnly;};
    inline virtual BOOL IsReadable(void) { return TRUE;};
    inline virtual BOOL IsSeekable(void) { return TRUE;};
};

#endif  /* include-once check. */
