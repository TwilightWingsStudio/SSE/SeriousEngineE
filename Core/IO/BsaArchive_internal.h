#pragma pack(1)

static ULONG BSA_COMPRESSED_FILE_BIT = 0x4000000;

enum BsaVersion
{
  //! Morrowind.
  BSA_VERSION_LEGACY = 1,

  //! Oblivion.
  BSA_VERSION_TES4 = 103,

  //! Fallout 3 / Fallout NV / Skyrim.
  BSA_VERSION_TES5 = 104,

  //! TESV Special Edition.
  BSA_VERSION_TES5_SE = 105,
};

enum BsaFlags
{
  //! Include Directory Names.
  BSA_FLAG_INC_DIRNAMES = (1L << 0),

  //! Include File Names.
  BSA_FLAG_INC_FILENAMES = (1L << 1),

  //! Compressed Archive. This does not mean all files are compressed. It means they are compressed by default.
  BSA_FLAG_COMPRESSED = (1L << 2),

  //! Retain Directory Names.
  BSA_FLAG_RETAIN_DIRNAMES = (1L << 3),

  //! Retain File Names.
  BSA_FLAG_RETAIN_FILENAMES = (1L << 4),

  //!	Retain File Name Offsets.
  BSA_FLAG_RETAIN_FILENAME_OFFSETS = (1L << 5),

  //! Xbox360 archive. Hash values and numbers after the header are encoded big-endian.
  BSA_FLAG_BIGENDIAN = (1L << 6),

  //! Retain Strings During Startup.
  BSA_RETAIN_STRINGS_DURING_STARTUP = (1L << 7),

  //! Embed File Names.
  BSA_FLAG_EMBED_FILE_NAMES = (1L << 8),

  //! XMem Codec. This can only be used with Bit 3 (Compress Archive). This is an Xbox 360 only compression algorithm.
  BSA_FLAG_COMPRESSED_XMEM = (1L << 9),
};

struct BsaSizeAndOffset
{
  ULONG Size;
  ULONG Offset;
};

struct BsaHeader
{
  ULONG Version;

  //! Directory tree offset;
  ULONG Offset;
  ULONG Flags;
  ULONG DirCount;
  ULONG FileCount;
  ULONG TotalDirNameLength;
  ULONG TotalFileNameLength;

  UWORD FileFlags;

  //! Dummy.
  UWORD Padding;
};

struct BsaFileRecord
{
  //! For fast search.
  UQUAD Hash;

  //! Lossless compression method.
  enum CompressionMethod eCompressionMethod;

  //! Compressed size;
  ULONG CompressedSize;

  //! Original size.
  ULONG UncompressedSize;

  //! Size of the file data. 
  ULONG Size;

  //! Offset to raw file data for this folder. Note that an "offset" is offset from file byte zero (start), NOT from this location.
  ULONG BlockOffset;

  //! Offset to the data.
  ULONG DataOffset;

  //! Name without.
  FString strName;

  //! Constructor.
  BsaFileRecord()
  {
    Hash = CompressedSize = UncompressedSize = BlockOffset = DataOffset = 0;
    eCompressionMethod = COMPRESSION_METHOD_STORE;
  }

  //! Check if compressed anyhow.
  inline bool IsCompressed() const
  {
    return eCompressionMethod != COMPRESSION_METHOD_STORE;
  }
};

struct BsaDirectory
{
  //! For fast search.
  UQUAD Hash;

  //! Offset to file records for this folder.
  //! (Subtract totalFileNameLength to get the actual offset within the file.)
  ULONG Offset;

  //! The path.
  FString strName;

  //! Files.
  TStaticArray<BsaFileRecord> Files;
};

//! BSA parsing context.
class FBsaLoaderContext : public FArchiveLoaderContext
{
  public:
    BsaHeader Header;
    TStaticArray<BsaDirectory> Directories;
    TStaticArray<FString> FileNames;

    //! Constructor.
    FBsaLoaderContext(FArchive *p)
    {
      pArchive = p;
    }
};

static void ReadBsaString_t(FDataStream &strm, FString &strValue)
{
  strValue.Clear();

  while (true)
  {
    char c;
    strm.Read(&c, 1);
    if (c == '\x00') {
      return;
    }

    strValue.PrintF("%s%c", strValue, c);
  }
}

static void ReadBsaStringLen_t(FDataStream &strm, FString &strValue)
{
  UBYTE len;
  strm >> len;

  if (len > 0) {
    FByteArray baString('\x00', len);
    strm.Read(baString.Data(), len);
    strValue = FString((char *)baString.Data());
  }
}