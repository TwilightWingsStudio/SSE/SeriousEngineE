/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FILEINDEXNODE_H
#define SE_INCL_FILEINDEXNODE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>

//! Represents archive within file tree.
class CORE_API FFileIndexNode
{
  public:
    enum Type
    {
      //! Undefined.
      FT_INVALID = 0,

      //! Just a file.
      FT_FILE,

      //! Link to file within the same file system.
      FT_HARDLINK,

      //! Directory with files.
      FT_DIRECTORY,
    };

    //! TEMP: Will be removed someday!
    enum Flags
    {
      //! Within root folder.
      FF_ROOT   = (1L << 0),

      //! Within shared folder.
      FF_SHARED = (1L << 1),

      //! Within mod folder.
      FF_MOD    = (1L << 2),
    };

  public:
    //! Default constructor.
    FFileIndexNode()
    {
      _fnmName = CTFILENAME("");
      _eType = FT_INVALID;
      _ulFlags = 0;
    }

    FFileIndexNode(const FPath &fnm, Type eType, ULONG ulFlags)
    {
      _fnmName = fnm;
      _eType = eType;
      _ulFlags = ulFlags;
    }

    //! Returns relative filename.
    inline const FPath &GetName() const
    {
      return _fnmName;
    }

    //! Setup archive filename.
    inline void SetName(const FPath &fnm)
    {
      _fnmName = fnm;
    }

    //! Returns file type.
    inline Type GetType() const
    {
      return _eType;
    }

    //! Setup archive file type.
    inline void SetType(Type eType)
    {
      _eType = eType;
    }

    inline ULONG GetFlags() const
    {
      return _ulFlags;
    }

    inline void SetFlags(ULONG ulFlags)
    {
      _ulFlags = ulFlags;
    }

  public:
    FPath _fnmName;
    enum Type _eType; 
    ULONG _ulFlags;
    TDynamicContainer<FFileIndexNode> Children;
};

#endif /* include-once check. */