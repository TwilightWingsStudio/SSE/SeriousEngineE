/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Console.h>

#include <Core/Math/Functions.h>

#include <Core/IO/FileName.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/PakArchive.h>

#include <Core/Threading/Synchronization.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

extern ULONG CalcArchiveEntryChecksum_t(FArchiveEntry *pEntry, FReadWriteDevice *pDevice);

//! Magic Id, file table offset and its size.
struct SPakHeader
{
  char tag[4];
  ULONG offset;
  ULONG size;
};

//! Entry within PAK archive file table.
struct SPakFileEntry
{
  char name[56];
  ULONG offset;
  ULONG size;
};

void FPakArchiveLoader::Load_t(FArchive *pArchive, FDataStream &strm) const
{
  FReadWriteDevice *pDevice = strm.Device(); 

  SPakHeader header;
  SPakFileEntry entry;

  pDevice->Read((char *)&header, sizeof(SPakHeader));

  FByteArray baTag(&header.tag[0], 4);

  if (baTag != "PACK") {
    ThrowF_t(TRANS("%s : Missing 'PACK' signature!"), pArchive->GetName().ConstData());
  }

  if (!pDevice->Seek(header.offset)) {
    ThrowF_t(TRANS("%s : Unable to seek the entry table!"), pArchive->GetName().ConstData());
  }

  int num_files = header.size / sizeof(SPakFileEntry);

  for (int i = 0; i < num_files; i++)
  {
    if (!pDevice->Read((char *)&entry, sizeof(SPakFileEntry))) {
      ThrowF_t(TRANS("%s : Unable to read file entry!"), pArchive->GetName().ConstData());
    }

    FByteArray baEntryName(&entry.name[0], 56);

    FArchiveEntry *pNew = new FArchiveEntry(pArchive, FString((const char *)baEntryName.ConstData()));
    pNew->SetCompressionMethod(COMPRESSION_METHOD_STORE);
    pNew->SetUncompressedSize(entry.size);
    pNew->SetOffset(entry.offset);
    pArchive->RegisterEntry(pNew);
  }

  pArchive->CalculateChecksums_t(pDevice);
}

bool FPakArchiveLoader::IsCompressionSupported(enum CompressionMethod eMethod) const
{
  return eMethod == COMPRESSION_METHOD_STORE;
}

void FPakArchiveLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".pak";
}