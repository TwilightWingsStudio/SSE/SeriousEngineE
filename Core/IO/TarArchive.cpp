/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Console.h>

#include <Core/IO/FileName.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/TarArchive.h>
#include <Core/IO/TarArchive_internal.h>

#include <Core/Threading/Synchronization.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

extern ULONG CalFArchiveEntryChecksum_t(FArchiveEntry *pEntry, FReadWriteDevice *pDevice);

void FTarArchiveLoader::Load_t(FArchive *pArchive, FDataStream &strm) const
{
  FReadWriteDevice *pDevice = strm.Device(); 

  STarHeader header;

  char* p_name = nullptr;
  size_t pos = 0;
  size_t fp_pos = 0;
  size_t fs_pos = 0;
  FByteArray baFullpath('\0', 255); // reserve 255 bytes
  FByteArray baSize('\0', 12); // reserver 12 bytes

  const size_t file_size = pDevice->Size();

  while (size_t(pos + sizeof(STarHeader)) < file_size)
  {
    pDevice->Seek(pos);
    pDevice->Read((char *)&header, sizeof(STarHeader));

    // only add standard files for now
    if (header.Link == TLI_NORMAL_FILE || TLI_NORMAL_FILE_OLD)
    {
      fp_pos = 0;
      fs_pos = 0;

      // USTAR archives have a filename prefix
      // may not be null terminated, copy carefully!
      if (!strncmp(header.Magic, "ustar", 5))
      {
        p_name = header.FileNamePrefix;
        while (*p_name && (p_name - header.FileNamePrefix) < 155)
          baFullpath[fp_pos++] = *p_name;
        p_name++;
      }

      p_name = header.FileName;
      while(*p_name && (p_name - header.FileName) < 100)
      {
        baFullpath[fp_pos++] = *p_name;
        p_name++;
      }

      p_name = header.Size;
      while(*p_name && (p_name - header.Size) < 12)
      {
        baSize[fs_pos++] = *p_name;
        p_name++;
      }

      u32 size = strtoul((const char *)baSize.ConstData(), NULL, 8);
      if (errno == ERANGE)
        ThrowF_t(TRANS("%s : File too large %s"), pArchive->GetName().ConstData(), baFullpath.ConstData());

      // save start position
      u32 offset = pos + 512;

      // move to next file header block
      pos = offset + (size / 512) * 512 + ((size % 512) ? 512 : 0);

      //printf("'%s' offset = %d, size = %d\n", baFullpath.ConstData(), offset, size);

      FArchiveEntry *pNew = new FArchiveEntry(pArchive, FString((const char *)baFullpath.ConstData()));
      pNew->SetCompressionMethod(COMPRESSION_METHOD_STORE);
      pNew->SetUncompressedSize(size);
      pNew->SetOffset(offset);
      pArchive->RegisterEntry(pNew);
    } else {
      if (header.Link == TLI_DIRECTORY) {
        // TODO: Write impl!
      }

      pos += 512;
    }
  }

  pArchive->CalculateChecksums_t(pDevice);
}

bool FTarArchiveLoader::IsCompressionSupported(enum CompressionMethod eMethod) const
{
  return eMethod == COMPRESSION_METHOD_STORE;
}

void FTarArchiveLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".tar";
}