/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ARCHIVEMANAGER_H
#define SE_INCL_ARCHIVEMANAGER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>
#include <Core/Templates/NameTable.h>
#include <Core/Templates/StaticStackArray.h>
#include <Core/Threading/Synchronization.h>

class CORE_API FArchiveManager
{
  public:
    //! Constructor.
    FArchiveManager();
    
    //! Destructor.
    ~FArchiveManager();

    //! Make loader available to be used.
    void RegisterLoader(FArchiveLoader *pLoader);

    //! Obtain an archive by its filename.
    FArchive *Obtain_t(const FPath &fnm);

    //! Unmount the archive from the file system.
    void Release(FArchive *pArchive);

    //! Query one or more files from archives.
    INDEX QueryFiles(const FPath &fnm, TDynamicContainer<FArchiveEntry> &entries, BOOL bModSearch, INDEX ctMaxFiles = -1);

    //! Close unused archives.
    void CloseUnused();

    //! Sort by archive id.
    static void SortByArchiveId(TDynamicContainer<FArchive> &cInput);

    //! Set of loaders for various archive formats.
    inline TDynamicContainer<FArchiveLoader> &GetLoaders()
    {
      return _ctLoaders;
    }

    void RegisterDirectory(const FPath &fnm, ULONG ulFlags);

    inline TDynamicContainer<FFileIndexNode> &GetDirectories()
    {
      return _ctArchiveDirs;
    }

    void SearchArchives();
    
  public:
    CSyncMutex _csManager; // make it thread safe
    ULONG _idNextArchive;
    TDynamicContainer<FArchive> _ctObjects;
    TNameTable<FArchive> _ntArchives;
    TDynamicContainer<FArchiveLoader> _ctLoaders;
    TDynamicContainer<FFileIndexNode> _ctArchiveDirs;
};

CORE_API extern FArchiveManager *_pArchiveMgr;

#endif  /* include-once check. */