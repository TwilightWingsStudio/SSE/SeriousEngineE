/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FILEDEVICE_H
#define SE_INCL_FILEDEVICE_H

#include <stdio.h>

#include <Core/Base/String.h>
#include <Core/IO/ReadWriteDevice.h>

class CORE_API FFileDevice final : public FReadWriteDevice
{
  public:
    //! Default constructor.
    FFileDevice();

    //! Constructor.
    FFileDevice(const FString &path);

    //! Destructor.
    ~FFileDevice() override;

    //! Set new path to the file.
    virtual bool SetFilename(const FString &path);

    //! Start interacting in given mode.
    bool Open(OpenMode om) override;

    //! End interacting with.
    void Close() override;

    //! Checks if carret at end.
    bool AtEnd() const override;

    //! Returns carret position.
    size_t Pos() const override;

    //! Size of the device.
    size_t Size() const override;

    //! Tries to move carret to specified position.
    bool Seek(size_t pos) override;

    //! Move formard.
    size_t Skip(size_t max_size) override;

    //! Read bytes from file.
    size_t Read(char *pData, size_t max_size) override;

    //! Read bytes without moving carret formard.
    size_t Peek(char *pData, size_t max_size) override;

    //! Write bytes into file.
    size_t Write(const char *pData, size_t max_size) override;

    //! Check device type.
    virtual Type GetType() const
    {
      return TYPE_FILE;
    }
  
  // File specific.
  public:
    //! Returns current filename.
    FString Filename() const;

    //! Checks if file exists on disk.
    bool Exists() const;

    //! Tries to remove the file from disk.
    bool Remove();

    //! Tries to rename file on disk
    bool Rename(const FString &new_name);

    //! Returns pointer to native file object.
    inline FILE *GetFileObject()
    {
      return _pFile;
    }

  protected:
    FILE *_pFile;
    FString _strFilename;
};

#endif /* include-once check */