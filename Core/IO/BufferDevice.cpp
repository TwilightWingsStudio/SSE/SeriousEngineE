/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/BufferDevice.h>
#include <string>

FBufferDevice::FBufferDevice()
{
  _pData = nullptr;
  _pos = 0;
}

FBufferDevice::FBufferDevice(FByteArray *pByteArray)
{
  _pData = pByteArray;
  _pos = 0;
}

bool FBufferDevice::Open(OpenMode om)
{
  if (_pData == nullptr) {
    return false;
  }

  _eOpenMode = om;
  _pos = 0;
  return IsOpen();
}

void FBufferDevice::Close()
{
  _eOpenMode = OM_NOTOPEN;
  _pos = 0;
}

size_t FBufferDevice::Pos() const
{
  if (!IsOpen()) {
    return -1;
  }

  return _pos;
}

bool FBufferDevice::AtEnd() const
{
  return Pos() >= Size();
}

size_t FBufferDevice::Size() const
{
  return _pData->Size();
}

bool FBufferDevice::Seek(size_t pos)
{
  if (_pData == nullptr || IsOpen()) {
    return false;
  }

  // If farther than AtEnd...
  if (pos > _pData->Size()) {
    return false;
  }

  _pos = pos;
  return true;
}

size_t FBufferDevice::Skip(size_t max_size)
{
  _pos += max_size;
  return max_size;
}

size_t FBufferDevice::Read(char *pData, size_t max_size)
{
  if (pData == nullptr || _pData == nullptr || _pData->IsNull()) {
    return -1;
  }

  if (AtEnd()) {
    return 0;
  }

  size_t expected_pos = _pos + max_size;

  // If will be farther than AtEnd...
  if (expected_pos > _pData->Size()) {
    size_t not_enough = expected_pos - _pData->Size();
    max_size -= not_enough;
  }

  memcpy(pData, &_pData->ConstData()[_pos], max_size);
  _pos += max_size;
  return max_size;
}

size_t FBufferDevice::Peek(char *pData, size_t max_size)
{
  size_t pos_before = Pos();
  size_t len = Read(pData, max_size);
  Seek(pos_before);
  return len;
}

size_t FBufferDevice::Write(const char *pData, size_t max_size)
{
  if (pData == NULL || _pData == nullptr || !IsWritable()) {
    return -1;
  }

  if (_pos + max_size >= _pData->Size()) {
    _pData->Resize(_pData->Size() + max_size);
  }

  memcpy(&_pData->Data()[_pos], pData, max_size);
  _pos += max_size;
  return max_size;
}

void FBufferDevice::SetBuffer(FByteArray *pData)
{
  if (IsOpen()) {
    return;
  }

  _pData = pData;
}