/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ARCHIVEFORMAT_H
#define SE_INCL_ARCHIVEFORMAT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! List of known archive formats.
enum ArchiveFormat
{
  //! Erroring placeholder.
  ARCHIVE_FORMAT_INVALID = 0,

  //! Zips they are!
  ARCHIVE_FORMAT_ZIP,

  //! Tarball.
  ARCHIVE_FORMAT_TAR,

  //! Optical Disk Image.
  ARCHIVE_FORMAT_ISO,

  //! PAK around idTech engines.
  ARCHIVE_FORMAT_IDPAK,

  //! Packages used in UE-based games.
  ARCHIVE_FORMAT_UNREAL,

  //! Bethesda Software Archive.
  ARCHIVE_FORMAT_BSA,

  //! Valve Pak.
  ARCHIVE_FORMAT_VPK,

  //! Group File. Used by Build engine and its sourceport.
  ARCHIVE_FORMAT_GRP,

  //! IWAD/PWAD, WAD2, WAD3.
  ARCHIVE_FORMAT_WAD,
};

#endif /* include-once check. */