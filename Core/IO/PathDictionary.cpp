/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Core.h>
#include <Core/Base/Memory.h>
#include <Core/Base/Console.h>
#include <Core/Base/Shell.h>
#include <Core/IO/Stream.h>
#include <Core/IO/ResourceStock.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicStackArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

FPathDictionary::FPathDictionary() : dict_ntDictionary(*new TNameTable_FPathDictionaryEntry)
{
  dict_slDictionaryPos = 0;
  dict_dmDictionaryMode = DM_NONE;
}

FPathDictionary::~FPathDictionary()
{
  Clear();
}

void FPathDictionary::Copy(FPathDictionary &other)
{
  dict_afnmDictionary = other.dict_afnmDictionary;

  for (INDEX i = 0; i < dict_afnmDictionary.Count(); i++) {
    dict_ntDictionary.Add(&dict_afnmDictionary[i]);
  }
}

void FPathDictionary::Clear()
{
  // clear dictionary vars
  dict_dmDictionaryMode = DM_NONE;
  dict_ntDictionary.Clear();
  dict_afnmDictionary.Clear();
  dict_slDictionaryPos = 0;
}

// enable dictionary in writable file from this point
void FPathDictionary::WriteBegin_t(CTStream *pStream, const FPath &fnmImportFrom, SLONG slImportOffset)
{ 
  ASSERT(dict_slDictionaryPos == 0);
  ASSERT(dict_dmDictionaryMode == DM_NONE);
  dict_ntDictionary.SetAllocationParameters(100, 5, 5);
  dict_ctDictionaryImported = 0;

  // if importing an existing dictionary to start with
  if (fnmImportFrom != "")
  {
    // open that file
    CTFileStream strmOther;
    strmOther.Open_t(fnmImportFrom);
    // read the dictionary in that stream
    strmOther.ReadDictionary_intenal_t(slImportOffset);
    // copy the dictionary here
    pStream->CopyDictionary(strmOther);
    // write dictionary importing data
    pStream->WriteID_t("DIMP");  // dictionary import
    (*pStream) << fnmImportFrom << slImportOffset;
    // remember how many filenames were imported
    dict_ctDictionaryImported = dict_afnmDictionary.Count();
  }

  // write dictionary position chunk id
  pStream->WriteID_t("DPOS");  // dictionary position
  dict_slDictionaryPos = pStream->GetPos_t(); // remember where position will be placed
  (*pStream) << SLONG(0); // leave space for position

  // start dictionary
  dict_dmDictionaryMode = DM_ENABLED;
}

void FPathDictionary::WriteEnd_t(CTStream *pStream)
{
  ASSERT(dict_dmDictionaryMode == DM_ENABLED);
  ASSERT(dict_slDictionaryPos > 0);
  // remember the dictionary position chunk position
  SLONG slDictPos = dict_slDictionaryPos;
  // mark that now saving dictionary
  dict_slDictionaryPos = -1;
  // remember where dictionary begins
  SLONG slDictBegin = pStream->GetPos_t();
  // start dictionary processing
  dict_dmDictionaryMode = DM_PROCESSING;

  pStream->WriteID_t("DICT");  // dictionary

  // write number of used filenames
  INDEX ctFileNames = dict_afnmDictionary.Count();
  INDEX ctFileNamesNew = ctFileNames - dict_ctDictionaryImported;
  (*pStream) << ctFileNamesNew;

  // for each filename
  for (INDEX iFileName = dict_ctDictionaryImported; iFileName < ctFileNames; iFileName++)
  {
    FPathDictionaryEntry &de = dict_afnmDictionary[iFileName];
    (*pStream) << dict_afnmDictionary[iFileName].GetName(); // write it to disk
  }

  pStream->WriteID_t("DEND");  // dictionary end

  // remember where is end of dictionary
  SLONG slContinue = pStream->GetPos_t();

  // write the position back to dictionary position chunk
  pStream->SetPos_t(slDictPos);
  (*pStream) << slDictBegin;

  // stop dictionary processing
  dict_dmDictionaryMode = DM_NONE;
  dict_ntDictionary.Clear();
  dict_afnmDictionary.Clear();

  // return to end of dictionary
  pStream->SetPos_t(slContinue);
  dict_slDictionaryPos = 0;
}

// read the dictionary from given offset in file (internal function)
void FPathDictionary::Read_intenal_t(CTStream *pStream, SLONG slOffset)
{
  // remember where to continue loading
  SLONG slContinue = pStream->GetPos_t();

  // go to dictionary beginning
  pStream->SetPos_t(slOffset);

  // start dictionary processing
  dict_dmDictionaryMode = DM_PROCESSING;

  pStream->ExpectID_t("DICT");  // dictionary

  // read number of new filenames
  INDEX ctFileNamesOld = dict_afnmDictionary.Count();
  INDEX ctFileNamesNew;
  (*pStream) >> ctFileNamesNew;

  // if there are any new filenames
  if (ctFileNamesNew > 0)
  {
    // create that much space
    dict_afnmDictionary.Push(ctFileNamesNew);

    // for each filename
    for (INDEX iFileName = ctFileNamesOld; iFileName < ctFileNamesOld+ctFileNamesNew; iFileName++)
    {
      FPathDictionaryEntry &de = dict_afnmDictionary[iFileName];

      #define SE_DICT_PATHS_FIXER

      #ifdef SE_DICT_PATHS_FIXER
      FPath fnmAsset;
      
      (*pStream) >> fnmAsset; // Read it.
      de.SetName(fnmAsset);

      // [SEE] Rev Import
      char *pcSrc = fnmAsset.Data();
      char *pcDst = de.m_fnmPath.Data();

      memset(pcDst, 0, strlen(pcDst));
      
      // Cycle through all chars...
      while (*pcSrc != 0)
      {
        // Fix all / slashes to \ //
        if (*pcSrc == '/') {
          *pcSrc = '\\';

          if (!_bWorldEditorApp) {
            ThrowF_t("ERROR! Wrong slashes '/' used for assets paths written inside of WLD file!\nI can guess this map was made with shitty SSR.\nYou can try to resave the map with editor to fix it.\n");
          }
        }
        
        char *pNext = pcSrc + 1; // Get pointer to next char.

        // If we have slash and next char is slash.
        if (*pcSrc == '\\' && (*pNext == '/' || *pNext == '\\')) {
          pcSrc++;
          
          if (!_bWorldEditorApp) {
            ThrowF_t("ERROR! Multiple slashes \\\\ were used for assets paths written inside of WLD file.\nI can guess this map was made with shitty SSR.\nYou can try to resave the map with editor to fix it.\n");
          }
          
          continue;
        }
        
        // Add plain text.
        *pcDst = *pcSrc;
        pcSrc++;
        pcDst++;
      }
      
      if (de.GetName().HasPrefix("\\")) {
        de.m_fnmPath.RemovePrefix("\\");
      }

      #else
      (*pStream) >> de.m_fnmPath; // Default Code.
      #endif
    }
  }

  pStream->ExpectID_t("DEND");  // dictionary end

  // remember where end of dictionary is
  dict_slDictionaryPos = pStream->GetPos_t();

  // return to continuing position
  pStream->SetPos_t(slContinue);
}

SLONG FPathDictionary::ReadBegin_t(CTStream *pStream)
{
  ASSERT(dict_dmDictionaryMode == DM_NONE);
  ASSERT(dict_slDictionaryPos == 0);
  dict_ntDictionary.SetAllocationParameters(100, 5, 5);

  SLONG slImportOffset = 0;

  // if there is imported dictionary
  if (pStream->PeekID_t() == CChunkID("DIMP")) {  // dictionary import
    // read dictionary importing data
    pStream->ExpectID_t("DIMP");  // dictionary import
    FPath fnmImportFrom;
    (*pStream) >> fnmImportFrom >> slImportOffset;

    // open that file
    CTFileStream strmOther;
    strmOther.Open_t(fnmImportFrom);
    // read the dictionary in that stream
    strmOther.ReadDictionary_intenal_t(slImportOffset);
    // copy the dictionary here
    pStream->CopyDictionary(strmOther);
  }

  // if the dictionary is not here then do nothing
  if (pStream->PeekID_t() != CChunkID("DPOS")) {  // dictionary position
    return 0;
  }

  // read dictionary position
  pStream->ExpectID_t("DPOS"); // dictionary position
  SLONG slDictBeg;
  (*pStream) >> slDictBeg;

  // read the dictionary from that offset in file
  pStream->ReadDictionary_intenal_t(slDictBeg);

  // stop dictionary processing - go to dictionary using
  dict_dmDictionaryMode = DM_ENABLED;

  // return offset of dictionary for later cross-file importing
  if (slImportOffset != 0) {
    return slImportOffset;
  } else {
    return slDictBeg;
  }
}

void FPathDictionary::ReadEnd_t(CTStream *pStream)
{
  if (dict_dmDictionaryMode != DM_ENABLED) {
    return;
  }
    
  ASSERT(dict_slDictionaryPos > 0);

  // just skip the dictionary (it was already read)
  pStream->SetPos_t(dict_slDictionaryPos);
  dict_slDictionaryPos = 0;
  dict_dmDictionaryMode = DM_NONE;
  dict_ntDictionary.Clear();

  // for each filename
  INDEX ctFileNames = dict_afnmDictionary.Count();

  for (INDEX iFileName = 0; iFileName < ctFileNames; iFileName++)
  {
    FPathDictionaryEntry &de = dict_afnmDictionary[iFileName];

    // if not preloaded then skip
    if (de.m_pPreloaded == NULL) {
      continue;
    }

    // free preloaded instance
    FString strExt = de.GetName().FileExt();

    // TODO: Fix me!
    if (strExt == ".tex") {
      _pTextureStock->Release(de.m_pPreloaded);
    } else if (strExt == ".mdl") {
      _pModelStock->Release(de.m_pPreloaded);
    } else if (strExt==".smc") {
      _pModelConfigStock->Release(de.m_pPreloaded);
    }
  }

  dict_afnmDictionary.Clear();
}

extern void (*_pCallProgressHook_t)(FLOAT fProgress);

void FPathDictionary::Preload_t(CTStream *pStream)
{
  INDEX ctFileNames = dict_afnmDictionary.Count();

  // for each filename
  for (INDEX iFileName = 0; iFileName < ctFileNames; iFileName++)
  {
    FPathDictionaryEntry &de = dict_afnmDictionary[iFileName];

    // preload it
    FString strExt = de.GetName().FileExt();

    if (_pCallProgressHook_t != nullptr) {
      _pCallProgressHook_t(FLOAT(iFileName) / ctFileNames); // update progress
    }

    try {
      // TODO: Fix me!
      if (strExt == ".tex") {
        de.m_pPreloaded = _pTextureStock->Obtain_t(de.GetName());
      } else if (strExt == ".mdl") {
        de.m_pPreloaded = _pModelStock->Obtain_t(de.GetName());
      } else if (strExt==".smc") {
        de.m_pPreloaded = _pModelConfigStock->Obtain_t(de.GetName());
      }

    } catch (char *strError) {
      CErrorF(TRANS("Cannot preload %s: %s\n"), (FString&)de.GetName(), strError);
    }
  }
}

BOOL FPathDictionary::Add(const FPath &fnm, ULONG &idx)
{
  // try to find the filename in dictionary
  BOOL bWasAdded = FALSE;
  FPathDictionaryEntry *pfnmExisting = dict_ntDictionary.Find(fnm);

  // if not existing
  if (pfnmExisting == NULL) {
    // add it
    pfnmExisting = &dict_afnmDictionary.Push();
    pfnmExisting->SetName(fnm);
    dict_ntDictionary.Add(pfnmExisting);
    bWasAdded = TRUE;
  }

  idx = dict_afnmDictionary.Index(pfnmExisting);
  return bWasAdded;
}

BOOL FPathDictionary::Get(FPath &fnm, ULONG idx)
{
  if (idx < dict_afnmDictionary.Count()) {
    fnm = dict_afnmDictionary[idx].GetName();
    return TRUE;
  }

  return FALSE;
}