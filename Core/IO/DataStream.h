/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_DATASTREAM_H
#define SE_INCL_DATASTREAM_H

#include <Core/Base/Types.h>
#include <Core/IO/ReadWriteDevice.h>

//! Inteface for stream class fusion.
class CORE_API IDataStream
{
  public:
    virtual void ReadString_t(FString &str) = 0;
    virtual void WriteString_t(const FString &str) = 0;
    virtual void ReadPath_t(FPath &fnm) = 0;
    virtual void WritePath_t(const FPath &fnm) = 0;
};

class CORE_API FDataStream : public IDataStream
{
  public:
    enum ByteOrder
    {
      BO_BIGENDIAN,
      BO_LITTLEENDIAN
    };

    enum Status {
      //! No error.
      STATUS_OK,

      //! Failed to read - out of device size.
      STATUS_READPASTEND,

      //! Failed to write.
      STATUS_WRITEFAILED,
    };

  public:
    //! Construct dummy data stream.
    FDataStream();

    //! Construct data stream for device.
    FDataStream(FReadWriteDevice *d);
    
    //! Construct read-only data stream for FByteArray.
    FDataStream(const FByteArray &ba);
    
    //! Construct data stream for FByteArray.
    FDataStream(FByteArray *pba, FReadWriteDevice::OpenMode om);
    
    //! Destructor.
    virtual ~FDataStream();

    //! Returns current ByteOrder used for io
    inline enum ByteOrder GetByteOrder() const
    {
      return _eByteOrder;
    }

    //! Change ByteOrder used for io.
    void SetByteOrder(enum ByteOrder bo);

    //! Wraps around FReadWriteDevice::AtEnd.
    bool AtEnd() const;

    //! Returns device.
    inline FReadWriteDevice *Device()
    {
      return _pDevice;
    }

    //! Returns status.
    inline enum Status GetStatus()
    {
      return _eStatus;
    }
    
    //! Change active device.
    void SetDevice(FReadWriteDevice *d);

    //! Change status. Works only if current status is STATUS_OK.
    void SetStatus(enum Status eStatus);
    
    //! Toggle the exception mode.
    void SetExceptionMode(bool bExceptionMode);

    //! Change status to STATUS_OK.
    void ResetStatus();

    //! Read from device.
    size_t Read(void *pBuffer, size_t len);
    
    //! Read from device.
    FByteArray Read(size_t len);

    //! Write to device.
    size_t Write(const void *pData, size_t len);
    
    //! Write to device.
    size_t Write(const FByteArray &baData);

    //! Skip data.
    size_t Skip(size_t len);
    
    //! Read without moving carret forward.
    size_t Peek(void *pBuffer, size_t len);

    //! Read without moving carret forward.
    FByteArray Peek(size_t len);

    // Read methods.
    FDataStream &operator>>(u8  &dst);
    FDataStream &operator>>(u16 &dst);
    FDataStream &operator>>(u32 &dst);
    FDataStream &operator>>(u64 &dst);
    FDataStream &operator>>(s8  &dst);
    FDataStream &operator>>(s16 &dst);
    FDataStream &operator>>(s32 &dst);
    FDataStream &operator>>(s64 &dst);
    FDataStream &operator>>(f32 &dst);
    FDataStream &operator>>(f64 &dst);

    // Write methods.
    FDataStream &operator<<(u8  src);
    FDataStream &operator<<(u16 src);
    FDataStream &operator<<(u32 src);
    FDataStream &operator<<(u64 src);
    FDataStream &operator<<(s8  src);
    FDataStream &operator<<(s16 src);
    FDataStream &operator<<(s32 src);
    FDataStream &operator<<(s64 src);
    FDataStream &operator<<(f32 src);
    FDataStream &operator<<(f64 src);

  // compat
  #ifdef _WIN32
    FDataStream &operator>>(INDEX &dst)
    {
      return (*this) >> (s32 &)dst;
    }

    FDataStream &operator>>(ULONG &dst)
    {
      return (*this) >> (u32 &)dst;
    }

    FDataStream &operator<<(INDEX src)
    {
      return (*this) << (s32)src;
    }

    FDataStream &operator<<(ULONG src)
    {
      return (*this) << (u32)src;
    }
  #endif

    // [SEE]
    void ReadString_t(FString &str) override;
    void WriteString_t(const FString &str) override;
    void ReadPath_t(FPath &fnm) override;
    void WritePath_t(const FPath &fnm) override;

  protected:
    FReadWriteDevice *_pDevice;
    enum ByteOrder _eByteOrder;
    enum Status _eStatus;
    bool _bExceptionMode;
    bool _bHasOwnDevice;
};

#endif /* include-once check */