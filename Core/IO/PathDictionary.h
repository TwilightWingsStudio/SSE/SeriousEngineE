/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PATHDICTIONARY_H
#define SE_INCL_PATHDICTIONARY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/String.h>
#include <Core/IO/FileName.h>
#include <Core/Templates/DynamicStackArray.h>

class CORE_API FPathDictionaryEntry
{
  public:
    FPath m_fnmPath;
    CResource *m_pPreloaded;

  public:
    //! Default constructor.
    FPathDictionaryEntry()
    {
      Clear();
    }

    FPathDictionaryEntry(const FPath &fnm)
    {
      Clear();
      m_fnmPath = fnm;
    }

    //! Filename is its own name (used for storing in nametable).
    inline const FPath &GetName(void)
    {
      return m_fnmPath;
    };

    inline void SetName(const FPath &fnm)
    {
      m_fnmPath = fnm;
    }

    //! Clear the object.
    inline void Clear(void)
    {
      m_fnmPath.Clear();
      m_pPreloaded = NULL;
    }
};

class FPathDictionary
{
  public:
    enum DictionaryMode {
      DM_NONE,        // no dictionary on this file (yet)
      DM_ENABLED,     // dictionary is enabled, reading/writing rest of file
      DM_PROCESSING,  // reading/writing the dictionary itself
    };
  
  public:
    // Dictionary system.
    enum DictionaryMode dict_dmDictionaryMode;    // dictionary mode of operation on this file
    SLONG dict_slDictionaryPos; // dictionary position in file (0 for no dictionary)
    INDEX dict_ctDictionaryImported;  // how many filenames were imported
    TNameTable_FPathDictionaryEntry &dict_ntDictionary;  // name table for the dictionary
    TDynamicStackArray<FPathDictionaryEntry> dict_afnmDictionary; // dictionary is stored here
  
  public:
    //! Constructor.
    FPathDictionary();
    
    //! Destructor.
    ~FPathDictionary();
    
    //! Copy data.
    void Copy(FPathDictionary &other);
    
    //! Free memory.
    void Clear();

    void WriteBegin_t(CTStream *pStream, const FPath &fnmImportFrom, SLONG slImportOffset);
    void WriteEnd_t(CTStream *pStream);
    void Read_intenal_t(CTStream *pStream, SLONG slOffset);
    SLONG ReadBegin_t(CTStream *pStream);
    void ReadEnd_t(CTStream *pStream);
    void Preload_t(CTStream *pStream);

    BOOL Add(const FPath &fnm, ULONG &idx);
    BOOL Get(FPath &fnm, ULONG idx);
};

#endif  /* include-once check. */