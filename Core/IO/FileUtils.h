/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FILEUTILS_H
#define SE_INCL_FILEUTILS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

// Test if a file exists.
CORE_API BOOL FileExists(const FPath &fnmFile);

//! Test if a file exists for writing. 
/*!
 (this is can be diferent than normal FileExists() if a mod uses basewriteexclude.lst
*/
CORE_API BOOL FileExistsForWriting(const FPath &fnmFile);

//! Get file timestamp.
CORE_API SLONG GetFileTimeStamp_t(const FPath &fnmFile); // throw char *

//! Get CRC32 of a file.
CORE_API ULONG GetFileCRC32_t(const FPath &fnmFile); // throw char *

//! Test if a file is read only (also returns FALSE if file does not exist).
CORE_API BOOL IsFileReadOnly(const FPath &fnmFile);

//! Delete a file (called 'remove' to avid name clashes with win32).
CORE_API BOOL RemoveFile(const FPath &fnmFile);

// Expand a file's filename to full path

// these are input flags for describing what you need the file for
#define EFP_READ   (1UL<<0)  // will open for reading
#define EFP_WRITE  (1UL<<1)  // will open for writing
#define EFP_NOZIPS (1UL<<31) // add this flag to forbid searching in zips

// these are return values 
#define EFP_NONE       0  // doesn't exist
#define EFP_FILE       1  // generic file on disk
#define EFP_BASEZIP    2  // file in one of base zips
#define EFP_MODZIP     3  // file in one of mod zips

CORE_API INDEX ExpandFilePath(ULONG ulType, const FPath &fnmFile, FPath &fnmExpanded);

// these are input flags for directory reading
#define DLI_RECURSIVE  (1UL<<0)  // recurse into subdirs
#define DLI_SEARCHCD   (1UL<<1)  // search the CD path also

//! Make a list of all files in a directory.
CORE_API void MakeDirList(
  TDynamicStackArray<FPath> &adeDir, 
  const FPath &fnmDir,     // directory to list
  const FString &strPattern,   // pattern for each file to match ("" matches all)
  ULONG ulFlags                 // additional flags
);

//! Application path usage funtions.
CORE_API void UseApplicationPath(void);
CORE_API void IgnoreApplicationPath(void);

#endif  /* include-once check. */