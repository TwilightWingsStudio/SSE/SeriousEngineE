/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Console.h>

#include <Core/Math/Functions.h>

#include <Core/IO/FileName.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/WadArchive.h>
#include <Core/IO/WadArchive_internal.h>

#include <Core/Threading/Synchronization.h>

#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>
#include <Core/Templates/StaticArray.cpp>

extern ULONG CalFArchiveEntryChecksum_t(FArchiveEntry *pEntry, FReadWriteDevice *pDevice);

static int IsMarker(const char *marker, const char *name)
{
  return !strnicmp(name, marker, 8) ||
    (*name == *marker && !strnicmp(name+1, marker, 7));
}


static CompressionMethod ConvertWadComressionMethod(int iMethod)
{
  switch (iMethod)
  {
    case WAD_COMPRESSION_STORE: return COMPRESSION_METHOD_STORE;
    case WAD_COMPRESSION_LZSS: return COMPRESSION_METHOD_LZSS;
  }

  return COMPRESSION_METHOD_INVALID;
}

static void SetNamespace(FWadDoomLoaderContext &ctx, const char *pStartMarker, const char *pEndMarker, EWadDoomLumpNamespace eNamespace, bool bFlathack = false)
{
  TStaticStackArray<SWadMarker> Markers;

  FString &strArchiveName = (FString&)ctx.pArchive->GetName();

	bool isWarned = false;
	int ctStartMarkers = 0, ctEndMarkers = 0;

  // Find all the markers...
	for (int j = 0; j < ctx.Lumps.Count(); j++)
	{
    SWadDoomFileEntry &lump = ctx.Lumps[j];

		if (IsMarker(&lump.Name[0], pStartMarker)) {
      SWadMarker &m = Markers.Push();
      m.isClosing = false;
      m.Index = j;
			ctStartMarkers++;
		} else if (IsMarker(&lump.Name[0], pEndMarker)) {
      SWadMarker &m = Markers.Push();
      m.isClosing = true;
      m.Index = j;
			ctEndMarkers++;
		}
	}

	if (ctStartMarkers == 0)
	{
    // if no markers found...
		if (ctEndMarkers == 0) {
      return;	
    }

		CWarningF("%s : '%s' marker without corresponding '%s' found.\n", strArchiveName, pEndMarker, pStartMarker);
		return;
	}

	unsigned int i = 0;
	while (i < Markers.Count())
	{
		int iStartPos, iEndPos;
		if (Markers[i].isClosing)
		{
			CWarningF("%s : '%s' marker without corresponding '%s' found.\n", strArchiveName, pEndMarker, pStartMarker);
			i++;
			continue;
		}

		iStartPos = i++;

		// skip over subsequent x_START markers
		while (i < Markers.Count() && !Markers[i].isClosing)
		{
			CWarningF("%s : duplicate '%s' marker found.\n", strArchiveName, pStartMarker);
			i++;
			continue;
		}

		// same for x_END markers
		while (i < Markers.Count()-1 && (Markers[i].isClosing && Markers[i+1].isClosing))
		{
			CWarningF("%s : duplicate '%s' marker found.\n", strArchiveName, pEndMarker);
			i++;
			continue;
		}

		// We found a starting marker but no end marker. Ignore this block.
		if (i >= Markers.Count()) {
			CWarningF("%s : '%s' marker without corresponding '%s' found.\n", strArchiveName, pStartMarker, pEndMarker);
			iEndPos = ctx.Lumps.Count();
		} else {
			iEndPos = Markers[i++].Index;
		}

		// Finally! We found a marked block!
		CDebugF("%s : Found '%s' block at (%d-%d)\n", strArchiveName, pStartMarker, Markers[iStartPos].Index, iEndPos);
  
    // Set corresponding namespace for lumps within block.
		for(int j = Markers[iStartPos].Index + 1; j < iEndPos; j++)
		{
      SWadDoomLumpInfo &info = ctx.LumpInfos[j];
      SWadDoomFileEntry &lump = ctx.Lumps[j];

			if (info.Namespace != WAD_DOOM_NAMESPACE_GLOBAL) {
				if (!isWarned) {
					CWarningF("%s : Overlapping namespaces found (lump %d)\n", strArchiveName, j);
				}

				isWarned = true;

			} else if (eNamespace == WAD_DOOM_NAMESPACE_SPITES && lump.Size < 8) {
				// sf 26/10/99:
				// ignore sprite lumps smaller than 8 bytes (the smallest possible)
				// in size -- this was used by some dmadds wads
				// as an 'empty' graphics resource
				CDebugF("%s : Skipped empty sprite '%s' (lump %d)\n", strArchiveName, lump.Name, j);
			} else {
				info.Namespace = eNamespace;
			}
		}
	}
}

static bool IsMapLump(FByteArray &name)
{
  static char *NAMES[] = {
    "THINGS",
    "LINEDEFS",
    "SIDEDEFS",
    "VERTEXES",
    "SEGS",
    "SSECTORS",
    "NODES",
    "SECTORS",
    "REJECT",
    "BLOCKMAP",
    "BEHAVIOR",
    "SCRIPTS",
  };

  for (int i = 0; i < ARRAYCOUNT(NAMES); i++)
  {
    if (name.StartsWith(NAMES[i])) {
      return true;
    }
  }
  
  return false;
}

static void ProcessDoomLumps_t(FWadDoomLoaderContext &ctx)
{
  SetNamespace(ctx, "S_START", "S_END", WAD_DOOM_NAMESPACE_SPITES);
  SetNamespace(ctx, "F_START", "F_END", WAD_DOOM_NAMESPACE_FLATS, true);
  SetNamespace(ctx, "C_START", "C_END", WAD_DOOM_NAMESPACE_COLORMAPS);
  SetNamespace(ctx, "A_START", "A_END", WAD_DOOM_NAMESPACE_ASCLIBRARY);
  SetNamespace(ctx, "TX_START", "TX_END", WAD_DOOM_NAMESPACE_NEWTEXTURES);
  SetNamespace(ctx, "V_START", "V_END", WAD_DOOM_NAMESPACE_STRIFEVOICES);
  SetNamespace(ctx, "HI_START", "HI_END", WAD_DOOM_NAMEPSACE_HIRES);
  SetNamespace(ctx, "VX_START", "VX_END", WAD_DOOM_NAMESPACE_VOXELS);

  FByteArray baLastMap;

  for (int i = 0; i < ctx.Lumps.Count(); i++)
  {
    SWadDoomFileEntry &entry = ctx.Lumps[i];
    SWadDoomLumpInfo &info = ctx.LumpInfos[i];

    FByteArray baEntryName(&entry.Name[0], 8);
    FString strLumpName((const char *)baEntryName.ConstData());

    const char *pNamespaceName = WadGetNamespaceName(info.Namespace);

    // MAPXX or ExMy
    if (baEntryName.StartsWith("MAP") || (baEntryName[0] == 'E' && baEntryName[2] == 'M')) {
      baLastMap = baEntryName;
    }

    if (IsMapLump(baEntryName)) {
      strLumpName.PrintF("%s\\%s", baLastMap.ConstData(), strLumpName);
    }

    if (info.Namespace != WAD_DOOM_NAMESPACE_GLOBAL) {
      strLumpName.PrintF("%s\\%s", WadGetNamespaceName(info.Namespace), strLumpName);
    }

    strLumpName += ".lmp";

    FArchiveEntry *pNew = new FArchiveEntry(ctx.pArchive, strLumpName);
    pNew->SetCompressionMethod(COMPRESSION_METHOD_STORE);
    pNew->SetUncompressedSize(entry.Size);
    pNew->SetOffset(entry.Offset);
    ctx.pArchive->RegisterEntry(pNew);
  }
}

static void ProcessLumps_t(FWadLoaderContext &ctx, EWadFormat eWadFormat)
{
  for (int i = 0; i < ctx.Lumps.Count(); i++)
  {
    SWadFileEntry &entry = ctx.Lumps[i];

    FByteArray baEntryName(&entry.Name[0], 16);
    FString strEntryName;
    FString strType;

    if (eWadFormat == WAD_FORMAT_QUAKE) {
      switch (entry.Type)
      {
        case WAD_ENTRY_QUAKE_PALETTE: strType = "palette"; break;
        case WAD_ENTRY_QUAKE_QPIC:    strType = "qpic"; break;
        case WAD_ENTRY_QUAKE_MIPTEX:  strType = "miptex"; break;
        case WAD_ENTRY_QUAKE_FONT:    strType = "font"; break;

        default:
          strType.PrintF("%02d", entry.Type);
      }
    } else if (eWadFormat == WAD_FORMAT_GOLDSRC) {
      switch (entry.Type)
      {
        case WAD_ENTRY_GOLDSRC_QPIC:   strType = "qpic"; break;
        case WAD_ENTRY_GOLDSRC_MIPTEX: strType = "wal2"; break;
        case WAD_ENTRY_GOLDSRC_FONT:   strType = "font"; break;

        default:
          strType.PrintF("%02d", entry.Type);
      }
    }

    strEntryName.PrintF("%s.%s", (const char *)baEntryName.ConstData(), strType);

    FArchiveEntry *pNew = new FArchiveEntry(ctx.pArchive, strEntryName);
    pNew->SetCompressionMethod(ConvertWadComressionMethod(entry.CompressionMethod));
    pNew->SetCompressedSize(entry.CompressedSize);
    pNew->SetUncompressedSize(entry.UncompressedSize);
    pNew->SetOffset(entry.Offset);
    ctx.pArchive->RegisterEntry(pNew);
  }
}

void FWadArchiveLoader::Load_t(FArchive *pArchive, FDataStream &strm) const
{
  FReadWriteDevice *pDevice = strm.Device(); 
  EWadFormat eWadFormat = WAD_FORMAT_UNKNOWN;

  SWadHeader header;

  if (!pDevice->Read((char *)&header, sizeof(SWadHeader))) {
    ThrowF_t(TRANS("%s : Unable to read archive header!"), pArchive->GetName().ConstData());
  }

  FByteArray baTag(&header.tag[0], 4);

  if (baTag == "IWAD" || baTag == "PWAD") {
    eWadFormat = WAD_FORMAT_DOOM;
  } else if (baTag == "WAD2") {
    eWadFormat = WAD_FORMAT_QUAKE;
  } else if (baTag == "WAD3") {
    eWadFormat = WAD_FORMAT_GOLDSRC;
  }

  if (eWadFormat == WAD_FORMAT_UNKNOWN) {
    ThrowF_t(TRANS("%s : Usupported WAD with '%s' signature!"), pArchive->GetName().ConstData(), baTag.ConstData());
  }

  if (!pDevice->Seek(header.InfoTableOffset)) {
    ThrowF_t(TRANS("%s : Unable to seek the entry table!"), pArchive->GetName().ConstData());
  }

  if (eWadFormat == WAD_FORMAT_DOOM) {
    FWadDoomLoaderContext ctx(pArchive);
    ctx.Lumps.New(header.NumberOfLumps);
    ctx.LumpInfos.New(header.NumberOfLumps);

    for (int i = 0; i < header.NumberOfLumps; i++)
    {
      SWadDoomFileEntry &entry = ctx.Lumps[i];

      if (!pDevice->Read((char *)&entry, sizeof(SWadDoomFileEntry))) {
        ThrowF_t(TRANS("%s : Unable to read file entry %d!"), pArchive->GetName().ConstData(), i);
      }
    }

    ProcessDoomLumps_t(ctx);
  } else {
    FWadLoaderContext ctx(pArchive);
    ctx.Lumps.New(header.NumberOfLumps);

    for (int i = 0; i < header.NumberOfLumps; i++)
    {
      SWadFileEntry &entry = ctx.Lumps[i];

      if (!pDevice->Read((char *)&entry, sizeof(SWadFileEntry))) {
        ThrowF_t(TRANS("%s : Unable to read file entry %d!"), pArchive->GetName().ConstData(), i);
      }
    }

    ProcessLumps_t(ctx, eWadFormat);
  }

  pArchive->CalculateChecksums_t(pDevice);
}

bool FWadArchiveLoader::IsCompressionSupported(enum CompressionMethod eMethod) const
{
  return eMethod == COMPRESSION_METHOD_STORE || eMethod == COMPRESSION_METHOD_LZSS;
}

void FWadArchiveLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".wad";
}