/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ARCHIVEENTRY_H
#define SE_INCL_ARCHIVEENTRY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Compression/CompressionMethod.h>

class CORE_API FArchiveEntry
{
  public:
    //! Dummy constructor.
    FArchiveEntry()
    {
      Clear();
    }

    FArchiveEntry(FArchive *pArchive, const FPath &fnm)
    {
      Clear();
      _pArchive = pArchive;
      _fnmName = fnm;
    }

    //! Checks if "Store" used as compression method.
    virtual BOOL IsStored() const
    {
      return _eCompressionMethod == COMPRESSION_METHOD_STORE;
    }

    //! Returns archive that owns the entry.
    inline FArchive *GetArchive()
    {
      return _pArchive;
    }

    //! Returns archive that owns the entry.
    inline const FArchive *GetArchive() const
    {
      return _pArchive;
    }

    //! Returns relative filename.
    inline const FPath &GetName() const
    {
      return _fnmName;
    }
    
    //! Returns data offset.
    inline SLONG GetOffset() const
    {
      return _slOffset;
    }

    //! Set offset in bytes for this entry.
    inline void SetOffset(SLONG slOffset)
    {
      _slOffset = slOffset;
    }

    //! How our data stored?
    inline CompressionMethod GetCompressionMethod() const
    {
      return _eCompressionMethod;
    }

    //! Set compression method applied to data.
    inline void SetCompressionMethod(CompressionMethod eCompressionMethod)
    {
      _eCompressionMethod = eCompressionMethod;
    }

    //! Returns size in archive.
    inline SLONG GetCompressedSize() const
    {
      return _slCompressedSize;
    }

    //! Set compressed size for this entry.
    inline void SetCompressedSize(SLONG slSize)
    {
      _slCompressedSize = slSize;
    }

    //! Returns size as if it was extracted.
    inline SLONG GetUncompressedSize() const
    {
      return _slUncompressedSize;
    }

    //! Set uncompressed size for this entry.
    inline void SetUncompressedSize(SLONG slSize)
    {
      _slUncompressedSize = slSize;
    }

    //! Returns CRC32.
    inline ULONG GetChecksum() const
    {
      return _ulCRC32;
    }

    //! Set checksum for this entry.
    inline void SetChecksum(ULONG ulValue)
    {
      _ulCRC32 = ulValue;
    }

    // Release memory.
    inline void Clear()
    {
      _pArchive = nullptr;
      _fnmName.Clear();
      _slOffset = 0;
      _eCompressionMethod = COMPRESSION_METHOD_INVALID;
      _slCompressedSize = 0;
      _slUncompressedSize = 0;
      _ulCRC32 = 0;
    }

  public:
    FArchive *_pArchive;
    FPath _fnmName;
    SLONG _slOffset;
    CompressionMethod _eCompressionMethod;
    SLONG _slCompressedSize;
    SLONG _slUncompressedSize;
    ULONG _ulCRC32;
};

#endif /* include-once check. */