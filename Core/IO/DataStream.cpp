/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Endian.h>
#include <Core/IO/BufferDevice.h>
#include <Core/IO/DataStream.h>
#include <Core/Base/ByteArray.h>

#include <string>

FDataStream::FDataStream()
{
  _pDevice = nullptr;
  _eByteOrder = BO_BIGENDIAN;
  _eStatus = STATUS_OK;
  _bExceptionMode = false;
  _bHasOwnDevice = false;
}

FDataStream::FDataStream(FReadWriteDevice *d)
{
  _pDevice = d;
  _eByteOrder = BO_BIGENDIAN;
  _eStatus = STATUS_OK;
  _bExceptionMode = false;
  _bHasOwnDevice = false;
}

FDataStream::FDataStream(const FByteArray &ba)
{
  _pDevice = new FBufferDevice((FByteArray *)&ba);
  _pDevice->Open(FReadWriteDevice::OM_READONLY);
  _eByteOrder = BO_BIGENDIAN;
  _eStatus = STATUS_OK;
  _bExceptionMode = false;
  _bHasOwnDevice = true;
}

FDataStream::FDataStream(FByteArray *pba, FReadWriteDevice::OpenMode om)
{
  _pDevice = new FBufferDevice(pba);
  _pDevice->Open(om);
  _eByteOrder = BO_BIGENDIAN;
  _eStatus = STATUS_OK;
  _bExceptionMode = false;
  _bHasOwnDevice = true;
}

FDataStream::~FDataStream()
{
  if (_bHasOwnDevice) {
    delete _pDevice;
  }
}

void FDataStream::SetByteOrder(enum ByteOrder bo)
{
  _eByteOrder = bo;
}

bool FDataStream::AtEnd() const
{
  return _pDevice == nullptr || _pDevice->AtEnd();
}

void FDataStream::SetDevice(FReadWriteDevice *d)
{
  if (_bHasOwnDevice) {
    delete _pDevice;
    _bHasOwnDevice = false;
  }

  _pDevice = d;
}

void FDataStream::SetStatus(enum Status eStatus)
{
  if (eStatus == STATUS_OK) {
    _eStatus = eStatus;
    
    if (_bExceptionMode && eStatus != STATUS_OK) {
      if (eStatus == STATUS_READPASTEND) {
        throw("STATUS_READPASTEND");
      } else if (eStatus == STATUS_WRITEFAILED) {
        throw("STATUS_WRITEFAILED");
      }
    }
  }
}

void FDataStream::SetExceptionMode(bool bExceptionMode)
{
  _bExceptionMode = bExceptionMode;
}

void FDataStream::ResetStatus()
{
  _eStatus = STATUS_OK;
}

size_t FDataStream::Read(void *pBuffer, size_t len)
{
  if (_eStatus != STATUS_OK || pBuffer == nullptr || Device() == nullptr) {
    return -1;
  }

  const size_t res = _pDevice->Read((char *)pBuffer, len);
  if (res != len) {
    SetStatus(STATUS_READPASTEND);
  }

  return res;
}

FByteArray FDataStream::Read(size_t len)
{
  FByteArray res;

  res.Resize(len);
  Read((char *)res.Data(), len);

  return res;
}

size_t FDataStream::Write(const void *pData, size_t len)
{
  if (_eStatus != STATUS_OK || pData == nullptr || _pDevice == nullptr) {
    return -1;
  }

  return _pDevice->Write((const char *)pData, len);
}

size_t FDataStream::Write(const FByteArray &baData)
{
  if (baData.IsNull()) {
    return -1;
  }

  return Write((const char *)baData.ConstData(), baData.Size());
}

size_t FDataStream::Skip(size_t len)
{
  if (_eStatus != STATUS_OK) {
    return -1;
  }

  const size_t res = _pDevice->Skip(len);
  if (res != len) {
    SetStatus(STATUS_READPASTEND);
  }

  return res;
}

size_t FDataStream::Peek(void *pBuffer, size_t len)
{
  if (_eStatus != STATUS_OK) {
    return -1;
  }

  const size_t res = _pDevice->Peek((char *)pBuffer, len);
  if (res != len) {
    SetStatus(STATUS_READPASTEND);
  }

  return res;
}

FByteArray FDataStream::Peek(size_t len)
{
  FByteArray res;

  res.Resize(len);
  Peek((char *)res.Data(), len);

  return res;
}

FDataStream &FDataStream::operator>>(u8 &dst)
{
  if (Read(&dst, 1) != 1) {
    dst = 0;
  }

  return *this;
}

FDataStream &FDataStream::operator>>(u16 &dst)
{
  if (Read(&dst, 2) != 2) {
    dst = 0;
  }
  
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }
  
  return *this;
}

FDataStream &FDataStream::operator>>(u32 &dst)
{
  if (Read(&dst, 4) != 4) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator>>(u64 &dst)
{
  if (Read(&dst, 8) != 8) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator>>(s8 &dst)
{
  if (Read(&dst, 1) != 1) {
    dst = 0;
  }

  return *this;
}

FDataStream &FDataStream::operator>>(s16 &dst)
{
  if (Read(&dst, 2) != 2) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator>>(s32 &dst)
{
  if (Read(&dst, 4) != 4) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator>>(s64 &dst)
{
  if (Read(&dst, 8) != 8) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator>>(f32 &dst)
{
  if (Read(&dst, 4) != 4) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator>>(f64 &dst)
{
  if (Read(&dst, 8) != 8) {
    dst = 0;
  }

  if (GetByteOrder() == BO_LITTLEENDIAN) {
    dst = CEndian::FromLittle(dst);
  } else {
    dst = CEndian::FromBig(dst);
  }

  return *this;
}

FDataStream &FDataStream::operator<<(u8 src)
{ 
  if (Write(&src, 1) != 1) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(u16 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 2) != 2) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(u32 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 4) != 4) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(u64 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 8) != 8) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(s8 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 1) != 1) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(s16 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 2) != 2) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(s32 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 4) != 4) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(s64 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 8) != 8) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(f32 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 4) != 4) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

FDataStream &FDataStream::operator<<(f64 src)
{
  if (GetByteOrder() == BO_LITTLEENDIAN) {
    src = CEndian::ToLittle(src);
  } else {
    src = CEndian::ToBig(src);
  }
  
  if (Write(&src, 8) != 8) {
    SetStatus(STATUS_WRITEFAILED);
  }
  
  return *this;
}

#include <Core/Base/String.h>

void FDataStream::ReadString_t(FString &str)
{
  ASSERT(str.IsValid());

  // read length
  ULONG iStringLen;
  (*this) >> iStringLen;
  ASSERT(iStringLen >= 0);

  // allocate that much memory
  FreeMemory(str.str_String);
  str.str_String = (char *)AllocMemory(iStringLen + 1);  // take end-marker in account

  // if the string is not empty then read string
  if (iStringLen > 0 && Read(str.Data(), iStringLen) != iStringLen) {
    memset(str.Data(), 0, iStringLen);
  }

  str.Data()[iStringLen] = 0; // terminate
}

void FDataStream::WriteString_t(const FString &str)
{
  ASSERT(str.IsValid());

  const INDEX iStringLen = str.Length(); // Calculate size...
  (*this) << iStringLen; // Try to write size...

  // Try to write the string itself...
  if (iStringLen > 0 && Write(str.ConstData(), iStringLen) != iStringLen) {
    SetStatus(FDataStream::STATUS_WRITEFAILED);
  }
}

void FDataStream::ReadPath_t(FPath &fnm)
{
  ReadString_t((FString &)fnm);
}

void FDataStream::WritePath_t(const FPath &fnm)
{
  WriteString_t((const FString &)fnm);
}