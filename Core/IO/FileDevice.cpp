/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/FileDevice.h>

FFileDevice::FFileDevice()
{
  _eOpenMode = OM_NOTOPEN;
  _pFile = nullptr;
}

FFileDevice::FFileDevice(const FString &path)
{
  _eOpenMode = OM_NOTOPEN;
  _pFile = nullptr;
  _strFilename = path;
}

FFileDevice::~FFileDevice()
{
  Close();
}

bool FFileDevice::SetFilename(const FString &path) {
  if (IsOpen()) {
    return false;
  }

  _strFilename = path;
  return true;
};

bool FFileDevice::Open(OpenMode om)
{
  if (om == OM_NOTOPEN) {
    return false;
  }

  if (om == OM_READONLY) {
    _pFile = fopen(_strFilename, "rb");
  } else if (om == OM_WRITEONLY) {
    _pFile = fopen(_strFilename, "wb");
  } else {
    _pFile = fopen(_strFilename, "rb+");
  }

  if (_pFile != nullptr) {
    _eOpenMode = om;
    return true;
  }

  return false;
}

void FFileDevice::Close()
{
  if (!IsOpen()) {
    return;
  }

  fclose(_pFile);
  _eOpenMode = OM_NOTOPEN;
}

bool FFileDevice::AtEnd() const
{
  int eof = feof(_pFile);
  return eof != 0;
}


size_t FFileDevice::Pos() const
{
  if (!IsOpen()) {
    return -1;
  }

  return ftell(_pFile);
}

size_t FFileDevice::Size() const
{
  if (!IsOpen()) {
    return -1;
  }

  size_t current_pos = ftell(_pFile);
  fseek(_pFile, 0, SEEK_END);
  size_t file_size = ftell(_pFile);
  fseek(_pFile, current_pos, SEEK_SET);
  return file_size;
}

bool FFileDevice::Seek(size_t pos)
{
  return fseek(_pFile, pos, SEEK_SET) == 0;
}

size_t FFileDevice::Skip(size_t max_size)
{
  return fseek(_pFile, max_size, SEEK_CUR) == 0;
}

size_t FFileDevice::Read(char *pData, size_t max_size)
{
  return fread(pData, 1, max_size, _pFile);
}

size_t FFileDevice::Peek(char *pData, size_t max_size)
{
  size_t pos_before = Pos();
  size_t len = Read(pData, max_size);
  Seek(pos_before);
  return len;
}

size_t FFileDevice::Write(const char *pData, size_t max_size)
{
  if (!IsWritable()) {
    return -1;
  }

  return fwrite(pData, 1, max_size, _pFile);
}

FString FFileDevice::Filename() const
{
  return _strFilename;
}

bool FFileDevice::Exists() const
{
  FILE *file;

  if (file = fopen(_strFilename, "r")) {
    fclose(file);
    return true;
  }

  return false;
}

bool FFileDevice::Remove()
{
  int ires = remove(_strFilename);
  return ires == 0;
}

bool FFileDevice::Rename(const FString &new_name)
{
  if (_strFilename.Length() == 0 && new_name.Length() == 0) {
    return false;
  }

  int ires = rename(_strFilename, new_name);
  return ires == 0;
}