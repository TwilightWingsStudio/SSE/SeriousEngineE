/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_WADARCHIVE_H
#define SE_INCL_WADARCHIVE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/IO/ArchiveLoader.h>

//! There are different WAD files in games.
enum EWadFormat
{
  //! Unknown signatue.
  WAD_FORMAT_UNKNOWN,
  
  //! IWAD or PWAD.
  WAD_FORMAT_DOOM,

  //! WAD2.
  WAD_FORMAT_QUAKE,

  //! WAD3.
  WAD_FORMAT_GOLDSRC,
};

//! One zip archive.
class CORE_API FWadArchiveLoader : public FArchiveLoader
{
  public:
    //! Get file info.
    void Load_t(FArchive *pArchive, FDataStream &strm) const override;

    //! Check if compression method supported by format.
    bool IsCompressionSupported(enum CompressionMethod eMethod) const override;

    //! Fill list with valid extension strings.
    void GetValidExtensions(TStaticStackArray<FString> &astr) const override;

    //! Determine archive format.
    ArchiveFormat GetFormat() const override
    {
      return ARCHIVE_FORMAT_WAD;
    }
};

#endif /* include-once check. */