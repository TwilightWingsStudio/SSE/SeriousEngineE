/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FReadWriteDevice_H
#define SE_INCL_FReadWriteDevice_H

#include <Core/Base/Types.h>
#include <Core/Base/ByteArray.h>

//! Abstract interface for random-access device.
class CORE_API FReadWriteDevice
{
  public:
    enum OpenMode
    {
      OM_NOTOPEN = 0,
      
      //! rb
      OM_READONLY = 1,
      
      //! wb
      OM_WRITEONLY = 2,

      //! rb+
      OM_READWRITE = OM_READONLY | OM_WRITEONLY,
    };

    enum Type
    {
      TYPE_INVALID = 0,
      TYPE_BUFFER,
      TYPE_FILE,
      TYPE_LOCALSOCKET,
    };

  public:
    //! Destructor.
    virtual ~FReadWriteDevice() {};

    //! Start interacting in given mode.
    virtual bool Open(OpenMode om) = 0;

    //! End interacting with.
    virtual void Close() = 0;

    //! Returns access mode.
    inline OpenMode GetOpenMode() const
    {
      return _eOpenMode;
    }

    //! Checks if opened with any mode
    inline bool IsOpen() const
    {
      return GetOpenMode() != OM_NOTOPEN;
    }

    //! Checks if opened with mode that allows writing.
    inline bool IsWritable() const
    {
      return (GetOpenMode() & OM_WRITEONLY) != 0;
    }

    //! Checks if carret at end.
    virtual bool AtEnd() const = 0;

    //! Returns carret position.
    virtual size_t Pos() const = 0;

    //! Size of the device.
    virtual size_t Size() const = 0;

    //! Seeks to the start.
    virtual bool Reset()
    {
      return Seek(0);
    }

    //! Tries to move carret to specified position.
    virtual bool Seek(size_t pos) = 0;

    //! Move formard.
    virtual size_t Skip(size_t max_size) = 0;

    //! Take bytes from device.
    virtual size_t Read(char *pData, size_t max_size) = 0;
    
    //! Take bytes from device.
    virtual size_t Read(FByteArray &baData, size_t max_size)
    {
      baData.Clear();
      baData.Resize(max_size);
      return Read((char *)baData.Data(), max_size);
    }

    //! Take bytes without moving carret formard.
    virtual size_t Peek(char *pData, size_t max_size) = 0;

    //! Take bytes without moving carret formard.
    virtual size_t Peek(FByteArray &baData, size_t max_size)
    {
      baData.Clear();
      baData.Resize(max_size);
      return Peek((char *)baData.Data(), max_size);
    }

    //! Put bytes into device.
    virtual size_t Write(const char *pData, size_t max_size) = 0;

    //! Put bytes into device.
    virtual size_t Write(const FByteArray &baData)
    {
      if (baData.IsNull()) {
        return -1;
      }
      
      return Write((const char *)baData.ConstData(), baData.Size());
    }

    //! Check device type.
    virtual Type GetType() const
    {
      return TYPE_INVALID;
    }

  protected:
    OpenMode _eOpenMode;
};

#endif /* include-once check */