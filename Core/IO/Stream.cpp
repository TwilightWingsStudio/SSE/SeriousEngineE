/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _WIN32
#include <io.h>
#ifndef SE_OLD_COMPILER
#include <DbgHelp.h>
#endif
#endif

#include <Core/Core.h>
#include <Core/IO/Stream.h>

#include <Core/Base/Memory.h>
#include <Core/Base/Console.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/ListIterator.inl>
#include <Core/Math/Functions.h>
//#include <Engine/Base/ProgressHook.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Shell.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/Archive.h>
#include <Core/IO/ArchiveManager.h>
#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicStackArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

// maximum lenght of file that can be saved (default: 128Mb)
ULONG _ulMaxLenghtOfSavingFile = (1UL<<20)*128;

#ifdef _WIN32
// set if current thread has currently enabled stream handling
static _declspec(thread) BOOL _bThreadCanHandleStreams = FALSE;
// list of currently opened streams
static _declspec(thread) CListHead *_plhOpenedStreams = NULL;
#else
// set if current thread has currently enabled stream handling
static __thread BOOL _bThreadCanHandleStreams = FALSE;
// list of currently opened streams
static __thread CListHead* _plhOpenedStreams = NULL;
#endif

ULONG _ulVirtuallyAllocatedSpace = 0;
ULONG _ulVirtuallyAllocatedSpaceTotal = 0;

// global string with application path
FPath _fnmApplicationPath;
// global string with filename of the started application
FPath _fnmApplicationExe;
// global string with current MOD path
FPath _fnmMod;
// global string with current name (the parameter that is passed on cmdline)
FString _strModName;
// global string with url to be shown to users that don't have the mod installed
// (should be set by game.dll)
FString _strModURL;
// global string with current MOD extension (for adding to dlls)
FString _strModExt;
// global string with shared resources path (for minimal installations)
FPath _fnmSharedPath;

/////////////////////////////////////////////////////////////////////////////
// Helper functions

/* Static function enable stream handling. */
void CTStream::EnableStreamHandling(void)
{
  ASSERT(!_bThreadCanHandleStreams && _plhOpenedStreams == NULL);

  _bThreadCanHandleStreams = TRUE;
  _plhOpenedStreams = new CListHead;
}

/* Static function disable stream handling. */
void CTStream::DisableStreamHandling(void)
{
  ASSERT(_bThreadCanHandleStreams && _plhOpenedStreams != NULL);

  _bThreadCanHandleStreams = FALSE;
  delete _plhOpenedStreams;
  _plhOpenedStreams = NULL;
}

int CTStream::ExceptionFilter(DWORD dwCode, _EXCEPTION_POINTERS *pExceptionInfoPtrs)
{
  // If the exception is not a page fault, exit.
  if (dwCode != EXCEPTION_ACCESS_VIOLATION) {
    return EXCEPTION_CONTINUE_SEARCH;
  }

  // obtain access violation virtual address
  UBYTE *pIllegalAdress = (UBYTE *)pExceptionInfoPtrs->ExceptionRecord->ExceptionInformation[1];

  CTStream *pstrmAccessed = NULL;

  // search for stream that was accessed
  FOREACHINLIST(CTStream, strm_lnListNode, (*_plhOpenedStreams), itStream)
  {
    // if access violation happened inside curently testing stream
    if (itStream.Current().PointerInStream(pIllegalAdress))
    {
      // remember accesed stream ptr
      pstrmAccessed = &itStream.Current();
      // stream found, stop searching
      break;
    }
  }

  // if none of our streams was accessed, real access violation occured
  if (pstrmAccessed == NULL)
  {
    // so continue default exception handling
    return EXCEPTION_CONTINUE_SEARCH;
  }

  // Continue execution where the page fault occurred
  return EXCEPTION_CONTINUE_EXECUTION;
}

/*
 * Static function to report fatal exception error.
 */
void CTStream::ExceptionFatalError(void)
{
  FatalError(GetWindowsError(GetLastError()) );
}

/*
 * Throw an exception of formatted string.
 */
void CTStream::Throw_t(char *strFormat, ...)  // throws char *
{
  const SLONG slBufferSize = 256;
  char strFormatBuffer[slBufferSize];
  char strBuffer[slBufferSize];

  // add the stream description to the format string
  _snprintf(strFormatBuffer, slBufferSize, "%s (%s)", strFormat, strm_strStreamDescription.ConstData());

  // format the message in buffer
  va_list arg;
  va_start(arg, strFormat); // variable arguments start after this argument
  _vsnprintf(strBuffer, slBufferSize, strFormatBuffer, arg);
  throw strBuffer;
}

/////////////////////////////////////////////////////////////////////////////
// Binary access methods

/* Get CRC32 of stream */
ULONG CTStream::GetStreamCRC32_t(void)
{
  // remember where stream is now
  SLONG slOldPos = GetPos_t();
  // go to start of file
  SetPos_t(0);
  // get size of file
  SLONG slFileSize = GetStreamSize();

  ULONG ulCRC;
  CRC_Start(ulCRC);

  // for each block in file
  const SLONG slBlockSize = 4096;

  for (SLONG slPos = 0; slPos < slFileSize; slPos += slBlockSize)
  {
    // read the block
    UBYTE aubBlock[slBlockSize];
    SLONG slThisBlockSize = Min(slFileSize - slPos, slBlockSize);
    Read_t(aubBlock, slThisBlockSize);
    // checksum it
    CRC_AddBlock(ulCRC, aubBlock, slThisBlockSize);
  }

  // restore position
  SetPos_t(slOldPos);

  CRC_Finish(ulCRC);
  return ulCRC;
}

/////////////////////////////////////////////////////////////////////////////
// Text access methods

/* Get a line of text from file. */
// throws char *
void CTStream::GetLine_t(char *strBuffer, SLONG slBufferSize, char cDelimiter /*='\n'*/ )
{
  // check parameters
  ASSERT(strBuffer!=NULL && slBufferSize>0);
  // check that the stream can be read
  ASSERT(IsReadable());
  // letters slider
  INDEX iLetters = 0;

  // test if EOF reached
  if (AtEOF()) {
    ThrowF_t(TRANS("EOF reached, file %s"), strm_strStreamDescription);
  }

  // get line from istream
  FOREVER
  {
    char c;
    Read_t(&c, 1);

    if (AtEOF()) {
      // cut off
      strBuffer[iLetters] = 0;
      break;
    }

    // don't read "\r" characters but rather act like they don't exist
    if (c != '\r') {
      strBuffer[iLetters] = c;
      // stop reading when delimiter loaded
      if (strBuffer[iLetters] == cDelimiter) {
        // convert delimiter to zero
        strBuffer[iLetters] = 0;
        // jump over delimiter
        //Seek_t(1, SD_CUR);
        break;
      }
      // jump to next destination letter
      iLetters++;
    }

    // test if maximum buffer lenght reached
    if (iLetters == slBufferSize) {
      return;
    }
  }
}

void CTStream::GetLine_t(FString &strLine, char cDelimiter/*='\n'*/) // throw char *
{
  char strBuffer[1024];
  GetLine_t(strBuffer, sizeof(strBuffer)-1, cDelimiter);
  strLine = strBuffer;
}

/* Put a line of text into file. */
void CTStream::PutLine_t(const char *strBuffer) // throws char *
{
  // check parameters
  ASSERT(strBuffer!=NULL);
  // check that the stream is writteable
  ASSERT(IsWriteable());
  // get string length
  INDEX iStringLength = strlen(strBuffer);
  // put line into stream
  Write_t(strBuffer, iStringLength);
  // write "\r\n" into stream
  Write_t("\r\n", 2);
}

void CTStream::PutString_t(const char *strString) // throw char *
{
  // check parameters
  ASSERT(strString!=NULL);
  // check that the stream is writteable
  ASSERT(IsWriteable());
  // get string length
  INDEX iStringLength = strlen(strString);

  // put line into stream
  for (INDEX iLetter = 0; iLetter < iStringLength; iLetter++)
  {
    if (*strString == '\n') {
      // write "\r\n" into stream
      Write_t("\r\n", 2);
      strString++;
    } else {
      Write_t(strString++, 1);
    }
  }
}

void CTStream::FPrintF_t(const char *strFormat, ...) // throw char *
{
  const SLONG slBufferSize = 2048;
  char strBuffer[slBufferSize];
  // format the message in buffer
  va_list arg;
  va_start(arg, strFormat); // variable arguments start after this argument
  _vsnprintf(strBuffer, slBufferSize, strFormat, arg);
  // print the buffer
  PutString_t(strBuffer);
}

/////////////////////////////////////////////////////////////////////////////
// Chunk reading/writing methods

CChunkID CTStream::GetID_t(void) // throws char *
{
	CChunkID cidToReturn;
	Read_t(&cidToReturn.cid_ID[0], CID_LENGTH);
	return(cidToReturn);
}

CChunkID CTStream::PeekID_t(void) // throw char *
{
  // read the chunk id
	CChunkID cidToReturn;
	Read_t(&cidToReturn.cid_ID[0], CID_LENGTH);

  // return the stream back
  Seek_t(-CID_LENGTH, SD_CUR);
	return(cidToReturn);
}

void CTStream::ExpectID_t(const CChunkID &cidExpected) // throws char *
{
	CChunkID cidToCompare;

	Read_t(&cidToCompare.cid_ID[0], CID_LENGTH);

	if (!(cidToCompare == cidExpected))
	{
		ThrowF_t(TRANS("Chunk ID validation failed.\nExpected ID \"%s\" but found \"%s\"\nOffset: %d"),
      cidExpected.cid_ID, cidToCompare.cid_ID, GetPos_t());
	}
}

void CTStream::ExpectKeyword_t(const FString &strKeyword) // throw char *
{
  // check that the keyword is present
  for (INDEX iKeywordChar = 0; iKeywordChar < strKeyword.Length(); iKeywordChar++)
  {
    SBYTE chKeywordChar;
    (*this) >> chKeywordChar;

    if (chKeywordChar != strKeyword[iKeywordChar]) {
      ThrowF_t(TRANS("Expected keyword %s not found"), strKeyword);
    }
  }
}

SLONG CTStream::GetSize_t(void) // throws char *
{
	SLONG chunkSize;

	Read_t((char *) &chunkSize, sizeof(SLONG));
	return(chunkSize);
}

void CTStream::ReadRawChunk_t(void *pvBuffer, SLONG slSize)  // throws char *
{
	Read_t((char *)pvBuffer, slSize);
}

void CTStream::ReadChunk_t(void *pvBuffer, SLONG slExpectedSize) // throws char *
{
	if (slExpectedSize != GetSize_t()) {
		throw TRANS("Chunk size not equal as expected size");
	}

  Read_t((char *)pvBuffer, slExpectedSize);
}

void CTStream::ReadFullChunk_t(const CChunkID &cidExpected, void *pvBuffer,
                             SLONG slExpectedSize) // throws char *
{
	ExpectID_t(cidExpected);
	ReadChunk_t(pvBuffer, slExpectedSize);
};

void* CTStream::ReadChunkAlloc_t(SLONG slSize) // throws char *
{
	UBYTE *buffer;
	SLONG chunkSize;

	if (slSize != 0) {
		chunkSize = slSize;
	} else {
		chunkSize = GetSize_t(); // throws char *
	}
  
  buffer = (UBYTE *) AllocMemory(chunkSize);

	if (buffer == NULL) {
		throw TRANS("ReadChunkAlloc: Unable to allocate needed amount of memory.");
	}
  
  Read_t((char *)buffer, chunkSize); // throws char *

	return buffer;
}
void CTStream::ReadStream_t(CTStream &strmOther) // throw char *
{
  // TODO: implement this !!!! @@@@
}

void CTStream::WriteID_t(const CChunkID &cidSave) // throws char *
{
	Write_t(&cidSave.cid_ID[0], CID_LENGTH);
}

void CTStream::WriteSize_t(SLONG slSize) // throws char *
{
	Write_t((char *)&slSize, sizeof(SLONG));
}

void CTStream::WriteRawChunk_t(void *pvBuffer, SLONG slSize) // throws char *
{
	Write_t((char *)pvBuffer, slSize);
}

void CTStream::WriteChunk_t(void *pvBuffer, SLONG slSize) // throws char *
{
	WriteSize_t(slSize);
	WriteRawChunk_t(pvBuffer, slSize);
}

void CTStream::WriteFullChunk_t(const CChunkID &cidSave, void *pvBuffer, SLONG slSize) // throws char *
{
	WriteID_t(cidSave); // throws char *
	WriteChunk_t(pvBuffer, slSize); // throws char *
}

void CTStream::WriteStream_t(CTStream &strmOther) // throw char *
{
  // TODO: implement this !!!! @@@@
}

// whether or not the given pointer is coming from this stream (mainly used for exception handling)
BOOL CTStream::PointerInStream(void* pPointer)
{
  // safe to return FALSE, we're using virtual functions anyway
  return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// filename dictionary operations

// enable dictionary in writable file from this point
void CTStream::DictionaryWriteBegin_t(const FPath &fnmImportFrom, SLONG slImportOffset)
{
  strm_Dictionary.WriteBegin_t(this, fnmImportFrom, slImportOffset);
}

// write the dictionary (usually at the end of file)
void CTStream::DictionaryWriteEnd_t(void)
{
  strm_Dictionary.WriteEnd_t(this);
}

// read the dictionary from given offset in file (internal function)
void CTStream::ReadDictionary_intenal_t(SLONG slOffset)
{
  strm_Dictionary.Read_intenal_t(this, slOffset);
}

// copy filename dictionary from another stream
void CTStream::CopyDictionary(CTStream &strmOther)
{
  strm_Dictionary.Copy(strmOther.GetDictionary());
}

SLONG CTStream::DictionaryReadBegin_t(void)
{
  return strm_Dictionary.ReadBegin_t(this);
}

void CTStream::DictionaryReadEnd_t(void)
{
  strm_Dictionary.ReadEnd_t(this);
}

void CTStream::DictionaryPreload_t(void)
{
  strm_Dictionary.Preload_t(this);
}

/////////////////////////////////////////////////////////////////////////////
// General construction/destruction

/* Default constructor. */
CTStream::CTStream(void) : strm_Dictionary(*new FPathDictionary), strm_ulFlags(0)
{
  strm_strStreamDescription = "";
}

/* Destructor. */
CTStream::~CTStream(void)
{
  delete &strm_Dictionary;
}

/////////////////////////////////////////////////////////////////////////////
// File stream opening/closing methods

/*
 * Default constructor.
 */
CTFileStream::CTFileStream(void)
{
  fstrm_pFile = NULL;
  // mark that file is created for writing
  fstrm_bReadOnly = TRUE;
  fstrm_iZipHandle = -1;
  fstrm_iZipLocation = 0;
  fstrm_ulZipChecksum = 0;
  fstrm_pubZipBuffer = NULL;
}

/*
 * Destructor.
 */
CTFileStream::~CTFileStream(void)
{
  // close stream
  if (fstrm_pFile != NULL || fstrm_iZipHandle != -1) {
    Close();
  }
}

/*
 * Open an existing file.
 */
// throws char *
void CTFileStream::Open_t(const FPath &fnFileName, CTStream::OpenMode om/*=OM_READ*/)
{
  // if current thread has not enabled stream handling then error
  if (!_bThreadCanHandleStreams) {
    ::ThrowF_t(TRANS("Cannot open file `%s', stream handling is not enabled for this thread"), (FString&)fnFileName);
  }

  // check parameters
  ASSERT(strlen(fnFileName) > 0);
  // check that the file is not open
  ASSERT(fstrm_pFile == NULL && fstrm_iZipHandle == -1);

  // expand the filename to full path
  FPath fnmFullFileName;
  INDEX iFile = ExpandFilePath((om == OM_READ)?EFP_READ:EFP_WRITE, fnFileName, fnmFullFileName);
  
  // if read only mode requested
  if (om == OM_READ) {
    // initially, no physical file
    fstrm_pFile = NULL;

    // if zip file
    if (iFile == EFP_MODZIP || iFile == EFP_BASEZIP) {
      // open from zip
      TDynamicContainer<FArchiveEntry> arentries;
      const INDEX ctFound = _pArchiveMgr->QueryFiles(fnmFullFileName, arentries, TRUE, 1);
      FArchiveEntry *pEntry = arentries.Pointer(0);

      fstrm_iZipHandle = 666; // Put just something to know that file is from archive.
      fstrm_slZipSize = pEntry->GetUncompressedSize();
      fstrm_ulZipChecksum = pEntry->GetChecksum();

      // load the file from the zip in the buffer
#ifdef _WIN32
      fstrm_pubZipBuffer = (UBYTE*)VirtualAlloc(NULL, fstrm_slZipSize, MEM_COMMIT, PAGE_READWRITE);
#else
      fstrm_pubZipBuffer = new UBYTE[fstrm_slZipSize];
      memset(fstrm_pubZipBuffer, 0, fstrm_slZipSize);
#endif

      pEntry->GetArchive()->Prepare_t();
      pEntry->GetArchive()->ReadFile_t(*pEntry, fstrm_pubZipBuffer);
    // if it is a physical file
    } else if (iFile == EFP_FILE) {
      // open file in read only mode
      fstrm_pFile = fopen(fnmFullFileName, "rb");
    }

    fstrm_bReadOnly = TRUE;
  
  // if write mode requested
  } else if (om == OM_WRITE) {
    // open file for reading and writing
    fstrm_pFile = fopen(fnmFullFileName, "rb+");
    fstrm_bReadOnly = FALSE;

  // if unknown mode
  } else {
    FatalError(TRANS("File stream opening requested with unknown open mode: %d\n"), om);
  }

  // if openning operation was not successfull then throw exception
  if (fstrm_pFile == NULL && fstrm_iZipHandle == -1) {
    Throw_t(TRANS("Cannot open file `%s' (%s)"), (FString&)fnmFullFileName, strerror(errno));
  }

  // if file opening was successfull, set stream description to file name
  strm_strStreamDescription = fnmFullFileName;
  // add this newly opened file into opened stream list
  _plhOpenedStreams->AddTail(strm_lnListNode);
}

/*
 * Create a new file or overwrite existing.
 */
void CTFileStream::Create_t(const FPath &fnFileName) // throws char *
{
  FPath fnFileNameAbsolute = fnFileName;
  fnFileNameAbsolute.SetAbsolutePath();

  // if current thread has not enabled stream handling then error
  if (!_bThreadCanHandleStreams) {
    ::ThrowF_t(TRANS("Cannot create file `%s', stream handling is not enabled for this thread"),
      (FString&)fnFileNameAbsolute);
  }

  FPath fnmFullFileName;
  INDEX iFile = ExpandFilePath(EFP_WRITE, fnFileNameAbsolute, fnmFullFileName);

  // check parameters
  ASSERT(strlen(fnFileNameAbsolute) > 0);
  // check that the file is not open
  ASSERT(fstrm_pFile == NULL);

  // create the directory for the new file if it doesn't exist yet
#if (defined _WIN32) && !(defined SE_OLD_COMPILER) 
  MakeSureDirectoryPathExists(fnmFullFileName);
#else
  #pragma message("Fix CTFileStream::Create_t for your platform!")
#endif

  // open file stream for writing (destroy file context if file existed before)
  fstrm_pFile = fopen(fnmFullFileName, "wb+");

  // if not successfull then throw exception
  if (fstrm_pFile == NULL) {
    Throw_t(TRANS("Cannot create file `%s' (%s)"), (FString&)fnmFullFileName, strerror(errno));
  }

  // if file creation was successfull, set stream description to file name
  strm_strStreamDescription = fnFileNameAbsolute;
  // mark that file is created for writing
  fstrm_bReadOnly = FALSE;
  // add this newly created file into opened stream list
  _plhOpenedStreams->AddTail(strm_lnListNode);
}

/*
 * Close an open file.
 */
void CTFileStream::Close(void)
{
  // if file is not open
  if (fstrm_pFile == NULL && fstrm_iZipHandle == -1) {
    ASSERT(FALSE);
    return;
  }

  // clear stream description
  strm_strStreamDescription = "";
  // remove file from list of curently opened streams
  strm_lnListNode.Remove();

  // if file on disk then close file
  if (fstrm_pFile != NULL) {
    fclose(fstrm_pFile);
    fstrm_pFile = NULL;

  // if file in zip
  } else if (fstrm_iZipHandle >= 0) {
    // close zip entry
    fstrm_iZipHandle = -1;

#ifdef _WIN32
    VirtualFree(fstrm_pubZipBuffer, 0, MEM_RELEASE);
#else
    delete[] fstrm_pubZipBuffer;
#endif

    _ulVirtuallyAllocatedSpace -= fstrm_slZipSize;
    //CInfoF("Freed virtual memory with size ^c00ff00%d KB^C (now %d KB)\n", (fstrm_slZipSize / 1000), (_ulVirtuallyAllocatedSpace / 1000));
  }

  strm_Dictionary.Clear();
}

/* Get CRC32 of stream */
ULONG CTFileStream::GetStreamCRC32_t(void)
{
  // if file on disk then use base class implementation (really calculates the CRC)
  if (fstrm_pFile != NULL) {
    return CTStream::GetStreamCRC32_t();

  // if file in zip
  } else if (fstrm_iZipHandle >= 0) {
    return fstrm_ulZipChecksum;
  } else {
    ASSERT(FALSE);
    return 0;
  }
}

/* Read a block of data from stream. */
void CTFileStream::Read_t(void *pvBuffer, SLONG slSize)
{
  if (fstrm_iZipHandle != -1) {
    memcpy(pvBuffer, fstrm_pubZipBuffer + fstrm_iZipLocation, slSize);
    fstrm_iZipLocation += slSize;
    return;
  }

  fread(pvBuffer, slSize, 1, fstrm_pFile);
}

/* Write a block of data to stream. */
void CTFileStream::Write_t(const void *pvBuffer, SLONG slSize)
{
  if (fstrm_bReadOnly || fstrm_iZipHandle != -1) {
    throw "Stream is read-only!";
  }

  fwrite(pvBuffer, slSize, 1, fstrm_pFile);
}

/* Seek in stream. */
void CTFileStream::Seek_t(SLONG slOffset, enum SeekDir sd)
{
  if (fstrm_iZipHandle != -1) {
    switch (sd)
    {
      case SD_BEG: fstrm_iZipLocation = slOffset; break;
      case SD_CUR: fstrm_iZipLocation += slOffset; break;
      case SD_END: fstrm_iZipLocation = GetSize_t() + slOffset; break;
    }

  } else {
    fseek(fstrm_pFile, slOffset, sd);
  }
}

/* Set absolute position in stream. */
void CTFileStream::SetPos_t(SLONG slPosition)
{
  Seek_t(slPosition, SD_BEG);
}

/* Get absolute position in stream. */
SLONG CTFileStream::GetPos_t(void)
{
  if (fstrm_iZipHandle != -1) {
    return fstrm_iZipLocation;

  } else {
    return ftell(fstrm_pFile);
  }
}

/* Get size of stream */
SLONG CTFileStream::GetStreamSize(void)
{
  if (fstrm_iZipHandle != -1) {
    return fstrm_slZipSize;

  } else {
    long lCurrentPos = ftell(fstrm_pFile);
    fseek(fstrm_pFile, 0, SD_END);
    long lRet = ftell(fstrm_pFile);
    fseek(fstrm_pFile, lCurrentPos, SD_BEG);
    return lRet;
  }
}

/* Check if file position points to the EOF */
BOOL CTFileStream::AtEOF(void)
{
  if (fstrm_iZipHandle != -1) {
    return fstrm_iZipLocation >= fstrm_slZipSize;

  } else {
    int eof = feof(fstrm_pFile);
    return eof != 0;
  }
}

// whether or not the given pointer is coming from this stream (mainly used for exception handling)
BOOL CTFileStream::PointerInStream(void* pPointer)
{
  // we're not using virtual allocation buffers so it's fine to return FALSE here.
  return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Memory stream construction/destruction

/*
 * Create dynamically resizing stream for reading/writing.
 */
CTMemoryStream::CTMemoryStream(void)
{
  // if current thread has not enabled stream handling then error
  if (!_bThreadCanHandleStreams) {
    ::FatalError(TRANS("Can create memory stream, stream handling is not enabled for this thread"));
  }

  mstrm_ctLocked = 0;
  mstrm_bReadable = TRUE;
  mstrm_bWriteable = TRUE;
  mstrm_slLocation = 0;
  // set stream description
  strm_strStreamDescription = "dynamic memory stream";
  // add this newly created memory stream into opened stream list
  _plhOpenedStreams->AddTail(strm_lnListNode);

  // allocate amount of memory needed to hold maximum allowed file length (when saving)
#ifdef _WIN32
  mstrm_pubBuffer = (UBYTE*)VirtualAlloc(NULL, _ulMaxLenghtOfSavingFile, MEM_COMMIT, PAGE_READWRITE);
#else
  mstrm_pubBuffer = new UBYTE[_ulMaxLenghtOfSavingFile];
  memset(mstrm_pubBuffer, 0, _ulMaxLenghtOfSavingFile);
#endif

  mstrm_pubBufferEnd = mstrm_pubBuffer + _ulMaxLenghtOfSavingFile;
  mstrm_pubBufferMax = mstrm_pubBuffer;
}

/*
 * Create static stream from given buffer.
 */
CTMemoryStream::CTMemoryStream(void *pvBuffer, SLONG slSize,
                               CTStream::OpenMode om /*= CTStream::OM_READ*/)
{
  // if current thread has not enabled stream handling then error
  if (!_bThreadCanHandleStreams) {
    ::FatalError(TRANS("Can create memory stream, stream handling is not enabled for this thread"));
  }

  // allocate amount of memory needed to hold maximum allowed file length (when saving)
#ifdef _WIN32
  mstrm_pubBuffer = (UBYTE*)VirtualAlloc(NULL, _ulMaxLenghtOfSavingFile, MEM_COMMIT, PAGE_READWRITE);
#else
  mstrm_pubBuffer = new UBYTE[_ulMaxLenghtOfSavingFile];
  memset(mstrm_pubBuffer, 0, _ulMaxLenghtOfSavingFile);
#endif

  mstrm_pubBufferEnd = mstrm_pubBuffer + _ulMaxLenghtOfSavingFile;
  mstrm_pubBufferMax = mstrm_pubBuffer + slSize;
  // copy given block of memory into memory file
  memcpy(mstrm_pubBuffer, pvBuffer, slSize);

  mstrm_ctLocked = 0;
  mstrm_bReadable = TRUE;
  mstrm_slLocation = 0;

  // if stram is opened in read only mode
  if (om == OM_READ) {
    mstrm_bWriteable = FALSE;

  // otherwise, write is enabled
  } else {
    mstrm_bWriteable = TRUE;
  }

  // set stream description
  strm_strStreamDescription = "dynamic memory stream";
  // add this newly created memory stream into opened stream list
  _plhOpenedStreams->AddTail(strm_lnListNode);
}

/* Destructor. */
CTMemoryStream::~CTMemoryStream(void)
{
  ASSERT(mstrm_ctLocked == 0);

#ifdef _WIN32
  VirtualFree(mstrm_pubBuffer, 0, MEM_RELEASE);
#else
  delete[] mstrm_pubBuffer;
#endif

  // remove memory stream from list of curently opened streams
  strm_lnListNode.Remove();
}

/////////////////////////////////////////////////////////////////////////////
// Memory stream buffer operations

/*
 * Lock the buffer contents and it's size.
 */
void CTMemoryStream::LockBuffer(void **ppvBuffer, SLONG *pslSize)
{
  mstrm_ctLocked++;
  ASSERT(mstrm_ctLocked > 0);

  *ppvBuffer = mstrm_pubBuffer;
  *pslSize = GetSize_t();
}

/*
 * Unlock buffer.
 */
void CTMemoryStream::UnlockBuffer()
{
  mstrm_ctLocked--;
  ASSERT(mstrm_ctLocked >= 0);
}

/////////////////////////////////////////////////////////////////////////////
// Memory stream overrides from CTStream

BOOL CTMemoryStream::IsReadable(void)
{
  return mstrm_bReadable && (mstrm_ctLocked == 0);
}

BOOL CTMemoryStream::IsWriteable(void)
{
  return mstrm_bWriteable && (mstrm_ctLocked == 0);
}

BOOL CTMemoryStream::IsSeekable(void)
{
  return TRUE;
}

/* Read a block of data from stream. */
void CTMemoryStream::Read_t(void *pvBuffer, SLONG slSize)
{
  memcpy(pvBuffer, mstrm_pubBuffer + mstrm_slLocation, slSize);
  mstrm_slLocation += slSize;
}

/* Write a block of data to stream. */
void CTMemoryStream::Write_t(const void *pvBuffer, SLONG slSize)
{
  memcpy(mstrm_pubBuffer + mstrm_slLocation, pvBuffer, slSize);
  mstrm_slLocation += slSize;

  if (mstrm_pubBuffer + mstrm_slLocation > mstrm_pubBufferMax) {
    mstrm_pubBufferMax = mstrm_pubBuffer + mstrm_slLocation;
  }
}

/* Seek in stream. */
void CTMemoryStream::Seek_t(SLONG slOffset, enum SeekDir sd)
{
  switch (sd)
  {
    case SD_BEG: mstrm_slLocation = slOffset; break;
    case SD_CUR: mstrm_slLocation += slOffset; break;
    case SD_END: mstrm_slLocation = GetStreamSize() + slOffset; break;
  }
}

/* Set absolute position in stream. */
void CTMemoryStream::SetPos_t(SLONG slPosition)
{
  mstrm_slLocation = slPosition;
}

/* Get absolute position in stream. */
SLONG CTMemoryStream::GetPos_t(void)
{
  return mstrm_slLocation;
}

/* Get size of stream. */
SLONG CTMemoryStream::GetSize_t(void)
{
  return GetStreamSize();
}

/* Get size of stream */
SLONG CTMemoryStream::GetStreamSize(void)
{
  return mstrm_pubBufferMax - mstrm_pubBuffer;
}

/* Get CRC32 of stream */
ULONG CTMemoryStream::GetStreamCRC32_t(void)
{
  return CTStream::GetStreamCRC32_t();
}

/* Check if file position points to the EOF */
BOOL CTMemoryStream::AtEOF(void)
{
  return mstrm_slLocation >= GetStreamSize();
}

// whether or not the given pointer is coming from this stream (mainly used for exception handling)
BOOL CTMemoryStream::PointerInStream(void* pPointer)
{
  return pPointer >= mstrm_pubBuffer && pPointer < mstrm_pubBufferEnd;
}

#include <Core/Base/ByteArray.h>
#include <Core/Base/UUID.h>
#include <Core/Base/Variant.h>
#include <Core/Math/Plane.h>
#include <Core/Math/AABBox.h>
#include <Core/Math/Placement.h>
#include <Core/Math/Quaternion.h>

// FVariant reading/writing
CTStream &operator>>(CTStream &strmStream, FVariant &varData)
{
  UBYTE ubType;
  strmStream >> ubType;
  
  switch (ubType)
  {
    case CAbstractVar::TYPE_NIL: break;
    case CAbstractVar::TYPE_BOOL: {
      UBYTE ubValue;
      strmStream >> ubValue;
      varData.FromBool(ubValue);
    } break;

    case CAbstractVar::TYPE_S8: {
      SBYTE sbValue;
      strmStream >> sbValue;
      varData.FromInt8(sbValue);
    } break;

    case CAbstractVar::TYPE_U8: {
      SBYTE sbValue;
      strmStream >> sbValue;
      varData.FromUInt8(sbValue);
    } break;

    case CAbstractVar::TYPE_S16: {
      SWORD swValue;
      strmStream >> swValue;
      varData.FromInt16(swValue);
    } break;

    case CAbstractVar::TYPE_U16: {
      UWORD uwValue;
      strmStream >> uwValue;
      varData.FromUInt16(uwValue);
    } break;

    case CAbstractVar::TYPE_S32: {
      SLONG slValue;
      strmStream >> slValue;
      varData.FromInt(slValue);
    } break;

    case CAbstractVar::TYPE_U32: {
      ULONG ulValue;
      strmStream >> ulValue;
      varData.FromUInt(ulValue);
    } break;

    case CAbstractVar::TYPE_S64: {
      INT64 snValue;
      strmStream >> snValue;
      varData.FromInt64(snValue);
    } break;

    case CAbstractVar::TYPE_U64: {
      UINT64 unValue;
      strmStream >> unValue;
      varData.FromUInt64(unValue);
    } break;

    case CAbstractVar::TYPE_F32: {
      FLOAT fValue;
      strmStream >> fValue;
      varData.FromFloat(fValue);
    } break;

    case CAbstractVar::TYPE_F64: {
      DOUBLE fValue;
      strmStream >> fValue;
      varData.FromDouble(fValue);
    } break;

    case CAbstractVar::TYPE_VEC2I: {
      VEC2I vValue;
      strmStream >> vValue(1) >> vValue(2);
      varData.FromVec2i(vValue);
    } break;

    case CAbstractVar::TYPE_VEC3I: {
      VEC3I vValue;
      strmStream >> vValue(1) >> vValue(2) >> vValue(3);
      varData.FromVec3i(vValue);
    } break;

    case CAbstractVar::TYPE_VEC2F: {
      VEC2F vValue;
      strmStream >> vValue(1) >> vValue(2);
      varData.FromVec2f(vValue);
    } break;

    case CAbstractVar::TYPE_VEC3F: {
      VEC3F vValue;
      strmStream >> vValue(1) >> vValue(2) >> vValue(3);
      varData.FromVec3f(vValue);
    } break;

    case CAbstractVar::TYPE_VEC2D: {
      VEC2D vValue;
      strmStream >> vValue(1) >> vValue(2);
      varData.FromVec2d(vValue);
    } break;

    case CAbstractVar::TYPE_VEC3D: {
      VEC3D vValue;
      strmStream >> vValue(1) >> vValue(2) >> vValue(3);
      varData.FromVec3d(vValue);
    } break;

    case CAbstractVar::TYPE_PLANE3F: {
      PLANE3F vValue;
      strmStream >> vValue(1) >> vValue(2) >> vValue(3) >> vValue.pl_distance;
      varData.FromPlane3f(vValue);
    } break;

    case CAbstractVar::TYPE_PLANE3D: {
      PLANE3D vValue;
      strmStream >> vValue(1) >> vValue(2) >> vValue(3) >> vValue.pl_distance;
      varData.FromPlane3d(vValue);
    } break;

    case CAbstractVar::TYPE_BOX3F: {
      BOX3F box;
      strmStream >> box.minvect(1) >> box.minvect(2) >> box.minvect(3);
      strmStream >> box.maxvect(1) >> box.maxvect(2) >> box.maxvect(3);
      varData.FromBox3f(box);
    } break;

    case CAbstractVar::TYPE_BOX3D: {
      BOX3D box;
      strmStream >> box.minvect(1) >> box.minvect(2) >> box.minvect(3);
      strmStream >> box.maxvect(1) >> box.maxvect(2) >> box.maxvect(3);
      varData.FromBox3d(box);
    } break;

    case CAbstractVar::TYPE_QUAT4F: {
      QUAT4F q;
      strmStream >> q.q_w >> q.q_x >> q.q_y >> q.q_z;
      varData.FromQuat4f(q);
    } break;

    case CAbstractVar::TYPE_MAT33F: {
      MAT33F mat;
      strmStream.Read_t(&mat, sizeof(FLOATmatrix3D));
      varData.FromMat33f(mat);
    } break;

    case CAbstractVar::TYPE_PLACEMENT3D: {
      CPlacement3D pl;
      strmStream.Read_t(&pl, sizeof(CPlacement3D));
      varData.FromPlacement3D(pl);
    } break;

    case CAbstractVar::TYPE_UUID: {
      FUuid uuid;
      strmStream.Read_t(uuid.Data(), 16);
      varData.FromUuid(uuid);
    } break;

    case CAbstractVar::TYPE_STRING: {
      FString str;
      strmStream >> str;
      varData.FromString(str);
    } break;

    case CAbstractVar::TYPE_BYTEARRAY: {
      FByteArray ba;
      strmStream >> ba;
      varData.FromByteArray(ba);
    } break;

    default: ThrowF_t("FVariant (type=%d) is not supported for reading!", ubType);
  }
  
  return strmStream;
}

CTStream &operator<<(CTStream &strmStream, const FVariant &varData)
{
  switch (varData.GetType())
  {
    case CAbstractVar::TYPE_NIL: break;
    case CAbstractVar::TYPE_BOOL: {
      strmStream << UBYTE(varData.ToBool());
    } break;

    case CAbstractVar::TYPE_S8: {
      strmStream << varData.ToInt8();
    } break;

    case CAbstractVar::TYPE_U8: {
      strmStream << varData.ToUInt8();
    } break;

    case CAbstractVar::TYPE_S16: {
      strmStream << varData.ToInt16();
    } break;

    case CAbstractVar::TYPE_U16: {
      strmStream << varData.ToUInt16();
    } break;

    case CAbstractVar::TYPE_S32: {
      strmStream << varData.ToInt();
    } break;

    case CAbstractVar::TYPE_U32: {
      strmStream << ULONG(varData.ToUInt());
    } break;

    case CAbstractVar::TYPE_S64: {
      strmStream << varData.ToInt64();
    } break;

    case CAbstractVar::TYPE_U64: {
      strmStream << varData.ToUInt64();
    } break;

    case CAbstractVar::TYPE_F32: {
      strmStream << varData.ToFloat();
    } break;

    case CAbstractVar::TYPE_F64: {
      strmStream << varData.ToDouble();
    } break;

    case CAbstractVar::TYPE_VEC2I: {
      VEC2I vValue;
      varData.ToVec2i(vValue);
      strmStream << vValue(1) << vValue(2);
    } break;

    case CAbstractVar::TYPE_VEC3I: {
      VEC3I vValue;
      varData.ToVec3i(vValue);
      strmStream << vValue(1) << vValue(2) << vValue(3);
    } break;

    case CAbstractVar::TYPE_VEC2F: {
      VEC2F vValue;
      varData.ToVec2f(vValue);
      strmStream << vValue(1) << vValue(2);
    } break;

    case CAbstractVar::TYPE_VEC3F: {
      VEC3F vValue;
      varData.ToVec3f(vValue);
      strmStream << vValue(1) << vValue(2) << vValue(3);
    } break;

    case CAbstractVar::TYPE_VEC2D: {
      VEC2D vValue;
      varData.ToVec2d(vValue);
      strmStream << vValue(1) << vValue(2);
    } break;

    case CAbstractVar::TYPE_VEC3D: {
      VEC3D vValue;
      varData.ToVec3d(vValue);
      strmStream << vValue(1) << vValue(2) << vValue(3);
    } break;

    case CAbstractVar::TYPE_PLANE3F: {
      PLANE3F vValue;
      varData.ToPlane3f(vValue);
      strmStream << vValue(1) << vValue(2) << vValue(3) << vValue.pl_distance;
    } break;

    case CAbstractVar::TYPE_PLANE3D: {
      PLANE3D vValue;
      varData.ToPlane3d(vValue);
      strmStream << vValue(1) << vValue(2) << vValue(3) << vValue.pl_distance;
    } break;

    case CAbstractVar::TYPE_BOX3F: {
      BOX3F box;
      varData.ToBox3f(box);
      strmStream << box.minvect(1) << box.minvect(2) << box.minvect(3);
      strmStream << box.maxvect(1) << box.maxvect(2) << box.maxvect(3);
    } break;

    case CAbstractVar::TYPE_BOX3D: {
      BOX3D box;
      varData.ToBox3d(box);
      strmStream << box.minvect(1) << box.minvect(2) << box.minvect(3);
      strmStream << box.maxvect(1) << box.maxvect(2) << box.maxvect(3);
    } break;

    case CAbstractVar::TYPE_QUAT4F: {
      QUAT4F q;
      varData.ToQuat4f(q);
      strmStream << q.q_w << q.q_x << q.q_y << q.q_z;
    } break;

    case CAbstractVar::TYPE_MAT33F: {
      MAT33F mat;
      varData.ToMat33f(mat);
      strmStream.Write_t(&mat, sizeof(FLOATmatrix3D));
    } break;

    case CAbstractVar::TYPE_PLACEMENT3D: {
      CPlacement3D pl;
      varData.ToPlacement3D(pl);
      strmStream.Write_t(&pl, sizeof(CPlacement3D));
    } break;

    case CAbstractVar::TYPE_UUID: {
      FUuid uuid;
      varData.ToUuid(uuid);
      strmStream.Write_t(uuid.Data(), 16);
    } break;

    case CAbstractVar::TYPE_STRING: {
      FString str;
      varData.ToString(str);
      strmStream << str;
    } break;

    case CAbstractVar::TYPE_BYTEARRAY: {
      FByteArray ba;
      varData.ToByteArray(ba);
      strmStream << ba;
    } break;

    default: ThrowF_t("FVariant (type=%d) is not supported for writing!", varData.GetType());
  }
  
  return strmStream;
}

#include <Core/Templates/NameTable.cpp>

void CTStream::ReadString_t(FString &str)
{
  ASSERT(str.IsValid());

  INDEX iLength;
  (*this) >> iLength; // read length
  ASSERT(iLength >= 0);

  // allocate that much memory
  FreeMemory(str.str_String);
  str.str_String = (char *)AllocMemory(iLength + 1);  // take end-marker in account

  // if the string is not empty then read string
  if (iLength > 0) {
    Read_t(str.Data(), iLength);  // without end-marker
  }

  str.Data()[iLength] = 0; // terminate
}

void CTStream::WriteString_t(const FString &str)
{
  ASSERT(str.IsValid());

  const ULONG iStringLen = str.Length(); // calculate size
  (*this) << iStringLen; // write size

  // if the string is not empty then write string
  if (iStringLen > 0) {
    Write_t(str.ConstData(), iStringLen); // without end-marker
  }
}

void CTStream::ReadPath_t(FPath &fnm)
{
  FPathDictionary &dict = GetDictionary();
  ULONG iFileName = ULONG(-1);
  
  // if dictionary is enabled
  if (dict.dict_dmDictionaryMode == FPathDictionary::DM_ENABLED) {
    (*this) >> iFileName; // read the index in dictionary
    dict.Get(fnm, iFileName); // get that file from the dictionary

  // if dictionary is processing or not active
  } else {
    char strTag[] = "_FNM"; strTag[0] = 'D';  // must create tag at run-time!

    // skip dependency catcher header
    ExpectID_t(strTag);    // data filename

    // read the string
    (*this) >> (FString &)fnm;
  }
}

void CTStream::WritePath_t(const FPath &fnm)
{
  FPathDictionary &dict = GetDictionary();
  ULONG iFileName = ULONG(-1);
  
  // if dictionary is enabled
  if (dict.dict_dmDictionaryMode == FPathDictionary::DM_ENABLED) {
    dict.Add(fnm, iFileName);
    (*this) << iFileName;

  // if dictionary is processing or not active
  } else {
    char strTag[] = "_FNM"; strTag[0] = 'D';  // must create tag at run-time!

    // write dependency catcher header
    WriteID_t(strTag);     // data filename

    // write the string
    (*this) << (const FString &)fnm;
  }
}
