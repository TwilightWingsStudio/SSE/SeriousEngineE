/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/CRC.h>
#include <Core/Base/Translation.h>
#include <Core/Base/ErrorReporting.h>
#include <Core/Base/Console.h>

#include <Core/Math/Functions.h>

#include <Core/IO/FileName.h>
#include <Core/IO/DataStream.h>
#include <Core/IO/ArchiveEntry.h>
#include <Core/IO/BsaArchive.h>
#include <Core/IO/BsaArchive_internal.h>

#include <Core/Threading/Synchronization.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>
#include <Core/Templates/NameTable.cpp>

extern ULONG CalcArchiveEntryChecksum_t(FArchiveEntry *pEntry, FReadWriteDevice *pDevice);

static void BsaLoadLegacy_t(FArchive *pArchive, FDataStream &strm)
{
  FReadWriteDevice *pDevice = strm.Device();

  TStaticArray<BsaSizeAndOffset> aSizeAndOffsets;
  TStaticArray<ULONG> aNameOffsets;
  TStaticArray<UQUAD> aHashes;
  ULONG iBytesUsedForTable, iNumberOfFiles;

  if (pDevice->Size() < 12) {
    ThrowF_t(TRANS("%s : Too small to be a valid BSA archive."), pArchive->GetName().ConstData());
  }

  pDevice->Read((char *)&iBytesUsedForTable, 4);
  pDevice->Read((char *)&iNumberOfFiles, 4);

  if (iNumberOfFiles == 0 || iBytesUsedForTable == 0) {
    return;
  }

  // Allocate the stuff.
  aSizeAndOffsets.New(iNumberOfFiles);
  aNameOffsets.New(iNumberOfFiles);
  aHashes.New(iNumberOfFiles);

  FByteArray baStringBuf('\x00', iBytesUsedForTable - 12 * iNumberOfFiles);

  // Read the stuff.
  pDevice->Read((char *)&aSizeAndOffsets[0], sizeof(BsaSizeAndOffset) * iNumberOfFiles);
  pDevice->Read((char *)&aNameOffsets[0], sizeof(ULONG) * iNumberOfFiles);
  pDevice->Read((char *)baStringBuf.Data(), baStringBuf.Size());

  // Finally build archive entries.
  for (int i = 0; i < iNumberOfFiles; i++)
  {
    const INDEX iNameOffset = aNameOffsets[i];
    FString strName((char *)&baStringBuf.Data()[iNameOffset]);

    FArchiveEntry *pNew = new FArchiveEntry(pArchive, strName);
    pNew->SetCompressionMethod(COMPRESSION_METHOD_STORE);
    pNew->SetUncompressedSize(aSizeAndOffsets[i].Size);
    pNew->SetOffset(aSizeAndOffsets[i].Offset);
    pArchive->RegisterEntry(pNew);
  }

  pArchive->CalculateChecksums_t(pDevice);
}

static void ReadHeader_t(FBsaLoaderContext &ctx, FDataStream &strm)
{
  BsaHeader &header = ctx.Header;

  strm >> header.Version >> header.Offset >> header.Flags >> header.DirCount >> header.FileCount >>
    header.TotalDirNameLength >> header.TotalFileNameLength >> header.FileFlags >> header.Padding;
}

static void ReadDirsAndFiles_t(FBsaLoaderContext &ctx, FDataStream &strm)
{
  ctx.Directories.New(ctx.Header.DirCount);

  // Read directory records...
  for (int i = 0; i < ctx.Directories.Count(); i++)
  {
    BsaDirectory &dir = ctx.Directories[i];
    ULONG Padding_0, Padding_1, FileCount;

    if (ctx.Header.Version >= BSA_VERSION_TES5_SE) {
      strm >> dir.Hash >> FileCount >> Padding_0 >> dir.Offset >> Padding_1;
    } else {
      strm >> dir.Hash >> FileCount >> dir.Offset;
    }

    dir.Files.New(FileCount); // Allocate files.
    //printf("  Folder: %d\n", dir.Files.Count);
  }

  // Read file record blocks...
  for (int j = 0; j < ctx.Directories.Count(); j++)
  {
    BsaDirectory &dir = ctx.Directories[j];

    if (ctx.Header.Flags & BSA_FLAG_INC_DIRNAMES) {
      ReadBsaStringLen_t(strm, dir.strName);
    }

    for (int k = 0; k < dir.Files.Count(); k++)
    {
      BsaFileRecord &entry = dir.Files[k];
      ULONG Size;
      strm >> entry.Hash >> Size >> entry.BlockOffset;

      if (Size & BSA_COMPRESSED_FILE_BIT) {
        Size ^= BSA_COMPRESSED_FILE_BIT;

        if (ctx.Header.Version >= BSA_VERSION_TES5_SE) {
          entry.eCompressionMethod = COMPRESSION_METHOD_DEFLATE;
        } else {
          entry.eCompressionMethod = COMPRESSION_METHOD_LZ4; // LZ4
        }

        entry.CompressedSize = Size;
      } else {
        entry.UncompressedSize = Size;
      }
    }
  }

  //! Read and set the filenames.
  if (ctx.Header.Flags & BSA_FLAG_INC_FILENAMES) {
    if (ctx.Header.TotalFileNameLength > 0) {
      FByteArray baFileNames('\x00', ctx.Header.TotalFileNameLength);
      strm.Read(baFileNames.Data(), ctx.Header.TotalFileNameLength);
      FDataStream strmFileNames(baFileNames);

      for (int l = 0; l < ctx.Directories.Count(); l++)
      {
        BsaDirectory &dir = ctx.Directories[l];
      
        for (int m = 0; m < dir.Files.Count(); m++)
        {
          BsaFileRecord &entry = dir.Files[m];
          ReadBsaString_t(strmFileNames, entry.strName);
        }
      }
    }
  }
}

static void RegisterEntries(FBsaLoaderContext &ctx)
{
  // Read data blocks.
  for (int i = 0; i < ctx.Directories.Count(); i++)
  {
    BsaDirectory &dir = ctx.Directories[i];

    for (int j = 0; j < dir.Files.Count(); j++)
    {
      BsaFileRecord &entry = dir.Files[j];

      FString fnm;
      fnm.PrintF("%s\\%s", dir.strName, entry.strName);

      FArchiveEntry *pNew = new FArchiveEntry(ctx.pArchive, fnm);
      pNew->SetCompressionMethod(entry.eCompressionMethod);
      pNew->SetUncompressedSize(entry.UncompressedSize);
      pNew->SetCompressedSize(entry.CompressedSize);
      pNew->SetOffset(entry.DataOffset);
      ctx.pArchive->RegisterEntry(pNew);
    }
  }
}

void FBsaArchiveLoader::Load_t(FArchive *pArchive, FDataStream &strm) const
{
  FReadWriteDevice *pDevice = strm.Device(); 

  FByteArray baExpectedTagLegacy("\x00\x01\x00\x00", 4);
  FByteArray baExpectedTag("BSA\x00", 4);

  FByteArray baSignature('\x00', 4);
  pDevice->Read((char *)baSignature.Data(), 4);

  // Legacy BSA format.
  if (baSignature == baExpectedTagLegacy) {
    BsaLoadLegacy_t(pArchive, strm);
    return;
  }

  if (baSignature != baExpectedTag) {
    ThrowF_t(TRANS("%s : Missing BSA signature!"), pArchive->GetName().ConstData());
  }

  strm.SetByteOrder(FDataStream::BO_LITTLEENDIAN);
  FBsaLoaderContext ctx(pArchive);
  ReadHeader_t(ctx, strm);

  if (ctx.Header.Flags & BSA_FLAG_BIGENDIAN) {
    strm.SetByteOrder(FDataStream::BO_BIGENDIAN);
  }

  ReadDirsAndFiles_t(ctx, strm);

  // Read data blocks.
  for (int i = 0; i < ctx.Directories.Count(); i++)
  {
    BsaDirectory &dir = ctx.Directories[i];

    //printf("%s\n", dir.strName);

    for (int j = 0; j < dir.Files.Count(); j++)
    {
      BsaFileRecord &entry = dir.Files[j];

      strm.Device()->Seek(entry.BlockOffset);

      if (ctx.Header.Flags & BSA_FLAG_EMBED_FILE_NAMES) {
        FString strDummy;
        ReadBsaStringLen_t(strm, strDummy);
      }

      if (entry.IsCompressed()) {
        strm >> entry.UncompressedSize;
      }

      entry.DataOffset = strm.Device()->Pos();
    }
  }

  RegisterEntries(ctx);
  pArchive->CalculateChecksums_t(strm.Device());
}

bool FBsaArchiveLoader::IsCompressionSupported(enum CompressionMethod eMethod) const
{
  switch (eMethod)
  {
    case COMPRESSION_METHOD_STORE:
    case COMPRESSION_METHOD_DEFLATE:
    case COMPRESSION_METHOD_LZ4:
      return true;
  }

  return false;
}

void FBsaArchiveLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".bsa";
}