/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Resource.h>

#include <Core/IO/Stream.h>
#include <Core/Base/CRCTable.h>

/*
 * Default constructor.
 */
CResource::CResource(void)
{
}

/*
 * Destructor.
 */
CResource::~CResource(void)
{
  // must not be used at all
  ASSERT(GetReferenceCount() == 0);    // look at _strLastCleared for possible name
}

/*
 * Clear the object.
 */
void CResource::Clear(void)
{
  // mark that you have changed
  MarkChanged();

  // clear the filename
  ser_FileName.Clear();
}

/* Get the description of this object. */
FString CResource::GetDescription(void)
{
  return "<no description>";
}

/*
 * Load from file.
 */
void CResource::Load_t(const FPath fnFileName)  // throw char *
{
  ASSERT(!IsReferenced());

  // mark that you have changed
  MarkChanged();

  // open a stream
  CTFileStream istrFile;
  istrFile.Open_t(fnFileName);

  // read object from stream
  Read_t(&istrFile);
  // if still here (no exceptions raised)
  // remember filename
  ser_FileName = fnFileName;
}

/*
 * Reload from file.
 */
void CResource::Reload(void)
{
  // mark that you have changed
  MarkChanged();

  FPath fnmOldName = ser_FileName;
  Clear();
  // try to
  //try {
    // open a stream
    CTFileStream istrFile;
    istrFile.Open_t(fnmOldName);
    // read object from stream
    Read_t(&istrFile);

  // if there is some error while reloading
  //} catch (char *strError) {
    // quit the application with error explanation
    //FatalError(TRANS("Cannot reload file '%s':\n%s"), (FString&)fnmOldName, strError);
  //}

  // if still here (no exceptions raised)
  // remember filename
  ser_FileName = fnmOldName;
}

/*
 * Save to file.
 */
void CResource::Save_t(const FPath fnFileName)  // throw char *
{
  // open a stream
  CTFileStream ostrFile;
  ostrFile.Create_t(fnFileName);

  // write object to stream
  Write_t(&ostrFile);
  // if still here (no exceptions raised)
  // remember new filename
  ser_FileName = fnFileName;
}

// gather the CRC of the file
void CResource::AddToCRCTable(void)
{
  // add the file to CRC table
  CRCT_AddFile_t(ser_FileName);
}
