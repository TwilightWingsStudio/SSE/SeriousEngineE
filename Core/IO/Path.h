/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PATH_H
#define SE_INCL_PATH_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/String.h>

//! Special kind of string, dedicated to storing paths.
class CORE_API FPath : public FString
{
  private:
    //! Construct from character string.
    inline FPath(const char *pString) : FString(pString) {};

  public:
    //! Default constructor.
    inline FPath(void) {};

    //! Copy constructor.
    inline FPath(const FString &strOriginal) : FString(strOriginal) {};

    //! Constructor from character string for insertion in exe-file.
    inline FPath(const char *pString, int i) : FString(pString+i) {};

    //! Assignment.
    FPath &operator=(const char *strCharString);
    
    //! Assignment.
    inline void operator=(const FString &strOther) {
      FString::operator=(strOther);
    };

    //! Get directory part of a filename.
    FPath FileDir(void) const;

    //! Get name part of a filename.
    FPath FileName(void) const;

    //! Get extension part of a filename.
    FPath FileExt(void) const;

    //! Get path and file name without extension.
    FPath NoExt(void) const;

    //! Set path to the absolute path, taking \.. and /.. into account.
    void SetAbsolutePath(void);

    //! Remove application path from a file name.
    BOOL RemoveApplicationPath_t(void); // throw char *

    //! Filename is its own name (used for storing in nametable).
    inline const FPath &GetName(void) { return *this; };

  public:
    void ReadFromText_t(CTStream &strmStream, const FString &strKeyword=""); // throw char *

    //! Read from stream.
    CORE_API friend CTStream &operator>>(CTStream &strmStream, FPath &fnmFileName);

    //! Write to stream.
    CORE_API friend CTStream &operator<<(CTStream &strmStream, const FPath &fnmFileName);
    
    //! Write to stream.
    CORE_API friend FDataStream &operator<<(FDataStream &strm, const FPath &fnm);

    //! Read from stream.
    CORE_API friend FDataStream &operator>>(FDataStream &strm, FPath &fnm);
};

// macro for defining a literal filename in code (EFNM = exe-filename)
// macro for defining a literal filename in code (EFNM = exe-filename)
#define CTFILENAME(string) FPath("EFNM" string, 4)
#define DECLARE_CTFILENAME(name, string) FPath name("EFNM" string, 4)

#endif  /* include-once check. */
