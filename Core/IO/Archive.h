/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ARCHIVE_H
#define SE_INCL_ARCHIVE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>
#include <Core/Templates/NameTable.h>
#include <Core/IO/ArchiveFormat.h>

enum ArchiveFlags
{
  ARCHIVE_FLAG_MOD = (1L << 0),
};

//! Any archive.
class CORE_API FArchive
{
  public:
    //! Do your job!
    virtual void CalculateChecksums_t(FReadWriteDevice *pDevice);

    //! Setup the stuff!
    virtual void Initialize();

    //! Destructor.
    virtual ~FArchive();

    //! Returns number of files in this archive.
    virtual ULONG GetFileCount() const;

    //! Returns pointer to file info.
    virtual FArchiveEntry* FindEntry(const FPath &fnm);

    //! Prepare for read.
    virtual void Prepare_t();

    //! Read file from archive using filename.
    virtual void ReadFile_t(const FPath &fnm, UBYTE *pBuffer);

    //! Read file from archive.
    virtual void ReadFile_t(const FArchiveEntry &ae, UBYTE *pBuffer);

    //! Close the archive.
    virtual void Close();

  public:
    //! Returns numeric identifier.
    inline INDEX GetId() const
    {
      return _idArchive;
    }

    //! Change numeric identifier.
    inline void SetId(INDEX idArchive)
    {
      _idArchive = idArchive;
    }

    //! Returns archive filename.
    inline const FPath& GetName() const
    {
      return _fnmName;
    }

    //! Change archive filename.
    void SetName(const FPath &fnm);

    //! Check if this archive is from a mod.
    inline BOOL IsMod() const
    {
      return _ulFlags & ARCHIVE_FLAG_MOD;
    }

    //! Mark if this archive is from a mod or not.
    inline void SetMod(BOOL bMod)
    {
      if (bMod) {
        _ulFlags |= ARCHIVE_FLAG_MOD;
      } else {
        _ulFlags &= ~ARCHIVE_FLAG_MOD;
      }
    }

    //! Check if archive open.
    BOOL IsOpen() const;

    //! Check if compression method supported by format.
    virtual bool IsCompressionSupported(enum CompressionMethod eMethod) const
    {
      return true;
    }

    //! Determine archive format.
    virtual ArchiveFormat GetFormat() const
    {
      return ARCHIVE_FORMAT_INVALID;
    }

  public:
    void RegisterEntry(FArchiveEntry *pEntry);

  public:
    FReadWriteDevice *_pDevice;
    TDynamicContainer<FArchiveEntry> _ctFileInfos; // objects within archive
    TNameTable<FArchiveEntry> _ntFileInfos; // fast lookup table
    ULONG _idArchive;
    FPath _fnmName;
    ULONG _ulFlags;
};

#endif /* include-once check. */