/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_RESOURCESTOCK_H
#define SE_INCL_RESOURCESTOCK_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>
#include <Core/Templates/NameTable.h>

//! Template for stock of some kind of objects that can be saved and loaded.
class CORE_API CResourceStock
{
  public:
    TDynamicContainer<CResource> st_ctObjects;   // objects on stock
    TNameTable<CResource> st_ntObjects;  // name table for fast lookup

  public:
    //! Default constructor. 
    CResourceStock(void);
    
    //! Destructor. 
    ~CResourceStock(void);

    //! Obtain an object from stock - loads if not loaded. 
    CResource *Obtain_t(const FPath &fnmFileName); // throw char *
    
    //! Release an object when not needed any more. 
    void Release(CResource *ptObject);
    
    //! Free all unused elements of the stock
    void FreeUnused(void);
    
    //! Calculate amount of memory used by all objects in the stock
    SLONG CalculateUsedMemory(void);
    
    //! Dump memory usage report to a file
    void DumpMemoryUsage_t(CTStream &strm); // throw char *
    
    //! Get number of total elements in stock
    INDEX GetTotalCount(void);
    
    //! Get number of used elements in stock
    INDEX GetUsedCount(void);

    //! Returns name of the stock.
    virtual const char *GetName() const = 0;
    
  protected:
    virtual CResource *New() = 0;
};

extern CORE_API CResourceStock *_pModuleStock;
extern CORE_API CResourceStock *_pAnimStock;
extern CORE_API CResourceStock *_pAnimSetStock;
extern CORE_API CResourceStock *_pEntityClassStock;
extern CORE_API CResourceStock *_pMeshStock;
extern CORE_API CResourceStock *_pModelStock;
extern CORE_API CResourceStock *_pModelConfigStock;
extern CORE_API CResourceStock *_pShaderStock;
extern CORE_API CResourceStock *_pSkeletonStock;
extern CORE_API CResourceStock *_pSoundStock;
extern CORE_API CResourceStock *_pTextureStock;

#endif  /* include-once check. */