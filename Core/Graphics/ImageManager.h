/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_IMAGEMANAGER_H
#define SE_INCL_IMAGEMANAGER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/DynamicContainer.h>
#include <Core/Graphics/ImageFormat.h>

//! This class provides simple API for image loading/saving.
class CORE_API FImageManager
{
  public:
    //! Constructor.
    FImageManager();

    void ReadImage_t(FImage &image, CTStream &strm, enum EImageFormat eFormat);
    void WriteImage_t(FImage &image, CTStream &strm, enum EImageFormat eFormat);

    //! Set of loaders for various image formats.
    inline TDynamicContainer<FImageLoader> &GetLoaders()
    {
      return _ctLoaders;
    }

    //! Set of savers for various image formats.
    inline TDynamicContainer<FImageSaver> &GetSavers()
    {
      return _ctSavers;
    }

  public:
    TDynamicContainer<FImageLoader> _ctLoaders;
    TDynamicContainer<FImageSaver> _ctSavers;
};

CORE_API extern FImageManager *_pImageMgr;

#endif  /* include-once check. */