/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Graphics/S3TC.h>

static int Unpack565(UBYTE *pBlock, int blockIndex, int packed_offset, UBYTE *pColor, int color_offset)
{
  // Build packed value
  int value = pBlock[blockIndex + packed_offset] | (pBlock[blockIndex + 1 + packed_offset] << 8);

  // get components in the stored range
  char red = (value >> 11) & 0x1F;
  char green = (value >> 5) & 0x3F;
  char blue = value & 0x1F;

  // Scale up to 8 Bit
  pColor[0 + color_offset] = (red << 3) | (red >> 2);
  pColor[1 + color_offset] = (green << 2) | (green >> 4);
  pColor[2 + color_offset] = (blue << 3) | (blue >> 2);
  pColor[3 + color_offset] = (char)0xFF;

  return value;
}

static void DecompressColor(UBYTE *pBuffer, UBYTE *pBlock, int blockIndex, bool isDxt1)
{
  // Unpack Endpoints
  UBYTE codes[16];
  int a = Unpack565(pBlock, blockIndex, 0, &codes[0], 0);
  int b = Unpack565(pBlock, blockIndex, 2, &codes[0], 4);

  // generate Midpoints
  for (int i = 0; i < 3; i++)
  {
    int c = codes[i];
    int d = codes[4 + i];

    if (isDxt1 && a <= b)
    {
      codes[8 + i] = (c + d) / 2;
      codes[12 + i] = 0;
    } else {
      codes[8 + i] = (2 * c + d) / 3;
      codes[12 + i] = (c + 2 * d) / 3;
    }
  }

  // Fill in alpha for intermediate values
  codes[8 + 3] = (char)255;
  codes[12 + 3] = (isDxt1 && a <= b) ? (char)0 : (char)255;

  // unpack the indices
  char indices[16];
  for (int j = 0; j < 4; j++)
  {
    char packed = pBlock[blockIndex + 4 + j];

    indices[0 + j * 4] = packed & 0x3;
    indices[1 + j * 4] = (packed >> 2) & 0x3;
    indices[2 + j * 4] = (packed >> 4) & 0x3;
    indices[3 + j * 4] = (packed >> 6) & 0x3;
  }

  // store out the colours
  for (int k = 0; k < 16; k++)
  {
    int offset = 4 * indices[k];

    pBuffer[4 * k + 0] = codes[offset + 0];
    pBuffer[4 * k + 1] = codes[offset + 1];
    pBuffer[4 * k + 2] = codes[offset + 2];
    pBuffer[4 * k + 3] = codes[offset + 3];
  }
}

static void DecompressAlphaDxt3(UBYTE *pBuffer, UBYTE *pBlock, int blockIndex)
{
  // Unpack the alpha values pairwise
  for (int i = 0; i < 8; i++)
  {
    // Quantise down to 4 bits
    char quant = pBlock[blockIndex + i];

    char lo = quant & 0x0F;
    char hi = quant & 0xF0;

    // Convert back up to bytes
    pBuffer[8 * i + 3] = lo | (lo << 4);
    pBuffer[8 * i + 7] = hi | (hi >> 4);
  }
}

static void DecompressAlphaDxt5(UBYTE *pBuffer, UBYTE *pBlock, int blockIndex)
{
  // Get the two alpha values
  char alpha0 = pBlock[blockIndex + 0];
  char alpha1 = pBlock[blockIndex + 1];

  // compare the values to build the codebook
  char codes[8];
  codes[0] = alpha0;
  codes[1] = alpha1;
  if (alpha0 <= alpha1)
  {
    // Use 5-Alpha Codebook
    for (int i = 1; i < 5; i++) {
      codes[1 + i] = ((5 - i) * alpha0 + i * alpha1) / 5;
    }

    codes[6] = 0;
    codes[7] = (char)255;
  } else {
    // Use 7-Alpha Codebook
    for (int i = 1; i < 7; i++)
    {
      codes[i + 1] = ((7 - i) * alpha0 + i * alpha1) / 7;
    }
  }

  // decode indices
  char indices[16];
  int blockSrc_pos = 2;
  int indices_pos = 0;
  for (int i = 0; i < 2; i++)
  {
    // grab 3 bytes
    int value = 0;
    for (int j = 0; j < 3; j++)
    {
      int _byte = pBlock[blockIndex + blockSrc_pos++];
      value |= (_byte << 8 * j);
    }

    // unpack 8 3-bit values from it
    for (int k = 0; k < 8; k++)
    {
      int index = (value >> 3 * k) & 0x07;
      indices[indices_pos++] = index;
    }
  }

  // write out the indexed codebook values
  for (int j = 0; j < 16; j++)
  {
    pBuffer[4 * j + 3] = codes[indices[j]];
  }
}

//! Decompress 4x4 pixels square.
static void Decompress(UBYTE *pBuffer, UBYTE *pBlock, int blockIndex, Cs3tcCompressor::CompressionType eType)
{
  // get the block locations
  int colorBlockIndex = blockIndex;

  if (eType == Cs3tcCompressor::TYPE_DXT3 || eType == Cs3tcCompressor::TYPE_DXT5)
      colorBlockIndex += 8;

  // decompress color
  DecompressColor(pBuffer, pBlock, colorBlockIndex, eType == Cs3tcCompressor::TYPE_DXT1);

  // decompress alpha separately if necessary
  if (eType == Cs3tcCompressor::TYPE_DXT3) {
    DecompressAlphaDxt3(pBuffer, pBlock, blockIndex);
  } else if (eType == Cs3tcCompressor::TYPE_DXT5) {
    DecompressAlphaDxt5(pBuffer, pBlock, blockIndex);
  }
}

void Cs3tcCompressor::Decode(UBYTE *pData, UBYTE *pImage, PIX width, PIX height, CompressionType eType)
{
  ASSERT((width % 4) == 0 && (width % 4) == 0);
  
  // initialise the block input
  int sourceBlock_pos = 0;
  int bytesPerBlock = eType == TYPE_DXT1 ? 8 : 16;
  UBYTE targetRGBA[4 * 16];
  
  // loop over blocks
  for (int y = 0; y < height; y += 4)
  {
    for (int x = 0; x < width; x += 4)
    {
      // decompress the block
      int targetRGBA_pos = 0;
      //if (data.Length == sourceBlock_pos) continue;
      Decompress(&targetRGBA[0], pData, sourceBlock_pos, eType);

      // Write the decompressed pixels to the correct image locations
      for (int py = 0; py < 4; py++)
      {
        for (int px = 0; px < 4; px++)
        {
          int sx = x + px;
          int sy = y + py;

          if (sx < width && sy < height) {
            int targetPixel = 4 * (width * sy + sx);

            pImage[targetPixel + 0] = targetRGBA[targetRGBA_pos + 0];
            pImage[targetPixel + 1] = targetRGBA[targetRGBA_pos + 1];
            pImage[targetPixel + 2] = targetRGBA[targetRGBA_pos + 2];
            pImage[targetPixel + 3] = targetRGBA[targetRGBA_pos + 3];

            targetRGBA_pos += 4;
          } else {
            // Ignore that pixel
            targetRGBA_pos += 4;
          }
        }
      }

      sourceBlock_pos += bytesPerBlock;
    }
  }
}
