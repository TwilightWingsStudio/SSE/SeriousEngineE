/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

struct STGAHeader
{
  UBYTE IdLenght;
  UBYTE ColorMapType;
  UBYTE ImageType;
  UBYTE ColorMapSpec[5];
  UWORD Xorigin;
  UWORD Yorigin;
  UWORD	Width;
  UWORD Height;
  UBYTE BitsPerPixel;
  UBYTE Descriptor;
};

enum TgaImageType
{
  //! Image data is not present.
  TGA_TYPE_INVALID = 0,

  //! Uses pallette.
  TGA_TYPE_INDEXED = 1,

  //! RGB.
  TGA_TYPE_TRUECOLOR = 2,

  //! Black and white.
  TGA_TYPE_GRAYSCALE = 3,
};

#define TGA_FLAG_RLE (1L << 3)

class FTgaLoaderContext : public FImageLoaderContext
{
  public:
    STGAHeader Header;
    BOOL bCompressed;

  public:
    //! Constructor.
    FTgaLoaderContext(FImage *p)
    {
      pImage = p;
      bCompressed = FALSE;
    }
};