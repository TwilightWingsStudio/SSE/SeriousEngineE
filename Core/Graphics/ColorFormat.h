/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_COLORFORMAT_H
#define SE_INCL_COLORFORMAT_H

//! All possible color formats.
enum EColorFormat
{
  //! If color format is not specified.
  ECF_INVALID = 0,
  
  //! 8 bits per pixel
  ECF_GRAYSCALE,
  
  //! High Color (5-5-5)
  ECF_R5G5B5,

  //! High Color (5-6-5)
  ECF_R5G6B5,
  
  //! High Color (5-5-5-1)
  ECF_R5G5B5A1,

  //! High Color (1-5-5-5)
  ECF_A1R5G5B5,
  
  //! True Color (8-8-8)
  ECF_RGB,
  
  //! True Color (8-8-8-8)
  ECF_RGBA,
  
  //! True Color (8-8-8-8)
  ECF_ARGB,
};

#endif  /* include-once check. */