/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/Graphics/Image.h>
#include <Core/Graphics/ImageManager.h>
#include <Core/Graphics/ImageManager_internal.h>

#include <Core/Graphics/TgaImage.h>
#include <Core/Graphics/TgaImage_internal.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

extern size_t GetImageDataSize(PIX pixWidth, PIX pixHeight, EColorFormat);

static void ReadCompressedImage_t(FTgaLoaderContext &ctx, CTStream &strm)
{
  size_t dataSize = ctx.pImage->Data().Size();
  UBYTE *pMemory = ctx.pImage->Data().Data();
  SLONG slBytesPerPixel = ctx.Header.BitsPerPixel / 8;
  SLONG slCurrentByte = 0;
  
  while (slCurrentByte < dataSize)
  {
    UBYTE ubControl;
    
    strm.Read_t(&ubControl, 1);
    
    // RAW chunk.
    if (ubControl < 128) {
      ubControl++;
      strm.Read_t(&pMemory[slCurrentByte], slBytesPerPixel * ubControl);
      slCurrentByte += slBytesPerPixel * ubControl;

    // RLE-header.
    } else {
      ubControl -= 127; // ~1XXXXXXX
      
      SLONG slOffset = slCurrentByte;
      strm.Read_t(&pMemory[slOffset], slBytesPerPixel);
      slCurrentByte += slBytesPerPixel;
      
      for (SLONG i = 1; i < ubControl; i++)
      {
        for (SLONG j = 0; j < slBytesPerPixel; j++)
        {
          pMemory[slCurrentByte + j] = pMemory[slOffset + j];
        }
        
        slCurrentByte += slBytesPerPixel;
      }
    }
  }
}

void FTgaImageLoader::Load_t(FImage *pImage, CTStream &strm) const
{
  FTgaLoaderContext ctx(pImage);
  CTFileStream inFile;

  strm.Read_t(&ctx.Header, sizeof(STGAHeader));

  if (ctx.Header.ImageType & TGA_FLAG_RLE) {
    ctx.Header.ImageType ^= TGA_FLAG_RLE;
    ctx.bCompressed = true;
  }
  
  SLONG slBytesPerPixel = ctx.Header.BitsPerPixel / 8;
  
  // Skip identification field.
  if (ctx.Header.IdLenght) {
    strm.Seek_t(ctx.Header.IdLenght, CTStream::SD_CUR);
  }
 
  // For indexed images.
  if (ctx.Header.ColorMapType) {
    throw("Color maps are unsupported!");
  }
  
  if (slBytesPerPixel != 3 && slBytesPerPixel != 4) {
    throw(TRANS("Unsupported BitsPerPixel in TGA format."));
  }

  if (ctx.Header.ImageType == TGA_TYPE_INVALID) {
    throw("Missing image data!");
  }

  enum EColorFormat eFormat = ECF_INVALID;

  if (slBytesPerPixel == 4) {
    eFormat = ECF_RGBA;
  } else if (slBytesPerPixel == 3) {
    eFormat = ECF_RGB;
  }

  pImage->Reallocate(ctx.Header.Width, ctx.Header.Height, eFormat);

  if (ctx.Header.ImageType == TGA_TYPE_TRUECOLOR) {
    // RLE-encoded true-color image.
    if (ctx.bCompressed) {
      ReadCompressedImage_t(ctx, strm);
    // Uncompressed true-color image.
    } else {
      strm.Read_t(pImage->GetData(), pImage->Data().Size());
    }
  }
}

void FTgaImageLoader::GetValidExtensions(TStaticStackArray<FString> &astr) const
{
  astr.Push() = ".tga";
}
