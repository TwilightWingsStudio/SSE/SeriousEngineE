/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/Graphics/Image.h>
#include <Core/Graphics/ImageLoader.h>
#include <Core/Graphics/ImageManager.h>
#include <Core/Graphics/TgaImage.h>

#include <Core/Templates/StaticArray.cpp>
#include <Core/Templates/DynamicContainer.cpp>

FImageManager *_pImageMgr = NULL;

FImageManager::FImageManager()
{
  // Internal loaders.
  _ctLoaders.Push() = new FTgaImageLoader;

  // Internal savers.
  _ctSavers.Push() = new FTgaImageSaver;
}

void FImageManager::ReadImage_t(FImage &image, CTStream &strm, enum EImageFormat eFormat)
{
  const FPath fnm(strm.GetDescription());

  TStaticStackArray<FString> aValidExtensions;
  aValidExtensions.SetAllocationStep(4);

  TDynamicContainer<FImageLoader> _ctAssumedByExt;
  FOREACHINDYNAMICCONTAINER(_ctLoaders, FImageLoader, itt)
  {
    FImageLoader &loader = itt.Current();
    aValidExtensions.Clear();
    loader.GetValidExtensions(aValidExtensions);

    for (int i = 0; i < aValidExtensions.Count(); i++)
    {
      if (aValidExtensions[i] == fnm.FileExt()) {
        _ctAssumedByExt.Push() = &loader;
        break;
      }
    }
  }

  if (_ctAssumedByExt.Count() == 0) {
    ThrowF_t(TRANS("Unknown image format!"));
  }

  _ctAssumedByExt.Pointer(0)->Load_t(&image, strm);
}

void FImageManager::WriteImage_t(FImage &image, CTStream &strm, enum EImageFormat eFormat)
{
  const FPath fnm(strm.GetDescription());

  TDynamicContainer<FImageSaver> _ctAssumedByExt;
  FOREACHINDYNAMICCONTAINER(_ctSavers, FImageSaver, itt)
  {
    FImageSaver &saver = itt.Current();

    if (saver.IsValidExtension(fnm)) {
      _ctAssumedByExt.Push() = &saver;
    }
  }

  if (_ctAssumedByExt.Count() == 0) {
    ThrowF_t(TRANS("Unknown image format!"));
  }

  _ctAssumedByExt.Pointer(0)->Save_t(&image, strm);
}