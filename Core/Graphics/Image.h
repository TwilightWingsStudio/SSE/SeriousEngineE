/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_IMAGE_H
#define SE_INCL_IMAGE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/ByteArray.h>
#include <Core/Graphics/ColorFormat.h>
#include <Core/Templates/StaticArray.h>

//! Hardware-independent image representation.
class CORE_API FImage
{
  protected:
    EColorFormat _eFormat;
    PIX _pixWidth;
    PIX _pixHeight;
    FByteArray _baData;
    TStaticArray<ULONG> _aPalette; // For indexed images.

  public:
    //! Default constructor.
    FImage();

    //! Construtor.
    FImage(PIX pixWidth, PIX pixHeight, EColorFormat eFormat);
    
    //! Destructor.
    ~FImage();

    //! Allocate buffer.
    void Reallocate(PIX pixWidth, PIX pixHeight, EColorFormat eFormat);

    //! Free memory.
    void Clear(void);

  public:
    inline EColorFormat GetFormat()
    {
      return _eFormat;
    }
  
    inline PIX GetWidth() const
    {
      return _pixWidth;
    }

    inline PIX GetHeight() const
    {
      return _pixHeight;
    }
    
    inline void *GetData()
    {
      return _baData.Data();
    }
    
    inline bool IsNull() const
    {
      return _baData.IsNull();
    }

    inline TStaticArray<ULONG> &GetPalette()
    {
      return _aPalette;
    }

    inline FByteArray &Data()
    {
      return _baData;
    }
};

#endif  /* include-once check. */