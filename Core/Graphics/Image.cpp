/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Memory.h>
#include <Core/Graphics/Image.h>
#include <Core/Graphics/ImageUtils.h>

#include <Core/Templates/StaticArray.cpp>

size_t GetImageDataSize(PIX pixWidth, PIX pixHeight, EColorFormat eFormat)
{
  switch (eFormat)
  {
    case ECF_GRAYSCALE:
      return pixWidth * pixHeight;

    case ECF_R5G5B5:
    case ECF_R5G6B5:
    case ECF_R5G5B5A1:
    case ECF_A1R5G5B5:
      return pixWidth * pixHeight * 2;

    case ECF_RGB:
      return pixWidth * pixHeight * 3;

    case ECF_ARGB:
    case ECF_RGBA:
      return pixWidth * pixHeight * 4;
  }

  return 0;
}

FImage::FImage()
{
  _eFormat = ECF_INVALID;
  _pixWidth = _pixHeight = 0;
}

FImage::FImage(PIX pixWidth, PIX pixHeight, EColorFormat eFormat)
{
  Reallocate(pixWidth, pixHeight, eFormat);
}

FImage::~FImage()
{
  Clear();
}

void FImage::Clear()
{
  _pixWidth = _pixHeight = 0;
  _eFormat = ECF_INVALID;

  _baData.Clear();
  _aPalette.Clear();
}

void FImage::Reallocate(PIX pixWidth, PIX pixHeight, EColorFormat eFormat)
{
  if (pixWidth <= 0 || pixHeight <= 0) {
    eFormat = ECF_INVALID;
  }

  if (eFormat == ECF_INVALID) {
    _baData.Clear();
    return;
  }

  const size_t dataSize = GetImageDataSize(pixWidth, pixHeight, eFormat);
  _eFormat = eFormat;

  // It it has similar size then just reallocate.
  if (_baData.Size() == dataSize) {
    _baData.Fill('\x00');
    return;
  }

  _baData.Clear(); // Remove old memory.
  _baData.Resize(dataSize);

  _pixWidth = pixWidth;
  _pixHeight = pixHeight;
}
