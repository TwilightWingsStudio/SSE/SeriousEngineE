/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_IMAGEUTILS_H
#define SE_INCL_IMAGEUTILS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class CORE_API FImageUtils
{
  public:
    static inline void FlipX(void *pOut, const void *pSrc, const int width,
      const int height, const int channels);

    static inline void FlipY(void *pOut, const void *pSrc, const int width,
      const int height, const int channels);
};

void FImageUtils::FlipX(void *pOut, const void *pSrc,
                           const int width,
                           const int height,
                           const int channels)
{
  const UBYTE *pSrcByte = (UBYTE *)pSrc;
  UBYTE *pDstByte = (UBYTE *)pOut;

  for (int row = 0; row < height; row++)
  {
    for (int col = 0; col < width; col++)
    {
      const UBYTE *pSrcPix = &pSrcByte[(row * width + (width - 1 - col)) * channels];
      UBYTE *pDstPix = &pDstByte[(row * width + col) * channels];

      for (int k = 0; k < channels; ++k)
      {
        pDstPix[k] = pSrcPix[k];
      }
    }
  }
}

void FImageUtils::FlipY(void *pOut, const void *pSrc,
                           const int width,
                           const int height,
                           const int channels)
{
  const UBYTE *pSrcByte = (UBYTE *)pSrc;
  UBYTE *pDstByte = (UBYTE *)pOut;

  for (int i = 0; i < width; ++i)
  {
    for (int j = 0; j < height; ++j)
    {
      const UBYTE *pSrcPix = &pSrcByte[(i + (height - 1 - j) * width) * channels];
      UBYTE *pDstPix = &pDstByte[(i + j * width) * channels];

      for (int k = 0; k < channels; ++k)
      {
        pDstPix[k] = pSrcPix[k]; // for each chanel
      }
    }
  }
}

#endif
