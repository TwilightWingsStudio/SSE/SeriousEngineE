/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_IMAGEFORMAT_H
#define SE_INCL_IMAGEFORMAT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum EImageFormat
{
  //! Erroring.
  IMAGE_FORMAT_INVALID = 0,

  //! Targa.
  IMAGE_FORMAT_TGA,
  
  //! Bitmap.
  IMAGE_FORMAT_BMP,

  //! Portable Network Graphics.
  IMAGE_FORMAT_PNG,

  //! PCExchange.
  IMAGE_FORMAT_PCX,

  //! Direct Draw Surface.
  IMAGE_FORMAT_DDS,

  //! Tag Image File Format.
  IMAGE_FORMAT_TIFF,

  //! Joint Photographic Experts Group
  IMAGE_FORMAT_JPG,

  //! Valve Texture Format.
  IMAGE_FORMAT_VTF,

  //! Wall Texture.
  IMAGE_FORMAT_WAL,
};

#endif  /* include-once check. */