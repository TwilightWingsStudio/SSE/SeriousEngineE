/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/IO/Stream.h>
#include <Core/Graphics/Image.h>
#include <Core/Graphics/TgaImage.h>
#include <Core/Graphics/TgaImage_internal.h>

#include <Core/Templates/StaticArray.cpp>

void FTgaImageSaver::Save_t(FImage *pImage, CTStream &strm) const
{
  STGAHeader header;
  header.IdLenght = 0;
  header.ColorMapType = 0;
  header.ImageType = TGA_TYPE_TRUECOLOR;
  header.ColorMapSpec[0] = 0;
  header.ColorMapSpec[1] = 0;
  header.ColorMapSpec[2] = 0;
  header.ColorMapSpec[3] = 0;
  header.ColorMapSpec[4] = 0;
  header.Xorigin = 0;
  header.Yorigin = 0;
  header.Width = pImage->GetWidth();
  header.Height = pImage->GetHeight();
  
  switch (pImage->GetFormat())
  {
    case ECF_RGB: {
      header.BitsPerPixel = 24;
    } break;

    case ECF_RGBA: {
      header.BitsPerPixel = 32;
    } break;

    default: ThrowF_t("Unsupported pixel format!");
  }

  ASSERT(pImage->GetWidth() > 0 && pImage->GetHeight() > 0);

  strm.Write_t(&header, sizeof(STGAHeader));
  strm.Write_t(pImage->GetData(), header.Width * header.Height * header.BitsPerPixel / 8);
}

bool FTgaImageSaver::IsValidExtension(const FPath &fnm) const
{
  return fnm.FileExt() == ".tga";
}