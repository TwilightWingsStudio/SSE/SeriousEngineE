/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CRCTABLE_H
#define SE_INCL_CRCTABLE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

extern CORE_API BOOL CRCT_bGatherCRCs;  // set while gathering CRCs of all loaded files

//! Init CRC table.
CORE_API void CRCT_Init(void);

//! Add one file to active list.
CORE_API void CRCT_AddFile_t(const FPath &fnm, ULONG ulCRC=0);// throw char *

//! Check if a file is added.
CORE_API BOOL CRCT_IsFileAdded(const FPath &fnm);

//! Reset all files to not active.
CORE_API void CRCT_ResetActiveList(void);

//! Free all memory used by the crc cache.
CORE_API void CRCT_Clear(void);

//! Dump list of all active files to the stream.
CORE_API void CRCT_MakeFileList_t(CTStream &strmFiles);  // throw char *

//! Dump checksums for all files from the list.
CORE_API ULONG CRCT_MakeCRCForFiles_t(CTStream &strmFiles);  // throw char *

#endif  /* include-once check. */
