/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_FSTRING_H
#define SE_INCL_FSTRING_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Types.h>

//! Main string class.
class CORE_API FString
{
  public:
    char *str_String;         // pointer to memory holding the character string

  public:
    //! Construct dummy string.
    inline FString(void);

    //! Copy constructor.
    inline FString(const FString &strOriginal);

    ///! Constructor from character string.
    inline FString(const char *pString);

    //! Constructor with formatting.
    inline FString(INDEX iDummy, const char *strFormat, ...);

    //! Destructor.
    inline ~FString();

    //! Clear the object.
    inline void Clear(void);

    //! Returns pointer to string data.
    inline char *Data()
    {
      return str_String;
    }
    
    //! Returns constant pointer to string data.
    inline const char *ConstData() const
    {
      return str_String;
    }

    //! Conversion into character string.
    inline operator const char*() const;

    //! Assignment.
    inline FString &operator=(const char *strCharString);

    //! Assignment.
    inline FString &operator=(const FString &strOther);

    //! Check if string data is valid.
    BOOL IsValid(void) const;

    //! return length of the string.
    inline ULONG Length(void) const { return (ULONG)strlen(ConstData()); };

    //! Return length of the string without decorations.
    INDEX LengthNaked(void) const;

    //! Strip decorations from the string.
    FString Undecorated(void) const;

    //! Find index of a substring in a string (returns -1 if not found).
    INDEX FindSubstr(const FString &strSub);

    //! Replace a substring in a string.
    BOOL ReplaceSubstr(const FString &strSub, const FString &strNewSub);

    //! Check if has given prefix.
    BOOL HasPrefix( const FString &strPrefix) const;

    //! Remove given prefix string from this string.
    BOOL RemovePrefix( const FString &strPrefix);

    //! Trim the string to contain at most given number of characters.
    INDEX TrimLeft(  INDEX ctCharacters);

    //! Trim the string to contain at most given number of characters.
    INDEX TrimRight( INDEX ctCharacters);

    //! Trim the string from leading spaces.
    INDEX TrimSpacesLeft(void);

    //! Trim the string from trailing spaces.
    INDEX TrimSpacesRight(void);

    //! Calcuate hashing value for the string.
    ULONG GetHash(void) const;

    //! Retain only first line of the string.
    void OnlyFirstLine(void);

    //! Equality comparison.
    BOOL operator==(const FString &strOther) const;

    //! Equality comparison.
    BOOL operator==(const char *strOther) const;

    //! Equality comparison.
    CORE_API friend BOOL operator==(const char *strThis, const FString &strOther);

    //! Inequality comparison.
    BOOL operator!=(const FString &strOther) const;

    //! Inequality comparison.
    BOOL operator!=(const char *strOther) const;

    //! Inequality comparison.
    CORE_API friend BOOL operator!=(const char *strThis, const FString &strOther);

    //! Wild card comparison (other string may contain wildcards).
    BOOL Matches(const FString &strOther) const;

    //! Wild card comparison (other string may contain wildcards).
    BOOL Matches(const char *strOther) const;

    //! String concatenation.
    FString operator+(const FString &strSecond) const;

    //! String concatenation.
    FString &operator+=(const FString &strSecond);

    //! String concatenation.
    CORE_API friend FString operator+(const char *strFirst, const FString &strSecond);

    //! split string in two strings at specified position (char AT splitting position goes to str2).
    void Split( INDEX iPos, FString &str1, FString &str2);
    
    //! Insert char at position.
    void InsertChar( INDEX iPos, char cChr); 
    
    //! Delete char at position.
    void DeleteChar( INDEX iPos); 
    
    //! Throw exception.
    void Throw_t(void);
  
    //! Print formatted to a string
    INDEX PrintF(const char *strFormat, ...);

    //! Print formatted to a string
    INDEX VPrintF(const char *strFormat, va_list arg);

    //! Scan formatted from a string.
    INDEX ScanF(const char *strFormat, ...) const;
  
  // IO
  public:
    //! Read from stream.
    CORE_API friend CTStream &operator>>(CTStream &strmStream, FString &strString);
    
    //! Read from stream.
    void ReadFromText_t(CTStream &strmStream, const FString &strKeyword); // throw char *

    //! Write to stream.
    CORE_API friend CTStream &operator<<(CTStream &strmStream, const FString &strString);

    //! Load an entire text file into a string.
    void Load_t(const class FPath &fnmFile);  // throw char *

    //! Load an entire text file into a string.
    void LoadKeepCRLF_t(const class FPath &fnmFile);  // throw char *

    //! Load an entire text file into a string.
    void ReadUntilEOF_t(CTStream &strmStream);  // throw char *

    //! Save an entire string into a text file.
    void Save_t(const class FPath &fnmFile);  // throw char *

    //! Save an entire string into a text file.
    void SaveKeepCRLF_t(const class FPath &fnmFile);  // throw char *

    //! Load variable from file.
    void LoadVar(const FPath &fnmFile);

    //! Save variable to file.
    void SaveVar(const FPath &fnmFile);
    
    //! Write to stream.
    CORE_API friend FDataStream &operator<<(FDataStream &strm, const FString &str);

    //! Read from stream.
    CORE_API friend FDataStream &operator>>(FDataStream &strm, FString &str);
};

// general variable functions
CORE_API void LoadStringVar( const FPath &fnmVar, FString &strVar);
CORE_API void SaveStringVar( const FPath &fnmVar, FString &strVar);
CORE_API void LoadIntVar(    const FPath &fnmVar, INDEX &iVar);
CORE_API void SaveIntVar(    const FPath &fnmVar, INDEX &iVar);

#include <Core/Base/String.inl>

#endif  /* include-once check. */
