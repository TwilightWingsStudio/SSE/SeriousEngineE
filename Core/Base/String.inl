#ifndef SE_INCL_CTSTRING_INL
#define SE_INCL_CTSTRING_INL

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Memory.h>
#include <Core/Base/Assert.h>

//! Default constructor.
CORE_API FString::FString(void)
{
  str_String = StringDuplicate(""); // Allocate dummy string.
}

//! Copy constructor.
CORE_API FString::FString(const FString &strOriginal)
{
  ASSERT(strOriginal.IsValid());

  // make string duplicate
  str_String = StringDuplicate(strOriginal.str_String);
}

//! Constructor from character string.
CORE_API FString::FString(const char *strCharString)
{
  ASSERT(strCharString!=NULL);

  // make string duplicate
  str_String = StringDuplicate( strCharString);
}

//! Constructor with formatting.
CORE_API FString::FString(INDEX iDummy, const char *strFormat, ...)
{
  str_String = StringDuplicate("");
  va_list arg;
  va_start(arg, strFormat);
  VPrintF(strFormat, arg);
}

//! Destructor.
CORE_API FString::~FString()
{
  // check that it is valid
  ASSERT(IsValid());
  // free memory
  FreeMemory(str_String);
}

//! Clear the object.
CORE_API void FString::Clear(void)
{
  operator=("");
}

//! Conversion into character string.
CORE_API FString::operator const char*() const
{
  ASSERT(IsValid());

  return str_String;
}

//! Assignment.
CORE_API FString &FString::operator=(const char *strCharString)
{
  ASSERT(IsValid());
  ASSERT(strCharString!=NULL);

  /* The other string must be copied _before_ this memory is freed, since it could be same
     pointer!
   */
  // make a copy of character string
  char *strCopy = StringDuplicate(strCharString);
  // empty this string
  FreeMemory(str_String);
  // assign it the copy of the character string
  str_String = strCopy;

  return *this;
}

//! Assignment.
CORE_API FString &FString::operator=(const FString &strOther)
{
  ASSERT(IsValid());
  ASSERT(strOther.IsValid());

  /* The other string must be copied _before_ this memory is freed, since it could be same
     pointer!
   */
  // make a copy of character string
  char *strCopy = StringDuplicate(strOther.str_String);
  // empty this string
  FreeMemory(str_String);
  // assign it the copy of the character string
  str_String = strCopy;

  return *this;
}

#endif /* include-once check. */

