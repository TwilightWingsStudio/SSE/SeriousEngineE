/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <string.h>
#include <Core/IO/Stream.h>

FByteArray::FByteArray()
{
  _pData = nullptr;
  _dataSize = 0;
}

FByteArray::FByteArray(const FByteArray &other)
{
  _pData = nullptr;
  _dataSize = 0;

  Copy(other);
}

FByteArray::FByteArray(const char* pData, size_t size)
{
  _pData = nullptr;
  _dataSize = 0;

  if (pData == nullptr) {
    return;
  }

  // 0xFFFFFFFF or 0xFFFFFFFFFFFFFFFF
  if (size == size_t(-1)) {
    size = strlen(pData);
  }

  if (size > 0) {
    Resize(size);
    memcpy(_pData, pData, size);
  }
}

FByteArray::FByteArray(char b, size_t size)
{
  _pData = nullptr;
  _dataSize = 0;

  if (size > 0) {
    Resize(size);
    memset(_pData, b, size);
  }
}

FByteArray::~FByteArray()
{
  Clear();
}

void FByteArray::Copy(const FByteArray &other)
{
  Clear();

  if (!other.IsNull() && other.Size() > 0) {
    Resize(other.Size());
    memcpy(_pData, other.ConstData(), other.Size());
  }
}

const u8* FByteArray::ConstData() const
{
  return _pData;
}

u8* FByteArray::Data()
{
  return _pData;
}

FByteArray &FByteArray::Insert(size_t pos, const FByteArray &other)
{
  return Insert(pos, (const char *)other.ConstData(), other.Size());
}

FByteArray &FByteArray::Insert(size_t pos, const char *pData, size_t size)
{
  // If nothing to add...
  if (pData == NULL) {
    return *this;
  }

  // 0xFFFFFFFF or 0xFFFFFFFFFFFFFFFF
  if (size == size_t(-1)) {
    size = strlen(pData);
  }

  if (pos > Size()) {
    pos = Size();
  }

  // Allocate new buffer.
  size_t new_size = Size() + size;
  u8 *pNewData = new u8[new_size + 1]; // Size + Terminator
  pNewData[new_size] = '\0';

  // Copy left piece of source array.
  if (pos > 0) {
    memcpy(&pNewData[0], Data(), pos);
  }

  memcpy(&pNewData[pos], pData, size); // Copy data of another array.
  memcpy(&pNewData[pos + size], &Data()[pos], Size() - pos); // Copy right piece of source array.

  Clear(); // Remove old buffer.

  // Set new buffer.
  _dataSize = new_size;
  _pData = pNewData;

  return *this;
}

FByteArray &FByteArray::Insert(size_t pos, size_t count, char ch)
{
  // If nothing to add...
  if (count == 0) {
    return *this;
  }

  if (pos > Size()) {
    pos = Size();
  }

  // Allocate new buffer.
  size_t new_size = Size() + count;
  u8 *pNewData = new u8[new_size + 1]; // Size + Terminator
  pNewData[new_size] = '\0';

  // Copy left piece of source array.
  if (pos > 0) {
    memcpy(&pNewData[0], Data(), pos);
  }

  memset(&pNewData[pos], ch,  count); // Fill with chars.
  memcpy(&pNewData[pos + count], &Data()[pos], Size() - pos); // Copy right piece of source array.

  Clear(); // Remove old buffer.

  // Set new buffer.
  _dataSize = new_size;
  _pData = pNewData;

  return *this;
}

FByteArray &FByteArray::Append(const FByteArray &other)
{
  return Insert(Size(), other);
}

FByteArray &FByteArray::Append(const char *pData, size_t size)
{
  return Insert(Size(), pData, size);
}

FByteArray &FByteArray::Append(size_t count, char ch)
{
  return Insert(Size(), count, ch);
}

FByteArray &FByteArray::Prepend(const FByteArray &other)
{
  return Insert(0, other);
}

FByteArray &FByteArray::Prepend(const char *pData, size_t size)
{
  return Insert(0, pData, size);
}

FByteArray &FByteArray::Prepend(size_t count, char ch)
{
  return Insert(0, count, ch);
}

FByteArray &FByteArray::Remove(size_t pos, size_t count)
{
  if (IsNull() || count == 0 || pos >= Size()) {
    return *this;
  }

  size_t max_removal = Size() - pos;
  if (count > max_removal) {
    count = max_removal;
  }

  size_t new_size = Size() - count;
  size_t right_size = Size() - count - pos;

  if (right_size > 0) {
    memcpy(&Data()[pos], &Data()[pos + count], right_size);
  }

  Resize(new_size);
  return *this;
}

void FByteArray::Resize(size_t new_size)
{
  if (new_size == 0) {
    Clear();
    return;
  }
  
  u8 *pNewData = new u8[new_size + 1]; // Size + Terminator
  pNewData[new_size] = '\0';

  // If we have data then copy it to a new buffer!
  if (Data()) {
    // If larger...
    if (new_size > Size()) {
      if (_dataSize > 0) {
        memcpy(&pNewData[0], _pData, _dataSize);
      }
    } else {
      if (new_size > 0) {
        memcpy(&pNewData[0], _pData, new_size);
      }
    }

    Clear(); // Remove old data.
  }

  // Udpate buffer.
  _dataSize = new_size;
  _pData = pNewData;
}

void FByteArray::Chop(size_t count)
{
  if (count > Size()) {
    count = Size();
  }

  Resize(Size() - count);
}

void FByteArray::Clear()
{
  if (_pData) {
    _dataSize = 0;
    delete[] _pData;
    _pData = nullptr;
  }
}

u8 FByteArray::At(size_t i) const
{
  ASSERT(_pData != nullptr);

  if (i >= _dataSize) {
    ASSERTALWAYS("Byte array index is out of bounds!");
    return 0;
  }

  // NOTE: Here should NOT be null check! If user did mistake then its his fail.
  return _pData[i];
}

FByteArray &FByteArray::Fill(u8 b, size_t size)
{
  if (Size() == 0 || IsNull()) {
    return *this;
  }

  // Clamp to allocated data size.
  if (size > Size()) {
    size = Size();
  }

  memset(_pData, b, size);

  return *this;
}

size_t FByteArray::Size() const
{
  return _dataSize;
}

bool FByteArray::IsNull() const
{
  return (_pData == nullptr);
}

FByteArray &FByteArray::operator=(const FByteArray &other)
{
  Clear();

  if (other.Size() > 0) {
    Resize(other.Size());
    memcpy(Data(), other.ConstData(), other.Size());
  }

  return *this;
}

bool FByteArray::operator==(const FByteArray &baOther) const
{
  // Size is different. 
  if (Size() != baOther.Size()) {
    return false;
  }
  
  // Same length and both are empty.
  if (IsNull() && baOther.IsNull()) {
    return true;
  }

  return memcmp(ConstData(), baOther.ConstData(), Size()) == 0;
}

bool FByteArray::operator==(const char *strOther) const
{
  // String is null and empty array.
  if (IsNull() && strOther == nullptr) {
    return true;
  }

  // String is null but array is not empty.
  if (strOther == nullptr) {
    return false;
  }

  size_t len = strlen(strOther);

  // Size is different. 
  if (Size() != len) {
    return false;
  }

  return memcmp(ConstData(), strOther, Size()) == 0;
}

bool FByteArray::operator!=(const FByteArray &strOther) const
{
  return !(*this == strOther);
}

bool FByteArray::operator!=(const char *strOther) const
{
  return !(*this == strOther);
}

void FByteArray::Swap(FByteArray &other)
{
  size_t temp_size = Size();
  u8 *pTempData = Data();
  
  _dataSize = other.Size();
  _pData = other.Data();
  
  other._dataSize = temp_size;
  other._pData = pTempData;
}

bool FByteArray::StartsWith(char ch) const
{
  if (Size() == 0) {
    return false;
  }

  return Front() == ch;
}

bool FByteArray::StartsWith(const FByteArray &other) const
{
  if (other.IsNull() || other.Size() > Size()) {
    return false;
  }

  return memcmp(ConstData(), other.ConstData(), other.Size()) == 0;
}

bool FByteArray::StartsWith(const char *pStr) const
{
  if (pStr == nullptr) {
    return false;
  }

  size_t len = strlen(pStr);

  if (len > Size()) {
    return false;
  }

  return memcmp(ConstData(), pStr, len) == 0;
}

bool FByteArray::EndsWith(char ch) const
{
  if (Size() == 0) {
    return false;
  }

  return Back() == ch;
}

bool FByteArray::EndsWith(const FByteArray &other) const
{
  if (other.IsNull() || other.Size() > Size()) {
    return false;
  }

  size_t pos = Size() - other.Size();
  return memcmp(&ConstData()[pos], other.ConstData(), other.Size()) == 0;
}

bool FByteArray::EndsWith(const char *pStr) const
{
  if (pStr == nullptr) {
    return false;
  }

  size_t len = strlen(pStr);

  if (len > Size()) {
    return false;
  }

  size_t pos = Size() - len;
  return memcmp(&ConstData()[pos], pStr, len) == 0;
}

size_t FByteArray::IndexOf(char ch, size_t from)
{
  if (IsNull() || from >= Size()) {
    return size_t(-1);
  }
  
  for (size_t i = from; i < Size(); i++)
  {
    if (ConstData()[i] == ch) {
      return i;
    }
  }
  
  return size_t(-1);
}

size_t FByteArray::IndexOf(const FByteArray &other, size_t from)
{
  if (IsNull() || other.IsNull() || from >= Size() || other.Size() > Size()) {
    return size_t(-1);
  }

  for (size_t i = from; i < Size(); i++)
  {
    size_t left = Size() - i;

    if (left < other.Size()) {
      return size_t(-1);
    }

    if (memcmp(&ConstData()[i], other.ConstData(), other.Size()) == 0) {
      return i;
    }
  }

  return size_t(-1);
}

size_t FByteArray::IndexOf(const char *pStr, size_t from)
{
  if (IsNull() || pStr == NULL || from >= Size()) {
    return size_t(-1);
  }

  size_t len = strlen(pStr);

  if (len > Size()) {
    return size_t(-1);
  }

  for (size_t i = from; i < Size(); i++)
  {
    size_t left = Size() - i;

    if (left < len) {
      return size_t(-1);
    }

    if (memcmp(&ConstData()[i], pStr, len) == 0) {
      return i;
    }
  }

  return size_t(-1);
}

bool FByteArray::Contains(char ch)
{
  return IndexOf(ch, 0) != size_t(-1);
}

bool FByteArray::Contains(const FByteArray &other)
{
  return IndexOf(other, 0) != size_t(-1);
}

bool FByteArray::Contains(const char *pStr)
{
  return IndexOf(pStr, 0) != size_t(-1);
}

FByteArray FByteArray::ToHex(char separator) const
{
  static const char *digits = "0123456789ABCDEF";
  
  size_t len = separator == '\0' ? Size() * 2 : (Size() * 3 - 1);

  FByteArray result('0', len);

  size_t j = 0;

  for (size_t i = 0; i < Size(); i++)
  {
    result.Data()[j++] = digits[(ConstData()[i] & 0xF0) >> 4];
    result.Data()[j++] = digits[ConstData()[i] & 0x0F];
    
    if (separator != '\0' && (i + 1) < Size()) {
      result.Data()[j++] = separator;
    }
  }

  return result;
}

CTStream &operator<<(CTStream &strmStream, const FByteArray &baData)
{
  // calculate size
  ULONG ulDataLength = baData.Size();
  strmStream << ulDataLength;

  if (ulDataLength > 0) {
    strmStream.Write_t(baData.ConstData(), ulDataLength);
  }

  return strmStream;
}

CTStream &operator>>(CTStream &strmStream, FByteArray &baData)
{
  ULONG ulDataLength;
  strmStream >> ulDataLength;

  baData.Clear();
  baData.Resize(ulDataLength);

  if (ulDataLength > 0) {
    strmStream.Read_t(baData.Data(), ulDataLength);
  }

  return strmStream;
}

#include <Core/IO/DataStream.h>

FDataStream &operator<<(FDataStream &strmStream, const FByteArray &baData)
{
  const u32 ulDataLength = baData.Size();
  strmStream << ulDataLength;
  
  if (ulDataLength > 0 && strmStream.Write(baData.ConstData(), ulDataLength) != ulDataLength) {
    strmStream.SetStatus(FDataStream::STATUS_WRITEFAILED);
  }
  
  return strmStream;
}

FDataStream &operator>>(FDataStream &strmStream, FByteArray &baData)
{
  u32 ulDataLength;
  strmStream >> ulDataLength; // read length

  baData.Clear();
  baData.Resize(ulDataLength);

  // if the string is not empty then read string
  if (ulDataLength > 0 && strmStream.Read(baData.Data(), ulDataLength) != ulDataLength) {
    memset(baData.Data(), 0, ulDataLength);
  }
  
  return strmStream;
}