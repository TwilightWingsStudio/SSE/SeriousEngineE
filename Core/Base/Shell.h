/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_SHELL_H
#define SE_INCL_SHELL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Threading/Synchronization.h>

#include <Core/Templates/DynamicArray.h>
#include <Core/Base/Shell_internal.h>

#define NEXTARGUMENT(type) ( *((type*&)pArgs)++ )

//! Object that takes care of shell functions, variables, macros etc.
class CORE_API CShell
{
  public:
    CSyncMutex sh_csShell; // critical section for access to shell data
    TDynamicArray<CShellSymbol> sh_assSymbols;  // all defined symbols

  public:
    //! Constructor.
    CShell(void);
    
    //! Destructor.
    ~CShell(void);

    //! Initialize the shell.
    void Initialize(void);

    //! Report error in shell script processing.
    void ErrorF(const char *strFormat, ...);

    //! Declare a symbol in the shell.
    void DeclareSymbol(const FString &strDeclaration, void *pvValue);

    //! Execute command(s).
    void Execute(const FString &strCommands);

    //! Save shell commands to restore persistent symbols to a script file
    void StorePersistentSymbols(const FPath &fnScript);

    //! Get a shell symbol by its name.
    CShellSymbol *GetSymbol(const FString &strName, BOOL bDeclaredOnly);

    //! Get symbol as FLOAT.
    FLOAT GetFLOAT(const FString &strName);

    //! Set symbol as FLOAT.
    void SetFLOAT(const FString &strName, FLOAT fValue);

    //! Get symbol as INDEX.
    INDEX GetINDEX(const FString &strName);

    //! Set symbol as INDEX.
    void SetINDEX(const FString &strName, INDEX iValue);
    
    //! Get symbol as String.
    FString GetString(const FString &strName);

    //! Set symbol as String.
    void SetString(const FString &strName, const FString &strValue);
    
    //! Get symbol as 64-bit integer.
    s64 GetInt64(const FString &strName);

    //! Set symbol as 64-bit integer.
    void SetInt64(const FString &strName, s64 iValue);

    //! Get symbol as value.
    FString GetValue(const FString &strName);

    //! Set symbol as value.
    void SetValue(const FString &strName, const FString &strValue);
};

// pointer to global shell object
CORE_API extern CShell *_pShell;

CORE_API void ReportGlobalMemoryStatus(void);

#endif  /* include-once check. */
