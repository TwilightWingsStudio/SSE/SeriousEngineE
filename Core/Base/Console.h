/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CONSOLE_H
#define SE_INCL_CONSOLE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

extern CORE_API BOOL con_bCapture;
extern CORE_API BOOL con_bNoWarnings;
extern CORE_API FString con_strCapture;

enum LogMessageType
{
  //! For cases when we have function with default arguments. This type should never print anything!
  LMT_DISABLED = 0,

  //! Debug output.
  LMT_DEBUG,

  //! An information message.
  LMT_INFO,

  //! Use it when something important or not so good happened.
  LMT_WARNING,

  //! Use it when something bad happened.
  LMT_ERROR,

  //! Use it when fatal error occured. Should be unhideable.
  LMT_FATAL_ERROR,

  //! Use this dummy type for arrays or cycles.
  LMT_MAX
};

//! Get prefix string of a log message from its type.
CORE_API const char *GetLogLevelPrefix(LogMessageType eType);

//! Get color of a log message type.
CORE_API COLOR GetLogLevelColor(LogMessageType eType);

//! Print message marked as Debug.
CORE_API extern void CDebugF(const char *strFormat, ...);

//! Print message marked as Info.
CORE_API extern void CInfoF(const char *strFormat, ...);

//! Print message marked as Warning.
CORE_API extern void CWarningF(const char *strFormat, ...);

//! Print message marked as Rrror.
CORE_API extern void CErrorF(const char *strFormat, ...);

//! Print message marked as Fatal Error.
CORE_API extern void CFatalErrorF(const char *strFormat, ...);

//! Print message of specific type taking args list.
CORE_API extern void CPrintFV(LogMessageType eType, const char *strFormat, va_list arg);

//! Add a string of text to console
CORE_API void CPutString(LogMessageType eType, const char *strString);

//! Get number of lines newer than given time
CORE_API INDEX CON_NumberOfLinesAfter(TIME tmLast);

//! Get one of last lines
CORE_API FString CON_GetLastLine(INDEX iLine);

//! Discard timing info for last lines
CORE_API void CON_DiscardLastLineTimes(void);

//! Get current console buffer.
CORE_API const char *CON_GetBuffer(void);

//! Get size of the console buffer.
CORE_API INDEX CON_GetBufferSize(void);

#endif  /* include-once check. */
