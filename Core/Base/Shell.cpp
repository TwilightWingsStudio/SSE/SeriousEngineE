/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Shell.h>
#include <Core/Base/Shell_internal.h>
#include <Core/Base/ParsingSymbols.h>

#include <Core/Templates/DynamicStackArray.h>
#include <Core/Base/Console.h>
#include <Core/IO/Stream.h>

#include <Core/Templates/AllocationArray.cpp>
#include <Core/Templates/DynamicArray.cpp>
#include <Core/Templates/DynamicStackArray.cpp>

template class TDynamicArray<CShellSymbol>;

// shell type used for undeclared symbols
extern INDEX _shell_istUndeclared = -1;

// pointer to global shell object
CShell *_pShell = NULL;
void *_pvNextToDeclare = NULL; // != NULL if declaring external symbol defined in exe code

// define console variable for number of last console lines
extern INDEX con_iLastLines    = 5;

extern void yy_switch_to_buffer(YY_BUFFER_STATE);

// declarations for recursive shell script parsing
struct BufferStackEntry
{
  YY_BUFFER_STATE bse_bs;
  const char *bse_strName;
  const char *bse_strContents;
  int bse_iLineCt;
  BOOL bse_bParserEnd;
};

static BufferStackEntry _abseBufferStack[SHELL_MAX_INCLUDE_LEVEL];
static int _ibsBufferStackTop = -1;

BOOL _bExecNextBlock = 1;

void ShellPushBuffer(const char *strName, const char *strBuffer, BOOL bParserEnd)
{
  _ibsBufferStackTop++;

  _abseBufferStack[_ibsBufferStackTop].bse_strContents = strdup(strBuffer);
  _abseBufferStack[_ibsBufferStackTop].bse_strName = strdup(strName);
  _abseBufferStack[_ibsBufferStackTop].bse_iLineCt = 1;
  _abseBufferStack[_ibsBufferStackTop].bse_bParserEnd = bParserEnd;

  _abseBufferStack[_ibsBufferStackTop].bse_bs = yy_scan_string(strBuffer);

  yy_switch_to_buffer(_abseBufferStack[_ibsBufferStackTop].bse_bs);
}

BOOL ShellPopBuffer(void)
{
  yy_delete_buffer( _abseBufferStack[_ibsBufferStackTop].bse_bs);
  free((void*)_abseBufferStack[_ibsBufferStackTop].bse_strName);
  free((void*)_abseBufferStack[_ibsBufferStackTop].bse_strContents);
  BOOL bParserEnd = _abseBufferStack[_ibsBufferStackTop].bse_bParserEnd;

  _ibsBufferStackTop--;

  if (_ibsBufferStackTop >= 0) {
    yy_switch_to_buffer(_abseBufferStack[_ibsBufferStackTop].bse_bs);
  }

  return bParserEnd;
}

const char *ShellGetBufferName(void)
{
  return _abseBufferStack[_ibsBufferStackTop].bse_strName;
}

int ShellGetBufferLineNumber(void)
{
  return _abseBufferStack[_ibsBufferStackTop].bse_iLineCt;
}

int ShellGetBufferStackDepth(void)
{
  return _ibsBufferStackTop;
}

const char *ShellGetBufferContents(void)
{
  return _abseBufferStack[_ibsBufferStackTop].bse_strContents;
}

void ShellCountOneLine(void)
{
  _abseBufferStack[_ibsBufferStackTop].bse_iLineCt++;
}

// temporary values for parsing
TDynamicStackArray<FString> _shell_astrTempStrings;
// values for extern declarations
TDynamicStackArray<FString> _shell_astrExtStrings;
TDynamicStackArray<FLOAT> _shell_afExtFloats;

static const char *strCommandLine = "";

CORE_API extern FLOAT tmp_af[10] = { 0 };
CORE_API extern INDEX tmp_ai[10] = { 0 };
CORE_API extern INDEX tmp_fAdd   = 0.0f;
CORE_API extern INDEX tmp_i      = 0;

void CShellSymbol::Clear(void)
{
  ss_istType = -1;
  ss_strName.Clear();
  ss_ulFlags = 0;
};

BOOL CShellSymbol::IsDeclared(void)
{
  return ss_istType>=0 && ss_istType!=_shell_istUndeclared;
}

FString CShellSymbol::GetCompletionString(void) const
{
  // get its type
  ShellType &st = _shell_ast[ss_istType];

  // get its name
  if (st.st_sttType == STT_FUNCTION) {
    return ss_strName + "()";
  } else if (st.st_sttType == STT_ARRAY) {
    return ss_strName + "[]";
  } else {
    return ss_strName;
  }
}

// Constructor.
CShell::CShell(void)
{
  // allocate undefined symbol
  _shell_istUndeclared = _shell_ast.Allocate();
};

CShell::~CShell(void)
{
  _shell_astrExtStrings.Clear();
  _shell_afExtFloats.Clear();
};

static const INDEX _bTRUE  = TRUE;
static const INDEX _bFALSE = FALSE;

FString ScriptEsc(const FString &str)
{
  FString strResult = "";

  const char *pchSrc = str.ConstData();
  char buf[2];
  buf[1] = 0;

  while (*pchSrc != 0)
  {
    switch (*pchSrc)
    {
      case  10: strResult += "\\n"; break;
      case  13: strResult += "\\r"; break;
      case '\\': strResult += "\\\\"; break;
      case '"': strResult += "\\\""; break;
      default: buf[0] = *pchSrc; strResult += buf; break;
    }

    pchSrc++;
  }

  return strResult;
}

#pragma inline_depth(0)
void MakeAccessViolation(void* pArgs)
{
  INDEX bDont = NEXTARGUMENT(INDEX);

  if (bDont) return;
  char *p = NULL;
  *p = 1;
}

extern int _a=123;
void MakeStackOverflow(void* pArgs)
{
  INDEX bDont = NEXTARGUMENT(INDEX);
  if (bDont) return;
  int a[1000];
  a[999] = _a;
  MakeStackOverflow(0);
  _a=a[999];
}

void MakeFatalError(void* pArgs)
{
  INDEX bDont = NEXTARGUMENT(INDEX);

  if (bDont) return;
  FatalError( "MakeFatalError()");
}

extern void ReportGlobalMemoryStatus(void)
{
  #pragma message("Tweak this to work on x64!")
  #if defined _MSC_VER && !defined _WIN64
  CInfoF(TRANS("Global memory status...\n"));

  MEMORYSTATUS ms;
  GlobalMemoryStatus(&ms);

#define MB (1024*1024)
  CInfoF(TRANS("  Physical memory used: %4d/%4dMB\n"), (ms.dwTotalPhys    -ms.dwAvailPhys    )/MB, ms.dwTotalPhys    /MB);
  CInfoF(TRANS("  Page file used:       %4d/%4dMB\n"), (ms.dwTotalPageFile-ms.dwAvailPageFile)/MB, ms.dwTotalPageFile/MB);
  CInfoF(TRANS("  Virtual memory used:  %4d/%4dMB\n"), (ms.dwTotalVirtual -ms.dwAvailVirtual )/MB, ms.dwTotalVirtual /MB);
  CInfoF(TRANS("  Memory load: %3d%%\n"), ms.dwMemoryLoad);

  DWORD dwMin;
  DWORD dwMax;
  GetProcessWorkingSetSize(GetCurrentProcess(), &dwMin, &dwMax);
  CInfoF(TRANS("  Process working set: %dMB-%dMB\n\n"), dwMin/(1024*1024), dwMax/(1024*1024));
  #endif
}

static void MemoryInfo(void)
{
  #pragma message("Tweak this to work on x64!")
#if defined _MSC_VER && !defined _WIN64
  ReportGlobalMemoryStatus();

  _HEAPINFO hinfo;
  int heapstatus;
  hinfo._pentry = NULL;
  SLONG slTotalUsed = 0;
  SLONG slTotalFree = 0;
  INDEX ctUsed = 0;
  INDEX ctFree = 0;

  CInfoF( "Walking heap...\n");
  while (( heapstatus = _heapwalk( &hinfo ) ) == _HEAPOK )
  {
    if (hinfo._useflag == _USEDENTRY ) {
      slTotalUsed += hinfo._size;
      ctUsed++;
    } else {
      slTotalFree += hinfo._size;
      ctFree++;
    }
  }

  switch (heapstatus )
  {
     case _HEAPEMPTY:     CInfoF("Heap empty?!?\n");                break;
     case _HEAPEND:       CInfoF("Heap ok.\n");                     break;
     case _HEAPBADPTR:    CErrorF("ERROR - bad pointer to heap\n");  break;
     case _HEAPBADBEGIN:  CErrorF("ERROR - bad start of heap\n");    break;
     case _HEAPBADNODE:   CErrorF("ERROR - bad node in heap\n");     break;
  }

  CInfoF("Total used: %d bytes (%.2f MB) in %d blocks\n", slTotalUsed, slTotalUsed / 1024.0f / 1024.0f, ctUsed);
  CInfoF("Total free: %d bytes (%.2f MB) in %d blocks\n", slTotalFree, slTotalFree / 1024.0f / 1024.0f, ctFree);
  #endif
}

// get help for a shell symbol
extern FString GetShellSymbolHelp_t(const FString &strSymbol)
{
  FString strPattern = strSymbol + "*";
  // open the symbol help file
  CTFileStream strm;
  strm.Open_t(FString("Help\\ShellSymbols.txt"));

  // while not at the end of file
  while (!strm.AtEOF())
  {
    // read the symbol name and its help
    FString strSymbolInFile;
    strm.GetLine_t(strSymbolInFile, ':');
    strSymbolInFile.TrimSpacesLeft();
    strSymbolInFile.TrimSpacesRight();
    FString strHelpInFile;
    strm.GetLine_t(strHelpInFile, '$');
    strHelpInFile.TrimSpacesLeft();
    strHelpInFile.TrimSpacesRight();

    // if that is the one then print the help
    if (strSymbolInFile.Matches(strPattern)) {
      return strHelpInFile;
    }
  }

  return "";
}

// check if there is help for a shell symbol
extern BOOL CheckShellSymbolHelp(const FString &strSymbol)
{
  try {
    return GetShellSymbolHelp_t(strSymbol) != "";
  } catch(char *strError) {
    (void)strError;
    return FALSE;
  }
}

// print help for a shell symbol
extern void PrintShellSymbolHelp(const FString &strSymbol)
{
  // try to
  try {
    FString strHelp = GetShellSymbolHelp_t(strSymbol);

    if (strHelp != "") {
      CInfoF("%s\n", strHelp);
    } else {
      CInfoF(TRANS("No help found for '%s'.\n"), strSymbol);
    }

  // if failed
  } catch(char *strError) {
    // just print the error
    CWarningF(TRANS("Cannot print help for '%s': %s\n"), strSymbol, strError);
  }
}

extern void ListSymbolsByPattern(FString strPattern)
{
  // synchronize access to global shell
  CSyncLock slShell(&_pShell->sh_csShell, TRUE);

  // for each of symbols in the shell
  FOREACHINDYNAMICARRAY(_pShell->sh_assSymbols, CShellSymbol, itss)
  {
    CShellSymbol &ss = *itss;

    // if it is not visible to user, or not matching then skip it
    if (!(ss.ss_ulFlags&SSF_USER) || !ss.ss_strName.Matches(strPattern)) {
      continue;
    }

    // get its type
    ShellType &st = _shell_ast[ss.ss_istType];

    if (ss.ss_ulFlags & SSF_CONSTANT) {
      CInfoF("const ");
    }

    if (ss.ss_ulFlags & SSF_PERSISTENT) {
      CInfoF("persistent ");
    }

    // print its declaration to the console
    if (st.st_sttType == STT_FUNCTION) {
      CInfoF("void %s(void)", ss.ss_strName);

    } else if (st.st_sttType == STT_STRING) {
      CInfoF("FString %s = \"%s\"", ss.ss_strName, ss.AsString());
    } else if (st.st_sttType == STT_FLOAT) {
      CInfoF("FLOAT %s = %g", ss.ss_strName, ss.AsFloat());
    } else if (st.st_sttType == STT_INDEX) {
      CInfoF("INDEX %s = %d (0x%08x)", ss.ss_strName, ss.AsIndex(), ss.AsIndex());
    } else if (st.st_sttType == STT_ARRAY) {
      // get base type
      ShellType &stBase = _shell_ast[st.st_istBaseType];
      if (stBase.st_sttType == STT_FLOAT) {
        CInfoF("FLOAT %s[%d]", ss.ss_strName, st.st_ctArraySize);
      } else if (stBase.st_sttType == STT_INDEX) {
        CInfoF("INDEX %s[%d]", ss.ss_strName, st.st_ctArraySize);
      } else if (stBase.st_sttType == STT_STRING) {
        CInfoF("FString %s[%d]", ss.ss_strName, st.st_ctArraySize);
      } else {
        ASSERT(FALSE);
      }
    } else {
      ASSERT(FALSE);
    }

    if (!CheckShellSymbolHelp(ss.ss_strName)) {
      CInfoF( TRANS(" help N/A"));
    }

    CInfoF("\n");
  }
}

// Print a list of all symbols in global shell to console.
static void ListSymbols(void)
{
  // print header
  CInfoF( TRANS("Useful symbols:\n"));

  // list all symbols
  ListSymbolsByPattern("*");
}

// output any string to console
void Echo(void* pArgs)
{
  FString str = *NEXTARGUMENT(FString*);
  CInfoF("%s", str);
}

FString UndecorateString(void* pArgs)
{
  FString strString = *NEXTARGUMENT(FString*);
  return strString.Undecorated();
}

BOOL MatchStrings(void* pArgs)
{
  FString strString = *NEXTARGUMENT(FString*);
  FString strPattern = *NEXTARGUMENT(FString*);
  return strString.Matches(strPattern);
}

FString MyLoadString(void* pArgs)
{
  FString strFileName = *NEXTARGUMENT(FString*);

  try {
    FString strString;
    strString.Load_t(strFileName);
    return strString;
  } catch (char *strError) {
    (void)strError;
    return "";
  }
}

void MySaveString(void* pArgs)
{
  FString strFileName = *NEXTARGUMENT(FString*);
  FString strString = *NEXTARGUMENT(FString*);

  try {
    strString.Save_t(strFileName);
  } catch (char *strError) {
    (void)strError;
  }
}

// load command batch files
void LoadCommands(void)
{
  // list all command files
  TDynamicStackArray<FPath> afnmCmds;
  MakeDirList(afnmCmds, FString("Scripts\\Commands\\"), "*.ini", DLI_RECURSIVE);

  // for each file
  for (INDEX i = 0; i < afnmCmds.Count(); i++)
  {
    FPath &fnm = afnmCmds[i];

    // load the file
    FString strCmd;

    try {
      strCmd.Load_t(fnm);
    } catch (char *strError) {
      CErrorF("%s\n", strError);
      continue;
    }

    FString strName = fnm.FileName();

    // declare it
    extern void Declaration(
      ULONG ulQualifiers, INDEX istType, CShellSymbol &ssNew,
      INDEX (*pPreFunc)(INDEX), void (*pPostFunc)(INDEX));

    INDEX iType = ShellTypeNewString();
    CShellSymbol &ssNew = *_pShell->GetSymbol(strName, FALSE);
    Declaration(SSF_EXTERNAL | SSF_USER, iType, ssNew, NULL, NULL);
    ShellTypeDelete(iType);

    // get symbol type
    ShellTypeType stt = _shell_ast[ssNew.ss_istType].st_sttType;

    // if the symbol is ok then assign value
    if (stt == STT_STRING && !(ssNew.ss_ulFlags & SSF_CONSTANT)) {
      *(FString*)ssNew.ss_pvValue = "!command "+strCmd;
    } else {
      _pShell->ErrorF("Symbol '%s' is not suitable to be a command", ssNew.ss_strName);
    }
  }
}

static FString ToUpper(FString &strResult)
{
  char *pch = strResult.Data();
  const INDEX ctLen = strResult.Length();

  for (INDEX i = 0; i < ctLen; i++) {
    pch[i] = toupper(pch[i]);
  }

  return strResult;
}

static FString ToUpperCfunc(void* pArgs)
{
  FString strResult = *NEXTARGUMENT(FString*);

  return ToUpper(strResult);
}

static FString ToLower(FString &strResult)
{
  char *pch = strResult.Data();
  const INDEX ctLen = strResult.Length();

  for (INDEX i = 0; i < ctLen; i++) {
    pch[i] = tolower(pch[i]);
  }

  return strResult;
}

static FString ToLowerCfunc(void* pArgs)
{
  FString strResult = *NEXTARGUMENT(FString*);

  return ToLower(strResult);
}

static FString RemoveSubstring(FString &strFull, FString &strSub)
{
  FString strFullL = ToLower(strFull);
  FString strSubL = ToLower(strSub);

  const char *pchFullL = strFullL;
  const char *pchSubL = strSubL;
  const char *pchFound = strstr(pchFullL, pchSubL);

  if (pchFound == NULL || strSub.Length() == 0) {
    return strFull;
  }

  INDEX iOffset = pchFound-pchFullL;
  INDEX iLenFull = strFull.Length();
  INDEX iLenSub = strSub.Length();

  FString strLeft = strFull;
  strLeft.TrimRight(iOffset);
  FString strRight = strFull;
  strRight.TrimLeft(iLenFull-iOffset-iLenSub);
  return strLeft+strRight;
}

static FString RemoveSubstringCfunc(void* pArgs)
{
  FString strFull = *NEXTARGUMENT(FString*);
  FString strSub = *NEXTARGUMENT(FString*);
  return RemoveSubstring(strFull, strSub);
}

// Initialize the shell.
void CShell::Initialize(void)
{
  sh_csShell.cs_iIndex = -1;

  // synchronize access to shell
  CSyncLock slShell(&sh_csShell, TRUE);

  // add built in commands and constants
  DeclareSymbol("const INDEX TRUE;",  (void *)&_bTRUE);
  DeclareSymbol("const INDEX FALSE;", (void *)&_bFALSE);
  DeclareSymbol("const INDEX ON;",    (void *)&_bTRUE);
  DeclareSymbol("const INDEX OFF;",   (void *)&_bFALSE);
  DeclareSymbol("const INDEX YES;",   (void *)&_bTRUE);
  DeclareSymbol("const INDEX NO;",    (void *)&_bFALSE);

  DeclareSymbol("user void LoadCommands(void);", (void *)&LoadCommands);
  DeclareSymbol("user void ListSymbols(void);", (void *)&ListSymbols);
  DeclareSymbol("user void MemoryInfo(void);", (void *)&MemoryInfo);
  DeclareSymbol("user void MakeAccessViolation(INDEX);", (void *)&MakeAccessViolation);
  DeclareSymbol("user void MakeStackOverflow(INDEX);", (void *)&MakeStackOverflow);
  DeclareSymbol("user void MakeFatalError(INDEX);", (void *)&MakeFatalError);
  DeclareSymbol("persistent user INDEX con_iLastLines;", (void *)&con_iLastLines);
  DeclareSymbol("persistent user FLOAT tmp_af[10];", (void *)&tmp_af);
  DeclareSymbol("persistent user INDEX tmp_ai[10];", (void *)&tmp_ai);
  DeclareSymbol("persistent user INDEX tmp_i;", (void *)&tmp_i);
  DeclareSymbol("persistent user FLOAT tmp_fAdd;", (void *)&tmp_fAdd);

  DeclareSymbol("user void Echo(FString);", (void *)&Echo);
  DeclareSymbol("user FString UndecorateString(FString);", (void *)&UndecorateString);
  DeclareSymbol("user INDEX Matches(FString, FString);", (void *)&MatchStrings);
  DeclareSymbol("user FString LoadString(FString);", (void *)&MyLoadString);
  DeclareSymbol("user void SaveString(FString, FString);", (void *)&MySaveString);
  DeclareSymbol("user FString RemoveSubstring(FString, FString);", (void *)&RemoveSubstringCfunc);
  DeclareSymbol("user FString ToUpper(FString);", (void *)&ToUpperCfunc);
  DeclareSymbol("user FString ToLower(FString);", (void *)&ToLowerCfunc);
}

static BOOL _iParsing = 0;

// Declare a symbol in the shell.
void CShell::DeclareSymbol(const FString &strDeclaration, void *pvValue)
{
  // synchronize access to shell
  CSyncLock slShell(&sh_csShell, TRUE);

  _pvNextToDeclare = pvValue;

  _iParsing++;

  // parse the string
  const BOOL old_bExecNextBlock = _bExecNextBlock;
  _bExecNextBlock = 1;

  ShellPushBuffer("<declaration>", strDeclaration, TRUE);
  yyparse();
//  ShellPopBuffer();

  _bExecNextBlock = old_bExecNextBlock;

  _iParsing--;

  if (_iParsing <= 0) {
    _shell_astrTempStrings.PopAll();
  }

  // don't use that value for parsing any more
  _pvNextToDeclare = NULL;
};

// Execute command(s).
void CShell::Execute(const FString &strCommands)
{
  // synchronize access to shell
  CSyncLock slShell(&sh_csShell, TRUE);

//  ASSERT(_iParsing==0);
  _iParsing++;

  // parse the string
  const BOOL old_bExecNextBlock = _bExecNextBlock;
  _bExecNextBlock = 1;

  ShellPushBuffer("<command>", strCommands, TRUE);
  yyparse();
  //ShellPopBuffer();

  _bExecNextBlock = old_bExecNextBlock;

  _iParsing--;

  if (_iParsing <= 0) {
    _shell_astrTempStrings.PopAll();
  }
};

// Get a shell symbol by its name.
CShellSymbol *CShell::GetSymbol(const FString &strName, BOOL bDeclaredOnly)
{
  // synchronize access to shell
  CSyncLock slShell(&sh_csShell, TRUE);

  // for each of symbols in the shell
  FOREACHINDYNAMICARRAY(sh_assSymbols, CShellSymbol, itss)
  {
    // if it is the right one then return it
    if (itss->ss_strName==strName) {
      return itss;
    }
  }
  // if none is found...

  // if only declared symbols are allowed
  if (bDeclaredOnly) {
    // return nothing
    return NULL;

  // if undeclared symbols are allowed
  } else {
    // create a new one with that name and undefined type
    CShellSymbol &ssNew = *sh_assSymbols.New(1);
    ssNew.ss_strName = strName;
    ssNew.ss_istType = _shell_istUndeclared;
    ssNew.ss_pvValue = NULL;
    ssNew.ss_ulFlags = 0;
    ssNew.ss_pPreFunc = NULL;
    ssNew.ss_pPostFunc = NULL;
    return &ssNew;
  }
};

FLOAT CShell::GetFLOAT(const FString &strName)
{
  // get the symbol
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_FLOAT) {
    ASSERT(FALSE);
    return -666.0f;
  }

  // get it
  return pss->AsFloat();
}

void CShell::SetFLOAT(const FString &strName, FLOAT fValue)
{
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_FLOAT) {
    ASSERT(FALSE);
    return;
  }

  // set it
  pss->AsFloat() = fValue;
}

INDEX CShell::GetINDEX(const FString &strName)
{
  // get the symbol
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_INDEX) {
    ASSERT(FALSE);
    return -666;
  }

  // get it
  return pss->AsIndex();
}

void CShell::SetINDEX(const FString &strName, INDEX iValue)
{
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_INDEX) {
    ASSERT(FALSE);
    return;
  }

  // set it
  pss->AsIndex() = iValue;
}

FString CShell::GetString(const FString &strName)
{
  // get the symbol
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_STRING) {
    ASSERT(FALSE);
    return "<invalid>";
  }

  // get it
  return pss->AsString();
}

void CShell::SetString(const FString &strName, const FString &strValue)
{
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_STRING) {
    ASSERT(FALSE);
    return;
  }

  // set it
  pss->AsString() = strValue;
}

s64 CShell::GetInt64(const FString &strName)
{
  // get the symbol
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_INT64) {
    ASSERT(FALSE);
    return -666;
  }

  // get it
  return pss->AsInt64();
}

void CShell::SetInt64(const FString &strName, s64 iValue)
{
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist or is not of given type then error
  if (pss == NULL || _shell_ast[pss->ss_istType].st_sttType != STT_INT64) {
    ASSERT(FALSE);
    return;
  }

  // set it
  pss->AsInt64() = iValue;
}

FString CShell::GetValue(const FString &strName)
{
  // get the symbol
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist then error
  if (pss == NULL) {
    ASSERT(FALSE);
    return "<invalid>";
  } 

  // get it
  ShellTypeType stt = _shell_ast[pss->ss_istType].st_sttType;
  FString strValue;

  switch (stt)
  {
    case STT_STRING: {
      strValue = pss->AsString();
    } break;

    case STT_INDEX: {
      strValue.PrintF("%d", pss->AsIndex());
    } break;

    case STT_FLOAT: {
      strValue.PrintF("%g", pss->AsFloat());
    } break;

    default: {
      ASSERT(FALSE);
      return "";
    }
  }

  return strValue;
}

void CShell::SetValue(const FString &strName, const FString &strValue)
{
  // get the symbol
  CShellSymbol *pss = GetSymbol(strName, TRUE);

  // if it doesn't exist then error
  if (pss == NULL) {
    ASSERT(FALSE);
    return;
  }

  // get it
  ShellTypeType stt = _shell_ast[pss->ss_istType].st_sttType;

  switch (stt)
  {
    case STT_STRING: {
       pss->AsString() = strValue;
    } break;

    case STT_INDEX: {
      strValue.ScanF("%d", &pss->AsIndex());
    } break;

    case STT_FLOAT: {
      strValue.ScanF("%g", &pss->AsFloat());
    } break;

    default: {
      ASSERT(FALSE);
    }
  }

  return;
}

/*
 * Report error in shell script processing.
 */
void CShell::ErrorF(const char *strFormat, ...)
{
  // synchronize access to shell
  CSyncLock slShell(&sh_csShell, TRUE);

  // print the error file and line
  const char *strName = ShellGetBufferName();
  int iLine = ShellGetBufferLineNumber();

  if (strName[0] == '<') {
    CErrorF("%s\n%s(%d): ", ShellGetBufferContents(), strName, iLine);
  } else {
    CErrorF("%s(%d): ", strName, iLine);
  }

  // format the message in buffer
  va_list arg;
  va_start(arg, strFormat);
  FString strBuffer;
  strBuffer.VPrintF(strFormat, arg);

  // print it to the main console
  CErrorF(strBuffer);
  // go to new line
  CErrorF("\n");
}

// Save shell commands to restore persistent symbols to a script file
void CShell::StorePersistentSymbols(const FPath &fnScript)
{
  // synchronize access to global shell
  CSyncLock slShell(&sh_csShell, TRUE);

  try {
    // open the file
    CTFileStream fScript;
    fScript.Create_t(fnScript);

    // print header
    fScript.FPrintF_t("// automatically saved persistent symbols:\n");

    // for each of symbols in the shell
    FOREACHINDYNAMICARRAY(sh_assSymbols, CShellSymbol, itss)
    {
      CShellSymbol &ss = *itss;

      // if it is not persistent then skip it
      if (! (ss.ss_ulFlags & SSF_PERSISTENT)) {
        continue;
      }

      const char *strUser = (ss.ss_ulFlags & SSF_USER) ? "user " : "";

      // get its type
      ShellType &st = _shell_ast[ss.ss_istType];

      // if array
      if (st.st_sttType == STT_ARRAY) {
        // get base type
        ShellType &stBase = _shell_ast[st.st_istBaseType];
        FString strType;

        // if float
        if (stBase.st_sttType == STT_FLOAT) {
          // dump all members as floats
          for (INDEX i = 0; i < st.st_ctArraySize; i++) {
            fScript.FPrintF_t("%s[%d]=(FLOAT)%g;\n", ss.ss_strName, i, ((FLOAT*)ss.ss_pvValue)[i]);
          }

        // if index
        } else if (stBase.st_sttType == STT_INDEX) {
          // dump all members as indices
          for (INDEX i = 0; i < st.st_ctArraySize; i++) {
            fScript.FPrintF_t("%s[%d]=(INDEX)%d;\n", ss.ss_strName, i, ((INDEX*)ss.ss_pvValue)[i]);
          }

        // if string
        } else if (stBase.st_sttType == STT_STRING) {
          // dump all members
          for (INDEX i = 0; i < st.st_ctArraySize; i++) {
            fScript.FPrintF_t("%s[%d]=\"%s\";\n", ss.ss_strName, i, ScriptEsc(*(FString *)ss.ss_pvValue).ConstData()[i]);
          }

        // otherwise
        } else {
          ThrowF_t("%s is an array of wrong type", ss.ss_strName);
        }

      // if float
      } else if (st.st_sttType == STT_FLOAT) {
        // dump as float
        fScript.FPrintF_t("persistent extern %sFLOAT %s=(FLOAT)%g;\n", strUser, ss.ss_strName, ss.AsFloat());

      // if index
      } else if (st.st_sttType == STT_INDEX) {
        // dump as index
        fScript.FPrintF_t("persistent extern %sINDEX %s=(INDEX)%d;\n", strUser, ss.ss_strName, ss.AsIndex());

      // if string
      } else if (st.st_sttType == STT_STRING) {
        // dump as index
        fScript.FPrintF_t("persistent extern %sFString %s=\"%s\";\n", strUser, ss.ss_strName, ScriptEsc(*(FString *)ss.ss_pvValue).ConstData());

      // otherwise
      } else {
        ThrowF_t("%s of wrong type", ss.ss_strName);
      }
    }
  } catch (char *strError) {
    WarningMessage(TRANS("Cannot save persistent symbols:\n%s"), strError);
  }
}
