/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ENDIAN_H
#define SE_INCL_ENDIAN_H

#include <Core/Base/Types.h>
#include <Core/Base/ByteSwap.h>
#include <Core/Base/Processor.h>

class CEndian final
{
  public:
    static inline u16 FromLittle(u16 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline u32 FromLittle(u32 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline u64 FromLittle(u64 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline s16 FromLittle(s16 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline s32 FromLittle(s32 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline s64 FromLittle(s64 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline f32 FromLittle(f32 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      u32* pSource = reinterpret_cast<u32 *>(&source);
      *pSource = ByteSwap32(*pSource);
      return source;
      #endif
    }

    static inline f64 FromLittle(f64 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      u64* pSource = reinterpret_cast<u64 *>(&source);
      *pSource = ByteSwap64(*pSource);
      return source;
      #endif
    }

    static inline u16 ToLittle(u16 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline u32 ToLittle(u32 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline u64 ToLittle(u64 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline s16 ToLittle(s16 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline s32 ToLittle(s32 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline s64 ToLittle(s64 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline f32 ToLittle(f32 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      u32* pSource = reinterpret_cast<u32 *>(&source);
      *pSource = ByteSwap32(*pSource);
      return source;
      #endif
    }

    static inline f64 ToLittle(f64 source)
    {
      #if SE_BYTE_ORDER == SE_LITTLE_ENDIAN
      return source;
      #else
      u64* pSource = reinterpret_cast<u64 *>(&source);
      *pSource = ByteSwap64(*pSource);
      return source;
      #endif
    }

    static inline u16 FromBig(u16 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline u32 FromBig(u32 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline u64 FromBig(u64 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline s16 FromBig(s16 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline s32 FromBig(s32 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline s64 FromBig(s64 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline f32 FromBig(f32 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      u32* pSource = reinterpret_cast<u32 *>(&source);
      *pSource = ByteSwap32(*pSource);
      return source;
      #endif
    }

    static inline f64 FromBig(f64 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      u64* pSource = reinterpret_cast<u64 *>(&source);
      *pSource = ByteSwap64(*pSource);
      return source;
      #endif
    }

    static inline u16 ToBig(u16 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline u32 ToBig(u32 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline u64 ToBig(u64 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline s16 ToBig(s16 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap16(source);
      #endif
    }

    static inline s32 ToBig(s32 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap32(source);
      #endif
    }

    static inline s64 ToBig(s64 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      return ByteSwap64(source);
      #endif
    }

    static inline f32 ToBig(f32 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      u32* pSource = reinterpret_cast<u32 *>(&source);
      *pSource = ByteSwap32(*pSource);
      return source;
      #endif
    }

    static inline f64 ToBig(f64 source)
    {
      #if SE_BYTE_ORDER == SE_BIG_ENDIAN
      return source;
      #else
      u64* pSource = reinterpret_cast<u64 *>(&source);
      *pSource = ByteSwap64(*pSource);
      return source;
      #endif
    }
};

#endif // include
