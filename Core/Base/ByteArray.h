/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_BYTEARRAY_H
#define SE_INCL_BYTEARRAY_H

#include <Core/CoreApi.h>
#include <Core/Base/Types.h>

//! Class that reperesents array of bytes.
class CORE_API FByteArray final
{
  public:
    //! Construct dummy byte array.
    FByteArray();

    //! Construct byte array from another one (clone).
    FByteArray(const FByteArray &other);

    //! Construct byte array from simple byte array.
    FByteArray(const char* pData, size_t size = size_t(-1));

    //! Construct byte array filled with B and with specified size.
    FByteArray(char b, size_t data_size);

    //! Destructor.
    ~FByteArray();

    //! Clone data from another array.
    void Copy(const FByteArray &other);

    //! Returns const pointer to data.
    const u8* ConstData() const;

    //! Returns pointer to data.
    u8* Data();    

    //! Insert another byte array into specified position.
    FByteArray &Insert(size_t pos, const FByteArray &other);

    //! Insert data of specified size into specified position.
    FByteArray &Insert(size_t pos, const char *pData, size_t size = size_t(-1));

    //! Insert specified amount of specified chars into specified position.
    FByteArray &Insert(size_t pos, size_t count, char ch);

    //! Insert another byte array to the end.
    FByteArray &Append(const FByteArray &other);

    //! Insert data of specified size to the end.
    FByteArray &Append(const char *pData, size_t size = size_t(-1));

    //! insert specified amount of chars to the end.
    FByteArray &Append(size_t count, char ch);

    //! Insert another byte array to the beginning.
    FByteArray &Prepend(const FByteArray &other);

    //! Insert data of specified size into specified position.
    FByteArray &Prepend(const char *pData, size_t size = size_t(-1));

    //! insert specified amount of chars to the beginning.
    FByteArray &Prepend(size_t count, char ch);

    //! Remove specified amount of bytes beginning from position.
    FByteArray &Remove(size_t pos, size_t count);

    //! Change size of allocated memory piece.
    void Resize(size_t new_size);

    //! Remove specified amound of bytes from the end.
    void Chop(size_t count);

    //! Clear the contents of byte array.
    void Clear();

    //! Returns byte at position.
    u8 At(size_t i) const;

    //! Fills array with specified char.
    FByteArray &Fill(u8 b, size_t size = size_t(-1));

    //! Returns size of the byte array.
    size_t Size() const;

    //! Returns true if memory is not allocated.
    bool IsNull() const;

    //! Random access operator.
    inline u8& operator[](size_t i)
    {
      return Data()[i];
    }

    //! Read Only random access operator.
    inline const u8& operator[](size_t i) const
    {
      return ConstData()[i];
    }

    //! Duplicate contents of another byte array.
    FByteArray &operator=(const FByteArray &other);

    //! Append content of another byte array.
    inline FByteArray &operator+=(const FByteArray &other)
    {
      return Append(other);
    }

    //! Append zero terminated string
    inline FByteArray &operator+=(const char *pStr)
    {
      return Append(pStr);
    }

    //! Append specified character to the end.
    inline FByteArray &operator+=(char ch)
    {
      return Append(1, ch);
    }
    
    //! Equality comparison.
    bool operator==(const FByteArray &baOther) const;

    //! Equality comparison.
    bool operator==(const char *strOther) const;
    
    //! Inequality comparison.
    bool operator!=(const FByteArray &baOther) const;

    //! Inequality comparison.
    bool operator!=(const char *strOther) const;

    //! Returns first byte.
    inline char Front() const
    {
      return ConstData()[0];
    }

    //! Returns last byte.
    inline char Back() const
    {
      return ConstData()[Size() - 1];
    }
    
    //! Swap byte array with other byte array.
    void Swap(FByteArray &other);

    //! Checks if array starts with another byte array.
    bool StartsWith(const FByteArray &other) const;

    //! Checks if array ends with specified char.
    bool StartsWith(char ch) const;

    //! Checks if array starts with specified string.
    bool StartsWith(const char *pStr) const;

    //! Checks if array starts with another byte array.
    bool EndsWith(const FByteArray &other) const;

    //! Checks if array ends with specified char.
    bool EndsWith(char ch) const;

    //! Checks if array ends with specified string.
    bool EndsWith(const char *pStr) const;

    //! Returns position of first occurence of byte array.
    size_t IndexOf(const FByteArray &other, size_t from = 0);
    
    //! Returns position of first occurence of char.
    size_t IndexOf(char ch, size_t from = 0);

    //! Returns position of first occurence of string.
    size_t IndexOf(const char *pStr, size_t from = 0);

    //! Checks if byte array contains another byte array.
    bool Contains(const FByteArray &other);
    
    //! Checks if byte array contains char.
    bool Contains(char ch);
    
    //! Checks if byte array contains string.
    bool Contains(const char *pStr);
    
    //! Returns HEX-encoded copy of array.
    FByteArray ToHex(char separator = '\x00') const;
    
    //! Write to stream.
    CORE_API friend FDataStream &operator<<(FDataStream &strm, const FByteArray &baData);

    //! Read from stream.
    CORE_API friend FDataStream &operator>>(FDataStream &strm, FByteArray &baData);

  private:
    size_t _dataSize;
    u8 *_pData;
};

#endif /* include-once check */