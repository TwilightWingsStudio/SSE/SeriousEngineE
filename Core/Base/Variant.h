/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_VARIANT_H
#define SE_INCL_VARIANT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Base/Types.h>
#include <Core/Math/Vector.h>

class CORE_API CAbstractVar
{
  public:
    enum Type
    {
      TYPE_NIL = 0,
      
      // Integral types.
      TYPE_BOOL,
      TYPE_S8,
      TYPE_U8,
      TYPE_S16,
      TYPE_U16,
      TYPE_S32,
      TYPE_U32,
      TYPE_S64,
      TYPE_U64,

      //! Single precission floating-point number.
      TYPE_F32,

      //! Double precission floating-point number.
      TYPE_F64,

      //! Pointer.
      TYPE_PTR,
 
      // Math
      TYPE_VEC2I,
      TYPE_VEC3I,
      TYPE_VEC2F,
      TYPE_VEC3F,
      TYPE_VEC2D,
      TYPE_VEC3D,
      TYPE_PLANE3F,
      TYPE_PLANE3D,
      TYPE_BOX3F,
      TYPE_BOX3D,
      TYPE_QUAT4F,
      TYPE_PLACEMENT3D,
      TYPE_MAT33F,
      
      // Data
      TYPE_UUID,
      TYPE_STRING,
      TYPE_BYTEARRAY,
      TYPE_FILENAME,

      TYPE_MAX,

      TYPE_INT_MIN = TYPE_BOOL,
      TYPE_INT_MAX = TYPE_U64,
      TYPE_SCALAR_MIN = TYPE_BOOL,
      TYPE_SCALAR_MAX = TYPE_PTR,
    };
    
    //! Returns variable type.
    virtual Type GetType() const = 0;

    //! Check if variable is integer type.
    bool IsIntegral() const;

    //! Check if variable is floating-point type.
    bool IsFloating() const;

    // Check if any arithmetic type.
    bool IsArithmetic() const;

    // Check if any scalar type.
    bool IsScalar() const;
    
    static int SizeOf(Type t);
};

class CORE_API CScalarVar : public CAbstractVar
{
  public:
    //! Constructor.
    CScalarVar();
    
    //! Copy constructor.
    CScalarVar(const CScalarVar &src);

    //! Returns variable type.
    CAbstractVar::Type GetType() const;
    
    //! Fills data area with zeroes.
    virtual void Clear();

    void FromBool(bool val);
    void FromInt8(s8 val);
    void FromUInt8(u8 val);
    void FromInt16(s16 val);
    void FromUInt16(u16 val);
    void FromInt(s32 val);
    void FromUInt(u32 val);
    void FromInt64(s64 val);
    void FromUInt64(u64 val);
    void FromFloat(float val);
    void FromDouble(double val);
    void FromPtr(void *pVal);
    virtual bool FromRawData(enum CAbstractVar::Type type, const void *pData);

    bool ToBool() const;
    s8 ToInt8() const;
    u8 ToUInt8() const;
    s16 ToInt16() const;
    u16 ToUInt16() const;
    s32 ToInt() const;
    u32 ToUInt() const;
    s64 ToInt64() const;
    u64 ToUInt64() const;
    f32 ToFloat() const;
    f64 ToDouble() const;
    void *ToPtr() const;

    inline bool &AsBool()
    {
      bool as_dummy = false;
      return IsIntegral() ? _data.as_bool : as_dummy;
    }

    inline s8 &AsInt8()
    {
      s8 as_dummy = 0;
      return IsIntegral() ? (s8&)_data.as_u8 : as_dummy;
    }

    inline u8 &AsUInt8()
    {
      u8 as_dummy = 0;
      return IsIntegral() ? _data.as_u8 : as_dummy;
    }

    inline s16 &AsInt16()
    {
      s16 as_dummy = 0;
      return IsIntegral() ? (s16&)_data.as_u16 : as_dummy;
    }

    inline u16 &AsUInt16()
    {
      u16 as_dummy = 0;
      return IsIntegral() ? _data.as_u16 : as_dummy;
    }

    inline s32 &AsInt()
    {
      s32 as_dummy = 0;
      return IsIntegral() ? (s32&)_data.as_u32 : as_dummy;
    }

    inline u32 &AsUInt()
    {
      u32 as_dummy = 0;
      return IsIntegral() ? _data.as_u32 : as_dummy;
    }

    inline s64 &AsInt64()
    {
      s64 as_dummy = 0;
      return IsIntegral() ? (s64&)_data.as_u64 : as_dummy;
    }

    inline u64 &AsUInt64()
    {
      u64 as_dummy = 0;
      return IsIntegral() ? _data.as_u64 : as_dummy;
    }

    inline f32 &AsFloat()
    {
      f32 as_dummy = 0;
      return (GetType() == TYPE_F32) ? _data.as_f32 : as_dummy;
    }

    inline f64 &AsDouble()
    {
      f64 as_dummy = 0;
      return (GetType() == TYPE_F64) ? _data.as_f64 : as_dummy;
    }

    inline uintptr_t &AsPtr()
    {
      uintptr_t as_dummy = 0;
      return (GetType() == TYPE_PTR) ? _data.as_ptrval : as_dummy;
    }
    
  protected:
    enum CAbstractVar::Type _type;
    
    union {
      bool as_bool;
      u8 as_u8;
      u16 as_u16;
      u32 as_u32;
      u64 as_u64;
      f32 as_f32;
      f64 as_f64;
      void *as_ptr;
      uintptr_t as_ptrval;
    } _data;
};

//! Class that stores single value of any type.
class CORE_API FVariant final : public CScalarVar
{
  public:
    //! Constructor.
    FVariant();
    
    //! Copy constructor.
    FVariant(const FVariant &src);

    //! 1 x bool
    FVariant(bool val);

    //! 1 x unsigned char
    FVariant(u8 val);

    //! 1 x char
    FVariant(s8 val);

    //! 1 x unsigned short
    FVariant(u16 val);

    //! 1 x short
    FVariant(s16 val);

    //! 1 x unsigned int
    FVariant(u32 val);

    //! 1 x int
    FVariant(s32 val);

    //! 1 x u64
    FVariant(u64 val);

    //! 1 x s64
    FVariant(s64 val);

    //! 1 x float
    FVariant(f32 val);

    //! 1 x double
    FVariant(f64 val);

    //! Pointer
    FVariant(void *pVal);

    //! 2 x int.
    FVariant(const VEC2I &val);
    
    //! 3 x int.
    FVariant(const VEC3I &val);
    
    //! 2 x float.
    FVariant(const VEC2F &val);
    
    //! 3 x float.
    FVariant(const VEC3F &val);
    
    //! 2 x double.
    FVariant(const VEC2D &val);
    
    //! 3 x double.
    FVariant(const VEC3D &val);

    //! 4 x float.
    FVariant(const PLANE3F &val);

    //! 4 x double.
    FVariant(const PLANE3D &val);

    //! 6 x float.
    FVariant(const BOX3F &val);

    //! 6 x double.
    FVariant(const BOX3D &val);

    //! 4 x float.
    FVariant(const QUAT4F &val);

    //! 9 x float.
    FVariant(const MAT33F &val);

    //! 6 x float.
    FVariant(const CPlacement3D &val);

    //! 16 bytes
    FVariant(const FUuid &val);

    //! Zero-terminated string.
    FVariant(const FString &val);

    //! Sized array that can contain even '\0'.
    FVariant(const FByteArray &val);

    //! String bundled with 'CResource *'.
    FVariant(const FPath &val);
    
    //! Destructor.
    ~FVariant();

    //! Remove memory.
    void Clear();
    
    //! Copy data from another instance.
    void Copy(const FVariant &src);

    // Math Setters
    void FromVec2i(const VEC2I& val);
    void FromVec3i(const VEC3I& val);
    void FromVec2f(const VEC2F& val);
    void FromVec3f(const VEC3F& val);
    void FromVec2d(const VEC2D& val);
    void FromVec3d(const VEC3D& val);
    void FromPlane3f(const PLANE3F& val);
    void FromPlane3d(const PLANE3D& val);
    void FromBox3f(const BOX3F& val);
    void FromBox3d(const BOX3D& val);
    void FromQuat4f(const QUAT4F& val);
    void FromMat33f(const MAT33F& val);
    void FromPlacement3D(const CPlacement3D& val);

    // Data Setters
    void FromUuid(const FUuid &val);
    void FromString(const FString &val);
    void FromByteArray(const FByteArray &val);
    void FromFilename(const FPath &val);
    virtual bool FromRawData(enum CAbstractVar::Type type, const void *pData);

    // Math Getters
    void ToVec2i(VEC2I &val) const;
    void ToVec3i(VEC3I &val) const;
    void ToVec2f(VEC2F &val) const;
    void ToVec3f(VEC3F &val) const;
    void ToVec2d(VEC2D &val) const;
    void ToVec3d(VEC3D &val) const;
    void ToPlane3f(PLANE3F &val) const;
    void ToPlane3d(PLANE3D &val) const;
    void ToBox3f(BOX3F &val) const;
    void ToBox3d(BOX3D &val) const;
    void ToQuat4f(QUAT4F &val) const;
    void ToMat33f(MAT33F &val) const;
    void ToPlacement3D(CPlacement3D &val) const;
    
    // Data Getters
    void ToUuid(FUuid &val) const;
    void ToString(FString &val) const;
    void ToByteArray(FByteArray &val) const;
    void ToFilename(FPath &val) const;
};

#endif  /* include-once check. */