/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PROCESSOR_H
#define SE_INCL_PROCESSOR_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/CoreApi.h>
#include <Core/Base/Types.h>

#if defined(__ORDER_BIG_ENDIAN__)
  #define SE_BIG_ENDIAN __ORDER_BIG_ENDIAN__
#else
  #define SE_BIG_ENDIAN 4321
#endif

#if defined(__ORDER_LITTLE_ENDIAN__)
  #define SE_LITTLE_ENDIAN __ORDER_LITTLE_ENDIAN__
#else
  #define SE_LITTLE_ENDIAN 1234
#endif

// Any Compiler - Any RISC
#if defined(__arm__) || defined(__TARGET_ARCH_ARM) || defined(_M_ARM) || defined(_M_ARM64) || defined(__aarch64__) || defined(__ARM64__)
  #if defined(__ARMEL__) || defined(_M_ARM64)
    #define SE_BYTE_ORDER SE_LITTLE_ENDIAN
  #elif defined(__ARMEB__)
    #define SE_BYTE_ORDER SE_BIG_ENDIAN
  #endif
// GCC - Get for any non RISC
#elif defined(__BYTE_ORDER__)
  #define SE_BYTE_ORDER __BYTE_ORDER__
// MSVC - Non RISC
#else
  #define SE_BYTE_ORDER SE_LITTLE_ENDIAN
#endif

class CORE_API FProcessorId final
{
    private:
        u32 _registers[4];

    public:
        FProcessorId(u32 funcId, u32 subFuncId);

        const u32 &EAX() const
        {
            return _registers[0];
        }

        const u32 &EBX() const
        {
            return _registers[1];
        }

        const u32 &ECX() const
        {
            return _registers[2];
        }

        const u32 &EDX() const
        {
            return _registers[3];
        }
};

#endif