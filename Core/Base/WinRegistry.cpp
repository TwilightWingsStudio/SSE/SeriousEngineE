/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/Base/String.h>
#include <Core/IO/FileName.h>
#include <Core/Base/WinRegistry.h>

#ifdef _WIN32
#pragma comment(lib, "advapi32.lib")

static void ParseKeyName(const FString &strKey, HKEY &hKeyRoot, FString &strKeyPath, FString &strKeyName)
{
  FString strRemain = strKey;
  // separate key into the value part and path
  strKeyName = FPath(strRemain).FileName();
  strRemain = FPath(strRemain).FileDir();
  strRemain.TrimRight(strRemain.Length() - 1); // removes trailing backslash

  // try to find root key value
  if (strRemain.RemovePrefix("HKEY_CLASSES_ROOT\\")) {
    hKeyRoot = HKEY_CLASSES_ROOT;
  } else if (strRemain.RemovePrefix("HKEY_CURRENT_CONFIG\\")) {
    hKeyRoot = HKEY_CURRENT_CONFIG;
  } else if (strRemain.RemovePrefix("HKEY_CURRENT_USER\\")) {
    hKeyRoot = HKEY_CURRENT_USER;
  } else if (strRemain.RemovePrefix("HKEY_LOCAL_MACHINE\\")) {
    hKeyRoot = HKEY_LOCAL_MACHINE;
  } else if (strRemain.RemovePrefix("HKEY_USERS\\")) {
    hKeyRoot = HKEY_USERS;
  } else {
    ASSERT(FALSE);
    hKeyRoot = HKEY_CURRENT_USER;
  }

  strKeyPath = strRemain;
}

CORE_API BOOL REG_GetString(const FString &strKey, FString &strString)
{
  // parse the key name
  HKEY hKeyRoot; FString strKeyPath; FString strKeyName;
  ParseKeyName(strKey, hKeyRoot, strKeyPath, strKeyName);

  // open the key
  HKEY hkey;
  LONG lRes = RegOpenKeyExA(hKeyRoot, strKeyPath, 0, KEY_READ, &hkey);

  if (lRes != ERROR_SUCCESS) {
    return FALSE;
  }

  // get the value
  char achrKeyValue[1024];
  ULONG ctType;
  ULONG ctLength = sizeof(achrKeyValue);
  LONG lResult = RegQueryValueExA(hkey, strKeyName, 0, &ctType, (unsigned char *)achrKeyValue, &ctLength);

  if (lResult != ERROR_SUCCESS) {
    return FALSE;
  }

  // close the key
  RegCloseKey(hkey);

  strString = FString( achrKeyValue);
  return TRUE;
}

CORE_API BOOL REG_GetLong(const FString &strKey, ULONG &ulLong)
{
  // parse the key name
  HKEY hKeyRoot; FString strKeyPath; FString strKeyName;
  ParseKeyName(strKey, hKeyRoot, strKeyPath, strKeyName);

  // open the key
  HKEY hkey;
  LONG lRes = RegOpenKeyExA(hKeyRoot, strKeyPath, 0, KEY_READ, &hkey);

  if (lRes != ERROR_SUCCESS) {
    return FALSE;
  }

  // get the value
  ULONG ulKeyValue;
  ULONG ctType;
  ULONG ctLength = sizeof(ulKeyValue);
  LONG lResult = RegQueryValueExA(hkey, strKeyName, 0, &ctType, (unsigned char *)&ulKeyValue, &ctLength);

  if (lResult != ERROR_SUCCESS) {
    return FALSE;
  }

  // close the key
  RegCloseKey(hkey);

  ulLong = ulKeyValue;
  return TRUE;
}

CORE_API BOOL REG_SetString(const FString &strKey, const FString &strString)
{
  // parse the key name
  HKEY hKeyRoot; FString strKeyPath; FString strKeyName;
  ParseKeyName(strKey, hKeyRoot, strKeyPath, strKeyName);

  // create the registry key
  HKEY hkey;
  DWORD dwDisposition;
  LONG lRes = RegCreateKeyExA(hKeyRoot, strKeyPath.ConstData(), 0,
    "", REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, &dwDisposition);

  if (lRes != ERROR_SUCCESS) {
    return FALSE;
  }

  // set the value
  lRes = RegSetValueExA(hkey, strKeyName, 0, REG_SZ, (const UBYTE *)strString.ConstData(), strString.Length());

  // close the key
  RegCloseKey(hkey);

  return lRes==ERROR_SUCCESS;
}

#endif
