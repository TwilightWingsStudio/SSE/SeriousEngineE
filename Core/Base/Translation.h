/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TRANSLATION_H
#define SE_INCL_TRANSLATION_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Init translation routines with given translation table.
CORE_API void InitTranslation(void);

//! Add given translation table.
CORE_API void AddTranslationTable_t(const FPath &fnmTable); // throw char *

//! Add several tables from a directory using wildcards.
CORE_API void AddTranslationTablesDir_t(const FPath &fnmDir, const FPath &fnmPattern); // throw char *

//! Finish translation table building.
CORE_API void FinishTranslationTable(void);

//! Read given translation table from file - for internal use.
CORE_API void ReadTranslationTable_t(
  TDynamicArray<class CTranslationPair> &atpPairs, const FPath &fnmTable); // throw char *

//! Translate a string.
CORE_API char * Translate(char *str, INDEX iOffset=0);

//! Translate a string.
CORE_API const char * TranslateConst(const char *str, INDEX iOffset=0);

// macro for inserting a string for translation into executables
#define TRANS(str) Translate("ETRS" str, 4)

// macro for translating a variable string (usually can be FString)
#define TRANSV(str) TranslateConst(str, 0)

#endif  /* include-once check. */
