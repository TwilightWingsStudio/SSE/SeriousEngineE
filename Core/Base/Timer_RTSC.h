/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TIMER_RTSC_H
#define SE_INCL_TIMER_RTSC_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#ifdef PLATFORM_UNIX
#include <x86intrin.h>
#endif

// Read the Pentium TimeStampCounter
inline __int64 ReadTSC(void)
{
#ifdef SE_OLD_COMPILER
  __int64 mmRet;
  __asm {
    rdtsc
    mov   dword ptr [mmRet+0],eax
    mov   dword ptr [mmRet+4],edx
  }
  return mmRet;
#elif (defined _MSC_VER)
  return __rdtsc(); // NOTE: Windows Only!
#else
  #error Fill this in for your platform.
#endif
}

#endif /* include-once check. */