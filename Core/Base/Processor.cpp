/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/Processor.h>

#ifdef PLATFORM_UNIX
#include <cpuid.h>
#endif

#ifdef SE_OLD_COMPILER
static void __cpuidex(int CPUInfo[4], int Function, int SubLeaf)
{
  __asm {
    mov     eax, Function;
    cpuid
    mov     dword ptr [CPUInfo + 0], eax
    mov     dword ptr [CPUInfo + 4], ebx
    mov     dword ptr [CPUInfo + 8], edx
    mov     dword ptr [CPUInfo + 12], ecx
  }
}
#endif

FProcessorId::FProcessorId(u32 funcId, u32 subFuncId)
{
#ifdef _WIN32
  __cpuidex((int*)_registers, (int)funcId, (int)subFuncId);
#else
  __cpuid(funcId, _registers[0], _registers[1], _registers[2], _registers[3]);
#endif
}