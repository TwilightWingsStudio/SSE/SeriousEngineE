/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/String.h>
#include <Core/Base/Memory.h>
#include <Core/IO/Stream.h>
#include <Core/Base/Console.h>

/*
 * Equality comparison.
 */
BOOL FString::operator==(const FString &strOther) const
{
  ASSERT(IsValid() && strOther.IsValid());

  return stricmp(ConstData(), strOther.ConstData()) == 0;
}

BOOL FString::operator==(const char *strOther) const
{
  ASSERT(IsValid() && strOther!=NULL);

  return stricmp(ConstData(), strOther) == 0;
}

BOOL operator==(const char *strThis, const FString &strOther)
{
  ASSERT(strOther.IsValid() && strThis!=NULL);

  return strOther == strThis;
}

/*
 * Inequality comparison.
 */
BOOL FString::operator!=(const FString &strOther) const
{
  ASSERT(IsValid() && strOther.IsValid());

  return !(*this == strOther);
}

BOOL FString::operator!=(const char *strOther) const
{
  ASSERT(IsValid() && strOther!=NULL);

  return !(*this == strOther);
}

BOOL operator!=(const char *strThis, const FString &strOther)
{
  ASSERT(strOther.IsValid() && strThis!=NULL);

  return !(strOther == strThis);
}

/*
 * String concatenation.
 */
FString FString::operator+(const FString &strSecond) const
{
  ASSERT(IsValid() && strSecond.IsValid());

  return (FString(*this) += strSecond);
}

FString operator+(const char *strFirst, const FString &strSecond)
{
  ASSERT(strFirst!=NULL && strSecond.IsValid());

  return (FString(strFirst) += strSecond);
}

FString &FString::operator+=(const FString &strSecond)
{
  ASSERT(IsValid() && strSecond.IsValid());

  GrowMemory((void **)&str_String, Length() + strSecond.Length() + 1);
  strcat(Data(), strSecond.ConstData());

  return *this;
}

/*
 * Remove given prefix string from this string
 */
BOOL FString::RemovePrefix( const FString &strPrefix)
{
  INDEX lenPrefix = strPrefix.Length();
  INDEX lenDest = Length() - lenPrefix;

  if (strnicmp(ConstData(), strPrefix.ConstData(), lenPrefix) != 0) {
    return FALSE;
  }

  FString strTemp = &ConstData()[lenPrefix];
  ShrinkMemory((void **)&str_String, lenDest + 1);
  strcpy(Data(), strTemp.ConstData());

  return TRUE;
}

/* Check if has given prefix */
BOOL FString::HasPrefix( const FString &strPrefix) const
{
  INDEX lenPrefix = strPrefix.Length();

  if (strnicmp(ConstData(), strPrefix.ConstData(), lenPrefix) != 0) {
    return FALSE;
  }

  return TRUE;
}

/* Find index of a substring in a string (returns -1 if not found). */
INDEX FString::FindSubstr(const FString &strSub)
{
  INDEX ct = Length();
  INDEX ctSub = strSub.Length();

  for (INDEX i = 0; i < ct - ctSub + 1; i++)
  {
    for (INDEX iSub = 0; iSub < ctSub; iSub++) {
      if ((*this)[i + iSub] != strSub[iSub]) {
        goto wrong_door;
      }
    }

    return i;
wrong_door:;
    // Hey buddy, I think you've got the wrong door, the substring club's two blocks down.
  }

  return -1;
}

/* Replace a substring in a string. */
BOOL FString::ReplaceSubstr(const FString &strSub, const FString &strNewSub)
{
  INDEX iPos = FindSubstr(strSub);

  if (iPos < 0) {
    return FALSE;
  }

  FString strPart1, strPart2;
  Split(iPos, strPart1, strPart2);
  strPart2.RemovePrefix(strSub);

  *this = strPart1 + strNewSub + strPart2;

  return TRUE;
}

/* Trim the string from left to contain at most given number of characters. */
INDEX FString::TrimLeft( INDEX ctCharacters)
{
  // clamp negative values
  if (ctCharacters < 0) ctCharacters = 0;

  // find how much characters to remove
  INDEX lenOriginal = Length();
  INDEX lenPrefix = lenOriginal - ctCharacters;

  // if nothing needs to be removed
  if (lenPrefix <= 0) return 0;

  // crop
  memmove(Data(), &ConstData()[lenPrefix], ctCharacters + 1);
  ShrinkMemory((void **)&str_String, ctCharacters + 1);

  return lenPrefix;
}

/* Trim the string from right to contain at most given number of characters. */
INDEX FString::TrimRight( INDEX ctCharacters)
{
  // clamp negative values
  if (ctCharacters < 0) ctCharacters = 0;

  // find how much characters to remove
  INDEX lenOriginal = Length();
  INDEX lenPrefix = lenOriginal - ctCharacters;

  // if nothing needs to be removed
  if (lenPrefix <= 0) return 0;

  // crop
  Data()[ctCharacters] = '\0';
  ShrinkMemory((void **)&str_String, ctCharacters + 1);

  return lenPrefix;
}

// return naked length of the string (ignoring all decorate codes)
INDEX FString::LengthNaked(void) const
{
  return Undecorated().Length();
}

// strip decorations from the string
FString FString::Undecorated(void) const
{
  // make a copy of the string to hold the result - we will rewrite it without the codes
  FString strResult = *this;

  // start at the beginning of both strings
  const char *pchSrc = ConstData();
  char *pchDst = strResult.Data();

  // while the source is not finished
  while (pchSrc[0] != 0)
  {
    // if the source char is not escape char
    if (pchSrc[0] != '^') {
      // copy it over
      *pchDst++ = *pchSrc++;
      // go to next char
      continue;
    }

    // check the next char
    switch (pchSrc[1])
    {
      // if one of the control codes, skip corresponding number of characters
      case 'c':  pchSrc += 2 + FindZero((UBYTE*)pchSrc + 2, 6);  break;
      case 'a':  pchSrc += 2 + FindZero((UBYTE*)pchSrc + 2, 2);  break;
      case 'f':  pchSrc += 2 + FindZero((UBYTE*)pchSrc + 2, 2);  break;
      case 'b':  case 'i':  case 'r':  case 'o':
      case 'C':  case 'A':  case 'F':  case 'B':  case 'I':  pchSrc += 2;  break;
      // if it is the escape char again, skip the first escape and copy the char
      case '^':  pchSrc++; *pchDst++ = *pchSrc++; break;
      // if it is something else
      default:
        // just copy over the control char
        *pchDst++ = *pchSrc++;
        break;
    }
  }

  *pchDst++ = 0;
  ASSERT(strResult.Length() <= Length());
  return strResult;
}

BOOL IsSpace(char c)
{
  return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

/* Trim the string from from spaces from left. */
INDEX FString::TrimSpacesLeft(void)
{
  // for each character in string
  const char *chr;

  for (chr = ConstData(); *chr != 0; chr++)
  {
    // if the character is not space then stop searching
    if (!IsSpace(*chr)) {
      break;
    }
  }

  // trim to that character
  return TrimLeft(ConstData() + Length() - chr);
}

/* Trim the string from from spaces from right. */
INDEX FString::TrimSpacesRight(void)
{
  // for each character in string reversed
  const char *chr;

  for (chr = ConstData() + Length() - 1; chr > ConstData(); chr--)
  {
    // if the character is not space then stop searching
    if (!IsSpace(*chr)) {
      break;
    }
  }

  // trim to that character
  return TrimRight(chr - ConstData() + 1);
}

// retain only first line of the string
void FString::OnlyFirstLine(void)
{
  // get position of first line end
  const char *pchNL = strchr(ConstData(), '\n');

  // if none then do nothing
  if (pchNL == NULL) {
    return;
  }

  // trim everything after that char
  TrimRight(pchNL - ConstData());
}


/* Calculate hashing value for the string. */
ULONG FString::GetHash(void) const
{
  ULONG ulKey = 0;
  INDEX len = Length();

  for (INDEX i = 0; i < len; i++) {
    ulKey = _rotl(ulKey, 4) + toupper(ConstData()[i]);
  }

  return ulKey;
}

/*
 * Throw exception
 */
void FString::Throw_t(void)
{
  throw(str_String);
}

void FString::ReadFromText_t(CTStream &strmStream,
                              const FString &strKeyword="") // throw char *
{
  ASSERT(IsValid());

  // keyword must be present
  strmStream.ExpectKeyword_t(strKeyword);

  // read the string from the file
  char str[1024];
  strmStream.GetLine_t(str, sizeof(str));

  // copy it here
  (*this) = str;
}

#ifndef NDEBUG
/*
 * Check if string data is valid.
 */
BOOL FString::IsValid(void) const
{
  ASSERT(this != NULL && str_String != NULL);
  return TRUE;
}
#endif // NDEBUG

/* Load an entire text file into a string. */
void FString::ReadUntilEOF_t(CTStream &strmFile)  // throw char *
{
  // get the file size
  SLONG slFileSize = strmFile.GetStreamSize() - strmFile.GetPos_t();

  // allocate that much memory
  FreeMemory(str_String);
  str_String = (char *)AllocMemory(slFileSize + 1);  // take end-marker in account

  // read the entire file there
  if (slFileSize > 0) {
    strmFile.Read_t(Data(), slFileSize);
  }

  // add end marker
  Data()[slFileSize] = 0;

  // rewrite entire string
  char *pchRead = Data();
  char *pchWrite = Data();

  while (*pchRead != 0)
  {
    // skip the '\r' characters
    if (*pchRead != '\r') {
      *pchWrite++ = *pchRead++;
    } else {
      pchRead++;
    }
  }

  *pchWrite = 0;
}

void FString::Load_t(const class FPath &fnmFile)  // throw char *
{
  ASSERT(IsValid());

  // open the file for reading
  CTFileStream strmFile;
  strmFile.Open_t(fnmFile);

  // read string until end of file
  ReadUntilEOF_t(strmFile);
}


void FString::LoadKeepCRLF_t(const class FPath &fnmFile)  // throw char *
{
  ASSERT(IsValid());

  // open the file for reading
  CTFileStream strmFile;
  strmFile.Open_t(fnmFile);
  // get the file size
  SLONG slFileSize = strmFile.GetStreamSize();

  // allocate that much memory
  FreeMemory(str_String);
  str_String = (char *)AllocMemory(slFileSize + 1);  // take end-marker in account

  // read the entire file there
  if (slFileSize > 0) {
    strmFile.Read_t(Data(), slFileSize);
  }

  // add end marker
  Data()[slFileSize] = 0;
}

/* Save an entire string into a text file. */
void FString::Save_t(const class FPath &fnmFile)  // throw char *
{
  // open the file for writing
  CTFileStream strmFile;
  strmFile.Create_t(fnmFile);

  // save the string to the file
  strmFile.PutString_t(*this);
}

void FString::SaveKeepCRLF_t(const class FPath &fnmFile)  // throw char *
{
  // open the file for writing
  CTFileStream strmFile;
  strmFile.Create_t(fnmFile);

  // save the string to the file
  if (Length() > 0) {
    strmFile.Write_t(ConstData(), Length());
  }
}

// Print formatted to a string
INDEX FString::PrintF(const char *strFormat, ...)
{
  va_list arg;
  va_start(arg, strFormat);

  return VPrintF(strFormat, arg);
}

INDEX FString::VPrintF(const char *strFormat, va_list arg)
{
  INDEX ctBufferSize = 256;
  char *pBuffer = (char*)AllocMemory(ctBufferSize); // allocate it

  INDEX iLen;

  // Repeat until we print it out.
  FOREVER {
    // print to the buffer
    iLen = _vsnprintf(pBuffer, ctBufferSize, strFormat, arg);

    // If printed ok then stop.
    if (iLen != -1) {
      break;
    }

    // increase the buffer size
    ctBufferSize += 256;
    GrowMemory((void**)&pBuffer, ctBufferSize);
  }

  (*this) = pBuffer;
	FreeMemory(pBuffer);
  return iLen;
}

#ifdef SE_OLD_COMPILER

static void *psscanf = &sscanf;

// Scan formatted from a string
__declspec(naked) INDEX FString::ScanF(const char *strFormat, ...) const
{
  __asm {
    push    eax
    mov     eax,dword ptr [esp+8]
    mov     eax,dword ptr [eax]
    mov     dword ptr [esp+8], eax
    pop     eax
    jmp     dword ptr [psscanf]
  }
}

#else

// Scan formatted from a string
INDEX FString::ScanF(const char *strFormat, ...) const
{
  INDEX iResult;
  va_list arg;
  va_start(arg, strFormat);
  iResult = vsscanf(ConstData(), strFormat, arg);
  va_end(arg);

  return iResult;
}

#endif

// split string in two strings at specified position (char AT splitting position goes to str2)
void FString::Split( INDEX iPos, FString &str1, FString &str2)
{
  str1 = ConstData();
  str2 = ConstData();
  str1.TrimRight(iPos);
  str2.TrimLeft(str2.Length() - iPos);
}

// insert one character in string at specified pos
void FString::InsertChar( INDEX iPos, char cChr)
{
  // clamp position
  INDEX ctChars = Length();

  if (iPos > ctChars) {
    iPos = ctChars;
  } else if (iPos < 0) {
    iPos = 0;
  }

  // grow memory used by string
  GrowMemory((void **)&str_String, ctChars + 2);

  // copy part of string to make room for char to insert
  memmove(&Data()[iPos + 1], &ConstData()[iPos], ctChars + 1 - iPos);
  Data()[iPos] = cChr;
}

// delete one character from string at specified pos
void FString::DeleteChar( INDEX iPos)
{
  // clamp position
  INDEX ctChars = Length();

  if (ctChars == 0) {
    return;
  }

  if (iPos > ctChars) {
    iPos = ctChars;
  } else if (iPos < 0) {
    iPos = 0;
  }

  // copy part of string
  memmove(&Data()[iPos], &ConstData()[iPos + 1], ctChars - iPos + 1);
  // shrink memory used by string over deleted char
  ShrinkMemory((void **)&str_String, ctChars);
}

// wild card comparison
BOOL FString::Matches(const FString &strOther) const
{
  return Matches(strOther.ConstData());
}

BOOL FString::Matches(const char *strOther) const
{
// pattern matching code from sourceforge.net codesnippet archive
// adjusted a bit to match in/out parameters
#define        MAX_CALLS        200
  int        calls = 0, wild = 0, q = 0;
  const char *mask = strOther, *name = ConstData();
  const char *m = mask, *n = name, *ma = mask, *na = name;
  
  for (;;)
  {
    if (++calls > MAX_CALLS) {
      return FALSE;
    }

    if (*m == '*') {
      while (*m == '*') ++m;
      wild = 1;
      ma = m;
      na = n;
    }
    
    if (!*m) {
      if (!*n) {
        return TRUE;
      }
      
      for (--m; (m > mask) && (*m == '?'); --m) ;
      
      if ((*m == '*') && (m > mask) && (m[-1] != '\\')) {
        return TRUE;
      }

      if (!wild) {
        return FALSE;
      }

      m = ma;
    } else if (!*n) {
      while (*m == '*') ++m;

      if (*m != 0) {
        return FALSE;
      } else {
        return TRUE;
      }
    }

    if ((*m == '\\') && ((m[1] == '*') || (m[1] == '?'))) {
      ++m;
      q = 1;
    } else {
      q = 0;
    }
    
    if ((tolower(*m) != tolower(*n)) && ((*m != '?') || q)) {
      if (!wild) {
        return FALSE;
      }

      m = ma;
      n = ++na;
    } else {
      if (*m) ++m;
        if (*n) ++n;
    }
  }
}

// variable management functions
void FString::LoadVar(const class FPath &fnmFile)
{
  try {
    FString str;
    str.Load_t(fnmFile);
    *this = str;
  } catch (char *strError) {
    CWarningF(TRANS("Cannot load variable from '%s':\n%s\n"), fnmFile.ConstData(), strError);
  }
}

void FString::SaveVar(const class FPath &fnmFile)
{
  try {
    Save_t(fnmFile);
  } catch (char *strError) {
    CWarningF(TRANS("Cannot save variable to '%s':\n%s\n"), fnmFile.ConstData(), strError);
  }
}

// general variable functions
void LoadStringVar(const FPath &fnmVar, FString &strVar)
{
  strVar.LoadVar(fnmVar);
}

void SaveStringVar(const FPath &fnmVar, FString &strVar)
{
  strVar.SaveVar(fnmVar);
}

void LoadIntVar(const FPath &fnmVar, INDEX &iVar)
{
  FString strVar;
  strVar.LoadVar(fnmVar);

  if (strVar != "") {
    FString strHex = strVar;
    if (strHex.RemovePrefix("0x")) {
      strHex.ScanF("%x", &iVar);
    } else {
      strVar.ScanF("%d", &iVar);
    }
  }
}

void SaveIntVar(const FPath &fnmVar, INDEX &iVar)
{
  FString strVar;
  strVar.PrintF("%d", iVar);
  strVar.SaveVar(fnmVar);
}

/*
 * Read from stream.
 */
CTStream &operator>>(CTStream &strmStream, FString &str)
{
  strmStream.ReadString_t(str);
  return strmStream;
}

/*
 * Write to stream.
 */
CTStream &operator<<(CTStream &strmStream, const FString &str)
{
  strmStream.WriteString_t(str);
  return strmStream;
}

#include <Core/IO/DataStream.h>

FDataStream &operator>>(FDataStream &strmStream, FString &str)
{
  strmStream.ReadString_t(str);
  return strmStream;
}

FDataStream &operator<<(FDataStream &strmStream, const FString &str)
{
  strmStream.WriteString_t(str);
  return strmStream;
}