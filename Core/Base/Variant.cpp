/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Base/String.h>
#include <Core/Base/Memory.h>
#include <Core/Base/Variant.h>
#include <Core/Base/UUID.h>
#include <Core/Math/AABBox.h>
#include <Core/Math/Placement.h>
#include <Core/Math/Quaternion.h>
#include <Core/IO/FileName.h>

int CAbstractVar::SizeOf(Type t)
{
  ASSERT(t > TYPE_NIL && t < TYPE_MAX);

  static const int TYPE_SIZES[TYPE_MAX] = {
    0, // TYPE_NIL

    // Scalar Types
    1, // TYPE_BOOL
    1, // TYPE_S8
    1, // TYPE_U8
    2, // TYPE_S16
    2, // TYPE_U16
    4, // TYPE_S32
    4, // TYPE_U32
    8, // TYPE_S64
    8, // TYPE_U64
    4, // TYPE_F32
    8, // TYPE_F64
    8, // TYPE_PTR

    // Math Types
    sizeof(s32) * 2, // TYPE_VEC2I
    sizeof(s32) * 3, // TYPE_VEC3I
    sizeof(float) * 2, // TYPE_VEC2F
    sizeof(float) * 3, // TYPE_VEC3F
    sizeof(double) * 2, // TYPE_VEC2D
    sizeof(double) * 3, // TYPE_VEC3D
    sizeof(PLANE3F), // TYPE_PLANE3F
    sizeof(PLANE3D), // TYPE_PLANE3D
    sizeof(BOX3F), // TYPE_BOX3F
    sizeof(BOX3D), // TYPE_BOX3D
    sizeof(QUAT4F), // TYPE_QUAT4F
    sizeof(MAT33F), // TYPE_MAT33F
    sizeof(float) * 6, // TYPE_PLACEMENT3D

    // Data
    16,                 // TYPE_UUID
    0,                  // TYPE_STRING
    sizeof(FByteArray), // TYPE_BYTEARRAY
    sizeof(FPath), // TYPE_FILENAME
  };

  if (t > TYPE_NIL && t < TYPE_MAX) {
    return TYPE_SIZES[t];
  }

  return 0;
}

bool CAbstractVar::IsIntegral() const
{
  return GetType() >= TYPE_INT_MIN && GetType() <= TYPE_INT_MAX;
}

bool CAbstractVar::IsFloating() const
{
  return GetType() == TYPE_F32 || GetType() == TYPE_F64;
}

bool CAbstractVar::IsArithmetic() const
{
  return IsIntegral() && IsFloating();
}

bool CAbstractVar::IsScalar() const
{
  return GetType() >= TYPE_SCALAR_MIN && GetType() <= TYPE_SCALAR_MAX;
}

CScalarVar::CScalarVar()
{
  Clear();
}

CAbstractVar::Type CScalarVar::GetType() const
{
  return _type;
}

void CScalarVar::Clear()
{
  _type = TYPE_NIL;
  _data.as_u64 = 0;
}

void CScalarVar::FromBool(bool val)
{
  Clear();
  _type = TYPE_BOOL;
  _data.as_bool = val;
}

void CScalarVar::FromInt8(s8 val)
{
  Clear();
  _type = TYPE_S8;
  _data.as_u8 = val;
}

void CScalarVar::FromUInt8(u8 val)
{
  Clear();
  _type = TYPE_U8;
  _data.as_u8 = val;
}

void CScalarVar::FromInt16(s16 val)
{
  Clear();
  _type = TYPE_S16;
  _data.as_u16 = val;
}

void CScalarVar::FromUInt16(u16 val)
{
  Clear();
  _type = TYPE_U16;
  _data.as_u16 = val;
}

void CScalarVar::FromInt(s32 val)
{
  Clear();
  _type = TYPE_S32;
  _data.as_u32 = val;
}

void CScalarVar::FromUInt(u32 val)
{
  Clear();
  _type = TYPE_U32;
  _data.as_u32 = val;
}

void CScalarVar::FromInt64(s64 val)
{
  Clear();
  _type = TYPE_S64;
  _data.as_u64 = val;
}

void CScalarVar::FromUInt64(u64 val)
{
  Clear();
  _type = TYPE_U64;
  _data.as_u64 = val;
}

void CScalarVar::FromFloat(float val)
{
  Clear();
  _type = TYPE_F32;
  _data.as_f32 = val;
}

void CScalarVar::FromDouble(double val)
{
  Clear();
  _type = TYPE_F64;
  _data.as_f64 = val;
}

void CScalarVar::FromPtr(void *pVal)
{
  Clear();
  _type = TYPE_PTR;
  _data.as_ptr = pVal;
}

bool CScalarVar::FromRawData(enum CAbstractVar::Type type, const void *pData)
{
  switch (type)
  {
    case CAbstractVar::TYPE_BOOL: {
      const bool *pValue = (const bool *)pData;
      FromBool(*pValue);
      return true;
    }

    case CAbstractVar::TYPE_S8: {
      const s8 *pValue = (const s8 *)pData;
      FromInt8(*pValue);
      return true;
    }
    
    case CAbstractVar::TYPE_U8: {
      const u8 *pValue = (const u8 *)pData;
      FromUInt8(*pValue);
      return true;
    }

    case CAbstractVar::TYPE_S16: {
      const s16 *pValue = (const s16 *)pData;
      FromInt16(*pValue);
      return true;
    }
    
    case CAbstractVar::TYPE_U16: {
      const u16 *pValue = (const u16 *)pData;
      FromUInt16(*pValue);
      return true;
    }
    
    case CAbstractVar::TYPE_S32: {
      const s32 *pValue = (const s32 *)pData;
      FromInt(*pValue);
      return true;
    }
    
    case CAbstractVar::TYPE_U32: {
      const u32 *pValue = (const u32 *)pData;
      FromUInt(*pValue);
      return true;
    }

    case CAbstractVar::TYPE_S64: {
      const s64 *pValue = (const s64 *)pData;
      FromInt64(*pValue);
      return true;
    }

    case CAbstractVar::TYPE_U64: {
      const u64 *pValue = (const u64 *)pData;
      FromUInt64(*pValue);
      return true;
    }
    
    case CAbstractVar::TYPE_F32: {
      const f32 *pValue = (const f32 *)pData;
      FromFloat(*pValue);
      return true;
    }

    case CAbstractVar::TYPE_F64: {
      const f64 *pValue = (const f64 *)pData;
      FromDouble(*pValue);
      return true;
    }

    case CAbstractVar::TYPE_PTR: {
      FromPtr((void *)pData);
      return true;
    }
  }

  ASSERTALWAYS("Unsupported type passed into CScalarVar::FromRawData!");
  return false;
}

bool CScalarVar::ToBool() const
{
  return _data.as_bool;
}

s8 CScalarVar::ToInt8() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

u8 CScalarVar::ToUInt8() const
{ 
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

s16 CScalarVar::ToInt16() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

u16 CScalarVar::ToUInt16() const
{ 
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

s32 CScalarVar::ToInt() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

u32 CScalarVar::ToUInt() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

s64 CScalarVar::ToInt64() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

u64 CScalarVar::ToUInt64() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0;
}

f32 CScalarVar::ToFloat() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
  #ifdef SE_OLD_COMPILER 
    #pragma message("Variant.cpp : Missing implementation!")
  #else
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
  #endif
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0.0F;
}

f64 CScalarVar::ToDouble() const
{
  switch (_type)
  {
    case TYPE_BOOL: return _data.as_bool;
    case TYPE_U8: case TYPE_S8: return _data.as_u8;
    case TYPE_U16: case TYPE_S16: return _data.as_u16;
    case TYPE_U32: case TYPE_S32: return _data.as_u32;
  #ifdef SE_OLD_COMPILER 
    #pragma message("Variant.cpp : Missing implementation!")
  #else
    case TYPE_U64: case TYPE_S64: return _data.as_u64;
  #endif
    case TYPE_F32: return _data.as_f32;
    case TYPE_F64: return _data.as_f64;
  }
  
  return 0.0;
}

void *CScalarVar::ToPtr() const
{
  if (_type == TYPE_PTR) {
    return _data.as_ptr;
  }

  return NULL;
}

FVariant::FVariant()
{
  Clear();
}

FVariant::FVariant(const FVariant &src)
{
  Copy(src);
}

FVariant::FVariant(bool val)
{
  FromBool(val);
}

FVariant::FVariant(u8 val)
{
  FromUInt8(val);
}

FVariant::FVariant(s8 val)
{
  FromInt8(val);
}

FVariant::FVariant(u16 val)
{
  FromUInt16(val);
}

FVariant::FVariant(s16 val)
{
  FromInt16(val);
}

FVariant::FVariant(u32 val)
{
  FromUInt(val);
}

FVariant::FVariant(s32 val)
{
  FromInt(val);
}

FVariant::FVariant(u64 val)
{
  FromUInt64(val);
}

FVariant::FVariant(s64 val)
{
  FromInt64(val);
}

FVariant::FVariant(f32 val)
{
  FromFloat(val);
}

FVariant::FVariant(f64 val)
{
  FromDouble(val);
}

FVariant::FVariant(void *pVal)
{
  FromPtr(pVal);
}

FVariant::FVariant(const VEC2I &val)
{
  FromVec2i(val);
}

FVariant::FVariant(const VEC3I &val)
{
  FromVec3i(val);
}

FVariant::FVariant(const VEC2F &val)
{
  FromVec2f(val);
}

FVariant::FVariant(const VEC3F &val)
{
  FromVec3f(val);
}

FVariant::FVariant(const VEC2D &val)
{
  FromVec2d(val);
}

FVariant::FVariant(const VEC3D &val)
{
  FromVec3d(val);
}

FVariant::FVariant(const PLANE3F &val)
{
  FromPlane3f(val);
}

FVariant::FVariant(const PLANE3D &val)
{
  FromPlane3d(val);
}

FVariant::FVariant(const BOX3F &val)
{
  FromBox3f(val);
}

FVariant::FVariant(const BOX3D &val)
{
  FromBox3d(val);
}

FVariant::FVariant(const QUAT4F &val)
{
  FromQuat4f(val);
}

FVariant::FVariant(const MAT33F &val)
{
  FromMat33f(val);
}

FVariant::FVariant(const CPlacement3D &val)
{
  FromPlacement3D(val);
}

FVariant::FVariant(const FUuid &val)
{
  FromUuid(val);
}

FVariant::FVariant(const FString &val)
{
  FromString(val);
}

FVariant::FVariant(const FByteArray &val)
{
  FromByteArray(val);
}

FVariant::FVariant(const FPath &val)
{
  FromFilename(val);
}

FVariant::~FVariant()
{
  Clear();
}

void FVariant::Clear()
{
  // Clear scalar types in their way.
  if (GetType() <= CAbstractVar::TYPE_SCALAR_MAX) {
    _type = TYPE_NIL;
    _data.as_u64 = 0;
    return;
  }
  
  if (GetType() == TYPE_BYTEARRAY) {
    FByteArray *pAsByteArray = reinterpret_cast<FByteArray *>(this);
    delete pAsByteArray;
    _type  = TYPE_NIL;
    _data.as_u64 = 0;
    return;
  }

  _type = TYPE_NIL;
  FreeMemory(_data.as_ptr);
}

//! Copy data from another instance.
void FVariant::Copy(const FVariant &src)
{
  Clear();
  _type = src.GetType();
  
  // Simple copy for scalar types.
  if (src.GetType() <= TYPE_SCALAR_MAX) {
    _data.as_u64 = src._data.as_u64;
    return;
  }
  
  // A bit harder for complex types.
  _data.as_ptr = AllocMemory(SizeOf(src.GetType()));
  memcpy(_data.as_ptr, src._data.as_ptr, SizeOf(src.GetType()));
}

void FVariant::FromVec2i(const VEC2I& val)
{
  Clear();
  _type = TYPE_VEC2I;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromVec3i(const VEC3I& val)
{
  Clear();
  _type = TYPE_VEC3I;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromVec2f(const VEC2F& val)
{
  Clear();
  _type = TYPE_VEC2F;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromVec3f(const VEC3F& val)
{
  Clear();
  _type = TYPE_VEC3F;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromVec2d(const VEC2D& val)
{
  Clear();
  _type = TYPE_VEC2D;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromVec3d(const VEC3D& val)
{
  Clear();
  _type = TYPE_VEC3D;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromPlane3f(const PLANE3F& val)
{
  Clear();
  _type = TYPE_PLANE3F;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromPlane3d(const PLANE3D& val)
{
  Clear();
  _type = TYPE_PLANE3D;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromBox3f(const BOX3F& val)
{
  Clear();
  _type = TYPE_BOX3F;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromBox3d(const BOX3D& val)
{
  Clear();
  _type = TYPE_BOX3D;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromQuat4f(const QUAT4F& val)
{
  Clear();
  _type = TYPE_QUAT4F;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromMat33f(const MAT33F& val)
{
  Clear();
  _type = TYPE_MAT33F;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromPlacement3D(const CPlacement3D& val)
{
  Clear();
  _type = TYPE_PLACEMENT3D;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromUuid(const FUuid& val)
{
  Clear();
  _type = TYPE_UUID;
  _data.as_ptr = AllocMemory(SizeOf(GetType()));
  memcpy(_data.as_ptr, &val, SizeOf(GetType()));
}

void FVariant::FromString(const FString& val)
{
  Clear();
  _type = TYPE_STRING;
  _data.as_ptr = StringDuplicate(val.ConstData());
}

void FVariant::FromByteArray(const FByteArray& val)
{
  Clear();
  _type = TYPE_BYTEARRAY;
  _data.as_ptr = new FByteArray(val);
}

void FVariant::FromFilename(const FPath& val)
{
  Clear();
  _type = TYPE_FILENAME;
  _data.as_ptr = new FPath(val);
}

bool FVariant::FromRawData(enum CAbstractVar::Type type, const void *pData)
{
  switch (type)
  {
    case TYPE_VEC2I: {
      const VEC2I *pValue = (const VEC2I *)pData;
      FromVec2i(*pValue);
      return true;
    }

    case TYPE_VEC3I: {
      const VEC3I *pValue = (const VEC3I *)pData;
      FromVec3i(*pValue);
      return true;
    }

    case TYPE_VEC2F: {
      const VEC2F *pValue = (const VEC2F *)pData;
      FromVec2f(*pValue);
      return true;
    }

    case TYPE_VEC3F: {
      const VEC3F *pValue = (const VEC3F *)pData;
      FromVec3f(*pValue);
      return true;
    }

    case TYPE_VEC2D: {
      const VEC2D *pValue = (const VEC2D *)pData;
      FromVec2d(*pValue);
      return true;
    }

    case TYPE_VEC3D: {
      const VEC3D *pValue = (const VEC3D *)pData;
      FromVec3d(*pValue);
      return true;
    }

    case TYPE_PLANE3F: {
      const PLANE3F *pValue = (const PLANE3F *)pData;
      FromPlane3f(*pValue);
      return true;
    }

    case TYPE_PLANE3D: {
      const PLANE3D *pValue = (const PLANE3D *)pData;
      FromPlane3d(*pValue);
      return true;
    }

    case TYPE_BOX3F: {
      const BOX3F *pValue = (const BOX3F *)pData;
      FromBox3f(*pValue);
      return true;
    }

    case TYPE_BOX3D: {
      const BOX3D *pValue = (const BOX3D *)pData;
      FromBox3d(*pValue);
      return true;
    }

    case TYPE_QUAT4F: {
      const QUAT4F *pValue = (const QUAT4F *)pData;
      FromQuat4f(*pValue);
      return true;
    }

    case TYPE_MAT33F: {
      const MAT33F *pValue = (const MAT33F *)pData;
      FromMat33f(*pValue);
      return true;
    }

    case TYPE_UUID: {
      const FUuid *pValue = (const FUuid *)pData;
      FromUuid(*pValue);
      return true;
    }

    case TYPE_STRING: {
      const FString *pValue = (const FString *)pData;
      FromString(*pValue);
      return true;
    }

    case TYPE_BYTEARRAY: {
      const FByteArray *pValue = (const FByteArray *)pData;
      FromByteArray(*pValue);
      return true;
    }

    case TYPE_FILENAME: {
      const FPath *pValue = (const FPath *)pData;
      FromFilename(*pValue);
      return true;
    }
  }

  return CScalarVar::FromRawData(type, pData);
}

void FVariant::ToVec2i(VEC2I &val) const
{
  if (GetType() != TYPE_VEC2I) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToVec3i(VEC3I &val) const
{
  if (GetType() != TYPE_VEC3I) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToVec2f(VEC2F &val) const
{
  if (GetType() != TYPE_VEC2F) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToVec3f(VEC3F &val) const
{
  if (GetType() != TYPE_VEC3F) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToVec2d(VEC2D &val) const
{
  if (GetType() != TYPE_VEC2D) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToVec3d(VEC3D &val) const
{
  if (GetType() != TYPE_VEC3D) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToPlane3f(PLANE3F &val) const
{
  if (GetType() != TYPE_PLANE3F) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToPlane3d(PLANE3D &val) const
{
  if (GetType() != TYPE_PLANE3D) {
    return;
  }
  
  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToBox3f(BOX3F &val) const
{
  if (GetType() != TYPE_BOX3F) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToBox3d(BOX3D &val) const
{
  if (GetType() != TYPE_BOX3D) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToQuat4f(QUAT4F &val) const
{
  if (GetType() != TYPE_QUAT4F) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToMat33f(MAT33F &val) const
{
  if (GetType() != TYPE_MAT33F) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToPlacement3D(CPlacement3D &val) const
{
  if (GetType() != TYPE_PLACEMENT3D) {
    return;
  }

  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToUuid(FUuid &val) const
{
  if (GetType() != TYPE_UUID) {
    return;
  }
  
  memcpy(&val, _data.as_ptr, SizeOf(GetType()));
}

void FVariant::ToString(FString &val) const
{
  if (GetType() != TYPE_STRING) {
    return;
  }
  
  const FString *pAsStr = reinterpret_cast<const FString *>(&_data);
  val = *pAsStr;
}

void FVariant::ToByteArray(FByteArray &val) const
{
  if (GetType() != TYPE_BYTEARRAY) {
    return;
  }
  
  const FByteArray *pAsByteArray = reinterpret_cast<const FByteArray *>(_data.as_ptr);
  val = *pAsByteArray;
}

void FVariant::ToFilename(FPath &val) const
{
  if (GetType() != TYPE_FILENAME) {
    return;
  }
  
  const FPath *pAsFilename = reinterpret_cast<const FPath *>(_data.as_ptr);
  val = *pAsFilename;
}