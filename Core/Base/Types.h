/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CORE_TYPES_H
#define SE_INCL_CORE_TYPES_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/CoreApi.h>
#include <Core/Base/TypesExt.h>

#ifdef SE_OLD_COMPILER
  #define nullptr 0
  #define override
  #define final

  typedef int          intptr_t;
  typedef unsigned int uintptr_t;
#else
  #include <stdint.h> // uintptr_t etc.
#endif

typedef signed long  int    SLONG;
typedef signed short int    SWORD;
typedef signed char	        SBYTE;
typedef signed int          SINT;

#if defined(_MSC_VER)
typedef __int64 INT64;
#else
typedef long long INT64;
#endif

typedef INT64 SQUAD;

typedef unsigned long  int  ULONG;
typedef unsigned short int  UWORD;
typedef unsigned char       UBYTE;
typedef unsigned int        UINT;

#if defined(_MSC_VER)
typedef unsigned __int64 UINT64;
#else
typedef unsigned long long UINT64;
#endif

typedef UINT64 UQUAD;

#ifdef PLATFORM_UNIX
    #include <ctype.h>
    #include <errno.h>

    #define __forceinline inline
    
    #define _CrtCheckMemory() 1
    #define stricmp strcasecmp
    #define strcmpi strcasecmp
    #define strnicmp strncasecmp
    #define _vsnprintf vsnprintf
    #define _snprintf snprintf
    #define _finite isfinite
    
    inline void _RPT_do(const char *type, const char *fmt, ...)
    {
    } // _RPT0
    
    #define _CRT_WARN "_CRT_WARN"
    #define _CRT_ERROR "_CRT_ERROR"
    #define _CRT_ASSER "_CRT_ASSERT"
    
    #define _RPT0(type, fmt)                 _RPT_do(type, fmt)
    #define _RPT1(type, fmt, a1)             _RPT_do(type, fmt, a1)
    #define _RPT2(type, fmt, a1, a2)         _RPT_do(type, fmt, a1, a2)
    #define _RPT3(type, fmt, a1, a2, a3)     _RPT_do(type, fmt, a1, a2, a3)
    #define _RPT4(type, fmt, a1, a2, a3, a4) _RPT_do(type, fmt, a1, a2, a3, a4)

    #if (!defined MAX_PATH)
      #define MAX_PATH 256
    #endif
    
    // IO
    #define _O_BINARY   0
    #define _O_RDONLY   O_RDONLY
    #define _S_IWRITE   S_IWRITE
    #define _S_IREAD    S_IREAD
    
    #define _mkdir(x) mkdir(x, S_IRWXU)
    #define _open open
    #define _close close
    
    inline ULONG _rotl(ULONG ul, int bits)
    {
      // DG: according to http://blog.regehr.org/archives/1063 this is fast
      return (ul << bits) | (ul >> (-bits & 31));
    }

    typedef long long  __int64;
    typedef uint16_t WORD;
    typedef uint32_t DWORD;
    typedef signed long  int    LONG;

    typedef void *HWND;  /* !!! FIXME this sucks. */
    typedef void *HINSTANCE;  /* !!! FIXME this sucks. */
    typedef void *HGLRC;  /* !!! FIXME this sucks. */
    typedef ULONG COLORREF;  /* !!! FIXME this sucks. */

    typedef struct
    {
        LONG x;
        LONG y;
    } POINT;

    typedef struct
    {
        LONG left;
        LONG top;
        LONG right;
        LONG bottom;
    } RECT;
#endif


#define MAX_SLONG ((SLONG)0x7FFFFFFFL)
#define MAX_SWORD ((UWORD)0x7FFF)
#define MAX_SBYTE ((SBYTE)0x7F)

#define MIN_SLONG ((SLONG)0x80000000L)
#define MIN_SWORD ((SWORD)0x8000)
#define MIN_SBYTE ((SBYTE)0x80)

#define MIN_ULONG ((ULONG)0x00000000L)
#define MIN_UWORD ((UWORD)0x0000)
#define MIN_UBYTE ((UBYTE)0x00)

#define MAX_ULONG ((ULONG)0xFFFFFFFFL)
#define MAX_UWORD ((UWORD)0xFFFF)
#define MAX_UBYTE ((UBYTE)0xFF)

typedef int BOOL;		        // this is for TRUE/FALSE
typedef long int RESULT;		// for error codes
typedef long int INDEX;     // for indexed values and quantities

#define FALSE 0
#define TRUE  1

#define NONE 0
#define NOTHING ((void) 0)
#define FOREVER for (;;)

#define DECLARE_NOCOPYING(classname)        \
  classname(const classname &c);            \
  classname &operator=(const classname &c); 
#define IMPLEMENT_NOCOPYING(classname)      \
  classname::classname(const classname &c) { ASSERT(FALSE); };            \
  classname &classname::operator=(const classname &c) { ASSERT(FALSE); return *this; }; 

// standard angles
#define ANGLE_0    (  0.0f)
#define ANGLE_90   ( 90.0f)
#define ANGLE_180  (180.0f)
#define ANGLE_270  (270.0f)
#define ANGLE_360  (360.0f)

// you need <stddef.h> for this!
#define structptr(structure, member, ptr) \
( (struct structure *) ( ((UBYTE *)(ptr)) - \
 offsetof(struct structure, member)) )

// standard types

// simple types
typedef SLONG   PIX;    // pixel coordinates
typedef SLONG   TEX;    // texel coordinates
typedef SLONG   MEX;    // texels in mip-level 0
typedef float   FLOAT;
typedef double  DOUBLE;
typedef float   ANGLE;
typedef float   TIME;
typedef FLOAT   RANGE;
typedef ULONG   COLOR;  // color is always in 32 bit true-color format

// macros for windows/croteam true color conversion
#define CLRF_CLR(clr) ( ((clr & 0xff000000) >> 24) | \
                        ((clr & 0x00ff0000) >>  8) | \
                        ((clr & 0x0000ff00) <<  8))
#define CLR_CLRF(clrref) ( ((clrref & 0x000000ff) << 24) | \
                           ((clrref & 0x0000ff00) <<  8) | \
                           ((clrref & 0x00ff0000) >>  8))

// z-buffer depth constants
#define ZBUF_FRONT  (0.0f)
#define ZBUF_BACK   (1.0f)

// alpha factor constants
#define CT_OPAQUE      MAX_UBYTE
#define CT_TRANSPARENT MIN_UBYTE

// line types (masks)
#define _FULL_	   0xFFFFFFFF
#define _SYMMET16_ 0xFF88FF88
#define _SYMMET32_ 0xFFFF0180
#define _POINT_    0xAAAAAAAA

#define _DOT2_	   0xCCCCCCCC
#define _DOT4_	   0xF0F0F0F0
#define _DOT8_	   0xFF00FF00
#define _DOT16_    0xFFFF0000

#define _TY31_	   0xEEEEEEEE
#define _TY62_	   0xFCFCFCFC
#define _TY124_    0xFFF0FFF0
#define _TY13_	   0x88888888
#define _TY26_	   0xC0C0C0C0
#define _TY412_    0xF000F000

// some mexels constants
#define MAX_MEX_LOG2 10
#define MIN_MEX_LOG2  0
#define MAX_MEX     (1L<<MAX_MEX_LOG2)
#define MIN_MEX     (1L<<MIN_MEX_LOG2)

// macro for converting mexels to meters
#define METERS_MEX(mex)    ((FLOAT)(((FLOAT)mex)/MAX_MEX))
#define MEX_METERS(meters) ((MEX)(meters*MAX_MEX))

#define ARRAYCOUNT(array) (sizeof(array)/sizeof((array)[0]))

// sound volume constants
#define SL_VOLUME_MIN (0.0f)
#define SL_VOLUME_MAX (4.0f)

inline DOUBLE FLOATtoDOUBLE(const FLOAT f) { return DOUBLE(f); }
inline FLOAT DOUBLEtoFLOAT(const DOUBLE d) { return FLOAT(d);  }

inline float UpperLimit(float x) { return +3E38f; }
inline float LowerLimit(float x) { return -3E38f; }
inline double UpperLimit(double x) { return +1E308; }
inline double LowerLimit(double x) { return -1E308; }
inline SLONG UpperLimit(SLONG x) { return MAX_SLONG; }
inline SLONG LowerLimit(SLONG x) { return MIN_SLONG; }
inline SWORD UpperLimit(SWORD x) { return MAX_SWORD; }
inline SWORD LowerLimit(SWORD x) { return MIN_SWORD; }

// class predeclarations
class CAnyProjection3D;
class CChangeable;
class CChangeableRT;
class CConsole;
class CIsometricProjection3D;
class CListHead;
class CListNode;
class CPlacement3D;
class CProjection3D;
class CPlanarGradients;
class CParallelProjection3D;
class CPerspectiveProjection3D;
class CRelationDst;
class CRelationLnk;
class CRelationSrc;
class CProfileCounter;
class CProfileForm;
class CProfileTimer;
class CSimpleProjection3D_DOUBLE;
class CShell;
class CShellSymbol;
class CSyncMutex;
class CSyncLock;
class FString;
class CTimer;
class CTimerHandler;
class CTimerValue;
class CObject3D;
class CObjectSector;
class CObjectPolygon;
class CObjectVertex;
class CObjectPlane;
class CObjectMaterial;
class CUpdateable;
class CUpdateableRT;

// [SEE] Base
class FByteArray;
class FCryptographicHash;
class FProcessorId;
class FUuid;
class FVariant;

#define CTString FString
#define CByteArray FByteArray
#define CCryptographicHash FCryptographicHash
#define CProcessorId FProcessorId
#define CUuid FUuid
#define CVariant FVariant

// [SEE] Graphics
class FImage;
class FImageLoader;
class FImageSaver;
#define CImage FImage

// Objects
class CGameObject;
class CRefCounted;
class CNode;
class IObjectFactory;

// IO Classes
class FCompressor;
class FPath;
class CTFileStream;
class CTMemoryStream;
class CResource;
class FPathDictionary;
class CTStream;
class FReadWriteDevice;
class FBufferDevice;
class FFileDevice;
class FDataStream;

#define CCompressor FCompressor
#define CLZCompressor FLZCompressor
#define CRLEBBCompressor FRLEBBCompressor
#define CzlibCompressor FDeflateCompressor
#define CTFileName FPath
#define CDataStream FDataStream

class FFileIndexNode;
class FArchive;
class FArchiveEntry;
class FArchiveLoader;

#define CArchiveEntry FArchiveEntry
#define CAbstractArchive FArchive

// Modules
class FModule;
class FModuleInfo;
class FModuleClass;

#define CPluginModule FModule
#define CPluginClass FModuleClass

// Backward compat for template classes.
#define CAllocationArray TAllocationArray
#define CDynamicArray TDynamicArray
#define CDynamicArrayIterator TDynamicArrayIterator
#define CDynamicContainer TDynamicContainer
#define CDynamicStackArray TDynamicStackArray
#define CHashTable THashTable
#define CHashTableSlot THashTableSlot
#define CNameTableSlot TNameTableSlot
#define CLinearAllocator TLinearAllocator
#define CNameTable TNameTable
#define CSelection TSelection
#define CStaticArray TStaticArray
#define CStaticArrayIterator TStaticArrayIterator
#define CStaticStackArray TStaticStackArray

// Backward compat for template math classes.
#define FixInt TFixInt
#define Vector TVector
#define Matrix TMatrix
#define Quaternion TQuaternion
#define AABBox TAABBox
#define OBBox TOBBox
#define Plane TPlane

#define BSPNode TBspNode
#define BSPVertex TBspVertex
#define BSPTree TBspTree
#define BSPVertexContainer TBspVertexContainer
#define BSPEdge TBspEdge
#define BSPPolygon TBspPolygon
#define BSPLine TBspLine
#define BSPCutter TBspCutter

// template class predeclarations
template<class Type, int iOffset> class CListIter;
template<class Type> class TDynamicArray;
template<class Type> class TDynamicStackArray;
template<class Type> class TDynamicArrayIterator;
template<class Type> class TStaticArray;
template<class Type> class TStaticStackArray;
template<class Type> class TStaticArrayIterator;
template<class Type> class TLinearAllocator;
template<class Type> class TDynamicContainer;
template<class Type, int iRows, int iColumns> class TMatrix;
template<class Type, int iDimensions> class TAABBox;
template<class Type, int iDimensions> class TVector;
template<class Type, int iDimensions> class TPlane;
template<class Type> class TOBBox;
template<class Type> class TQuaternion;
template<int iInt, int iFrac> class TFixInt;

template<class Type, int iDimensions> class TBspVertex;
template<class Type, int iDimensions> class TBspVertexContainer;
template<class Type, int iDimensions> class TBspEdge;
template<class Type, int iDimensions> class TBspNode;
template<class Type, int iDimensions> class TBspPolygon;
template<class Type, int iDimensions> class TBspTree;
template<class Type, int iDimensions> class TBspCutter;

typedef TFixInt<16,16>           FIX16_16;

// [SEE] Types
typedef TVector<INDEX, 2>        VEC2I;
typedef TVector<INDEX, 3>        VEC3I;
typedef TVector<FLOAT, 2>        VEC2F;
typedef TVector<FLOAT, 3>        VEC3F;
typedef TVector<DOUBLE, 2>       VEC2D;
typedef TVector<DOUBLE, 3>       VEC3D;

typedef TPlane<FLOAT, 3>         PLANE3F;
typedef TPlane<DOUBLE, 3>        PLANE3D;

typedef TAABBox<INDEX, 2>        BOX2I;
typedef TAABBox<INDEX, 3>        BOX3I;
typedef TAABBox<FLOAT, 2>        BOX2F;
typedef TAABBox<FLOAT, 3>        BOX3F;
typedef TAABBox<DOUBLE, 2>       BOX2D;
typedef TAABBox<DOUBLE, 3>       BOX3D;

typedef TQuaternion<FLOAT>       QUAT4F;
typedef TQuaternion<DOUBLE>      QUAT4D;

typedef TMatrix<FLOAT, 3, 3>     MAT33F;
typedef TMatrix<DOUBLE, 3, 3>    MAT33D;

// vectors
typedef TVector<PIX, 2>          PIX2D;
typedef TVector<MEX, 2>          MEX2D;
typedef TVector<PIX, 3>          PIX3D;
typedef TVector<ANGLE, 3>        ANGLE3D;
typedef TVector<FLOAT, 2>        FLOAT2D;
typedef TVector<FLOAT, 3>        FLOAT3D;
typedef TVector<DOUBLE, 2>       DOUBLE2D;
typedef TVector<DOUBLE, 3>       DOUBLE3D;

// planes
typedef TPlane<FLOAT, 3>         FLOATplane3D;
typedef TPlane<DOUBLE, 3>        DOUBLEplane3D;

// axis-aligned bounding boxes
typedef TAABBox<MEX, 2>          MEXaabbox2D;
typedef TAABBox<PIX, 2>          PIXaabbox2D;
typedef TAABBox<FLOAT, 2>        FLOATaabbox2D;
typedef TAABBox<FLOAT, 3>        FLOATaabbox3D;
typedef TAABBox<DOUBLE, 2>       DOUBLEaabbox2D;
typedef TAABBox<DOUBLE, 3>       DOUBLEaabbox3D;

// oriented bounding boxes
typedef TOBBox<FLOAT>            FLOATobbox3D;
typedef TOBBox<DOUBLE>           DOUBLEobbox3D;

// matrices
typedef TMatrix<PIX, 2, 2>       PIXmatrix2D;
typedef TMatrix<PIX, 3, 3>       PIXmatrix3D;
typedef TMatrix<ANGLE, 3, 3>     ANGLEmatrix3D;
typedef TMatrix<FLOAT, 2, 2>     FLOATmatrix2D;
typedef TMatrix<FLOAT, 3, 3>     FLOATmatrix3D;
typedef TMatrix<DOUBLE, 3, 3>    DOUBLEmatrix3D;
typedef FLOAT Matrix12[12];

// quaternions
typedef TQuaternion<FLOAT>       FLOATquat3D;
typedef TQuaternion<DOUBLE>      DOUBLEquat3D;

// BSP types
typedef TBspVertex<DOUBLE, 3>          DOUBLEbspvertex3D;
typedef TBspVertexContainer<DOUBLE, 3> DOUBLEbspvertexcontainer3D;
typedef TBspEdge<DOUBLE, 3>            DOUBLEbspedge3D;
typedef TBspNode<DOUBLE, 3>            DOUBLEbspnode3D;
typedef TBspPolygon<DOUBLE, 3>         DOUBLEbsppolygon3D;
typedef TBspTree<DOUBLE, 3>            DOUBLEbsptree3D;
typedef TBspCutter<DOUBLE, 3>          DOUBLEbspcutter3D;

typedef TBspVertex<FLOAT, 3>          FLOATbspvertex3D;
typedef TBspVertexContainer<FLOAT, 3> FLOATbspvertexcontainer3D;
typedef TBspEdge<FLOAT, 3>            FLOATbspedge3D;
typedef TBspNode<FLOAT, 3>            FLOATbspnode3D;
typedef TBspPolygon<FLOAT, 3>         FLOATbsppolygon3D;
typedef TBspTree<FLOAT, 3>            FLOATbsptree3D;
typedef TBspCutter<FLOAT, 3>          FLOATbspcutter3D;

// general clearing functions
template<class cType>
inline void Clear(cType &t) { t.cType::Clear(); };

// specific clearing functions for built-in types
inline void Clear(signed long int sli) {};
inline void Clear(unsigned long int uli) {};
inline void Clear(int i) {};
inline void Clear(float i) {};
inline void Clear(double i) {};
inline void Clear(void *pv) {};

#define SYMBOLLOCATOR(symbol)

class CTranslationPair;
class FPathDictionaryEntry;

template<class Type> class TNameTable;
typedef TNameTable<FPathDictionaryEntry> TNameTable_FPathDictionaryEntry;
typedef TNameTable<CTranslationPair> TNameTable_CTranslationPair;

class CResourceStock;

#endif  /* include-once check. */
