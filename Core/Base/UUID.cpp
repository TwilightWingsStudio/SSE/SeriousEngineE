/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Base/UUID.h>

FUuid::FUuid()
{
  Clear();
}

FUuid::FUuid(const FUuid &other)
{
  d.a = other.d.a;
  d.b = other.d.b;
  d.c = other.d.c;
  d.d = other.d.d;
}

UBYTE *FUuid::Data()
{
  return &_bytes[0];
}

void FUuid::Clear()
{
  d.a = 0;
  d.b = 0;
  d.c = 0;
  d.d = 0;
}

BOOL FUuid::operator==(const FUuid &other) const
{
  return (d.a == other.d.a) && (d.b == other.d.b) && (d.c == other.d.c) && (d.d == other.d.d);
};