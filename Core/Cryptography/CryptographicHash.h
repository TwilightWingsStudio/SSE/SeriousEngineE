/* Copyright (c) 2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_CRYPTOGRAPHICHASH_H
#define SE_INCL_CRYPTOGRAPHICHASH_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class FByteArray;
class FCryptographicHashPrivate;

//! Class that represents universally unique identifier.
class CORE_API FCryptographicHash final
{
  public:
    enum Algorithm
    {
      //! Message Digest 4
      MD4,

      //! Message Digest 5
      MD5,

      //! Secure Hash 1
      SHA1,

      //! SHA256 (Secure Hash 2)
      SHA256,
    };

  public:
    //! Constructor.
    FCryptographicHash(Algorithm method);
    
    //! Destructor.
    ~FCryptographicHash();
    
    //! Add some bytes to mesagge digest.
    void AddData(const char *pData, size_t length);
    
    //! Add bytes from byte array to message digest.
    void AddData(const FByteArray &ba);
    
    //! Reset context.
    void Reset();
    
    //! Calculate result or return if it was previously done.
    FByteArray Result();
    
  private:
    FCryptographicHashPrivate *d_ptr;
};

#endif  /* include-once check. */
