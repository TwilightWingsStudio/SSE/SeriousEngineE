/* Copyright (c) 2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include <string.h>   /* for memcpy() */
#ifndef _WIN32_WCE
#include <sys/types.h>    /* for stupid systems */
#else
#include <windef.h>
#include <types.h>
#endif

#include "md5.h"

static unsigned int S[] = {7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                       5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
                       4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                       6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21};

static unsigned int K[] = {0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
                       0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
                       0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
                       0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
                       0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
                       0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
                       0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
                       0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
                       0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
                       0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
                       0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
                       0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
                       0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
                       0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
                       0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
                       0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391};

void byteSwap(md5uword *buf, unsigned words)
{
  const unsigned int byteOrderTest = 0x1;

  if (((const char *)&byteOrderTest)[0] == 0) {
    md5byte *p = (md5byte *)buf;

    do {
      *buf++ = (md5uword)((unsigned)p[3] << 8 | p[2]) << 16 |
          ((unsigned)p[1] << 8 | p[0]);
      p += 4;
    } while (--words);
  }
}

/*
 * Start MD5 accumulation. Set bit count to 0 and state to magic
 * initialization constants.
 */
void MD5_Init(struct MD5Context *ctx)
{
  ctx->state[0] = 0x67452301;
  ctx->state[1] = 0xefcdab89;
  ctx->state[2] = 0x98badcfe;
  ctx->state[3] = 0x10325476;

  ctx->bytes[0] = 0;
  ctx->bytes[1] = 0;
}

/*
 * Update context to reflect the concatenation of another buffer full
 * of bytes.
 */
void MD5_Update(struct MD5Context *ctx, const md5byte *buf, unsigned len)
{
  md5uword t;

  /* Update byte count */

  t = ctx->bytes[0];
  if ((ctx->bytes[0] = t + len) < t) {
    ctx->bytes[1]++;  /* Carry from low to high */
  }

  t = 64 - (t & 0x3f);  /* Space available in ctx->in (at least 1) */
  if (t > len) {
    memcpy((md5byte *)ctx->in + 64 - t, buf, len);
    return;
  }
  /* First chunk is an odd size */
  memcpy((md5byte *)ctx->in + 64 - t, buf, t);
  byteSwap(ctx->in, 16);
  MD5_Transform(ctx->state, ctx->in);
  buf += t;
  len -= t;

  /* Process data in 64-byte chunks */
  while (len >= 64)
  {
    memcpy(ctx->in, buf, 64);
    byteSwap(ctx->in, 16);
    MD5_Transform(ctx->state, ctx->in);
    buf += 64;
    len -= 64;
  }

  /* Handle any remaining bytes of data. */
  memcpy(ctx->in, buf, len);
}

/*
 * Final wrapup - pad to 64-byte boundary with the bit pattern 
 * 1 0* (64-bit count of bits processed, MSB-first)
 */
void MD5_Final(struct MD5Context *ctx, md5byte digest[16])
{
  int count = ctx->bytes[0] & 0x3f; /* Number of bytes in ctx->in */
  md5byte *p = (md5byte *)ctx->in + count;

  /* Set the first char of padding to 0x80.  There is always room. */
  *p++ = 0x80;

  /* Bytes of padding needed to make 56 bytes (-8..55) */
  count = 56 - 1 - count;

  if (count < 0) {  /* Padding forces an extra block */
    memset(p, 0, count + 8);
    byteSwap(ctx->in, 16);
    MD5_Transform(ctx->state, ctx->in);
    p = (md5byte *)ctx->in;
    count = 56;
  }
  memset(p, 0, count);
  byteSwap(ctx->in, 14);

  /* Append length in bits and transform */
  ctx->in[14] = ctx->bytes[0] << 3;
  ctx->in[15] = ctx->bytes[1] << 3 | ctx->bytes[0] >> 29;
  MD5_Transform(ctx->state, ctx->in);

  byteSwap(ctx->state, 4);
  memcpy(digest, ctx->state, 16);
  memset(ctx, 0, sizeof(*ctx)); /* In case it's sensitive */
}

/* The four core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */
#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))

/* This is the central step in the MD5 algorithm. */
#define MD5STEP(f,w,x,y,z,in,s) \
   (w += f(x,y,z) + in, w = (w<<s | w>>(32-s)) + x)

/*
 * The core of the MD5 algorithm, this alters an existing MD5 hash to
 * reflect the addition of 16 longwords of new data.  MD5Update blocks
 * the data and converts bytes into longwords for this routine.
 */
void MD5_Transform(md5uword state[4], const md5uword in[16])
{
  int i, j;
  md5uword a, b, c, d, temp;

  a = state[0];
  b = state[1];
  c = state[2];
  d = state[3];

  for (i = 0; i < 64; i++)
  {
    switch (i / 16)
    {
      /* Round 1 (0-15) */
      case 0:
        j = i;
        MD5STEP(F1, a, b, c, d, in[j] + K[i], S[i]);
        break;

      /* Round 2 (16-31) */
      case 1:
        j = ((i * 5) + 1) % 16;
        MD5STEP(F2, a, b, c, d, in[j] + K[i], S[i]);
        break;

      /* Round 3 (32-47) */
      case 2:
        j = ((i * 3) + 5) % 16;
        MD5STEP(F3, a, b, c, d, in[j] + K[i], S[i]);
        break;

      /* Round 4 (48-63) */
      default:
        j = (i * 7) % 16;
        MD5STEP(F4, a, b, c, d, in[j] + K[i], S[i]);
        break;
    }

    temp = d;
    d = c;
    c = b;
    b = a;
    a = temp;
  }

  state[0] += a;
  state[1] += b;
  state[2] += c;
  state[3] += d;
}
