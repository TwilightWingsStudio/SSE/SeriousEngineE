#include "StdH.h"

#include <Core/Base/ByteArray.h>
#include <Core/Cryptography/CryptographicHash.h>
#include <Core/Cryptography/md5.h>
#include <Core/Cryptography/sha1.h>
#include <Core/Cryptography/sha256.h>

class FCryptographicHashPrivate
{
  public:
    FCryptographicHash::Algorithm method;
    union {
      MD5Context md5Context;
      SHA_CTX sha1Context;
      SHA256_CTX sha256Context;
    };
    FByteArray result;
};

FCryptographicHash::FCryptographicHash(Algorithm method) : d_ptr(new FCryptographicHashPrivate)
{
  d_ptr->method = method;
  Reset();
}

FCryptographicHash::~FCryptographicHash()
{
  delete d_ptr;
}

void FCryptographicHash::Reset()
{
  switch (d_ptr->method)
  {
    case MD5: {
      MD5_Init(&d_ptr->md5Context);
    } break;

    case SHA1: {
      SHA1_Init(&d_ptr->sha1Context);
    } break;

    case SHA256: {
      SHA256_Init(&d_ptr->sha256Context);
    } break;
  }
}

void FCryptographicHash::AddData(const char *pData, size_t length)
{
  switch (d_ptr->method)
  {
    case MD5: {
      MD5_Update(&d_ptr->md5Context, (const unsigned char *)pData, length);
    } break;

    case SHA1: {
      SHA1_Update(&d_ptr->sha1Context, pData, length);
    } break;

    case SHA256: {
      SHA256_Update(&d_ptr->sha256Context, (const unsigned char *)pData, length);
    } break;
  }

  d_ptr->result.Clear();
}

void FCryptographicHash::AddData(const FByteArray &ba)
{
  AddData((const char *)ba.ConstData(), ba.Size());
}

FByteArray FCryptographicHash::Result()
{
  if (!d_ptr->result.IsNull()) {
    return d_ptr->result;
  }

  switch (d_ptr->method)
  {
    case MD5: {
      MD5Context copy = d_ptr->md5Context;
      d_ptr->result.Resize(16);
      MD5_Final(&copy, (unsigned char *)d_ptr->result.Data());
    } break;

    case SHA1: {
      SHA_CTX copy = d_ptr->sha1Context;
      d_ptr->result.Resize(20);
      SHA1_Final((unsigned char *)d_ptr->result.Data(), &copy);
    } break;

    case SHA256: {
      SHA256_CTX copy = d_ptr->sha256Context;
      d_ptr->result.Resize(32);
      SHA256_Final(&copy, (unsigned char *)d_ptr->result.Data());
    } break;
  }

  return d_ptr->result;
}