/* Copyright (c) 2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef MD5_H
#define MD5_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned char md5byte;
typedef unsigned int md5uword;

typedef struct MD5Context {
  md5uword state[4];
  md5uword bytes[2];
  md5uword in[16];
} MD5Context_t;

void MD5_Init(struct MD5Context *context);
void MD5_Update(struct MD5Context *context, const md5byte *buf, unsigned len);
void MD5_Final(struct MD5Context *context, unsigned char digest[16]);
void MD5_Transform(md5uword state[4], const md5uword in[16]);

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* !MD5_H */
