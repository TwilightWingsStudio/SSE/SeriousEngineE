/* Copyright (c) 2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SHA256_H
#define SHA256_H

#ifdef __cplusplus
extern "C" {
#endif

/*************************** HEADER FILES ***************************/
#include <stddef.h>

/****************************** MACROS ******************************/
#define SHA256_BLOCK_SIZE 32            // SHA256 outputs a 32 byte digest

/**************************** DATA TYPES ****************************/
typedef unsigned char sha256byte;             // 8-bit byte
typedef unsigned int  sha256uword;             // 32-bit word, change to "long" for 16-bit machines

typedef struct {
	sha256byte data[64];
	sha256uword datalen;
	unsigned long long bitlen;
	sha256uword state[8];
} SHA256_CTX;

/*********************** FUNCTION DECLARATIONS **********************/
void SHA256_Init(SHA256_CTX *ctx);
void SHA256_Update(SHA256_CTX *ctx, const sha256byte data[], size_t len);
void SHA256_Final(SHA256_CTX *ctx, sha256byte hash[]);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // SHA256_H
